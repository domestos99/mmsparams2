import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CoreModule} from '@app/core';
import {SharedModule} from '@app/shared';
import {RouterModule} from '@angular/router';
import {ErrorInterceptor} from '@app/core/interceptors';
import {AppRoutingModule} from '@app/app.routing';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {DashboardComponent} from '@app/modules/documentation/dashboard/dashboard.component';
import {NavigationComponent} from '@app/layouts/documentation/navigation/navigation.component';
import {InstallComponent} from '@app/modules/documentation/install/install.component';
import {TestsComponent} from '@app/modules/documentation/tests/tests.component';
import {TechnicalComponent} from '@app/modules/documentation/technical/technical.component';
import {DevelopmentComponent} from '@app/modules/documentation/development/development.component';
import {TestsDocComponent} from '@app/modules/documentation/testsdoc/tests-doc.component';
import {TestNotesComponent} from '@app/modules/documentation/test-notes/test-notes.component';
import {AnalyticsService} from '@app/core/utils/analytics.service';
import {HeaderComponent} from '@app/layouts/documentation/header/header.component';
import {DocumentationLayoutComponent} from '@app/layouts/documentation/documentation-layout.component';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HeaderComponent,

    DashboardComponent,

    InstallComponent,
    TestsComponent,
    TechnicalComponent,
    DevelopmentComponent,
    TestsDocComponent,
    TestNotesComponent,


    DocumentationLayoutComponent,
  ],
  imports: [
    BrowserModule,
    CoreModule,
    SharedModule,
    RouterModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  entryComponents:
    [

      // modals here

    ],
  providers: [

    AnalyticsService,
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},


  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
