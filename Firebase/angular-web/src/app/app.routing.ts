import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DocumentationLayoutComponent} from '@app/layouts/documentation/documentation-layout.component';
import {PUBLIC_ROUTES} from '@app/layouts/documentation/documentation-layout.routes';

const routes: Routes = [
  {
    path: '', redirectTo: '/dashboard', pathMatch: 'full'
  },
  {
    path: '',
    component: DocumentationLayoutComponent,
    data: {title: 'Secure Views'},
    children: PUBLIC_ROUTES
  },
  {
    path: '**', redirectTo: '/dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // useHash: true,
    //  enableTracing: true
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {
}

