import {Routes} from '@angular/router';
import {DashboardComponent} from "@app/modules/documentation/dashboard/dashboard.component";
import {InstallComponent} from "@app/modules/documentation/install/install.component";
import {TestsComponent} from "@app/modules/documentation/tests/tests.component";
import {TechnicalComponent} from "@app/modules/documentation/technical/technical.component";
import {DevelopmentComponent} from "@app/modules/documentation/development/development.component";
import {TestsDocComponent} from "@app/modules/documentation/testsdoc/tests-doc.component";
import {TestNotesComponent} from "@app/modules/documentation/test-notes/test-notes.component";

export const PUBLIC_ROUTES: Routes = [

  {path: '', redirectTo: '', pathMatch: 'full'},
  {path: '', component: DashboardComponent},
  {path: 'dashboard', component: DashboardComponent},


  {path: 'install', component: InstallComponent},
  {path: 'tests', component: TestsComponent},
  {path: 'tests-doc', component: TestsDocComponent},
  {path: 'technical', component: TechnicalComponent},
  {path: 'development', component: DevelopmentComponent},
  {path: 'test-notes', component: TestNotesComponent},








];
