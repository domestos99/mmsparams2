import {NgModule, Optional, SkipSelf} from '@angular/core';
import {throwIfAlreadyLoaded} from '@app/core/guards/module-import.guard';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  exports: [
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule,
  ]
})
export class AppMaterialModule {
  constructor(@Optional() @SkipSelf() parentModule: AppMaterialModule) {
    throwIfAlreadyLoaded(parentModule, 'AppMaterialModule');
  }
}
