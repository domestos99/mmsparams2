import {Component, Input, Output, EventEmitter, ViewChild} from '@angular/core';


@Component({
  selector: 'download',
  templateUrl: 'download.component.html'
})
export class DownloadComponent {

  @Input() type : string;

}
