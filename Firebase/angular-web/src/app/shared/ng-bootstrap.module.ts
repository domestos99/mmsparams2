import {NgModule, Optional, SkipSelf} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {throwIfAlreadyLoaded} from '@app/core/guards/module-import.guard';

@NgModule({
  imports: [
    NgbModule
  ],
  exports: [
    NgbModule
  ],
})
export class NgBootstrapModule {
  constructor(@Optional() @SkipSelf() parentModule: NgBootstrapModule) {
    throwIfAlreadyLoaded(parentModule, 'NgBootstrapModule');
  }
}
