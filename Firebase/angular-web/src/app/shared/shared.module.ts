import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {MessageDocComponent} from '@app/shared/components/message-doc/message-doc.component';
import {DownloadComponent} from '@app/shared/components/download/download.component';
import {BoxComponent} from '@app/shared/components/box/box.component';
import {FabButtonTopComponent} from '@app/shared/components/fab-button-top/fab-button-top.component';
import {NgBootstrapModule} from '@app/shared/ng-bootstrap.module';
import {ExpandHideButtonComponent} from '@app/shared/components/expand-hide-button/expand-hide-button.component';
import {AppMaterialModule} from '@app/shared/app-material.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AppMaterialModule,
    NgBootstrapModule
  ],
  declarations: [

    MessageDocComponent,
    DownloadComponent,
    BoxComponent,
    FabButtonTopComponent,
    ExpandHideButtonComponent,


  ],
  entryComponents:
    [],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    NgBootstrapModule,
    AppMaterialModule,

    MessageDocComponent,
    DownloadComponent,
    BoxComponent,
    FabButtonTopComponent,
    ExpandHideButtonComponent,


  ]
})
export class SharedModule {
}
