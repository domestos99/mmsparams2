package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

public class MyAcknowledgeInd extends AcknowledgeInd
{
    public MyAcknowledgeInd(int mmsVersion, byte[] transactionId) throws InvalidHeaderValueException
    {
        super(mmsVersion, transactionId);
    }

    MyAcknowledgeInd(PduHeaders headers)
    {
        super(headers);
    }
}
