package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

public class MyDeliveryInd extends DeliveryInd
{
    public MyDeliveryInd() throws InvalidHeaderValueException
    {
    }

    public MyDeliveryInd(PduHeaders headers)
    {
        super(headers);
    }


    public byte[] getApplicId()
    {
        return mPduHeaders.getTextString(PduHeaders.APPLIC_ID);
    }

    public void setApplicId(byte[] value)
    {
        mPduHeaders.setTextString(value, PduHeaders.APPLIC_ID);
    }

    public byte[] getAuxApplicId()
    {
        return mPduHeaders.getTextString(PduHeaders.AUX_APPLIC_ID);

    }

    public void setAuxApplicId(byte[] value)
    {
        mPduHeaders.setTextString(value, PduHeaders.AUX_APPLIC_ID);
    }

    public byte[] getReplyApplicId()
    {
        return mPduHeaders.getTextString(PduHeaders.REPLY_APPLIC_ID);
    }

    public void setReplyApplicId(byte[] value)
    {
        mPduHeaders.setTextString(value, PduHeaders.REPLY_APPLIC_ID);
    }

    public EncodedStringValue getStatusText()
    {
        return mPduHeaders.getEncodedStringValue(PduHeaders.STATUS_TEXT);
    }

    public void setStatusText(EncodedStringValue value)
    {
        mPduHeaders.setEncodedStringValue(value, PduHeaders.STATUS_TEXT);
    }


    /*
     * Optional, not supported header fields:
     *
     *     public byte[] getApplicId() {return null;}
     *     public void setApplicId(byte[] value) {}
     *
     *     public byte[] getAuxApplicId() {return null;}
     *     public void getAuxApplicId(byte[] value) {}
     *
     *     public byte[] getReplyApplicId() {return 0x00;}
     *     public void setReplyApplicId(byte[] value) {}
     *
     *     public EncodedStringValue getStatusText() {return null;}
     *     public void setStatusText(EncodedStringValue value) {}
     */
}
