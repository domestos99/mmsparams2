package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

public class MyNotifyRespInd extends NotifyRespInd
{
    public MyNotifyRespInd(int mmsVersion, byte[] transactionId, int status) throws InvalidHeaderValueException
    {
        super(mmsVersion, transactionId, status);
    }

    MyNotifyRespInd(PduHeaders headers)
    {
        super(headers);
    }
}
