package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

public class MyReadRecInd extends ReadRecInd
{
    public MyReadRecInd(EncodedStringValue from, byte[] messageId, int mmsVersion, int readStatus, EncodedStringValue[] to) throws InvalidHeaderValueException
    {
        super(from, messageId, mmsVersion, readStatus, to);
    }

    MyReadRecInd(PduHeaders headers)
    {
        super(headers);
    }



    /*
     * Optional, not supported header fields:
     *
     *     public byte[] getApplicId() {return null;}
     *     public void setApplicId(byte[] value) {}
     *
     *     public byte[] getAuxApplicId() {return null;}
     *     public void getAuxApplicId(byte[] value) {}
     *
     *     public byte[] getReplyApplicId() {return 0x00;}
     *     public void setReplyApplicId(byte[] value) {}
     */


}
