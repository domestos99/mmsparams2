package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

public class MyRetrieveConf extends RetrieveConf
{
    public MyRetrieveConf() throws InvalidHeaderValueException
    {
        super();
    }

    public MyRetrieveConf(PduHeaders headers)
    {
        super(headers);
    }

    public MyRetrieveConf(PduHeaders headers, PduBody body)
    {
        super(headers, body);
    }

    public int getDrmContent()
    {
        return mPduHeaders.getOctet(PduHeaders.DRM_CONTENT);
    }

    public void setDrmContent(int value) throws InvalidHeaderValueException
    {
        mPduHeaders.setOctet(value, PduHeaders.DRM_CONTENT);
    }

    public byte[] getApplicId()
    {
        return mPduHeaders.getTextString(PduHeaders.APPLIC_ID);
    }

    public void setApplicId(byte[] value)
    {
        mPduHeaders.setTextString(value, PduHeaders.APPLIC_ID);
    }

    public byte[] getAuxApplicId()
    {
        return mPduHeaders.getTextString(PduHeaders.AUX_APPLIC_ID);
    }

    public void setAuxApplicId(byte[] value)
    {
    }

    public int getContentClass()
    {
        return mPduHeaders.getOctet(PduHeaders.CONTENT_CLASS);
    }

    public void setApplicId(int value) throws InvalidHeaderValueException
    {
        mPduHeaders.setOctet(value, PduHeaders.CONTENT);
    }


    public int getDistributionIndicator()
    {
        return mPduHeaders.getOctet(PduHeaders.DISTRIBUTION_INDICATOR);
    }

    public void setDistributionIndicator(byte value) throws InvalidHeaderValueException
    {
        mPduHeaders.setOctet(value, PduHeaders.DISTRIBUTION_INDICATOR);
    }

//    public PreviouslySentByValue getPreviouslySentBy()
//    {
////        PduHeaders.PREVIOUSLY_SENT_BY
//    }
//
//    public void setPreviouslySentBy(PreviouslySentByValue value)
//    {
////         PduHeaders.PREVIOUSLY_SENT_BY
//    }
//
//    public PreviouslySentDateValue getPreviouslySentDate()
//    {
////        PduHeaders.PREVIOUSLY_SENT_DATE
//    }
//
//    public void setPreviouslySentDate(PreviouslySentDateValue value)
//    {
////        PduHeaders.PREVIOUSLY_SENT_DATE
//    }
//
//    public MmFlagsValue getMmFlags()
//    {
//        //PduHeaders.MM_FLAGS
//    }
//
//    public void setMmFlags(MmFlagsValue value)
//    {
//        //PduHeaders.MM_FLAGS
//    }
//
//    public MmStateValue getMmState()
//    {
//        //PduHeaders.MM_STATE_SENT
//    }
//
//    public void setMmState(MmStateValue value)
//    {
//    }

    public byte[] getReplaceId()
    {
        return mPduHeaders.getTextString(PduHeaders.REPLACE_ID);
    }

    public void setReplaceId(byte[] value)
    {
        mPduHeaders.setTextString(value, PduHeaders.REPLACE_ID);
    }

    public byte[] getReplyApplicId()
    {
        return mPduHeaders.getTextString(PduHeaders.REPLY_APPLIC_ID);
    }

    public void setReplyApplicId(byte[] value)
    {
        mPduHeaders.setTextString(value, PduHeaders.REPLY_APPLIC_ID);
    }

    public int getReplyCharging()
    {
        return mPduHeaders.getOctet(PduHeaders.REPLY_CHARGING);
    }

    public void setReplyCharging(int value) throws InvalidHeaderValueException
    {
        mPduHeaders.setOctet(value, PduHeaders.REPLY_CHARGING);
    }

    public int getReplyChargingDeadline()
    {
        return mPduHeaders.getOctet(PduHeaders.REPLY_CHARGING_DEADLINE);
    }

    public void setReplyChargingDeadline(int value) throws InvalidHeaderValueException
    {
        mPduHeaders.setOctet(value, PduHeaders.REPLY_CHARGING_DEADLINE);
    }

    public byte[] getReplyChargingId()
    {
        return mPduHeaders.getTextString(PduHeaders.REPLY_CHARGING_ID);
    }

    public void setReplyChargingId(byte[] value)
    {
        mPduHeaders.setTextString(value, PduHeaders.REPLY_CHARGING_ID);
    }

    public long getReplyChargingSize()
    {
        return mPduHeaders.getLongInteger(PduHeaders.REPLY_CHARGING_SIZE);
    }

    public void setReplyChargingSize(long value)
    {
        mPduHeaders.setLongInteger(value, PduHeaders.REPLY_CHARGING_SIZE);
    }





    /*
     * Optional, not supported header fields:
     *
     *     public byte[] getApplicId() {return null;}
     *     public void setApplicId(byte[] value) {}
     *
     *     public byte[] getAuxApplicId() {return null;}
     *     public void getAuxApplicId(byte[] value) {}
     *
     *     public byte getContentClass() {return 0x00;}
     *     public void setApplicId(byte value) {}
     *
     *     public byte getDrmContent() {return 0x00;}
     *     public void setDrmContent(byte value) {}
     *
     *     public byte getDistributionIndicator() {return 0x00;}
     *     public void setDistributionIndicator(byte value) {}
     *
     *     public PreviouslySentByValue getPreviouslySentBy() {return null;}
     *     public void setPreviouslySentBy(PreviouslySentByValue value) {}
     *
     *     public PreviouslySentDateValue getPreviouslySentDate() {}
     *     public void setPreviouslySentDate(PreviouslySentDateValue value) {}
     *
     *     public MmFlagsValue getMmFlags() {return null;}
     *     public void setMmFlags(MmFlagsValue value) {}
     *
     *     public MmStateValue getMmState() {return null;}
     *     public void getMmState(MmStateValue value) {}
     *
     *     public byte[] getReplaceId() {return 0x00;}
     *     public void setReplaceId(byte[] value) {}
     *
     *     public byte[] getReplyApplicId() {return 0x00;}
     *     public void setReplyApplicId(byte[] value) {}
     *
     *     public byte getReplyCharging() {return 0x00;}
     *     public void setReplyCharging(byte value) {}
     *
     *     public byte getReplyChargingDeadline() {return 0x00;}
     *     public void setReplyChargingDeadline(byte value) {}
     *
     *     public byte[] getReplyChargingId() {return 0x00;}
     *     public void setReplyChargingId(byte[] value) {}
     *
     *     public long getReplyChargingSize() {return 0;}
     *     public void setReplyChargingSize(long value) {}
     */
}
