package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

public class MySendReq extends SendReq
{
    public MySendReq()
    {
        super();
    }

    public MySendReq(byte[] contentType, EncodedStringValue from, int mmsVersion, byte[] transactionId) throws InvalidHeaderValueException
    {
        super(contentType, from, mmsVersion, transactionId);
    }

    public MySendReq(PduHeaders headers)
    {
        super(headers);
    }

    public MySendReq(PduHeaders headers, PduBody body)
    {
        super(headers, body);
    }


    public void setDeliveryTime(long value)
    {
        mPduHeaders.setLongInteger(value, PduHeaders.DELIVERY_TIME);
    }

    public long getDeliveryTime()
    {
        return mPduHeaders.getLongInteger(PduHeaders.DELIVERY_TIME);
    }

    public void setSenderIsVisible(boolean visible) throws InvalidHeaderValueException
    {
        // zrejme je to krizem - proverit
        if (visible)
        {
            mPduHeaders.setOctet(PduHeaders.SENDER_VISIBILITY_SHOW, PduHeaders.SENDER_VISIBILITY);
        }
        else
        {
            mPduHeaders.setOctet(PduHeaders.SENDER_VISIBILITY_HIDE, PduHeaders.SENDER_VISIBILITY);
        }
    }

    public int getSenderIsVisible()
    {
        int value = mPduHeaders.getOctet(PduHeaders.SENDER_VISIBILITY);

        if (value == PduHeaders.SENDER_VISIBILITY_SHOW)
            return 1;
        else if (value == PduHeaders.SENDER_VISIBILITY_HIDE)
            return 0;
        else
            return -1;
    }

    public int getSenderIsVisible2()
    {
        return mPduHeaders.getOctet(PduHeaders.SENDER_VISIBILITY);
    }


    public int getAdaptationAllowed()
    {
        return mPduHeaders.getOctet(PduHeaders.ADAPTATION_ALLOWED);
    }

    public void setAdaptationAllowed(int value) throws InvalidHeaderValueException
    {
        mPduHeaders.setOctet(value, PduHeaders.ADAPTATION_ALLOWED);
    }


    public int getDrmContent()
    {
        return mPduHeaders.getOctet(PduHeaders.DRM_CONTENT);
    }

    public void setDrmContent(int value) throws InvalidHeaderValueException
    {
        mPduHeaders.setOctet(value, PduHeaders.DRM_CONTENT);
    }



    /*
     * Optional, not supported header fields:
     *
     *     public byte getAdaptationAllowed() {return 0};
     *     public void setAdaptationAllowed(btye value) {};
     *
     *     public byte[] getApplicId() {return null;}
     *     public void setApplicId(byte[] value) {}
     *
     *     public byte[] getAuxApplicId() {return null;}
     *     public void getAuxApplicId(byte[] value) {}
     *
     *     public byte getContentClass() {return 0x00;}
     *     public void setApplicId(byte value) {}
     *
     *     public long getDeliveryTime() {return 0};
     *     public void setDeliveryTime(long value) {};
     *
     *     public byte getDrmContent() {return 0x00;}
     *     public void setDrmContent(byte value) {}
     *
     *     public MmFlagsValue getMmFlags() {return null;}
     *     public void setMmFlags(MmFlagsValue value) {}
     *
     *     public MmStateValue getMmState() {return null;}
     *     public void getMmState(MmStateValue value) {}
     *
     *     public byte[] getReplyApplicId() {return 0x00;}
     *     public void setReplyApplicId(byte[] value) {}
     *
     *     public byte getReplyCharging() {return 0x00;}
     *     public void setReplyCharging(byte value) {}
     *
     *     public byte getReplyChargingDeadline() {return 0x00;}
     *     public void setReplyChargingDeadline(byte value) {}
     *
     *     public byte[] getReplyChargingId() {return 0x00;}
     *     public void setReplyChargingId(byte[] value) {}
     *
     *     public long getReplyChargingSize() {return 0;}
     *     public void setReplyChargingSize(long value) {}
     *
     *     public byte[] getReplyApplicId() {return 0x00;}
     *     public void setReplyApplicId(byte[] value) {}
     *
     *     public byte getStore() {return 0x00;}
     *     public void setStore(byte value) {}
     */


}
