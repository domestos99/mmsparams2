package cz.uhk.dip.mmsparams.android.adapters;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.logging.Log;
import org.thoughtcrime.securesms.util.ViewUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import cz.uhk.dip.mmsparams.android.adapters.base.MyAdapterBase;
import cz.uhk.dip.mmsparams.android.db.model.ErrorBLO;
import cz.uhk.dip.mmsparams.android.db.model.MmsError;
import cz.uhk.dip.mmsparams.api.json.JsonUtils;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;

public class MmsErrorListAdapter extends MyAdapterBase<MmsError>
{
    private static final int row_layout_ID = R.layout.mms_error_browse_item;

    private static final String TAG = MmsErrorListAdapter.class.getSimpleName();

    public MmsErrorListAdapter(Context context, List<MmsError> objects)
    {
        super(context, row_layout_ID, objects);
    }

    @Override
    protected int getResource()
    {
        return row_layout_ID;
    }

    @Override
    protected void setupView(View view, MmsError obj, int position)
    {
        TextView tvTag = ViewUtil.findById(view, R.id.tvTag);
        TextView tvInfo = ViewUtil.findById(view, R.id.tvInfo);
        TextView tvMessage = ViewUtil.findById(view, R.id.tvMessage);

        TextView tvDateTime = ViewUtil.findById(view, R.id.tvDateTime);
        // tvTag.setText(obj.get);

        long ms = obj.getDateCreated();
        if (ms < 0)
            tvDateTime.setText(String.valueOf(ms));

        Date dt = new Date(ms);
        String dateTimeS = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").format(dt);
        tvDateTime.setText(dateTimeS);


        String json = obj.getError();
        if (StringUtil.isEmptyOrNull(json))
            return;

        try
        {
            ErrorBLO blo = JsonUtils.fromJson(json, ErrorBLO.class);

            tvTag.setText(blo.tag);
            tvInfo.setText(blo.info);
            tvMessage.setText(blo.message);
        }
        catch (Exception e)
        {
            Log.e(TAG, "setupView", e);
        }

    }
}

