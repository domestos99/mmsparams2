package cz.uhk.dip.mmsparams.android.apiinterface;

import android.content.Context;
import android.os.AsyncTask;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.android.http.HttpGetTask;
import cz.uhk.dip.mmsparams.android.http.HttpPostTask;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;
import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.http.TaskResult;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;


public abstract class ApiClientBase
{

    protected final Context context;
    public final static String TAG = ApiClientBase.class.getSimpleName();

    public ApiClientBase(Context context)
    {
        this.context = context;
    }

    protected String getApiServerAddress()
    {
        return MmsParamsPreferences.getApiServerAddress(context);
    }

    protected String getBaseAddress()
    {
        return MmsParamsPreferences.getHttpPrefix(context) + getApiServerAddress() + "/api/" + getControllerName();
    }

    protected abstract String getControllerName();


    public <T extends Serializable> T getData(String address, Class<T> clazz) throws Exception
    {
        String url = MmsParamsPreferences.getHttpPrefix(context) + address + "/api/" + getControllerName();
        AsyncTask<String, Void, TaskResult<String>> task = HttpGetTask.get().execute(url);
        TaskResult<String> response = task.get();

        if (response.hasError())
            throw response.getError();

        return JsonUtilsSafe.fromJson(response.getResult(), clazz);
    }

    public <T extends Serializable> T postData(String address, String body, Class<T> clazz) throws Exception
    {
        String url = address + "/api/" + getControllerName();
        AsyncTask<String, Void, TaskResult<String>> task = HttpPostTask.post().execute(url, body);
        TaskResult<String> response = task.get();

        if (response.hasError())
            throw response.getError();

        return JsonUtilsSafe.fromJson(response.getResult(), clazz);
    }

    protected String getGetUrl()
    {
        return getBaseAddress();
    }

}
