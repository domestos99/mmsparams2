package cz.uhk.dip.mmsparams.android.apiinterface;

import android.content.Context;

import org.thoughtcrime.securesms.logging.Log;

import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.http.auth.JwtResponse;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;

public class AuthClient extends ApiClientBase
{
    public AuthClient(Context context)
    {
        super(context);
    }

    @Override
    protected String getControllerName()
    {
        return "authenticate";
    }

    public JwtResponse auth(WSConnectSettings wsConnectSettings) throws Exception
    {
        JwtRequest req = new JwtRequest(wsConnectSettings.getUsername(), wsConnectSettings.getPassword());
        String json = JsonUtilsSafe.toJson(req);

        try
        {
            return super.postData(wsConnectSettings.getServerAddressProvider().getServerAddress(), json, JwtResponse.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG, "auth", e);
            throw e;
        }
    }
}
