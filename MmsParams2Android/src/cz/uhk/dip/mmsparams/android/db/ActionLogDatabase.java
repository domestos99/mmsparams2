package cz.uhk.dip.mmsparams.android.db;

import android.content.Context;
import android.database.Cursor;

import net.sqlcipher.database.SQLiteDatabase;

import org.thoughtcrime.securesms.database.helpers.SQLCipherOpenHelper;
import org.thoughtcrime.securesms.logging.Log;

import java.util.Calendar;
import java.util.Date;

import cz.uhk.dip.mmsparams.android.db.model.ActionLogModel;

public class ActionLogDatabase extends DatabaseBase<ActionLogModel>
{

    private static final String TAG = ActionLogDatabase.class.getSimpleName();

    public static final String TABLE_NAME = "actionlog";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME +
                    " (" + ActionLogModel.MetaData.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +

                    ActionLogModel.MetaData.TAG + " TEXT, " +
                    ActionLogModel.MetaData.MESSAGE + " TEXT, " +
                    ActionLogModel.MetaData.BODY + " TEXT, " +

                    ActionLogModel.MetaData.DATE_CREATED + " BIGINT " +

                    ");";


    public ActionLogDatabase(Context context, SQLCipherOpenHelper databaseHelper)
    {
        super(context, databaseHelper);
    }

    @Override
    public String getTableName()
    {
        return TABLE_NAME;
    }

    @Override
    public String getIDColumnName()
    {
        return ActionLogModel.MetaData.ID;
    }

    @Override
    public ActionLogModel getFromCursor(Cursor cursor)
    {
        return ActionLogModel.getFromCursor(cursor);
    }

    public ActionLogModel getById(int id)
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = null;

        try
        {
            cursor = database.query(TABLE_NAME, null, ActionLogModel.MetaData.ID + " = ?", new String[]{id + ""}, null, null, null);

            while (cursor != null && cursor.moveToFirst())
            {
                return ActionLogModel.getFromCursor(cursor);
            }
        }
        finally
        {
            if (cursor != null)
                cursor.close();
        }
        Log.e(TAG, "ActionLogModel ID not found: " + id);


        return null;
    }


    public void removeOldRecord()
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        Date dt = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.add(Calendar.DATE, -1);
        long milis = cal.getTimeInMillis();

        database.delete(getTableName(), ActionLogModel.MetaData.DATE_CREATED + " < " + String.valueOf(milis), null);
    }

    public void deleteAll()
    {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        database.delete(getTableName(), null, null);
    }
}

