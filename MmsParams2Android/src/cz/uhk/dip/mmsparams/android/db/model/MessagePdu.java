package cz.uhk.dip.mmsparams.android.db.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.android.mms.pdu_alt.DeliveryInd;
import com.google.android.mms.pdu_alt.GenericPdu;
import com.google.android.mms.pdu_alt.MyRetrieveConf;
import com.google.android.mms.pdu_alt.MySendConf;
import com.google.android.mms.pdu_alt.PduHeaders;
import com.google.android.mms.pdu_alt.ReadOrigInd;
import com.google.android.mms.pdu_alt.ReadRecInd;
import com.google.android.mms.pdu_alt.RetrieveConf;
import com.google.android.mms.pdu_alt.SendConf;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;

import cz.uhk.dip.mmsparams.android.pdus.PduByteHelper;

public class MessagePdu implements Serializable, IDBModel<MessagePdu>
{
    public int id;
    public long messageID;
    public int type;
    public String msgPduID;
    public byte[] data;
    public long dateCreated;

    @Override
    public ContentValues getContentValues(MessagePdu messagePdu)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MessagePdu.MetaData.MESSAGE_ID, messagePdu.getMessageID());
        contentValues.put(MetaData.MSG_PDU_ID, messagePdu.getMsgPduID());
        contentValues.put(MessagePdu.MetaData.TYPE, messagePdu.getType());
        contentValues.put(MessagePdu.MetaData.DATE_CREATED, messagePdu.getDateCreated());

        byte[] data = messagePdu.getData();
        if (data == null)
            data = new byte[0];

        contentValues.put(MessagePdu.MetaData.DATA, data);

        return contentValues;
    }

    public String getInfoString()
    {
        return toString();
    }

    @Override
    public String toString()
    {
        return "MessagePdu{" +
                "id=" + id +
                ", messageID=" + messageID +
                ", type=" + type +
                ", msgPduID='" + msgPduID + '\'' +
                ", data=" + Arrays.toString(data) +
                ", dateCreated=" + dateCreated +
                '}';
    }

    @Override
    public int getID()
    {
        return getId();
    }


    @Override
    public void setIDNegative()
    {
        this.id = -1;
    }

    @Override
    public void setCreatedDT(long dt)
    {
        this.dateCreated = dt;
    }

    @Override
    public void setUpdatedDT(long dt)
    {

    }

    public static class MetaData
    {
        public static String ID = "_id";
        public static String MESSAGE_ID = "msg_id";
        public static String TYPE = "type";
        public static String MSG_PDU_ID = "msg_pdu_id";
        public static String DATA = "data";
        public static String DATE_CREATED = "date_created";
    }


    public static MessagePdu createFromCursor(Cursor cursor)
    {
        MessagePdu pdu = new MessagePdu();

        pdu.id = cursor.getInt(cursor.getColumnIndexOrThrow(MessagePdu.MetaData.ID));
        pdu.messageID = cursor.getInt(cursor.getColumnIndexOrThrow(MessagePdu.MetaData.MESSAGE_ID));
        pdu.type = cursor.getInt(cursor.getColumnIndexOrThrow(MessagePdu.MetaData.TYPE));

        if (cursor.getColumnIndex(MetaData.MSG_PDU_ID) != -1)
            pdu.msgPduID = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.MSG_PDU_ID));

        pdu.data = cursor.getBlob(cursor.getColumnIndexOrThrow(MessagePdu.MetaData.DATA));

        pdu.dateCreated = cursor.getLong(cursor.getColumnIndexOrThrow(MetaData.DATE_CREATED));

        return pdu;
    }

    public MessagePdu()
    {
    }

    public MessagePdu(int id, long messageID, int type, byte[] data)
    {
        this.id = id;
        this.messageID = messageID;
        this.msgPduID = getPduMessageID(data);
        this.type = type;
        this.data = data;
    }

    private String getString(byte[] bytes)
    {
        if (bytes == null || bytes.length == 0)
            return null;

        return new String(bytes);
    }

    private String getPduMessageID(byte[] data)
    {
        if (data == null)
            return null;

        GenericPdu pdu = PduByteHelper.getPdu(data); // new MyPduParser(data).parse();

        if (pdu == null)
            return null;

        int type = pdu.getMessageType();
        switch (type)
        {
            case PduHeaders.MESSAGE_TYPE_SEND_CONF:
                return getString(((SendConf) pdu).getMessageId());

            case PduHeaders.MESSAGE_TYPE_RETRIEVE_CONF:
                return getString(((RetrieveConf) pdu).getMessageId());

            case PduHeaders.MESSAGE_TYPE_READ_ORIG_IND:
                return getString(((ReadOrigInd) pdu).getMessageId());

            case PduHeaders.MESSAGE_TYPE_READ_REC_IND:
                return getString(((ReadRecInd) pdu).getMessageId());

            case PduHeaders.MESSAGE_TYPE_DELIVERY_IND:
                return getString(((DeliveryInd) pdu).getMessageId());
        }
        return null;
    }

//    public MessagePdu(int id, long messageID, String msgPduID, int type, byte[] data)
//    {
//        this.id = id;
//        this.messageID = messageID;
//        this.msgPduID = msgPduID;
//        this.type = type;
//        this.data = data;
//    }

    public int getId()
    {
        return id;
    }

    public String getMsgPduID()
    {
        return msgPduID;
    }

    public int getType()
    {
        return type;
    }

    public byte[] getData()
    {
        return data;
    }

    public long getMessageID()
    {
        return messageID;
    }

    public long getDateCreated()
    {
        return dateCreated;
    }

    public static class MessagePduCreatedComparator implements Comparator<MessagePdu>
    {
        @Override
        public int compare(MessagePdu o1, MessagePdu o2)
        {
            if (o1.getDateCreated() < o2.getDateCreated())
                return -1;
            if (o1.getDateCreated() > o2.getDateCreated())
                return 1;
            return 0;
        }
    }

    public GenericPdu getGenericPdu()
    {
        if (this.getData() == null)
            return null;

        return PduByteHelper.getPdu(this.getData()); // new MyPduParser(this.getData()).parse();
    }


}

