package cz.uhk.dip.mmsparams.android.devicekey;

import android.content.Context;

public class DeviceIdSingleton
{
    private static DeviceIdSingleton instance = null;
    private String deviKey = null;

    protected DeviceIdSingleton()
    {
        // Exists only to defeat instantiation.
    }

    public void init(Context context)
    {
        deviKey = new DeviceUuidFactory(context).getDeviceUuid().toString();
    }

    public static DeviceIdSingleton getInstance()
    {
        if (instance == null)
        {
            instance = new DeviceIdSingleton();
        }
        return instance;
    }

    public String getDeviKey()
    {
        return deviKey;
    }

}

