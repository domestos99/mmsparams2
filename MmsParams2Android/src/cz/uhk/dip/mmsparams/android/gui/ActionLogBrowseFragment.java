package cz.uhk.dip.mmsparams.android.gui;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.util.ViewUtil;

import java.util.List;

import cz.uhk.dip.mmsparams.android.adapters.ActionLogListAdaper;
import cz.uhk.dip.mmsparams.android.db.ActionLogDatabase;
import cz.uhk.dip.mmsparams.android.db.model.ActionLogModel;
import cz.uhk.dip.mmsparams.android.gui.base.BrowseBaseFragment;

public class ActionLogBrowseFragment extends BrowseBaseFragment<ActionLogModel>
{
    ListView listView;
    ActionLogListAdaper adapter;

    private static final String TAG = ActionLogBrowseFragment.class.getSimpleName();


    @Override
    protected ArrayAdapter getAdapter()
    {
        return adapter;
    }

    @Override
    protected ListView getListView()
    {
        return listView;
    }


    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.action_log_browse_fragment;
    }


    @Override
    protected List<ActionLogModel> loadData()
    {
        ActionLogDatabase db = DatabaseFactory.getActionLogDatabase(getContext());
        db.removeOldRecord();
        return db.getAll();
    }

    @Override
    protected void initControls(View view)
    {
        listView = ViewUtil.findById(view, R.id.list);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_delete, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);
        switch (item.getItemId())
        {
            case R.id.menu_delete:
                deleteAll();
                return true;
        }
        return false;
    }

    private void deleteAll()
    {
        ActionLogDatabase db = DatabaseFactory.getActionLogDatabase(getContext());
        db.deleteAll();
        refreshData();
    }

    @Override
    protected void setupListeners()
    {

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ActionLogModel obj = (ActionLogModel) parent.getItemAtPosition(position);

                if (allowOpenDetail())
                {
                    // This Activity was called by startActivity
                    openDetail(ActionLogDetailFragment.class, obj.getID());
                }
                else
                {
                    //This Activity was called by startActivityForResult
                    returnResult(obj);
                }

            }
        });

    }

    @Override
    protected void afterBaseInit()
    {
        adapter = new ActionLogListAdaper(getApplicationContext(), data);
        listView.setAdapter(adapter);

        super.afterBaseInit();
    }


    @Override
    public void onResume()
    {
        getActivity().setTitle("Action Log");
        super.onResume();
    }
}
