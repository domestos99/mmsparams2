package cz.uhk.dip.mmsparams.android.gui;

import android.view.View;
import android.widget.TextView;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.util.ViewUtil;

import androidx.fragment.app.FragmentManager;

import cz.uhk.dip.mmsparams.android.db.model.ActionLogModel;
import cz.uhk.dip.mmsparams.android.gui.base.BaseFragment;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;

public class ActionLogDetailDetailControls extends DetailControlsBase<ActionLogModel>
{
    TextView tvTag, tvMessage, tvBody;


    protected ActionLogDetailDetailControls(BaseFragment baseFragment, FragmentManager fragmentManager, View view)
    {
        super(baseFragment, fragmentManager, view);
    }

    @Override
    protected ActionLogModel updateModelFromControls(ActionLogModel obj)
    {
        return null;
    }

    @Override
    protected void fillControls(ActionLogModel obj)
    {
        tvTag.setText(obj.getTag());
        tvMessage.setText(obj.getMessage());
        tvBody.setText(JsonUtilsSafe.getPretty(obj.getBody()));

    }

    @Override
    protected void initControls(View view)
    {
        tvTag = ViewUtil.findById(view, R.id.tvTag);
        tvMessage = ViewUtil.findById(view, R.id.tvMessage);
        tvBody = ViewUtil.findById(view, R.id.tvBody);

    }

    @Override
    protected void setupListeners()
    {

    }

    public void afterLoadData(int id)
    {
        // setID(id);
    }
}
