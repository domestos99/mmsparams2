package cz.uhk.dip.mmsparams.android.gui;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.util.ViewUtil;

import java.util.TimerTask;

import cz.uhk.dip.mmsparams.android.gui.base.BaseFragment;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;
import cz.uhk.dip.mmsparams.android.updater.ApkUpdateAsyncTask;
import cz.uhk.dip.mmsparams.android.websocket.WebSocketSingleton;
import cz.uhk.dip.mmsparams.android.websocket.WebSocketUtils;
import cz.uhk.dip.mmsparams.api.constants.AppVersion;
import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;

public class ApiInterfaceFragment extends BaseFragment
{
    private static final String TAG = ApiInterfaceFragment.class.getName();

    private Button btnShowSettings, btnStopSocket, btnStartSocket, btnUpdateApk;
    private TextView wsAddress, tvWsStatus, tvApiTestInfoLine, tvAppVersion;
    private CheckBox chbSendKeepAliveMessage;

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.api_interface_fragment;
    }

    @Override
    protected void afterBaseInit()
    {
        super.afterBaseInit();

        tvAppVersion.setText("App Version: " + AppVersion.SYSTEM_VERSION);

        if (!ApiInterfaceSettingsFragment.areSettingsOk(getContext()))
        {
            btnShowSettings.setBackgroundColor(getResources().getColor(R.color.red_400));
            btnStartSocket.setEnabled(false);
            btnStopSocket.setEnabled(false);
            //handleOpenSettings();
        }
        else if (!ApiInterfaceSettingsFragment.checkSystemVersionBuild(getContext()))
        {
            btnShowSettings.setBackgroundColor(getResources().getColor(R.color.red_400));
            btnStartSocket.setEnabled(false);
            btnStopSocket.setEnabled(false);
        }
        else
        {
            btnShowSettings.setBackgroundColor(getResources().getColor(R.color.green_500));
            btnStartSocket.setEnabled(true);
            btnStopSocket.setEnabled(true);
        }

        wsAddress.setText(WebSocketUtils.getWebSocketAddress(getApplicationContext()));
        updateWsStatusTest();
    }

    private MyTimerTask myTimerTask;

    @Override
    protected void initControls(View view)
    {
        btnShowSettings = ViewUtil.findById(view, R.id.btnShowSettings);
        btnStopSocket = ViewUtil.findById(view, R.id.btnStopSocket);
        btnStartSocket = ViewUtil.findById(view, R.id.btnStartSocket);
        btnUpdateApk = ViewUtil.findById(view, R.id.btnUpdateApk);
        wsAddress = ViewUtil.findById(view, R.id.wsAddress);
        chbSendKeepAliveMessage = ViewUtil.findById(view, R.id.chbSendKeepAliveMessage);
        tvWsStatus = ViewUtil.findById(view, R.id.tvWsStatus);
        tvApiTestInfoLine = ViewUtil.findById(view, R.id.tvApiTestInfoLine2);
        tvAppVersion = ViewUtil.findById(view, R.id.tvAppVersion);

        chbSendKeepAliveMessage.setChecked(MmsParamsPreferences.getSendKeepAliveMessage(getContext()));
        createTimer();
    }

    @Override
    public void onDestroy()
    {
        handler.removeCallbacks(runnableCode);
        super.onDestroy();
    }

    Handler handler = new Handler();
    // Define the code block to be executed
    Runnable runnableCode = new Runnable()
    {
        @Override
        public void run()
        {
            // Do something here on the main thread
            Log.d("Handlers", "Called on main thread");
            updateWsStatusTest();

            // Repeat this the same runnable code block again another 2 seconds
            handler.postDelayed(runnableCode, 2000);
        }
    };

    private void createTimer()
    {
        // Start the initial runnable task by posting through the handler
        handler.post(runnableCode);
    }

    @Override
    protected void setupListeners()
    {
        btnShowSettings.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                handleOpenSettings();
            }
        });

        btnStartSocket.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                handleOpenSocket();
            }
        });

        btnStopSocket.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                handleCloseSocket();
            }
        });

        chbSendKeepAliveMessage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                handleChBSendKeepAliveMessageChange();
            }
        });

        btnUpdateApk.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                handleApkUpdate();
            }
        });
    }

    private void handleApkUpdate()
    {

        String url = "https://mmsparams2.firebaseapp.com/assets/data/MmsParams-1.0.2.apk";

        new ApkUpdateAsyncTask().execute(url);

    }

    private void handleChBSendKeepAliveMessageChange()
    {
        MmsParamsPreferences.setSendKeepAliveMessage(getContext(), chbSendKeepAliveMessage.isChecked());
    }

    private void updateWsStatusTest()
    {
        updateWsStatusText();

        if (WebSocketSingleton.getInstance().isSocketOpen())
        {
            btnShowSettings.setEnabled(false);
            btnStartSocket.setEnabled(false);
            btnStopSocket.setEnabled(true);
        }
        else
        {
            btnShowSettings.setEnabled(true);
            btnStartSocket.setEnabled(true);
            btnStopSocket.setEnabled(false);
        }

        tvApiTestInfoLine.setText(WebSocketSingleton.getInstance().getLastError());
    }

    private void handleOpenSocket()
    {
        try
        {
            chbSendKeepAliveMessage.setEnabled(false);
            WebSocketUtils.connect(getApplicationContext());
            tvApiTestInfoLine.setText("");
            createTimer();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG, "handleOpenSocket", e);
            tvApiTestInfoLine.setText(e.getMessage());
            chbSendKeepAliveMessage.setEnabled(true);
        }
    }


    private void handleCloseSocket()
    {
        chbSendKeepAliveMessage.setEnabled(true);
        WebSocketSingleton.getInstance().disconnect();
        updateWsStatusTest();
    }

    private void handleOpenSettings()
    {
        openFragment(new ApiInterfaceSettingsFragment());
    }


    public void updateWsStatusText()
    {
        if (WebSocketSingleton.getInstance().isSocketOpen())
        {
            tvWsStatus.setText("CONNECTED");
            try
            {
                tvWsStatus.setTextColor(getResources().getColor(R.color.green_500));
            }
            catch (Exception e)
            {
                Log.w(TAG, ExceptionHelper.getStackTrace(e));
            }
        }
        else
        {
            tvWsStatus.setText("NOT CONNECTED");
            try
            {
                tvWsStatus.setTextColor(getResources().getColor(R.color.red_400));
            }
            catch (Exception e)
            {
                Log.w(TAG, ExceptionHelper.getStackTrace(e));
            }
        }
    }

    class MyTimerTask extends TimerTask
    {

        @Override
        public void run()
        {
            getActivity().runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    updateWsStatusTest();
                }
            });
        }
    }
}

