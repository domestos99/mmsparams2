package cz.uhk.dip.mmsparams.android.gui;

import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import cz.uhk.dip.mmsparams.android.R;

import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;

import androidx.annotation.NonNull;
import cz.uhk.dip.mmsparams.android.apiinterface.AuthClient;
import cz.uhk.dip.mmsparams.android.apiinterface.PingClient;
import cz.uhk.dip.mmsparams.android.apiinterface.SystemVersionClient;
import cz.uhk.dip.mmsparams.android.gui.base.BaseFragment;
import cz.uhk.dip.mmsparams.android.gui.dialog.DialogHelper;
import cz.uhk.dip.mmsparams.android.permissions.PermissionUtil;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;
import cz.uhk.dip.mmsparams.android.utils.AppUtils;
import cz.uhk.dip.mmsparams.api.constants.AppVersion;
import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;
import cz.uhk.dip.mmsparams.api.http.ServerAddressProvider;
import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.http.auth.JwtResponse;
import cz.uhk.dip.mmsparams.api.websocket.model.SystemVersionModel;

public class ApiInterfaceSettingsFragment extends BaseFragment
{
    private static final String TAG = ApiInterfaceSettingsFragment.class.getName();


    private TextView lbIsDefaultSmsMmsApp, tvApiTestInfoLine;
    private Button btnSetDefaultSmsMmsApp, btnTestApiServer, btnApiAuthenticate;
    private EditText tbApiServerAddress, tbApiUsername, tbApiPassword, tbPhoneCustomName, tbPhoneNumber;
    private PermissionViewControls permissionViewControls;
    private Switch switchUseHttps;


    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.api_interface_settings_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        lbIsDefaultSmsMmsApp = ViewUtil.findById(view, R.id.lbIsDefaultSmsMmsApp);
        tvApiTestInfoLine = ViewUtil.findById(view, R.id.tvApiTestInfoLine);
        btnSetDefaultSmsMmsApp = ViewUtil.findById(view, R.id.btnSetDefaultSmsMmsApp);
        btnTestApiServer = ViewUtil.findById(view, R.id.btnTestApiServer);
        btnApiAuthenticate = ViewUtil.findById(view, R.id.btnApiAuthenticate);
        tbApiServerAddress = ViewUtil.findById(view, R.id.tbApiServerAddress);
        tbApiUsername = ViewUtil.findById(view, R.id.tbApiUsername);
        tbApiPassword = ViewUtil.findById(view, R.id.tbApiPassword);

        tbPhoneCustomName = ViewUtil.findById(view, R.id.tbPhoneCustomName);
        tbPhoneNumber = ViewUtil.findById(view, R.id.tbPhoneNumber);
        switchUseHttps = ViewUtil.findById(view, R.id.switchUseHttps);

        permissionViewControls = new PermissionViewControls(this, getFragmentManager(), view);


        switchUseHttps.setChecked(MmsParamsPreferences.getUseHttps(getContext()));
        updateControls();

        setHasOptionsMenu(true);
    }

    @Override
    protected void setupListeners()
    {
        btnSetDefaultSmsMmsApp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                handleSetDefaultSmsMmsApp();
            }
        });

        btnTestApiServer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                pingApiServer();
            }
        });

        btnApiAuthenticate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                authenticateApi();
            }
        });

        switchUseHttps.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                MmsParamsPreferences.setUseHttps(getContext(), b);
            }
        });
    }

    private boolean authenticateApi()
    {
        tvApiTestInfoLine.setText("Authenticating...");
        JwtResponse response = authenticate();
        if (response == null)
        {
            Toast.makeText(getContext(), "Unable to authenticate", Toast.LENGTH_SHORT).show();
            return false;
        }
        else
        {
            tvApiTestInfoLine.setText("Auth - OK");
            MmsParamsPreferences.setJwtResponse(getContext(), response);
            Toast.makeText(getContext(), "Auth - OK", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    private void pingApiServer()
    {
        tvApiTestInfoLine.setText("Pinging...");
        boolean ok = ping();
        if (ok)
        {
            tvApiTestInfoLine.setText("Ping - OK");
            Toast.makeText(getContext(), "Ping - OK", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(getContext(), "Ping - Error", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean ping()
    {
        try
        {
            return new PingClient(getContext()).pingServer(tbApiServerAddress.getText().toString());
        } catch (Exception e)
        {
            e.printStackTrace();
            this.tvApiTestInfoLine.setText(e.getMessage());
            return false;
        }
    }

    private JwtResponse authenticate()
    {
        HttpProtocolEnum prot = switchUseHttps.isChecked() ? HttpProtocolEnum.HTTPS : HttpProtocolEnum.HTTP;
        ServerAddressProvider serverAddressProvider = new ServerAddressProvider(prot, tbApiServerAddress.getText().toString());
        WSConnectSettings wsConnectSettings = new WSConnectSettings(serverAddressProvider, tbApiUsername.getText().toString(), tbApiPassword.getText().toString());
        try
        {
            return new AuthClient(getContext()).auth(wsConnectSettings);
        } catch (Exception e)
        {
            e.printStackTrace();
            this.tvApiTestInfoLine.setText(e.getMessage());
            return null;
        }
    }

    private static boolean pingFromPref(Context context)
    {
        try
        {
            return new PingClient(context).pingServer(MmsParamsPreferences.getApiServerAddress(context));
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean authFromPref(Context context)
    {
        try
        {
            WSConnectSettings wsConnectSettings = MmsParamsPreferences.getWSConnectSettings(context);
            JwtResponse resp = new AuthClient(context).auth(wsConnectSettings);
            return resp != null;
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    private void updateControls()
    {
        final boolean isDefault = Util.isDefaultSmsProvider(getApplicationContext());
        lbIsDefaultSmsMmsApp.setText(String.valueOf(isDefault));

        // Disable set button if is active
        btnSetDefaultSmsMmsApp.setEnabled(!isDefault);
        tbApiServerAddress.setText(MmsParamsPreferences.getApiServerAddress(getContext()));
        tbApiUsername.setText(MmsParamsPreferences.getApiUsername(getContext()));
        tbApiPassword.setText(MmsParamsPreferences.getApiPassword(getContext()));

        permissionViewControls.updateControls();


        tbPhoneCustomName.setText(MmsParamsPreferences.getPhoneCustomName(getContext()));
        tbPhoneNumber.setText(MmsParamsPreferences.getPhoneNumber(getContext()));
    }

    private void handleSetDefaultSmsMmsApp()
    {
        AppUtils.makeDefaultSmsApp(getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        updateControls();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        updateControls();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_save, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);
        switch (item.getItemId())
        {
            case R.id.menu_save:
                checkSaveReturn(true);
                return true;
        }
        return false;
    }

    private boolean checkSaveReturn(boolean close)
    {
        if (!this.areSettingsOk2(getContext()))
        {
            // Show error
            Toast.makeText(getContext(), "Wrong configuration!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!this.checkSystemVersionBuild2(getContext()))
        {
            return false;
        }

        save();

        if (close)
        {
            super.closeFragment();
            return true;
        }
        return true;
    }

    private void save()
    {
        MmsParamsPreferences.setApiServerAddress(getContext(), tbApiServerAddress.getText().toString());
        MmsParamsPreferences.setApiUsername(getContext(), tbApiUsername.getText().toString());
        MmsParamsPreferences.setApiPassword(getContext(), tbApiPassword.getText().toString());


        MmsParamsPreferences.setPhoneCustomName(getContext(), tbPhoneCustomName.getText().toString());
        MmsParamsPreferences.setPhoneNumber(getContext(), tbPhoneNumber.getText().toString());
    }

    public boolean areSettingsOk2(Context context)
    {
        boolean result = true;

        final boolean isDefault = Util.isDefaultSmsProvider(context);
        result = result && isDefault;
        result = result && ping();
        result = result && authenticateApi();
        result = result && hasAllPermissions(context);

        return result;
    }

    public static boolean areSettingsOk(Context context)
    {
        boolean result = true;

        final boolean isDefault = Util.isDefaultSmsProvider(context);
        result = result && isDefault;

        result = result && hasAllPermissions(context);

        result = result && pingFromPref(context);
        result = result && authFromPref(context);

        return result;
    }

    public boolean checkSystemVersionBuild2(Context context)
    {
        SystemVersionModel systemVersionModel = new SystemVersionClient(context).getSystemVersion(tbApiServerAddress.getText().toString());

        if (!AppVersion.SYSTEM_BUILD.equals(systemVersionModel.getSystemBuild()))
        {
            Toast.makeText(context, "Server and Android versions are different! Please update app.", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public static boolean checkSystemVersionBuild(Context context)
    {
        SystemVersionModel systemVersionModel = new SystemVersionClient(context).getSystemVersion(MmsParamsPreferences.getApiServerAddress(context));

        if (!AppVersion.SYSTEM_BUILD.equals(systemVersionModel.getSystemBuild()))
        {
            Toast.makeText(context, "Server and Android versions are different! Please update app.", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private static boolean hasAllPermissions(Context activity)
    {
        return PermissionUtil.hasAllPerm(activity);
    }

    @Override
    public boolean handleBeforeClose()
    {
        DialogHelper.createAndShowSaveAlertDialog(getActivity(), new DialogHelper.OnOkListener()
                {
                    @Override
                    public void OnOk()
                    {
                        checkSaveReturn(true);
                    }
                },
                new DialogHelper.OnCancelListener()
                {
                    @Override
                    public void OnCancel()
                    {
                        closeFragment();
                    }
                });

        return false;
    }


}
