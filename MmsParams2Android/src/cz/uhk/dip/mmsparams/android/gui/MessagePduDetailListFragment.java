package cz.uhk.dip.mmsparams.android.gui;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.mms.pdu_alt.PduHeaders;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.util.ViewUtil;

import java.util.ArrayList;
import java.util.Collections;

import androidx.fragment.app.Fragment;
import cz.uhk.dip.mmsparams.android.db.MessagePduDatabase;
import cz.uhk.dip.mmsparams.android.db.model.MessagePdu;
import cz.uhk.dip.mmsparams.android.gui.base.BaseFragment;
import cz.uhk.dip.mmsparams.android.gui.base.OnFragmentInteractionListener;
import cz.uhk.dip.mmsparams.android.gui.pdu.*;

public class MessagePduDetailListFragment extends BaseFragment
{
    private OnFragmentInteractionListener mListener;
    private long messageID;
    ArrayList<MessagePdu> messagePdu;
    //private MmsSetting setting;
    public static final String TAG = MessagePduDetailListFragment.class.getSimpleName();

    @Override
    protected void beforeBaseInit()
    {
        if (getArguments() != null)
        {
            messageID = getArguments().getLong(MessagePduDetailActivity.MESSAGE_ID_EXTRA);

            MessagePduDatabase db = DatabaseFactory.getmessagePduDatabase(getApplicationContext());
            messagePdu = db.getMessagePdu(messageID);


            //MmsSettingsDatabase mmsSettingsDatabase = DatabaseFactory.getMmsProfileDatabase(getApplicationContext());
            //setting = mmsSettingsDatabase.getMmsSettingByMessageId(messageID, false);
        }
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    LinearLayout llContainer;

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_detail_list_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        llContainer = ViewUtil.findById(view, R.id.llContainer);

        int fragCount = 0;

//        if (setting != null)
//        {
//            Fragment fr = MessagePduSettingsFragment.newInstance(messageID);
//
//            getFragmentManager().beginTransaction().add(llContainer.getId(), fr, "fragment" + fragCount).commit();
//
//            fragCount++;
//        }

        Collections.sort(messagePdu, new MessagePdu.MessagePduCreatedComparator());

        for (MessagePdu pdu : messagePdu)
        {
            int pduId = pdu.getId();

            Fragment fragment = resolveFragment(pdu.getType(), pdu.getId());

            if (fragment == null)
                continue;

            getFragmentManager().beginTransaction().add(llContainer.getId(), fragment, "fragment" + fragCount).commit();

            fragCount++;
        }
    }

    @Override
    protected void setupListeners()
    {

    }

    private Fragment resolveFragment(int type, int id)
    {
        switch (type)
        {
            case PduHeaders.MESSAGE_TYPE_SEND_REQ:
                return MessagePduSendReqFragment.newInstance(id);

            case PduHeaders.MESSAGE_TYPE_SEND_CONF:
                return MessagePduSendConfFragment.newInstance(id);

            case PduHeaders.MESSAGE_TYPE_NOTIFICATION_IND:
                return MessagePduNotificationIndFragment.newInstance(id);

            case PduHeaders.MESSAGE_TYPE_NOTIFYRESP_IND:
                return MessagePduNotifyRespIndFragment.newInstance(id);

            case PduHeaders.MESSAGE_TYPE_RETRIEVE_CONF:
                return MessagePduRetrieveConfFragment.newInstance(id);

            case PduHeaders.MESSAGE_TYPE_ACKNOWLEDGE_IND:
                return MessagePduAcknowledgeIndFragment.newInstance(id);

            case PduHeaders.MESSAGE_TYPE_DELIVERY_IND:
                return MessagePduDeliveryIndFragment.newInstance(id);

            case PduHeaders.MESSAGE_TYPE_READ_REC_IND:
                return MessagePduReadRecIndFragment.newInstance(id);

            case PduHeaders.MESSAGE_TYPE_READ_ORIG_IND:
                return MessagePduReadOrigIndFragment.newInstance(id);

            default:
                return null;
        }
    }


    public void onButtonPressed(Uri uri)
    {
        if (mListener != null)
        {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        }
        else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }


}

