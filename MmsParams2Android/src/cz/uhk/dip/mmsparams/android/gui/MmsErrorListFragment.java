package cz.uhk.dip.mmsparams.android.gui;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.thoughtcrime.securesms.MessageDetailsActivity;
import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.util.ViewUtil;

import java.util.List;

import cz.uhk.dip.mmsparams.android.adapters.MmsErrorListAdapter;
import cz.uhk.dip.mmsparams.android.db.MmsErrorDatabase;
import cz.uhk.dip.mmsparams.android.db.model.MmsError;
import cz.uhk.dip.mmsparams.android.gui.base.BrowseBaseFragment;

public class MmsErrorListFragment extends BrowseBaseFragment<MmsError>
{
    public static final String TAG = MmsErrorListFragment.class.getSimpleName();

    ListView listView;
    MmsErrorListAdapter adapter;
    long messageID;

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.mms_error_browse_fragment;
    }

    public static MmsErrorListFragment newInstance()
    {
        MmsErrorListFragment mt = new MmsErrorListFragment();
        return mt;
    }

    @Override
    protected void afterBaseInit()
    {
        Bundle extras = this.getArguments();
        messageID = extras.getLong(MessageDetailsActivity.MESSAGE_ID_EXTRA, -1);


        adapter = new MmsErrorListAdapter(getApplicationContext(), data);
        listView.setAdapter(adapter);

        super.afterBaseInit();
    }

    @Override
    protected List<MmsError> loadData()
    {
        MmsErrorDatabase db = DatabaseFactory.getMmsErrorDatabase(getContext());

        return db.getMessageError(messageID);
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected ArrayAdapter getAdapter()
    {
        return adapter;
    }

    @Override
    protected ListView getListView()
    {
        return listView;
    }

    @Override
    protected void initControls(View view)
    {
        listView = ViewUtil.findById(view, R.id.list);

        setHasOptionsMenu(true);
    }


    @Override
    protected void setupListeners()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                MmsError obj = (MmsError) parent.getItemAtPosition(position);

                if (allowOpenDetail())
                {
                    // This Activity was called by startActivity
                    // TODO
                    openDetail(MmsErrorDetailFragment.class, obj.getID());
                }
                else
                {
                    //This Activity was called by startActivityForResult
                    returnResult(obj);
                }

            }
        });
    }


}

