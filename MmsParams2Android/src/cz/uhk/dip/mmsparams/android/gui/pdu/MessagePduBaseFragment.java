package cz.uhk.dip.mmsparams.android.gui.pdu;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.MyNotificationInd;
import com.google.android.mms.pdu_alt.PduBody;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.database.Address;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ViewUtil;

import cz.uhk.dip.mmsparams.android.db.model.MessagePdu;
import cz.uhk.dip.mmsparams.android.gui.MessagePduDetailActivity;
import cz.uhk.dip.mmsparams.android.gui.base.BaseFragment;
import cz.uhk.dip.mmsparams.android.gui.base.OnFragmentInteractionListener;
import cz.uhk.dip.mmsparams.android.log.DBLog;
import cz.uhk.dip.mmsparams.android.pdus.PduByteHelper;
import cz.uhk.dip.mmsparams.android.pdus.PduHeaderConvertor;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;

public abstract class MessagePduBaseFragment extends BaseFragment
{
    public static final String TAG = MessagePduBaseFragment.class.getSimpleName();
    public static final String DEFAULT_EMPTY_VALUE = "...";

    protected String getDateTime(long seconds)
    {
        return PduHeaderConvertor.getDateString(seconds);
    }

    protected String getString(boolean value)
    {
        return String.valueOf(value);
    }

    protected String getString(String value)
    {
        if (value == null)
            return DEFAULT_EMPTY_VALUE;

        return value;
    }


    protected String getString(long value)
    {
        return String.valueOf(value);
    }

    protected String getString(EncodedStringValue value)
    {
        if (value == null)
            return DEFAULT_EMPTY_VALUE;
        return value.getString();
    }


    protected String getNumberString(EncodedStringValue from)
    {
        final String s = getString(from);
        if (StringUtil.isEmptyOrNull(s) || DEFAULT_EMPTY_VALUE.equals(s))
            return DEFAULT_EMPTY_VALUE;

        try
        {
            Recipient r = Recipient.from(getContext(), Address.fromExternal(getContext(), s), false);

            if (r == null || r.getName() == null)
                return DEFAULT_EMPTY_VALUE;

            String res = s;
            if (!StringUtil.isEmptyOrNull(r.getName()))
            {
                res += " (" + r.getName() + ")";
            }
            return res;
        }
        catch (Exception ex)
        {
            DBLog.logToDB(getContext(), "getNumberString", TAG, ex);
            return s;
        }
    }

    protected String getToAsString(EncodedStringValue[] toList)
    {
        if (toList == null)
            return DEFAULT_EMPTY_VALUE;

        String to = "";

        for (EncodedStringValue s : toList)
        {
            to += getNumberString(s) + ", ";
        }

        to = to.trim();
        to = StringUtil.trim(to, ',');

        return to;
    }


    protected String getString(byte[] value)
    {
        if (value == null || value.length == 0)
            return "...";

        return new String(value);
    }

    protected String getStringInt(int value)
    {
        return String.valueOf(value);
    }

    protected String getStringLong(long value)
    {
        return String.valueOf(value);
    }


    protected String getSizeCalc(final PduBody body, final EncodedStringValue subject)
    {
        long size = PduByteHelper.getPduBodySize(body, subject);

//        if (subject != null)
//        {
//            size += subject.getString().length();
//        }

        return getSize(size);
    }


    protected String getSize(long size)
    {
        return PduByteHelper.getSizeString(size);
    }

    protected String getCreatedString(long dtCreated)
    {
        if (dtCreated == 0)
            return DEFAULT_EMPTY_VALUE;
        return PduHeaderConvertor.getDateStringMilis(dtCreated);
    }


    protected String getUrlString(Uri uri)
    {
        if (uri == null)
            return DEFAULT_EMPTY_VALUE;


        // TODO
        return uri.toString();

    }


    private OnFragmentInteractionListener mListener;

    public void onButtonPressed(Uri uri)
    {
        if (mListener != null)
        {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        }
        else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }


}
