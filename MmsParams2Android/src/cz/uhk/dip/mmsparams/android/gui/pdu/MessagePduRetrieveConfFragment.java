package cz.uhk.dip.mmsparams.android.gui.pdu;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.mms.ContentType;
import com.google.android.mms.pdu_alt.MyNotificationInd;
import com.google.android.mms.pdu_alt.MyRetrieveConf;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduPart;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.util.ViewUtil;

import cz.uhk.dip.mmsparams.android.db.model.MessagePdu;
import cz.uhk.dip.mmsparams.android.gui.MessagePduDetailActivity;
import cz.uhk.dip.mmsparams.android.pdus.PduHeaderConvertor;

public class MessagePduRetrieveConfFragment extends MessagePduBaseFragment
{
    private int paramID;
    private MyRetrieveConf retrieveConf;
    private MessagePdu messagePdu;

    private TextView tvMessageFrom, tvMessageType, tvMmsVersion, tvMessageSubject, tvMessageTo,
            tvMessagePriority, tvMessageDate, tvMessageCc, tvMessageContentType, tvPduCreated,
            tvMessageDeliveryReport, tvMessageClass, tvMmsMessageID,
            tvMmsMessageReadReport, tvMmsMessageRetrieveStatus,
            tvMmsMessageRetrieveText, tvMmsTransactionID, tvMessageBody, tvMessageSizeCalc, tvMessageDrmContent;

    private Button btnShowBody;
    public static final String TAG = MessagePduRetrieveConfFragment.class.getSimpleName();

    public static MessagePduRetrieveConfFragment newInstance(int paramID)
    {
        MessagePduRetrieveConfFragment fragment = new MessagePduRetrieveConfFragment();
        Bundle args = new Bundle();
        args.putInt(MessagePduDetailActivity.ID_EXTRA, paramID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected void beforeBaseInit()
    {
        super.beforeBaseInit();

        paramID = getArguments().getInt(MessagePduDetailActivity.ID_EXTRA);
        messagePdu = MessagePduDetailActivity.getGenericPdu(getContext(), paramID);
        retrieveConf = (MyRetrieveConf) messagePdu.getGenericPdu();
    }

    private void fillControls()
    {

        try
        {
            tvPduCreated.setText(getCreatedString(messagePdu.getDateCreated()));

            tvMessageFrom.setText(getString(retrieveConf.getFrom()));
            tvMessageType.setText(PduHeaderConvertor.getMessageTypeString(retrieveConf.getMessageType()));
            tvMmsVersion.setText(PduHeaderConvertor.getMmsVersionString(retrieveConf.getMmsVersion()));


            tvMessageSubject.setText(getString(retrieveConf.getSubject()));
            tvMessageTo.setText(getToAsString(retrieveConf.getTo()));
            tvMessagePriority.setText(PduHeaderConvertor.getPriorityString(retrieveConf.getPriority()));
            tvMessageDate.setText(getDateTime(retrieveConf.getDate()));
            tvMessageCc.setText(getToAsString(retrieveConf.getCc()));
            tvMessageBody.setText(getBody(retrieveConf.getBody()));
            tvMessageContentType.setText(getString(retrieveConf.getContentType()));
            tvMessageDeliveryReport.setText(PduHeaderConvertor.getValueString(retrieveConf.getDeliveryReport()));
            tvMessageClass.setText(PduHeaderConvertor.getMessageClassString(retrieveConf.getMessageClass()));
            tvMmsMessageID.setText(getString(retrieveConf.getMessageId()));
            tvMmsMessageReadReport.setText(PduHeaderConvertor.getValueString(retrieveConf.getReadReport()));
            tvMmsMessageRetrieveStatus.setText(PduHeaderConvertor.getRetrieveStatusString(retrieveConf.getRetrieveStatus()));
            tvMmsMessageRetrieveText.setText(getString(retrieveConf.getRetrieveText()));

            tvMmsTransactionID.setText(getString(retrieveConf.getTransactionId()));
            tvMessageDrmContent.setText(PduHeaderConvertor.getValueString(retrieveConf.getDrmContent()));

            tvMessageSizeCalc.setText(getSizeCalc(retrieveConf.getBody(), retrieveConf.getSubject()));


        }
        catch (Exception e)
        {
            int k = 0;
        }
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_retrieve_conf_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        tvPduCreated = ViewUtil.findById(view, R.id.tvPduCreated);

        tvMessageFrom = ViewUtil.findById(view, R.id.tvMessageFrom);
        tvMessageType = ViewUtil.findById(view, R.id.tvMessageType);
        tvMmsVersion = ViewUtil.findById(view, R.id.tvMmsVersion);


        tvMessageSubject = ViewUtil.findById(view, R.id.tvMessageSubject);
        tvMessageTo = ViewUtil.findById(view, R.id.tvMessageTo);
        tvMessagePriority = ViewUtil.findById(view, R.id.tvMessagePriority);
        tvMessageDate = ViewUtil.findById(view, R.id.tvMessageDate);
        tvMessageBody = ViewUtil.findById(view, R.id.tvMessageBody);
        tvMessageCc = ViewUtil.findById(view, R.id.tvMessageCc);
        tvMessageContentType = ViewUtil.findById(view, R.id.tvMessageContentType);
        tvMessageDeliveryReport = ViewUtil.findById(view, R.id.tvMessageDeliveryReport);
        tvMessageClass = ViewUtil.findById(view, R.id.tvMessageClass);
        tvMmsMessageID = ViewUtil.findById(view, R.id.tvMmsMessageID);
        tvMmsMessageReadReport = ViewUtil.findById(view, R.id.tvMmsMessageReadReport);
        tvMmsMessageRetrieveStatus = ViewUtil.findById(view, R.id.tvMmsMessageRetrieveStatus);
        tvMmsMessageRetrieveText = ViewUtil.findById(view, R.id.tvMmsMessageRetrieveText);
        tvMmsTransactionID = ViewUtil.findById(view, R.id.tvMmsTransactionID);
        tvMessageSizeCalc = ViewUtil.findById(view, R.id.tvMessageSizeCalc);
        tvMessageDrmContent = ViewUtil.findById(view, R.id.tvMessageDrmContent);


        btnShowBody = ViewUtil.findById(view, R.id.btnShowBody);

        fillControls();
    }


    private String getBody(PduBody body)
    {
        if (body == null)
        {
            return DEFAULT_EMPTY_VALUE;
        }
        int count = body.getPartsNum();

        for (int i = 0; i < count; i++)
        {
            PduPart pduPart = body.getPart(i);

            byte[] ct = pduPart.getContentType();
            if (ct == null)
                continue;

            String cts = getString(ct);

            if (ContentType.TEXT_PLAIN.equals(cts))
            {
                return getString(pduPart.getData());
            }
        }

        return DEFAULT_EMPTY_VALUE;
    }

    @Override
    protected void setupListeners()
    {
        btnShowBody.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                handleShowBodyClick();
            }
        });
    }

    private void handleShowBodyClick()
    {
        openFragment(MessagePduPduBodyListFragment.newInstance(paramID), null);
    }


}
