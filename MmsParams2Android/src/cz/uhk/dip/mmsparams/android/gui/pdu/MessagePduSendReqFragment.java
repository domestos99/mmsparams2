package cz.uhk.dip.mmsparams.android.gui.pdu;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.mms.ContentType;
import com.google.android.mms.pdu_alt.MySendReq;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduPart;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.util.ViewUtil;

import cz.uhk.dip.mmsparams.android.db.model.MessagePdu;
import cz.uhk.dip.mmsparams.android.gui.MessagePduDetailActivity;
import cz.uhk.dip.mmsparams.android.pdus.PduHeaderConvertor;


public class MessagePduSendReqFragment extends MessagePduBaseFragment
{
    private int paramID;
    private MySendReq sendReq;
    private MessagePdu messagePdu;


    private TextView tvMessageFrom, tvMessageType, tvMmsVersion, tvMessageSubject, tvMessageTo, tvMessagePriority, tvMessageDate, tvMessageBody, tvMessageSenderVisibility, tvMessageContentType;
    private TextView tvMessageBcc, tvMessageCc, tvMessageDeliveryReport, tvMessageExpiry, tvMessageEarliestDelivery, tvMessageSizePdu, tvMessageSizeCalc, tvMessageClass, tvPduCreated, tvMessageReadReport, tvMessageTransactionId,
            tvMessageAdaptationAllowed, tvMessageDrmContent;
    private Button btnShowBody;

    public static final String TAG = MessagePduSendReqFragment.class.getSimpleName();


    public MessagePduSendReqFragment()
    {
    }

    public static MessagePduSendReqFragment newInstance(int paramID)
    {
        MessagePduSendReqFragment fragment = new MessagePduSendReqFragment();
        Bundle args = new Bundle();
        args.putInt(MessagePduDetailActivity.ID_EXTRA, paramID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected void beforeBaseInit()
    {
        super.beforeBaseInit();

        paramID = getArguments().getInt(MessagePduDetailActivity.ID_EXTRA);
        messagePdu = MessagePduDetailActivity.getGenericPdu(getContext(), paramID);
        sendReq = (MySendReq) messagePdu.getGenericPdu();
    }

    private void fillControls()
    {

        try
        {
            tvPduCreated.setText(getCreatedString(messagePdu.getDateCreated()));

            tvMessageFrom.setText(getNumberString(sendReq.getFrom()));

            tvMessageType.setText(PduHeaderConvertor.getMessageTypeString(sendReq.getMessageType()));

            tvMmsVersion.setText(PduHeaderConvertor.getMmsVersionString(sendReq.getMmsVersion()));

            tvMessageSubject.setText(getString(sendReq.getSubject()));

            tvMessageTo.setText(getToAsString(sendReq.getTo()));

            tvMessagePriority.setText(PduHeaderConvertor.getPriorityString(sendReq.getPriority()));

            tvMessageDate.setText(getDateTime(sendReq.getDate()));

            tvMessageBody.setText(getBody(sendReq.getBody()));

            tvMessageSenderVisibility.setText(PduHeaderConvertor.getSenderVisibility(sendReq.getSenderIsVisible()));


            tvMessageBcc.setText(getToAsString(sendReq.getBcc()));
            tvMessageCc.setText(getToAsString(sendReq.getCc()));
            tvMessageDeliveryReport.setText(PduHeaderConvertor.getValueString(sendReq.getDeliveryReport()));

            tvMessageExpiry.setText(PduHeaderConvertor.getDateString(sendReq.getExpiry()));

            tvMessageEarliestDelivery.setText(PduHeaderConvertor.getDateString(sendReq.getDeliveryTime()));

            tvMessageSizePdu.setText(getSize(sendReq.getMessageSize()));

            tvMessageSizeCalc.setText(getSizeCalc(sendReq.getBody(), sendReq.getSubject()));

            tvMessageClass.setText(PduHeaderConvertor.getMessageClassString(sendReq.getMessageClass()));
            tvMessageReadReport.setText(PduHeaderConvertor.getValueString(sendReq.getReadReport()));

            tvMessageTransactionId.setText(getString(sendReq.getTransactionId()));

            tvMessageContentType.setText(getString(sendReq.getContentType()));

            tvMessageDrmContent.setText(PduHeaderConvertor.getValueString(sendReq.getDrmContent()));
            tvMessageAdaptationAllowed.setText(PduHeaderConvertor.getValueString(sendReq.getAdaptationAllowed()));

        }
        catch (Exception e)
        {
            int k = 0;
        }
    }


    private String getBody(PduBody body)
    {
        if (body == null)
        {
            return DEFAULT_EMPTY_VALUE;
        }
        int count = body.getPartsNum();

        for (int i = 0; i < count; i++)
        {
            PduPart pduPart = body.getPart(i);

            byte[] ct = pduPart.getContentType();
            if (ct == null)
                continue;

            String cts = getString(ct);

            if (ContentType.TEXT_PLAIN.equals(cts))
            {
                return getString(pduPart.getData());
            }
        }

        return DEFAULT_EMPTY_VALUE;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_send_req_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        tvPduCreated = ViewUtil.findById(view, R.id.tvPduCreated);

        tvMessageFrom = ViewUtil.findById(view, R.id.tvMessageFrom);
        tvMessageType = ViewUtil.findById(view, R.id.tvMessageType);
        tvMmsVersion = ViewUtil.findById(view, R.id.tvMmsVersion);
        tvMessageSubject = ViewUtil.findById(view, R.id.tvMessageSubject);
        tvMessageTo = ViewUtil.findById(view, R.id.tvMessageTo);
        tvMessagePriority = ViewUtil.findById(view, R.id.tvMessagePriority);
        tvMessageDate = ViewUtil.findById(view, R.id.tvMessageDate);
        tvMessageBody = ViewUtil.findById(view, R.id.tvMessageBody);

        tvMessageSenderVisibility = ViewUtil.findById(view, R.id.tvMessageSenderVisibility);


        tvMessageBcc = ViewUtil.findById(view, R.id.tvMessageBcc);
        tvMessageCc = ViewUtil.findById(view, R.id.tvMessageCc);
        tvMessageDeliveryReport = ViewUtil.findById(view, R.id.tvMessageDeliveryReport);
        tvMessageExpiry = ViewUtil.findById(view, R.id.tvMessageExpiry);
        tvMessageEarliestDelivery = ViewUtil.findById(view, R.id.tvMessageEarliestDelivery);
        tvMessageSizePdu = ViewUtil.findById(view, R.id.tvMessageSizePdu);
        tvMessageSizeCalc = ViewUtil.findById(view, R.id.tvMessageSizeCalc);
        tvMessageClass = ViewUtil.findById(view, R.id.tvMessageClass);
        tvMessageReadReport = ViewUtil.findById(view, R.id.tvMessageReadReport);
        tvMessageTransactionId = ViewUtil.findById(view, R.id.tvMessageTransactionId);
        tvMessageContentType = ViewUtil.findById(view, R.id.tvMessageContentType);
        tvMessageDrmContent = ViewUtil.findById(view, R.id.tvMessageDrmContent);
        tvMessageAdaptationAllowed = ViewUtil.findById(view, R.id.tvMessageAdaptationAllowed);

        btnShowBody = ViewUtil.findById(view, R.id.btnShowBody);


        fillControls();
    }

    @Override
    protected void setupListeners()
    {

        btnShowBody.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                handleShowBodyClick();
            }
        });
    }


    private void handleShowBodyClick()
    {
        openFragment(MessagePduPduBodyListFragment.newInstance(paramID), null);
    }


}
