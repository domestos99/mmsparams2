package cz.uhk.dip.mmsparams.android.http;

import android.os.AsyncTask;

import org.thoughtcrime.securesms.logging.Log;

import cz.uhk.dip.mmsparams.api.http.HttpGetUtil;
import cz.uhk.dip.mmsparams.api.http.TaskResult;

public class HttpGetTask
{
    public static AsyncTask<String, Void, TaskResult<String>> get()
    {
        return new GetDataTask();
    }

    private static class GetDataTask extends AsyncTask<String, Void, TaskResult<String>>
    {
        private static final String TAG = GetDataTask.class.getName();

        @Override
        protected TaskResult<String> doInBackground(String... strings)
        {
            return getJSON(strings[0]);
        }

        protected TaskResult<String> getJSON(String url)
        {
            Log.i(TAG, "Getting data from: " + url);
            return HttpGetUtil.getDataFromUrl(url);
        }
    }


}
