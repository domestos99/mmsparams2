package cz.uhk.dip.mmsparams.android.jobs;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.android.mms.dom.smil.parser.SmilXmlSerializer;
import com.google.android.mms.ContentType;
import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.pdu_alt.CharacterSets;
import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.MySendConf;
import com.google.android.mms.pdu_alt.MySendReq;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.MyPduComposer;
import com.google.android.mms.pdu_alt.PduHeaders;
import com.google.android.mms.pdu_alt.PduPart;
import com.google.android.mms.pdu_alt.SendConf;
import com.google.android.mms.pdu_alt.SendReq;
import com.google.android.mms.smil.SmilHelper;
import com.klinker.android.send_message.Utils;

import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.database.Address;
import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.jobs.MmsSendJob;
import org.thoughtcrime.securesms.jobs.SendJob;
import org.thoughtcrime.securesms.logging.Log;
import org.thoughtcrime.securesms.mms.CompatMmsConnection;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.MmsSendResult;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.notifications.MessageNotifier;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.transport.InsecureFallbackApprovalException;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;
import org.thoughtcrime.securesms.util.Hex;
import org.thoughtcrime.securesms.util.NumberUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import cz.uhk.dip.mmsparams.android.log.MmsErrorLogger;
import cz.uhk.dip.mmsparams.android.mms.MySendReqFactory;
import cz.uhk.dip.mmsparams.android.pdus.PduByteHelper;
import cz.uhk.dip.mmsparams.android.websocket.WebSocketErrorFactory;
import cz.uhk.dip.mmsparams.android.websocket.WebSocketPduHelper;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;

public class MmsSendWSJob extends SendJob
{

    public static final String KEY =  MmsSendWSJob.class.getSimpleName();

    private static final String TAG = MmsSendWSJob.class.getSimpleName();

    private static final String KEY_MESSAGE_ID = "message_id";
    private static final String KEY_REQUEST = "request";

    private long messageId;
    private final MmsSendPhoneRequestMessage request;

    public MmsSendWSJob(long messageId, MmsSendPhoneRequestMessage request)
    {
        this(new Job.Parameters.Builder()
                        .setQueue("mms-operation")
                        .addConstraint(NetworkConstraint.KEY)
                        .setMaxAttempts(15)
                        .build(),
                messageId, request);
    }

    private MmsSendWSJob(@NonNull Job.Parameters parameters, long messageId, MmsSendPhoneRequestMessage request)
    {
        super(parameters);
        this.messageId = messageId;
        this.request = request;
    }

    @Override
    public @NonNull
    Data serialize()
    {
        return new Data.Builder()
                .putLong(KEY_MESSAGE_ID, messageId)
                .putString(KEY_REQUEST, JsonUtilsSafe.toJson(request))
                .build();
    }

    @Override
    public @NonNull
    String getFactoryKey()
    {
        return KEY;
    }

    @Override
    public void onSend() throws MmsException, NoSuchMessageException, IOException
    {
        final MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);

         try
         {

             MmsDatabase database = DatabaseFactory.getMmsDatabase(context);
             OutgoingMediaMessage message = database.getOutgoingMessage(messageId);

             if (database.isSent(messageId))
             {
                 Log.w(TAG, "Message " + messageId + " was already sent. Ignoring.");
                 return;
             }

             try
             {
                 Log.i(TAG, "Sending message: " + messageId);


                 MySendReq pdu = MySendReqFactory.constructSendPdu(context, request.getMmsSendModel());

//            SendReq pdu = constructSendPdu(message);
                 // TODO
                 // validateDestinations(message, pdu);

                 final byte[] pduBytes = PduByteHelper.getPduBytes(context, pdu);

                 PduByteHelper.storeMessagePdu(context, messageId, PduHeaders.MESSAGE_TYPE_SEND_REQ, pduBytes);

                 mmsErrorLogger.setMessageID(messageId);
                 WebSocketPduHelper.handleSendSendReq(pdu, request);


                 final byte[] sendConf = new CompatMmsConnection(context, mmsErrorLogger).send(pduBytes, message.getSubscriptionId());

                 MySendConf sendConfObj = PduByteHelper.getPdu(sendConf);  //(SendConf) new MyPduParser(sendConf).parse();

                 // Save PDU data of confirmation
                 PduByteHelper.storeMessagePdu(context, messageId, PduHeaders.MESSAGE_TYPE_SEND_CONF, sendConf);
                 // Send to WebSocket
                 WebSocketPduHelper.handleSendSendConf(sendConfObj, request);

                 final MmsSendResult result = PduByteHelper.getSendResult(sendConfObj, pdu);

                 mmsErrorLogger.setMessageID(messageId);
                 database.markAsSent(messageId, false);
                 markAttachmentsUploaded(messageId, message.getAttachments());

                 mmsErrorLogger.setMessageID(messageId);
                 Log.i(TAG, "Sent message: " + messageId);
             }
             catch (UndeliverableMessageException e)
             {
                 Log.w(TAG, e);
                 database.markAsSentFailed(messageId);
                 notifyMediaMessageDeliveryFailed(context, messageId);
                 mmsErrorLogger.log(messageId, TAG, e);
                 WebSocketErrorFactory.sendError(TAG, e, request, "MmsSendWSJob.handleSentResult");
             }
         }
         catch (Exception ex)
         {
             // DBLog.logToDB(context, "OnSend - whole t-c", TAG, ex);
             mmsErrorLogger.log(messageId, TAG, ex);
             WebSocketErrorFactory.sendError(TAG, ex, request, "MmsSendWSJob.handleSentResult");
             onCanceled();
             return;
         }
    }

    @Override
    public boolean onShouldRetry(@NonNull Exception exception)
    {
        return false;
    }

    @Override
    public void onCanceled()
    {
        Log.i(TAG, "onCanceled() messageId: " + messageId);
        DatabaseFactory.getMmsDatabase(context).markAsSentFailed(messageId);
        notifyMediaMessageDeliveryFailed(context, messageId);
    }

    private byte[] getPduBytes(MySendReq message)
            throws IOException, UndeliverableMessageException, InsecureFallbackApprovalException
    {
        byte[] pduBytes = new MyPduComposer(context, message).make();

        if (pduBytes == null)
        {
            throw new UndeliverableMessageException("PDU composition failed, null payload");
        }

        return pduBytes;
    }

    private MmsSendResult getSendResult(MySendConf conf, MySendReq message)
            throws UndeliverableMessageException
    {
        if (conf == null)
        {
            throw new UndeliverableMessageException("No M-Send.conf received in response to send.");
        }
        else if (conf.getResponseStatus() != PduHeaders.RESPONSE_STATUS_OK)
        {
            throw new UndeliverableMessageException("Got bad response: " + conf.getResponseStatus());
        }
        else if (isInconsistentResponse(message, conf))
        {
            throw new UndeliverableMessageException("Mismatched response!");
        }
        else
        {
            return new MmsSendResult(conf.getMessageId(), conf.getResponseStatus());
        }
    }

    private boolean isInconsistentResponse(MySendReq message, MySendConf response)
    {
        Log.i(TAG, "Comparing: " + Hex.toString(message.getTransactionId()));
        Log.i(TAG, "With:      " + Hex.toString(response.getTransactionId()));
        return !Arrays.equals(message.getTransactionId(), response.getTransactionId());
    }

    private void validateDestinations(EncodedStringValue[] destinations) throws UndeliverableMessageException
    {
        if (destinations == null) return;

        for (EncodedStringValue destination : destinations)
        {
            if (destination == null || !NumberUtil.isValidSmsOrEmail(destination.getString()))
            {
                throw new UndeliverableMessageException("Invalid destination: " +
                        (destination == null ? null : destination.getString()));
            }
        }
    }

    private void validateDestinations(OutgoingMediaMessage media, MySendReq message) throws UndeliverableMessageException
    {
        validateDestinations(message.getTo());
        validateDestinations(message.getCc());
        validateDestinations(message.getBcc());

        if (message.getTo() == null && message.getCc() == null && message.getBcc() == null)
        {
            throw new UndeliverableMessageException("No to, cc, or bcc specified!");
        }

        if (media.isSecure())
        {
            throw new UndeliverableMessageException("Attempt to send encrypted MMS?");
        }
    }

    private MySendReq constructSendPdu(OutgoingMediaMessage message)
            throws UndeliverableMessageException
    {
        MySendReq req = new MySendReq();
        String lineNumber = getMyNumber(context);
        Address destination = message.getRecipient().getAddress();
        MediaConstraints mediaConstraints = MediaConstraints.getMmsMediaConstraints(message.getSubscriptionId());
        List<Attachment> scaledAttachments = scaleAndStripExifFromAttachments(mediaConstraints, message.getAttachments());

        if (!TextUtils.isEmpty(lineNumber))
        {
            req.setFrom(new EncodedStringValue(lineNumber));
        }
        else
        {
            req.setFrom(new EncodedStringValue(TextSecurePreferences.getLocalNumber(context)));
        }

        if (destination.isMmsGroup())
        {
            List<Recipient> members = DatabaseFactory.getGroupDatabase(context).getGroupMembers(destination.toGroupString(), false);

            for (Recipient member : members)
            {
                if (message.getDistributionType() == ThreadDatabase.DistributionTypes.BROADCAST)
                {
                    req.addBcc(new EncodedStringValue(member.getAddress().serialize()));
                }
                else
                {
                    req.addTo(new EncodedStringValue(member.getAddress().serialize()));
                }
            }
        }
        else
        {
            req.addTo(new EncodedStringValue(destination.serialize()));
        }

        req.setDate(System.currentTimeMillis() / 1000);

        PduBody body = new PduBody();
        int size = 0;

        if (!TextUtils.isEmpty(message.getBody()))
        {
            PduPart part = new PduPart();
            String name = String.valueOf(System.currentTimeMillis());
            part.setData(Util.toUtf8Bytes(message.getBody()));
            part.setCharset(CharacterSets.UTF_8);
            part.setContentType(ContentType.TEXT_PLAIN.getBytes());
            part.setContentId(name.getBytes());
            part.setContentLocation((name + ".txt").getBytes());
            part.setName((name + ".txt").getBytes());

            body.addPart(part);
            size += getPartSize(part);
        }

        for (Attachment attachment : scaledAttachments)
        {
            try
            {
                if (attachment.getDataUri() == null)
                    throw new IOException("Assertion failed, attachment for outgoing MMS has no data!");

                String fileName = attachment.getFileName();
                PduPart part = new PduPart();

                if (fileName == null)
                {
                    fileName = String.valueOf(Math.abs(Util.getSecureRandom().nextLong()));
                    String fileExtension = MimeTypeMap.getSingleton().getExtensionFromMimeType(attachment.getContentType());

                    if (fileExtension != null) fileName = fileName + "." + fileExtension;
                }

                if (attachment.getContentType().startsWith("text"))
                {
                    part.setCharset(CharacterSets.UTF_8);
                }

                part.setContentType(attachment.getContentType().getBytes());
                part.setContentLocation(fileName.getBytes());
                part.setName(fileName.getBytes());

                int index = fileName.lastIndexOf(".");
                String contentId = (index == -1) ? fileName : fileName.substring(0, index);
                part.setContentId(contentId.getBytes());
                part.setData(Util.readFully(PartAuthority.getAttachmentStream(context, attachment.getDataUri())));

                body.addPart(part);
                size += getPartSize(part);
            }
            catch (IOException e)
            {
                Log.w(TAG, e);
            }
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        SmilXmlSerializer.serialize(SmilHelper.createSmilDocument(body), out);
        PduPart smilPart = new PduPart();
        smilPart.setContentId("smil".getBytes());
        smilPart.setContentLocation("smil.xml".getBytes());
        smilPart.setContentType(ContentType.APP_SMIL.getBytes());
        smilPart.setData(out.toByteArray());
        body.addPart(0, smilPart);

        req.setBody(body);
        req.setMessageSize(size);
        req.setMessageClass(PduHeaders.MESSAGE_CLASS_PERSONAL_STR.getBytes());
        req.setExpiry(7 * 24 * 60 * 60);

        try
        {
            req.setPriority(PduHeaders.PRIORITY_NORMAL);
            req.setDeliveryReport(PduHeaders.VALUE_NO);
            req.setReadReport(PduHeaders.VALUE_NO);
        }
        catch (InvalidHeaderValueException e)
        {
        }

        return req;
    }

    private long getPartSize(PduPart part)
    {
        return part.getName().length + part.getContentLocation().length +
                part.getContentType().length + part.getData().length +
                part.getContentId().length;
    }

    private void notifyMediaMessageDeliveryFailed(Context context, long messageId)
    {
        long threadId = DatabaseFactory.getMmsDatabase(context).getThreadIdForMessage(messageId);
        Recipient recipient = DatabaseFactory.getThreadDatabase(context).getRecipientForThreadId(threadId);

        if (recipient != null)
        {
            MessageNotifier.notifyMessageDeliveryFailed(context, recipient, threadId);
        }
    }

    private String getMyNumber(Context context) throws UndeliverableMessageException
    {
        try
        {
            return Utils.getMyPhoneNumber(context);
        }
        catch (SecurityException e)
        {
            throw new UndeliverableMessageException(e);
        }
    }

    public static class Factory implements Job.Factory<MmsSendWSJob>
    {
        @Override
        public @NonNull
        MmsSendWSJob create(@NonNull Job.Parameters parameters, @NonNull Data data)
        {
            return new MmsSendWSJob(parameters, data
                    .getLong(KEY_MESSAGE_ID),
                    JsonUtilsSafe.fromJson(data.getString(KEY_REQUEST), MmsSendPhoneRequestMessage.class));
        }
    }
}
