package cz.uhk.dip.mmsparams.android.jobs;

import android.content.Context;

import org.thoughtcrime.securesms.ApplicationContext;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.database.Address;
import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.jobs.BaseJob;
import org.thoughtcrime.securesms.logging.Log;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.dualsim.SubscriptionInfoCompat;
import org.thoughtcrime.securesms.util.dualsim.SubscriptionManagerCompat;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import cz.uhk.dip.mmsparams.android.mms.SlidesFactory;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;

public class MmsSendWSProcess extends BaseJob
{
    public static final String KEY = MmsSendWSProcess.class.getSimpleName();

    private static final String TAG = MmsSendWSProcess.class.getSimpleName();

    private static final String KEY_REQUEST = "request";

    private final MmsSendPhoneRequestMessage req;

    public MmsSendWSProcess(MmsSendPhoneRequestMessage req)
    {
        this(new Job.Parameters.Builder()
                .setQueue("mms-operation")
                .addConstraint(NetworkConstraint.KEY)
                .setMaxAttempts(15)
                .build(), req);
    }

    private MmsSendWSProcess(@NonNull Job.Parameters parameters, MmsSendPhoneRequestMessage req)
    {
        super(parameters);
        this.req = req;
    }

    @Override
    public @NonNull
    Data serialize()
    {
        return new Data.Builder()
                .putString(KEY_REQUEST, JsonUtilsSafe.toJson(req))
                .build();
    }

    @Override
    public @NonNull
    String getFactoryKey()
    {
        return KEY;
    }


    @Override
    protected void onRun() throws Exception
    {
        final OutgoingMediaMessage outgoingMessage = createMessage(context, req.getMmsSendModel());

        ThreadDatabase threadDatabase = DatabaseFactory.getThreadDatabase(context);
        final long threadId = threadDatabase.getThreadIdFor(outgoingMessage.getRecipient(), outgoingMessage.getDistributionType());

        MmsDatabase database = DatabaseFactory.getMmsDatabase(context);

        long messageId = 0;
        try
        {
            messageId = database.insertMessageOutbox(outgoingMessage, threadId, false, null);
        }
        catch (MmsException e)
        {
            e.printStackTrace();
            Log.e(TAG, "process", e);
        }


        ApplicationContext.getInstance(context)
                .getJobManager()
                .add(new MmsSendWSJob(messageId, req));

    }

    public static OutgoingMediaMessage createMessage(Context context, @NonNull final MmsSendModel mt)
    {
        List<Address> memberAddresses = getMembers(context, mt);

        // TODO check
        memberAddresses.add(Address.fromSerialized(TextSecurePreferences.getLocalNumber(context)));

        String groupId = DatabaseFactory.getGroupDatabase(context).getOrCreateGroupForMembers(memberAddresses, true);

        Recipient groupRecipient = Recipient.from(context, Address.fromSerialized(groupId), true);
        long threadId = DatabaseFactory.getThreadDatabase(context).getThreadIdFor(groupRecipient, ThreadDatabase.DistributionTypes.DEFAULT);


//        List<Slide> slides = SlidesFactory.create(context, mt.getMmsAttachmentSendModel());
//        List<? extends Attachment> attachments;
        SlideDeck sd = SlidesFactory.create(context, mt.getMmsAttachmentSendModel());


        // TODO
        long expiresIn = groupRecipient.getExpireMessages() * 1000;


//        SubscriptionManagerCompat subscriptionManager = new SubscriptionManagerCompat(context);
//        List<SubscriptionInfoCompat> subscriptions = subscriptionManager.getActiveSubscriptionInfoList();

        int subsID = -1;
//        if (subscriptions.size() > 0)
//        {
//            SubscriptionInfoCompat s = subscriptions.get(0);
//            subsID = s.getSubscriptionId();
//        }

        OutgoingMediaMessage outgoingMessage = new OutgoingMediaMessage(groupRecipient,
                sd,
                mt.getText(),
                System.currentTimeMillis(),
                subsID,
                expiresIn,
                false, ThreadDatabase.DistributionTypes.CONVERSATION,
                null, Collections.emptyList(), Collections.emptyList());


        return outgoingMessage;
    }

    private static List<Address> getMembers(Context context, MmsSendModel mt)
    {

        List<Address> addresses = new LinkedList<>();

        if (mt.getTo() != null)
        {
            for (String to : mt.getTo())
            {
                if (!StringUtil.isEmptyOrNull(to))
                {
                    addresses.add(Address.fromExternal(context, to));
                }
            }
        }
        if (mt.getBcc() != null)
        {
            for (String to : mt.getBcc())
            {
                if (!StringUtil.isEmptyOrNull(to))
                {
                    addresses.add(Address.fromExternal(context, to));
                }
            }
        }
        if (mt.getCc() != null)
        {
            for (String to : mt.getCc())
            {
                if (!StringUtil.isEmptyOrNull(to))
                {
                    addresses.add(Address.fromExternal(context, to));
                }
            }
        }

        return addresses;
    }


    @Override
    public boolean onShouldRetry(@NonNull Exception exception)
    {
        return false;
    }

    @Override
    public void onCanceled()
    {
    }


    public static class Factory implements Job.Factory<MmsSendWSProcess>
    {
        @Override
        public @NonNull
        MmsSendWSProcess create(@NonNull Parameters parameters, @NonNull Data data)
        {
            MmsSendPhoneRequestMessage req = JsonUtilsSafe.fromJson(data.getString(KEY_REQUEST), MmsSendPhoneRequestMessage.class);
            return new MmsSendWSProcess(parameters, req);
        }
    }
}