package cz.uhk.dip.mmsparams.android.mms;

import android.util.Pair;

public class MessageThreadIDPair extends Pair<Long, Long>
{
    public MessageThreadIDPair(Long messageID, Long threadID)
    {
        super(messageID, threadID);
    }

    public long getMessageId()
    {
        return super.first;
    }

    public long getThreadID()
    {
        return super.second;
    }
}
