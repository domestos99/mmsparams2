package cz.uhk.dip.mmsparams.android.mms;

import android.content.Context;
import android.net.Uri;

import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.mms.AudioSlide;
import org.thoughtcrime.securesms.mms.DocumentSlide;
import org.thoughtcrime.securesms.mms.GifSlide;
import org.thoughtcrime.securesms.mms.ImageSlide;
import org.thoughtcrime.securesms.mms.LocationSlide;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.mms.VideoSlide;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import cz.uhk.dip.mmsparams.api.constants.MediaType;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsAttachmentSendModel;

public class SlidesFactory
{
    public static SlideDeck create(Context context, ArrayList<MmsAttachmentSendModel> attachment)
    {
        SlideDeck sd = new SlideDeck();

        if (attachment == null)
            return sd;

        for (MmsAttachmentSendModel at : attachment)
        {
            String filename = UUID.randomUUID().toString() + "_" + at.getFileName();
            byte[] fileContents = at.getData();

            Uri uri = writeFile(context, filename, fileContents);

            Slide slide = null;
            switch (at.getMediaType())
            {
                case IMAGE:
                    slide = new ImageSlide(context, uri, at.getData().length, 0, 0, filename);
                    break;
                case LOCATION:
                    // slide = new LocationSlide()
                    // slide = MediaType.LOCATION.createSlide(context, uri, filename, at.getMimeType(), at.getData().length);
                    break;
                case GIF:
                    slide = new GifSlide(context, uri, at.getData().length, 0, 0, filename);
                    break;
                case AUDIO:
                    slide = new AudioSlide(context, uri, at.getData().length, false);
                    break;
                case VIDEO:
                    slide = new VideoSlide(context, uri, at.getData().length, filename);
                    break;
                case DOCUMENT:
                    slide = new DocumentSlide(context, uri, at.getMimeType(), at.getData().length, at.getFileName());
                    break;
            }

            sd.addSlide(slide);
        }


        return sd;
    }

    private static Uri writeFile(Context context, String filename, byte[] fileContent)
    {
        FileOutputStream outputStream;

        File f = null;
        try
        {
            f = new File(context.getFilesDir(), filename);

            outputStream = new FileOutputStream(f, false);
            outputStream.write(fileContent);
            outputStream.close();

            return Uri.fromFile(f);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
