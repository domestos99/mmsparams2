package cz.uhk.dip.mmsparams.android.pdus;

import android.content.Context;

import com.google.android.mms.pdu_alt.MyAcknowledgeInd;
import com.google.android.mms.pdu_alt.PduHeaders;

import org.thoughtcrime.securesms.mms.CompatMmsConnection;

import cz.uhk.dip.mmsparams.android.log.MmsErrorLogger;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;
import cz.uhk.dip.mmsparams.android.websocket.WebSocketPduHelper;

public class AcknowledgeIndHelper
{

    public static void sendAcknowledge(Context context, byte[] transactionId, long messageID, int subscriptionId) throws Exception
    {
        MyAcknowledgeInd ack = new MyAcknowledgeInd(PduHeaders.CURRENT_MMS_VERSION, transactionId);

        boolean allowDeliveryReport = MmsParamsPreferences.isMmsAllowSendDeliveryReport(context);
        ack.setReportAllowed(allowDeliveryReport ? PduHeaders.VALUE_YES : PduHeaders.VALUE_NO);

        byte[] nBytes = PduByteHelper.getPduBytes(context, ack);

        MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);
        mmsErrorLogger.setMessageID(messageID);

        final byte[] conf = new CompatMmsConnection(context, mmsErrorLogger).send(nBytes, subscriptionId);

        PduByteHelper.storeMessagePdu(context, messageID, PduHeaders.MESSAGE_TYPE_ACKNOWLEDGE_IND, nBytes);
        WebSocketPduHelper.handleAcknowledge(ack);
    }


}

