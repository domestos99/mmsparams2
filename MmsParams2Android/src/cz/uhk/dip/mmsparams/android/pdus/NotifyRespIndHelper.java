package cz.uhk.dip.mmsparams.android.pdus;

import android.content.Context;

import com.google.android.mms.pdu_alt.MyNotifyRespInd;
import com.google.android.mms.pdu_alt.PduHeaders;

import org.thoughtcrime.securesms.mms.CompatMmsConnection;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

import cz.uhk.dip.mmsparams.android.log.MmsErrorLogger;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;
import cz.uhk.dip.mmsparams.android.websocket.WebSocketPduHelper;

public class NotifyRespIndHelper
{
    private static final String TAG = NotifyRespIndHelper.class.getSimpleName();

    public enum RetrievedStatus
    {
        RETRIEVED,
        DEFERRED
    }

    public static void sendNotifyRespIndRetrieved(final Context context, final byte[] transactionId, final long messageID,
                                                  final int subscriptionId, final RetrievedStatus retrievedStatus) throws Exception
    {
        int status;

        switch (retrievedStatus)
        {
            case RETRIEVED:
                status = PduHeaders.STATUS_RETRIEVED;
                break;
            case DEFERRED:
                status = PduHeaders.STATUS_DEFERRED;
                break;

            default:
                status = PduHeaders.STATUS_RETRIEVED;
                break;
        }

        MyNotifyRespInd notifyResponse = new MyNotifyRespInd(PduHeaders.CURRENT_MMS_VERSION,
                transactionId,
                status);

        boolean allowDeliveryReport = MmsParamsPreferences.isMmsAllowSendDeliveryReport(context);
        notifyResponse.setReportAllowed(allowDeliveryReport ? PduHeaders.VALUE_YES : PduHeaders.VALUE_NO);


        byte[] nBytes = PduByteHelper.getPduBytes(context, notifyResponse);

        MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);
        mmsErrorLogger.setMessageID(messageID);

        final byte[] sendConf = new CompatMmsConnection(context, mmsErrorLogger).send(nBytes, subscriptionId);

        PduByteHelper.storeMessagePdu(context, messageID, PduHeaders.MESSAGE_TYPE_NOTIFYRESP_IND, nBytes);
        WebSocketPduHelper.handleNotifyResp(notifyResponse);
    }


}

