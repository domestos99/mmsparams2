package cz.uhk.dip.mmsparams.android.preferences;

import android.content.Context;
import android.preference.PreferenceManager;

import cz.uhk.dip.mmsparams.android.db.model.MmsProfile;
import cz.uhk.dip.mmsparams.android.profiles.MmsProfilesFactory;
import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;
import cz.uhk.dip.mmsparams.api.http.ServerAddressProvider;
import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.http.auth.JwtResponse;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;

public class MmsParamsPreferences
{
    private static final String DEFAULT_MMS_SETTING = "default_mms_setting";
    private static final String MMS_SETTING_PROFILE = "mms_setting_profile";
    private static final String API_SERVER_ADDRESS = "pref_api_server_address";
    private static final String API_USERNAME = "pref_api_username";
    private static final String API_PASSWORD = "pref_api_password";

    public static final String MMS_USE_DEFAULT_PROFILE = "pref_mms_use_default_profile";

    public static final String MMS_RECIPIENT_PHONE_PROFILE = "pref_mms_recipient_phone_profile";
    public static final String MMS_USE_DEFAULT_RECIPIENT_PROFILE = "pref_mms_use_default_recipient_profile";

    public static final String PREF_PHONE_CUSTOM_NAME = "pref_phone_custom_name";
    public static final String PREF_PHONE_NUMBER = "pref_phone_number";
    public static final String PREF_JWT_RESPONSE = "pref_jwt_response";
    public static final String PREF_SEND_KEEP_ALIVE_MESSAGE = "pref_send_keep_alive_message";
    public static final String WS_USE_HTTPS = "ws_use_https";

    public static boolean getBooleanPreference(Context context, String key, boolean defaultValue)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, defaultValue);
    }

    public static void setStringPreference(Context context, String key, String value)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).apply();
    }

    public static String getStringPreference(Context context, String key, String defaultValue)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, defaultValue);
    }

    public static void setRecipientProfile(Context context, MmsRecipientPhoneProfile mmsRecipientPhoneProfile)
    {
        setStringPreference(context, MMS_RECIPIENT_PHONE_PROFILE, JsonUtilsSafe.toJson(mmsRecipientPhoneProfile));
    }

    public static String getPhoneCustomName(Context context)
    {
        return getStringPreference(context, PREF_PHONE_CUSTOM_NAME, null);
    }

    public static void setPhoneCustomName(Context context, String phoneCustomName)
    {
        setStringPreference(context, PREF_PHONE_CUSTOM_NAME, phoneCustomName);
    }

    public static String getPhoneNumber(Context context)
    {
        return getStringPreference(context, PREF_PHONE_NUMBER, null);
    }

    public static void setPhoneNumber(Context context, String phoneNumber)
    {
        setStringPreference(context, PREF_PHONE_NUMBER, phoneNumber);
    }

    public static void setJwtResponse(Context context, JwtResponse response)
    {
        String json = JsonUtilsSafe.toJson(response);
        setStringPreference(context, PREF_JWT_RESPONSE, json);
    }

    public static String getApiServerAddress(Context context)
    {
        return getStringPreference(context, API_SERVER_ADDRESS, null);
    }

    public static String getApiUsername(Context context)
    {
        return getStringPreference(context, API_USERNAME, null);
    }

    public static String getApiPassword(Context context)
    {
        return getStringPreference(context, API_PASSWORD, null);
    }


    public static JwtResponse getJwtResponse(Context context)
    {
        String json = getStringPreference(context, PREF_JWT_RESPONSE, null);
        if (StringUtil.isEmptyOrNull(json))
            return null;
        return JsonUtilsSafe.fromJson(json, JwtResponse.class);
    }

    public static WSConnectSettings getWSConnectSettings(Context context)
    {
        ServerAddressProvider provider = new ServerAddressProvider(MmsParamsPreferences.getHttpProtocol(context), getApiServerAddress(context));
        return new WSConnectSettings(provider, getApiUsername(context), getApiPassword(context));
    }

    public static void setSendKeepAliveMessage(Context context, boolean checked)
    {
        setBooleanPreference(context, PREF_SEND_KEEP_ALIVE_MESSAGE, checked);
    }

    public static void setBooleanPreference(Context context, String key, boolean value)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(key, value).apply();
    }

    public static boolean getSendKeepAliveMessage(Context context)
    {
        return getBooleanPreference(context, PREF_SEND_KEEP_ALIVE_MESSAGE, true);
    }

    public static void setApiServerAddress(Context context, String apiServerAddress)
    {
        setStringPreference(context, API_SERVER_ADDRESS, apiServerAddress);
    }

    public static void setApiUsername(Context context, String username)
    {
        setStringPreference(context, API_USERNAME, username);
    }

    public static void setApiPassword(Context context, String password)
    {
        setStringPreference(context, API_PASSWORD, password);
    }

    public static MmsProfile getMmsSettingProfileObj(Context context)
    {
        String json = getMmsSettingProfile(context);

        MmsProfile ms = MmsProfilesFactory.getFromJsonOrDefault(context, json);
        return ms;
    }

    public static String getMaxAttachSize(Context context)
    {
        MmsProfile ms = getMmsSettingProfileObj(context);
        return ms.getMaxAttachSize();
    }

    public static void setDefaultMmsSettingJSON(Context context, String value)
    {
        setStringPreference(context, DEFAULT_MMS_SETTING, value);
    }

    public static String getMmsSettingProfile(Context context)
    {
        return getStringPreference(context, MMS_SETTING_PROFILE, null);
    }

    public static String getDefaultMmsSettingJSON(Context context)
    {
        return getStringPreference(context, DEFAULT_MMS_SETTING, null);
    }

    public static boolean isMmsAllowSendDeliveryReport(Context context)
    {
        MmsProfile ms = getMmsSettingProfileObj(context);
        return ms.isAllowSendDR();
    }

    public static MmsRecipientPhoneProfile getMmsRecipientPhoneProfile(Context context)
    {
        String jsonProfile = getStringPreference(context, MMS_RECIPIENT_PHONE_PROFILE, null);


        if (StringUtil.isEmptyOrNull(jsonProfile))
        {
            // Use default or none?
            if (getBooleanPreference(context, MMS_USE_DEFAULT_RECIPIENT_PROFILE, true))
            {
                return MmsRecipientPhoneProfile.createDefault();
            }
            else
                return null;
        }
        return JsonUtilsSafe.fromJson(jsonProfile, MmsRecipientPhoneProfile.class);
    }

    public static boolean isMmsAllowSendReadReport(Context context)
    {
        MmsProfile ms = getMmsSettingProfileObj(context);
        return ms.isAllowSendRR();
    }

    public static boolean isMmsSendReadReportEvenIfNotRequired(Context context)
    {
        MmsProfile ms = getMmsSettingProfileObj(context);
        return ms.isSendRRIfNotRequired();
    }

    public static boolean isAutoDownloadMms(Context context)
    {
        MmsProfile ms = getMmsSettingProfileObj(context);
        return ms.isAutoDownload();
    }

    public static void setUseHttps(Context context, boolean value)
    {
        setBooleanPreference(context, WS_USE_HTTPS, value);
    }

    public static boolean getUseHttps(Context context)
    {
        return getBooleanPreference(context, WS_USE_HTTPS, false);
    }

    public static HttpProtocolEnum getHttpProtocol(Context context)
    {
        return getUseHttps(context) ? HttpProtocolEnum.HTTPS : HttpProtocolEnum.HTTP;
    }

    public static String getHttpPrefix(Context context)
    {
       return getUseHttps(context) ? HttpConstants.URL_PREFIX_HTTPS : HttpConstants.URL_PREFIX_HTTP;
    }
}
