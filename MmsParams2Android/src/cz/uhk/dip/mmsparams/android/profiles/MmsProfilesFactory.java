package cz.uhk.dip.mmsparams.android.profiles;

import android.content.Context;

import com.google.android.mms.pdu_alt.PduHeaders;

import cz.uhk.dip.mmsparams.android.db.model.MmsProfile;
import cz.uhk.dip.mmsparams.android.pdus.PduHeaderConvertor;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;

public class MmsProfilesFactory
{

    public static final long DEFAULT_EXPIRY_TIME = 7 * 24 * 60 * 60;
    public static final long DEFAULT_DELIVERY_TIME = 0;
    public static final String DEFAULT_MAX_ATTACH_SIZE = PduHeaderConvertor.ATTACH_SIZE_300;

    public static MmsProfile getFromPref(Context context)
    {
        String defaultMmsSettingJSON = MmsParamsPreferences.getDefaultMmsSettingJSON(context);

        MmsProfileBLO blo = MmsProfileBLO.getFromJson(defaultMmsSettingJSON);
        if (blo == null)
        {
            MmsProfile setting = MmsProfilesFactory.getDefault();
            // Serialize to JSON

            setToPref(context, setting);

            return setting;
        }
        else
        {
            return getFromBlo(blo);
        }

    }

    private static MmsProfile getFromBlo(MmsProfileBLO blo)
    {
        return MmsProfileBLO.getMmsSetting(blo);
    }

    private static MmsProfileBLO getBLO(MmsProfile setting)
    {
        return MmsProfileBLO.getMmsBlo(setting);
    }

    public static void setToPref(Context context, MmsProfile mmsProfile)
    {
        String json = MmsProfileBLO.getJson(getBLO(mmsProfile));
        MmsParamsPreferences.setDefaultMmsSettingJSON(context, json);
    }

    public static MmsProfile getDefault()
    {
        MmsProfile ms = new MmsProfile("default", true, true,
                DEFAULT_EXPIRY_TIME, DEFAULT_DELIVERY_TIME,
                PduHeaders.PRIORITY_NORMAL, PduHeaders.MESSAGE_CLASS_PERSONAL, true, false,
                true, true, false, MmsProfile.DEFAULT_MAX_ATTACH_SIZE, true, true, false);

        return ms;
    }

    public static String getBloJson(MmsProfile mmsProfile)
    {
        return MmsProfileBLO.getJson(getBLO(mmsProfile));
    }

    public static MmsProfile getFromJson(String json)
    {
        MmsProfileBLO blo = MmsProfileBLO.getFromJson(json);
        if (blo == null)
        {
            return null;
        }
        else
        {
            return getFromBlo(blo);
        }
    }

    public static MmsProfile getFromJsonOrDefault(Context context, String json)
    {
        MmsProfile ms = getFromJson(json);
        if (ms == null)
        {
            ms = getFromPref(context);
        }
        return ms;
    }
}
