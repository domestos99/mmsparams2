package cz.uhk.dip.mmsparams.android.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;

public class AppUtils
{
    public static void makeDefaultSmsApp(Activity activity)
    {
        handleMakeDefaultSms(activity);
    }

    @Deprecated
    public static void makeDefaultSmsApp(final Context context)
    {
        Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
        intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, context.getPackageName());
        context.startActivity(intent);
    }

    private static final int SMS_DEFAULT = 10;


    private static void handleMakeDefaultSms(Activity activity)
    {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q)
//        {
//            RoleManager roleManager = activity.getSystemService(RoleManager.class);
//            // check if the app is having permission to be as default SMS app
//            boolean isRoleAvailable = roleManager.isRoleAvailable(RoleManager.ROLE_SMS);
//            if (isRoleAvailable)
//            {
//                // check whether your app is already holding the default SMS app role.
//                boolean isRoleHeld = roleManager.isRoleHeld(RoleManager.ROLE_SMS);
//                if (isRoleHeld)
//                {
//                    Intent roleRequestIntent = roleManager.createRequestRoleIntent(RoleManager.ROLE_SMS);
//                    activity.startActivityForResult(roleRequestIntent, SMS_DEFAULT);
//                }
//            }
//        }
//        else
//        {
            Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
            intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, activity.getPackageName());
            activity.startActivityForResult(intent, SMS_DEFAULT);
//        }
    }
}

