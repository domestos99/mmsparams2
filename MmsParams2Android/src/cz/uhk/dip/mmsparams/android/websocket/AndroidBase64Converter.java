package cz.uhk.dip.mmsparams.android.websocket;

import android.util.Base64;

import cz.uhk.dip.mmsparams.api.utils.IBase64Converter;

public class AndroidBase64Converter implements IBase64Converter
{
    @Override
    public String encodeToString(byte[] bytes)
    {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    @Override
    public byte[] decode(String s)
    {
        return Base64.decode(s, Base64.DEFAULT);
    }
}
