package cz.uhk.dip.mmsparams.android.websocket;

import okhttp3.WebSocket;
import okio.ByteString;

public interface IWebSocketMessageReceive
{
    void onMessage(WebSocket webSocket, String text);

    void onMessage(WebSocket webSocket, ByteString bytes);
}
