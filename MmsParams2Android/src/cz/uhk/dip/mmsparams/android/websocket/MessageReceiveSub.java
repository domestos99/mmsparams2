package cz.uhk.dip.mmsparams.android.websocket;

import android.content.Context;

import cz.uhk.dip.mmsparams.android.facades.WebSocketLoggerFacade;
import cz.uhk.dip.mmsparams.android.profile.ProfileHelper;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.listeners.IMessageReceiveSub;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.KeepAliveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.email.EmailReceiveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.TestErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsRecipientPhoneProfileRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.AcknowledgeIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.DeliveryIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotifyRespIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadOrigIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadRecIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendReqResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReportReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ReadReplyReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7SubmitResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneAllPartsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDisconnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;

public class MessageReceiveSub implements IMessageReceiveSub<WebSocketMessageBase>
{
    private final Context context;
    private MmsSendWebSocketHelper MmsSendWebSocketHelper;

    public MessageReceiveSub(Context context)
    {
        this.context = context;
    }

    @Override
    public WebSocketMessageBase onReceiveUnknown(String msg)
    {
        WebSocketLoggerFacade.logUnknownMessage(context, msg);
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(PhoneListRequestMessage msg)
    {
        return msg;
        // ignore
    }

    @Override
    public WebSocketMessageBase onReceive(RegisterPhoneMessage msg)
    {
        return msg;
        // ignore
    }

    @Override
    public WebSocketMessageBase onReceive(EmptyMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(PhoneListResponseMessage msg)
    {
        return msg;
        // ignore
    }

    @Override
    public WebSocketMessageBase onReceive(SmsSendPhoneRequestMessage msg)
    {
        SmsSendWebSocketHelper.process(context, msg);
        WebSocketUtils.sendBooleanResponse(msg, true);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(SmsReceivePhoneMessage msg)
    {
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(SmsReceivePhoneAllPartsMessage msg)
    {
        return msg;
        // ignore
    }

    @Override
    public WebSocketMessageBase onReceive(SmsSendPhoneResponseMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(SmsSendDeliveryReportMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(MmsSendPhoneRequestMessage msg)
    {
        MmsSendWebSocketHelper.process(context, msg);
        WebSocketUtils.sendBooleanResponse(msg, true);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(DevMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(MmsRecipientPhoneProfileRequestMessage msg)
    {
        ProfileHelper.setRecipientProfile(context, msg.getMmsRecipientPhoneProfile());
        WebSocketUtils.sendBooleanResponse(msg, true);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(GenericBooleanResponseMessage msg)
    {
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(GenericErrorResponseMessage msg)
    {
        return msg;

    }

    @Override
    public WebSocketMessageBase onReceive(SendConfResponseMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(RegisterClientLibMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(NotificationIndResponseMessage msg)
    {
        return msg;
        // ignore
    }

    @Override
    public WebSocketMessageBase onReceive(DeliveryIndResponseMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(ReadOrigIndResponseMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(AcknowledgeIndResponseMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(RetrieveConfResponseMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(ReadRecIndResponseMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(NotifyRespIndResponseMessage msg)
    {
        return msg;
// ignore
    }

    @Override
    public WebSocketMessageBase onReceive(MM7DeliveryReqMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(MM7DeliveryReportReqMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(MM7ReadReplyReqMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(SmscConnectMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(SmscSendSmsMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(SmscDeliverSmMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(SmscDisconnectMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(SmscDeliveryReportMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(SmscConnectResponseMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(SmscSendSmsResponseMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(UnLockPhoneMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(SendReqResponseMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(MmscSendMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(LockPhoneMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(LockedPhonesListResponseMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(LockedPhonesListRequestMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(MM7SubmitResponseMessage msg)
    {
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(MmscAcquireRouteRequestMessage msg)
    {
        return msg;
        // ignore
    }

    @Override
    public WebSocketMessageBase onReceive(TestValidationMessage msg)
    {
        return msg;
        // ignore
    }

    @Override
    public WebSocketMessageBase onReceive(TestErrorMessage msg)
    {
        return msg;
        // ignore
    }

    @Override
    public WebSocketMessageBase onReceive(MM7ErrorMessage msg)
    {
        return msg;
        // ignore
    }

    @Override
    public WebSocketMessageBase onReceive(KeepAliveMessage msg)
    {
        return msg;
        // ignore
    }

    @Override
    public WebSocketMessageBase onReceive(TestResultMessage msg)
    {
        return msg;
        // ignore
    }

    @Override
    public WebSocketMessageBase onReceive(EmailReceiveMessage msg)
    {
        return msg;
        // ignore
    }
}
