package cz.uhk.dip.mmsparams.android.websocket;

import android.content.Context;

import org.thoughtcrime.securesms.ApplicationContext;

import cz.uhk.dip.mmsparams.android.jobs.MmsSendWSProcess;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;

public class MmsSendWebSocketHelper
{
    private static final String TAG = MmsSendWebSocketHelper.class.getSimpleName();

    public static void process(Context context, MmsSendPhoneRequestMessage req)
    {
        // Request to send SMS
        ApplicationContext.getInstance(context)
                .getJobManager()
                .add(new MmsSendWSProcess(req));
    }


}
