package cz.uhk.dip.mmsparams.android.websocket;

import java.util.TimerTask;

import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.KeepAliveMessage;

public class SendAliveMessageTask extends TimerTask
{
    @Override
    public void run()
    {
        KeepAliveMessage msg = new MessageBuilder()
                .withMessageIdRandom()
                .withNoTestId()
                .withRecipientKeyServer()
                .build(KeepAliveMessage.class);
        WebSocketSingleton.getInstance().sendMessage(msg, false);
    }
}
