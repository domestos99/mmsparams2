package cz.uhk.dip.mmsparams.android.websocket;

import cz.uhk.dip.mmsparams.api.exceptions.SystemException;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;

public class WebSocketErrorFactory
{
    public static void sendError(String tag, Exception ex, WebSocketMessageBase request, String info)
    {
        // TODO Check all header parameters
        GenericErrorResponseMessage genericErrorResponseMessage = MessageFactory.createGenericErrorResponseMessage(tag, new SystemException(ex), request, info);
        genericErrorResponseMessage = MessageUtils.prepareResponse(genericErrorResponseMessage, request);
        WebSocketFacade.send(genericErrorResponseMessage, true);
//        WebSocketFacade.sendResposne(genericErrorResponseMessage, request);
    }

    public static void sendError(String tag, Exception ex, String messageID, String info)
    {
        // TODO Check all header parameters
        GenericErrorResponseMessage genericErrorResponseMessage = MessageFactory.createGenericErrorResponseMessage(tag, new SystemException(ex), messageID, info);
        WebSocketFacade.sendClientLibBroadcast(genericErrorResponseMessage, true);
    }
}
