package cz.uhk.dip.mmsparams.android.websocket;

import android.telephony.SmsMessage;

import com.google.android.mms.pdu_alt.MyAcknowledgeInd;
import com.google.android.mms.pdu_alt.MyDeliveryInd;
import com.google.android.mms.pdu_alt.MyNotificationInd;
import com.google.android.mms.pdu_alt.MyNotifyRespInd;
import com.google.android.mms.pdu_alt.MyReadOrigInd;
import com.google.android.mms.pdu_alt.MyReadRecInd;
import com.google.android.mms.pdu_alt.MyRetrieveConf;
import com.google.android.mms.pdu_alt.MySendConf;
import com.google.android.mms.pdu_alt.MySendReq;

import java.util.ArrayList;

import cz.uhk.dip.mmsparams.android.pdus.PduModelConvertor;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.AcknowledgeIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.DeliveryIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotifyRespIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadOrigIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadRecIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendReqResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneAllPartsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;

public class WebSocketPduHelper
{
    public static void handleSendSendConf(MySendConf sendConfObj, MmsSendPhoneRequestMessage request)
    {
        SendConfResponseMessage sendConfResponseMessage = MessageFactory.create(SendConfResponseMessage.class);

        SendConfModel sendConfModel = PduModelConvertor.GetModel(sendConfObj);

        sendConfResponseMessage.setSendConfModel(sendConfModel);

        sendConfResponseMessage = MessageUtils.prepareResponse(sendConfResponseMessage, request);

        WebSocketFacade.send(sendConfResponseMessage, true);
    }

    public static void handleSendSendReq(MySendReq pdu, MmsSendPhoneRequestMessage request)
    {
        SendReqResponseMessage sendReqResponseMessage = MessageFactory.create(SendReqResponseMessage.class);
        SendReqModel sendReqModel = PduModelConvertor.GetModel(pdu);

        sendReqResponseMessage.setSendReqModel(sendReqModel);

        sendReqResponseMessage = MessageUtils.prepareResponse(sendReqResponseMessage, request);

        WebSocketFacade.send(sendReqResponseMessage, true);
    }


    public static void handleSendNotificationInd(MyNotificationInd pduNotificationInd)
    {
        NotificationIndResponseMessage message = MessageFactory.createClientBroadcastMessage(NotificationIndResponseMessage.class);
        message.setNotificationIndModel(PduModelConvertor.GetModel(pduNotificationInd));
        WebSocketFacade.sendClientLibBroadcast(message, false);
    }

    public static void handleSendDeliveryInd(MyDeliveryInd deliveryInd)
    {
        DeliveryIndResponseMessage message = MessageFactory.createClientBroadcastMessage(DeliveryIndResponseMessage.class);
        message.setDeliveryIndModel(PduModelConvertor.GetModel(deliveryInd));
        WebSocketFacade.sendClientLibBroadcast(message, false);
    }

    public static void handleSendReadOrigInd(MyReadOrigInd readOrigInd)
    {
        ReadOrigIndResponseMessage message = MessageFactory.createClientBroadcastMessage(ReadOrigIndResponseMessage.class);
        message.setReadOrigIndModel(PduModelConvertor.GetModel(readOrigInd));
        WebSocketFacade.sendClientLibBroadcast(message, false);
    }

    public static void handleAcknowledge(MyAcknowledgeInd ack)
    {
        AcknowledgeIndResponseMessage message = MessageFactory.createClientBroadcastMessage(AcknowledgeIndResponseMessage.class);
        message.setAcknowledgeIndModel(PduModelConvertor.GetModel(ack));
        WebSocketFacade.sendClientLibBroadcast(message, false);
    }

    public static void handleRetrieveConf(MyRetrieveConf retrieved)
    {
        RetrieveConfResponseMessage message = MessageFactory.createClientBroadcastMessage(RetrieveConfResponseMessage.class);
        message.setRetrieveConfModel(PduModelConvertor.GetModel(retrieved));
        WebSocketFacade.sendClientLibBroadcast(message, false);
    }

    public static void handleReadRecInf(MyReadRecInd readRec)
    {
        ReadRecIndResponseMessage message = MessageFactory.createClientBroadcastMessage(ReadRecIndResponseMessage.class);
        message.setReadRecIndModel(PduModelConvertor.GetModel(readRec));
        WebSocketFacade.sendClientLibBroadcast(message, false);
    }

    public static void handleNotifyResp(MyNotifyRespInd notifyResponse)
    {
        NotifyRespIndResponseMessage message = MessageFactory.createClientBroadcastMessage(NotifyRespIndResponseMessage.class);
        message.setNotifyRespIndModel(PduModelConvertor.GetModel(notifyResponse));
        WebSocketFacade.sendClientLibBroadcast(message, false);
    }

    public static void handleSmsReceive(Object[] pdus, String format)
    {
        ArrayList<String> bodys = new ArrayList<>();

        SmsMessage smsMessage = null;
        for (Object pdu : pdus)
        {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
            {
                SmsMessage msg = SmsMessage.createFromPdu((byte[]) pdu, format);
                bodys.add(msg.getDisplayMessageBody());
                smsMessage = msg;
            }
            else
            {
                SmsMessage msg = SmsMessage.createFromPdu((byte[]) pdu);
                bodys.add(msg.getDisplayMessageBody());
                smsMessage = msg;
            }
        }

        SmsReceivePhoneMessage smsReceivePhoneMessage = MessageFactory.createClientBroadcastMessage(SmsReceivePhoneMessage.class);

        smsReceivePhoneMessage.setSmsReceiveModel(PduModelConvertor.GetModel(smsMessage, bodys));

        WebSocketFacade.sendClientLibBroadcast(smsReceivePhoneMessage, false);


        handleSmsReceiveAllParts(pdus, format);


    }

    private static void handleSmsReceiveAllParts(Object[] pdus, String format)
    {
        ArrayList<String> bodys = new ArrayList<>();
        ArrayList<SmsMessage> smsMessage = new ArrayList<>();

        for (Object pdu : pdus)
        {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
            {
                SmsMessage msg = SmsMessage.createFromPdu((byte[]) pdu, format);
                bodys.add(msg.getDisplayMessageBody());
                smsMessage.add(msg);
            }
            else
            {
                SmsMessage msg = SmsMessage.createFromPdu((byte[]) pdu);
                bodys.add(msg.getDisplayMessageBody());
                smsMessage.add(msg);
            }
        }

        SmsReceivePhoneAllPartsMessage smsReceivePhoneMessage = MessageFactory.createClientBroadcastMessage(SmsReceivePhoneAllPartsMessage.class);

        smsReceivePhoneMessage.setSmsReceiveAllPartsModel(PduModelConvertor.GetModel(smsMessage, bodys));

        WebSocketFacade.sendClientLibBroadcast(smsReceivePhoneMessage, false);
    }


    public static SmsDeliveryReport getSmsDeliveryReportModel(byte[] pdu, String format)
    {
        SmsDeliveryReport dr = new SmsDeliveryReport();


        SmsMessage msg;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
        {
            msg = SmsMessage.createFromPdu(pdu, format);
        }
        else
        {
            msg = SmsMessage.createFromPdu(pdu);
        }
        if (msg == null)
            return dr;

        PduModelConvertor.fillSmsDeliveryReport(dr, msg);

        return dr;
    }



}
