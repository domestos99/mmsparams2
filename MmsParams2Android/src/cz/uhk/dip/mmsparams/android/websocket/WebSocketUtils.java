package cz.uhk.dip.mmsparams.android.websocket;

import android.app.Activity;
import android.content.Context;
import android.telephony.SmsManager;

import org.thoughtcrime.securesms.logging.Log;

import cz.uhk.dip.mmsparams.android.devicekey.DeviceIdSingleton;
import cz.uhk.dip.mmsparams.android.gui.ApiInterfaceSettingsFragment;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;
import cz.uhk.dip.mmsparams.api.connections.AndroidConnectBuilder;
import cz.uhk.dip.mmsparams.api.connections.AndroidConnectModel;
import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.utils.UrlUtils;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketConstants;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;


public class WebSocketUtils
{
    private static final String TAG = WebSocketUtils.class.getSimpleName();

    public static void sendBooleanResponse(WebSocketMessageBase req, boolean value)
    {

        GenericBooleanResponseMessage res = MessageFactory.create(GenericBooleanResponseMessage.class);

        res.setValue(value);

//        WebSocketFacade.sendResposne(res, req);
        res = MessageUtils.prepareResponse(res, req);

        WebSocketFacade.send(res, true);

    }

    public static String getWebSocketAddress(final Context context)
    {
        return UrlUtils.getServerSocketUrl(MmsParamsPreferences.getHttpProtocol(context), MmsParamsPreferences.getApiServerAddress(context));
    }

    public static void connect(final Context context, String connectMessage) throws Exception
    {
        AndroidConnectModel model = AndroidConnectBuilder.parse(new AndroidBase64Converter(), connectMessage);
        MmsParamsPreferences.setApiServerAddress(context, model.getAddress());

        MmsParamsPreferences.setUseHttps(context, model.isUseHttps());

        if (!StringUtil.isEmptyOrNull(model.getName()))
            MmsParamsPreferences.setPhoneCustomName(context, model.getName());
        if (model.getJwtRequest() != null)
        {
            MmsParamsPreferences.setApiUsername(context, model.getJwtRequest().getUsername());
            MmsParamsPreferences.setApiPassword(context, model.getJwtRequest().getPassword());
        }

        connect(context);
    }

    public static String connect(final Context context) throws Exception
    {
        // openFragment(new WebSocketTestFragment());
        if (!checkRequirements(context))
        {
            return "Requirements not fulfilled";
        }

        DeviceIdSingleton.getInstance().init(context);
        final String serverAddress = WebSocketUtils.getWebSocketAddress(context);
        // TODO refactor to more buttons and more options

        if (WebSocketSingleton.getInstance().isSocketOpen())
        {
            WebSocketSingleton.getInstance().disconnect();
            try
            {
                Thread.sleep(2000);
            } catch (InterruptedException e)
            {
                Log.e(TAG, e);
            }
        }

        WebSocketSingleton.getInstance().init(context, serverAddress);
        try
        {
            Thread.sleep(2000);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        registerPhone(context);
        return "OK";
    }

    private static void registerPhone(final Context context)
    {
        // registrace zarizeni
        RegisterPhoneMessage registerPhoneMessage = MessageFactory.createRegisterPhoneMessage(new DeviceHelper(context));
        WebSocketFacade.sendToServer(registerPhoneMessage, false);
    }

    private static boolean checkRequirements(Context applicationContext)
    {
        return ApiInterfaceSettingsFragment.areSettingsOk(applicationContext) && ApiInterfaceSettingsFragment.checkSystemVersionBuild(applicationContext);
    }

    public static void sendSmsDelivery(String messageRequestID, String testId, String messageSenderKey, byte[] pdu, String format)
    {
        SmsSendDeliveryReportMessage smsSendDeliveryResponseMessage = MessageFactory.create(SmsSendDeliveryReportMessage.class);

        smsSendDeliveryResponseMessage.setMessageID(messageRequestID);

        if (StringUtil.isEmptyOrNull(messageSenderKey))
            smsSendDeliveryResponseMessage.setRecipientKey(WebSocketConstants.Server_To_Client_Broadcast);
        else
            smsSendDeliveryResponseMessage.setRecipientKey(messageSenderKey);

        smsSendDeliveryResponseMessage.setTestID(testId);

        final SmsDeliveryReport smsDR = WebSocketPduHelper.getSmsDeliveryReportModel(pdu, format);

        smsSendDeliveryResponseMessage.setSmsDeliveryReport(smsDR);

        WebSocketFacade.send(smsSendDeliveryResponseMessage, true);
    }

    public static void sendSmsResponseOk(int result, String messageRequestID, String messageSenderKey, String testId)
    {
        SmsSendPhoneResponseMessage smsSendPhoneresponseMessage = MessageFactory.create(SmsSendPhoneResponseMessage.class);

        smsSendPhoneresponseMessage.setMessageID(messageRequestID);

        if (StringUtil.isEmptyOrNull(messageSenderKey))
            smsSendPhoneresponseMessage.setRecipientKey(WebSocketConstants.Server_To_Client_Broadcast);
        else
            smsSendPhoneresponseMessage.setRecipientKey(messageSenderKey);

        smsSendPhoneresponseMessage.setTestID(testId);

        SmsSendResponseModel responseModel = new SmsSendResponseModel();
        responseModel.setResult(result);
        responseModel.setResultText(getSmsResultStatusText(result));

        smsSendPhoneresponseMessage.setSmsSendResponseModel(responseModel);

        WebSocketFacade.send(smsSendPhoneresponseMessage, true);
    }

    public static String getSmsResultStatusText(int result)
    {
        switch (result)
        {
            case Activity.RESULT_OK:
                return "OK";
            case SmsManager.RESULT_ERROR_NULL_PDU:
                return "RESULT_ERROR_NULL_PDU";
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                return "RESULT_ERROR_NO_SERVICE";
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                return "RESULT_ERROR_RADIO_OFF";
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                return "RESULT_ERROR_GENERIC_FAILURE";
        }
        return String.valueOf(result);
    }

    public static boolean handleRemoteConnectMessage(final Context context, final String messageBody)
    {
        if (StringUtil.isEmptyOrNull(messageBody))
            return false;

        if (messageBody.startsWith(AndroidConnectBuilder.PREFIX))
        {
            try
            {
                WebSocketUtils.connect(context, messageBody);
            } catch (Exception e)
            {
                e.printStackTrace();
                Log.e(TAG, "handleConnectMessage", e);
            }


            return true;
        }
        return false;
    }
}
