package org.thoughtcrime.securesms.mms;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.mms.pdu_alt.RetrieveConf;

import java.io.IOException;

public interface IncomingMmsConnection
{
    @Nullable
    byte[] retrieve(@NonNull String contentLocation, byte[] transactionId, long messageID, int subscriptionId) throws MmsException, MmsRadioException, ApnUnavailableException, IOException;
}
