package org.thoughtcrime.securesms.mmsparams;

import org.junit.Test;

import cz.uhk.dip.mmsparams.android.websocket.AndroidBase64Converter;
import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.utils.SimplePasswordUtil;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class SimplePasswordUtilTest
{

    @Test
    public void encrypt_Decrypt_Test()
    {
        String username = "abc";
        String password = "yxz";
        JwtRequest jwtRequest = new JwtRequest(username, password);

        String encrypt = SimplePasswordUtil.encrypt(new AndroidBase64Converter(), jwtRequest);
        assertFalse(StringUtil.isEmptyOrNull(encrypt));

        JwtRequest decrypt = SimplePasswordUtil.decrypt(new AndroidBase64Converter(), encrypt);

        assertEquals(username, decrypt.getUsername());
        assertEquals(password, decrypt.getPassword());
    }

    @Test
    public void encrypt_null_Test()
    {

        String encrypt = SimplePasswordUtil.encrypt(new AndroidBase64Converter(), new JwtRequest("abc", null));
        assertNull(encrypt);

        encrypt = SimplePasswordUtil.encrypt(new AndroidBase64Converter(), new JwtRequest(null, "xyz"));
        assertNull(encrypt);

        encrypt = SimplePasswordUtil.encrypt(new AndroidBase64Converter(), new JwtRequest(null, null));
        assertNull(encrypt);
    }

    @Test
    public void decrypt_nonsence_Test()
    {
        JwtRequest result = SimplePasswordUtil.decrypt(new AndroidBase64Converter(), "abcd");
        assertNotNull(result);
        assertNull(result.getUsername());
        assertNull(result.getPassword());
    }

    @Test
    public void decrypt_null_Test()
    {
        JwtRequest result = SimplePasswordUtil.decrypt(new AndroidBase64Converter(), null);
        assertNotNull(result);
        assertNull(result.getUsername());
        assertNull(result.getPassword());
    }

}

