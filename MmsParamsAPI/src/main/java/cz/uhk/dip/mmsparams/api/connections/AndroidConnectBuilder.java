package cz.uhk.dip.mmsparams.api.connections;

import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.utils.IBase64Converter;
import cz.uhk.dip.mmsparams.api.utils.SimplePasswordUtil;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;

public class AndroidConnectBuilder
{
    private AndroidConnectBuilder()
    {
    }

    public static final String PREFIX = "MMSPARAMS_CONNECT_WS";

    /**
     * Create SMS message text for remote phone connect
     *
     * @param address         server address
     * @param useHttps        use HTTPS connection
     * @param customPhoneName custom phone name to set
     * @param jwtRequest      username and password for authentication
     * @return SMS message with remote connect info
     */
    public static String createConnectMessage(IBase64Converter iBase64Converter, String address, boolean useHttps,
                                              String customPhoneName, JwtRequest jwtRequest)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(PREFIX);
        sb.append(";");
        sb.append(address);
        sb.append(";");
        sb.append(useHttps);
        sb.append(";");
        if (!StringUtil.isEmptyOrNull(customPhoneName))
            sb.append(customPhoneName);
        String auth = SimplePasswordUtil.encrypt(iBase64Converter, jwtRequest);
        sb.append(";");
        if (!StringUtil.isEmptyOrNull(auth))
            sb.append(auth);

        return sb.toString();
    }

    public static AndroidConnectModel parse(IBase64Converter iBase64Converter, String connectionText)
    {
        String[] params = connectionText.split(";");
        JwtRequest jwtRequest = SimplePasswordUtil.decrypt(iBase64Converter, params[4]);
        return new AndroidConnectModel(params[1], Boolean.parseBoolean(params[2]), params[3], jwtRequest);
    }
}
