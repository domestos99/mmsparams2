package cz.uhk.dip.mmsparams.api.connections;

import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;

public class AndroidConnectModel
{
    private String address;
    private boolean useHttps;
    private String name;
    private JwtRequest jwtRequest;

    public AndroidConnectModel()
    {
    }

    public AndroidConnectModel(String address, boolean useHttps, String name, JwtRequest jwtRequest)
    {
        this.address = address;
        this.useHttps = useHttps;
        this.name = name;
        this.jwtRequest = jwtRequest;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public boolean isUseHttps()
    {
        return useHttps;
    }

    public void setUseHttps(boolean useHttps)
    {
        this.useHttps = useHttps;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public JwtRequest getJwtRequest()
    {
        return jwtRequest;
    }

    public void setJwtRequest(JwtRequest jwtRequest)
    {
        this.jwtRequest = jwtRequest;
    }

    @Override
    public String toString()
    {
        return "AndroidConnectModel{" +
                "address='" + address + '\'' +
                ", forceReconect=" + useHttps +
                ", name='" + name + '\'' +
                ", jwtRequest=" + jwtRequest +
                '}';
    }
}
