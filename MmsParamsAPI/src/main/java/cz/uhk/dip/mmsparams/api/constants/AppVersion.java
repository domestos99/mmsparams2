package cz.uhk.dip.mmsparams.api.constants;

public class AppVersion
{
    private AppVersion()
    {
    }

    public static final String SYSTEM_VERSION = "1.0.3.10";
    public static final String SYSTEM_BUILD = "d3b3ce0c-5bd6-4313-bd75-6390cdf281a4";
}
