package cz.uhk.dip.mmsparams.api.constants;

public class ContentTypeConstants
{
    public static final String PLAIN_TEXT = "text/plain";
    public static final String APPLICATION_SMIL = "application/smil";
}
