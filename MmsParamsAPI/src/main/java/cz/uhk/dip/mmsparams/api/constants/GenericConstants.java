package cz.uhk.dip.mmsparams.api.constants;

public class GenericConstants
{


    public static final String CLIENT_INFO = "clientInfo";
    public static final String SMSC_SESSION_ID = "smscSessionId";
    public static final String TIMEOUT = "timeout";
    public static final String EXPECTED_COUNT = "expectedCount";
    public static final String CLZ = "clz";
    public static final String PHONE_KEY = "phoneKey";
    public static final String PHONE_INFO_MODEL = "phoneInfoModel";
    public static final String MESSAGE_ID = "messageID";
    public static final String WAIT_FOR_RESPONSE_MESSAGE = "waitForResponseMessage";
    public static final String TEST_ID = "testID";
    public static final String CLIENT_KEY = "clientKey";
    public static final String SENDER_KEY = "senderKey";
    public static final String RECIPIENT_KEY = "recipientKey";
    public static final String PAYLOAD = "payload";
    public static final String SESSION_ID = "sessionId";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String PHONE_CUSTOM_NAME = "phoneCustomName";
    public static final String DUMMY = "dummy";
    public static final String SMS_SEND_REQ_ID = "smsSendReqId";
    public static final String VALIDATION_ITEM = "validationItem";
    public static final String VALIDATION_ITEMS = "validationItems";
    public static final String VALIDATION_RESULT = "validationResult";
    public static final String MMS = "mms";
    public static final String PHONE_SENDER = "phoneSender";
    public static final String PHONE_RECIPIENT = "phoneRecipient";
    public static final String MMS_SEND_REQ_ID = "mmsSendReqId";
    public static final String MMS_RECIPIENT_PHONE_PROFILE = "mmsRecipientPhoneProfile";
    public static final String MMS_DOWNLOAD_OPTIONS_MODEL = "mmsDownloadOptionsModel";
    public static final String MMS_READ_REPORT_OPTIONS_MODEL = "mmsReadReportOptionsModel";
    public static final String MMS_SEND_MODEL = "mmsSendModel";
    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String CUSTOM_NAME = "customName";
    public static final String IMSI = "imsi";
    public static final String PHONE_TARGET_NUMBER = "phoneTargetNumber";
    public static final String PHONE_TARGET_CUSTOM_NAME = "phoneTargetCustomName";
    public static final String ACQUIRE_ROUTE_MODEL = "acquireRouteModel";
    public static final String SMSC_SEND_SMS_MODEL = "smscSendSmsModel";
    public static final String SMSC_CONNECT_MODEL = "smscConnectModel";
    public static final String HOST = "host";
    public static final String PORT = "port";
    public static final String SYSTEM_ID = "systemId";
    public static final String PASSWORD = "password";
    public static final String SYSTEM_TYPE = "systemType";
    public static final String CONN_TYPE = "connType";
    public static final String RECIPIENT_ADDRESS = "recipientAddress";
    public static final String SENDER_ADDRESS = "senderAddress";
    public static final String SMS_SEND_MESSAGE_ID = "smsSendMessageId";
    public static final String SMS = "sms";
    public static final String TEST_SETTINGS = "testSettings";
    public static final String SERVER_ADDRESS_PROVIDER = "serverAddressProvider";
    public static final String DEFAULT_WAIT_SLEEP_LOOP = "defaultWaitSleepLoop";
    public static final String LONG_TIMEOUT = "longTimeout";
    public static final String TEST = "test";
    public static final String CLIENT_LIB_INSTANCE = "clientLibInstance";
    public static final String SESSION = "session";
    public static final String MESSAGE = "message";
    public static final String OPEN = "open";
    public static final String VALIDATION_BUILDER = "validation_builder";
    public static final String PATH = "path";
    public static final String FILE = "file";
    public static final String FILE_EXTENSION = "file_extension";
    public static final String CUSTOM_LOGGER = "custom_logger";
    public static final String LIST = "list";
    public static final String SEND_AFTER_SECONDS = "sendAfterSeconds";
    public static final String READ_STATUS = "readStatus";
    public static final String USERNAME = "username";
    public static final String JWT_RESPONSE = "JwtResponse";
    public static final String JWTTOKEN = "jwt_token";
    public static final String EXPIRY = "expiry";
    public static final String TEXT_LENGHT = "text_lenght";
    public static final String ADDRESS = "address";

    private GenericConstants()
    {
    }
}
