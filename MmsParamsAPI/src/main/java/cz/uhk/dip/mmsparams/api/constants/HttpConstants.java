package cz.uhk.dip.mmsparams.api.constants;

import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;

public class HttpConstants
{
    public static final String AUTHORIZATION = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";

    public static final String URL_PREFIX_HTTP = "http://";
    public static final String URL_PREFIX_HTTPS = "https://";
    public static final String WS_PREFIX = "ws://";
    public static final String WSS_PREFIX = "wss://";


    private HttpConstants()
    {
    }

    public static String getAuthHeaderValue(String token)
    {
        return TOKEN_PREFIX + token;
    }

    public static String getHttpPrefix(HttpProtocolEnum httpProtocolEnum)
    {
        if (httpProtocolEnum == null)
            return URL_PREFIX_HTTP;
        switch (httpProtocolEnum)
        {
            case HTTP:
                return URL_PREFIX_HTTP;
            case HTTPS:
                return URL_PREFIX_HTTPS;
            default:
                return URL_PREFIX_HTTP;
        }
    }

    public static String getWsPrefix(HttpProtocolEnum httpProtocolEnum)
    {
        if (httpProtocolEnum == null)
            return WS_PREFIX;
        switch (httpProtocolEnum)
        {
            case HTTP:
                return WS_PREFIX;
            case HTTPS:
                return WSS_PREFIX;
            default:
                return WS_PREFIX;
        }
    }
}

