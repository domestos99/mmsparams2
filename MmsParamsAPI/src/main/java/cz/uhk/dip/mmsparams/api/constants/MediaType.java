package cz.uhk.dip.mmsparams.api.constants;

public enum MediaType
{
    IMAGE, LOCATION, GIF, AUDIO, VIDEO, DOCUMENT
}
