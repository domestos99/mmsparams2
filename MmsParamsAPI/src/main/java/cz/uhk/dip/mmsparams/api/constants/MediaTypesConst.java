package cz.uhk.dip.mmsparams.api.constants;

public class MediaTypesConst
{
    private MediaTypesConst()
    {
    }
    // book (business) 11.3.7
    // "application/vnd.wap.mmsmessage"

    // OMA 5. Message Structure Overview
    // application/vnd.wap.multipart.related

    public static final String JSON = "application/json";
}
