package cz.uhk.dip.mmsparams.api.constants;

public class MemoryConstants
{
    private MemoryConstants()
    {
    }

    public static final int KB32 = 32768;
    public static final int MB16 = 16777216;
}
