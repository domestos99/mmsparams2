package cz.uhk.dip.mmsparams.api.constants;

public class PduHeaders2
{
    private PduHeaders2()
    {
    }

    /**
     * X-Mms-Delivery-Report |
     * X-Mms-Read-Report |
     * X-Mms-Report-Allowed |
     * X-Mms-Sender-Visibility |
     * X-Mms-Store |
     * X-Mms-Stored |
     * X-Mms-Totals |
     * X-Mms-Quotas |
     * X-Mms-Distribution-Indicator |
     * X-Mms-DRM-Content |
     * X-Mms-Adaptation-Allowed |
     * field types.
     */
    public static final int VALUE_YES = 0x80;
    public static final int VALUE_NO = 0x81;

    /**
     * X-Mms-Message-Class field types.
     */
    public static final int MESSAGE_CLASS_PERSONAL = 0x80;
    public static final int MESSAGE_CLASS_ADVERTISEMENT = 0x81;
    public static final int MESSAGE_CLASS_INFORMATIONAL = 0x82;
    public static final int MESSAGE_CLASS_AUTO = 0x83;
    public static final String MESSAGE_CLASS_PERSONAL_STR = "personal";
    public static final String MESSAGE_CLASS_ADVERTISEMENT_STR = "advertisement";
    public static final String MESSAGE_CLASS_INFORMATIONAL_STR = "informational";
    public static final String MESSAGE_CLASS_AUTO_STR = "auto";


    /**
     * X-Mms-Priority field types.
     */
    public static final int PRIORITY_LOW = 0x80;
    public static final int PRIORITY_NORMAL = 0x81;
    public static final int PRIORITY_HIGH = 0x82;


    /**
     * X-Mms-Status Field.
     */
    public static final int STATUS_EXPIRED = 0x80;
    public static final int STATUS_RETRIEVED = 0x81;
    public static final int STATUS_REJECTED = 0x82;
    public static final int STATUS_DEFERRED = 0x83;
    public static final int STATUS_UNRECOGNIZED = 0x84;
    public static final int STATUS_INDETERMINATE = 0x85;
    public static final int STATUS_FORWARDED = 0x86;
    public static final int STATUS_UNREACHABLE = 0x87;


    /**
     * X-Mms-Response-Status field types.
     */
    public static final int RESPONSE_STATUS_OK = 0x80;
    public static final int RESPONSE_STATUS_ERROR_UNSPECIFIED = 0x81;
    public static final int RESPONSE_STATUS_ERROR_SERVICE_DENIED = 0x82;
    public static final int RESPONSE_STATUS_ERROR_MESSAGE_FORMAT_CORRUPT = 0x83;
    public static final int RESPONSE_STATUS_ERROR_SENDING_ADDRESS_UNRESOLVED = 0x84;
    public static final int RESPONSE_STATUS_ERROR_MESSAGE_NOT_FOUND = 0x85;
    public static final int RESPONSE_STATUS_ERROR_NETWORK_PROBLEM = 0x86;
    public static final int RESPONSE_STATUS_ERROR_CONTENT_NOT_ACCEPTED = 0x87;
    public static final int RESPONSE_STATUS_ERROR_UNSUPPORTED_MESSAGE = 0x88;
    public static final int RESPONSE_STATUS_ERROR_TRANSIENT_FAILURE = 0xC0;
    public static final int RESPONSE_STATUS_ERROR_TRANSIENT_SENDNG_ADDRESS_UNRESOLVED = 0xC1;
    public static final int RESPONSE_STATUS_ERROR_TRANSIENT_MESSAGE_NOT_FOUND = 0xC2;
    public static final int RESPONSE_STATUS_ERROR_TRANSIENT_NETWORK_PROBLEM = 0xC3;
    public static final int RESPONSE_STATUS_ERROR_TRANSIENT_PARTIAL_SUCCESS = 0xC4;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_FAILURE = 0xE0;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_SERVICE_DENIED = 0xE1;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_MESSAGE_FORMAT_CORRUPT = 0xE2;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_SENDING_ADDRESS_UNRESOLVED = 0xE3;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_MESSAGE_NOT_FOUND = 0xE4;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_CONTENT_NOT_ACCEPTED = 0xE5;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_LIMITATIONS_NOT_MET = 0xE6;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_REQUEST_NOT_ACCEPTED = 0xE6;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_FORWARDING_DENIED = 0xE8;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_NOT_SUPPORTED = 0xE9;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_ADDRESS_HIDING_NOT_SUPPORTED = 0xEA;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_LACK_OF_PREPAID = 0xEB;
    public static final int RESPONSE_STATUS_ERROR_PERMANENT_END = 0xFF;


    /**
     * X-Mms-Retrieve-Status field types.
     */
    public static final int RETRIEVE_STATUS_OK = 0x80;
    public static final int RETRIEVE_STATUS_ERROR_TRANSIENT_FAILURE = 0xC0;
    public static final int RETRIEVE_STATUS_ERROR_TRANSIENT_MESSAGE_NOT_FOUND = 0xC1;
    public static final int RETRIEVE_STATUS_ERROR_TRANSIENT_NETWORK_PROBLEM = 0xC2;
    public static final int RETRIEVE_STATUS_ERROR_PERMANENT_FAILURE = 0xE0;
    public static final int RETRIEVE_STATUS_ERROR_PERMANENT_SERVICE_DENIED = 0xE1;
    public static final int RETRIEVE_STATUS_ERROR_PERMANENT_MESSAGE_NOT_FOUND = 0xE2;
    public static final int RETRIEVE_STATUS_ERROR_PERMANENT_CONTENT_UNSUPPORTED = 0xE3;
    public static final int RETRIEVE_STATUS_ERROR_END = 0xFF;


    /**
     * X-Mms-MMS-Version field types.
     */
    public static final int MMS_VERSION_1_3 = 19;
    public static final int MMS_VERSION_1_2 = 18;
    public static final int MMS_VERSION_1_1 = 17;
    public static final int MMS_VERSION_1_0 = 16;

}
