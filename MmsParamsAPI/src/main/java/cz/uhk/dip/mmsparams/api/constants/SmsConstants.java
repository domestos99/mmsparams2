package cz.uhk.dip.mmsparams.api.constants;

public class SmsConstants
{
    private SmsConstants()
    {
    }

    public static final int RESULT_CANCELED = 0;
    public static final int RESULT_FIRST_USER = 1;
    public static final int RESULT_OK = -1;
}
