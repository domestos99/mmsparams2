package cz.uhk.dip.mmsparams.api.constants;

import java.io.Serializable;

public enum SmsEncoding implements Serializable
{
    UTF8,
    UTF7,
    BIT7
}
