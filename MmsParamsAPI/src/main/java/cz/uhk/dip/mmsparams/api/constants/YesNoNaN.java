package cz.uhk.dip.mmsparams.api.constants;

import java.io.Serializable;

public enum YesNoNaN implements Serializable
{
    YES("YES"),
    NO("NO"),
    NAN("NAN");

    private String value;

    YesNoNaN(String value)
    {
        this.value = value;
    }

    public String value()
    {
        return value;
    }
}
