package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum AddressCoding implements Serializable
{
    ENCRYPTED, OBFUSCATED;

    @Override
    public String toString()
    {
        return name().toString();
    }
}
