package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum AddressType implements Serializable
{
    RFC822_ADDRESS,
    NUMBER,
    SHORT_CODE
}
