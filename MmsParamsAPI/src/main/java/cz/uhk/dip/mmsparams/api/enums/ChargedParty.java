package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum ChargedParty implements Serializable
{
    SENDER,
    RECIPIENT,
    BOTH,
    NEITHER,
    THIRD_PARTY
}
