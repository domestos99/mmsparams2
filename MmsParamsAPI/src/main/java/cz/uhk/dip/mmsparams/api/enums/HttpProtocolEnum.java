package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum HttpProtocolEnum implements Serializable
{
    HTTP,
    HTTPS
}
