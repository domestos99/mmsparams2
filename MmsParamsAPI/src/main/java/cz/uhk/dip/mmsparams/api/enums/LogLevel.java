package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum LogLevel implements Serializable
{
    INFO("INFO"),
    WARNING("WARNING"),
    ERROR("ERROR"),
    MESSAGE_IN("MESSAGE_IN"),
    MESSAGE_OUT("MESSAGE_OUT");

    private String value;

    LogLevel(String value)
    {
        this.value = value;
    }

    public String value()
    {
        return value;
    }
}
