package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum MM7Encoding implements Serializable
{
    BASE64,
    UTF8
}
