package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum MessageClass implements Serializable
{
    UNDEFINED,
    PERSONAL,
    ADVERTISEMENT,
    INFORMATIONAL,
    AUTO,
}