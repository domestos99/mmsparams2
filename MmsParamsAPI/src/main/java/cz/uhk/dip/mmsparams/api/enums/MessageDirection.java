package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum MessageDirection implements Serializable
{
    IN("IN"),
    OUT("OUT");

    private String value;

    MessageDirection(String value)
    {
        this.value = value;
    }

    public String value()
    {
        return value;
    }
}
