package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum PhoneListRequestFilter implements Serializable
{
    ALL,
    NOT_LOCKED,
    LOCKED_FOR_TEST
}
