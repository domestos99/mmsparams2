package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum Priority implements Serializable
{
    // UNDEFINED,
    LOW,
    NORMAL,
    HIGH
}
