package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum ReadStatus implements Serializable
{
    RAED,
    DELETED
}