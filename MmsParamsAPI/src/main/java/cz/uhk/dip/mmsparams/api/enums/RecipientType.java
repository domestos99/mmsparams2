package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum RecipientType implements Serializable
{
    FROM, TO, CC, BCC
}
