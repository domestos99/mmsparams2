package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum SmsClass implements Serializable
{
    CLASS_0,
    CLASS_1,
    CLASS_2,
    CLASS_3,
    UNKNOWN;
}
