package cz.uhk.dip.mmsparams.api.enums;

import java.io.Serializable;

public enum YesNoUndefined implements Serializable
{
    UNDEFINED,
    YES,
    NO
}
