package cz.uhk.dip.mmsparams.api.enums.mmsc;

import java.io.Serializable;

public enum MM7ContentType implements Serializable
{
    TEXT,
    BINARY,
    BASIC
}
