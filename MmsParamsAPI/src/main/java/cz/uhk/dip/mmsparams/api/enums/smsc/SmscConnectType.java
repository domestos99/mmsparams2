package cz.uhk.dip.mmsparams.api.enums.smsc;

import java.io.Serializable;

public enum SmscConnectType implements Serializable
{
    TRANSCEIVER,
    TRANSMITTER,
    RECEIVER
}
