/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package cz.uhk.dip.mmsparams.api.enums.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.constants.smsc.SMSCConstant;
import cz.uhk.dip.mmsparams.api.logging.APILoggerFactory;
import cz.uhk.dip.mmsparams.api.logging.ApiLogFacade;
import cz.uhk.dip.mmsparams.api.logging.ILogger;

/**
 * Type of Number based on SMPP version 3.4.
 *
 * @author uudashr
 * @version 1.0
 * @since 1.0
 */
public enum TypeOfNumber implements Serializable
{
    TON_UNKNOWN(SMSCConstant.TON_UNKNOWN),
    TON_INTERNATIONAL(SMSCConstant.TON_INTERNATIONAL),
    TON_NATIONAL(SMSCConstant.TON_NATIONAL),
    TON_NETWORK(SMSCConstant.TON_NETWORK),
    TON_SUBSCRIBER(SMSCConstant.TON_SUBSCRIBER),
    TON_ALPHANUMERIC(SMSCConstant.TON_ALPHANUMERIC),
    TON_ABBREVIATED(SMSCConstant.TON_ABBREVIATED);

    private static final ILogger LOGGER = APILoggerFactory.getLogger(NumberingPlanIndicator.class);
    private byte value;

    TypeOfNumber(byte value)
    {
        this.value = value;
    }

    /**
     * Get the byte value of the enum constant.
     *
     * @return the byte value.
     */
    public byte value()
    {
        return value;
    }

    /**
     * Get the <tt>TypeOfNumber</tt> based on the specified byte value
     * representation.
     *
     * @param value is the byte value representation.
     * @return is the enum const related to the specified byte value.
     * @throws IllegalArgumentException if there is no enum const associated
     *                                  with specified byte value.
     */
    public static TypeOfNumber valueOf(byte value)
    {
        for (TypeOfNumber val : values())
        {
            if (val.value == value)
                return val;
        }

        ApiLogFacade.logWarning(LOGGER, "No enum const TypeOfNumber with value " + value);
        return TypeOfNumber.TON_UNKNOWN;
    }
}