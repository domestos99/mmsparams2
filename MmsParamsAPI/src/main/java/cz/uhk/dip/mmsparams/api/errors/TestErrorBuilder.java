package cz.uhk.dip.mmsparams.api.errors;

import cz.uhk.dip.mmsparams.api.exceptions.MmsParamsExceptionBase;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

public class TestErrorBuilder
{
    private final TestErrorModel model;

    public TestErrorBuilder()
    {
        this.model = new TestErrorModel();
    }

    public static TestErrorBuilder create()
    {
        return new TestErrorBuilder();
    }

    public TestErrorBuilder withErrorType(TestErrorType testErrorType)
    {
        model.setTestErrorType(testErrorType);
        return this;
    }

    public TestErrorBuilder withMessage(String message)
    {
        this.model.setMessage(message);
        return this;
    }

    public TestErrorBuilder withException(MmsParamsExceptionBase exception)
    {
        this.model.setException(exception);
        return this;
    }

    public TestErrorBuilder withExceptionAndMessage(MmsParamsExceptionBase exception)
    {
        this.model.setException(exception);
        return this.withMessage(exception.getMessage());
    }

    public TestErrorBuilder withAll(MmsParamsExceptionBase exception)
    {
        return this.withException(exception).withMessage(exception.getMessage()).withErrorType(exception.getTestErrorType());
    }

    public TestErrorModel build()
    {
        return this.model;
    }
}
