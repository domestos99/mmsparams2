package cz.uhk.dip.mmsparams.api.exceptions;

import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

public class AndroidException extends MmsParamsExceptionBase
{
    public AndroidException()
    {
        setTestErrorType(TestErrorType.ANDROID);
    }

    public AndroidException(String message)
    {
        super(message);
        setTestErrorType(TestErrorType.ANDROID);
    }

    public AndroidException(String message, Throwable cause)
    {
        super(message, cause);
        setTestErrorType(TestErrorType.ANDROID);
    }

    public AndroidException(Throwable cause)
    {
        super(cause);
        setTestErrorType(TestErrorType.ANDROID);
    }

    public AndroidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
        setTestErrorType(TestErrorType.ANDROID);
    }
}
