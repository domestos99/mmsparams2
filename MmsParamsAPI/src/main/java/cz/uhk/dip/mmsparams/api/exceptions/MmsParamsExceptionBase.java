package cz.uhk.dip.mmsparams.api.exceptions;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

public class MmsParamsExceptionBase extends RuntimeException implements Serializable
{
    private TestErrorType testErrorType;

    public MmsParamsExceptionBase()
    {
    }

    public MmsParamsExceptionBase(String message)
    {
        super(message);
    }

    public MmsParamsExceptionBase(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MmsParamsExceptionBase(Throwable cause)
    {
        super(cause);
    }

    public MmsParamsExceptionBase(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public TestErrorType getTestErrorType()
    {
        return testErrorType;
    }

    public void setTestErrorType(TestErrorType testErrorType)
    {
        this.testErrorType = testErrorType;
    }
}
