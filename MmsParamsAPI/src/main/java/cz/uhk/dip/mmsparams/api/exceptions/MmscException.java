package cz.uhk.dip.mmsparams.api.exceptions;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

public class MmscException extends MmsParamsExceptionBase implements Serializable
{
    public MmscException()
    {
        setTestErrorType(TestErrorType.MMSC);
    }

    public MmscException(String message)
    {
        super(message);
        setTestErrorType(TestErrorType.MMSC);
    }

    public MmscException(String message, Throwable cause)
    {
        super(message, cause);
        setTestErrorType(TestErrorType.MMSC);
    }

    public MmscException(Throwable cause)
    {
        super(cause);
        setTestErrorType(TestErrorType.MMSC);
    }

    public MmscException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
        setTestErrorType(TestErrorType.MMSC);
    }
}
