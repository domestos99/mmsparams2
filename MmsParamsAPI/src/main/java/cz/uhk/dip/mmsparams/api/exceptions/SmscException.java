package cz.uhk.dip.mmsparams.api.exceptions;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

public class SmscException extends MmsParamsExceptionBase implements Serializable
{
    public SmscException()
    {
        setTestErrorType(TestErrorType.SMSC);
    }

    public SmscException(String message)
    {
        super(message);
        setTestErrorType(TestErrorType.SMSC);
    }

    public SmscException(String message, Throwable cause)
    {
        super(message, cause);
        setTestErrorType(TestErrorType.SMSC);
    }

    public SmscException(Throwable cause)
    {
        super(cause);
        setTestErrorType(TestErrorType.SMSC);
    }

    public SmscException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
        setTestErrorType(TestErrorType.SMSC);
    }
}
