package cz.uhk.dip.mmsparams.api.exceptions;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

public class SystemException extends MmsParamsExceptionBase implements Serializable
{
    public SystemException()
    {
        setTestErrorType(TestErrorType.SYSTEM_ERROR);
    }

    public SystemException(String message)
    {
        super(message);
        setTestErrorType(TestErrorType.SYSTEM_ERROR);
    }

    public SystemException(String message, Throwable cause)
    {
        super(message, cause);
        setTestErrorType(TestErrorType.SYSTEM_ERROR);
    }

    public SystemException(Throwable cause)
    {
        super(cause);
        setTestErrorType(TestErrorType.SYSTEM_ERROR);
    }

    public SystemException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
        setTestErrorType(TestErrorType.SYSTEM_ERROR);
    }
}
