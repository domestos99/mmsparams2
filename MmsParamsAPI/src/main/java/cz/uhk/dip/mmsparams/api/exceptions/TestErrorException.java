package cz.uhk.dip.mmsparams.api.exceptions;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

public class TestErrorException extends MmsParamsExceptionBase implements Serializable
{
    public TestErrorException()
    {
        super();
        setTestErrorType(TestErrorType.TEST_ERROR);
    }

    public TestErrorException(String message)
    {
        super(message);
        setTestErrorType(TestErrorType.TEST_ERROR);
    }

    public TestErrorException(String message, Throwable cause)
    {
        super(message, cause);
        setTestErrorType(TestErrorType.TEST_ERROR);
    }

    public TestErrorException(Throwable cause)
    {
        super(cause);
        setTestErrorType(TestErrorType.TEST_ERROR);
    }

    public TestErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
        setTestErrorType(TestErrorType.TEST_ERROR);
    }
}
