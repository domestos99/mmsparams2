package cz.uhk.dip.mmsparams.api.exceptions;

import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

public class TestInvalidException extends MmsParamsExceptionBase
{
    public TestInvalidException()
    {
        setTestErrorType(TestErrorType.WRONGLY_WRITTEN_TEST);
    }

    public TestInvalidException(String message)
    {
        super(message);
        setTestErrorType(TestErrorType.WRONGLY_WRITTEN_TEST);
    }

    public TestInvalidException(String message, Throwable cause)
    {
        super(message, cause);
        setTestErrorType(TestErrorType.WRONGLY_WRITTEN_TEST);
    }

    public TestInvalidException(Throwable cause)
    {
        super(cause);
        setTestErrorType(TestErrorType.WRONGLY_WRITTEN_TEST);
    }

    public TestInvalidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
        setTestErrorType(TestErrorType.WRONGLY_WRITTEN_TEST);
    }
}
