package cz.uhk.dip.mmsparams.api.exceptions;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

public class TimeOutException extends MmsParamsExceptionBase implements Serializable
{
    public TimeOutException()
    {
        super();
        setTestErrorType(TestErrorType.TIMEOUT);
    }

    public TimeOutException(String message)
    {
        super(message);
        setTestErrorType(TestErrorType.TIMEOUT);
    }

    public TimeOutException(String message, Throwable cause)
    {
        super(message, cause);
        setTestErrorType(TestErrorType.TIMEOUT);
    }

    public TimeOutException(Throwable cause)
    {
        super(cause);
        setTestErrorType(TestErrorType.TIMEOUT);
    }

    public TimeOutException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
        setTestErrorType(TestErrorType.TIMEOUT);
    }
}
