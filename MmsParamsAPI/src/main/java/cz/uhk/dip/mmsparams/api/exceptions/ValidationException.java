package cz.uhk.dip.mmsparams.api.exceptions;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

public class ValidationException extends MmsParamsExceptionBase implements Serializable
{
    public ValidationException()
    {
        setTestErrorType(TestErrorType.VALIDATIONS);
    }

    public ValidationException(ValidationResult validationResult)
    {
        super(validationResult == null ? "validationResult is null!" : validationResult.toString());
        setTestErrorType(TestErrorType.VALIDATIONS);
    }

    public ValidationException(String message)
    {
        super(message);
        setTestErrorType(TestErrorType.VALIDATIONS);
    }
}
