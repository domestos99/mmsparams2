package cz.uhk.dip.mmsparams.api.http;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.utils.UrlUtils;

/**
 * Provider of Server address and Server WebSocket address
 */
public class ServerAddressProvider
{
    private final HttpProtocolEnum httpProtocolEnum;
    private final String address;
    private final String port;

    /**
     * Initialize ServerAddressProvider
     *
     * @param httpProtocolEnum whether user HTTP or HTTPS
     * @param address          server address in simple format (eg. 192.168.1.10; test.com)
     */
    public ServerAddressProvider(HttpProtocolEnum httpProtocolEnum, String address)
    {
        Preconditions.checkNotNullOrEmpty(address, GenericConstants.ADDRESS);
        this.httpProtocolEnum = httpProtocolEnum;
        this.address = address;
        this.port = null;
    }

    /**
     * Initialize ServerAddressProvider
     *
     * @param httpProtocolEnum whether user HTTP or HTTPS
     * @param address          server address in simple format (eg. 192.168.1.10; test.com)
     * @param port             server port
     */
    public ServerAddressProvider(HttpProtocolEnum httpProtocolEnum, String address, String port)
    {
        Preconditions.checkNotNullOrEmpty(address, GenericConstants.ADDRESS);
        Preconditions.checkNotNullOrEmpty(port, GenericConstants.PORT);
        this.httpProtocolEnum = httpProtocolEnum;
        this.address = address;
        this.port = port;
    }

    /**
     * Returns Server address
     *
     * @return server address with http/https prefix
     */
    public String getServerAddress()
    {
        return getServerAddress(true);
    }


    /**
     * Returns Server address
     *
     * @param includePrefix if true, include http/https prefix
     * @return server address
     */
    public String getServerAddress(boolean includePrefix)
    {
        if (includePrefix)
        {
            if (StringUtil.isEmptyOrNull(port))
                return getHttpPrefix() + address;
            else
                return getHttpPrefix() + address + ":" + port;
        }
        else
        {
            if (StringUtil.isEmptyOrNull(port))
                return address;
            else
                return address + ":" + port;
        }
    }


    /**
     * Returns Server WebSocket address
     *
     * @return server WebSocket address
     */
    public String getWSAddress()
    {
        if (StringUtil.isEmptyOrNull(port))
            return UrlUtils.getServerSocketUrl(this.httpProtocolEnum, address);
        else
            return UrlUtils.getServerSocketUrl(this.httpProtocolEnum, address, port);
    }

    public HttpProtocolEnum getHttpProtocolEnum()
    {
        return httpProtocolEnum;
    }

    public String getHttpPrefix()
    {
        return HttpConstants.getHttpPrefix(getHttpProtocolEnum());
    }

    public boolean isHttps()
    {
        return httpProtocolEnum == HttpProtocolEnum.HTTPS;
    }
}
