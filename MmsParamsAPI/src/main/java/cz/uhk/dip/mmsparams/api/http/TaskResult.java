package cz.uhk.dip.mmsparams.api.http;

public class TaskResult<T>
{
    private final T result;
    private final Exception error;

    public T getResult()
    {
        return result;
    }

    public Exception getError()
    {
        return error;
    }

    public boolean hasResult()
    {
        return result != null;
    }

    public boolean hasError()
    {
        return error != null;
    }

    public TaskResult(T result)
    {
        super();
        this.result = result;
        this.error = null;
    }

    public TaskResult(Exception error)
    {
        super();
        this.result = null;
        this.error = error;
    }
}
