package cz.uhk.dip.mmsparams.api.http;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;

public class WSConnectSettings
{
    private final String username;
    private final String password;
    private final ServerAddressProvider serverAddressProvider;

    public WSConnectSettings(ServerAddressProvider serverAddressProvider, String username, String password)
    {
        Preconditions.checkNotNull(serverAddressProvider, GenericConstants.SERVER_ADDRESS_PROVIDER);
        Preconditions.checkNotNullOrEmpty(serverAddressProvider.getWSAddress(), GenericConstants.SERVER_ADDRESS_PROVIDER);
        Preconditions.checkNotNullOrEmpty(serverAddressProvider.getServerAddress(), GenericConstants.SERVER_ADDRESS_PROVIDER);
        Preconditions.checkNotNullOrEmpty(username, GenericConstants.USERNAME);
        Preconditions.checkNotNullOrEmpty(password, GenericConstants.PASSWORD);

        this.serverAddressProvider = serverAddressProvider;
        this.username = username;
        this.password = password;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    public ServerAddressProvider getServerAddressProvider()
    {
        return serverAddressProvider;
    }
}
