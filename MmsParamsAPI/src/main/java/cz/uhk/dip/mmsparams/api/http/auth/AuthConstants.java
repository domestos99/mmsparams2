package cz.uhk.dip.mmsparams.api.http.auth;

public class AuthConstants
{
    private AuthConstants()
    {
    }

    public static final String AUTH_ADDRESS = "/api/authenticate";
}
