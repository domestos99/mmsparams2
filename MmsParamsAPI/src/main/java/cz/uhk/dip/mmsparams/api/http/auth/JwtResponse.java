package cz.uhk.dip.mmsparams.api.http.auth;

import java.io.Serializable;

public class JwtResponse implements Serializable
{
    private String token;
    private long expiry;

    public JwtResponse()
    {
    }

    public JwtResponse(String token, long expiry)
    {
        this.token = token;
        this.expiry = expiry;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public long getExpiry()
    {
        return expiry;
    }

    public void setExpiry(long expiry)
    {
        this.expiry = expiry;
    }
}
