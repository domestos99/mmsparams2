package cz.uhk.dip.mmsparams.api.interfaces;

import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;

public interface IClientInfoProvider
{
    ClientInfo getClientInfo();
}
