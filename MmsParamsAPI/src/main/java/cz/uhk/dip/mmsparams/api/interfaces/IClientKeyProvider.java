package cz.uhk.dip.mmsparams.api.interfaces;

/**
 * Provides ClientKey
 */
public interface IClientKeyProvider
{
    /**
     * Provides clientKey
     *
     * @return client key
     */
    String getClientKey();
}
