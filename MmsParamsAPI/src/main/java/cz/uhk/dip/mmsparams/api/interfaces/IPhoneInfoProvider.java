package cz.uhk.dip.mmsparams.api.interfaces;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;

public interface IPhoneInfoProvider
{
    PhoneInfoModel getPhoneInfo();
}
