package cz.uhk.dip.mmsparams.api.interfaces;

/**
 * Provides TestID
 */
public interface ITestIdProvider
{
    /**
     * Provides clientKey
     *
     * @return test id
     */
    String getTestId();
}
