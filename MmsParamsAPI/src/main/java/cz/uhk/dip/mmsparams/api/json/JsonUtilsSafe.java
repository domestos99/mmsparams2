package cz.uhk.dip.mmsparams.api.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.Reader;

import cz.uhk.dip.mmsparams.api.logging.APILoggerFactory;
import cz.uhk.dip.mmsparams.api.logging.ApiLogFacade;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.ByteUtil;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;


public class JsonUtilsSafe
{
    public static final String TAG = JsonUtilsSafe.class.getSimpleName();
    private static final ILogger LOGGER = APILoggerFactory.getLogger(JsonUtilsSafe.class);

    private JsonUtilsSafe()
    {
    }

    public static String getPretty(final String json)
    {
        JsonParser parser = new JsonParser();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        JsonElement el = parser.parse(json);
        return gson.toJson(el); // done
    }


    public static <T> T fromJson(byte[] serialized, Class<T> clazz)
    {
        try
        {
            if (ByteUtil.isEmptyOrNull(serialized))
                return null;

            return fromJson(new String(serialized), clazz);
        }
        catch (Exception e)
        {
            ApiLogFacade.logEx(LOGGER, "fromJson: " + serialized + " : " + clazz, e);
            return null;
        }
    }

    public static <T> T fromJson(String serialized, Class<T> clazz)
    {
        try
        {
            if (StringUtil.isEmptyOrNull(serialized))
                return null;

            return JsonUtils.fromJson(serialized, clazz);
        }
        catch (Exception e)
        {
            ApiLogFacade.logEx(LOGGER, "fromJson: " + serialized + " : " + clazz, e);
            return null;
        }
    }

    public static <T> T fromJson(Reader serialized, Class<T> clazz)
    {
        try
        {
            return JsonUtils.fromJson(serialized, clazz);
        }
        catch (Exception e)
        {
            ApiLogFacade.logEx(LOGGER, "fromJson: " + serialized + " : " + clazz, e);
            return null;
        }
    }

    public static String toJson(Object object)
    {
        try
        {
            if (object == null)
                return null;
            return JsonUtils.toJson(object);
        }
        catch (Exception e)
        {
            ApiLogFacade.logEx(LOGGER, "fromJson: " + object, e);
            return null;
        }
    }

}
