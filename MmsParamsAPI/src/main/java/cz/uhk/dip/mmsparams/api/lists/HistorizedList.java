package cz.uhk.dip.mmsparams.api.lists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.uhk.dip.mmsparams.api.utils.Preconditions;

public class HistorizedList<T, R> implements Iterable<T>
{
    private final List<T> list;
    private final List<R> historized;
    private final HistorizedListConverter<T, R> converter;

    public HistorizedList(HistorizedListConverter<T, R> converter)
    {
        Preconditions.checkNotNull(converter, "converter");
        this.converter = converter;
        this.list = new ArrayList<>();
        this.historized = new ArrayList<>();
    }

    public List<T> getAll()
    {
        return this.list;
    }

    public void add(T obj)
    {
        this.list.add(obj);
        this.historized.add(this.converter.convert(obj));
    }

    public int size()
    {
        return this.list.size();
    }

    public void remove(int index)
    {
        this.list.remove(index);
    }

    @Override
    public Iterator<T> iterator()
    {
        return this.list.iterator();
    }

    public T get(int index)
    {
        return this.list.get(index);
    }

    public void addAll(List<T> list)
    {
        this.list.addAll(list);
        this.historized.addAll(converter.convert(list));
    }

    public List<R> getHistorizedAll()
    {
        return this.historized;
    }


    public void clear()
    {
        this.list.clear();
    }
}
