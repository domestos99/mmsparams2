package cz.uhk.dip.mmsparams.api.lists;

import java.util.ArrayList;
import java.util.List;

public interface HistorizedListConverter<T, R>
{
    default List<R> convert(List<T> obj)
    {
        List<R> list = new ArrayList<>();
        for (T i : obj)
            list.add(convert(i));
        return list;
    }

    R convert(T obj);
}
