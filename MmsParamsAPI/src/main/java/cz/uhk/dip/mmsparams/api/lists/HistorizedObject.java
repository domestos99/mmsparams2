package cz.uhk.dip.mmsparams.api.lists;

import cz.uhk.dip.mmsparams.api.utils.Preconditions;

public class HistorizedObject<T, R>
{
    private T obj;
    private final R historized;

    public HistorizedObject(T obj, HistorizedListConverter<T, R> converter)
    {
        Preconditions.checkNotNull(converter, "converter");
        this.obj = obj;
        this.historized = converter.convert(obj);
    }

    public T getObject()
    {
        return this.obj;
    }

    public R getHistorizedObject()
    {
        return this.historized;
    }

    public void clear()
    {
        this.obj = null;
    }
}
