package cz.uhk.dip.mmsparams.api.logging;

public class APILoggerFactory
{
    private APILoggerFactory()
    {
    }

    public static <T> ILogger getLogger(Class<T> clz)
    {
        return new JavaLogger(clz);
    }
}
