package cz.uhk.dip.mmsparams.api.logging;


public class ApiLogFacade
{
    private ApiLogFacade()
    {
    }

    public static void logEx(final ILogger logger, Exception e)
    {
        logger.error(e);
    }

    public static void logInfo(final ILogger logger, String info)
    {
        logger.info(info);
    }

    public static void logWarning(final ILogger logger, String warning)
    {
        logger.warn(warning);
    }

    public static void logEx(final ILogger logger, String message, Exception ex)
    {
        logger.error(message, ex);
    }

    public static void logWebSocket(final ILogger logger, String info)
    {
        logger.info(info);
    }

    public static void logDebug(ILogger logger, String message)
    {
        logger.warn(message);
    }
}
