package cz.uhk.dip.mmsparams.api.logging;


public interface ILogger
{
    /**
     * Log info message
     *
     * @param info info message
     */
    void info(String info);

    /**
     * Log warning message
     *
     * @param warning warning message
     */
    void warn(String warning);

    /**
     * Log exceptin
     *
     * @param t exception
     */
    void error(Throwable t);

    /**
     * Log error message
     *
     * @param error error message
     */
    void error(String error);

    /**
     * Log error message and exception
     *
     * @param message error message
     * @param t       exception
     */
    void error(String message, Throwable t);

    /**
     * Log received unknown message
     *
     * @param messageJson message in JSON format
     */
    default void logUnknownMessage(String messageJson)
    {
        logIncomingMessage(messageJson);
    }

    /**
     * Log new incoming message
     *
     * @param messageJson message in JSON format
     */
    void logIncomingMessage(String messageJson);

    /**
     * Log new outgoing message
     *
     * @param messageJson message in JSON format
     */
    void logOutgoingMessage(String messageJson);

    /**
     * Log new incoming error message
     *
     * @param messageJson message in JSON format
     */
    default void logErrorMessage(String messageJson)
    {
        logIncomingMessage(messageJson);
    }
}
