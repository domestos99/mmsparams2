package cz.uhk.dip.mmsparams.api.logging;

import java.util.logging.Level;
import java.util.logging.Logger;

public class JavaLogger implements ILogger
{
    private final Logger logger;

    public JavaLogger(Class clz)
    {
        this.logger = Logger.getLogger(clz.getName());
    }

    @Override
    public void info(String info)
    {
        logger.info(info);
    }

    @Override
    public void warn(String warning)
    {
        logger.warning(warning);
    }

    @Override
    public void error(Throwable t)
    {
        logger.log(Level.SEVERE, "", t);
    }

    @Override
    public void error(String message, Throwable t)
    {
        logger.log(Level.SEVERE, message, t);
    }

    @Override
    public void error(String error)
    {
        logger.log(Level.SEVERE, error);
    }

    @Override
    public void logIncomingMessage(String messageJson)
    {
        info(messageJson);
    }

    @Override
    public void logOutgoingMessage(String messageJson)
    {
        info(messageJson);
    }
}
