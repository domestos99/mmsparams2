package cz.uhk.dip.mmsparams.api.logging.formats;

import cz.uhk.dip.mmsparams.api.websocket.router.MessageRouter;

public class MessageFormater
{
    private MessageFormater()
    {
    }

    public static String getPrettyMessage(String message)
    {
        MessageFormaterRouter form = new MessageFormaterRouter();
        return MessageRouter.process(form, message);
    }


}
