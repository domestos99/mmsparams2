package cz.uhk.dip.mmsparams.api.logging.formats;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.listeners.IMessageReceiveSub;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.KeepAliveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.email.EmailReceiveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.TestErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsRecipientPhoneProfileRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.AcknowledgeIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.DeliveryIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotifyRespIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadOrigIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadRecIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendReqResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReportReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ReadReplyReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7SubmitResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneAllPartsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDisconnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;

public class MessageFormaterRouter implements IMessageReceiveSub<String>
{
    private String getToString(WebSocketMessageBase msg)
    {
        return msg.toString();
    }

    @Override
    public String onReceiveUnknown(String msg)
    {
        return "Unknown message: " + msg;
    }

    @Override
    public String onReceive(PhoneListRequestMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(RegisterPhoneMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(EmptyMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(PhoneListResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmsSendPhoneRequestMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmsReceivePhoneMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmsReceivePhoneAllPartsMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmsSendPhoneResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmsSendDeliveryReportMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(MmsSendPhoneRequestMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(DevMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(MmsRecipientPhoneProfileRequestMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(GenericBooleanResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(GenericErrorResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SendConfResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(RegisterClientLibMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(NotificationIndResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(DeliveryIndResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(ReadOrigIndResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(AcknowledgeIndResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(RetrieveConfResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(ReadRecIndResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(NotifyRespIndResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(MM7DeliveryReqMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(MM7DeliveryReportReqMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(MM7ReadReplyReqMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmscConnectMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmscSendSmsMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmscDeliverSmMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmscDisconnectMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmscDeliveryReportMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmscConnectResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SmscSendSmsResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(UnLockPhoneMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(SendReqResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(MmscSendMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(LockPhoneMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(LockedPhonesListResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(LockedPhonesListRequestMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(MM7SubmitResponseMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(MmscAcquireRouteRequestMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(TestValidationMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(TestErrorMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(MM7ErrorMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(KeepAliveMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(TestResultMessage msg)
    {
        return getToString(msg);
    }

    @Override
    public String onReceive(EmailReceiveMessage msg)
    {
        return getToString(msg);
    }
}

