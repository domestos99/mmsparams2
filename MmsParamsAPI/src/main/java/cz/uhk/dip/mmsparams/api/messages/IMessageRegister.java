package cz.uhk.dip.mmsparams.api.messages;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;

public interface IMessageRegister
{
    void insertIncomingMsg(final WebSocketMessageBase msg);

    void insertOutgoingMsg(final WebSocketMessageBase msg);

    default WebSocketMessageBase getIncomByMsgId(final String messageID, final String messageKey)
    {
        return getIncomByMsgId(new MessageId(messageID), messageKey);
    }

    WebSocketMessageBase getIncomByMsgId(final MessageId messageID, final String messageKey);

    List<MessageRegisterItem> getIncomByClz(final String messageKey);

    <T extends WebSocketMessageBase> int getIncomingCount(final Class<T> clz);

    List<WebSocketMessageBase> getIncomByDeviceId(final PhoneInfoModel phoneInfoModel, final String messageKey);

    List<MessageRegisterItem> getAllIncoming();

    List<MessageRegisterItem> getAllOutgoing();

    List<String> getAllUnknown();

    List<WebSocketMessageBase> getIncomBySmscSessionId(final SmscSessionId smscSessionId, final String messageKey);

    void insertUnknownMessage(String msg);

    List<MessageRegisterItem> getOutgoingByClz(String messageKey);
}
