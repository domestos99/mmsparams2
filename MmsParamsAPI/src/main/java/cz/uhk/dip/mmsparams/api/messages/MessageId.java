package cz.uhk.dip.mmsparams.api.messages;

import java.io.Serializable;

/**
 * Represents send or receive message ID
 */
public class MessageId implements Serializable
{
    private String messageId;

    public MessageId()
    {
        this.messageId = null;
    }

    public MessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }
}
