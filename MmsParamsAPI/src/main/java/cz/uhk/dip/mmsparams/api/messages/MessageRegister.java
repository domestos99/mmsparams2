package cz.uhk.dip.mmsparams.api.messages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import cz.uhk.dip.mmsparams.api.enums.MessageDirection;
import cz.uhk.dip.mmsparams.api.logging.APILoggerFactory;
import cz.uhk.dip.mmsparams.api.logging.ApiLogFacade;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;

public class MessageRegister implements IMessageRegister
{
    private final ConcurrentHashMap<String, List<MessageRegisterItem>> outgoing = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, List<MessageRegisterItem>> incoming = new ConcurrentHashMap<>();
    private final CopyOnWriteArrayList<String> unknownMessages = new CopyOnWriteArrayList<>();

    private static final ILogger LOGGER = APILoggerFactory.getLogger(MessageRegister.class);

    @Override
    public synchronized void insertIncomingMsg(final WebSocketMessageBase msg)
    {
        if (msg == null)
        {
            ApiLogFacade.logWarning(LOGGER, "Unable to insert null Incoming message!");
            return;
        }
        insertMsgToList(incoming, msg, MessageDirection.IN);
    }

    @Override
    public synchronized void insertOutgoingMsg(final WebSocketMessageBase msg)
    {
        if (msg == null)
        {
            ApiLogFacade.logWarning(LOGGER, "Unable to insert null Outgoing message!");
            return;
        }
        insertMsgToList(outgoing, msg, MessageDirection.OUT);
    }

    private synchronized void insertMsgToList(ConcurrentHashMap<String, List<MessageRegisterItem>> list, WebSocketMessageBase msg, MessageDirection direction)
    {
        final String msgKey = MessageType.getKeyByClass(msg.getClass());

        if (list.containsKey(msgKey))
        {
            MessageRegisterItem item = new MessageRegisterItem(msg, direction);
            list.get(msgKey).add(item);
        }
        else
        {
            List<MessageRegisterItem> l = new ArrayList<>();
            l.add(new MessageRegisterItem(msg, direction));
            list.put(msgKey, l);
        }
    }

    @Override
    public synchronized WebSocketMessageBase getIncomByMsgId(final MessageId messageID, final String messageKey)
    {
        if (messageID == null || StringUtil.isEmptyOrNull(messageID.getMessageId()))
        {
            ApiLogFacade.logWarning(LOGGER, "getIncomByMsgId: messageID cannot be null or empty");
            return null;
        }
        if (StringUtil.isEmptyOrNull(messageKey))
        {
            ApiLogFacade.logWarning(LOGGER, "getIncomByMsgId: messageKey cannot be null or empty");
            return null;
        }

        final List<MessageRegisterItem> list = incoming.get(messageKey);

        if (list == null)
            return null;

        for (MessageRegisterItem msg : list)
        {
            if (messageID.getMessageId().equals(msg.getMessage().getMessageID()))
                return msg.getMessage();
        }
        return null;
    }

    @Override
    public synchronized List<MessageRegisterItem> getIncomByClz(final String messageKey)
    {
        if (StringUtil.isEmptyOrNull(messageKey))
        {
            ApiLogFacade.logWarning(LOGGER, "getIncomByClz: messageKey cannot be null or empty");
            return new ArrayList<>();
        }

        final List<MessageRegisterItem> list = incoming.get(messageKey);
        if (list == null)
            return new ArrayList<>();
        return list;
    }

    @Override
    public List<MessageRegisterItem> getOutgoingByClz(String messageKey)
    {
        if (StringUtil.isEmptyOrNull(messageKey))
        {
            ApiLogFacade.logWarning(LOGGER, "getOutgoingByClz: messageKey cannot be null or empty");
            return new ArrayList<>();
        }

        final List<MessageRegisterItem> list = this.outgoing.get(messageKey);
        if (list == null)
            return new ArrayList<>();
        return list;
    }

    @Override
    public synchronized <T extends WebSocketMessageBase> int getIncomingCount(final Class<T> clz)
    {
        if (clz == null)
        {
            ApiLogFacade.logWarning(LOGGER, "getIncomingCount: clz cannot be null");
            return 0;
        }

        final String msgKey = MessageType.getKeyByClass(clz);
        final List<MessageRegisterItem> list = incoming.get(msgKey);

        if (list == null)
            return 0;
        return list.size();
    }

    @Override
    public synchronized List<WebSocketMessageBase> getIncomByDeviceId(final PhoneInfoModel phoneInfoModel, final String messageKey)
    {
        List<WebSocketMessageBase> result = new ArrayList<>();
        if (phoneInfoModel == null || StringUtil.isEmptyOrNull(phoneInfoModel.getPhoneKey()))
        {
            ApiLogFacade.logWarning(LOGGER, "getIncomByDeviceId: phoneInfoModel cannot be null or empty");
            return result;
        }
        if (StringUtil.isEmptyOrNull(messageKey))
        {
            ApiLogFacade.logWarning(LOGGER, "getIncomByDeviceId: messageKey cannot be null or empty");
            return result;
        }

        final List<MessageRegisterItem> list = incoming.get(messageKey);
        if (list == null)
            return result;

        for (final MessageRegisterItem msg : list)
        {
            if (phoneInfoModel.getPhoneKey().equals(msg.getMessage().getSenderKey()))
                result.add(msg.getMessage());
        }
        return result;
    }

    @Override
    public synchronized List<MessageRegisterItem> getAllIncoming()
    {
        return ListUtils.toList2(incoming.values());
    }

    @Override
    public synchronized List<MessageRegisterItem> getAllOutgoing()
    {
        return ListUtils.toList2(outgoing.values());
    }

    @Override
    public synchronized List<String> getAllUnknown()
    {
        return ListUtils.toList(unknownMessages);
    }

    @Override
    public synchronized List<WebSocketMessageBase> getIncomBySmscSessionId(SmscSessionId smscSessionId, String messageKey)
    {
        if (smscSessionId == null || StringUtil.isEmptyOrNull(smscSessionId.getSmscSessionId()))
        {
            ApiLogFacade.logWarning(LOGGER, "getIncomBySmscSessionId: smscSessionId cannot be null or empty");
            return new ArrayList<>();
        }
        if (StringUtil.isEmptyOrNull(messageKey))
        {
            ApiLogFacade.logWarning(LOGGER, "getIncomBySmscSessionId: messageKey cannot be null or empty");
            return new ArrayList<>();
        }

        final List<MessageRegisterItem> list = incoming.get(messageKey);

        if (list == null)
            return new ArrayList<>();

        List<WebSocketMessageBase> result = new ArrayList<>();

        for (MessageRegisterItem msg : list)
        {
            if ((!(msg.getMessage() instanceof SmscMessageBase)) || (((SmscMessageBase) msg.getMessage()).getSmscSessionId() == null))
            {
                continue;
            }

            if (smscSessionId.getSmscSessionId().equals(((SmscMessageBase) msg.getMessage()).getSmscSessionId().getSmscSessionId()))
                result.add(msg.getMessage());
        }
        return result;
    }

    @Override
    public synchronized void insertUnknownMessage(String msg)
    {
        unknownMessages.add(msg);
    }


}
