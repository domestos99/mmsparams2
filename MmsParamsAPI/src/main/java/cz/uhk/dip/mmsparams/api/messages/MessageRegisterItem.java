package cz.uhk.dip.mmsparams.api.messages;

import java.util.List;
import java.util.stream.Collectors;

import cz.uhk.dip.mmsparams.api.enums.MessageDirection;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class MessageRegisterItem
{
    private final long insertedDT;
    private final MessageDirection direction;
    private final WebSocketMessageBase message;

    public MessageRegisterItem(final WebSocketMessageBase message, MessageDirection direction)
    {
        this.message = message;
        this.direction = direction;
        this.insertedDT = System.currentTimeMillis();
    }

    public static List<WebSocketMessageBase> getOnlyMessages(List<MessageRegisterItem> list)
    {
        return list.stream().map(x -> x.getMessage()).collect(Collectors.toList());
    }

    public long getInsertedDT()
    {
        return insertedDT;
    }

    public WebSocketMessageBase getMessage()
    {
        return message;
    }

    public MessageDirection getDirection()
    {
        return direction;
    }
}
