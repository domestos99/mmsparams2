package cz.uhk.dip.mmsparams.api.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CommonUtils
{
    private CommonUtils()
    {
    }

    public static String getUUID()
    {
        return UUID.randomUUID().toString();
    }

    public static List<String> getUUIDList(int count)
    {
        List<String> l = new ArrayList<>();
        if (count < 1)
            return l;
        for (int i = 0; i < count; i++)
            l.add(getUUID());
        return l;
    }
}
