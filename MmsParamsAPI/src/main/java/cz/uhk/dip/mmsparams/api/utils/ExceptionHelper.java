package cz.uhk.dip.mmsparams.api.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

import cz.uhk.dip.mmsparams.api.exceptions.MmsParamsExceptionBase;
import cz.uhk.dip.mmsparams.api.exceptions.SystemException;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;

public class ExceptionHelper
{
    private ExceptionHelper()
    {
    }

    public static String getStackTrace(Throwable exception)
    {
        if (exception == null)
            return "";

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        return sw.toString(); // stack trace as a string
    }

    public static Exception generateException(GenericErrorResponseMessage error)
    {
        if (error == null)
            return new Exception("GenericErrorResponseMessage not present");

        if (error.getTestErrorModel() == null)
        {
            return new Exception(error.toString());
        }
        return new Exception(error.toString(), error.getTestErrorModel().getException());
    }

    public static <T extends MmsParamsExceptionBase> T castException(Throwable ex)
    {
        if (InheritanceUtil.isSubclass(ex, MmsParamsExceptionBase.class))
        {
            return (T) ex;
        }
        else
        {
            return (T) new SystemException(ex);
        }
    }
}
