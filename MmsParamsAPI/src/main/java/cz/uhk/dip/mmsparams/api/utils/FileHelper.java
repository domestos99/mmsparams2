package cz.uhk.dip.mmsparams.api.utils;

import java.io.File;
import java.util.Optional;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.constants.MimeTypeMap;

public class FileHelper
{
    private FileHelper()
    {
    }

    public static Optional<String> getExtension(final File file)
    {
        Preconditions.checkNotNull(file, GenericConstants.FILE);

        final String fileName = file.getName();
        return Optional.ofNullable(fileName).filter(f -> f.contains(".")).map(f -> f.substring(fileName.lastIndexOf('.') + 1));
    }

    public static String getMimeType(final File file)
    {
        Preconditions.checkNotNull(file, GenericConstants.FILE);
        Optional<String> fileExt = getExtension(file);
        String val = Preconditions.checkOptionalFilled(fileExt, GenericConstants.FILE_EXTENSION);
        return MimeTypeMap.getMimeType(val);
    }
}
