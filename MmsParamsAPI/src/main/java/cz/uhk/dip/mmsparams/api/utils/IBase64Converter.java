package cz.uhk.dip.mmsparams.api.utils;

public interface IBase64Converter
{
    String encodeToString(byte[] bytes);

    byte[] decode(String param);
}
