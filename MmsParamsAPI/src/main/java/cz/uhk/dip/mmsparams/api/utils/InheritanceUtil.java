package cz.uhk.dip.mmsparams.api.utils;

public class InheritanceUtil
{
    public static boolean isSubclass(Object o, Class clz)
    {
        if (o == null || clz == null)
            return false;
        return clz.isAssignableFrom(o.getClass());
    }

}
