package cz.uhk.dip.mmsparams.api.utils;

import java.util.Base64;

public class JavaBase64Converter implements IBase64Converter
{
    @Override
    public String encodeToString(byte[] bytes)
    {
        return Base64.getEncoder().encodeToString(bytes);
    }

    @Override
    public byte[] decode(String param)
    {
        return Base64.getDecoder().decode(param);
    }
}
