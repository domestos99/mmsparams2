package cz.uhk.dip.mmsparams.api.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ListUtils
{
    private ListUtils()
    {
    }

    public static List<String> getString(List objList)
    {
        if (objList == null)
            return null;

        final List<String> list = new ArrayList<>();
        for (Object o : objList)
        {
            list.add(StringUtil.getStringSafe(o));
        }
        return list;
    }

    public static <T> List<T> toList2(final Collection<List<T>> values)
    {
        final List<T> list = new ArrayList<>();

        if (values == null)
            return list;

        for (List<T> v : values)
        {
            list.addAll(v);
        }
        return list;
    }

    public static <T> List<T> toList(final Collection<T> values)
    {
        if (values == null)
            return new ArrayList<>();
        return new ArrayList<>(values);
    }

    public static <T> ArrayList<T> toArrayList(List<T> values)
    {
        if (values == null)
        {
            return new ArrayList<>();
        }
        return new ArrayList<>(values);
    }

    public static <T> List<T> toList(T[] array)
    {
        return array == null ? new ArrayList<>() : new ArrayList<>(Arrays.asList(array));
    }

    public static <T> List<T> toListNull(T[] array)
    {
        return array == null ? null : new ArrayList<>(Arrays.asList(array));
    }

    public static <T> List<T> newList()
    {
        return new ArrayList<>();
    }

    public static <T> List<T> newList(List<T> items)
    {
        if (items == null)
            return newList();
        return new ArrayList<>(items);
    }
}
