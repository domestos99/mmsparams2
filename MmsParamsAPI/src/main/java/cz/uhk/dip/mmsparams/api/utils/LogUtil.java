package cz.uhk.dip.mmsparams.api.utils;

import java.util.Arrays;

public class LogUtil
{
    public static final String NEW_LINE = System.getProperty("line.separator");
    public static final String LOG_LINE_SEPARATOR = "------------------------------------------------------\n";
    public static final String HASHES = "#################################################################################################################################################";

    private LogUtil()
    {
    }

    public static String getCurrentStackTrace()
    {
        return Arrays.toString(Thread.currentThread().getStackTrace());
    }

    public static StringBuilder appendLine(final StringBuilder sb, String text)
    {
        return sb.append(text + NEW_LINE);
    }

}
