package cz.uhk.dip.mmsparams.api.utils;

import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.websocket.MessageIdGenerator;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketConstants;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class MessageBuilder
{
    private String senderKey;
    private String recipientKey;
    private String messageID;
    private String testID;

    public MessageBuilder withSenderKey(String senderKey)
    {
        this.senderKey = senderKey;
        return this;
    }

    public MessageBuilder withRecipientKey(String recipientKey)
    {
        this.recipientKey = recipientKey;
        return this;
    }

    public MessageBuilder withRecipientKeyServer()
    {
        this.recipientKey = WebSocketConstants.Server_Recipient_Key;
        return this;
    }

    public MessageBuilder withSenderKeyServer()
    {
        this.senderKey = WebSocketConstants.Server_Recipient_Key;
        return this;
    }

    public MessageBuilder withRecipientKeyClientBroadcast()
    {
        this.recipientKey = WebSocketConstants.Server_To_Client_Broadcast;
        return this;
    }

    private MessageBuilder withTestId(String testId)
    {
        this.testID = testId;
        return this;
    }

    public MessageBuilder withTestId(ITestIdProvider iTestIdProvider)
    {
        if (iTestIdProvider != null)
            this.testID = iTestIdProvider.getTestId();
        return this;
    }

    public MessageBuilder withNoTestId()
    {
        this.testID = null;
        return this;
    }

    public MessageBuilder withMessageId(String messageId)
    {
        this.messageID = messageId;
        return this;
    }

    public MessageBuilder withMessageIdRandom()
    {
        this.messageID = MessageIdGenerator.getNext();
        return this;
    }


    public <T extends WebSocketMessageBase> T build(Class<T> clazz)
    {
        T msg = MessageFactory.create(clazz);
        msg.setSenderKey(senderKey);
        msg.setRecipientKey(recipientKey);
        msg.setTestID(testID);
        msg.setMessageID(messageID);
        return msg;
    }


    public static <T extends WebSocketMessageBase> T build(Class<T> clazz, ITestIdProvider iTestIdProvider, String senderKey, String recipientKey, String messageID)
    {
        return new MessageBuilder()
                .withTestId(iTestIdProvider)
                .withSenderKey(senderKey)
                .withRecipientKey(recipientKey)
                .withMessageId(messageID)
                .build(clazz);
    }


    public <R extends WebSocketMessageBase> MessageBuilder withPrepareResponse(R request)
    {
        return this
                .withMessageId(request.getMessageID())
                .withTestId(request.getTestID())
                .withRecipientKey(request.getSenderKey());
    }


}
