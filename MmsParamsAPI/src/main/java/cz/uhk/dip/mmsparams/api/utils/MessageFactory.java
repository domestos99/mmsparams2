package cz.uhk.dip.mmsparams.api.utils;

import cz.uhk.dip.mmsparams.api.exceptions.MmsParamsExceptionBase;
import cz.uhk.dip.mmsparams.api.interfaces.IClientBroadcastMessage;
import cz.uhk.dip.mmsparams.api.interfaces.IClientInfoProvider;
import cz.uhk.dip.mmsparams.api.interfaces.IPhoneInfoProvider;
import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.logging.APILoggerFactory;
import cz.uhk.dip.mmsparams.api.logging.ApiLogFacade;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketConstants;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;

public class MessageFactory
{
    private static final ILogger LOGGER = APILoggerFactory.getLogger(MessageFactory.class);

    private MessageFactory()
    {
    }

    public static <T extends WebSocketMessageBase> T create(Class<T> clz)
    {
        return create(null, clz);
    }

    public static <T extends WebSocketMessageBase> T create(ITestIdProvider iTestIdProvider, Class<T> clz)
    {
        try
        {
            Preconditions.checkNotNull(clz, "clz");
            T msg = clz.newInstance();
            msg.setMessageKey(MessageType.getKeyByClass(msg.getClass()));

            if (iTestIdProvider != null)
                msg.setTestID(iTestIdProvider.getTestId());

            return msg;
        }
        catch (Exception ex)
        {
            ApiLogFacade.logEx(LOGGER, "create", ex);
            return null;
        }
    }

    public static <T extends WebSocketMessageBase> T createServerRecipient(Class<T> clz)
    {
        return createServerRecipient(null, clz);
    }

    public static <T extends WebSocketMessageBase> T createServerSender(Class<T> clz)
    {
        return createServerSender(null, clz);
    }

    public static <T extends WebSocketMessageBase> T createServerSender(ITestIdProvider iTestIdProvider, Class<T> clz)
    {
        T msg = create(iTestIdProvider, clz);
        msg.setSenderKey(WebSocketConstants.Server_Recipient_Key);
        return msg;
    }

    public static <T extends WebSocketMessageBase & IResponseMessage> T createServerSenderResponse(Class<T> clz, WebSocketMessageBase originalMessage)
    {
        return createServerSenderResponse(null, clz, originalMessage);
    }

    public static <T extends WebSocketMessageBase & IResponseMessage> T createServerSenderResponse(ITestIdProvider iTestIdProvider, Class<T> clz, WebSocketMessageBase originalMessage)
    {
        T response = createServerSender(iTestIdProvider, clz);
        response = MessageUtils.prepareResponse(response, originalMessage);
        return response;
    }

    public static <T extends WebSocketMessageBase> T createServerRecipient(ITestIdProvider iTestIdProvider, Class<T> clz)
    {
        return new MessageBuilder()
                .withTestId(iTestIdProvider)
                .withRecipientKeyServer()
                .build(clz);
    }

    public static <T extends WebSocketMessageBase & IClientBroadcastMessage> T createClientBroadcastMessage(Class<T> clz)
    {
        return new MessageBuilder()
                .withRecipientKeyClientBroadcast()
                .build(clz);
    }

    public static RegisterPhoneMessage createRegisterPhoneMessage(IPhoneInfoProvider iPhoneInfoProvider)
    {
        RegisterPhoneMessage msg = createServerRecipient(RegisterPhoneMessage.class);

        msg.setPhoneInfoModel(iPhoneInfoProvider.getPhoneInfo());

        return msg;
    }

    public static GenericErrorResponseMessage createGenericErrorResponseMessage(String clzTag, MmsParamsExceptionBase ex, WebSocketMessageBase request, String info)
    {
        GenericErrorResponseMessage msg = create(null, GenericErrorResponseMessage.class);

        final TestErrorModel model = new TestErrorModel();
        model.setException(ex);
        if (ex != null)
            model.setTestErrorType(ex.getTestErrorType());
        model.setMessage(info);
        msg.setTestErrorModel(model);
        msg.setWebSocketMessage(JsonUtilsSafe.toJson(request));

        msg = MessageUtils.prepareResponse(msg, request);

        return msg;
    }

    @Deprecated
    public static GenericErrorResponseMessage createGenericErrorResponseMessage(String clzTag, MmsParamsExceptionBase ex, String messageID, String info)
    {
        GenericErrorResponseMessage msg = create(null, GenericErrorResponseMessage.class);

        final TestErrorModel model = new TestErrorModel();
        model.setException(ex);
        model.setMessage(info);
        if (ex != null)
            model.setTestErrorType(ex.getTestErrorType());
        msg.setTestErrorModel(model);
        msg.setMessageID(messageID);

        return msg;
    }

    public static RegisterClientLibMessage createRegisterClientLibMessage(ITestIdProvider iTestIdProvider, IClientInfoProvider iClientInfoProvider)
    {
        return createRegisterClientLibMessage(iTestIdProvider, iClientInfoProvider.getClientInfo());
    }

    public static RegisterClientLibMessage createRegisterClientLibMessage(ITestIdProvider iTestIdProvider, ClientInfo clientInfo)
    {
        Preconditions.checkNotNull(iTestIdProvider, "ITestIdProvider is required!");

        RegisterClientLibMessage msg = createServerRecipient(iTestIdProvider, RegisterClientLibMessage.class);

        msg.setClientInfo(clientInfo);

        return msg;
    }


}
