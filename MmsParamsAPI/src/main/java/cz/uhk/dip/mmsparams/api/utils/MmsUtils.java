package cz.uhk.dip.mmsparams.api.utils;

import cz.uhk.dip.mmsparams.api.enums.MessageClass;
import cz.uhk.dip.mmsparams.api.enums.Priority;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;

public class MmsUtils
{
    private MmsUtils()
    {
    }

    public static final long DEFAULT_EXPIRY = 7L * 24L * 60L * 60L;
    public static final long DEFAULT_DELIVERY_TIME = 0L;

    public static MmsSendModel setDefaultTestProfile(final MmsSendModel mms)
    {
        mms.setDeliveryReport(true);
        mms.setReadReport(false);
        mms.setSenderVisible(true);

        mms.setExpiry(DEFAULT_EXPIRY);
        mms.setDeliveryTime(DEFAULT_DELIVERY_TIME);

        mms.setPriority(Priority.NORMAL);
        mms.setMsgClass(MessageClass.PERSONAL);

        return mms;
    }
}
