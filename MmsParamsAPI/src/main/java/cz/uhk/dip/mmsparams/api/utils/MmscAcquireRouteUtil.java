package cz.uhk.dip.mmsparams.api.utils;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MmscAcquireRouteModel;

public class MmscAcquireRouteUtil
{
    private MmscAcquireRouteUtil()
    {
    }

    public static boolean isMatch(final MmscAcquireRouteModel mmscAcquireRouteModel, final List<MM7Address> recipients)
    {
        if (mmscAcquireRouteModel == null || recipients == null)
            return false;

        for (MM7Address adr : recipients)
        {
            final boolean match = isMatch(mmscAcquireRouteModel, adr);
            if (match)
                return true;
        }

        return false;
    }

    public static boolean isMatch(final MmscAcquireRouteModel mmscAcquireRouteModel, final MM7Address recipients)
    {
        String pattern = mmscAcquireRouteModel.getPattern();
        String address = recipients.getAddress();

        if (StringUtil.isEmptyOrNull(pattern) || StringUtil.isEmptyOrNull(address))
            return false;

        if (pattern.length() > address.length())
            return false;

        if (pattern.equals(address))
            return true;

        for (int i = 0; i < pattern.length(); i++)
        {
            if (pattern.charAt(i) == '*')
                return true;

            if (pattern.charAt(i) != address.charAt(i))
                return false;
        }

        return false;
    }
}
