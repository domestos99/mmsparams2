package cz.uhk.dip.mmsparams.api.utils;

import java.util.Optional;

public class OptionalUtil
{
    private OptionalUtil()
    {
    }

    public static <T> Optional<T> fillOptional(T obj)
    {
        if (obj == null)
            return Optional.empty();
        return Optional.of(obj);
    }
}
