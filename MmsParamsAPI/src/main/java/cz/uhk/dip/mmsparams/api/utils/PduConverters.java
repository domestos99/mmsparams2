package cz.uhk.dip.mmsparams.api.utils;

import cz.uhk.dip.mmsparams.api.constants.PduHeaders2;
import cz.uhk.dip.mmsparams.api.enums.MessageClass;
import cz.uhk.dip.mmsparams.api.enums.Priority;

public class PduConverters
{
    private PduConverters()
    {
    }

    public static int getYesNoValue(boolean pduValue)
    {
        return pduValue ? PduHeaders2.VALUE_YES : PduHeaders2.VALUE_NO;
    }

    public static boolean getYesNoValue(int pduValue)
    {
        return pduValue == PduHeaders2.VALUE_YES;
    }

    public static byte[] getClassValue(MessageClass msgClass)
    {
        switch (msgClass)
        {
            case PERSONAL:
                return PduHeaders2.MESSAGE_CLASS_PERSONAL_STR.getBytes();
            case ADVERTISEMENT:
                return PduHeaders2.MESSAGE_CLASS_ADVERTISEMENT_STR.getBytes();
            case INFORMATIONAL:
                return PduHeaders2.MESSAGE_CLASS_INFORMATIONAL_STR.getBytes();
            case AUTO:
                return PduHeaders2.MESSAGE_CLASS_AUTO_STR.getBytes();
            default:
                return PduHeaders2.MESSAGE_CLASS_PERSONAL_STR.getBytes();
        }

    }

    public static int getPriority(Priority priority)
    {
        switch (priority)
        {
            case LOW:
                return PduHeaders2.PRIORITY_LOW;
            case NORMAL:
                return PduHeaders2.PRIORITY_NORMAL;
            case HIGH:
                return PduHeaders2.PRIORITY_HIGH;
            default:
                return PduHeaders2.PRIORITY_NORMAL;
        }

    }


    public static MessageClass getMsgClass(String messageClass)
    {
        switch (messageClass.toLowerCase())
        {
            case PduHeaders2.MESSAGE_CLASS_PERSONAL_STR:
                return MessageClass.PERSONAL;
            case PduHeaders2.MESSAGE_CLASS_ADVERTISEMENT_STR:
                return MessageClass.ADVERTISEMENT;
            case PduHeaders2.MESSAGE_CLASS_INFORMATIONAL_STR:
                return MessageClass.INFORMATIONAL;
            case PduHeaders2.MESSAGE_CLASS_AUTO_STR:
                return MessageClass.AUTO;
            default:
                return null;
        }
    }

    public static Boolean getDeliveryReport(int deliveryReport)
    {
        return getYesNoValue(deliveryReport);
    }

    public static Boolean getReadReport(int readReport)
    {
        return getYesNoValue(readReport);
    }

    public static Priority getMsgPriority(int priority)
    {
        switch (priority)
        {
            case PduHeaders2.PRIORITY_LOW:
                return Priority.LOW;
            case PduHeaders2.PRIORITY_NORMAL:
                return Priority.NORMAL;
            case PduHeaders2.PRIORITY_HIGH:
                return Priority.HIGH;
            default:
                return null;
        }
    }


}
