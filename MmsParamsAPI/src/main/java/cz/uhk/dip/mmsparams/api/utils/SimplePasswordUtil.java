package cz.uhk.dip.mmsparams.api.utils;

import java.util.Base64;

import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;

public class SimplePasswordUtil
{
    private static final String SEPARATOR = ":";

    public static String encrypt(IBase64Converter iBase64Converter, JwtRequest jwtRequest)
    {
        if (jwtRequest == null || StringUtil.isEmptyOrNull(jwtRequest.getUsername())
                || StringUtil.isEmptyOrNull(jwtRequest.getPassword()))
            return null;

        String authString = jwtRequest.getUsername() + SEPARATOR + jwtRequest.getPassword();
        return iBase64Converter.encodeToString(authString.getBytes());
    }

    /**
     * Decrypt base64 username:password
     *
     * @param param base64 username:password to decrypt
     * @return username-password pair
     */
    public static JwtRequest decrypt(IBase64Converter iBase64Converter, String param)
    {
        if (StringUtil.isEmptyOrNull(param))
        {
            return new JwtRequest(null, null);
        }
        String s = new String(iBase64Converter.decode(param));
        String[] up = s.split(SEPARATOR);
        if (up.length == 2)
        {
            return new JwtRequest(up[0], up[1]);
        }
        return new JwtRequest(null, null);
    }
}
