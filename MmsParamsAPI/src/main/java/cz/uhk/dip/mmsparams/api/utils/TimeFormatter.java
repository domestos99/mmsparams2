package cz.uhk.dip.mmsparams.api.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeFormatter
{
    private TimeFormatter()
    {
    }

    public static String format(long millis)
    {
        Date date = new Date(millis);
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
        return formatter.format(date);
    }
}
