package cz.uhk.dip.mmsparams.api.utils;

import java.util.Date;

public class TimeoutHelper
{
    private TimeoutHelper()
    {
    }

    /**
     * Check whether timeout occurred
     *
     * @param startDt    start time for check timeout
     * @param timeoutSec timeout in seconds
     * @return true if timeout occurred
     */
    public static synchronized boolean checkTimeOut(final Date startDt, final int timeoutSec)
    {
        // Returns the number of milliseconds
        return timeoutSec * 1000 < (new Date().getTime() - startDt.getTime());

    }
}
