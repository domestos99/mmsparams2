package cz.uhk.dip.mmsparams.api.utils;

public class TlvHelper
{
    private TlvHelper()
    {
    }

    public static String getTlvName(short tlvTag)
    {
        switch (tlvTag)
        {
            case 0x0019:
                return "TAG_PAYLOAD_TYPE";
            case 0x0201:
                return "TAG_PRIVACY_INDICATOR";
            case 0x0204:
                return "TAG_USER_MESSAGE_REFERENCE";
            case 0x0205:
                return "TAG_USER_RESPONSE_CODE";
            case 0x020A:
                return "TAG_SOURCE_PORT";
            case 0x020B:
                return "TAG_DESTINATION_PORT";
            case 0x020C:
                return "TAG_SAR_MSG_REF_NUM";
            case 0x020D:
                return "TAG_LANGUAGE_INDICATOR";
            case 0x020E:
                return "TAG_SAR_TOTAL_SEGMENTS";
            case 0x020F:
                return "TAG_SAR_SEGMENT_SEQNUM";
            case 0x0202:
                return "TAG_SOURCE_SUBADDRESS";
            case 0x0203:
                return "TAG_DEST_SUBADDRESS";
            case 0x0381:
                return "TAG_CALLBACK_NUM";
            case 0x0424:
                return "TAG_MESSAGE_PAYLOAD";
            case 0x0210:
                return "TAG_SC_INTERFACE_VERSION";
            case 0x1201:
                return "TAG_DISPLAY_TIME";
            case 0x1204:
                return "TAG_MS_VALIDITY";
            case 0x0420:
                return "TAG_DPF_RESULT";
            case 0x0421:
                return "TAG_SET_DPF";
            case 0x0422:
                return "TAG_MS_AVAIL_STATUS";
            case 0x0423:
                return "TAG_NETWORK_ERROR_CODE";
            case 0x0425:
                return "TAG_DELIVERY_FAILURE_REASON";
            case 0x0426:
                return "TAG_MORE_MSGS_TO_FOLLOW";
            case 0x0427:
                return "TAG_MSG_STATE";
            case 0x0302:
                return "TAG_CALLBACK_NUM_PRES_IND";
            case 0x0303:
                return "TAG_CALLBACK_NUM_ATAG";
            case 0x0304:
                return "TAG_NUM_MSGS";
            case 0x1203:
                return "TAG_SMS_SIGNAL";
            case 0x130C:
                return "TAG_ALERT_ON_MSG_DELIVERY";
            case 0x1380:
                return "TAG_ITS_REPLY_TYPE";
            case 0x1383:
                return "TAG_ITS_SESSION_INFO";
            case 0x0501:
                return "TAG_USSD_SERVICE_OP";
            // Tohle je pekna blbost, je ale je tak v origo souboru SmppConstants v SMPP lib
            case (short) 0x8081:
                return "TAG_ORIG_MSC_ADDR";
            case (short) 0x8082:
                return "TAG_DEST_MSC_ADDR";
            case 0x0005:
                return "TAG_DEST_ADDR_SUBUNIT";
            case 0x0006:
                return "TAG_DEST_NETWORK_TYPE";
            case 0x0007:
                return "TAG_DEST_BEAR_TYPE";
            case 0x0008:
                return "TAG_DEST_TELE_ID";
            case 0x000D:
                return "TAG_SOURCE_ADDR_SUBUNIT";
            case 0x000E:
                return "TAG_SOURCE_NETWORK_TYPE";
            case 0x000F:
                return "TAG_SOURCE_BEAR_TYPE";
            case 0x0010:
                return "TAG_SOURCE_TELE_ID";
            case 0x0017:
                return "TAG_QOS_TIME_TO_LIVE";
            case 0x001D:
                return "TAG_ADD_STATUS_INFO";
            case 0x001E:
                return "TAG_RECEIPTED_MSG_ID";
            default:
                return String.valueOf(tlvTag);
        }
    }


}
