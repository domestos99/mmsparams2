package cz.uhk.dip.mmsparams.api.utils;

import cz.uhk.dip.mmsparams.api.constants.smsc.SMSCConstant;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.TlvModel;

public class TlvUtils
{
    private TlvUtils()
    {
    }

    public static TlvModel createMessageTlv(String text)
    {
        return createMessageTlv(text.getBytes());
    }

    public static TlvModel createMessageTlv(byte[] byteText)
    {
        TlvModel tlv = new TlvModel();
        tlv.setTag(SMSCConstant.TAG_MESSAGE_PAYLOAD);
        tlv.setValue(byteText);
        return tlv;
    }
}
