package cz.uhk.dip.mmsparams.api.utils;

import java.util.ArrayList;
import java.util.List;


public class TupleUtils
{
    private TupleUtils()
    {
    }

    public static <R, T> List<R> getXOnly(List<Tuple<R, T>> tuples)
    {
        List<R> result = new ArrayList<>();
        for (Tuple<R, T> t : tuples)
        {
            result.add(t.getX());
        }
        return result;
    }

    public static <R, T> List<T> getYOnly(List<Tuple<R, T>> tuples)
    {
        List<T> result = new ArrayList<>();
        for (Tuple<R, T> t : tuples)
        {
            result.add(t.getY());
        }
        return result;
    }
}
