package cz.uhk.dip.mmsparams.api.utils;

import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;

public class UrlUtils
{
    private UrlUtils()
    {
    }

    public static String getServerSocketUrl(HttpProtocolEnum httpProtocolEnum, String address)
    {
        return HttpConstants.getWsPrefix(httpProtocolEnum) + address + "/appsocket";
    }

    public static String getServerSocketUrl(HttpProtocolEnum httpProtocolEnum, String address, String port)
    {
        return HttpConstants.getWsPrefix(httpProtocolEnum) + address + ":" + port + "/appsocket";
    }
}
