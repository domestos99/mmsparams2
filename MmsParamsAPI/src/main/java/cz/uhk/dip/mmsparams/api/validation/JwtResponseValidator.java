package cz.uhk.dip.mmsparams.api.validation;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.http.auth.JwtResponse;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;

public class JwtResponseValidator
{
    private JwtResponseValidator()
    {
    }

    public static boolean validate(JwtResponse jwtResponse)
    {
        Preconditions.checkNotNull(jwtResponse, GenericConstants.JWT_RESPONSE);
        Preconditions.checkNotNullOrEmpty(jwtResponse.getToken(), GenericConstants.JWTTOKEN);
        Preconditions.isGreaterZero(jwtResponse.getExpiry(), GenericConstants.EXPIRY);
        return true;
    }
}
