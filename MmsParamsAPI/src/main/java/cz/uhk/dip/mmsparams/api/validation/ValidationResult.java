package cz.uhk.dip.mmsparams.api.validation;

import java.io.Serializable;

public class ValidationResult implements Serializable
{
    private String validationItemName;
    private Object expected;
    private Object actual;
    private String validationName;
    private Boolean result;

    public ValidationResult()
    {
    }

    public ValidationResult(String validationItemName, Object expected, Object actual, String validationName, Boolean result)
    {
        this.validationItemName = validationItemName;
        this.expected = expected;
        this.actual = actual;
        this.validationName = validationName;
        this.result = result;
    }

    public String getValidationItemName()
    {
        return validationItemName;
    }

    public void setValidationItemName(String validationItemName)
    {
        this.validationItemName = validationItemName;
    }

    public Object getExpected()
    {
        return expected;
    }

    public void setExpected(Object expected)
    {
        this.expected = expected;
    }

    public Object getActual()
    {
        return actual;
    }

    public void setActual(Object actual)
    {
        this.actual = actual;
    }

    public String getValidationName()
    {
        return validationName;
    }

    public void setValidationName(String validationName)
    {
        this.validationName = validationName;
    }

    public Boolean getResult()
    {
        return result;
    }

    public void setResult(Boolean result)
    {
        this.result = result;
    }

    @Override
    public String toString()
    {
        return "ValidationResult{" +
                "validationItemName='" + validationItemName + '\'' +
                ", expected=" + expected +
                ", actual=" + actual +
                ", validationName='" + validationName + '\'' +
                ", result=" + result +
                '}';
    }
}
