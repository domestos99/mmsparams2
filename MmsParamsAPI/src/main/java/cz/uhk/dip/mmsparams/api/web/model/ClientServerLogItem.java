package cz.uhk.dip.mmsparams.api.web.model;

import java.io.Serializable;
import java.util.Objects;

import cz.uhk.dip.mmsparams.api.enums.LogLevel;

public class ClientServerLogItem implements Serializable
{
    private long createdDT;
    private String log;
    private LogLevel logLevel;

    public long getCreatedDT()
    {
        return createdDT;
    }

    public void setCreatedDT(long createdDT)
    {
        this.createdDT = createdDT;
    }

    public String getLog()
    {
        return log;
    }

    public void setLog(String log)
    {
        this.log = log;
    }

    public LogLevel getLogLevel()
    {
        return logLevel;
    }

    public void setLogLevel(LogLevel logLevel)
    {
        this.logLevel = logLevel;
    }

    @Override
    public String toString()
    {
        return "ClientServerLogItem{" +
                "createdDT=" + createdDT +
                ", log='" + log + '\'' +
                ", logLevel=" + logLevel +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientServerLogItem that = (ClientServerLogItem) o;
        return createdDT == that.createdDT &&
                Objects.equals(log, that.log) &&
                logLevel == that.logLevel;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(createdDT, log, logLevel);
    }
}
