package cz.uhk.dip.mmsparams.api.web.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ClientServerLogModel implements Serializable
{
    private String testID;
    private List<ClientServerLogItem> clientServerLogItems;

    public ClientServerLogModel()
    {
        this.clientServerLogItems = new ArrayList<>();
    }

    public String getTestID()
    {
        return testID;
    }

    public void setTestID(String testID)
    {
        this.testID = testID;
    }

    public List<ClientServerLogItem> getClientServerLogItems()
    {
        return clientServerLogItems;
    }

    public void setClientServerLogItems(List<ClientServerLogItem> clientServerLogItems)
    {
        this.clientServerLogItems = clientServerLogItems;
    }

    @Override
    public String toString()
    {
        return "ClientServerLogModel{" +
                "testID='" + testID + '\'' +
                ", clientServerLogItems=" + clientServerLogItems +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientServerLogModel that = (ClientServerLogModel) o;
        return Objects.equals(testID, that.testID) &&
                Objects.equals(clientServerLogItems, that.clientServerLogItems);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(testID, clientServerLogItems);
    }
}
