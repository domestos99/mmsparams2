package cz.uhk.dip.mmsparams.api.websocket;

import java.util.UUID;

public class MessageIdGenerator
{
    private MessageIdGenerator()
    {
    }

    public static String getNext()
    {
        return UUID.randomUUID().toString();
    }
}
