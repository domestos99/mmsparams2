package cz.uhk.dip.mmsparams.api.websocket;

import java.util.Hashtable;
import java.util.Map;

import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.KeepAliveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.email.EmailReceiveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.TestErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsRecipientPhoneProfileRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.AcknowledgeIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.DeliveryIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotifyRespIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadOrigIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadRecIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendReqResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReportReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ReadReplyReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7SubmitResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneAllPartsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDisconnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;

public class MessageType
{
    private MessageType()
    {
    }

    public static final String Register_Phone_Message = RegisterPhoneMessage.class.getSimpleName().toUpperCase();
    public static final String Phone_List_Request_Message = PhoneListRequestMessage.class.getSimpleName().toUpperCase();
    public static final String Phone_List_Response_Message = PhoneListResponseMessage.class.getSimpleName().toUpperCase();
    public static final String Sms_SendPhone_Request_Message = SmsSendPhoneRequestMessage.class.getSimpleName().toUpperCase();
    public static final String Sms_ReceivePhone_Message = SmsReceivePhoneMessage.class.getSimpleName().toUpperCase();

    public static final String Sms_SendPhone_Response_Message = SmsSendPhoneResponseMessage.class.getSimpleName().toUpperCase();
    public static final String Sms_Send_Delivery_Response_Message = SmsSendDeliveryReportMessage.class.getSimpleName().toUpperCase();
    public static final String Mms_SendPhone_Request_Message = MmsSendPhoneRequestMessage.class.getSimpleName().toUpperCase();


    public static final String Dev_Message = DevMessage.class.getSimpleName().toUpperCase();

    public static final String Generic_Boolean_Response_Message = GenericBooleanResponseMessage.class.getSimpleName().toUpperCase();


    public static final String MmsRecipient_Phone_Profile_Request_Message = MmsRecipientPhoneProfileRequestMessage.class.getSimpleName().toUpperCase();

    public static final String Generic_Error_Response_Message = GenericErrorResponseMessage.class.getSimpleName().toUpperCase();

    public static final String Register_ClientLib_Message = RegisterClientLibMessage.class.getSimpleName().toUpperCase();

    public static final String SendConf_Response_Message = SendConfResponseMessage.class.getSimpleName().toUpperCase();
    public static final String NotificationInd_Response_Message = NotificationIndResponseMessage.class.getSimpleName().toUpperCase();
    public static final String DeliveryInd_Response_Message = DeliveryIndResponseMessage.class.getSimpleName().toUpperCase();
    public static final String ReadOrigInd_Response_Message = ReadOrigIndResponseMessage.class.getSimpleName().toUpperCase();
    public static final String AcknowledgeInd_Response_Message = AcknowledgeIndResponseMessage.class.getSimpleName().toUpperCase();
    public static final String RetrieveConf_Response_Message = RetrieveConfResponseMessage.class.getSimpleName().toUpperCase();
    public static final String ReadRecInd_Response_Message = ReadRecIndResponseMessage.class.getSimpleName().toUpperCase();
    public static final String NotifyRespInd_Response_Message = NotifyRespIndResponseMessage.class.getSimpleName().toUpperCase();
    public static final String SendReq_Response_Message = SendReqResponseMessage.class.getSimpleName().toUpperCase();

    public static final String Smsc_DeliverSm_Message = SmscDeliverSmMessage.class.getSimpleName().toUpperCase();
    public static final String Smsc_Disconnect_Message = SmscDisconnectMessage.class.getSimpleName().toUpperCase();

    public static final String Mmsc_Send_Message = MmscSendMessage.class.getSimpleName().toUpperCase();
    public static final String MM7_DeliveryReq_Message = MM7DeliveryReqMessage.class.getSimpleName().toUpperCase();
    public static final String MM7_Delivery_ReportReq_Message = MM7DeliveryReportReqMessage.class.getSimpleName().toUpperCase();
    public static final String MM7_ReadReplyReq_Message = MM7ReadReplyReqMessage.class.getSimpleName().toUpperCase();


    public static final String Smsc_SendSmsResponse_Message = SmscSendSmsResponseMessage.class.getSimpleName().toUpperCase();
    public static final String Smsc_Connect_Message = SmscConnectMessage.class.getSimpleName().toUpperCase();
    public static final String Smsc_SendSms_Message = SmscSendSmsMessage.class.getSimpleName().toUpperCase();
    public static final String Smsc_DeliveryReport_Message = SmscDeliveryReportMessage.class.getSimpleName().toUpperCase();
    public static final String Smsc_Connect_Response_Message = SmscConnectResponseMessage.class.getSimpleName().toUpperCase();

    public static final String Lock_Phone_Message = LockPhoneMessage.class.getSimpleName().toUpperCase();
    public static final String UnLock_Phone_Message = UnLockPhoneMessage.class.getSimpleName().toUpperCase();


    public static final String Locked_Phones_List_Request = LockedPhonesListRequestMessage.class.getSimpleName().toUpperCase();
    public static final String Locked_Phones_List_Response = LockedPhonesListResponseMessage.class.getSimpleName().toUpperCase();

    public static final String MM7_Submit_Response_Message = MM7SubmitResponseMessage.class.getSimpleName().toUpperCase();

    public static final String Mmsc_AcquireRoute_Request_Message = MmscAcquireRouteRequestMessage.class.getSimpleName().toUpperCase();
    public static final String Sms_ReceivePhone_AllParts_Message = SmsReceivePhoneAllPartsMessage.class.getSimpleName().toUpperCase();

    public static final String Test_Validation_Message = TestValidationMessage.class.getSimpleName().toUpperCase();

    public static final String Test_Error_Message = TestErrorMessage.class.getSimpleName().toUpperCase();
    public static final String MM7_Error_Message = MM7ErrorMessage.class.getSimpleName().toUpperCase();
    public static final String Keep_Alive_Message = KeepAliveMessage.class.getSimpleName().toUpperCase();

    public static final String Test_Result_Message = TestResultMessage.class.getSimpleName().toUpperCase();

    public static final String Email_Receive_Message = EmailReceiveMessage.class.getSimpleName().toUpperCase();


    private static Hashtable<String, Class> Types;

    static
    {
        Types = new Hashtable<String, Class>();
        addType(Types, RegisterPhoneMessage.class);
        addType(Types, EmptyMessage.class);
        addType(Types, PhoneListRequestMessage.class);
        addType(Types, PhoneListResponseMessage.class);
        addType(Types, SmsSendPhoneRequestMessage.class);
        addType(Types, SmsReceivePhoneMessage.class);
        addType(Types, SmsSendPhoneResponseMessage.class);
        addType(Types, SmsSendDeliveryReportMessage.class);
        addType(Types, MmsSendPhoneRequestMessage.class);
        addType(Types, DevMessage.class);
        addType(Types, SmsReceivePhoneAllPartsMessage.class);


        addType(Types, MmsRecipientPhoneProfileRequestMessage.class);

        addType(Types, GenericBooleanResponseMessage.class);
        addType(Types, GenericErrorResponseMessage.class);

        addType(Types, SendConfResponseMessage.class);
        addType(Types, NotificationIndResponseMessage.class);
        addType(Types, DeliveryIndResponseMessage.class);
        addType(Types, ReadOrigIndResponseMessage.class);
        addType(Types, AcknowledgeIndResponseMessage.class);
        addType(Types, RetrieveConfResponseMessage.class);
        addType(Types, ReadRecIndResponseMessage.class);
        addType(Types, NotifyRespIndResponseMessage.class);

        addType(Types, SmscConnectMessage.class);
        addType(Types, SmscSendSmsMessage.class);

        addType(Types, SmscDeliverSmMessage.class);


        addType(Types, MmscSendMessage.class);
        addType(Types, MM7DeliveryReqMessage.class);
        addType(Types, MM7DeliveryReportReqMessage.class);
        addType(Types, MM7ReadReplyReqMessage.class);
        addType(Types, SendReqResponseMessage.class);
        addType(Types, SmscDisconnectMessage.class);
        addType(Types, SmscSendSmsResponseMessage.class);
        addType(Types, SmscDeliveryReportMessage.class);
        addType(Types, SmscConnectResponseMessage.class);

        addType(Types, RegisterClientLibMessage.class);


        addType(Types, LockPhoneMessage.class);
        addType(Types, UnLockPhoneMessage.class);
        addType(Types, LockedPhonesListRequestMessage.class);
        addType(Types, LockedPhonesListResponseMessage.class);

        addType(Types, MM7SubmitResponseMessage.class);
        addType(Types, MmscAcquireRouteRequestMessage.class);

        addType(Types, TestValidationMessage.class);

        addType(Types, TestErrorMessage.class);
        addType(Types, MM7ErrorMessage.class);
        addType(Types, KeepAliveMessage.class);
        addType(Types, TestResultMessage.class);
        addType(Types, EmailReceiveMessage.class);

    }

    private static void addType(Map<String, Class> t, Class clz)
    {
        t.put(getMsgKey(clz), clz);
    }

    private static String getMsgKey(Class clz)
    {
        return clz.getSimpleName().toUpperCase();
    }

    public static Class<WebSocketMessageBase> getClassByKey(String key)
    {
        Preconditions.checkNotNullOrEmpty(key, "key");
        return Types.get(key);
    }

    public static String getKeyByClass(Class clz)
    {
        Preconditions.checkNotNull(clz, "clz");
        for (Map.Entry<String, Class> o : Types.entrySet())
        {
            if (o.getValue().equals(clz))
            {
                return o.getKey();
            }
        }
        return null;
    }


    public static void setCreatedDT(WebSocketMessageBase msg)
    {
        msg.setCreatedDT(System.currentTimeMillis());
    }
}
