package cz.uhk.dip.mmsparams.api.websocket;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.logging.APILoggerFactory;
import cz.uhk.dip.mmsparams.api.logging.ApiLogFacade;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;

public class MessageUtils
{
    private static final ILogger LOGGER = APILoggerFactory.getLogger(MessageUtils.class);

    private MessageUtils()
    {
    }

    @Deprecated
    public static String getSendableMessage(WebSocketMessageBase message)
    {
        if (message == null)
        {
            ApiLogFacade.logWarning(LOGGER, "getSendableMessage: message is null!");
            return null;
        }

        if (StringUtil.isEmptyOrNull(message.getMessageKey()))
            ApiLogFacade.logDebug(LOGGER, "getSendableMessage - messageKey not set");
        message.setMessageKey(MessageType.getKeyByClass(message.getClass()));
        return JsonUtilsSafe.toJson(message);
    }

    public static <T extends WebSocketMessageBase & IResponseMessage, R extends WebSocketMessageBase> T prepareResponse(final T response, final R request)
    {
        Preconditions.checkNotNull(response, "Response cannot be null");
        Preconditions.checkNotNull(request, "Request cannot be null");

        if (response == request)
        {
            throw new RuntimeException("Request and response cannot be the same");
        }

        response.setMessageID(request.getMessageID());
        response.setTestID(request.getTestID());
        response.setRecipientKey(request.getSenderKey());

        return response;
    }

    public static boolean isMessageValid(WebSocketMessageBase message)
    {
        if (message == null)
            return false;

        if (StringUtil.isEmptyOrNull(message.getMessageKey()))
            return false;

        Class clz = MessageType.getClassByKey(message.getMessageKey());
        if (clz == null)
            return false;


        if (StringUtil.isEmptyOrNull(message.getMessageID()))
            return false;

        if (StringUtil.isEmptyOrNull(message.getSenderKey()))
            return false;

        if (StringUtil.isEmptyOrNull(message.getRecipientKey()))
            return false;

        if (StringUtil.isEmptyOrNull(message.getTestID()))
        {
            ApiLogFacade.logWarning(LOGGER, "isMessageValid: testID missing: " + message);
        }

        return true;
    }

    public static WebSocketMessageBase getMessageNonTyped(final String payload)
    {
        Preconditions.checkNotNullOrEmpty(payload, "payload");
        final EmptyMessage emptyMessage = JsonUtilsSafe.fromJson(payload, EmptyMessage.class);
        Preconditions.checkNotNull(emptyMessage, "emptyMessage");
        final String msgKey = emptyMessage.getMessageKey();
        Preconditions.checkNotNullOrEmpty(msgKey, "msgKey");

        final Class<WebSocketMessageBase> clz = MessageType.getClassByKey(msgKey);

        return JsonUtilsSafe.fromJson(payload, clz);
    }

    public static <T extends WebSocketMessageBase> List<T> copyList(List<WebSocketMessageBase> response)
    {
        List<T> l = new ArrayList<>();
        if (response == null)
        {
            ApiLogFacade.logWarning(LOGGER, "copyList: list cannot be null");
            return l;
        }

        for (WebSocketMessageBase msg : response)
        {
            l.add((T) msg);
        }
        return l;
    }

    public static String getMessageKey(final String payload)
    {
        Preconditions.checkNotNullOrEmpty(payload, "payload");
        final EmptyMessage emptyMessage = JsonUtilsSafe.fromJson(payload, EmptyMessage.class);
        Preconditions.checkNotNull(emptyMessage, "emptyMessage");
        return emptyMessage.getMessageKey();
    }

    public static boolean isRecipientPhone(String recipientKey)
    {
        return recipientKey.startsWith(WebSocketConstants.Android_Key_Prefix);
    }
}
