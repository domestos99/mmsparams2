package cz.uhk.dip.mmsparams.api.websocket;

public class WebSocketConstants
{
    private WebSocketConstants()
    {
    }

    public static final String Server_Recipient_Key = "SERVER";
    public static final String Server_To_Client_Broadcast = "SERVERTOCLIENTBROADCAST";
    public static final String Android_Key_Prefix = "Andr_";
}
