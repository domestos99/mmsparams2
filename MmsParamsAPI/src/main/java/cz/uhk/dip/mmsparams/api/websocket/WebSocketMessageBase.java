package cz.uhk.dip.mmsparams.api.websocket;

import java.io.Serializable;

/**
 * Parent for all messages which are send or received via WebSocket connection
 * It is required, that each message has this class as parent
 */
public abstract class WebSocketMessageBase implements Serializable
{
    private String senderKey;
    private String recipientKey;
    private String messageKey;
    private String messageID;
    private String testID;
    private long createdDT;

    public WebSocketMessageBase()
    {
        this.createdDT = System.currentTimeMillis();
    }

    /**
     * Get SenderKey - identification of message sender
     *
     * @return senderKey
     */
    public String getSenderKey()
    {
        return senderKey;
    }

    public void setSenderKey(String senderKey)
    {
        this.senderKey = senderKey;
    }

    /**
     * Get RecipientKey - identification of message recipient
     *
     * @return recipientKey
     */
    public String getRecipientKey()
    {
        return recipientKey;
    }

    public void setRecipientKey(String recipientKey)
    {
        this.recipientKey = recipientKey;
    }

    /**
     * Get MessageKey - identification of message type (key)
     *
     * @return messageKey
     */
    public String getMessageKey()
    {
        return messageKey;
    }

    public void setMessageKey(String messageKey)
    {
        this.messageKey = messageKey;
    }

    /**
     * Get MessageID - identification of message id (unique identification)
     *
     * @return messageID
     */
    public String getMessageID()
    {
        return messageID;
    }

    public void setMessageID(String messageID)
    {
        this.messageID = messageID;
    }

    /**
     * Get TestID - identification in what text message belongs
     *
     * @return testID
     */
    public String getTestID()
    {
        return testID;
    }

    public void setTestID(String testID)
    {
        this.testID = testID;
    }

    /**
     * Get CreatedDT - when message was created
     *
     * @return createdDT
     */
    public long getCreatedDT()
    {
        return createdDT;
    }

    public void setCreatedDT(long createdDT)
    {
        this.createdDT = createdDT;
    }

    @Override
    public String toString()
    {
        return "WebSocketMessageBase{" +
                "senderKey='" + senderKey + '\'' +
                ", recipientKey='" + recipientKey + '\'' +
                ", messageKey='" + messageKey + '\'' +
                ", messageID='" + messageID + '\'' +
                ", testID='" + testID + '\'' +
                ", createdDT=" + createdDT +
                '}';
    }
}
