package cz.uhk.dip.mmsparams.api.websocket.listeners;

public interface IMessageReceiveListener<T>
{
    void onReceive(T message);
}
