package cz.uhk.dip.mmsparams.api.websocket.listeners;

import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.KeepAliveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.email.EmailReceiveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.TestErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsRecipientPhoneProfileRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.AcknowledgeIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.DeliveryIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotifyRespIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadOrigIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadRecIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendReqResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReportReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ReadReplyReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7SubmitResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneAllPartsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDisconnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;

public interface IMessageReceiveSub<T>
{
    T onReceiveUnknown(String msg);

    T onReceive(PhoneListRequestMessage msg);

    T onReceive(RegisterPhoneMessage msg);

    T onReceive(EmptyMessage msg);

    T onReceive(PhoneListResponseMessage msg);

    T onReceive(SmsSendPhoneRequestMessage msg);

    T onReceive(SmsReceivePhoneMessage msg);

    T onReceive(SmsReceivePhoneAllPartsMessage msg);

    T onReceive(SmsSendPhoneResponseMessage msg);

    T onReceive(SmsSendDeliveryReportMessage msg);

    T onReceive(MmsSendPhoneRequestMessage msg);

    T onReceive(DevMessage msg);

    T onReceive(MmsRecipientPhoneProfileRequestMessage msg);

    T onReceive(GenericBooleanResponseMessage msg);

    T onReceive(GenericErrorResponseMessage msg);

    T onReceive(SendConfResponseMessage msg);

    T onReceive(RegisterClientLibMessage msg);

    T onReceive(NotificationIndResponseMessage msg);

    T onReceive(DeliveryIndResponseMessage msg);

    T onReceive(ReadOrigIndResponseMessage msg);

    T onReceive(AcknowledgeIndResponseMessage msg);

    T onReceive(RetrieveConfResponseMessage msg);

    T onReceive(ReadRecIndResponseMessage msg);

    T onReceive(NotifyRespIndResponseMessage msg);

    T onReceive(MM7DeliveryReqMessage msg);

    T onReceive(MM7DeliveryReportReqMessage msg);

    T onReceive(MM7ReadReplyReqMessage msg);


    T onReceive(SmscConnectMessage msg);

    T onReceive(SmscSendSmsMessage msg);

    T onReceive(SmscDeliverSmMessage msg);

    T onReceive(SmscDisconnectMessage msg);

    T onReceive(SmscDeliveryReportMessage msg);

    T onReceive(SmscConnectResponseMessage msg);


    T onReceive(SmscSendSmsResponseMessage msg);

    T onReceive(UnLockPhoneMessage msg);

    T onReceive(SendReqResponseMessage msg);

    T onReceive(MmscSendMessage msg);

    T onReceive(LockPhoneMessage msg);

    T onReceive(LockedPhonesListResponseMessage msg);

    T onReceive(LockedPhonesListRequestMessage msg);

    T onReceive(MM7SubmitResponseMessage msg);

    T onReceive(MmscAcquireRouteRequestMessage msg);

    T onReceive(TestValidationMessage msg);

    T onReceive(TestErrorMessage msg);

    T onReceive(MM7ErrorMessage msg);

    T onReceive(KeepAliveMessage msg);

    T onReceive(TestResultMessage msg);

    T onReceive(EmailReceiveMessage msg);
}
