package cz.uhk.dip.mmsparams.api.websocket.messages;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class DevMessage extends WebSocketMessageBase implements Serializable
{
    private String testContent;

    public String getTestContent()
    {
        return testContent;
    }

    public void setTestContent(String testContent)
    {
        this.testContent = testContent;
    }

    @Override
    public String toString()
    {
        return "DevMessage{" +
                "testContent='" + testContent + '\'' +
                "} " + super.toString();
    }
}
