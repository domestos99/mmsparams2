package cz.uhk.dip.mmsparams.api.websocket.messages;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

/**
 * This message is used to deserialize incoming messages in string and then recognize
 * message type from messageKey property. After that it is possible to deserialize message of correct type
 */
public class EmptyMessage extends WebSocketMessageBase implements Serializable
{
    @Override
    public String toString()
    {
        return "EmptyMessage{} " + super.toString();
    }
}
