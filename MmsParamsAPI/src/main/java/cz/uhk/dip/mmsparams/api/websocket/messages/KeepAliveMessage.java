package cz.uhk.dip.mmsparams.api.websocket.messages;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class KeepAliveMessage extends WebSocketMessageBase implements Serializable
{
    @Override
    public String toString()
    {
        return "KeepAliveMessage{} " + super.toString();
    }
}
