package cz.uhk.dip.mmsparams.api.websocket.messages.clientlib;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.TestGroupModel;
import cz.uhk.dip.mmsparams.api.websocket.model.registration.TestClientInfoModel;

/**
 * Message for Client registration on server
 */
public class RegisterClientLibMessage extends WebSocketMessageBase implements Serializable
{
    private TestClientInfoModel testClientInfoModel;
    private ClientInfo clientInfo;
    private String systemBuildVersion;
    private TestGroupModel testGroupModel;

    public ClientInfo getClientInfo()
    {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo)
    {
        this.clientInfo = clientInfo;
    }

    public TestClientInfoModel getTestClientInfoModel()
    {
        return testClientInfoModel;
    }

    public void setTestClientInfoModel(TestClientInfoModel testClientInfoModel)
    {
        this.testClientInfoModel = testClientInfoModel;
    }

    public String getSystemBuildVersion()
    {
        return systemBuildVersion;
    }

    public void setSystemBuildVersion(String systemBuildVersion)
    {
        this.systemBuildVersion = systemBuildVersion;
    }

    public TestGroupModel getTestGroupModel()
    {
        return testGroupModel;
    }

    public void setTestGroupModel(TestGroupModel testGroupModel)
    {
        this.testGroupModel = testGroupModel;
    }

    @Override
    public String toString()
    {
        return "RegisterClientLibMessage{" +
                "testClientInfoModel=" + testClientInfoModel +
                ", clientInfo=" + clientInfo +
                ", systemBuildVersion='" + systemBuildVersion + '\'' +
                ", testGroupModel=" + testGroupModel +
                "} " + super.toString();
    }
}
