package cz.uhk.dip.mmsparams.api.websocket.messages.clientlib;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class TestResultMessage extends WebSocketMessageBase implements Serializable
{
    @Override
    public String toString()
    {
        return "TestResultMessage{} " + super.toString();
    }
}
