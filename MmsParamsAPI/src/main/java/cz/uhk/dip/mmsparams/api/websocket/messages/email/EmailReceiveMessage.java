package cz.uhk.dip.mmsparams.api.websocket.messages.email;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.email.ReceiveEmailMessageModel;

public class EmailReceiveMessage extends WebSocketMessageBase implements Serializable
{
    private ReceiveEmailMessageModel receiveEmailMessageModel;

    public ReceiveEmailMessageModel getReceiveEmailMessageModel()
    {
        return receiveEmailMessageModel;
    }

    public void setReceiveEmailMessageModel(ReceiveEmailMessageModel receiveEmailMessageModel)
    {
        this.receiveEmailMessageModel = receiveEmailMessageModel;
    }

    @Override
    public String toString()
    {
        return "EmailReceiveMessage{" +
                "receiveEmailMessageModel=" + receiveEmailMessageModel +
                "} " + super.toString();
    }
}
