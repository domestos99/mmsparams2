package cz.uhk.dip.mmsparams.api.websocket.messages.errors;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;

/**
 * Message representing general error.
 * It can be error in test pipeline or unexpected bug in code
 */
public class GenericErrorResponseMessage extends WebSocketMessageBase implements Serializable, IResponseMessage
{
    private TestErrorModel testErrorModel;
    private String webSocketMessage;

    public TestErrorModel getTestErrorModel()
    {
        return testErrorModel;
    }

    public void setTestErrorModel(TestErrorModel testErrorModel)
    {
        this.testErrorModel = testErrorModel;
    }

    public String getWebSocketMessage()
    {
        return webSocketMessage;
    }

    public void setWebSocketMessage(String webSocketMessage)
    {
        this.webSocketMessage = webSocketMessage;
    }

    @Override
    public String toString()
    {
        return "GenericErrorResponseMessage{" +
                "testErrorModel=" + testErrorModel +
                ", webSocketMessage='" + webSocketMessage + '\'' +
                "} " + super.toString();
    }
}
