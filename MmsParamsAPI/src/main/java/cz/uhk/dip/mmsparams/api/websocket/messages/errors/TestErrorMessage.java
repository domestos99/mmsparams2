package cz.uhk.dip.mmsparams.api.websocket.messages.errors;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;

public class TestErrorMessage extends WebSocketMessageBase implements Serializable
{
    private TestErrorModel testErrorModel;

    public TestErrorModel getTestErrorModel()
    {
        return testErrorModel;
    }

    public void setTestErrorModel(TestErrorModel testErrorModel)
    {
        this.testErrorModel = testErrorModel;
    }

    @Override
    public String toString()
    {
        return "TestErrorMessage{" +
                "testErrorModel=" + testErrorModel +
                "} " + super.toString();
    }
}
