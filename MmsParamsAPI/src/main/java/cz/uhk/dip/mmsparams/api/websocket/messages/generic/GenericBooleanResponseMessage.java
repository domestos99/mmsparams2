package cz.uhk.dip.mmsparams.api.websocket.messages.generic;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

/**
 * Message representing logical value (true/false).
 * Usually used for acknowledging that message was accepter by recipient
 */
public class GenericBooleanResponseMessage extends WebSocketMessageBase implements Serializable, IResponseMessage
{
    private boolean value;

    public boolean isValue()
    {
        return value;
    }

    public void setValue(boolean value)
    {
        this.value = value;
    }


    @Override
    public String toString()
    {
        return "GenericBooleanResponseMessage{" +
                "value=" + value +
                "} " + super.toString();
    }
}
