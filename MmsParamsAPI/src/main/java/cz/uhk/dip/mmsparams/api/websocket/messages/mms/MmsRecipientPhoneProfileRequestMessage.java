package cz.uhk.dip.mmsparams.api.websocket.messages.mms;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;

/**
 * Message for setting recipient MMS profile
 */
public class MmsRecipientPhoneProfileRequestMessage extends WebSocketMessageBase implements Serializable
{
    private MmsRecipientPhoneProfile mmsRecipientPhoneProfile;

    public MmsRecipientPhoneProfile getMmsRecipientPhoneProfile()
    {
        return mmsRecipientPhoneProfile;
    }

    public void setMmsRecipientPhoneProfile(MmsRecipientPhoneProfile mmsRecipientPhoneProfile)
    {
        this.mmsRecipientPhoneProfile = mmsRecipientPhoneProfile;
    }

    @Override
    public String toString()
    {
        return "MmsRecipientPhoneProfileRequestMessage{" +
                "mmsRecipientPhoneProfile=" + mmsRecipientPhoneProfile +
                "} " + super.toString();
    }
}
