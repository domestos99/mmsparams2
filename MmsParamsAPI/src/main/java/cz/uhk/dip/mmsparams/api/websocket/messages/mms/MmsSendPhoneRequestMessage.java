package cz.uhk.dip.mmsparams.api.websocket.messages.mms;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;

/**
 * Message for requesting to send new MMS message
 */
public class MmsSendPhoneRequestMessage extends WebSocketMessageBase implements Serializable
{
    private MmsSendModel mmsSendModel;

    public MmsSendModel getMmsSendModel()
    {
        return mmsSendModel;
    }

    public void setMmsSendModel(MmsSendModel mmsSendModel)
    {
        this.mmsSendModel = mmsSendModel;
    }

    @Override
    public String toString()
    {
        return "MmsSendPhoneRequestMessage{" +
                "mmsSend=" + mmsSendModel +
                "} " + super.toString();
    }
}
