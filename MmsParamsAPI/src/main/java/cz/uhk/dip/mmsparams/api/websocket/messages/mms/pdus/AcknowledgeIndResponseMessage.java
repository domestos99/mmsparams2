package cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IClientBroadcastMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.AcknowledgeIndModel;

public class AcknowledgeIndResponseMessage extends WebSocketMessageBase implements Serializable, IClientBroadcastMessage
{
    private AcknowledgeIndModel acknowledgeIndModel;

    public AcknowledgeIndModel getAcknowledgeIndModel()
    {
        return acknowledgeIndModel;
    }

    public void setAcknowledgeIndModel(AcknowledgeIndModel acknowledgeIndModel)
    {
        this.acknowledgeIndModel = acknowledgeIndModel;
    }

    @Override
    public String toString()
    {
        return "AcknowledgeIndResponseMessage{" +
                "acknowledgeIndModel=" + acknowledgeIndModel +
                "} " + super.toString();
    }
}
