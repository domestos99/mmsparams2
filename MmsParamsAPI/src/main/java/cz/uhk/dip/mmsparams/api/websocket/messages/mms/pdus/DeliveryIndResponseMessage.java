package cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IClientBroadcastMessage;
import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.DeliveryIndModel;

public class DeliveryIndResponseMessage extends WebSocketMessageBase implements Serializable, IClientBroadcastMessage, IResponseMessage
{
    private DeliveryIndModel deliveryIndModel;

    public DeliveryIndModel getDeliveryIndModel()
    {
        return deliveryIndModel;
    }

    public void setDeliveryIndModel(DeliveryIndModel deliveryIndModel)
    {
        this.deliveryIndModel = deliveryIndModel;
    }

    @Override
    public String toString()
    {
        return "DeliveryIndResponseMessage{" +
                "deliveryIndModel=" + deliveryIndModel +
                "} " + super.toString();
    }
}
