package cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IClientBroadcastMessage;
import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotificationIndModel;

public class NotificationIndResponseMessage extends WebSocketMessageBase implements Serializable, IClientBroadcastMessage, IResponseMessage
{
    private NotificationIndModel notificationIndModel;

    public NotificationIndModel getNotificationIndModel()
    {
        return notificationIndModel;
    }

    public void setNotificationIndModel(NotificationIndModel notificationIndModel)
    {
        this.notificationIndModel = notificationIndModel;
    }

    @Override
    public String toString()
    {
        return "NotificationIndResponseMessage{" +
                "notificationIndModel=" + notificationIndModel +
                "} " + super.toString();
    }
}
