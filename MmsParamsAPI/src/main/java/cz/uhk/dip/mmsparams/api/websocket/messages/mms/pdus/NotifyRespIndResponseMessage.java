package cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IClientBroadcastMessage;
import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotifyRespIndModel;

public class NotifyRespIndResponseMessage extends WebSocketMessageBase implements Serializable, IClientBroadcastMessage, IResponseMessage
{
    private NotifyRespIndModel notifyRespIndModel;

    public NotifyRespIndModel getNotifyRespIndModel()
    {
        return notifyRespIndModel;
    }

    public void setNotifyRespIndModel(NotifyRespIndModel notifyRespIndModel)
    {
        this.notifyRespIndModel = notifyRespIndModel;
    }

    @Override
    public String toString()
    {
        return "NotifyRespIndResponseMessage{" +
                "notifyRespIndModel=" + notifyRespIndModel +
                "} " + super.toString();
    }
}
