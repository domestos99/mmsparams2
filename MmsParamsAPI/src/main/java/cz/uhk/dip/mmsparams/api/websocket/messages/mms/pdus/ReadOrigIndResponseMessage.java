package cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IClientBroadcastMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.ReadOrigIndModel;

public class ReadOrigIndResponseMessage extends WebSocketMessageBase implements Serializable, IClientBroadcastMessage
{
    private ReadOrigIndModel readOrigIndModel;

    public ReadOrigIndModel getReadOrigIndModel()
    {
        return readOrigIndModel;
    }

    public void setReadOrigIndModel(ReadOrigIndModel readOrigIndModel)
    {
        this.readOrigIndModel = readOrigIndModel;
    }

    @Override
    public String toString()
    {
        return "ReadOrigIndResponseMessage{" +
                "readOrigIndModel=" + readOrigIndModel +
                "} " + super.toString();
    }
}
