package cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IClientBroadcastMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.ReadRecIndModel;

public class ReadRecIndResponseMessage extends WebSocketMessageBase implements Serializable, IClientBroadcastMessage
{
    private ReadRecIndModel readRecIndModel;

    public ReadRecIndModel getReadRecIndModel()
    {
        return readRecIndModel;
    }

    public void setReadRecIndModel(ReadRecIndModel readRecIndModel)
    {
        this.readRecIndModel = readRecIndModel;
    }

    @Override
    public String toString()
    {
        return "ReadRecIndResponseMessage{" +
                "readRecIndModel=" + readRecIndModel +
                "} " + super.toString();
    }
}
