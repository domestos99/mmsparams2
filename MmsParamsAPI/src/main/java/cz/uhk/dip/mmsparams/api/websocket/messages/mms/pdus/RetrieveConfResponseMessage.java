package cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IClientBroadcastMessage;
import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;

public class RetrieveConfResponseMessage extends WebSocketMessageBase implements Serializable, IClientBroadcastMessage, IResponseMessage
{
    private RetrieveConfModel retrieveConfModel;

    public RetrieveConfModel getRetrieveConfModel()
    {
        return retrieveConfModel;
    }

    public void setRetrieveConfModel(RetrieveConfModel retrieveConfModel)
    {
        this.retrieveConfModel = retrieveConfModel;
    }

    @Override
    public String toString()
    {
        return "RetrieveConfResponseMessage{" +
                "retrieveConfModel=" + retrieveConfModel +
                "} " + super.toString();
    }
}
