package cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;

public class SendConfResponseMessage extends WebSocketMessageBase implements Serializable, IResponseMessage
{
    private SendConfModel sendConfModel;


    public SendConfModel getSendConfModel()
    {
        return sendConfModel;
    }

    public void setSendConfModel(SendConfModel sendConfModel)
    {
        this.sendConfModel = sendConfModel;
    }

    @Override
    public String toString()
    {
        return "SendConfResponseMessage{" +
                "sendConfModel=" + sendConfModel +
                "} " + super.toString();
    }
}
