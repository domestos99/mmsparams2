package cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendReqModel;

public class SendReqResponseMessage extends WebSocketMessageBase implements Serializable, IResponseMessage
{
    private SendReqModel sendReqModel;

    public SendReqModel getSendReqModel()
    {
        return sendReqModel;
    }

    public void setSendReqModel(SendReqModel sendReqModel)
    {
        this.sendReqModel = sendReqModel;
    }

    @Override
    public String toString()
    {
        return "SendReqResponseMessage{" +
                "sendReqModel=" + sendReqModel +
                "} " + super.toString();
    }
}
