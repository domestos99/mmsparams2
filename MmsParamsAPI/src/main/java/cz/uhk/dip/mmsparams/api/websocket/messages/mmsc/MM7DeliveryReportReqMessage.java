package cz.uhk.dip.mmsparams.api.websocket.messages.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;

public class MM7DeliveryReportReqMessage extends WebSocketMessageBase implements Serializable, IResponseMessage
{
    private MM7DeliveryReportReqModel mm7DeliveryReportReqModel;

    public MM7DeliveryReportReqModel getMm7DeliveryReportReqModel()
    {
        return mm7DeliveryReportReqModel;
    }

    public void setMm7DeliveryReportReqModel(MM7DeliveryReportReqModel mm7DeliveryReportReqModel)
    {
        this.mm7DeliveryReportReqModel = mm7DeliveryReportReqModel;
    }

    @Override
    public String toString()
    {
        return "MM7DeliveryReportReqMessage{" +
                "mm7DeliveryReportReqModel=" + mm7DeliveryReportReqModel +
                "} " + super.toString();
    }
}
