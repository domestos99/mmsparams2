package cz.uhk.dip.mmsparams.api.websocket.messages.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;

public class MM7DeliveryReqMessage extends WebSocketMessageBase implements Serializable, IResponseMessage
{
    private MM7DeliveryReqModel mm7DeliveryReqModel;

    public MM7DeliveryReqModel getMm7DeliveryReqModel()
    {
        return mm7DeliveryReqModel;
    }

    public void setMm7DeliveryReqModel(MM7DeliveryReqModel mm7DeliveryReqModel)
    {
        this.mm7DeliveryReqModel = mm7DeliveryReqModel;
    }

    @Override
    public String toString()
    {
        return "MM7DeliveryReqMessage{" +
                "mm7DeliveryReqModel=" + mm7DeliveryReqModel +
                "} " + super.toString();
    }
}
