package cz.uhk.dip.mmsparams.api.websocket.messages.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ErrorModel;

public class MM7ErrorMessage extends WebSocketMessageBase implements Serializable
{
    private MM7ErrorModel mm7ErrorModel;

    public MM7ErrorModel getMm7ErrorModel()
    {
        return mm7ErrorModel;
    }

    public void setMm7ErrorModel(MM7ErrorModel mm7ErrorModel)
    {
        this.mm7ErrorModel = mm7ErrorModel;
    }

    @Override
    public String toString()
    {
        return "MM7ErrorMessage{" +
                "mm7ErrorModel=" + mm7ErrorModel +
                "} " + super.toString();
    }
}
