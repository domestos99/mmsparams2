package cz.uhk.dip.mmsparams.api.websocket.messages.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;

public class MM7ReadReplyReqMessage extends WebSocketMessageBase implements Serializable
{
    private MM7ReadReplyReqModel mm7ReadReplyReqModel;

    public MM7ReadReplyReqModel getMm7ReadReplyReqModel()
    {
        return mm7ReadReplyReqModel;
    }

    public void setMm7ReadReplyReqModel(MM7ReadReplyReqModel mm7ReadReplyReqModel)
    {
        this.mm7ReadReplyReqModel = mm7ReadReplyReqModel;
    }

    @Override
    public String toString()
    {
        return "MM7ReadReplyReqMessage{" +
                "mm7ReadReplyReqModel=" + mm7ReadReplyReqModel +
                "} " + super.toString();
    }
}
