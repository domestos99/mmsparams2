package cz.uhk.dip.mmsparams.api.websocket.messages.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MmscAcquireRouteModel;

public class MmscAcquireRouteRequestMessage extends WebSocketMessageBase implements Serializable
{
    private MmscAcquireRouteModel mmscAcquireRouteModel;

    public MmscAcquireRouteModel getMmscAcquireRouteModel()
    {
        return mmscAcquireRouteModel;
    }

    public void setMmscAcquireRouteModel(MmscAcquireRouteModel mmscAcquireRouteModel)
    {
        this.mmscAcquireRouteModel = mmscAcquireRouteModel;
    }

    @Override
    public String toString()
    {
        return "MmscAcquireRouteRequestMessage{" +
                "mmscAcquireRouteModel=" + mmscAcquireRouteModel +
                "} " + super.toString();
    }
}
