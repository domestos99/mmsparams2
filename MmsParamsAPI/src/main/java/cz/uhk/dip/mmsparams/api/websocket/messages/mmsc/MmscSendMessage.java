package cz.uhk.dip.mmsparams.api.websocket.messages.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;

public class MmscSendMessage extends WebSocketMessageBase implements Serializable
{
    private MmscSendModel mmscSendModel;

    public MmscSendModel getMmscSendModel()
    {
        return mmscSendModel;
    }

    public void setMmscSendModel(MmscSendModel mmscSendModel)
    {
        this.mmscSendModel = mmscSendModel;
    }
}
