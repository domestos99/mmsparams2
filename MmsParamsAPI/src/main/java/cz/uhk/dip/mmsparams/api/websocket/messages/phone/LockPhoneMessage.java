package cz.uhk.dip.mmsparams.api.websocket.messages.phone;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;

public class LockPhoneMessage extends WebSocketMessageBase implements Serializable
{
    private PhoneInfoModel phoneInfoModel;

    public PhoneInfoModel getPhoneInfoModel()
    {
        return phoneInfoModel;
    }

    public void setPhoneInfoModel(PhoneInfoModel phoneInfoModel)
    {
        this.phoneInfoModel = phoneInfoModel;
    }

    @Override
    public String toString()
    {
        return "LockPhoneMessage{" +
                "deviceInfo=" + phoneInfoModel +
                "} " + super.toString();
    }
}
