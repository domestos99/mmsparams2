package cz.uhk.dip.mmsparams.api.websocket.messages.phone;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class LockedPhonesListRequestMessage extends WebSocketMessageBase implements Serializable
{
    @Override
    public String toString()
    {
        return "LockedPhonesListRequest{} " + super.toString();
    }
}
