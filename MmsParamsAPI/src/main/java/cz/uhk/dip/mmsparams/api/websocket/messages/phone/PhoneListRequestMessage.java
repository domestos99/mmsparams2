package cz.uhk.dip.mmsparams.api.websocket.messages.phone;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.enums.PhoneListRequestFilter;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class PhoneListRequestMessage extends WebSocketMessageBase implements Serializable
{
    private PhoneListRequestFilter filter;

    public void setFilter(PhoneListRequestFilter filter)
    {
        this.filter = filter;
    }

    public PhoneListRequestFilter getFilter()
    {
        return filter;
    }

    @Override
    public String toString()
    {
        return "PhoneListRequestMessage{" +
                "filter=" + filter +
                "} " + super.toString();
    }
}
