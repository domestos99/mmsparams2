package cz.uhk.dip.mmsparams.api.websocket.messages.phone;

import java.io.Serializable;
import java.util.ArrayList;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;

public class PhoneListResponseMessage extends WebSocketMessageBase implements Serializable, IResponseMessage
{
    private ArrayList<PhoneInfoModel> phoneInfoModels;

    public PhoneListResponseMessage()
    {
        phoneInfoModels = new ArrayList<>();
    }

    public ArrayList<PhoneInfoModel> getPhoneInfoModels()
    {
        return phoneInfoModels;
    }

    public void setPhoneInfoModels(ArrayList<PhoneInfoModel> phoneInfoModels)
    {
        this.phoneInfoModels = phoneInfoModels;
    }

    @Override
    public String toString()
    {
        return "DeviceListResponseMessage{" +
                "deviceInfos=" + phoneInfoModels +
                "} " + super.toString();
    }
}
