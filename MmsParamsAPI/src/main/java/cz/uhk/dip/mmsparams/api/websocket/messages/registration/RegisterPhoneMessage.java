package cz.uhk.dip.mmsparams.api.websocket.messages.registration;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;

/**
 * Message for Phone registration on server
 */
public class RegisterPhoneMessage extends WebSocketMessageBase implements Serializable
{

    private PhoneInfoModel phoneInfoModel;

    public PhoneInfoModel getPhoneInfoModel()
    {
        return phoneInfoModel;
    }

    public void setPhoneInfoModel(PhoneInfoModel phoneInfoModel)
    {
        this.phoneInfoModel = phoneInfoModel;
    }

    @Override
    public String toString()
    {
        return "RegisterPhoneMessage{" +
                "deviceInfo=" + phoneInfoModel +
                "} " + super.toString();
    }
}
