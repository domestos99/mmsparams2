package cz.uhk.dip.mmsparams.api.websocket.messages.sms;

import java.io.Serializable;
import java.util.ArrayList;

import cz.uhk.dip.mmsparams.api.interfaces.IClientBroadcastMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;

/**
 * Message containing all SMS parts received by recipient
 */
public class SmsReceivePhoneAllPartsMessage extends WebSocketMessageBase implements Serializable, IClientBroadcastMessage
{
    private ArrayList<SmsReceiveModel> smsReceiveAllPartsModel;


    public ArrayList<SmsReceiveModel> getSmsReceiveAllPartsModel()
    {
        return smsReceiveAllPartsModel;
    }

    public void setSmsReceiveAllPartsModel(ArrayList<SmsReceiveModel> smsReceiveAllPartsModel)
    {
        this.smsReceiveAllPartsModel = smsReceiveAllPartsModel;
    }

    @Override
    public String toString()
    {
        return "SmsReceivePhoneAllPartsMessage{" +
                "smsReceiveAllPartsModel=" + smsReceiveAllPartsModel +
                "} " + super.toString();
    }
}