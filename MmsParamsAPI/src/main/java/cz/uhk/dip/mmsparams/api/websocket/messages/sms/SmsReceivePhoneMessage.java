package cz.uhk.dip.mmsparams.api.websocket.messages.sms;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IClientBroadcastMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;

/**
 * Message containing SMS received by recipient
 */
public class SmsReceivePhoneMessage extends WebSocketMessageBase implements Serializable, IClientBroadcastMessage
{
    private SmsReceiveModel smsReceiveModel;

    public SmsReceiveModel getSmsReceiveModel()
    {
        return smsReceiveModel;
    }

    public void setSmsReceiveModel(SmsReceiveModel smsReceiveModel)
    {
        this.smsReceiveModel = smsReceiveModel;
    }

    @Override
    public String toString()
    {
        return "SmsReceivePhoneMessage{" +
                "smsReceive=" + smsReceiveModel +
                "} " + super.toString();
    }
}
