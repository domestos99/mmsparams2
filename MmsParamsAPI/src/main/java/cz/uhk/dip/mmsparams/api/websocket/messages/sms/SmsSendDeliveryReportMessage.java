package cz.uhk.dip.mmsparams.api.websocket.messages.sms;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;

/**
 * Message containing SMS delivery report from sender
 */
public class SmsSendDeliveryReportMessage extends WebSocketMessageBase implements Serializable, IResponseMessage
{
    private SmsDeliveryReport smsDeliveryReport;


    public SmsDeliveryReport getSmsDeliveryReport()
    {
        return smsDeliveryReport;
    }

    public void setSmsDeliveryReport(SmsDeliveryReport smsDeliveryReport)
    {
        this.smsDeliveryReport = smsDeliveryReport;
    }


    @Override
    public String toString()
    {
        return "SmsSendDeliveryResponseMessage{" +
                "smsDeliveryReport=" + smsDeliveryReport +
                "} " + super.toString();
    }
}

