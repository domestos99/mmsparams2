package cz.uhk.dip.mmsparams.api.websocket.messages.sms;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;

/**
 * Message for requesting to send new SMS message
 */
public class SmsSendPhoneRequestMessage extends WebSocketMessageBase implements Serializable
{
    private SmsSendModel smsSendModel;

    public SmsSendModel getSmsSendModel()
    {
        return smsSendModel;
    }

    public void setSmsSendModel(SmsSendModel smsSendModel)
    {
        this.smsSendModel = smsSendModel;
    }


    @Override
    public String toString()
    {
        return "SmsSendPhoneRequestMessage{" +
                "smsSend=" + smsSendModel +
                "} " + super.toString();
    }
}
