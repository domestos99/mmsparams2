package cz.uhk.dip.mmsparams.api.websocket.messages.sms;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;

/**
 * Message containing information about status of sending SMS message
 */
public class SmsSendPhoneResponseMessage extends WebSocketMessageBase implements Serializable, IResponseMessage
{
    private SmsSendResponseModel smsSendResponseModel;


    public SmsSendResponseModel getSmsSendResponseModel()
    {
        return smsSendResponseModel;
    }

    public void setSmsSendResponseModel(SmsSendResponseModel smsSendResponseModel)
    {
        this.smsSendResponseModel = smsSendResponseModel;
    }


    @Override
    public String toString()
    {
        return "SmsSendPhoneResponseMessage{" +
                "smsSendPhoneResponseModel=" + smsSendResponseModel +
                "} " + super.toString();
    }
}
