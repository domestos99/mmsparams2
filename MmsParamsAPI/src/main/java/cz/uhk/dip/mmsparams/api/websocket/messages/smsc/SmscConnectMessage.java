package cz.uhk.dip.mmsparams.api.websocket.messages.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;

public class SmscConnectMessage extends WebSocketMessageBase implements ISmscMessage, Serializable
{
    private SmscConnectModel smscSendSmsModel;

    public SmscConnectModel getSmscSendSmsModel()
    {
        return smscSendSmsModel;
    }

    public void setSmscSendSmsModel(SmscConnectModel smscSendSmsModel)
    {
        this.smscSendSmsModel = smscSendSmsModel;
    }

    @Override
    public String toString()
    {
        return "SmscConnectMessage{" +
                "smscSendSmsModel=" + smscSendSmsModel +
                "} " + super.toString();
    }
}
