package cz.uhk.dip.mmsparams.api.websocket.messages.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;

public class SmscConnectResponseMessage extends WebSocketMessageBase implements Serializable, IResponseMessage
{
    private SmscConnectResponseModel smscConnectResponseModel;

    public SmscConnectResponseModel getSmscConnectResponseModel()
    {
        return smscConnectResponseModel;
    }

    public void setSmscConnectResponseModel(SmscConnectResponseModel smscConnectResponseModel)
    {
        this.smscConnectResponseModel = smscConnectResponseModel;
    }

    @Override
    public String toString()
    {
        return "SmscConnectResponseMessage{" +
                "smscConnectResponseModel=" + smscConnectResponseModel +
                "} " + super.toString();
    }
}
