package cz.uhk.dip.mmsparams.api.websocket.messages.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;

public class SmscDeliverSmMessage extends SmscMessageBase implements Serializable
{
    private SmscDeliverSmModel smscDeliverSmModel;

    public SmscDeliverSmModel getSmscDeliverSmModel()
    {
        return smscDeliverSmModel;
    }

    public void setSmscDeliverSmModel(SmscDeliverSmModel smscDeliverSmModel)
    {
        this.smscDeliverSmModel = smscDeliverSmModel;
    }

    @Override
    public String toString()
    {
        return "SmscDeliverSmMessage{" +
                "smscDeliverSmModel=" + smscDeliverSmModel +
                "} " + super.toString();
    }
}
