package cz.uhk.dip.mmsparams.api.websocket.messages.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;

public class SmscDeliveryReportMessage extends SmscMessageBase implements Serializable
{
    private SmscDeliveryReportModel smscDeliveryReportModel;

    public SmscDeliveryReportModel getSmscDeliveryReportModel()
    {
        return smscDeliveryReportModel;
    }

    public void setSmscDeliveryReportModel(SmscDeliveryReportModel smscDeliveryReportModel)
    {
        this.smscDeliveryReportModel = smscDeliveryReportModel;
    }

    @Override
    public String toString()
    {
        return "SmscDeliveryReportMessage{" +
                "smscDeliveryReportModel=" + smscDeliveryReportModel +
                "} " + super.toString();
    }
}
