package cz.uhk.dip.mmsparams.api.websocket.messages.smsc;

import java.io.Serializable;

public class SmscDisconnectMessage extends SmscMessageBase implements Serializable
{

    @Override
    public String toString()
    {
        return "SmscDisconnectMessage{} " + super.toString();
    }
}
