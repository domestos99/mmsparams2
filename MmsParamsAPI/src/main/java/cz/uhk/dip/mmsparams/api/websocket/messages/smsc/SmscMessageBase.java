package cz.uhk.dip.mmsparams.api.websocket.messages.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;

public abstract class SmscMessageBase extends WebSocketMessageBase implements Serializable
{
    private SmscSessionId smscSessionId;

    public SmscSessionId getSmscSessionId()
    {
        return smscSessionId;
    }

    public void setSmscSessionId(SmscSessionId smscSessionId)
    {
        this.smscSessionId = smscSessionId;
    }

    @Override
    public String toString()
    {
        return "SmscMessageBase{" +
                "smscSessionId=" + smscSessionId +
                "} " + super.toString();
    }
}
