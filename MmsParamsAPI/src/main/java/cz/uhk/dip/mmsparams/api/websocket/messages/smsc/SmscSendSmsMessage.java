package cz.uhk.dip.mmsparams.api.websocket.messages.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;

public class SmscSendSmsMessage extends SmscMessageBase implements ISmscMessage, Serializable
{
    private SmscSendModel smscSendModel;

    public SmscSendModel getSmscSendModel()
    {
        return smscSendModel;
    }

    public void setSmscSendModel(SmscSendModel smscSendModel)
    {
        this.smscSendModel = smscSendModel;
    }

    @Override
    public String toString()
    {
        return "SmscSendSmsMessage{" +
                "smscSendSmsModel=" + smscSendModel +
                "} " + super.toString();
    }
}
