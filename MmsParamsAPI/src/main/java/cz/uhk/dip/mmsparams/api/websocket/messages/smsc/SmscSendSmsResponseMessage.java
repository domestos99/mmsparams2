package cz.uhk.dip.mmsparams.api.websocket.messages.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;

public class SmscSendSmsResponseMessage extends WebSocketMessageBase implements Serializable, IResponseMessage
{
    private SmscSendSmsResponseModel smscSendSmsResponseModel;


    public SmscSendSmsResponseModel getSmscSendSmsResponseModel()
    {
        return smscSendSmsResponseModel;
    }

    public void setSmscSendSmsResponseModel(SmscSendSmsResponseModel smscSendSmsResponseModel)
    {
        this.smscSendSmsResponseModel = smscSendSmsResponseModel;
    }

    @Override
    public String toString()
    {
        return "SmscSendSmsResponseMessage{" +
                "smscSendSmsResponseModel=" + smscSendSmsResponseModel +
                "} " + super.toString();
    }
}
