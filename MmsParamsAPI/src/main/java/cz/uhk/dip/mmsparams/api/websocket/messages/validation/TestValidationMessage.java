package cz.uhk.dip.mmsparams.api.websocket.messages.validation;

import java.io.Serializable;
import java.util.ArrayList;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class TestValidationMessage extends WebSocketMessageBase implements Serializable
{
    private ArrayList<Tuple<Boolean, ValidationResult>> validationResult;

    public void setValidationResult(ArrayList<Tuple<Boolean, ValidationResult>> validationResult)
    {
        this.validationResult = validationResult;
    }

    public ArrayList<Tuple<Boolean, ValidationResult>> getValidationResult()
    {
        return validationResult;
    }
}
