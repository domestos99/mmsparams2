package cz.uhk.dip.mmsparams.api.websocket.model;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class PingGetModel extends WebSocketModelBase implements Serializable
{
    private int status;

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "PingGetModel{" +
                "status=" + status +
                '}';
    }
}
