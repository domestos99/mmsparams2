package cz.uhk.dip.mmsparams.api.websocket.model;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.constants.AppVersion;

public class SystemVersionModel implements Serializable
{
    private String systemVersion;
    private String systemBuild;

    public SystemVersionModel()
    {
    }

    public SystemVersionModel(String systemVersion, String systemBuild)
    {
        this.systemVersion = systemVersion;
        this.systemBuild = systemBuild;
    }

    public static SystemVersionModel create()
    {
        return new SystemVersionModel(AppVersion.SYSTEM_VERSION, AppVersion.SYSTEM_BUILD);
    }

    public String getSystemVersion()
    {
        return systemVersion;
    }

    public void setSystemVersion(String systemVersion)
    {
        this.systemVersion = systemVersion;
    }

    public String getSystemBuild()
    {
        return systemBuild;
    }

    public void setSystemBuild(String systemBuild)
    {
        this.systemBuild = systemBuild;
    }
}
