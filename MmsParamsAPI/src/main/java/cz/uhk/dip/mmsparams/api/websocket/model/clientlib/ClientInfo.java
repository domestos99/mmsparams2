package cz.uhk.dip.mmsparams.api.websocket.model.clientlib;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class ClientInfo extends WebSocketModelBase implements Serializable
{
    private String clientKey;
    private String computerName;
    private String currentUserName;

    public String getClientKey()
    {
        return clientKey;
    }

    public void setClientKey(String clientKey)
    {
        this.clientKey = clientKey;
    }

    public void setComputerName(String computerName)
    {
        this.computerName = computerName;
    }

    public String getComputerName()
    {
        return computerName;
    }

    public void setCurrentUserName(String currentUserName)
    {
        this.currentUserName = currentUserName;
    }

    public String getCurrentUserName()
    {
        return currentUserName;
    }

    @Override
    public String toString()
    {
        return "ClientInfo{" +
                "clientKey='" + clientKey + '\'' +
                ", computerName='" + computerName + '\'' +
                ", currentUserName='" + currentUserName + '\'' +
                "} " + super.toString();
    }
}
