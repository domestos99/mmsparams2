package cz.uhk.dip.mmsparams.api.websocket.model.clientlib;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class TestGroupModel extends WebSocketModelBase implements Serializable
{
    private String testGroupID;
    private String testGroupName;
    private String testGroupDesc;

    public String getTestGroupID()
    {
        return testGroupID;
    }

    public void setTestGroupID(String testGroupID)
    {
        this.testGroupID = testGroupID;
    }

    public String getTestGroupName()
    {
        return testGroupName;
    }

    public void setTestGroupName(String testGroupName)
    {
        this.testGroupName = testGroupName;
    }

    public String getTestGroupDesc()
    {
        return testGroupDesc;
    }

    public void setTestGroupDesc(String testGroupDesc)
    {
        this.testGroupDesc = testGroupDesc;
    }

    @Override
    public String toString()
    {
        return "TestGroupModel{" +
                "testGroupID='" + testGroupID + '\'' +
                ", testGroupName='" + testGroupName + '\'' +
                ", testGroupDesc='" + testGroupDesc + '\'' +
                "} " + super.toString();
    }
}
