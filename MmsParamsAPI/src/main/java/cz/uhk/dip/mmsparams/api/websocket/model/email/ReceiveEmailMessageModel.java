package cz.uhk.dip.mmsparams.api.websocket.model.email;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class ReceiveEmailMessageModel extends WebSocketModelBase implements Serializable
{
    private String from;
    private ArrayList<String> to;
    private Date sentDate;
    private Date receivedDate;
    private String subject;
    private String contentType;
    private ArrayList<ReceiveEmailMessagePartModel> parts;

    public ReceiveEmailMessageModel()
    {
        this.to = new ArrayList<>();
        this.parts = new ArrayList<>();
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public ArrayList<String> getTo()
    {
        return to;
    }

    public void setTo(ArrayList<String> to)
    {
        this.to = to;
    }

    public void addTo(String to)
    {
        if (this.to == null)
        {
            this.to = new ArrayList<>();
        }
        this.to.add(to);
    }

    public Date getSentDate()
    {
        return sentDate;
    }

    public void setSentDate(Date sentDate)
    {
        this.sentDate = sentDate;
    }

    public Date getReceivedDate()
    {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate)
    {
        this.receivedDate = receivedDate;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public ArrayList<ReceiveEmailMessagePartModel> getParts()
    {
        return parts;
    }

    public void setParts(ArrayList<ReceiveEmailMessagePartModel> parts)
    {
        this.parts = parts;
    }

    public void addPart(ReceiveEmailMessagePartModel part)
    {
        if (this.parts == null)
        {
            this.parts = new ArrayList<>();
        }
        this.parts.add(part);
    }
}
