package cz.uhk.dip.mmsparams.api.websocket.model.email;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class ReceiveEmailMessagePartModel extends WebSocketModelBase implements Serializable
{

    private String fileName;
    private String encoding;
    private String contentType;
    private String description;
    private String disposition;
    private int size;
    private byte[] data;

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setEncoding(String encoding)
    {
        this.encoding = encoding;
    }

    public String getEncoding()
    {
        return encoding;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDisposition(String disposition)
    {
        this.disposition = disposition;
    }

    public String getDisposition()
    {
        return disposition;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public int getSize()
    {
        return size;
    }

    public void setData(byte[] data)
    {
        this.data = data;
    }

    public byte[] getData()
    {
        return data;
    }
}
