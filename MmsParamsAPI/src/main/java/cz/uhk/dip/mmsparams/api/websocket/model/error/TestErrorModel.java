package cz.uhk.dip.mmsparams.api.websocket.model.error;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class TestErrorModel extends WebSocketModelBase implements Serializable
{
    private TestErrorType testErrorType;
    private String message;
    private Exception exception;

    public TestErrorType getTestErrorType()
    {
        return testErrorType;
    }

    public void setTestErrorType(TestErrorType testErrorType)
    {
        this.testErrorType = testErrorType;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Exception getException()
    {
        return exception;
    }

    public void setException(Exception exception)
    {
        this.exception = exception;
    }

    @Override
    public String toString()
    {
        return "TestErrorModel{" +
                "testErrorType=" + testErrorType +
                ", message='" + message + '\'' +
                ", exception=" + exception +
                "} " + super.toString();
    }
}
