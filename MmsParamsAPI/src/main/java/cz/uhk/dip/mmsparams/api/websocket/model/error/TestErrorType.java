package cz.uhk.dip.mmsparams.api.websocket.model.error;

import java.io.Serializable;

public enum TestErrorType implements Serializable
{
    TEST_FORCE_CLOSE("FORCE_CLOSE"),
    VALIDATIONS("VALIDATIONS"),
    TIMEOUT("TIMEOUT"),
    WRONGLY_WRITTEN_TEST("WRONGLY_WRITTEN_TEST"),
    GENERIC_ERROR("GENERIC_ERROR"),
    SYSTEM_ERROR("SYSTEM_ERROR"),
    TEST_ERROR("TEST_ERROR"),
    SMSC("SMSC"),
    MMSC("MMSC"),
    ANDROID("ANDROID");

    private String value;

    TestErrorType(String value)
    {
        this.value = value;
    }

    public String value()
    {
        return value;
    }

//    public static TestErrorTypes valueOf(String value)
//    {
//        for (TestErrorTypes val : values())
//        {
//            if (val.value.equals(value))
//                return val;
//        }
//        return TestErrorTypes.GENERIC_ERROR;
//    }
}
