package cz.uhk.dip.mmsparams.api.websocket.model.mms;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.constants.MediaType;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

/**
 * Class representing MMS attachment
 */
public class MmsAttachmentSendModel extends WebSocketModelBase implements Serializable
{
    private String fileName;
    private byte[] data;
    private String extension;
    // private int dataLength;
    private String mimeType;
    private MediaType mediaType;


    public MmsAttachmentSendModel()
    {
    }

    public MmsAttachmentSendModel(String fileName, String extension, byte[] data, String mimeType, MediaType mediaType)
    {
        this.fileName = fileName;
        this.data = data;
        this.extension = extension;
        this.mimeType = mimeType;
        this.mediaType = mediaType;
    }

//    public String getContentType()
//    {
//        return contentType;
//    }
//
//    public void setContentType(String contentType)
//    {
//        this.contentType = contentType;
//    }

    public byte[] getData()
    {
        return data;
    }

    public void setData(byte[] data)
    {
        this.data = data;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getExtension()
    {
        return extension;
    }

    public void setExtension(String extension)
    {
        this.extension = extension;
    }

    public String getMimeType()
    {
        return mimeType;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }

    public MediaType getMediaType()
    {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType)
    {
        this.mediaType = mediaType;
    }

    @Override
    public String toString()
    {
        return "MmsAttachmentSendModel{" +
                "fileName='" + fileName + '\'' +
                ", data=[...]" +
                ", extension='" + extension + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", mediaType=" + mediaType +
                "} " + super.toString();
    }
}
