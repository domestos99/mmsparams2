package cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus;

import java.io.Serializable;
import java.util.Arrays;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

/**
 * Class representing MM1_acknowledgement.REQ PDU
 */
public class AcknowledgeIndModel extends WebSocketModelBase implements Serializable
{
    private String id;
    private int mmsVersion;
    private String transactionId;
    private int reportAllowed;
    private String from;
    private String[] to;

    public AcknowledgeIndModel()
    {
    }

    public AcknowledgeIndModel(String id, int mmsVersion, String transactionId, int reportAllowed, String from, String[] to)
    {
        this.id = id;
        this.mmsVersion = mmsVersion;
        this.transactionId = transactionId;
        this.reportAllowed = reportAllowed;
        this.from = from;
        this.to = to;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public int getMmsVersion()
    {
        return mmsVersion;
    }

    public void setMmsVersion(int mmsVersion)
    {
        this.mmsVersion = mmsVersion;
    }

    public String getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }

    public int getReportAllowed()
    {
        return reportAllowed;
    }

    public void setReportAllowed(int reportAllowed)
    {
        this.reportAllowed = reportAllowed;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public String[] getTo()
    {
        return to;
    }

    public void setTo(String[] to)
    {
        this.to = to;
    }

    @Override
    public String toString()
    {
        return "AcknowledgeIndModel{" +
                "id='" + id + '\'' +
                ", mmsVersion=" + mmsVersion +
                ", transactionId='" + transactionId + '\'' +
                ", reportAllowed=" + reportAllowed +
                ", from='" + from + '\'' +
                ", to=" + Arrays.toString(to) +
                '}';
    }
}
