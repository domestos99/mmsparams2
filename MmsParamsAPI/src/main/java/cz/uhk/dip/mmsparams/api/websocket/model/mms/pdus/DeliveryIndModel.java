package cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus;

import java.io.Serializable;
import java.util.Arrays;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

/**
 * Class representing MM1_delivery_report.REQ PDU
 */
public class DeliveryIndModel extends WebSocketModelBase implements Serializable
{
    private String id;
    private int mmsVersion;

    private String messageId;

    private String from;

    private Long date;

    private int status;

    private String[] to;
    private byte[] applicId;
    private String statusText;
    private byte[] auxApplicId;
    private byte[] replyApplicId;

    public DeliveryIndModel()
    {
    }

    public DeliveryIndModel(String id, int mmsVersion, String messageId, String from, Long date, int status, String[] to)
    {
        this.id = id;
        this.mmsVersion = mmsVersion;
        this.messageId = messageId;
        this.from = from;
        this.date = date;
        this.status = status;
        this.to = to;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public int getMmsVersion()
    {
        return mmsVersion;
    }

    public void setMmsVersion(int mmsVersion)
    {
        this.mmsVersion = mmsVersion;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public Long getDate()
    {
        return date;
    }

    public void setDate(Long date)
    {
        this.date = date;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String[] getTo()
    {
        return to;
    }

    public void setTo(String[] to)
    {
        this.to = to;
    }

    public void setApplicId(byte[] applicId)
    {
        this.applicId = applicId;
    }

    public void setAuxApplicId(byte[] auxApplicId)
    {
        this.auxApplicId = auxApplicId;
    }

    public void setReplyApplicId(byte[] replyApplicId)
    {
        this.replyApplicId = replyApplicId;
    }

    public void setStatusText(String statusText)
    {
        this.statusText = statusText;
    }

    public byte[] getApplicId()
    {
        return applicId;
    }

    public String getStatusText()
    {
        return statusText;
    }

    public byte[] getAuxApplicId()
    {
        return auxApplicId;
    }

    public byte[] getReplyApplicId()
    {
        return replyApplicId;
    }

    @Override
    public String toString()
    {
        return "DeliveryIndModel{" +
                "id='" + id + '\'' +
                ", mmsVersion=" + mmsVersion +
                ", messageId='" + messageId + '\'' +
                ", from='" + from + '\'' +
                ", date=" + date +
                ", status=" + status +
                ", to=" + Arrays.toString(to) +
                ", applicId=" + Arrays.toString(applicId) +
                ", statusText='" + statusText + '\'' +
                ", auxApplicId=" + Arrays.toString(auxApplicId) +
                ", replyApplicId=" + Arrays.toString(replyApplicId) +
                '}';
    }
}
