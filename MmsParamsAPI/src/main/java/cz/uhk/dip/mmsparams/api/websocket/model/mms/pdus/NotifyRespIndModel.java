package cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

/**
 * Class representing MM1_notification.RES PDU
 */
public class NotifyRespIndModel extends WebSocketModelBase implements Serializable
{
    private String id;
    private int status;
    private int reportAllowed;
    private String transactionId;
    private int mmsVersion;
    private String from;

    public NotifyRespIndModel()
    {
    }

    public NotifyRespIndModel(String id, int status, int reportAllowed, String transactionId, int mmsVersion, String from)
    {
        this.id = id;
        this.status = status;
        this.reportAllowed = reportAllowed;
        this.transactionId = transactionId;
        this.mmsVersion = mmsVersion;
        this.from = from;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public int getReportAllowed()
    {
        return reportAllowed;
    }

    public void setReportAllowed(int reportAllowed)
    {
        this.reportAllowed = reportAllowed;
    }

    public String getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }

    public int getMmsVersion()
    {
        return mmsVersion;
    }

    public void setMmsVersion(int mmsVersion)
    {
        this.mmsVersion = mmsVersion;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    @Override
    public String toString()
    {
        return "NotifyRespIndModel{" +
                "id='" + id + '\'' +
                ", status=" + status +
                ", reportAllowed=" + reportAllowed +
                ", transactionId='" + transactionId + '\'' +
                ", mmsVersion=" + mmsVersion +
                ", from='" + from + '\'' +
                '}';
    }
}
