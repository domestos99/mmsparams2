package cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class PduPartModel extends WebSocketModelBase implements Serializable
{
    private String id;
    private int charSet;
    private int dataLength;
    private String contentType;
    private String contentId;
    private String contentLocation;
    private String name;
    private byte[] data;
    private String dataUri;
    private String contentDisposition;
    private String contentTransferEncoding;
    private String fileName;
    private String generateLocation;


    public PduPartModel()
    {
    }

    public PduPartModel(String id, int charSet, int dataLength,
                        String contentType, String contentId,
                        String contentLocation, String name, byte[] data,
                        String dataUri, String contentDisposition,
                        String contentTransferEncoding,
                        String fileName, String generateLocation)
    {
        this.id = id;
        this.charSet = charSet;
        this.dataLength = dataLength;
        this.contentType = contentType;
        this.contentId = contentId;
        this.contentLocation = contentLocation;
        this.name = name;
        this.data = data;
        this.dataUri = dataUri;
        this.contentDisposition = contentDisposition;
        this.contentTransferEncoding = contentTransferEncoding;
        this.fileName = fileName;
        this.generateLocation = generateLocation;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public int getCharSet()
    {
        return charSet;
    }

    public void setCharSet(int charSet)
    {
        this.charSet = charSet;
    }

    public int getDataLength()
    {
        return dataLength;
    }

    public void setDataLength(int dataLength)
    {
        this.dataLength = dataLength;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public String getContentId()
    {
        return contentId;
    }

    public void setContentId(String contentId)
    {
        this.contentId = contentId;
    }

    public String getContentLocation()
    {
        return contentLocation;
    }

    public void setContentLocation(String contentLocation)
    {
        this.contentLocation = contentLocation;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public byte[] getData()
    {
        return data;
    }

    public void setData(byte[] data)
    {
        this.data = data;
    }

    public String getDataUri()
    {
        return dataUri;
    }

    public void setDataUri(String dataUri)
    {
        this.dataUri = dataUri;
    }

    public String getContentDisposition()
    {
        return contentDisposition;
    }

    public void setContentDisposition(String contentDisposition)
    {
        this.contentDisposition = contentDisposition;
    }

    public String getContentTransferEncoding()
    {
        return contentTransferEncoding;
    }

    public void setContentTransferEncoding(String contentTransferEncoding)
    {
        this.contentTransferEncoding = contentTransferEncoding;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getGenerateLocation()
    {
        return generateLocation;
    }

    public void setGenerateLocation(String generateLocation)
    {
        this.generateLocation = generateLocation;
    }

    @Override
    public String toString()
    {
        return "PduPartModel{" +
                "id='" + id + '\'' +
                ", charSet=" + charSet +
                ", dataLength=" + dataLength +
                ", contentType='" + contentType + '\'' +
                ", contentId='" + contentId + '\'' +
                ", contentLocation='" + contentLocation + '\'' +
                ", name='" + name + '\'' +
                ", data=" + "[data*...*data]" +
                ", dataUri='" + dataUri + '\'' +
                ", contentDisposition='" + contentDisposition + '\'' +
                ", contentTransferEncoding='" + contentTransferEncoding + '\'' +
                ", fileName='" + fileName + '\'' +
                ", generateLocation='" + generateLocation + '\'' +
                "} " + super.toString();
    }
}
