package cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus;

import java.io.Serializable;
import java.util.Arrays;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

/**
 * Class representing MM1_retrieve.RES PDU
 */
public class RetrieveConfModel extends WebSocketModelBase implements Serializable
{
    private String id;
    private String contentType;
    private int deliveryReport;
    private String msgClass;
    private String messageId;
    private String transactionId;
    private int readReport;
    private int retrieveStatus;
    private String retrieveText;
    private int mmsVersion;
    private String from;
    private Long date;
    private String subject;
    private int priority;
    private String messageClass;
    private String[] to;
    private String[] bcc;
    private String[] cc;
    private PduPartModel[] parts;
    private long replyChargingSize;
    private int replyCharging;
    private byte[] auxApplicId;
    private byte[] applicId;
    private int contentClass;
    private int drmContent;
    private int distributionIndicator;
    private byte[] replaceId;
    private byte[] replyApplicId;
    private int replyChargingDeadline;
    private byte[] replyChargingId;

    public RetrieveConfModel()
    {
    }

    public RetrieveConfModel(String id, String contentType, int deliveryReport, String msgClass, String messageId, String transactionId, int readReport, int retrieveStatus, String retrieveText, int mmsVersion, String from, Long date, String subject, int priority, String messageClass, String[] to, String[] bcc, String[] cc, PduPartModel[] parts)
    {
        this.id = id;
        this.contentType = contentType;
        this.deliveryReport = deliveryReport;
        this.msgClass = msgClass;
        this.messageId = messageId;
        this.transactionId = transactionId;
        this.readReport = readReport;
        this.retrieveStatus = retrieveStatus;
        this.retrieveText = retrieveText;
        this.mmsVersion = mmsVersion;
        this.from = from;
        this.date = date;
        this.subject = subject;
        this.priority = priority;
        this.messageClass = messageClass;
        this.to = to;
        this.bcc = bcc;
        this.cc = cc;
        this.parts = parts;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public int getDeliveryReport()
    {
        return deliveryReport;
    }

    public void setDeliveryReport(int deliveryReport)
    {
        this.deliveryReport = deliveryReport;
    }

    public String getMsgClass()
    {
        return msgClass;
    }

    public void setMsgClass(String msgClass)
    {
        this.msgClass = msgClass;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public String getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }

    public int getReadReport()
    {
        return readReport;
    }

    public void setReadReport(int readReport)
    {
        this.readReport = readReport;
    }

    public int getRetrieveStatus()
    {
        return retrieveStatus;
    }

    public void setRetrieveStatus(int retrieveStatus)
    {
        this.retrieveStatus = retrieveStatus;
    }

    public String getRetrieveText()
    {
        return retrieveText;
    }

    public void setRetrieveText(String retrieveText)
    {
        this.retrieveText = retrieveText;
    }

    public int getMmsVersion()
    {
        return mmsVersion;
    }

    public void setMmsVersion(int mmsVersion)
    {
        this.mmsVersion = mmsVersion;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public Long getDate()
    {
        return date;
    }

    public void setDate(Long date)
    {
        this.date = date;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    public String getMessageClass()
    {
        return messageClass;
    }

    public void setMessageClass(String messageClass)
    {
        this.messageClass = messageClass;
    }

    public String[] getTo()
    {
        return to;
    }

    public void setTo(String[] to)
    {
        this.to = to;
    }

    public String[] getBcc()
    {
        return bcc;
    }

    public void setBcc(String[] bcc)
    {
        this.bcc = bcc;
    }

    public String[] getCc()
    {
        return cc;
    }

    public void setCc(String[] cc)
    {
        this.cc = cc;
    }

    public PduPartModel[] getParts()
    {
        return parts;
    }

    public void setParts(PduPartModel[] parts)
    {
        this.parts = parts;
    }

    public void setApplicId(byte[] applicId)
    {
        this.applicId = applicId;
    }

    public void setAuxApplicId(byte[] auxApplicId)
    {
        this.auxApplicId = auxApplicId;
    }

    public void setContentClass(int contentClass)
    {
        this.contentClass = contentClass;
    }

    public void setDrmContent(int drmContent)
    {
        this.drmContent = drmContent;
    }

    public void setDistributionIndicator(int distributionIndicator)
    {
        this.distributionIndicator = distributionIndicator;
    }

    public void setReplaceId(byte[] replaceId)
    {
        this.replaceId = replaceId;
    }

    public void setReplyApplicId(byte[] replyApplicId)
    {
        this.replyApplicId = replyApplicId;
    }

    public void setReplyCharging(int replyCharging)
    {
        this.replyCharging = replyCharging;
    }

    public void setReplyChargingDeadline(int replyChargingDeadline)
    {
        this.replyChargingDeadline = replyChargingDeadline;
    }

    public void setReplyChargingId(byte[] replyChargingId)
    {
        this.replyChargingId = replyChargingId;
    }

    public void setReplyChargingSize(long replyChargingSize)
    {
        this.replyChargingSize = replyChargingSize;
    }

    public long getReplyChargingSize()
    {
        return replyChargingSize;
    }

    public int getReplyCharging()
    {
        return replyCharging;
    }

    public byte[] getAuxApplicId()
    {
        return auxApplicId;
    }

    public byte[] getApplicId()
    {
        return applicId;
    }

    public int getContentClass()
    {
        return contentClass;
    }

    public int getDrmContent()
    {
        return drmContent;
    }

    public int getDistributionIndicator()
    {
        return distributionIndicator;
    }

    public byte[] getReplaceId()
    {
        return replaceId;
    }

    public byte[] getReplyApplicId()
    {
        return replyApplicId;
    }

    public int getReplyChargingDeadline()
    {
        return replyChargingDeadline;
    }

    public byte[] getReplyChargingId()
    {
        return replyChargingId;
    }


    @Override
    public String toString()
    {
        return "RetrieveConfModel{" +
                "id='" + id + '\'' +
                ", contentType='" + contentType + '\'' +
                ", deliveryReport=" + deliveryReport +
                ", msgClass='" + msgClass + '\'' +
                ", messageId='" + messageId + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", readReport=" + readReport +
                ", retrieveStatus=" + retrieveStatus +
                ", retrieveText='" + retrieveText + '\'' +
                ", mmsVersion=" + mmsVersion +
                ", from='" + from + '\'' +
                ", date=" + date +
                ", subject='" + subject + '\'' +
                ", priority=" + priority +
                ", messageClass='" + messageClass + '\'' +
                ", to=" + Arrays.toString(to) +
                ", bcc=" + Arrays.toString(bcc) +
                ", cc=" + Arrays.toString(cc) +
                ", parts=" + Arrays.toString(parts) +
                ", replyChargingSize=" + replyChargingSize +
                ", replyCharging=" + replyCharging +
                ", auxApplicId=" + Arrays.toString(auxApplicId) +
                ", applicId=" + Arrays.toString(applicId) +
                ", contentClass=" + contentClass +
                ", drmContent=" + drmContent +
                ", distributionIndicator=" + distributionIndicator +
                ", replaceId=" + Arrays.toString(replaceId) +
                ", replyApplicId=" + Arrays.toString(replyApplicId) +
                ", replyChargingDeadline=" + replyChargingDeadline +
                ", replyChargingId=" + Arrays.toString(replyChargingId) +
                '}';
    }
}
