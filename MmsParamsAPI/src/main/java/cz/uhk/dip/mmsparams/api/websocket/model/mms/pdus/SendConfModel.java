package cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus;

import java.io.Serializable;
import java.util.Arrays;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

/**
 * Class representing MM1_submit.RES PDU
 */
public class SendConfModel extends WebSocketModelBase implements Serializable
{
    private String id;
    private String transactionId;
    private String responseText;
    private int responseStatus;
    private int mmsVersion;
    private String from;
    private String messageId;
    private byte[] storeStatusText;
    private String contentLocation;
    private int storeStatus;

    public SendConfModel()
    {
    }

    public SendConfModel(String id, String transactionId, String responseText, int responseStatus, int mmsVersion, String from, String messageId)
    {
        this.id = id;
        this.transactionId = transactionId;
        this.responseText = responseText;
        this.responseStatus = responseStatus;
        this.mmsVersion = mmsVersion;
        this.from = from;
        this.messageId = messageId;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getResponseText()
    {
        return responseText;
    }

    public void setResponseText(String responseText)
    {
        this.responseText = responseText;
    }

    public int getResponseStatus()
    {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus)
    {
        this.responseStatus = responseStatus;
    }

    public int getMmsVersion()
    {
        return mmsVersion;
    }

    public void setMmsVersion(int mmsVersion)
    {
        this.mmsVersion = mmsVersion;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public void setContentLocation(String contentLocation)
    {
        this.contentLocation = contentLocation;
    }

    public void setStoreStatus(int storeStatus)
    {
        this.storeStatus = storeStatus;
    }

    public void setStoreStatusText(byte[] storeStatusText)
    {
        this.storeStatusText = storeStatusText;
    }

    public byte[] getStoreStatusText()
    {
        return storeStatusText;
    }

    public String getContentLocation()
    {
        return contentLocation;
    }

    public int getStoreStatus()
    {
        return storeStatus;
    }

    @Override
    public String toString()
    {
        return "SendConfModel{" +
                "id='" + id + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", responseText='" + responseText + '\'' +
                ", responseStatus=" + responseStatus +
                ", mmsVersion=" + mmsVersion +
                ", from='" + from + '\'' +
                ", messageId='" + messageId + '\'' +
                ", storeStatusText=" + Arrays.toString(storeStatusText) +
                ", contentLocation='" + contentLocation + '\'' +
                ", storeStatus=" + storeStatus +
                '}';
    }
}
