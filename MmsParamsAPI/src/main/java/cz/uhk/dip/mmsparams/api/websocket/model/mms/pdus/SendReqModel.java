package cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus;

import java.io.Serializable;
import java.util.Arrays;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

/**
 * Class representing MM1_submit.REQ PDU
 */
public class SendReqModel extends WebSocketModelBase implements Serializable
{
    private String id;
    private String transactionID;
    private int deliveryReport;
    private int readReport;
    private Long expiry;
    private String from;
    private Long messageSize;
    private Long date;
    private String subject;
    private int priority;
    private int mmsVersion;
    private String contentType;
    private String messageClass;
    private String[] to;
    private String[] bcc;
    private String[] cc;
    private PduPartModel[] parts;

    public SendReqModel()
    {
    }

    public SendReqModel(String id, String transactionID, int deliveryReport, int readReport, Long expiry, String from,
                        Long messageSize, Long date, String subject, int priority, int mmsVersion,
                        String contentType, String messageClass, String[] to, String[] bcc, String[] cc, PduPartModel[] parts)
    {
        this.id = id;
        this.transactionID = transactionID;
        this.deliveryReport = deliveryReport;
        this.readReport = readReport;
        this.expiry = expiry;
        this.from = from;
        this.messageSize = messageSize;
        this.date = date;
        this.subject = subject;
        this.priority = priority;
        this.mmsVersion = mmsVersion;
        this.contentType = contentType;
        this.messageClass = messageClass;
        this.to = to;
        this.bcc = bcc;
        this.cc = cc;
        this.parts = parts;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTransactionID()
    {
        return transactionID;
    }

    public void setTransactionID(String transactionID)
    {
        this.transactionID = transactionID;
    }

    public int getDeliveryReport()
    {
        return deliveryReport;
    }

    public void setDeliveryReport(int deliveryReport)
    {
        this.deliveryReport = deliveryReport;
    }

    public int getReadReport()
    {
        return readReport;
    }

    public void setReadReport(int readReport)
    {
        this.readReport = readReport;
    }

    public Long getExpiry()
    {
        return expiry;
    }

    public void setExpiry(Long expiry)
    {
        this.expiry = expiry;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public Long getMessageSize()
    {
        return messageSize;
    }

    public void setMessageSize(Long messageSize)
    {
        this.messageSize = messageSize;
    }

    public Long getDate()
    {
        return date;
    }

    public void setDate(Long date)
    {
        this.date = date;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    public int getMmsVersion()
    {
        return mmsVersion;
    }

    public void setMmsVersion(int mmsVersion)
    {
        this.mmsVersion = mmsVersion;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public String getMessageClass()
    {
        return messageClass;
    }

    public void setMessageClass(String messageClass)
    {
        this.messageClass = messageClass;
    }

    public String[] getTo()
    {
        return to;
    }

    public void setTo(String[] to)
    {
        this.to = to;
    }

    public String[] getBcc()
    {
        return bcc;
    }

    public void setBcc(String[] bcc)
    {
        this.bcc = bcc;
    }

    public String[] getCc()
    {
        return cc;
    }

    public void setCc(String[] cc)
    {
        this.cc = cc;
    }

    public PduPartModel[] getParts()
    {
        return parts;
    }

    public void setParts(PduPartModel[] parts)
    {
        this.parts = parts;
    }


    @Override
    public String toString()
    {
        return "SendReqModel{" +
                "id='" + id + '\'' +
                ", transactionID='" + transactionID + '\'' +
                ", deliveryReport=" + deliveryReport +
                ", readReport=" + readReport +
                ", expiry=" + expiry +
                ", from='" + from + '\'' +
                ", messageSize=" + messageSize +
                ", date=" + date +
                ", subject='" + subject + '\'' +
                ", priority=" + priority +
                ", mmsVersion=" + mmsVersion +
                ", contentType='" + contentType + '\'' +
                ", messageClass='" + messageClass + '\'' +
                ", to=" + Arrays.toString(to) +
                ", bcc=" + Arrays.toString(bcc) +
                ", cc=" + Arrays.toString(cc) +
                ", parts=" + Arrays.toString(parts) +
                '}';
    }
}
