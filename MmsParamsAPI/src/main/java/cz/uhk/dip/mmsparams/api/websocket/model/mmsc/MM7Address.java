package cz.uhk.dip.mmsparams.api.websocket.model.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.enums.AddressCoding;
import cz.uhk.dip.mmsparams.api.enums.AddressType;
import cz.uhk.dip.mmsparams.api.enums.RecipientType;

public class MM7Address implements Serializable
{
    private String address;
    private AddressType addressType;
    private RecipientType recipientType;
    private boolean displayOnly;
    private AddressCoding addressCoding;
    private String id;

    public MM7Address()
    {
    }

    public MM7Address(String address, AddressType addressType, RecipientType recipientType)
    {
        this(address, addressType, recipientType, false);
    }

    public MM7Address(String address, AddressType addressType, RecipientType recipientType, boolean displayOnly)
    {
        this(address, addressType, recipientType, displayOnly, null);
    }

    public MM7Address(String address, AddressType addressType, RecipientType recipientType, boolean displayOnly, AddressCoding addressCoding)
    {
        this.address = address;
        this.addressType = addressType;
        this.recipientType = recipientType;
        this.displayOnly = displayOnly;
        this.addressCoding = addressCoding;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public AddressType getAddressType()
    {
        return addressType;
    }

    public void setAddressType(AddressType addressType)
    {
        this.addressType = addressType;
    }

    public RecipientType getRecipientType()
    {
        return recipientType;
    }

    public void setRecipientType(RecipientType recipientType)
    {
        this.recipientType = recipientType;
    }

    public boolean isDisplayOnly()
    {
        return displayOnly;
    }

    public void setDisplayOnly(boolean displayOnly)
    {
        this.displayOnly = displayOnly;
    }

    public AddressCoding getAddressCoding()
    {
        return addressCoding;
    }

    public void setAddressCoding(AddressCoding addressCoding)
    {
        this.addressCoding = addressCoding;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public String toString()
    {
        return "MM7Address{" +
                "address='" + address + '\'' +
                ", addressType=" + addressType +
                ", recipientType=" + recipientType +
                ", displayOnly=" + displayOnly +
                ", addressCoding=" + addressCoding +
                ", id='" + id + '\'' +
                '}';
    }
}
