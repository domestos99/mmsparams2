package cz.uhk.dip.mmsparams.api.websocket.model.mmsc;

import java.io.Serializable;
import java.util.Arrays;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class MM7Attachment extends WebSocketModelBase implements Serializable
{
    private byte[] data;
    private String contentType;
    private String contentId;

    public byte[] getData()
    {
        return data;
    }

    public void setData(byte[] data)
    {
        this.data = data;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public String getContentId()
    {
        return contentId;
    }

    public void setContentId(String contentId)
    {
        this.contentId = contentId;
    }

    @Override
    public String toString()
    {
        return "MM7Attachment{" +
                "data=" + Arrays.toString(data) +
                ", contentType='" + contentType + '\'' +
                ", contentId='" + contentId + '\'' +
                "} " + super.toString();
    }
}
