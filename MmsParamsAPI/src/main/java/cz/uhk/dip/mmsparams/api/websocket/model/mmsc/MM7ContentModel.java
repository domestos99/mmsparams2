package cz.uhk.dip.mmsparams.api.websocket.model.mmsc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import cz.uhk.dip.mmsparams.api.enums.MM7Encoding;
import cz.uhk.dip.mmsparams.api.enums.mmsc.MM7ContentType;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class MM7ContentModel extends WebSocketModelBase implements Serializable
{

    private String contentLocation;
    private String contentId;
    private String contentType;
    private ArrayList<MM7ContentModel> parts;
    private MM7ContentType mm7ContentType;

    private String text;
    private byte[] data;
    private MM7Encoding mm7Encoding;

    public MM7ContentModel()
    {
        this.parts = new ArrayList<>();
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public byte[] getData()
    {
        return data;
    }

    public void setData(byte[] data)
    {
        this.data = data;
    }

    public MM7Encoding getMm7Encoding()
    {
        return mm7Encoding;
    }

    public void setMm7Encoding(MM7Encoding mm7Encoding)
    {
        this.mm7Encoding = mm7Encoding;
    }

    public String getContentLocation()
    {
        return contentLocation;
    }

    public void setContentLocation(String contentLocation)
    {
        this.contentLocation = contentLocation;
    }

    public String getContentId()
    {
        return contentId;
    }

    public void setContentId(String contentId)
    {
        this.contentId = contentId;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public ArrayList<MM7ContentModel> getParts()
    {
        return parts;
    }

    public void setParts(ArrayList<MM7ContentModel> parts)
    {
        this.parts = parts;
    }

    public void addPart(MM7ContentModel part)
    {
        if (this.parts == null)
        {
            parts = new ArrayList<>();
        }
        this.parts.add(part);
    }

    public MM7ContentType getMm7ContentType()
    {
        return mm7ContentType;
    }

    public void setMm7ContentType(MM7ContentType mm7ContentType)
    {
        this.mm7ContentType = mm7ContentType;
    }

    @Override
    public String toString()
    {
        return "MM7ContentModel{" +
                "contentLocation='" + contentLocation + '\'' +
                ", contentId='" + contentId + '\'' +
                ", contentType='" + contentType + '\'' +
                ", parts=" + parts +
                ", mm7ContentType=" + mm7ContentType +
                ", text='" + text + '\'' +
                ", data=" + Arrays.toString(data) +
                ", mm7Encoding=" + mm7Encoding +
                '}';
    }
}
