package cz.uhk.dip.mmsparams.api.websocket.model.mmsc;

import java.io.Serializable;
import java.util.Date;

public class MM7DeliveryReportReqModel extends MM7RequestModelBase implements Serializable
{
    private String messageID;
    private MM7Address recipient;
    private MM7Address sender;
    private Date date;
    private String mmStatus;
    private String statusText;

    public String getMessageID()
    {
        return messageID;
    }

    public void setMessageID(String messageID)
    {
        this.messageID = messageID;
    }

    public MM7Address getRecipient()
    {
        return recipient;
    }

    public void setRecipient(MM7Address recipient)
    {
        this.recipient = recipient;
    }

    public MM7Address getSender()
    {
        return sender;
    }

    public void setSender(MM7Address sender)
    {
        this.sender = sender;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getMmStatus()
    {
        return mmStatus;
    }

    public void setMmStatus(String mmStatus)
    {
        this.mmStatus = mmStatus;
    }

    public String getStatusText()
    {
        return statusText;
    }

    public void setStatusText(String statusText)
    {
        this.statusText = statusText;
    }

    @Override
    public String toString()
    {
        return "MM7DeliveryReportReqModel{" +
                "messageID='" + messageID + '\'' +
                ", recipient=" + recipient +
                ", sender=" + sender +
                ", date=" + date +
                ", mmStatus='" + mmStatus + '\'' +
                ", statusText='" + statusText + '\'' +
                "} " + super.toString();
    }
}
