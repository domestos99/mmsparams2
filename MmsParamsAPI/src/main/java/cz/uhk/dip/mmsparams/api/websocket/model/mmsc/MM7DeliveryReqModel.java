package cz.uhk.dip.mmsparams.api.websocket.model.mmsc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import cz.uhk.dip.mmsparams.api.enums.Priority;

public class MM7DeliveryReqModel extends MM7RequestModelBase implements Serializable
{
    private MM7Address sender;
    private String linkedId;
    private ArrayList<MM7Address> recipients;
    private String senderSPI;
    private String recipientSPI;
    private Date timeStamp;
    private String replyChargingId;
    private Priority priority;
    private String subject;
    private String applicId;
    private String replyApplicId;
    private String auxApplicInfo;
    private MM7ContentModel content;

    public MM7Address getSender()
    {
        return sender;
    }

    public void setSender(MM7Address sender)
    {
        this.sender = sender;
    }

    public String getLinkedId()
    {
        return linkedId;
    }

    public void setLinkedId(String linkedId)
    {
        this.linkedId = linkedId;
    }

    public ArrayList<MM7Address> getRecipients()
    {
        return recipients;
    }

    public void setRecipients(ArrayList<MM7Address> recipients)
    {
        this.recipients = recipients;
    }

    public String getSenderSPI()
    {
        return senderSPI;
    }

    public void setSenderSPI(String senderSPI)
    {
        this.senderSPI = senderSPI;
    }

    public String getRecipientSPI()
    {
        return recipientSPI;
    }

    public void setRecipientSPI(String recipientSPI)
    {
        this.recipientSPI = recipientSPI;
    }

    public Date getTimeStamp()
    {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp)
    {
        this.timeStamp = timeStamp;
    }

    public String getReplyChargingId()
    {
        return replyChargingId;
    }

    public void setReplyChargingId(String replyChargingId)
    {
        this.replyChargingId = replyChargingId;
    }

    public Priority getPriority()
    {
        return priority;
    }

    public void setPriority(Priority priority)
    {
        this.priority = priority;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getApplicId()
    {
        return applicId;
    }

    public void setApplicId(String applicId)
    {
        this.applicId = applicId;
    }

    public String getReplyApplicId()
    {
        return replyApplicId;
    }

    public void setReplyApplicId(String replyApplicId)
    {
        this.replyApplicId = replyApplicId;
    }

    public String getAuxApplicInfo()
    {
        return auxApplicInfo;
    }

    public void setAuxApplicInfo(String auxApplicInfo)
    {
        this.auxApplicInfo = auxApplicInfo;
    }

    public MM7ContentModel getContent()
    {
        return content;
    }

    public void setContent(MM7ContentModel content)
    {
        this.content = content;
    }

    @Override
    public String toString()
    {
        return "MM7DeliverReqModel{" +
                "sender=" + sender +
                ", linkedId='" + linkedId + '\'' +
                ", recipients=" + recipients +
                ", senderSPI='" + senderSPI + '\'' +
                ", recipientSPI='" + recipientSPI + '\'' +
                ", timeStamp=" + timeStamp +
                ", replyChargingId='" + replyChargingId + '\'' +
                ", priority=" + priority +
                ", subject='" + subject + '\'' +
                ", applicId='" + applicId + '\'' +
                ", replyApplicId='" + replyApplicId + '\'' +
                ", auxApplicInfo='" + auxApplicInfo + '\'' +
                ", content=" + content +
                "} " + super.toString();
    }
}
