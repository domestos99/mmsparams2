package cz.uhk.dip.mmsparams.api.websocket.model.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class MM7ErrorModel extends WebSocketModelBase implements Serializable
{
    private String faultCode;
    private String faultMessage;
    private String message;
    private Exception exception;


    public String getFaultCode()
    {
        return faultCode;
    }

    public void setFaultCode(String faultCode)
    {
        this.faultCode = faultCode;
    }

    public String getFaultMessage()
    {
        return faultMessage;
    }

    public void setFaultMessage(String faultMessage)
    {
        this.faultMessage = faultMessage;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Exception getException()
    {
        return exception;
    }

    public void setException(Exception exception)
    {
        this.exception = exception;
    }

    @Override
    public String toString()
    {
        return "MM7ErrorModel{" +
                "faultCode='" + faultCode + '\'' +
                ", faultMessage='" + faultMessage + '\'' +
                ", message='" + message + '\'' +
                ", exception=" + exception +
                "} " + super.toString();
    }
}
