package cz.uhk.dip.mmsparams.api.websocket.model.mmsc;

import java.io.Serializable;

public class MM7RelativeDate implements Serializable
{
    private String representation;

    public MM7RelativeDate()
    {
    }

    public MM7RelativeDate(String representation)
    {
        this.representation = representation;
    }

    public String getRepresentation()
    {
        return representation;
    }

    public void setRepresentation(String representation)
    {
        this.representation = representation;
    }
}
