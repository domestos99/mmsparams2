package cz.uhk.dip.mmsparams.api.websocket.model.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class MM7RequestModelBase extends WebSocketModelBase implements Serializable
{
    private MM7Address senderAddress;
    private String relayServerId;
    private String vaspId;
    private String vasId;
    private String mm7Version;
    private String mm7NamespacePrefix;
    private String soapBoundary;
    private String soapContentId = "mm7-soap";
    private String soapContentType;
    private String transactionId;
    private String namespace;

    public MM7Address getSenderAddress()
    {
        return senderAddress;
    }

    public void setSenderAddress(MM7Address senderAddress)
    {
        this.senderAddress = senderAddress;
    }

    public String getRelayServerId()
    {
        return relayServerId;
    }

    public void setRelayServerId(String relayServerId)
    {
        this.relayServerId = relayServerId;
    }

    public String getVaspId()
    {
        return vaspId;
    }

    public void setVaspId(String vaspId)
    {
        this.vaspId = vaspId;
    }

    public String getVasId()
    {
        return vasId;
    }

    public void setVasId(String vasId)
    {
        this.vasId = vasId;
    }

    public String getMm7Version()
    {
        return mm7Version;
    }

    public void setMm7Version(String mm7Version)
    {
        this.mm7Version = mm7Version;
    }

    public String getMm7NamespacePrefix()
    {
        return mm7NamespacePrefix;
    }

    public void setMm7NamespacePrefix(String mm7NamespacePrefix)
    {
        this.mm7NamespacePrefix = mm7NamespacePrefix;
    }

    public String getSoapBoundary()
    {
        return soapBoundary;
    }

    public void setSoapBoundary(String soapBoundary)
    {
        this.soapBoundary = soapBoundary;
    }

    public String getSoapContentId()
    {
        return soapContentId;
    }

    public void setSoapContentId(String soapContentId)
    {
        this.soapContentId = soapContentId;
    }

    public String getSoapContentType()
    {
        return soapContentType;
    }

    public void setSoapContentType(String soapContentType)
    {
        this.soapContentType = soapContentType;
    }

    public String getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getNamespace()
    {
        return namespace;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    @Override
    public String toString()
    {
        return "MM7RequestModelBase{" +
                "senderAddress=" + senderAddress +
                ", relayServerId='" + relayServerId + '\'' +
                ", vaspId='" + vaspId + '\'' +
                ", vasId='" + vasId + '\'' +
                ", mm7Version='" + mm7Version + '\'' +
                ", mm7NamespacePrefix='" + mm7NamespacePrefix + '\'' +
                ", soapBoundary='" + soapBoundary + '\'' +
                ", soapContentId='" + soapContentId + '\'' +
                ", soapContentType='" + soapContentType + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", namespace='" + namespace + '\'' +
                "} " + super.toString();
    }
}
