package cz.uhk.dip.mmsparams.api.websocket.model.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class MM7SubmitResponseModel extends WebSocketModelBase implements Serializable
{
    private String messageId;
    private int statusCode;
    private String statusText;
    private String mm7Version;
    private String namespace;
    private String soapBoundary;
    private String soapContentId;
    private String soapContentType;
    private String transactionId;
    private boolean multipart;

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setStatusCode(int statusCode)
    {
        this.statusCode = statusCode;
    }

    public int getStatusCode()
    {
        return statusCode;
    }

    public void setStatusText(String statusText)
    {
        this.statusText = statusText;
    }

    public String getStatusText()
    {
        return statusText;
    }

    public void setMm7Version(String mm7Version)
    {
        this.mm7Version = mm7Version;
    }

    public String getMm7Version()
    {
        return mm7Version;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    public String getNamespace()
    {
        return namespace;
    }

    public void setSoapBoundary(String soapBoundary)
    {
        this.soapBoundary = soapBoundary;
    }

    public String getSoapBoundary()
    {
        return soapBoundary;
    }

    public void setSoapContentId(String soapContentId)
    {
        this.soapContentId = soapContentId;
    }

    public String getSoapContentId()
    {
        return soapContentId;
    }

    public void setSoapContentType(String soapContentType)
    {
        this.soapContentType = soapContentType;
    }

    public String getSoapContentType()
    {
        return soapContentType;
    }

    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getTransactionId()
    {
        return transactionId;
    }

    public void setMultipart(boolean multipart)
    {
        this.multipart = multipart;
    }

    public boolean getMultipart()
    {
        return multipart;
    }

    @Override
    public String toString()
    {
        return "MM7SubmitResponseModel{" +
                "messageId='" + messageId + '\'' +
                ", statusCode=" + statusCode +
                ", statusText='" + statusText + '\'' +
                ", mm7Version='" + mm7Version + '\'' +
                ", namespace='" + namespace + '\'' +
                ", soapBoundary='" + soapBoundary + '\'' +
                ", soapContentId='" + soapContentId + '\'' +
                ", soapContentType='" + soapContentType + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", multipart=" + multipart +
                "} " + super.toString();
    }
}
