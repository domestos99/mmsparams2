package cz.uhk.dip.mmsparams.api.websocket.model.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.enums.MM7Encoding;

public class MM7Text implements Serializable
{
    private String text;
    private MM7Encoding mm7Encoding;

    public MM7Text()
    {
    }

    public MM7Text(String text, MM7Encoding mm7Encoding)
    {
        this.text = text;
        this.mm7Encoding = mm7Encoding;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public MM7Encoding getMm7Encoding()
    {
        return mm7Encoding;
    }

    public void setMm7Encoding(MM7Encoding mm7Encoding)
    {
        this.mm7Encoding = mm7Encoding;
    }

    @Override
    public String toString()
    {
        return "MM7Text{" +
                "text='" + text + '\'' +
                ", mm7Encoding=" + mm7Encoding +
                '}';
    }
}
