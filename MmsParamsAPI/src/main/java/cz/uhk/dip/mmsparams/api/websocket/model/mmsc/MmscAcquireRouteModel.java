package cz.uhk.dip.mmsparams.api.websocket.model.mmsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class MmscAcquireRouteModel extends WebSocketModelBase implements Serializable
{
    private String pattern;

    public void setPattern(String pattern)
    {
        this.pattern = pattern;
    }

    public String getPattern()
    {
        return pattern;
    }
}
