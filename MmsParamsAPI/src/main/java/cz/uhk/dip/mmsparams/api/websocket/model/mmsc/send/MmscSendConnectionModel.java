package cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class MmscSendConnectionModel extends WebSocketModelBase implements Serializable
{
    private String vasId;
    private String vaspId;

    private String address;

    private String username;
    private String password;
    private String mm7Version;
    private String mm7NameSpace;


    public String getVasId()
    {
        return vasId;
    }

    public void setVasId(String vasId)
    {
        this.vasId = vasId;
    }

    public String getVaspId()
    {
        return vaspId;
    }

    public void setVaspId(String vaspId)
    {
        this.vaspId = vaspId;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getMm7Version()
    {
        return mm7Version;
    }

    public void setMm7Version(String mm7Version)
    {
        this.mm7Version = mm7Version;
    }

    public String getMm7NameSpace()
    {
        return mm7NameSpace;
    }

    public void setMm7NameSpace(String mm7NameSpace)
    {
        this.mm7NameSpace = mm7NameSpace;
    }

    @Override
    public String toString()
    {
        return "MmscSendConnectionModel{" +
                "vasId='" + vasId + '\'' +
                ", vaspId='" + vaspId + '\'' +
                ", address='" + address + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", mm7Version='" + mm7Version + '\'' +
                ", mm7NameSpace='" + mm7NameSpace + '\'' +
                "} " + super.toString();
    }
}
