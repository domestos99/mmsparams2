package cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send;

import java.io.Serializable;
import java.util.ArrayList;

import cz.uhk.dip.mmsparams.api.enums.ChargedParty;
import cz.uhk.dip.mmsparams.api.enums.MessageClass;
import cz.uhk.dip.mmsparams.api.enums.Priority;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Attachment;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7RelativeDate;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Text;

public class MmscSendMmsModel extends WebSocketModelBase implements Serializable
{
    private Boolean allowAdaptations;
    private String applicID;
    private String auxApplicId;
    private String chargedPartyId;
    private Boolean deliveryReport;
    private String linkedId;
    private Boolean drmContent;
    private Boolean distributionIndicator;
    private Boolean readReply;
    private Integer replyChargingSize;
    private String subject;
    private String replyApplicID;
    private String serviceCode;
    private Priority priority;
    private MessageClass messageClass;
    private ChargedParty changedParty;
    private ArrayList<MM7Address> recipientsTo;
    private ArrayList<MM7Address> recipientsBcc;
    private ArrayList<MM7Address> recipientsCc;
    private MM7RelativeDate expiryDate;


    private MM7Address senderAddress;
    private String relayServerId;

    private ArrayList<MM7Text> mm7Texts;
    private ArrayList<MM7Attachment> mm7Attachment;


    public Boolean getAllowAdaptations()
    {
        return allowAdaptations;
    }

    public void setAllowAdaptations(Boolean allowAdaptations)
    {
        this.allowAdaptations = allowAdaptations;
    }

    public String getApplicID()
    {
        return applicID;
    }

    public void setApplicID(String applicID)
    {
        this.applicID = applicID;
    }

    public String getAuxApplicId()
    {
        return auxApplicId;
    }

    public void setAuxApplicId(String auxApplicId)
    {
        this.auxApplicId = auxApplicId;
    }

    public String getChargedPartyId()
    {
        return chargedPartyId;
    }

    public void setChargedPartyId(String chargedPartyId)
    {
        this.chargedPartyId = chargedPartyId;
    }

    public Boolean getDeliveryReport()
    {
        return deliveryReport;
    }

    public void setDeliveryReport(Boolean deliveryReport)
    {
        this.deliveryReport = deliveryReport;
    }

    public String getLinkedId()
    {
        return linkedId;
    }

    public void setLinkedId(String linkedId)
    {
        this.linkedId = linkedId;
    }

    public Boolean getDrmContent()
    {
        return drmContent;
    }

    public void setDrmContent(Boolean drmContent)
    {
        this.drmContent = drmContent;
    }

    public Boolean getDistributionIndicator()
    {
        return distributionIndicator;
    }

    public void setDistributionIndicator(Boolean distributionIndicator)
    {
        this.distributionIndicator = distributionIndicator;
    }

    public Boolean getReadReply()
    {
        return readReply;
    }

    public void setReadReply(Boolean readReply)
    {
        this.readReply = readReply;
    }

    public Integer getReplyChargingSize()
    {
        return replyChargingSize;
    }

    public void setReplyChargingSize(Integer replyChargingSize)
    {
        this.replyChargingSize = replyChargingSize;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getReplyApplicID()
    {
        return replyApplicID;
    }

    public void setReplyApplicID(String replyApplicID)
    {
        this.replyApplicID = replyApplicID;
    }

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public Priority getPriority()
    {
        return priority;
    }

    public void setPriority(Priority priority)
    {
        this.priority = priority;
    }

    public MessageClass getMessageClass()
    {
        return messageClass;
    }

    public void setMessageClass(MessageClass messageClass)
    {
        this.messageClass = messageClass;
    }

    public ChargedParty getChangedParty()
    {
        return changedParty;
    }

    public void setChangedParty(ChargedParty changedParty)
    {
        this.changedParty = changedParty;
    }

    public ArrayList<MM7Address> getRecipientsTo()
    {
        return recipientsTo;
    }

    public void setRecipientsTo(ArrayList<MM7Address> recipientsTo)
    {
        this.recipientsTo = recipientsTo;
    }

    public ArrayList<MM7Address> getRecipientsBcc()
    {
        return recipientsBcc;
    }

    public void setRecipientsBcc(ArrayList<MM7Address> recipientsBcc)
    {
        this.recipientsBcc = recipientsBcc;
    }

    public ArrayList<MM7Address> getRecipientsCc()
    {
        return recipientsCc;
    }

    public void setRecipientsCc(ArrayList<MM7Address> recipientsCc)
    {
        this.recipientsCc = recipientsCc;
    }

    public ArrayList<MM7Text> getMm7Texts()
    {
        return mm7Texts;
    }

    public void setMm7Texts(ArrayList<MM7Text> mm7Texts)
    {
        this.mm7Texts = mm7Texts;
    }

    public MM7Address getSenderAddress()
    {
        return senderAddress;
    }

    public void setSenderAddress(MM7Address senderAddress)
    {
        this.senderAddress = senderAddress;
    }

    public String getRelayServerId()
    {
        return relayServerId;
    }

    public void setRelayServerId(String relayServerId)
    {
        this.relayServerId = relayServerId;
    }

    public ArrayList<MM7Attachment> getMm7Attachment()
    {
        return mm7Attachment;
    }

    public void setMm7Attachment(ArrayList<MM7Attachment> mm7Attachment)
    {
        this.mm7Attachment = mm7Attachment;
    }

    public MM7RelativeDate getExpiryDate()
    {
        return expiryDate;
    }

    public void setExpiryDate(MM7RelativeDate expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public void addRecipientTo(MM7Address recipientAddress)
    {
        if (recipientsTo == null)
        {
            recipientsTo = new ArrayList<>();
        }
        recipientsTo.add(recipientAddress);
    }

    public void addRecipientBcc(MM7Address recipientBccAddress)
    {
        if (recipientsBcc == null)
        {
            recipientsBcc = new ArrayList<>();
        }
        recipientsBcc.add(recipientBccAddress);
    }

    public void addRecipientCc(MM7Address recipientCcAddress)
    {
        if (recipientsCc == null)
        {
            recipientsCc = new ArrayList<>();
        }
        recipientsCc.add(recipientCcAddress);
    }

    public void addMm7Attachment(MM7Attachment attachment)
    {
        if (mm7Attachment == null)
        {
            mm7Attachment = new ArrayList<>();
        }
        mm7Attachment.add(attachment);
    }

    public void addMm7Text(MM7Text text)
    {
        if (mm7Texts == null)
        {
            mm7Texts = new ArrayList<>();
        }
        mm7Texts.add(text);
    }

    @Override
    public String toString()
    {
        return "MmscSendMmsModel{" +
                "allowAdaptations=" + allowAdaptations +
                ", applicID='" + applicID + '\'' +
                ", auxApplicId='" + auxApplicId + '\'' +
                ", chargedPartyId='" + chargedPartyId + '\'' +
                ", deliveryReport=" + deliveryReport +
                ", linkedId='" + linkedId + '\'' +
                ", drmContent=" + drmContent +
                ", distributionIndicator=" + distributionIndicator +
                ", readReply=" + readReply +
                ", replyChargingSize=" + replyChargingSize +
                ", subject='" + subject + '\'' +
                ", replyApplicID='" + replyApplicID + '\'' +
                ", serviceCode='" + serviceCode + '\'' +
                ", priority=" + priority +
                ", messageClass=" + messageClass +
                ", changedParty=" + changedParty +
                ", recipientsTo=" + recipientsTo +
                ", recipientsBcc=" + recipientsBcc +
                ", recipientsCc=" + recipientsCc +
                ", expiryDate=" + expiryDate +
                ", senderAddress=" + senderAddress +
                ", relayServerId='" + relayServerId + '\'' +
                ", mm7Texts=" + mm7Texts +
                ", mm7Attachment=" + mm7Attachment +
                '}';
    }
}
