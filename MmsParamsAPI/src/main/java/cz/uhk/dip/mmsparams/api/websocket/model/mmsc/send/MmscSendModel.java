package cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class MmscSendModel extends WebSocketModelBase implements Serializable
{
    private MmscSendConnectionModel mmscSendConnectionModel;
    private MmscSendMmsModel mmscSendMmsModel;

    public MmscSendModel()
    {
    }

    public MmscSendModel(MmscSendConnectionModel mmscSendConnectionModel, MmscSendMmsModel mmscSendMmsModel)
    {
        this.mmscSendConnectionModel = mmscSendConnectionModel;
        this.mmscSendMmsModel = mmscSendMmsModel;
    }

    public MmscSendConnectionModel getMmscSendConnectionModel()
    {
        return mmscSendConnectionModel;
    }

    public void setMmscSendConnectionModel(MmscSendConnectionModel mmscSendConnectionModel)
    {
        this.mmscSendConnectionModel = mmscSendConnectionModel;
    }

    public MmscSendMmsModel getMmscSendMmsModel()
    {
        return mmscSendMmsModel;
    }

    public void setMmscSendMmsModel(MmscSendMmsModel mmscSendMmsModel)
    {
        this.mmscSendMmsModel = mmscSendMmsModel;
    }

    @Override
    public String toString()
    {
        return "MmscSendModel{" +
                "mmscSendConnectionModel=" + mmscSendConnectionModel +
                ", mmscSendMmsModel=" + mmscSendMmsModel +
                "} " + super.toString();
    }
}
