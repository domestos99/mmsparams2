//package cz.uhk.dip.mmsparams.websocket.model;
//
//import java.io.Serializable;
//import java.util.Arrays;
//
//import cz.uhk.dip.mmsparams.websocket.WebSocketModelBase;
//
//public class PhoneSystemInfoModel extends WebSocketModelBase implements Serializable
//{
//    private String networkOperator;
//    private String networkCountryIso;
//    private int networkType;
//    private boolean iccCard;
//    private int simState;
//    private String simOperator;
//    private String simOperatorName;
//    private String simCountryIso;
//    private int callState;
//    private int dataActivity;
//    private int dataState;
//    private boolean voiceCapable;
//    private boolean smsCapable;
//    private String mmsUserAgent;
//    private String mmsUAProfUrl;
//    private boolean carrierPrivileges;
//    private boolean changeDtmfToneLength;
//    private boolean worldPhone;
//    private boolean ttyModeSupported;
//    private boolean hearingAidCompatibilitySupported;
//    private String networkOperatorName;
//    private boolean networkRoaming;
//    private int phoneCount;
//    //private String[] forbiddenPlmns;
//    private boolean concurrentVoiceAndDataSupported;
//    private String line1Number;
//    private boolean dataEnabled;
//    private String voiceMailNumber;
//    private String groupIdLevel1;
//    private String voiceMailAlphaTag;
//    private String subscriberId;
//    private String visualVoicemailPackageName;
//    private String simSerialNumber;
//    private int voiceNetworkType;
//    private String networkSpecifier;
//    private String deviceSoftwareVersion;
//    private String deviceId;
//    private String imei;
//    private String meid;
//    private int phoneType;
//    private int dataNetworkType;
//
//    public void setPhoneCount(int phoneCount)
//    {
//        this.phoneCount = phoneCount;
//    }
//
//    public int getPhoneCount()
//    {
//        return phoneCount;
//    }
//
//    public void setNetworkOperator(String networkOperator)
//    {
//        this.networkOperator = networkOperator;
//    }
//
//    public String getNetworkOperator()
//    {
//        return networkOperator;
//    }
//
//    public void setNetworkCountryIso(String networkCountryIso)
//    {
//        this.networkCountryIso = networkCountryIso;
//    }
//
//    public String getNetworkCountryIso()
//    {
//        return networkCountryIso;
//    }
//
//    public void setNetworkType(int networkType)
//    {
//        this.networkType = networkType;
//    }
//
//    public int getNetworkType()
//    {
//        return networkType;
//    }
//
//    public void setIccCard(boolean iccCard)
//    {
//        this.iccCard = iccCard;
//    }
//
//    public boolean getIccCard()
//    {
//        return iccCard;
//    }
//
//    public void setSimState(int simState)
//    {
//        this.simState = simState;
//    }
//
//    public int getSimState()
//    {
//        return simState;
//    }
//
//    public void setSimOperator(String simOperator)
//    {
//        this.simOperator = simOperator;
//    }
//
//    public String getSimOperator()
//    {
//        return simOperator;
//    }
//
//    public void setSimOperatorName(String simOperatorName)
//    {
//        this.simOperatorName = simOperatorName;
//    }
//
//    public String getSimOperatorName()
//    {
//        return simOperatorName;
//    }
//
//    public void setSimCountryIso(String simCountryIso)
//    {
//        this.simCountryIso = simCountryIso;
//    }
//
//    public String getSimCountryIso()
//    {
//        return simCountryIso;
//    }
//
//    public void setCallState(int callState)
//    {
//
//        this.callState = callState;
//    }
//
//    public int getCallState()
//    {
//        return callState;
//    }
//
//    public void setDataActivity(int dataActivity)
//    {
//        this.dataActivity = dataActivity;
//    }
//
//    public int getDataActivity()
//    {
//        return dataActivity;
//    }
//
//    public void setDataState(int dataState)
//    {
//        this.dataState = dataState;
//    }
//
//    public int getDataState()
//    {
//        return dataState;
//    }
//
//    public void setVoiceCapable(boolean voiceCapable)
//    {
//        this.voiceCapable = voiceCapable;
//    }
//
//    public boolean getVoiceCapable()
//    {
//        return voiceCapable;
//    }
//
//    public void setSmsCapable(boolean smsCapable)
//    {
//        this.smsCapable = smsCapable;
//    }
//
//    public boolean getSmsCapable()
//    {
//        return smsCapable;
//    }
//
//    public void setMmsUserAgent(String mmsUserAgent)
//    {
//        this.mmsUserAgent = mmsUserAgent;
//    }
//
//    public String getMmsUserAgent()
//    {
//        return mmsUserAgent;
//    }
//
//    public void setMmsUAProfUrl(String mmsUAProfUrl)
//    {
//        this.mmsUAProfUrl = mmsUAProfUrl;
//    }
//
//    public String getMmsUAProfUrl()
//    {
//        return mmsUAProfUrl;
//    }
//
//    public void setCarrierPrivileges(boolean carrierPrivileges)
//    {
//        this.carrierPrivileges = carrierPrivileges;
//    }
//
//    public boolean getCarrierPrivileges()
//    {
//        return carrierPrivileges;
//    }
//
//    public void setChangeDtmfToneLength(boolean changeDtmfToneLength)
//    {
//        this.changeDtmfToneLength = changeDtmfToneLength;
//    }
//
//    public boolean getChangeDtmfToneLength()
//    {
//        return changeDtmfToneLength;
//    }
//
//    public void setWorldPhone(boolean worldPhone)
//    {
//        this.worldPhone = worldPhone;
//    }
//
//    public boolean getWorldPhone()
//    {
//        return worldPhone;
//    }
//
//    public void setTtyModeSupported(boolean ttyModeSupported)
//    {
//        this.ttyModeSupported = ttyModeSupported;
//    }
//
//    public boolean getTtyModeSupported()
//    {
//        return ttyModeSupported;
//    }
//
//    public void setHearingAidCompatibilitySupported(boolean hearingAidCompatibilitySupported)
//    {
//        this.hearingAidCompatibilitySupported = hearingAidCompatibilitySupported;
//    }
//
//    public boolean getHearingAidCompatibilitySupported()
//    {
//        return hearingAidCompatibilitySupported;
//    }
//
//
//    public void setDeviceSoftwareVersion(String deviceSoftwareVersion)
//    {
//        this.deviceSoftwareVersion = deviceSoftwareVersion;
//    }
//
//    public void setDeviceId(String deviceId)
//    {
//        this.deviceId = deviceId;
//    }
//
//    public void setImei(String imei)
//    {
//        this.imei = imei;
//    }
//
//    public void setMeid(String meid)
//    {
//        this.meid = meid;
//    }
//
//    public void setPhoneType(int phoneType)
//    {
//        this.phoneType = phoneType;
//    }
//
//    public void setNetworkOperatorName(String networkOperatorName)
//    {
//        this.networkOperatorName = networkOperatorName;
//    }
//
//    public String getNetworkOperatorName()
//    {
//        return networkOperatorName;
//    }
//
//    public void setNetworkRoaming(boolean networkRoaming)
//    {
//        this.networkRoaming = networkRoaming;
//    }
//
//    public boolean getNetworkRoaming()
//    {
//        return networkRoaming;
//    }
//
//    public void setNetworkSpecifier(String networkSpecifier)
//    {
//        this.networkSpecifier = networkSpecifier;
//    }
//
//    public void setDataNetworkType(int dataNetworkType)
//    {
//        this.dataNetworkType = dataNetworkType;
//    }
//
//    public void setVoiceNetworkType(int voiceNetworkType)
//    {
//        this.voiceNetworkType = voiceNetworkType;
//    }
//
//    public void setSimSerialNumber(String simSerialNumber)
//    {
//        this.simSerialNumber = simSerialNumber;
//    }
//
//
//    public void setSubscriberId(String subscriberId)
//    {
//        this.subscriberId = subscriberId;
//    }
//
//    public void setGroupIdLevel1(String groupIdLevel1)
//    {
//        this.groupIdLevel1 = groupIdLevel1;
//    }
//
//    public void setLine1Number(String line1Number)
//    {
//        this.line1Number = line1Number;
//    }
//
//    public void setVoiceMailNumber(String voiceMailNumber)
//    {
//        this.voiceMailNumber = voiceMailNumber;
//    }
//
//    public void setVisualVoicemailPackageName(String visualVoicemailPackageName)
//    {
//        this.visualVoicemailPackageName = visualVoicemailPackageName;
//    }
//
//    public void setVoiceMailAlphaTag(String voiceMailAlphaTag)
//    {
//        this.voiceMailAlphaTag = voiceMailAlphaTag;
//    }
//
////    public void setAllCellInfo()
////    {
////        this.
////    }
//
////    public void setForbiddenPlmns(String[] forbiddenPlmns)
////    {
////        this.forbiddenPlmns = forbiddenPlmns;
////    }
//
//    public void setConcurrentVoiceAndDataSupported(boolean concurrentVoiceAndDataSupported)
//    {
//        this.concurrentVoiceAndDataSupported = concurrentVoiceAndDataSupported;
//    }
//
//    public void setDataEnabled(boolean dataEnabled)
//    {
//        this.dataEnabled = dataEnabled;
//    }
//
//    public boolean isIccCard()
//    {
//        return iccCard;
//    }
//
//    public boolean isVoiceCapable()
//    {
//        return voiceCapable;
//    }
//
//    public boolean isSmsCapable()
//    {
//        return smsCapable;
//    }
//
//    public boolean isCarrierPrivileges()
//    {
//        return carrierPrivileges;
//    }
//
//    public boolean isChangeDtmfToneLength()
//    {
//        return changeDtmfToneLength;
//    }
//
//    public boolean isWorldPhone()
//    {
//        return worldPhone;
//    }
//
//    public boolean isTtyModeSupported()
//    {
//        return ttyModeSupported;
//    }
//
//    public boolean isHearingAidCompatibilitySupported()
//    {
//        return hearingAidCompatibilitySupported;
//    }
//
//    public boolean isNetworkRoaming()
//    {
//        return networkRoaming;
//    }
//
////    public String[] getForbiddenPlmns()
////    {
////        return forbiddenPlmns;
////    }
//
//    public boolean isConcurrentVoiceAndDataSupported()
//    {
//        return concurrentVoiceAndDataSupported;
//    }
//
//    public String getLine1Number()
//    {
//        return line1Number;
//    }
//
//    public boolean isDataEnabled()
//    {
//        return dataEnabled;
//    }
//
//    public String getVoiceMailNumber()
//    {
//        return voiceMailNumber;
//    }
//
//    public String getGroupIdLevel1()
//    {
//        return groupIdLevel1;
//    }
//
//    public String getVoiceMailAlphaTag()
//    {
//        return voiceMailAlphaTag;
//    }
//
//    public String getSubscriberId()
//    {
//        return subscriberId;
//    }
//
//    public String getVisualVoicemailPackageName()
//    {
//        return visualVoicemailPackageName;
//    }
//
//    public String getSimSerialNumber()
//    {
//        return simSerialNumber;
//    }
//
//    public int getVoiceNetworkType()
//    {
//        return voiceNetworkType;
//    }
//
//    public String getNetworkSpecifier()
//    {
//        return networkSpecifier;
//    }
//
//    public String getDeviceSoftwareVersion()
//    {
//        return deviceSoftwareVersion;
//    }
//
//    public String getDeviceId()
//    {
//        return deviceId;
//    }
//
//    public String getImei()
//    {
//        return imei;
//    }
//
//    public String getMeid()
//    {
//        return meid;
//    }
//
//    public int getPhoneType()
//    {
//        return phoneType;
//    }
//
//    public int getDataNetworkType()
//    {
//        return dataNetworkType;
//    }
//
//
//    @Override
//    public String toString()
//    {
//        return "PhoneSystemInfoModel{" +
//                "networkOperator='" + networkOperator + '\'' +
//                ", networkCountryIso='" + networkCountryIso + '\'' +
//                ", networkType=" + networkType +
//                ", iccCard=" + iccCard +
//                ", simState=" + simState +
//                ", simOperator='" + simOperator + '\'' +
//                ", simOperatorName='" + simOperatorName + '\'' +
//                ", simCountryIso='" + simCountryIso + '\'' +
//                ", callState=" + callState +
//                ", dataActivity=" + dataActivity +
//                ", dataState=" + dataState +
//                ", voiceCapable=" + voiceCapable +
//                ", smsCapable=" + smsCapable +
//                ", mmsUserAgent='" + mmsUserAgent + '\'' +
//                ", mmsUAProfUrl='" + mmsUAProfUrl + '\'' +
//                ", carrierPrivileges=" + carrierPrivileges +
//                ", changeDtmfToneLength=" + changeDtmfToneLength +
//                ", worldPhone=" + worldPhone +
//                ", ttyModeSupported=" + ttyModeSupported +
//                ", hearingAidCompatibilitySupported=" + hearingAidCompatibilitySupported +
//                ", networkOperatorName='" + networkOperatorName + '\'' +
//                ", networkRoaming=" + networkRoaming +
//                ", phoneCount=" + phoneCount +
//                ", concurrentVoiceAndDataSupported=" + concurrentVoiceAndDataSupported +
//                ", line1Number='" + line1Number + '\'' +
//                ", dataEnabled=" + dataEnabled +
//                ", voiceMailNumber='" + voiceMailNumber + '\'' +
//                ", groupIdLevel1='" + groupIdLevel1 + '\'' +
//                ", voiceMailAlphaTag='" + voiceMailAlphaTag + '\'' +
//                ", subscriberId='" + subscriberId + '\'' +
//                ", visualVoicemailPackageName='" + visualVoicemailPackageName + '\'' +
//                ", simSerialNumber='" + simSerialNumber + '\'' +
//                ", voiceNetworkType=" + voiceNetworkType +
//                ", networkSpecifier='" + networkSpecifier + '\'' +
//                ", deviceSoftwareVersion='" + deviceSoftwareVersion + '\'' +
//                ", deviceId='" + deviceId + '\'' +
//                ", imei='" + imei + '\'' +
//                ", meid='" + meid + '\'' +
//                ", phoneType=" + phoneType +
//                ", dataNetworkType=" + dataNetworkType +
//                "} " + super.toString();
//    }
//}
