package cz.uhk.dip.mmsparams.api.websocket.model.phone;

import java.io.Serializable;
import java.util.Objects;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class SubscriptionInfoModel extends WebSocketModelBase implements Serializable
{
    private int subscriptionId;
    private String iccId;
    private String carrierName;
    private String displayName;
    private int iconTint;
    private int mcc;
    private int mnc;
    private String countryIso;
    private int simSlotIndex;
    private String number;
    private int dataRoaming;

    public int getSubscriptionId()
    {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId)
    {
        this.subscriptionId = subscriptionId;
    }

    public String getIccId()
    {
        return iccId;
    }

    public void setIccId(String iccId)
    {
        this.iccId = iccId;
    }

    public String getCarrierName()
    {
        return carrierName;
    }

    public void setCarrierName(String carrierName)
    {
        this.carrierName = carrierName;
    }

    public int getIconTint()
    {
        return iconTint;
    }

    public void setIconTint(int iconTint)
    {
        this.iconTint = iconTint;
    }

    public int getMcc()
    {
        return mcc;
    }

    public void setMcc(int mcc)
    {
        this.mcc = mcc;
    }

    public int getMnc()
    {
        return mnc;
    }

    public void setMnc(int mnc)
    {
        this.mnc = mnc;
    }

    public String getCountryIso()
    {
        return countryIso;
    }

    public void setCountryIso(String countryIso)
    {
        this.countryIso = countryIso;
    }

    public int getSimSlotIndex()
    {
        return simSlotIndex;
    }

    public void setSimSlotIndex(int simSlotIndex)
    {
        this.simSlotIndex = simSlotIndex;
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public int getDataRoaming()
    {
        return dataRoaming;
    }

    public void setDataRoaming(int dataRoaming)
    {
        this.dataRoaming = dataRoaming;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriptionInfoModel that = (SubscriptionInfoModel) o;
        return subscriptionId == that.subscriptionId &&
                iconTint == that.iconTint &&
                mcc == that.mcc &&
                mnc == that.mnc &&
                simSlotIndex == that.simSlotIndex &&
                dataRoaming == that.dataRoaming &&
                Objects.equals(iccId, that.iccId) &&
                Objects.equals(carrierName, that.carrierName) &&
                Objects.equals(displayName, that.displayName) &&
                Objects.equals(countryIso, that.countryIso) &&
                Objects.equals(number, that.number);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(subscriptionId, iccId, carrierName, displayName, iconTint, mcc, mnc, countryIso, simSlotIndex, number, dataRoaming);
    }

    @Override
    public String toString()
    {
        return "SubscriptionInfoModel{" +
                "subscriptionId=" + subscriptionId +
                ", iccId='" + iccId + '\'' +
                ", carrierName=" + carrierName +
                ", displayName=" + displayName +
                ", iconTint=" + iconTint +
                ", mcc=" + mcc +
                ", mnc=" + mnc +
                ", countryIso='" + countryIso + '\'' +
                ", simSlotIndex=" + simSlotIndex +
                ", number='" + number + '\'' +
                ", dataRoaming=" + dataRoaming +
                "} " + super.toString();
    }
}
