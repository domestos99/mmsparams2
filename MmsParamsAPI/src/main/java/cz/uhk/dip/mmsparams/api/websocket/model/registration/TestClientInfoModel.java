package cz.uhk.dip.mmsparams.api.websocket.model.registration;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

/**
 * Model containing basic information about current test
 */
public class TestClientInfoModel extends WebSocketModelBase implements Serializable
{
    private String testName;
    private String testDesc;

    public String getTestName()
    {
        return testName;
    }

    public void setTestName(String testName)
    {
        this.testName = testName;
    }

    public String getTestDesc()
    {
        return testDesc;
    }

    public void setTestDesc(String testDesc)
    {
        this.testDesc = testDesc;
    }

    @Override
    public String toString()
    {
        return "TestClientInfoModel{" +
                "testName='" + testName + '\'' +
                ", testDesc='" + testDesc + '\'' +
                "} " + super.toString();
    }
}
