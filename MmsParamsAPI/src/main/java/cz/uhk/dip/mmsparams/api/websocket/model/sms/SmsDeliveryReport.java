package cz.uhk.dip.mmsparams.api.websocket.model.sms;

import java.io.Serializable;

public class SmsDeliveryReport extends SmsReceiveModel implements Serializable
{
    @Override
    public String toString()
    {
        return "SmsDeliveryReport{} " + super.toString();
    }
}
