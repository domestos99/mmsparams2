package cz.uhk.dip.mmsparams.api.websocket.model.sms;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class SmsSendModel extends WebSocketModelBase implements Serializable
{
    private String to;
    private String text;
    private boolean deliveryReport;

    // param scAddress is the service center address or null to use the current default SMSC
    private String serviceCenterSender;

    public SmsSendModel()
    {
    }

    public SmsSendModel(String to, String text, boolean deliveryReport)
    {
        this.to = to;
        this.text = text;
        this.deliveryReport = deliveryReport;
    }

    public String getTo()
    {
        return to;
    }

    public void setTo(String to)
    {
        this.to = to;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public boolean isDeliveryReport()
    {
        return deliveryReport;
    }

    public void setDeliveryReport(boolean deliveryReport)
    {
        this.deliveryReport = deliveryReport;
    }

    public String getServiceCenterSender()
    {
        return serviceCenterSender;
    }

    public void setServiceCenterSender(String serviceCenterSender)
    {
        this.serviceCenterSender = serviceCenterSender;
    }

    @Override
    public String toString()
    {
        return "SmsSend{" +
                ", to='" + to + '\'' +
                ", text='" + text + '\'' +
                ", deliveryReport=" + deliveryReport +
                "} " + super.toString();
    }
}
