package cz.uhk.dip.mmsparams.api.websocket.model.sms;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class SmsSendResponseModel extends WebSocketModelBase implements Serializable
{
    private int result;
    private String resultText;

    public int getResult()
    {
        return result;
    }

    public void setResult(int result)
    {
        this.result = result;
    }

    public String getResultText()
    {
        return resultText;
    }

    public void setResultText(String resultText)
    {
        this.resultText = resultText;
    }

    @Override
    public String toString()
    {
        return "SmsSendResponseModel{" +
                "result=" + result +
                ", resultTest='" + resultText + '\'' +
                "} " + super.toString();
    }
}
