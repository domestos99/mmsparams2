package cz.uhk.dip.mmsparams.api.websocket.model.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;

public class SmscAddressModel implements Serializable
{
    private TypeOfNumber typeOfNumber;
    private NumberingPlanIndicator numberingPlanIndicator;
    private String address;

    public SmscAddressModel()
    {
    }

    public SmscAddressModel(TypeOfNumber typeOfNumber, NumberingPlanIndicator numberingPlanIndicator, String address)
    {
        this.typeOfNumber = typeOfNumber;
        this.numberingPlanIndicator = numberingPlanIndicator;
        this.address = address;
    }

    public TypeOfNumber getTypeOfNumber()
    {
        return typeOfNumber;
    }

    public void setTypeOfNumber(TypeOfNumber typeOfNumber)
    {
        this.typeOfNumber = typeOfNumber;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public NumberingPlanIndicator getNumberingPlanIndicator()
    {
        return numberingPlanIndicator;
    }

    public void setNumberingPlanIndicator(NumberingPlanIndicator numberingPlanIndicator)
    {
        this.numberingPlanIndicator = numberingPlanIndicator;
    }

    @Override
    public String toString()
    {
        return "SmscAddressModel{" +
                "typeOfNumber=" + typeOfNumber +
                ", numberingPlanIndicator=" + numberingPlanIndicator +
                ", address='" + address + '\'' +
                '}';
    }
}
