package cz.uhk.dip.mmsparams.api.websocket.model.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.enums.smsc.SmscConnectType;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class SmscConnectModel extends WebSocketModelBase implements Serializable
{
    private String host;
    private int port;
    private String systemId;
    private String password;
    private String systemType;
    private SmscConnectType connType;
    private long connectTimeout;
    private boolean logBytes;
    private long requestExpiryTimeout;
    private long windowMonitorInterval;
    private boolean countersEnabled;

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public String getSystemId()
    {
        return systemId;
    }

    public void setSystemId(String systemId)
    {
        this.systemId = systemId;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public SmscConnectType getConnType()
    {
        return connType;
    }

    public void setConnType(SmscConnectType connType)
    {
        this.connType = connType;
    }

    public String getSystemType()
    {
        return systemType;
    }

    public void setSystemType(String systemType)
    {
        this.systemType = systemType;
    }

    public long getConnectTimeout()
    {
        return connectTimeout;
    }

    public void setConnectTimeout(long connectTimeout)
    {
        this.connectTimeout = connectTimeout;
    }

    public boolean getLogBytes()
    {
        return logBytes;
    }

    public void setLogBytes(boolean logBytes)
    {
        this.logBytes = logBytes;
    }

    public long getRequestExpiryTimeout()
    {
        return requestExpiryTimeout;
    }

    public void setRequestExpiryTimeout(long requestExpiryTimeout)
    {
        this.requestExpiryTimeout = requestExpiryTimeout;
    }

    public long getWindowMonitorInterval()
    {
        return windowMonitorInterval;
    }

    public void setWindowMonitorInterval(long windowMonitorInterval)
    {
        this.windowMonitorInterval = windowMonitorInterval;
    }

    public boolean getCountersEnabled()
    {
        return countersEnabled;
    }

    public void setCountersEnabled(boolean countersEnabled)
    {
        this.countersEnabled = countersEnabled;
    }

    @Override
    public String toString()
    {
        return "SmscConnectModel{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", systemId='" + systemId + '\'' +
                ", password='" + password + '\'' +
                ", systemType='" + systemType + '\'' +
                ", connType=" + connType +
                ", connectTimeout=" + connectTimeout +
                ", logBytes=" + logBytes +
                ", requestExpiryTimeout=" + requestExpiryTimeout +
                ", windowMonitorInterval=" + windowMonitorInterval +
                ", countersEnabled=" + countersEnabled +
                "} " + super.toString();
    }
}
