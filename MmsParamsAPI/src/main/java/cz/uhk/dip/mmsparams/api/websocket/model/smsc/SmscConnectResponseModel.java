package cz.uhk.dip.mmsparams.api.websocket.model.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.interfaces.IResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class SmscConnectResponseModel extends WebSocketModelBase implements Serializable, IResponseMessage
{
    private SmscSessionId sessionId;
    private boolean status;
    private SmscSessionModel smscSessionModel;

    public boolean getStatus()
    {
        return status;
    }

    public void setStatus(boolean status)
    {
        this.status = status;
    }

    public SmscSessionId getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(SmscSessionId sessionId)
    {
        this.sessionId = sessionId;
    }

    public void setSmscSessionModel(SmscSessionModel smscSessionModel)
    {
        this.smscSessionModel = smscSessionModel;
    }

    public SmscSessionModel getSmscSessionModel()
    {
        return smscSessionModel;
    }

    @Override
    public String toString()
    {
        return "SmscConnectResponseModel{" +
                "sessionId=" + sessionId +
                ", status=" + status +
                ", smscSessionModel=" + smscSessionModel +
                "} " + super.toString();
    }
}
