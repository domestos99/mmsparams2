package cz.uhk.dip.mmsparams.api.websocket.model.smsc;

import java.io.Serializable;
import java.util.Arrays;

public class SmscDeliverSmModel extends SmscModelBase implements Serializable
{
    private int shortMessageLength;
    private byte[] shortMessage;
    private byte replaceIfPresent;
    private byte dataCoding;
    private byte defaultMsgId;
    private byte registeredDelivery;
    private String validityPeriod;
    private String scheduleDeliveryTime;
    private byte priority;
    private byte esmClass;
    private byte protocolId;
    private String serviceType;
    private SmscAddressModel sourceAddress;
    private SmscAddressModel destAddress;

    public void setShortMessageLength(int shortMessageLength)
    {
        this.shortMessageLength = shortMessageLength;
    }

    public int getShortMessageLength()
    {
        return shortMessageLength;
    }

    public void setShortMessage(byte[] shortMessage)
    {
        this.shortMessage = shortMessage;
    }

    public byte[] getShortMessage()
    {
        return shortMessage;
    }

    public void setReplaceIfPresent(byte replaceIfPresent)
    {
        this.replaceIfPresent = replaceIfPresent;
    }

    public byte getReplaceIfPresent()
    {
        return replaceIfPresent;
    }

    public void setDataCoding(byte dataCoding)
    {
        this.dataCoding = dataCoding;
    }

    public byte getDataCoding()
    {
        return dataCoding;
    }

    public void setDefaultMsgId(byte defaultMsgId)
    {
        this.defaultMsgId = defaultMsgId;
    }

    public byte getDefaultMsgId()
    {
        return defaultMsgId;
    }

    public void setRegisteredDelivery(byte registeredDelivery)
    {
        this.registeredDelivery = registeredDelivery;
    }

    public byte getRegisteredDelivery()
    {
        return registeredDelivery;
    }

    public void setValidityPeriod(String validityPeriod)
    {
        this.validityPeriod = validityPeriod;
    }

    public String getValidityPeriod()
    {
        return validityPeriod;
    }

    public void setScheduleDeliveryTime(String scheduleDeliveryTime)
    {
        this.scheduleDeliveryTime = scheduleDeliveryTime;
    }

    public String getScheduleDeliveryTime()
    {
        return scheduleDeliveryTime;
    }

    public void setPriority(byte priority)
    {
        this.priority = priority;
    }

    public byte getPriority()
    {
        return priority;
    }

    public void setEsmClass(byte esmClass)
    {
        this.esmClass = esmClass;
    }

    public byte getEsmClass()
    {
        return esmClass;
    }

    public void setProtocolId(byte protocolId)
    {
        this.protocolId = protocolId;
    }

    public byte getProtocolId()
    {
        return protocolId;
    }

    public void setServiceType(String serviceType)
    {
        this.serviceType = serviceType;
    }

    public String getServiceType()
    {
        return serviceType;
    }

    public void setSourceAddress(SmscAddressModel sourceAddress)
    {
        this.sourceAddress = sourceAddress;
    }

    public SmscAddressModel getSourceAddress()
    {
        return sourceAddress;
    }

    public void setDestAddress(SmscAddressModel destAddress)
    {
        this.destAddress = destAddress;
    }

    public SmscAddressModel getDestAddress()
    {
        return destAddress;
    }

    @Override
    public String toString()
    {
        return "SmscDeliverSmModel{" +
                "shortMessageLength=" + shortMessageLength +
                ", shortMessage=" + Arrays.toString(shortMessage) +
                ", replaceIfPresent=" + replaceIfPresent +
                ", dataCoding=" + dataCoding +
                ", defaultMsgId=" + defaultMsgId +
                ", registeredDelivery=" + registeredDelivery +
                ", validityPeriod='" + validityPeriod + '\'' +
                ", scheduleDeliveryTime='" + scheduleDeliveryTime + '\'' +
                ", priority=" + priority +
                ", esmClass=" + esmClass +
                ", protocolId=" + protocolId +
                ", serviceType='" + serviceType + '\'' +
                ", sourceAddress=" + sourceAddress +
                ", destAddress=" + destAddress +
                "} " + super.toString();
    }
}
