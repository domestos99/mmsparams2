package cz.uhk.dip.mmsparams.api.websocket.model.smsc;

import java.io.Serializable;
import java.util.ArrayList;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class SmscModelBase extends WebSocketModelBase implements Serializable
{
    private String name;
    private int commandId;
    private int commandLength;
    private int commandStatus;
    private boolean isRequest;
    private boolean isResponse;
    private int sequenceNumber;
    private ArrayList<TlvModel> optionalParameters;

    public void setCommandId(int commandId)
    {
        this.commandId = commandId;
    }

    public int getCommandId()
    {
        return commandId;
    }

    public void setCommandLength(int commandLength)
    {
        this.commandLength = commandLength;
    }

    public int getCommandLength()
    {
        return commandLength;
    }

    public void setCommandStatus(int commandStatus)
    {
        this.commandStatus = commandStatus;
    }

    public int getCommandStatus()
    {
        return commandStatus;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setSequenceNumber(int sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public int getSequenceNumber()
    {
        return sequenceNumber;
    }

    public boolean getRequest()
    {
        return isRequest;
    }

    public void setRequest(boolean request)
    {
        isRequest = request;
    }

    public boolean getResponse()
    {
        return isResponse;
    }

    public void setResponse(boolean response)
    {
        isResponse = response;
    }

    public ArrayList<TlvModel> getOptionalParameters()
    {
        return optionalParameters;
    }

    public void setOptionalParameters(ArrayList<TlvModel> optionalParameters)
    {
        this.optionalParameters = optionalParameters;
    }

    @Override
    public String toString()
    {
        return "SmscModelBase{" +
                "name='" + name + '\'' +
                ", commandId=" + commandId +
                ", commandLength=" + commandLength +
                ", commandStatus=" + commandStatus +
                ", isRequest=" + isRequest +
                ", isResponse=" + isResponse +
                ", sequenceNumber=" + sequenceNumber +
                ", optionalParameters=" + optionalParameters +
                "} " + super.toString();
    }
}
