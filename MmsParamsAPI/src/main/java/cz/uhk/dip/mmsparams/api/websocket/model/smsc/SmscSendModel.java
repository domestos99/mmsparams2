package cz.uhk.dip.mmsparams.api.websocket.model.smsc;

import java.io.Serializable;
import java.util.ArrayList;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class SmscSendModel extends WebSocketModelBase implements Serializable
{
    private SmscAddressModel senderAddress;
    private SmscAddressModel recipientAddress;
    private String shortMessage;
    private boolean deliveryReport;
    private ArrayList<TlvModel> optionalParameters;
    private boolean useUCS2;

    public SmscAddressModel getSenderAddress()
    {
        return senderAddress;
    }

    public void setSenderAddress(SmscAddressModel senderAddress)
    {
        this.senderAddress = senderAddress;
    }

    public SmscAddressModel getRecipientAddress()
    {
        return recipientAddress;
    }

    public void setRecipientAddress(SmscAddressModel recipientAddress)
    {
        this.recipientAddress = recipientAddress;
    }

    public String getShortMessage()
    {
        return shortMessage;
    }

    public void setShortMessage(String shortMessage)
    {
        this.shortMessage = shortMessage;
    }

    public boolean isDeliveryReport()
    {
        return deliveryReport;
    }

    public void setDeliveryReport(boolean deliveryReport)
    {
        this.deliveryReport = deliveryReport;
    }

    public boolean getDeliveryReport()
    {
        return deliveryReport;
    }

    public ArrayList<TlvModel> getOptionalParameters()
    {
        return optionalParameters;
    }

    public void setOptionalParameters(ArrayList<TlvModel> optionalParameters)
    {
        this.optionalParameters = optionalParameters;
    }

    public void addOptionalParameters(TlvModel optionalParameter)
    {
        if (this.optionalParameters == null)
            this.optionalParameters = new ArrayList<>();
        this.optionalParameters.add(optionalParameter);
    }

    public boolean isUseUCS2()
    {
        return useUCS2;
    }

    public void setUseUCS2(boolean useUCS2)
    {
        this.useUCS2 = useUCS2;
    }

    @Override
    public String toString()
    {
        return "SmscSendModel{" +
                "senderAddress=" + senderAddress +
                ", recipientAddress=" + recipientAddress +
                ", shortMessage='" + shortMessage + '\'' +
                ", deliveryReport=" + deliveryReport +
                ", optionalParameters=" + optionalParameters +
                ", useUCS2=" + useUCS2 +
                "} " + super.toString();
    }
}
