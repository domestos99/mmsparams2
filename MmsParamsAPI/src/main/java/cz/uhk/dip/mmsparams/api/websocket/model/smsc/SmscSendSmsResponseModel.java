package cz.uhk.dip.mmsparams.api.websocket.model.smsc;

import java.io.Serializable;

public class SmscSendSmsResponseModel extends SmscModelBase implements Serializable
{
    private String messageId;
    private String resultMessage;

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setResultMessage(String resultMessage)
    {
        this.resultMessage = resultMessage;
    }

    public String getResultMessage()
    {
        return resultMessage;
    }

    @Override
    public String toString()
    {
        return "SmscSendSmsResponseModel{" +
                "messageId='" + messageId + '\'' +
                ", resultMessage='" + resultMessage + '\'' +
                "} " + super.toString();
    }
}
