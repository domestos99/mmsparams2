package cz.uhk.dip.mmsparams.api.websocket.model.smsc;

import java.io.Serializable;

public class SmscSessionId implements Serializable
{
    private String smscSessionId;

    public SmscSessionId()
    {
    }

    public SmscSessionId(String smscSessionId)
    {
        this.smscSessionId = smscSessionId;
    }

    public String getSmscSessionId()
    {
        return smscSessionId;
    }

    public void setSmscSessionId(String smscSessionId)
    {
        this.smscSessionId = smscSessionId;
    }

    @Override
    public String toString()
    {
        return "SmscSessionId{" +
                "smscSessionId='" + smscSessionId + '\'' +
                '}';
    }
}
