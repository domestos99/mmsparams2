package cz.uhk.dip.mmsparams.api.websocket.model.smsc;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class SmscSessionModel extends WebSocketModelBase implements Serializable
{
    private long boundTime;
    private byte interfaceVersion;
    private String localType;
    private String remoteType;
    private boolean useSsl;
    private boolean countersEnabled;
    private long writeTimeout;
    private long windowWaitTimeout;
    private int windowSize;
    private long windowMonitorInterval;
    private String type;
    private String systemType;
    private String systemId;
    private long requestExpiryTimeout;
    private int port;
    private String password;
    private String name;
    private String host;
    private long connectTimeout;
    private long bindTimeout;
    private String addressRange;


    public long getBoundTime()
    {
        return boundTime;
    }

    public void setBoundTime(long boundTime)
    {
        this.boundTime = boundTime;
    }

    public byte getInterfaceVersion()
    {
        return interfaceVersion;
    }

    public void setInterfaceVersion(byte interfaceVersion)
    {
        this.interfaceVersion = interfaceVersion;
    }

    public String getLocalType()
    {
        return localType;
    }

    public void setLocalType(String localType)
    {
        this.localType = localType;
    }

    public String getRemoteType()
    {
        return remoteType;
    }

    public void setRemoteType(String remoteType)
    {
        this.remoteType = remoteType;
    }

    public boolean isUseSsl()
    {
        return useSsl;
    }

    public void setUseSsl(boolean useSsl)
    {
        this.useSsl = useSsl;
    }

    public boolean isCountersEnabled()
    {
        return countersEnabled;
    }

    public void setCountersEnabled(boolean countersEnabled)
    {
        this.countersEnabled = countersEnabled;
    }

    public long getWriteTimeout()
    {
        return writeTimeout;
    }

    public void setWriteTimeout(long writeTimeout)
    {
        this.writeTimeout = writeTimeout;
    }

    public long getWindowWaitTimeout()
    {
        return windowWaitTimeout;
    }

    public void setWindowWaitTimeout(long windowWaitTimeout)
    {
        this.windowWaitTimeout = windowWaitTimeout;
    }

    public int getWindowSize()
    {
        return windowSize;
    }

    public void setWindowSize(int windowSize)
    {
        this.windowSize = windowSize;
    }

    public long getWindowMonitorInterval()
    {
        return windowMonitorInterval;
    }

    public void setWindowMonitorInterval(long windowMonitorInterval)
    {
        this.windowMonitorInterval = windowMonitorInterval;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getSystemType()
    {
        return systemType;
    }

    public void setSystemType(String systemType)
    {
        this.systemType = systemType;
    }

    public String getSystemId()
    {
        return systemId;
    }

    public void setSystemId(String systemId)
    {
        this.systemId = systemId;
    }

    public long getRequestExpiryTimeout()
    {
        return requestExpiryTimeout;
    }

    public void setRequestExpiryTimeout(long requestExpiryTimeout)
    {
        this.requestExpiryTimeout = requestExpiryTimeout;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public long getConnectTimeout()
    {
        return connectTimeout;
    }

    public void setConnectTimeout(long connectTimeout)
    {
        this.connectTimeout = connectTimeout;
    }

    public long getBindTimeout()
    {
        return bindTimeout;
    }

    public void setBindTimeout(long bindTimeout)
    {
        this.bindTimeout = bindTimeout;
    }

    public String getAddressRange()
    {
        return addressRange;
    }

    public void setAddressRange(String addressRange)
    {
        this.addressRange = addressRange;
    }

    @Override
    public String toString()
    {
        return "SmscSessionModel{" +
                "boundTime=" + boundTime +
                ", interfaceVersion=" + interfaceVersion +
                ", localType='" + localType + '\'' +
                ", remoteType='" + remoteType + '\'' +
                ", useSsl=" + useSsl +
                ", countersEnabled=" + countersEnabled +
                ", writeTimeout=" + writeTimeout +
                ", windowWaitTimeout=" + windowWaitTimeout +
                ", windowSize=" + windowSize +
                ", windowMonitorInterval=" + windowMonitorInterval +
                ", type='" + type + '\'' +
                ", systemType='" + systemType + '\'' +
                ", systemId='" + systemId + '\'' +
                ", requestExpiryTimeout=" + requestExpiryTimeout +
                ", port=" + port +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", connectTimeout=" + connectTimeout +
                ", bindTimeout=" + bindTimeout +
                ", addressRange='" + addressRange + '\'' +
                "} " + super.toString();
    }
}
