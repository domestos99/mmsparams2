package cz.uhk.dip.mmsparams.api.websocket.model.smsc;

import java.io.Serializable;
import java.util.Arrays;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;

public class TlvModel extends WebSocketModelBase implements Serializable
{
    private short tag;
    private byte[] value;     // length is stored in array
    private String tagName;      // short description of this tag
    private String valueString;

    public short getTag()
    {
        return tag;
    }

    public void setTag(short tag)
    {
        this.tag = tag;
    }

    public byte[] getValue()
    {
        return value;
    }

    public void setValue(byte[] value)
    {
        this.value = value;
    }

    public String getTagName()
    {
        return tagName;
    }

    public void setTagName(String tagName)
    {
        this.tagName = tagName;
    }

    public String getValueString()
    {
        return valueString;
    }

    public void setValueString(String valueString)
    {
        this.valueString = valueString;
    }

    @Override
    public String toString()
    {
        return "TlvModel{" +
                "tag=" + tag +
                ", value=" + Arrays.toString(value) +
                ", tagName='" + tagName + '\'' +
                ", valueString='" + valueString + '\'' +
                "} " + super.toString();
    }
}
