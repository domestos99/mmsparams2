package cz.uhk.dip.mmsparams.api.websocket.router;

import cz.uhk.dip.mmsparams.api.json.JsonUtils;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.logging.APILoggerFactory;
import cz.uhk.dip.mmsparams.api.logging.ApiLogFacade;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.listeners.IMessageReceiveSub;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.KeepAliveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.email.EmailReceiveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.TestErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsRecipientPhoneProfileRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.AcknowledgeIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.DeliveryIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotifyRespIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadOrigIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadRecIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendReqResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReportReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ReadReplyReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7SubmitResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneAllPartsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDisconnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;

public class MessageRouter
{
    private static final ILogger LOGGER = APILoggerFactory.getLogger(MessageRouter.class);

    public static <T> T process(IMessageReceiveSub<T> iMessageReceiveSub, String message)
    {
        EmptyMessage emptyMessage = JsonUtilsSafe.fromJson(message, EmptyMessage.class);

        if (emptyMessage == null || !MessageUtils.isMessageValid(emptyMessage))
        {
            ApiLogFacade.logWarning(LOGGER, "process: unknown or invalid message: " + message);
            return iMessageReceiveSub.onReceiveUnknown(message);
        }

        String key = emptyMessage.getMessageKey();

        if (MessageType.Register_Phone_Message.equals(key))
        {
            RegisterPhoneMessage msg = JsonUtilsSafe.fromJson(message, RegisterPhoneMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Phone_List_Request_Message.equals(key))
        {
            PhoneListRequestMessage msg = JsonUtilsSafe.fromJson(message, PhoneListRequestMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Phone_List_Response_Message.equals(key))
        {
            PhoneListResponseMessage msg = JsonUtilsSafe.fromJson(message, PhoneListResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Sms_SendPhone_Request_Message.equals(key))
        {
            SmsSendPhoneRequestMessage msg = JsonUtilsSafe.fromJson(message, SmsSendPhoneRequestMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Sms_ReceivePhone_Message.equals(key))
        {
            SmsReceivePhoneMessage msg = JsonUtilsSafe.fromJson(message, SmsReceivePhoneMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Sms_ReceivePhone_AllParts_Message.equals(key))
        {
            SmsReceivePhoneAllPartsMessage msg = JsonUtils.fromJson(message, SmsReceivePhoneAllPartsMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Sms_SendPhone_Response_Message.equals(key))
        {
            SmsSendPhoneResponseMessage msg = JsonUtilsSafe.fromJson(message, SmsSendPhoneResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Sms_Send_Delivery_Response_Message.equals(key))
        {
            SmsSendDeliveryReportMessage msg = JsonUtilsSafe.fromJson(message, SmsSendDeliveryReportMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Mms_SendPhone_Request_Message.equals(key))
        {
            MmsSendPhoneRequestMessage msg = JsonUtilsSafe.fromJson(message, MmsSendPhoneRequestMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.SendConf_Response_Message.equals(key))
        {
            SendConfResponseMessage msg = JsonUtilsSafe.fromJson(message, SendConfResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Dev_Message.equals(key))
        {
            DevMessage msg = JsonUtilsSafe.fromJson(message, DevMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Generic_Boolean_Response_Message.equals(key))
        {
            GenericBooleanResponseMessage msg = JsonUtilsSafe.fromJson(message, GenericBooleanResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.MmsRecipient_Phone_Profile_Request_Message.equals(key))
        {
            MmsRecipientPhoneProfileRequestMessage msg = JsonUtilsSafe.fromJson(message, MmsRecipientPhoneProfileRequestMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Generic_Error_Response_Message.equals(key))
        {
            GenericErrorResponseMessage msg = JsonUtilsSafe.fromJson(message, GenericErrorResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }

        else if (MessageType.NotificationInd_Response_Message.equals(key))
        {
            NotificationIndResponseMessage msg = JsonUtilsSafe.fromJson(message, NotificationIndResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.DeliveryInd_Response_Message.equals(key))
        {
            DeliveryIndResponseMessage msg = JsonUtilsSafe.fromJson(message, DeliveryIndResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.ReadOrigInd_Response_Message.equals(key))
        {
            ReadOrigIndResponseMessage msg = JsonUtilsSafe.fromJson(message, ReadOrigIndResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.AcknowledgeInd_Response_Message.equals(key))
        {
            AcknowledgeIndResponseMessage msg = JsonUtilsSafe.fromJson(message, AcknowledgeIndResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.RetrieveConf_Response_Message.equals(key))
        {
            RetrieveConfResponseMessage msg = JsonUtilsSafe.fromJson(message, RetrieveConfResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.ReadRecInd_Response_Message.equals(key))
        {
            ReadRecIndResponseMessage msg = JsonUtilsSafe.fromJson(message, ReadRecIndResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.NotifyRespInd_Response_Message.equals(key))
        {
            NotifyRespIndResponseMessage msg = JsonUtilsSafe.fromJson(message, NotifyRespIndResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }


        else if (MessageType.MM7_DeliveryReq_Message.equals(key))
        {
            MM7DeliveryReqMessage msg = JsonUtilsSafe.fromJson(message, MM7DeliveryReqMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.MM7_Delivery_ReportReq_Message.equals(key))
        {
            MM7DeliveryReportReqMessage msg = JsonUtilsSafe.fromJson(message, MM7DeliveryReportReqMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.MM7_ReadReplyReq_Message.equals(key))
        {
            MM7ReadReplyReqMessage msg = JsonUtilsSafe.fromJson(message, MM7ReadReplyReqMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }

        else if (MessageType.Smsc_Connect_Message.equals(key))
        {
            SmscConnectMessage msg = JsonUtilsSafe.fromJson(message, SmscConnectMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Smsc_SendSms_Message.equals(key))
        {
            SmscSendSmsMessage msg = JsonUtilsSafe.fromJson(message, SmscSendSmsMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Smsc_DeliverSm_Message.equals(key))
        {
            SmscDeliverSmMessage msg = JsonUtilsSafe.fromJson(message, SmscDeliverSmMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Smsc_Disconnect_Message.equals(key))
        {
            SmscDisconnectMessage msg = JsonUtilsSafe.fromJson(message, SmscDisconnectMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }

        else if (MessageType.Smsc_Connect_Response_Message.equals(key))
        {
            SmscConnectResponseMessage msg = JsonUtilsSafe.fromJson(message, SmscConnectResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Smsc_SendSmsResponse_Message.equals(key))
        {
            SmscSendSmsResponseMessage msg = JsonUtilsSafe.fromJson(message, SmscSendSmsResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }

        else if (MessageType.UnLock_Phone_Message.equals(key))
        {
            UnLockPhoneMessage msg = JsonUtilsSafe.fromJson(message, UnLockPhoneMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }

        else if (MessageType.SendReq_Response_Message.equals(key))
        {
            SendReqResponseMessage msg = JsonUtilsSafe.fromJson(message, SendReqResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }

        else if (MessageType.Mmsc_Send_Message.equals(key))
        {
            MmscSendMessage msg = JsonUtilsSafe.fromJson(message, MmscSendMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }

        else if (MessageType.Lock_Phone_Message.equals(key))
        {
            LockPhoneMessage msg = JsonUtilsSafe.fromJson(message, LockPhoneMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }

        else if (MessageType.Locked_Phones_List_Response.equals(key))
        {
            LockedPhonesListResponseMessage msg = JsonUtilsSafe.fromJson(message, LockedPhonesListResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }

        else if (MessageType.Locked_Phones_List_Request.equals(key))
        {
            LockedPhonesListRequestMessage msg = JsonUtilsSafe.fromJson(message, LockedPhonesListRequestMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.MM7_Submit_Response_Message.equals(key))
        {
            MM7SubmitResponseMessage msg = JsonUtilsSafe.fromJson(message, MM7SubmitResponseMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Smsc_DeliveryReport_Message.equals(key))
        {
            SmscDeliveryReportMessage msg = JsonUtilsSafe.fromJson(message, SmscDeliveryReportMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Register_ClientLib_Message.equals(key))
        {
            RegisterClientLibMessage msg = JsonUtilsSafe.fromJson(message, RegisterClientLibMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Mmsc_AcquireRoute_Request_Message.equals(key))
        {
            MmscAcquireRouteRequestMessage msg = JsonUtilsSafe.fromJson(message, MmscAcquireRouteRequestMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Test_Validation_Message.equals(key))
        {
            TestValidationMessage msg = JsonUtilsSafe.fromJson(message, TestValidationMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Test_Error_Message.equals(key))
        {
            TestErrorMessage msg = JsonUtilsSafe.fromJson(message, TestErrorMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.MM7_Error_Message.equals(key))
        {
            MM7ErrorMessage msg = JsonUtilsSafe.fromJson(message, MM7ErrorMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Keep_Alive_Message.equals(key))
        {
            KeepAliveMessage msg = JsonUtilsSafe.fromJson(message, KeepAliveMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Test_Result_Message.equals(key))
        {
            TestResultMessage msg = JsonUtilsSafe.fromJson(message, TestResultMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }
        else if (MessageType.Email_Receive_Message.equals(key))
        {
            EmailReceiveMessage msg = JsonUtilsSafe.fromJson(message, EmailReceiveMessage.class);
            return iMessageReceiveSub.onReceive(msg);
        }

        else
        {
            return iMessageReceiveSub.onReceiveUnknown(message);
        }
    }
}
