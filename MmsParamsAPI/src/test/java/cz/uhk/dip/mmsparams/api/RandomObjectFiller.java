package cz.uhk.dip.mmsparams.api;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import cz.uhk.dip.mmsparams.api.exceptions.MmsParamsExceptionBase;
import cz.uhk.dip.mmsparams.api.exceptions.TestErrorException;
import cz.uhk.dip.mmsparams.api.utils.CommonUtils;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsAttachmentSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.PduPartModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Attachment;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.SubscriptionInfoModel;

public class RandomObjectFiller
{

    private Random random = new Random();

    public <T> T createAndFill(Class<T> clazz) throws Exception
    {
        try
        {
            System.out.println("Creating: " + clazz);
            T instance = clazz.newInstance();
            for (Field field : clazz.getDeclaredFields())
            {
                field.setAccessible(true);
                Object value = getRandomValueForField(field);
                field.set(instance, value);
            }
            return instance;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            System.out.println(clazz.getName());
            throw ex;
        }
    }

    private Object getRandomValueForField(Field field) throws Exception
    {
        Class<?> type = field.getType();

        // Note that we must handle the different types here! This is just an
        // example, so this list is not complete! Adapt this to your needs!
        if (type.isEnum())
        {
            Object[] enumValues = type.getEnumConstants();
            return enumValues[random.nextInt(enumValues.length)];
        }
        else if (type.equals(Integer.TYPE) || type.equals(Integer.class))
        {
            return random.nextInt();
        }
        else if (type.equals(Long.TYPE) || type.equals(Long.class))
        {
            return random.nextLong();
        }
        else if (type.equals(Double.TYPE) || type.equals(Double.class))
        {
            return random.nextDouble();
        }
        else if (type.equals(Float.TYPE) || type.equals(Float.class))
        {
            return random.nextFloat();
        }
        else if (type.equals(Date.class))
        {
            return new Date(random.nextLong());
        }
        else if (type.equals(String.class))
        {
            return UUID.randomUUID().toString();
        }
        else if (type.equals(String[].class))
        {
            String[] sf = new String[random.nextInt(10)];

            for (int i = 0; i < sf.length; i++)
            {
                sf[i] = UUID.randomUUID().toString();
            }
            return sf;
        }
        else if (type.equals(byte.class))
        {
            byte[] sf = new byte[1];
            random.nextBytes(sf);
            return sf[0];
        }
        else if (type.equals(byte[].class))
        {
            byte[] sf = new byte[random.nextInt(10)];
            random.nextBytes(sf);
            return sf;
        }
        else if (type.equals(short.class))
        {
            return (short) random.nextInt();
        }
        else if (type.equals(boolean.class))
        {
            return true;
        }
        else if (type.equals(Boolean.class))
        {
            return new Boolean(true);
        }
        else if (type.equals(BigInteger.class))
        {
            return BigInteger.valueOf(random.nextInt());
        }

        else if (type.equals(PduPartModel[].class))
        {
            PduPartModel[] l = new PduPartModel[random.nextInt(10)];

            for (int i = 0; i < l.length; i++)
            {
                l[i] = createAndFill(PduPartModel.class);
            }
            return l;
        }
        else if (type.equals(MmsAttachmentSendModel[].class))
        {
            MmsAttachmentSendModel[] l = new MmsAttachmentSendModel[random.nextInt(10)];

            for (int i = 0; i < l.length; i++)
            {
                l[i] = createAndFill(MmsAttachmentSendModel.class);
            }
            return l;
        }
        else if (type.equals(SubscriptionInfoModel[].class))
        {
            SubscriptionInfoModel[] l = new SubscriptionInfoModel[random.nextInt(10)];

            for (int i = 0; i < l.length; i++)
            {
                l[i] = createAndFill(SubscriptionInfoModel.class);
            }
            return l;
        }
        else if (type.equals(ArrayList.class))
        {
            return null;
        }
        else if (type.equals(MM7Address.class))
        {
            return createAndFill(MM7Address.class);
        }
        else if (type.equals(MM7Address[].class))
        {
            MM7Address[] l = new MM7Address[random.nextInt(10)];

            for (int i = 0; i < l.length; i++)
            {
                l[i] = createAndFill(MM7Address.class);
            }
            return l;
        }
        else if (type.equals(MM7Attachment[].class))
        {
            MM7Attachment[] l = new MM7Attachment[random.nextInt(10)];

            for (int i = 0; i < l.length; i++)
            {
                l[i] = createAndFill(MM7Attachment.class);
            }
            return l;
        }
        else if (type.equals(TestErrorType.class))
        {
            return TestErrorType.TEST_ERROR;
        }

        else if (type.equals(MmsParamsExceptionBase.class))
        {
            return new TestErrorException(CommonUtils.getUUID());
        }

        else if (type.equals(Exception.class))
        {
            return new Exception(UUID.randomUUID().toString());
        }


        return createAndFill(type);
    }
}
