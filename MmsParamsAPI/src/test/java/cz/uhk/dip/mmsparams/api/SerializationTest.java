package cz.uhk.dip.mmsparams.api;

import org.junit.Test;
import org.reflections.Reflections;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Set;

import cz.uhk.dip.mmsparams.api.exceptions.MmsParamsExceptionBase;
import cz.uhk.dip.mmsparams.api.json.JsonUtils;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketModelBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscMessageBase;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class SerializationTest
{
    @Test
    public void serializableMessagesTest()
    {
        Reflections reflections = new Reflections(EmptyMessage.class.getPackage().getName());

        Set<Class<? extends WebSocketMessageBase>> allClasses = reflections.getSubTypesOf(WebSocketMessageBase.class);
        for (Class<? extends WebSocketMessageBase> a : allClasses)
        {
            Class<?>[] b = a.getInterfaces();

            assertTrue(a.getSimpleName(), Arrays.asList(b).contains(Serializable.class));
        }
    }

    @Test
    public void serializeMessagesTest() throws Exception
    {
        RandomObjectFiller filler = new RandomObjectFiller();
        Reflections reflections = new Reflections("cz.uhk.dip.mmsparams.api.websocket.messages");

        Set<Class<? extends WebSocketMessageBase>> allClasses = reflections.getSubTypesOf(WebSocketMessageBase.class);
        for (Class<? extends WebSocketMessageBase> a : allClasses)
        {
            Class<WebSocketMessageBase> clazz = (Class<WebSocketMessageBase>) Class.forName(a.getName());

            if (SmscMessageBase.class.getSimpleName().equals(clazz.getSimpleName()))
                continue;

            if (EmptyMessage.class.getSimpleName().equals(clazz.getSimpleName()))
                continue;

            if (MmsParamsExceptionBase.class.getSimpleName().equals(clazz.getSimpleName()))
                continue;

            System.out.println(clazz);

            WebSocketMessageBase object =
                    new MessageBuilder()
                            .withNoTestId()
                            .withSenderKey("xxx")
                            .withRecipientKey("yyy")
                            .withMessageIdRandom()
                            .build(clazz);

            String json = JsonUtils.toJson(object);

            assertNotNull(json);
            assertNotEquals("", json);


            object = filler.createAndFill(a);
            json = JsonUtils.toJson(object);

            assertNotNull(json);
            assertNotEquals("", json);


            String msgString = object.toString();

            assertTrue(msgString.length() > (object.getClass().getName().length() + 15));

        }
    }

    @Test
    public void messageTypesTest() throws Exception
    {
        RandomObjectFiller filler = new RandomObjectFiller();
        Reflections reflections = new Reflections("cz.uhk.dip.mmsparams.api.websocket.messages");

        Set<Class<? extends WebSocketMessageBase>> allClasses = reflections.getSubTypesOf(WebSocketMessageBase.class);
        for (Class<? extends WebSocketMessageBase> a : allClasses)
        {
            Class<WebSocketMessageBase> clazz = (Class<WebSocketMessageBase>) Class.forName(a.getName());

            if (clazz.getSimpleName().equals(SmscMessageBase.class.getSimpleName()))
                continue;

            String type = MessageType.getKeyByClass(clazz);

            if (type == null)
            {
                System.out.println("Type: " + type);
                System.out.println(clazz);
            }

            assertNotNull(type);
            assertNotEquals("", type);
        }
    }


    @Test
    public void serializableModelsTest()
    {
        Reflections reflections = new Reflections("cz.uhk.dip.mmsparams.api.websocket.model");

        Set<Class<? extends WebSocketModelBase>> allClasses = reflections.getSubTypesOf(WebSocketModelBase.class);
        for (Class<? extends WebSocketModelBase> a : allClasses)
        {
            Class<?>[] b = a.getInterfaces();

            assertTrue(a.getSimpleName(), Arrays.asList(b).contains(Serializable.class));
        }
    }

    @Test
    public void serializeModelTest() throws Exception
    {
        RandomObjectFiller filler = new RandomObjectFiller();
        Reflections reflections = new Reflections("cz.uhk.dip.mmsparams.api.websocket.model");

        Set<Class<? extends WebSocketModelBase>> allClasses = reflections.getSubTypesOf(WebSocketModelBase.class);
        for (Class<? extends WebSocketModelBase> a : allClasses)
        {
            Class<WebSocketModelBase> clazz = (Class<WebSocketModelBase>) Class.forName(a.getName());
            Constructor<WebSocketModelBase> ctor = clazz.getConstructor();
            WebSocketModelBase object = ctor.newInstance();

            String json = JsonUtils.toJson(object);

            assertNotNull(json);
            assertNotEquals("", json);


            object = filler.createAndFill(a);
            json = JsonUtils.toJson(object);

            assertNotNull(json);
            assertNotEquals("", json);

        }
    }

    private final String clzPackage = "cz.uhk.dip.mmsparams.api";

    @Test
    public void serializableTest() throws IllegalAccessException, InstantiationException
    {
        Class clazz = Serializable.class;
        Reflections reflections = new Reflections(clzPackage);
        Set<Class> subTypes = reflections.getSubTypesOf(clazz);

        for (Class c : subTypes)
        {
            if (c.isInterface())
                continue;

            if (Modifier.isAbstract(c.getModifiers()))
                continue;

            if (c.isEnum())
            {
                continue;
            }

            if (!c.getPackage().getName().startsWith(clzPackage))
                continue;

            try
            {
                System.out.println(c.getName());

                Object o = c.newInstance();
                String json = JsonUtils.toJson(o);
                Object o2 = JsonUtils.fromJson(json, c);

            }
            catch (Exception e)
            {
                System.out.println(c.getName());
                e.printStackTrace();
                fail();
            }
        }
    }


}
