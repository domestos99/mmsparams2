package cz.uhk.dip.mmsparams.api;

import java.util.Objects;

public class TestObject
{
    public TestObject(int a, String b, boolean c, Object d)
    {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    private int a;
    private String b;
    private boolean c;
    private Object d;

    public int getA()
    {
        return a;
    }

    public void setA(int a)
    {
        this.a = a;
    }

    public String getB()
    {
        return b;
    }

    public void setB(String b)
    {
        this.b = b;
    }

    public boolean isC()
    {
        return c;
    }

    public void setC(boolean c)
    {
        this.c = c;
    }

    public Object getD()
    {
        return d;
    }

    public void setD(Object d)
    {
        this.d = d;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestObject that = (TestObject) o;
        return a == that.a &&
                c == that.c &&
                Objects.equals(b, that.b) &&
                Objects.equals(d, that.d);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(a, b, c, d);
    }
}
