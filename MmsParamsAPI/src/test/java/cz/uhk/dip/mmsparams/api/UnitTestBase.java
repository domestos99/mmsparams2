package cz.uhk.dip.mmsparams.api;


import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;

public class UnitTestBase
{

    protected DevMessage getTestMessage()
    {
        return new MessageBuilder()
                .withSenderKey("sender")
                .withMessageIdRandom()
                .withTestId(new ITestIdProvider()
                {
                    @Override
                    public String getTestId()
                    {
                        return "test-id";
                    }
                })
                .build(DevMessage.class);
    }
}
