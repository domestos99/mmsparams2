package cz.uhk.dip.mmsparams.api.connections;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.utils.IBase64Converter;
import cz.uhk.dip.mmsparams.api.utils.JavaBase64Converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AndroidConnectBuilderTest
{
    private String addr = "192.168.0.101:4301";
    private String name = "samsung_phone";
    private String username = "adminusername";
    private String password = "mysecretpassword";
    private String expectedResult = "MMSPARAMS_CONNECT_WS;192.168.0.101:4301;true;samsung_phone;YWRtaW51c2VybmFtZTpteXNlY3JldHBhc3N3b3Jk";


    private IBase64Converter getBase64Converter()
    {
        return new JavaBase64Converter();
    }

    @Test
    public void createConnectMessageTest()
    {
        JwtRequest jwtRequest = new JwtRequest(username, password);
        String result = AndroidConnectBuilder.createConnectMessage(getBase64Converter(), addr, true, name, jwtRequest);
        assertEquals(expectedResult, result);
    }

    @Test
    public void parseTest()
    {
        AndroidConnectModel model = AndroidConnectBuilder.parse(getBase64Converter(),expectedResult);
        assertNotNull(model);
        assertEquals(addr, model.getAddress());
        assertEquals(name, model.getName());
        assertEquals(username, model.getJwtRequest().getUsername());
        assertEquals(password, model.getJwtRequest().getPassword());
    }


}
