package cz.uhk.dip.mmsparams.api.constants;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;

import static org.junit.Assert.assertEquals;

public class HttpConstantsTest
{
    @Test
    public void getAuthHeaderValueTest()
    {
        String result = HttpConstants.getAuthHeaderValue("xxx");
        assertEquals("Bearer xxx", result);
    }

    @Test
    public void getHttpPrefixTest()
    {
        assertEquals(HttpConstants.URL_PREFIX_HTTP, HttpConstants.getHttpPrefix(null));
        assertEquals(HttpConstants.URL_PREFIX_HTTP, HttpConstants.getHttpPrefix(HttpProtocolEnum.HTTP));
        assertEquals(HttpConstants.URL_PREFIX_HTTPS, HttpConstants.getHttpPrefix(HttpProtocolEnum.HTTPS));
    }

    @Test
    public void getWsPrefixTest()
    {
        assertEquals(HttpConstants.WS_PREFIX, HttpConstants.getWsPrefix(null));
        assertEquals(HttpConstants.WS_PREFIX, HttpConstants.getWsPrefix(HttpProtocolEnum.HTTP));
        assertEquals(HttpConstants.WSS_PREFIX, HttpConstants.getWsPrefix(HttpProtocolEnum.HTTPS));
    }
}
