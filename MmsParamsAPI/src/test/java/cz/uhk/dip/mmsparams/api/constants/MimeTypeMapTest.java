package cz.uhk.dip.mmsparams.api.constants;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MimeTypeMapTest
{
    @Test
    public void getMimeType_txt_Test()
    {
        String res1 = MimeTypeMap.getMimeType(".txt");
        assertEquals("text/plain", res1);

        String res2 = MimeTypeMap.getMimeType("txt");
        assertEquals("text/plain", res2);

        String res3 = MimeTypeMap.getMimeType("*.txt");
        assertEquals("text/plain", res3);
    }
}
