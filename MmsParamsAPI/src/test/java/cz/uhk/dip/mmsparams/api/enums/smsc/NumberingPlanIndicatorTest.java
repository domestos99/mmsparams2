package cz.uhk.dip.mmsparams.api.enums.smsc;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.smsc.SMSCConstant;

import static org.junit.Assert.assertEquals;

public class NumberingPlanIndicatorTest
{
    @Test
    public void valueTest()
    {
        byte value = NumberingPlanIndicator.NPI_NATIONAL.value();
        assertEquals(SMSCConstant.NPI_NATIONAL, value);
    }

    @Test
    public void valueOfTest()
    {
        NumberingPlanIndicator val = NumberingPlanIndicator.valueOf(SMSCConstant.NPI_NATIONAL);
        assertEquals(NumberingPlanIndicator.NPI_NATIONAL, val);

        val = NumberingPlanIndicator.valueOf((byte) 123);
        assertEquals(NumberingPlanIndicator.NPI_UNKNOWN, val);
    }
}
