package cz.uhk.dip.mmsparams.api.enums.smsc;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.smsc.SMSCConstant;

import static org.junit.Assert.assertEquals;

public class TypeOfNumberTest
{
    @Test
    public void valueTest()
    {
        byte value = TypeOfNumber.TON_INTERNATIONAL.value();
        assertEquals(SMSCConstant.TON_INTERNATIONAL, value);
    }

    @Test
    public void valueOfTest()
    {
        TypeOfNumber val = TypeOfNumber.valueOf(SMSCConstant.TON_INTERNATIONAL);
        assertEquals(TypeOfNumber.TON_INTERNATIONAL, val);

        val = TypeOfNumber.valueOf((byte) 123);
        assertEquals(TypeOfNumber.TON_UNKNOWN, val);
    }
}
