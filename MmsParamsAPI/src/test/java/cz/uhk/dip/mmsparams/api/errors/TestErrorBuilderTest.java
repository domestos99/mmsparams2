package cz.uhk.dip.mmsparams.api.errors;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.exceptions.TimeOutException;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestErrorBuilderTest
{

    @Test
    public void buildTestA()
    {
        TimeOutException ex = new TimeOutException();
        TestErrorModel result = TestErrorBuilder.create()
                .withMessage(GenericConstants.MESSAGE)
                .withErrorType(TestErrorType.TIMEOUT)
                .withException(ex)
                .build();

        assertNotNull(result);
        assertEquals(GenericConstants.MESSAGE, result.getMessage());
        assertEquals(ex, result.getException());
        assertEquals(TestErrorType.TIMEOUT, result.getTestErrorType());
    }

    @Test
    public void buildTestB()
    {
        TimeOutException ex = new TimeOutException("xxx");
        TestErrorModel result = TestErrorBuilder.create()
                .withAll(ex)
                .build();

        assertNotNull(result);
        assertEquals("xxx", result.getMessage());
        assertEquals(ex, result.getException());
        assertEquals(TestErrorType.TIMEOUT, result.getTestErrorType());
    }

}