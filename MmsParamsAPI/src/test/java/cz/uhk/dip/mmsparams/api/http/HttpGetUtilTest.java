package cz.uhk.dip.mmsparams.api.http;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.utils.StringUtil;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class HttpGetUtilTest
{
    @Test
    public void getDataFromUrlTestA()
    {
        TaskResult<String> result = HttpGetUtil.getDataFromUrl("http://google.com");
        assertNotNull(result);
        assertFalse(result.hasError());
        assertNull(result.getError());
        assertTrue(result.hasResult());
        assertFalse(StringUtil.isEmptyOrNull(result.getResult()));
    }

    @Test
    public void getDataFromUrlTestB()
    {
        TaskResult<String> result = HttpGetUtil.getDataFromUrl("http://google1213515353.com");
        assertNotNull(result);
        assertTrue(result.hasError());
        assertNotNull(result.getError());
        assertFalse(result.hasResult());
        assertTrue(StringUtil.isEmptyOrNull(result.getResult()));
    }
}