package cz.uhk.dip.mmsparams.api.http;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;

import static org.junit.Assert.assertEquals;

public class ServerAddressProviderTest
{
    ServerAddressProvider providerA;
    ServerAddressProvider providerB;

    @Before
    public void setUp() throws Exception
    {
        providerA = new ServerAddressProvider(HttpProtocolEnum.HTTP, "192.168.1.101", "4301");
        providerB = new ServerAddressProvider(HttpProtocolEnum.HTTPS, "google.com");
    }

    @Test
    public void getServerAddressTest()
    {
        String resultA = providerA.getServerAddress();
        assertEquals("http://192.168.1.101:4301", resultA);
        String resultB = providerB.getServerAddress();
        assertEquals("https://google.com", resultB);
    }

    @Test
    public void testGetServerAddressTest()
    {
        String resultA = providerA.getServerAddress(false);
        assertEquals("192.168.1.101:4301", resultA);
        String resultB = providerB.getServerAddress(false);
        assertEquals("google.com", resultB);
    }

    @Test
    public void getWSAddressTest()
    {
        String resultA = providerA.getWSAddress();
        assertEquals("ws://192.168.1.101:4301/appsocket", resultA);
        String resultB = providerB.getWSAddress();
        assertEquals("wss://google.com/appsocket", resultB);
    }

    @Test
    public void getHttpProtocolEnumTest()
    {
        HttpProtocolEnum resultA = providerA.getHttpProtocolEnum();
        assertEquals(HttpProtocolEnum.HTTP, resultA);
        HttpProtocolEnum resultB = providerB.getHttpProtocolEnum();
        assertEquals(HttpProtocolEnum.HTTPS, resultB);
    }

    @Test
    public void getHttpPrefixTest()
    {
        String resultA = providerA.getHttpPrefix();
        assertEquals(HttpConstants.URL_PREFIX_HTTP, resultA);
        String resultB = providerB.getHttpPrefix();
        assertEquals(HttpConstants.URL_PREFIX_HTTPS, resultB);
    }
}