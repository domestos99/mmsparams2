package cz.uhk.dip.mmsparams.api.lists;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.TestObject;
import cz.uhk.dip.mmsparams.api.TestObjectDTO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class HistorizedListTest
{
    private HistorizedList<TestObject, TestObjectDTO> historizedObject;
    private TestObject testObject;

    @Before
    public void setUp() throws Exception
    {
        this.testObject = new TestObject(1, "1", true, "123");
        historizedObject = new HistorizedList<TestObject, TestObjectDTO>(obj -> {
            return new TestObjectDTO(obj.getA(), obj.getB(), obj.isC(), obj.getD());
        });
    }

    @Test
    public void getAllTest()
    {
        List<TestObject> allObj = historizedObject.getAll();
        assertNotNull(allObj);
        assertEquals(0, allObj.size());

        this.historizedObject.add(this.testObject);

        allObj = historizedObject.getAll();
        assertNotNull(allObj);
        assertEquals(1, allObj.size());

        this.historizedObject.add(this.testObject);

        allObj = historizedObject.getAll();
        assertNotNull(allObj);
        assertEquals(2, allObj.size());
    }

    @Test
    public void sizeTest()
    {
        assertEquals(0, historizedObject.size());
        this.historizedObject.add(this.testObject);
        assertEquals(1, historizedObject.size());
        this.historizedObject.add(this.testObject);
        assertEquals(2, historizedObject.size());
    }

    @Test
    public void removeTest()
    {
        this.historizedObject.add(this.testObject);
        this.historizedObject.remove(0);

        assertEquals(0, historizedObject.getAll().size());
        assertEquals(1, historizedObject.getHistorizedAll().size());
    }

    @Test
    public void iteratorTest()
    {
        this.historizedObject.add(this.testObject);
        this.historizedObject.add(this.testObject);

        int count = 0;

        for (TestObject a : this.historizedObject)
        {
            assertNotNull(a);
            assertEquals(this.testObject, a);
            count++;
        }
        assertEquals(2, count);
    }

    @Test
    public void getTest()
    {
        this.historizedObject.add(this.testObject);
        TestObject a = this.historizedObject.get(0);
        assertEquals(this.testObject, a);
    }

    @Test
    public void addAllTest()
    {
        List<TestObject> list = new ArrayList<>();
        list.add(this.testObject);
        list.add(this.testObject);

        this.historizedObject.addAll(list);

        assertEquals(2, this.historizedObject.getAll().size());
        assertEquals(2, this.historizedObject.getHistorizedAll().size());
    }

    @Test
    public void getHistorizedAllTest()
    {
        List<TestObjectDTO> all = this.historizedObject.getHistorizedAll();
        assertNotNull(all);
        assertEquals(0, all.size());

        this.historizedObject.add(this.testObject);
        all = this.historizedObject.getHistorizedAll();
        assertNotNull(all);
        assertEquals(1, all.size());
        assertEquals(this.testObject.getA(), all.get(0).getA());
        assertEquals(this.testObject.getB(), all.get(0).getB());
        assertEquals(this.testObject.isC(), all.get(0).isC());
        assertEquals(this.testObject.getD(), all.get(0).getD());

    }

    @Test
    public void clearTest()
    {
        this.historizedObject.add(this.testObject);
        assertEquals(1, historizedObject.getAll().size());
        assertEquals(1, historizedObject.getHistorizedAll().size());

        this.historizedObject.clear();

        assertEquals(0, historizedObject.getAll().size());
        assertEquals(1, historizedObject.getHistorizedAll().size());


        this.historizedObject.add(this.testObject);
        assertEquals(1, historizedObject.getAll().size());
        assertEquals(2, historizedObject.getHistorizedAll().size());

        this.historizedObject.clear();

        assertEquals(0, historizedObject.getAll().size());
        assertEquals(2, historizedObject.getHistorizedAll().size());
    }


}
