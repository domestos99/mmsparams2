package cz.uhk.dip.mmsparams.api.lists;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.TestObject;
import cz.uhk.dip.mmsparams.api.TestObjectDTO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class HistorizedObjectTest
{
    private HistorizedObject<TestObject, TestObjectDTO> historizedObject;
    private TestObject testObject;

    @Before
    public void setUp() throws Exception
    {
        this.testObject = new TestObject(1, "1", true, "123");
        historizedObject = new HistorizedObject<>(testObject, obj -> {
            return new TestObjectDTO(obj.getA(), obj.getB(), obj.isC(), obj.getD());
        });
    }

    @Test
    public void getObjectTest()
    {
        TestObject histObj = this.historizedObject.getObject();
        assertEquals(this.testObject, histObj);
    }

    @Test
    public void getHistorizedObjectTest()
    {
        TestObjectDTO dto = this.historizedObject.getHistorizedObject();
        assertNotNull(dto);
        assertEquals(this.testObject.getA(), dto.getA());
        assertEquals(this.testObject.getB(), dto.getB());
        assertEquals(this.testObject.isC(), dto.isC());
        assertEquals(this.testObject.getD(), dto.getD());
    }

    @Test
    public void clearTest()
    {
        this.historizedObject.clear();
        assertNull(this.historizedObject.getObject());
        assertNotNull(this.historizedObject.getHistorizedObject());
    }


}
