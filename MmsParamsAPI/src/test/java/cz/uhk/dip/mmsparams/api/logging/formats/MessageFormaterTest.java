package cz.uhk.dip.mmsparams.api.logging.formats;

import org.junit.Test;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.json.JsonUtils;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;

import static org.junit.Assert.assertFalse;

public class MessageFormaterTest
{

    @Test
    public void getPrettyMessageTest() throws IOException
    {
        EmptyMessage emptyMessage = new EmptyMessage();


        String str = MessageFormater.getPrettyMessage(JsonUtils.toJson(emptyMessage));

        assertFalse(StringUtil.isEmptyOrNull(str));

    }
}