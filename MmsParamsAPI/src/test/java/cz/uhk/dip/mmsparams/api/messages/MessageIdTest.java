package cz.uhk.dip.mmsparams.api.messages;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class MessageIdTest
{
    @Test
    public void getMessageIdNullTest()
    {
        MessageId id = new MessageId();
        assertNull(id.getMessageId());
    }

    @Test
    public void getMessageIdNotNullTest()
    {
        MessageId id = new MessageId("test");
        assertNotNull(id.getMessageId());
        assertEquals("test", id.getMessageId());
    }

    @Test
    public void getMessageIdNotNull2Test()
    {
        MessageId id = new MessageId();
        id.setMessageId("test");
        assertNotNull(id.getMessageId());
        assertEquals("test", id.getMessageId());
    }
}
