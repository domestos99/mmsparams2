package cz.uhk.dip.mmsparams.api.messages;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.enums.MessageDirection;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MessageRegisterItemTest
{
    private MessageRegisterItem messageRegisterItem;
    private long inserted;

    @Before
    public void setUp() throws Exception
    {
        DevMessage msg = new MessageBuilder().build(DevMessage.class);
        inserted = System.currentTimeMillis();
        Thread.sleep(100);
        this.messageRegisterItem = new MessageRegisterItem(msg, MessageDirection.IN);
    }

    @Test
    public void getOnlyMessagesTest()
    {
        List<MessageRegisterItem> items = new ArrayList<>();
        items.add(this.messageRegisterItem);
        items.add(this.messageRegisterItem);

        List<WebSocketMessageBase> msgs = MessageRegisterItem.getOnlyMessages(items);
        assertEquals(2, msgs.size());
        assertEquals(DevMessage.class, msgs.get(0).getClass());
        assertEquals(DevMessage.class, msgs.get(1).getClass());
    }

    @Test
    public void getInsertedDTTest() throws InterruptedException
    {
        Thread.sleep(1000);
        long insertedDT = this.messageRegisterItem.getInsertedDT();
        assertTrue(insertedDT < System.currentTimeMillis() && insertedDT > this.inserted);
    }

    @Test
    public void getMessageTest()
    {
        WebSocketMessageBase msg = this.messageRegisterItem.getMessage();
        assertNotNull(msg);
        assertEquals(DevMessage.class, msg.getClass());
    }
}
