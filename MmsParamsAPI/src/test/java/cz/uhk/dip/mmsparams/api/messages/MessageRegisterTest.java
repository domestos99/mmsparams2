package cz.uhk.dip.mmsparams.api.messages;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import cz.uhk.dip.mmsparams.api.UnitTestBase;
import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class MessageRegisterTest extends UnitTestBase
{
    private IMessageRegister messageRegister;

    @Before
    public void setUp() throws Exception
    {
        messageRegister = new MessageRegister();
    }

    @Test
    public void insertIncomingMsgTest()
    {
        DevMessage msg = getTestMessage();

        messageRegister.insertIncomingMsg(msg);
        List<MessageRegisterItem> messages = messageRegister.getAllIncoming();
        assertEquals(1, messages.size());

        messageRegister.insertIncomingMsg(msg);
        messages = messageRegister.getAllIncoming();
        assertEquals(2, messages.size());
    }

    @Test
    public void insertIncomingMsgNullTest()
    {
        messageRegister.insertIncomingMsg(null);
        List<MessageRegisterItem> messages = messageRegister.getAllIncoming();
        assertEquals(0, messages.size());
    }

    @Test
    public void insertOutgoingMsgTest()
    {
        DevMessage msg = getTestMessage();

        messageRegister.insertOutgoingMsg(msg);
        List<MessageRegisterItem> messages = messageRegister.getAllOutgoing();
        assertEquals(1, messages.size());


        messageRegister.insertOutgoingMsg(msg);
        messages = messageRegister.getAllOutgoing();
        assertEquals(2, messages.size());
    }

    @Test
    public void insertOutgoingMsgNullTest()
    {
        messageRegister.insertOutgoingMsg(null);
        List<MessageRegisterItem> messages = messageRegister.getAllOutgoing();
        assertEquals(0, messages.size());
    }


    @Test
    public void getIncomByMsgIdTest()
    {
        DevMessage msg = getTestMessage();

        messageRegister.insertIncomingMsg(msg);
        WebSocketMessageBase messages = messageRegister.getIncomByMsgId(msg.getMessageID(), msg.getMessageKey());
        assertNotNull(messages);
    }

    @Test
    public void getIncomByMsgIdNullTest()
    {
        DevMessage msg = getTestMessage();

        messageRegister.insertIncomingMsg(msg);
        WebSocketMessageBase messages = messageRegister.getIncomByMsgId((String) null, null);
        assertNull(messages);

        messages = messageRegister.getIncomByMsgId((String) msg.getMessageID(), null);
        assertNull(messages);

        // Wrong type
        messages = messageRegister.getIncomByMsgId((String) msg.getMessageID(), MessageType.Register_ClientLib_Message);
        assertNull(messages);
    }

    @Test
    public void getIncomByMsgId_empty_Test()
    {
        DevMessage msg = getTestMessage();
        WebSocketMessageBase messages = messageRegister.getIncomByMsgId(msg.getMessageID(), msg.getMessageKey());
        assertNull(messages);
    }

    @Test
    public void getIncomByClzTest()
    {
        DevMessage msg = getTestMessage();
        messageRegister.insertIncomingMsg(msg);
        List<MessageRegisterItem> messages = messageRegister.getIncomByClz(msg.getMessageKey());
        assertEquals(1, messages.size());


        messageRegister.insertIncomingMsg(msg);
        messages = messageRegister.getIncomByClz(msg.getMessageKey());
        assertEquals(2, messages.size());
    }

    @Test
    public void getIncomByClzNullTest()
    {
        DevMessage msg = getTestMessage();
        messageRegister.insertIncomingMsg(msg);
        List<MessageRegisterItem> messages = messageRegister.getIncomByClz(null);
        assertEquals(0, messages.size());
    }

    @Test
    public void getIncomByClz_empty_Test()
    {
        List<MessageRegisterItem> messages = messageRegister.getIncomByClz(MessageType.Dev_Message);
        assertEquals(0, messages.size());
    }

    @Test
    public void getIncomingCountTest()
    {
        DevMessage msg = getTestMessage();
        messageRegister.insertIncomingMsg(msg);
        int count = messageRegister.getIncomingCount(DevMessage.class);
        assertEquals(1, count);

        messageRegister.insertIncomingMsg(msg);
        count = messageRegister.getIncomingCount(DevMessage.class);
        assertEquals(2, count);
    }

    @Test
    public void getIncomingCountNullTest()
    {
        DevMessage msg = getTestMessage();
        messageRegister.insertIncomingMsg(msg);
        int count = messageRegister.getIncomingCount(null);
        assertEquals(0, count);
    }

    @Test
    public void getIncomingCount_empty_Test()
    {
        int count = messageRegister.getIncomingCount(DevMessage.class);
        assertEquals(0, count);
    }

    @Test
    public void getIncomByDeviceIdTest()
    {
        DevMessage msg = getTestMessage();
        messageRegister.insertIncomingMsg(msg);

        PhoneInfoModel pi = new PhoneInfoModel();
        pi.setPhoneKey(msg.getSenderKey());

        List<WebSocketMessageBase> message = messageRegister.getIncomByDeviceId(pi, msg.getMessageKey());
        assertEquals(1, message.size());


        messageRegister.insertIncomingMsg(msg);
        message = messageRegister.getIncomByDeviceId(pi, msg.getMessageKey());

        assertEquals(2, message.size());
    }

    @Test
    public void getIncomByDeviceIdNullTest()
    {
        DevMessage msg = getTestMessage();
        messageRegister.insertIncomingMsg(msg);

        PhoneInfoModel pi = new PhoneInfoModel();
        pi.setPhoneKey(msg.getSenderKey());

        List<WebSocketMessageBase> message = messageRegister.getIncomByDeviceId(null, null);
        assertEquals(0, message.size());

        message = messageRegister.getIncomByDeviceId(pi, null);
        assertEquals(0, message.size());

        message = messageRegister.getIncomByDeviceId(null, MessageType.Dev_Message);
        assertEquals(0, message.size());

        message = messageRegister.getIncomByDeviceId(pi, MessageType.Register_ClientLib_Message);
        assertEquals(0, message.size());
    }

    @Test
    public void getAllIncomingNull()
    {
        DevMessage msg = getTestMessage();
        messageRegister.insertIncomingMsg(msg);
        List<MessageRegisterItem> messages = messageRegister.getAllIncoming();
        assertEquals(1, messages.size());

        messageRegister.insertIncomingMsg(msg);
        messages = messageRegister.getAllIncoming();
        assertEquals(2, messages.size());
    }

    @Test
    public void getIncomBySmscSessionIdTest()
    {
        SmscSendSmsMessage msg = new MessageBuilder()
                .withMessageIdRandom()
                .build(SmscSendSmsMessage.class);

        SmscSessionId smscSessionId = new SmscSessionId(GenericConstants.SMSC_SESSION_ID);
        msg.setSmscSessionId(smscSessionId);

        messageRegister.insertIncomingMsg(msg);

        List<WebSocketMessageBase> messages = messageRegister.getIncomBySmscSessionId(smscSessionId, msg.getMessageKey());
        assertEquals(1, messages.size());
    }

    @Test
    public void getIncomBySmscSessionId_not_found_Test()
    {
        SmscSendSmsMessage msg = new MessageBuilder()
                .withMessageIdRandom()
                .build(SmscSendSmsMessage.class);

        SmscSessionId smscSessionId = new SmscSessionId(GenericConstants.SMSC_SESSION_ID);
        msg.setSmscSessionId(smscSessionId);
        messageRegister.insertIncomingMsg(msg);

        List<WebSocketMessageBase> messages = messageRegister.getIncomBySmscSessionId(smscSessionId, MessageType.Smsc_Disconnect_Message);
        assertEquals(0, messages.size());
    }

    @Test
    public void getIncomBySmscSessionId_wrong_type_Test()
    {
        DevMessage devMessage = getTestMessage();

        SmscSessionId smscSessionId = new SmscSessionId(GenericConstants.SMSC_SESSION_ID);
        messageRegister.insertIncomingMsg(devMessage);

        List<WebSocketMessageBase> messages = messageRegister.getIncomBySmscSessionId(smscSessionId, MessageType.Dev_Message);
        assertEquals(0, messages.size());
    }

    @Test
    public void getIncomBySmscSessionId_null_Test()
    {
        List<WebSocketMessageBase> messages = messageRegister.getIncomBySmscSessionId(null, MessageType.Smsc_SendSms_Message);
        assertNotNull(messages);
        assertEquals(0, messages.size());

        messages = messageRegister.getIncomBySmscSessionId(new SmscSessionId(), MessageType.Smsc_SendSms_Message);
        assertNotNull(messages);
        assertEquals(0, messages.size());

        messages = messageRegister.getIncomBySmscSessionId(new SmscSessionId(GenericConstants.SMSC_SESSION_ID), null);
        assertNotNull(messages);
        assertEquals(0, messages.size());
    }

    @Test
    public void getIncomBySmscSessionId_empty_Test()
    {
        SmscSessionId smscSessionId = new SmscSessionId(GenericConstants.SMSC_SESSION_ID);
        List<WebSocketMessageBase> messages = messageRegister.getIncomBySmscSessionId(smscSessionId, MessageType.Smsc_Disconnect_Message);
        assertEquals(0, messages.size());
    }

    @Test
    public void insertUnknownMessageTest()
    {
        DevMessage devMessage = getTestMessage();

        messageRegister.insertUnknownMessage(JsonUtilsSafe.toJson(devMessage));

        List<String> result = messageRegister.getAllUnknown();

        assertNotNull(result);
        assertEquals(1, result.size());
    }


}
