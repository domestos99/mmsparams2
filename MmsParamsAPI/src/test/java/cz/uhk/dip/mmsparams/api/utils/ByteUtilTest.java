package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ByteUtilTest
{

    @Test
    public void isEmptyOrNull()
    {
        assertTrue(ByteUtil.isEmptyOrNull(null));
        assertTrue(ByteUtil.isEmptyOrNull(new byte[0]));
        assertFalse(ByteUtil.isEmptyOrNull(new byte[1]));
    }
}