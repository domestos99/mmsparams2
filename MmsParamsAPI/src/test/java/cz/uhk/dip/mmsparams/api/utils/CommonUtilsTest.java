package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class CommonUtilsTest
{

    @Test
    public void getUUIDTest()
    {
        String uuid = CommonUtils.getUUID();
        assertFalse(StringUtil.isEmptyOrNull(uuid));
    }

    @Test
    public void getUUIDListTest()
    {
        List<String> uuids = CommonUtils.getUUIDList(5);
        assertNotNull(uuids);
        assertEquals(5, uuids.size());
        for (String uuid : uuids)
        {
            assertFalse(StringUtil.isEmptyOrNull(uuid));
        }
    }

    @Test
    public void getUUIDList_negative_Test()
    {
        List<String> uuids = CommonUtils.getUUIDList(0);
        assertNotNull(uuids);
        assertEquals(0, uuids.size());

        uuids = CommonUtils.getUUIDList(-1);
        assertNotNull(uuids);
        assertEquals(0, uuids.size());
    }
}
