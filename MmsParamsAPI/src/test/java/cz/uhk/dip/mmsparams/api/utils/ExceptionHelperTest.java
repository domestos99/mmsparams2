package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.exceptions.SystemException;
import cz.uhk.dip.mmsparams.api.exceptions.TimeOutException;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ExceptionHelperTest
{
    @Test
    public void generateException_null_Test()
    {
        Exception ex = ExceptionHelper.generateException(null);
        assertNotNull(ex);
        assertEquals(Exception.class, ex.getClass());
    }

    @Test
    public void generateExceptionTest()
    {
        GenericErrorResponseMessage msg = new MessageBuilder().build(GenericErrorResponseMessage.class);

        Exception ex = ExceptionHelper.generateException(msg);
        assertEquals(msg.toString(), ex.getMessage());

        msg.setTestErrorModel(new TestErrorModel());
        ex = ExceptionHelper.generateException(msg);
        assertEquals(msg.toString(), ex.getMessage());
    }

    @Test
    public void castExceptionTest()
    {
        Exception ex = new Exception("xxx");
        SystemException result = ExceptionHelper.castException(ex);
        assertNotNull(result);
        assertEquals(SystemException.class, result.getClass());
        assertEquals(ex, result.getCause());
    }

    @Test
    public void castException2Test()
    {
        TimeOutException tmEx = new TimeOutException("xxx");
        TimeOutException result2 = ExceptionHelper.castException(tmEx);
        assertNotNull(result2);
        assertEquals(TimeOutException.class, result2.getClass());
    }
}