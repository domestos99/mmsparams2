package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Optional;

import cz.uhk.dip.mmsparams.api.UnitTestBase;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class FileHelperTest extends UnitTestBase
{

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private File fileJpg;
    private File fileAvi;
    private File fileTwoDots;
    private File fileMp3;

    @Before
    public void setUp() throws Exception
    {
        fileJpg = folder.newFile("image.jpg");
        fileAvi = folder.newFile("video.avi");
        fileMp3 = folder.newFile("fileMp3.mp3");
        fileTwoDots = folder.newFile("twoDots.xx.txt");
        System.out.println(fileJpg.getAbsolutePath());
        System.out.println(fileAvi.getAbsolutePath());
        System.out.println(fileTwoDots.getAbsolutePath());
    }

    @Test
    public void getExtension_jpg_Test() throws URISyntaxException
    {
        Optional<String> ext = FileHelper.getExtension(fileJpg);
        assertTrue(ext.isPresent());
        assertEquals("jpg", ext.get());
    }

    @Test
    public void getExtension_avi_Test() throws URISyntaxException
    {
        Optional<String> ext = FileHelper.getExtension(fileAvi);
        assertTrue(ext.isPresent());
        assertEquals("avi", ext.get());
    }

    @Test
    public void getExtension_two_dots_Test() throws URISyntaxException
    {
        Optional<String> ext = FileHelper.getExtension(fileTwoDots);
        assertTrue(ext.isPresent());
        assertEquals("txt", ext.get());
    }

    @Test
    public void getMimeType_jpeg_Test()
    {
        String mime = FileHelper.getMimeType(fileJpg);
        assertEquals("image/jpeg", mime);
    }

    @Test
    public void getMimeType_avi_Test()
    {
        String mime = FileHelper.getMimeType(fileAvi);
        assertEquals("video/x-msvideo", mime);
    }

    @Test
    public void getMimeType_txt_Test()
    {
        String mime = FileHelper.getMimeType(fileTwoDots);
        assertEquals("text/plain", mime);
    }

    @Test
    public void getMimeType_mp3_Test()
    {
        String mime = FileHelper.getMimeType(fileMp3);
        assertEquals("audio/mpeg", mime);
    }

}
