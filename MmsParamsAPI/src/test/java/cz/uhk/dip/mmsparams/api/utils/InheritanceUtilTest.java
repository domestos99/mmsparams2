package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.UnitTestBase;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class InheritanceUtilTest extends UnitTestBase
{

    @Test
    public void isSubclassTest()
    {
        boolean result = InheritanceUtil.isSubclass(new B(), A.class);
        assertTrue(result);
    }

    @Test
    public void isSubclass_interface_Test()
    {
        boolean result = InheritanceUtil.isSubclass(new D(), C.class);
        assertTrue(result);
    }

    @Test
    public void isSubclass_interface_false_Test()
    {
        boolean result = InheritanceUtil.isSubclass(new A(), C.class);
        assertFalse(result);
    }

    @Test
    public void isSubclass_false_Test()
    {
        boolean result = InheritanceUtil.isSubclass(new A(), B.class);
        assertFalse(result);
    }

    class A
    {

    }

    class B extends A
    {

    }

    interface C
    {

    }

    class D implements C
    {

    }
}
