package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ListUtilsTest
{
    @Before
    public void setUp() throws Exception
    {
        l.clear();
        l.add("a");
        l.add(1);
        l.add(true);
    }

    List<Object> l = new ArrayList<>();

    @Test
    public void getStringTest()
    {
        assertNull(ListUtils.getString(null));

        List<String> result = ListUtils.getString(l);
        assertNotNull(result);
        for (int i = 0; i < l.size(); i++)
        {
            assertEquals(l.get(i).toString(), result.get(i));
        }
    }

    @Test
    public void toList2Test()
    {
        assertNotNull(ListUtils.toList2(null));

        Collection<List<Object>> col = new ArrayList<>();
        col.add(l);
        col.add(l);

        List<Object> result = ListUtils.toList2(col);
        assertNotNull(result);
        assertEquals(l.size() * 2, result.size());
    }

    @Test
    public void toListTest()
    {
        assertNotNull(ListUtils.toList((List) null));

        List<Object> result = ListUtils.toList(l);
        assertNotNull(result);
        for (int i = 0; i < l.size(); i++)
        {
            assertEquals(l.get(i), result.get(i));
        }
    }

    @Test
    public void toList_arr_Test()
    {
        assertNotNull(ListUtils.toList((Object[]) null));

        Object[] arr = l.toArray();
        List<Object> result = ListUtils.toList(arr);
        assertNotNull(result);
        assertEquals(l.size(), arr.length);
        assertEquals(l.size(), result.size());
        for (int i = 0; i < l.size(); i++)
        {
            assertEquals(arr[i], result.get(i));
        }
    }

    @Test
    public void toArrayListTest()
    {
        assertNotNull(ListUtils.toArrayList(null));

        ArrayList<Object> result = ListUtils.toArrayList(l);
        assertEquals(l.size(), result.size());
        for (int i = 0; i < l.size(); i++)
        {
            assertEquals(l.get(i), result.get(i));
        }
    }

    @Test
    public void toListNullTest()
    {
        assertNull(ListUtils.toListNull(null));
        assertNotNull(l);
    }

    @Test
    public void newListTest()
    {
        assertNotNull(ListUtils.newList(null));

        List<Object> list = ListUtils.newList();
        assertNotNull(list);
        assertEquals(0, list.size());

        List<Object> list2 = ListUtils.newList(l);
        assertNotNull(list2);
        assertEquals(l.size(), list2.size());
    }


}