package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class LogUtilTest
{

    @Test
    public void getCurrentStackTrace()
    {
        String stackTrace = LogUtil.getCurrentStackTrace();
        System.out.println(stackTrace);
        assertNotNull(stackTrace);
    }

    @Test
    public void appendLineTest()
    {
        StringBuilder sb = new StringBuilder();
        LogUtil.appendLine(sb, "xxx");
        String result = sb.toString();
        assertNotNull(result);
        assertEquals("xxx\r\n", result);
    }
}
