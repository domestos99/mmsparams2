package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketConstants;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class MessageBuilderTest
{
    @Test
    public void builderTestA()
    {
        GenericBooleanResponseMessage msg = new MessageBuilder()
                .withSenderKeyServer()
                .withMessageId(GenericConstants.MESSAGE_ID)
                .withRecipientKeyServer()
                .withNoTestId()
                .build(GenericBooleanResponseMessage.class);

        assertNotNull(msg);
        assertEquals(WebSocketConstants.Server_Recipient_Key, msg.getSenderKey());
        assertEquals(GenericConstants.MESSAGE_ID, msg.getMessageID());
        assertEquals(WebSocketConstants.Server_Recipient_Key, msg.getRecipientKey());
        assertNull(msg.getTestID());
        assertEquals(MessageType.Generic_Boolean_Response_Message, msg.getMessageKey());
        assertTrue(msg.getCreatedDT() > 0);
    }

    @Test
    public void builderTestB()
    {
        GenericBooleanResponseMessage msg = new MessageBuilder()
                .withSenderKey(GenericConstants.SENDER_KEY)
                .withMessageIdRandom()
                .withRecipientKey(GenericConstants.RECIPIENT_KEY)
                .withTestId(new ITestIdProvider()
                {
                    @Override
                    public String getTestId()
                    {
                        return GenericConstants.TEST_ID;
                    }
                })
                .build(GenericBooleanResponseMessage.class);

        assertNotNull(msg);
        assertEquals(GenericConstants.SENDER_KEY, msg.getSenderKey());
        assertFalse(StringUtil.isEmptyOrNull(msg.getMessageID()));
        assertEquals(GenericConstants.RECIPIENT_KEY, msg.getRecipientKey());
        assertEquals(GenericConstants.TEST_ID, msg.getTestID());
        assertEquals(MessageType.Generic_Boolean_Response_Message, msg.getMessageKey());
        assertTrue(msg.getCreatedDT() > 0);
    }

    @Test
    public void builderTestC()
    {
        GenericBooleanResponseMessage msg = new MessageBuilder()
                .withRecipientKeyClientBroadcast()
                .build(GenericBooleanResponseMessage.class);

        assertNotNull(msg);
        assertNull(msg.getSenderKey());
        assertNull(msg.getMessageID());
        assertEquals(WebSocketConstants.Server_To_Client_Broadcast, msg.getRecipientKey());
        assertNull(msg.getTestID());
        assertEquals(MessageType.Generic_Boolean_Response_Message, msg.getMessageKey());
        assertTrue(msg.getCreatedDT() > 0);
    }

    @Test
    public void builderTestD()
    {
        GenericBooleanResponseMessage msg = new MessageBuilder()
                .withSenderKey(GenericConstants.SENDER_KEY)
                .withMessageIdRandom()
                .withRecipientKey(GenericConstants.RECIPIENT_KEY)
                .withTestId(new ITestIdProvider()
                {
                    @Override
                    public String getTestId()
                    {
                        return GenericConstants.TEST_ID;
                    }
                })
                .build(GenericBooleanResponseMessage.class);

        assertNotNull(msg);
        assertEquals(GenericConstants.SENDER_KEY, msg.getSenderKey());
        assertFalse(StringUtil.isEmptyOrNull(msg.getMessageID()));
        assertEquals(GenericConstants.RECIPIENT_KEY, msg.getRecipientKey());
        assertEquals(GenericConstants.TEST_ID, msg.getTestID());
        assertEquals(MessageType.Generic_Boolean_Response_Message, msg.getMessageKey());
        assertTrue(msg.getCreatedDT() > 0);
    }

    @Test
    public void builderTestE()
    {
        GenericBooleanResponseMessage msg = MessageBuilder
                .build(GenericBooleanResponseMessage.class,
                        new ITestIdProvider()
                        {
                            @Override
                            public String getTestId()
                            {
                                return GenericConstants.TEST_ID;
                            }
                        },
                        GenericConstants.SENDER_KEY, GenericConstants.RECIPIENT_KEY, GenericConstants.MESSAGE_ID);

        assertNotNull(msg);
        assertEquals(GenericConstants.SENDER_KEY, msg.getSenderKey());
        assertFalse(StringUtil.isEmptyOrNull(msg.getMessageID()));
        assertEquals(GenericConstants.RECIPIENT_KEY, msg.getRecipientKey());
        assertEquals(GenericConstants.TEST_ID, msg.getTestID());
        assertEquals(MessageType.Generic_Boolean_Response_Message, msg.getMessageKey());
        assertTrue(msg.getCreatedDT() > 0);
    }


    @Test
    public void builderTestF()
    {
        DevMessage msg = new MessageBuilder()
                .withSenderKey(GenericConstants.SENDER_KEY)
                .withMessageIdRandom()
                .withRecipientKey(GenericConstants.RECIPIENT_KEY)
                .withTestId(new ITestIdProvider()
                {
                    @Override
                    public String getTestId()
                    {
                        return GenericConstants.TEST_ID;
                    }
                })
                .build(DevMessage.class);


        GenericBooleanResponseMessage resp = new MessageBuilder()
                .withPrepareResponse(msg)
                .build(GenericBooleanResponseMessage.class);


        assertNotNull(resp);
        assertNull(resp.getSenderKey());
        assertEquals(msg.getMessageID(), resp.getMessageID());
        assertEquals(msg.getSenderKey(), resp.getRecipientKey());
        assertEquals(msg.getTestID(), resp.getTestID());
        assertEquals(MessageType.Generic_Boolean_Response_Message, resp.getMessageKey());
        assertTrue(resp.getCreatedDT() > 0);
    }
}