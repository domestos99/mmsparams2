package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.enums.AddressType;
import cz.uhk.dip.mmsparams.api.enums.RecipientType;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MmscAcquireRouteModel;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MmscAcquireRouteUtilTest
{
    @Test
    public void isMatch_basicPattern_test()
    {
        String pattern = "999123*";

        MmscAcquireRouteModel model = new MmscAcquireRouteModel();
        model.setPattern(pattern);

        List<MM7Address> addresses = new ArrayList<>();
        addresses.add(new MM7Address("99912301", AddressType.NUMBER, RecipientType.TO));

        assertTrue(MmscAcquireRouteUtil.isMatch(model, addresses));
    }

    @Test
    public void isMatch_specificPattern_test()
    {
        String pattern = "99912301";

        MmscAcquireRouteModel model = new MmscAcquireRouteModel();
        model.setPattern(pattern);

        List<MM7Address> addresses = new ArrayList<>();
        addresses.add(new MM7Address("99912301", AddressType.NUMBER, RecipientType.TO));

        assertTrue(MmscAcquireRouteUtil.isMatch(model, addresses));
    }

    @Test
    public void isMatch_different_specificPattern_test()
    {
        String pattern = "999123012";

        MmscAcquireRouteModel model = new MmscAcquireRouteModel();
        model.setPattern(pattern);

        List<MM7Address> addresses = new ArrayList<>();
        addresses.add(new MM7Address("99912301", AddressType.NUMBER, RecipientType.TO));

        assertFalse(MmscAcquireRouteUtil.isMatch(model, addresses));
    }

    @Test
    public void isMatch_different_specificPattern_test2()
    {
        String pattern = "99912301";

        MmscAcquireRouteModel model = new MmscAcquireRouteModel();
        model.setPattern(pattern);

        List<MM7Address> addresses = new ArrayList<>();
        addresses.add(new MM7Address("999123012", AddressType.NUMBER, RecipientType.TO));

        assertFalse(MmscAcquireRouteUtil.isMatch(model, addresses));
    }


    @Test
    public void isMatch_longerPattern_test()
    {
        String pattern = "999*";

        MmscAcquireRouteModel model = new MmscAcquireRouteModel();
        model.setPattern(pattern);

        List<MM7Address> addresses = new ArrayList<>();
        addresses.add(new MM7Address("999", AddressType.NUMBER, RecipientType.TO));

        assertFalse(MmscAcquireRouteUtil.isMatch(model, addresses));
    }

    @Test
    public void isMatchTest6()
    {
        String pattern = "999*";

        MmscAcquireRouteModel model = new MmscAcquireRouteModel();
        model.setPattern(pattern);

        List<MM7Address> addresses = new ArrayList<>();
        addresses.add(new MM7Address("999123", AddressType.NUMBER, RecipientType.TO));

        assertTrue(MmscAcquireRouteUtil.isMatch(model, addresses));
    }

    @Test
    public void isMatchTest7()
    {
        String pattern = "999*";

        MmscAcquireRouteModel model = new MmscAcquireRouteModel();
        model.setPattern(pattern);

        List<MM7Address> addresses = new ArrayList<>();
        addresses.add(new MM7Address("9992", AddressType.NUMBER, RecipientType.TO));

        assertTrue(MmscAcquireRouteUtil.isMatch(model, addresses));
    }

    @Test
    public void isMatch_textPattern_test()
    {
        String pattern = "MyServ*";

        MmscAcquireRouteModel model = new MmscAcquireRouteModel();
        model.setPattern(pattern);

        List<MM7Address> addresses = new ArrayList<>();
        addresses.add(new MM7Address("MyService123", AddressType.NUMBER, RecipientType.TO));

        assertTrue(MmscAcquireRouteUtil.isMatch(model, addresses));
    }

}
