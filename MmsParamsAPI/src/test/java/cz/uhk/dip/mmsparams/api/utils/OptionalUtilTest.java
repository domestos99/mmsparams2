package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class OptionalUtilTest
{

    @Test
    public void fillOptionalTest()
    {
        String a = "a";
        Optional<String> opt = OptionalUtil.fillOptional(a);
        assertNotNull(opt);
        assertEquals(a, opt.get());
    }

    @Test
    public void fillOptional_null_Test()
    {
        Optional<String> opt = OptionalUtil.fillOptional(null);
        assertNotNull(opt);
        assertFalse(opt.isPresent());
    }
}