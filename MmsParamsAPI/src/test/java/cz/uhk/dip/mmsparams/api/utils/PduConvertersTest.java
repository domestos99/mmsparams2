package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.PduHeaders2;
import cz.uhk.dip.mmsparams.api.enums.MessageClass;
import cz.uhk.dip.mmsparams.api.enums.Priority;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PduConvertersTest
{

    @Test
    public void getYesNoValueTest()
    {
        assertEquals(0x80, PduConverters.getYesNoValue(true));
        assertEquals(128, PduConverters.getYesNoValue(true));
        assertEquals(0x81, PduConverters.getYesNoValue(false));
        assertEquals(129, PduConverters.getYesNoValue(false));
    }

    @Test
    public void getYesNoValue1Test()
    {
        assertTrue(PduConverters.getYesNoValue(0x80));
        assertTrue(PduConverters.getYesNoValue(128));
        assertFalse(PduConverters.getYesNoValue(0x81));
        assertFalse(PduConverters.getYesNoValue(129));
        assertFalse(PduConverters.getYesNoValue(0));
    }

    @Test
    public void getClassValueTest()
    {
        assertArrayEquals(PduHeaders2.MESSAGE_CLASS_PERSONAL_STR.getBytes(), "personal".getBytes());
        assertArrayEquals(PduHeaders2.MESSAGE_CLASS_PERSONAL_STR.getBytes(), PduConverters.getClassValue(MessageClass.PERSONAL));
    }

    @Test
    public void getPriorityTest()
    {
        assertEquals(0x80, PduConverters.getPriority(Priority.LOW));
        assertEquals(128, PduConverters.getPriority(Priority.LOW));

        assertEquals(0x81, PduConverters.getPriority(Priority.NORMAL));
        assertEquals(129, PduConverters.getPriority(Priority.NORMAL));

        assertEquals(0x82, PduConverters.getPriority(Priority.HIGH));
        assertEquals(130, PduConverters.getPriority(Priority.HIGH));
    }

    @Test
    public void getMsgClassTest()
    {
        assertEquals(MessageClass.PERSONAL, PduConverters.getMsgClass("PERSONAL"));
        assertEquals(MessageClass.PERSONAL, PduConverters.getMsgClass("personal"));
    }

    @Test
    public void getDeliveryReportTest()
    {
        assertEquals(true, PduConverters.getDeliveryReport(0x80));
        assertEquals(true, PduConverters.getDeliveryReport(128));
        assertEquals(false, PduConverters.getDeliveryReport(0x81));
        assertEquals(false, PduConverters.getDeliveryReport(129));

        assertEquals(false, PduConverters.getDeliveryReport(0));
    }

    @Test
    public void getReadReportTest()
    {
        assertEquals(true, PduConverters.getReadReport(0x80));
        assertEquals(true, PduConverters.getReadReport(128));
        assertEquals(false, PduConverters.getReadReport(0x81));
        assertEquals(false, PduConverters.getReadReport(129));

        assertEquals(false, PduConverters.getReadReport(0));
    }

    @Test
    public void getMsgPriorityTest()
    {
        assertEquals(Priority.LOW, PduConverters.getMsgPriority(128));
        assertEquals(Priority.NORMAL, PduConverters.getMsgPriority(129));
        assertEquals(Priority.HIGH, PduConverters.getMsgPriority(130));

        assertNull(PduConverters.getMsgPriority(0));
    }
}
