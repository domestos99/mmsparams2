package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class StringUtilTest
{


    @Test
    public void getStringTest()
    {
        byte[] bytes = null;

        String s = StringUtil.getString(bytes);

        assertNull(s);

        bytes = new byte[0];

        s = StringUtil.getString(bytes);

        assertEquals("", s);
    }

    @Test
    public void bytesStringConvertTest()
    {
        final String s = "hello world";

        final byte[] bytes = StringUtil.toBytes(s);

        assertNotNull(bytes);

        final String a = StringUtil.getString(bytes);

        assertEquals(s, a);


        final byte[] bytes2 = StringUtil.toBytes(null);

        assertNull(bytes2);
    }

    @Test
    public void bytesStringConvertSpecialTest()
    {
        testxx("hello world");
        testxx("+ěščřžýáíé");
        testxx("{Đ][Đ[Đ]đĐ[$÷[]đĐĐ[÷đĐ[ßđ[Đ÷×đŁđ&>&ß[]łŁ$fgsd");
        testxx("čásdff321315ěššě++´´=´´´$ß¤ß×÷[×Đđ[đĐĐ[Łđ$[đĐ$đĐ÷ß");
    }

    private void testxx(final String s)
    {
        final byte[] bytes = StringUtil.toBytes(s);
        assertNotNull(bytes);
        final String a = StringUtil.getString(bytes);
        assertEquals(s, a);
    }

    @Test
    public void toFirstUpperTest()
    {
        final String init = "ahoj";
        final String result = StringUtil.toFirstUpper(init);
        assertEquals("Ahoj", result);

        final String result2 = StringUtil.toFirstUpper("");
        assertEquals("", result2);

        final String result3 = StringUtil.toFirstUpper(null);
        assertNull(result3);
    }

    @Test
    public void trim1Test()
    {
        String s1 = "hello";

        String s2 = StringUtil.trim(s1, ',');

        assertNotNull(s2);
        assertEquals(s1, s2);
    }

    @Test
    public void trim2Test()
    {
        String s1 = "hello";

        String s2 = StringUtil.trim(s1, ' ');

        assertNotNull(s2);
        assertEquals(s1, s2);
    }

    @Test
    public void trim3Test()
    {
        String s1 = "hello,";

        String s2 = StringUtil.trim(s1, ',');

        assertNotNull(s2);
        assertEquals("hello", s2);
    }

    @Test
    public void trim4Test()
    {
        String s1 = "hello)";

        String s2 = StringUtil.trim(s1, ',');

        assertNotNull(s2);
        assertEquals("hello)", s2);
    }


    @Test
    public void trim5Test()
    {
        String s1 = null;

        String s2 = StringUtil.trim(s1, ',');

        // assertNotNull(s2);
        assertEquals(s1, s2);
    }

    @Test
    public void trim6Test()
    {
        String s1 = "";

        String s2 = StringUtil.trim(s1, ',');

        // assertNotNull(s2);
        assertEquals(s1, s2);
    }


    @Test
    public void trim7Test()
    {
        String s1 = "x";

        String s2 = StringUtil.trim(s1, ',');

        // assertNotNull(s2);
        assertEquals(s1, s2);
    }

    @Test
    public void trim8Test()
    {
        String s1 = "xxx";

        String s2 = StringUtil.trim(s1, ',');

        // assertNotNull(s2);
        assertEquals(s1, s2);
    }


    @Test
    public void trim9Test()
    {
        String s1 = "xxx,,";

        String s2 = StringUtil.trim(s1, ',');

        // assertNotNull(s2);
        assertEquals("xxx,", s2);
    }


    @Test
    public void joinTest()
    {
        List<String> s = new ArrayList<>();

        s.add("a");
        s.add("bb");
        s.add("");
        s.add("cc");
        s.add(null);
        s.add("ddddddd");
        s.add("eeeeeee");

        String scv = StringUtil.join(s, ';');

        assertEquals("a;bb;cc;ddddddd;eeeeeee", scv);
    }

    @Test
    public void areEqual1Test()
    {
        String a = "a";
        String b = "a";
        assertTrue(StringUtil.areEqual(a, a));
        assertTrue(StringUtil.areEqual(a, b));
        b = "b";
        assertFalse(StringUtil.areEqual(a, b));
    }

    @Test
    public void getStringSafeTest()
    {
        assertNull(StringUtil.getStringSafe((Object) null));
        assertEquals("1", StringUtil.getStringSafe(1));
        assertEquals("abc", StringUtil.getStringSafe("abc"));
        assertEquals("true", StringUtil.getStringSafe(true));
    }

}
