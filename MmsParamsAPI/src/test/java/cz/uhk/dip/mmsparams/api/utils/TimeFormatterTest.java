package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TimeFormatterTest
{

    @Test
    public void formatTest()
    {
        String result = TimeFormatter.format(0);
        assertEquals("01:00:00.000", result);

        result = TimeFormatter.format(1570467851378L);
        assertEquals("19:04:11.378", result);
    }
}