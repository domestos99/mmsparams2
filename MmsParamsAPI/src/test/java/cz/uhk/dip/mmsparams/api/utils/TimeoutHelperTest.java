package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TimeoutHelperTest
{

    @Test
    public void timeoutTest()
    {
        Date start = new Date();
        assertFalse(TimeoutHelper.checkTimeOut(start, 60));
        assertFalse(TimeoutHelper.checkTimeOut(start, 10));


        start = new Date();
        start.setTime(start.getTime() - 20000);

        assertTrue(TimeoutHelper.checkTimeOut(start, 10));
    }

}