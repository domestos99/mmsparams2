package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.smsc.SMSCConstant;

import static org.junit.Assert.assertEquals;

public class TlvHelperTest
{

    @Test
    public void getTlvNameTest()
    {
        String name = TlvHelper.getTlvName(SMSCConstant.TAG_ORIG_MSC_ADDR);
        assertEquals("TAG_ORIG_MSC_ADDR", name);

        name = TlvHelper.getTlvName(SMSCConstant.TAG_PAYLOAD_TYPE);
        assertEquals("TAG_PAYLOAD_TYPE", name);
    }
}
