package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.smsc.SMSCConstant;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.TlvModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TlvUtilsTest
{

    @Test
    public void createMessageTlvTest()
    {
        TlvModel result = TlvUtils.createMessageTlv("ahoj");
        assertNotNull(result);
        assertEquals(SMSCConstant.TAG_MESSAGE_PAYLOAD, result.getTag());
    }
}