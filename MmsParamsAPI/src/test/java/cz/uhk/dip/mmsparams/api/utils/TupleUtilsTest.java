package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TupleUtilsTest
{
    List<Tuple<String, String>> l = new ArrayList<>();

    @Before
    public void setUp() throws Exception
    {
        l.clear();
        l.add(new Tuple<>("a", "A"));
        l.add(new Tuple<>("b", "B"));
        l.add(new Tuple<>("c", "C"));
    }

    @Test
    public void getYOnlyTest()
    {
        List<String> result = TupleUtils.getYOnly(l);
        assertNotNull(result);
        assertEquals(l.size(), result.size());

        for (int i = 0; i < l.size(); i++)
        {
            assertEquals(l.get(i).getY(), result.get(i));
        }
    }

    @Test
    public void getXOnlyTest()
    {
        List<String> result = TupleUtils.getXOnly(l);
        assertNotNull(result);
        assertEquals(l.size(), result.size());

        for (int i = 0; i < l.size(); i++)
        {
            assertEquals(l.get(i).getX(), result.get(i));
        }
    }
}