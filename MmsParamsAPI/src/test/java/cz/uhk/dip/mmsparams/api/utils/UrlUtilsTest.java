package cz.uhk.dip.mmsparams.api.utils;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;

import static org.junit.Assert.assertEquals;

public class UrlUtilsTest
{
    final String address = "192.168.1.101";
    final String port = "4301";

    @Test
    public void getServerSocketUrlTest()
    {
        String result = UrlUtils.getServerSocketUrl(HttpProtocolEnum.HTTP, address);
        assertEquals("ws://192.168.1.101/appsocket", result);
    }

    @Test
    public void getServerSocket_https_UrlTest()
    {
        String result = UrlUtils.getServerSocketUrl(HttpProtocolEnum.HTTPS, address);
        assertEquals("wss://192.168.1.101/appsocket", result);
    }

    @Test
    public void testGetServerSocketUrlTest()
    {
        String result = UrlUtils.getServerSocketUrl(HttpProtocolEnum.HTTP, address, port);
        assertEquals("ws://192.168.1.101:4301/appsocket", result);
    }

    @Test
    public void testGetServerSocket_https_UrlTest()
    {
        String result = UrlUtils.getServerSocketUrl(HttpProtocolEnum.HTTPS, address, port);
        assertEquals("wss://192.168.1.101:4301/appsocket", result);
    }
}