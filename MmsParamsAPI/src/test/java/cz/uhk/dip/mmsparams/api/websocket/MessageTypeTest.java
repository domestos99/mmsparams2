package cz.uhk.dip.mmsparams.api.websocket;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class MessageTypeTest
{

    @Test
    public void getTest()
    {

        Class clz = MessageType.getClassByKey(RegisterPhoneMessage.class.getSimpleName().toUpperCase());

        assertNotNull(clz);
        assertEquals(RegisterPhoneMessage.class, clz);


        String key = MessageType.getKeyByClass(RegisterPhoneMessage.class);

        assertEquals(RegisterPhoneMessage.class.getSimpleName().toUpperCase(), key);
    }

    @Test
    public void getKeyByClassNullTest()
    {
        String type = MessageType.getKeyByClass(MessageTypeTest.class);
        assertNull(type);
    }

}
