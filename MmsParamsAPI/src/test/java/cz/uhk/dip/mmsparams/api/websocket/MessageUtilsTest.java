package cz.uhk.dip.mmsparams.api.websocket;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.UnitTestBase;
import cz.uhk.dip.mmsparams.api.json.JsonUtils;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.utils.CommonUtils;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MessageUtilsTest extends UnitTestBase
{
    @Test
    public void getSendableMessageTest()
    {
        DevMessage msg = MessageFactory.create(DevMessage.class);
        String jsonMessage = MessageUtils.getSendableMessage(msg);

        assertFalse(StringUtil.isEmptyOrNull(jsonMessage));
    }

    @Test
    public void getSendableMessageNullTest()
    {
        String jsonMessage = MessageUtils.getSendableMessage(null);
        assertTrue(StringUtil.isEmptyOrNull(jsonMessage));
    }

    @Test
    public void prepareResponseTest()
    {
        DevMessage msg = getTestMessage();

        GenericBooleanResponseMessage resp =
                new MessageBuilder()
                        .build(GenericBooleanResponseMessage.class);

        GenericBooleanResponseMessage resp2 = MessageUtils.prepareResponse(resp, msg);

        assertNotNull(resp2);
        assertEquals(msg.getSenderKey(), resp2.getRecipientKey());
        assertEquals(msg.getTestID(), resp2.getTestID());
        assertEquals(msg.getMessageID(), resp2.getMessageID());
    }

    @Test(expected = RuntimeException.class)
    public void prepareResponseNull1Test()
    {
        DevMessage msg = getTestMessage();

        GenericBooleanResponseMessage resp = new MessageBuilder().build(GenericBooleanResponseMessage.class);
        GenericBooleanResponseMessage resp2 = MessageUtils.prepareResponse(null, msg);
    }

    @Test(expected = RuntimeException.class)
    public void prepareResponseNull2Test()
    {
        DevMessage msg = getTestMessage();

        GenericBooleanResponseMessage resp = new MessageBuilder().build(GenericBooleanResponseMessage.class);
        GenericBooleanResponseMessage resp2 = MessageUtils.prepareResponse(resp, null);
    }

    @Test(expected = RuntimeException.class)
    public void prepareResponseEqualTest()
    {
        DevMessage msg = getTestMessage();

        GenericBooleanResponseMessage resp = new MessageBuilder().build(GenericBooleanResponseMessage.class);
        GenericBooleanResponseMessage resp2 = MessageUtils.prepareResponse(resp, resp);
    }

    @Test
    public void isMessageValidNullTest()
    {
        assertFalse(MessageUtils.isMessageValid(null));
    }

    @Test
    public void getMessageKeyTest()
    {
        DevMessage msg = getTestMessage();
        String json = JsonUtilsSafe.toJson(msg);
        String msgKey = MessageUtils.getMessageKey(json);
        assertEquals(MessageType.Dev_Message, msgKey);
    }

    @Test
    public void copyListTest()
    {
        List<WebSocketMessageBase> msgList = new ArrayList<>();
        msgList.add(getTestMessage());
        msgList.add(getTestMessage());

        List<DevMessage> list = MessageUtils.copyList(msgList);
        assertNotNull(list);
        assertEquals(2, list.size());
    }

    @Test
    public void copyListNullTest()
    {
        List<DevMessage> list = MessageUtils.copyList(null);
        assertNotNull(list);
        assertEquals(0, list.size());
    }

    @Test
    public void isMessageValidTest()
    {
        EmptyMessage emptyMessage = new EmptyMessage();

        emptyMessage.setMessageKey("jfdkla");

        assertFalse(MessageUtils.isMessageValid(emptyMessage));

        emptyMessage.setMessageKey(MessageType.Dev_Message);

        assertFalse(MessageUtils.isMessageValid(emptyMessage));

        emptyMessage.setMessageID(CommonUtils.getUUID());

        assertFalse(MessageUtils.isMessageValid(emptyMessage));

        emptyMessage.setSenderKey(CommonUtils.getUUID());

        assertFalse(MessageUtils.isMessageValid(emptyMessage));

        emptyMessage.setRecipientKey(CommonUtils.getUUID());

        assertTrue(MessageUtils.isMessageValid(emptyMessage));
    }


    @Test
    public void isMessageValidTest2()
    {
        EmptyMessage emptyMessage = new EmptyMessage();

        emptyMessage.setMessageID(CommonUtils.getUUID());
        emptyMessage.setSenderKey(CommonUtils.getUUID());
        emptyMessage.setRecipientKey(CommonUtils.getUUID());

        assertFalse(MessageUtils.isMessageValid(emptyMessage));

        emptyMessage.setMessageKey("jfdkla");
        assertFalse(MessageUtils.isMessageValid(emptyMessage));

        emptyMessage.setMessageKey(MessageType.Dev_Message);
        assertTrue(MessageUtils.isMessageValid(emptyMessage));
    }


    @Test
    public void getMessageNonTypedTest() throws IOException
    {
        GenericErrorResponseMessage msg =
                new MessageBuilder()
                        .withMessageId("hello")
                        .build(GenericErrorResponseMessage.class);

        String json = JsonUtils.toJson(msg);

        WebSocketMessageBase typedMessage = MessageUtils.getMessageNonTyped(json);

        assertEquals(typedMessage.getClass(), msg.getClass());
        assertEquals("hello", ((GenericErrorResponseMessage) typedMessage).getMessageID());
    }

    @Test
    public void isRecipientPhoneTest()
    {
        String key = WebSocketConstants.Android_Key_Prefix + CommonUtils.getUUID();
        assertTrue(MessageUtils.isRecipientPhone(key));
        assertFalse(MessageUtils.isRecipientPhone(CommonUtils.getUUID()));
    }
}
