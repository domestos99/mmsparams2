package cz.uhk.dip.mmsparams.api.websocket;

import org.junit.Test;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.UnitTestBase;
import cz.uhk.dip.mmsparams.api.json.JsonUtils;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RegisterPhoneMessageTest extends UnitTestBase
{

    @Test
    public void parseTest() throws IOException
    {
        RegisterPhoneMessage msg = new MessageBuilder()
                .withSenderKey("test-sender-key")
                .withRecipientKey("test-rec-key")
                .withTestId(() -> "testID")
                .build(RegisterPhoneMessage.class);


        msg.setPhoneInfoModel(new PhoneInfoModel());

        String json = JsonUtils.toJson(msg);


        EmptyMessage emptyMessage = JsonUtils.fromJson(json, EmptyMessage.class);

        assertNotNull(emptyMessage);

        assertEquals(msg.getSenderKey(), emptyMessage.getSenderKey());
        assertEquals(msg.getRecipientKey(), emptyMessage.getRecipientKey());
        assertEquals(msg.getMessageKey(), emptyMessage.getMessageKey());
        assertEquals(msg.getTestID(), emptyMessage.getTestID());


        RegisterPhoneMessage parsed = JsonUtils.fromJson(json, RegisterPhoneMessage.class);


        assertNotNull(parsed);

        assertEquals(msg.getSenderKey(), parsed.getSenderKey());
        assertEquals(msg.getRecipientKey(), parsed.getRecipientKey());
        assertEquals(msg.getMessageKey(), parsed.getMessageKey());
        assertEquals(msg.getTestID(), parsed.getTestID());


    }

}
