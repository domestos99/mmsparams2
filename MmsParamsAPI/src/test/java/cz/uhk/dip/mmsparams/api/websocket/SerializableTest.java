package cz.uhk.dip.mmsparams.api.websocket;

import org.junit.Assert;
import org.junit.Test;
import org.reflections.Reflections;

import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.util.Set;

import cz.uhk.dip.mmsparams.api.json.JsonUtils;

import static org.junit.Assert.fail;

public class SerializableTest
{

    private final String clzPackage = "cz.uhk.dip.mmsparams.api.websocket.messages";

    @Test
    public void serializableTest() throws IllegalAccessException, InstantiationException
    {
        Class clazz = Serializable.class;
        Reflections reflections = new Reflections(clzPackage);
        Set<Class> subTypes = reflections.getSubTypesOf(clazz);

        for (Class c : subTypes)
        {
            if (c.isInterface())
                continue;

            if (c.isEnum())
            {
                continue;
            }

            if (!c.getPackage().getName().startsWith(clzPackage))
                continue;

            if (Modifier.isAbstract(c.getModifiers()))
                continue;

            if (!(c instanceof Serializable))
            {
                Assert.fail("Class " + c + " is not serializable");
            }

            try
            {
                System.out.println(c.getName());

                Object o = c.newInstance();
                String json = JsonUtils.toJson(o);
                Object o2 = JsonUtils.fromJson(json, c);

            }
            catch (Exception e)
            {
                System.out.println(c.getName());
                e.printStackTrace();
                fail();
            }
        }

    }
}
