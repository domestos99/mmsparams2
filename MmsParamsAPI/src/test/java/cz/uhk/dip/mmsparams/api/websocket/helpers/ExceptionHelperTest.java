package cz.uhk.dip.mmsparams.api.websocket.helpers;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.exceptions.MmsParamsExceptionBase;
import cz.uhk.dip.mmsparams.api.exceptions.SystemException;
import cz.uhk.dip.mmsparams.api.exceptions.TimeOutException;
import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ExceptionHelperTest
{

    @Test
    public void getStackTraceTest()
    {
        Exception ex = new Exception("Test exception");
        String s = ExceptionHelper.getStackTrace(ex);
        assertFalse(StringUtil.isEmptyOrNull(s));

    }

    @Test
    public void getStackTraceNullTest()
    {
        String s = ExceptionHelper.getStackTrace(null);
        assertTrue(StringUtil.isEmptyOrNull(s));
    }

    @Test
    public void generateExceptionTest()
    {
        GenericErrorResponseMessage msg =
                new MessageBuilder()
                        .build(GenericErrorResponseMessage.class);

        final TestErrorModel model = new TestErrorModel();

        model.setMessage("Test exception message");
        SystemException ex1 = new SystemException("Test exception");
        model.setException(ex1);
        msg.setTestErrorModel(model);

        Exception ex = ExceptionHelper.generateException(msg);

        assertFalse(StringUtil.isEmptyOrNull(ex.getMessage()));
        assertEquals(msg.getTestErrorModel().getException(), ex.getCause());
    }

    @Test
    public void generateExceptionNullTest()
    {
        Exception ex = ExceptionHelper.generateException(null);
        assertNotNull(ex);
    }

    @Test
    public void castExceptionTest()
    {
        TimeOutException te = new TimeOutException();
        MmsParamsExceptionBase result = ExceptionHelper.castException(te);
        assertNotNull(result);
        assertEquals(te.getClass(), result.getClass());
    }

    @Test
    public void castException_other_Test()
    {
        Exception te = new Exception();
        MmsParamsExceptionBase result = ExceptionHelper.castException(te);
        assertNotNull(result);
        assertEquals(SystemException.class, result.getClass());
    }


}
