package cz.uhk.dip.mmsparams.api.websocket.messages.errors;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.UnitTestBase;
import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.exceptions.TestErrorException;
import cz.uhk.dip.mmsparams.api.json.JsonUtils;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class GenericErrorResponseMessageTest extends UnitTestBase
{
    @Test
    public void serializeTest()
    {
        GenericErrorResponseMessage msg = MessageFactory.createGenericErrorResponseMessage("TAG", new TestErrorException("test ex"), GenericConstants.MESSAGE_ID, "my info");

        String json = JsonUtils.toJson(msg);
        assertFalse(StringUtil.isEmptyOrNull(json));

        GenericErrorResponseMessage msg2 = JsonUtils.fromJson(json, GenericErrorResponseMessage.class);

        assertNotNull(msg2);
        assertEquals(msg.getTestErrorModel().getMessage(), msg2.getTestErrorModel().getMessage());
//        assertEquals(msg.getTestErrorModel().getException(), msg2.getTestErrorModel().getException());
        assertEquals(msg.getTestErrorModel().getTestErrorType(), msg2.getTestErrorModel().getTestErrorType());


    }

}
