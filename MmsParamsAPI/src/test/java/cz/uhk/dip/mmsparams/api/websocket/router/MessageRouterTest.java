package cz.uhk.dip.mmsparams.api.websocket.router;

import org.junit.Assert;
import org.junit.Test;
import org.reflections.Reflections;

import java.util.Set;

import cz.uhk.dip.mmsparams.api.RandomObjectFiller;
import cz.uhk.dip.mmsparams.api.json.JsonUtils;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.listeners.IMessageReceiveSub;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.KeepAliveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.email.EmailReceiveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.TestErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsRecipientPhoneProfileRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.AcknowledgeIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.DeliveryIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotifyRespIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadOrigIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadRecIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendReqResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReportReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ReadReplyReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7SubmitResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneAllPartsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDisconnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class MessageRouterTest
{
    @Test
    public void allMessageTypesIncludedTest() throws Exception
    {
        RandomObjectFiller filler = new RandomObjectFiller();
        Reflections reflections = new Reflections("cz.uhk.dip.mmsparams.api.websocket.messages");

        Set<Class<? extends WebSocketMessageBase>> allClasses = reflections.getSubTypesOf(WebSocketMessageBase.class);
        for (Class<? extends WebSocketMessageBase> a : allClasses)
        {
            Class<WebSocketMessageBase> clazz = (Class<WebSocketMessageBase>) Class.forName(a.getName());

            if (SmscMessageBase.class.getSimpleName().equals(clazz.getSimpleName()))
                continue;

            if (EmptyMessage.class.getSimpleName().equals(clazz.getSimpleName()))
                continue;

            System.out.println(clazz);

            WebSocketMessageBase object =
                    new MessageBuilder()
                            .withNoTestId()
                            .withSenderKey("xxx")
                            .withRecipientKey("yyy")
                            .withMessageIdRandom()
                            .build(clazz);


            String json = JsonUtils.toJson(object);

            assertNotNull(json);
            assertNotEquals("", json);

            MessageRouter.process(new MockMessageReceiveSub(), json);
        }
    }

    @Test
    public void processNullTest()
    {
        assertNull(MessageRouter.process(new MockMessageReceiveSubNoFail(), null));
    }


    class MockMessageReceiveSubNoFail extends MockMessageReceiveSub
    {
        @Override
        public WebSocketMessageBase onReceiveUnknown(String msg)
        {
            return null;
        }
    }

    class MockMessageReceiveSub implements IMessageReceiveSub<WebSocketMessageBase>
    {

        @Override
        public WebSocketMessageBase onReceiveUnknown(String msg)
        {
            System.out.println("ERROR");
            System.out.println(msg);

            if ("{\"messageKey\":\"EMPTYMESSAGE\"}".equals(msg))
                return null;

            Assert.fail();

            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(PhoneListRequestMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(RegisterPhoneMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(EmptyMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(PhoneListResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmsSendPhoneRequestMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmsReceivePhoneMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmsReceivePhoneAllPartsMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmsSendPhoneResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmsSendDeliveryReportMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(MmsSendPhoneRequestMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(DevMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(MmsRecipientPhoneProfileRequestMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(GenericBooleanResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(GenericErrorResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SendConfResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(RegisterClientLibMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(NotificationIndResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(DeliveryIndResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(ReadOrigIndResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(AcknowledgeIndResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(RetrieveConfResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(ReadRecIndResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(NotifyRespIndResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(MM7DeliveryReqMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(MM7DeliveryReportReqMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(MM7ReadReplyReqMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmscConnectMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmscSendSmsMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmscDeliverSmMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmscDisconnectMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmscDeliveryReportMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmscConnectResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SmscSendSmsResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(UnLockPhoneMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(SendReqResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(MmscSendMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(LockPhoneMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(LockedPhonesListResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(LockedPhonesListRequestMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(MM7SubmitResponseMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(MmscAcquireRouteRequestMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(TestValidationMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(TestErrorMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(MM7ErrorMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(KeepAliveMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(TestResultMessage msg)
        {
            return null;
        }

        @Override
        public WebSocketMessageBase onReceive(EmailReceiveMessage msg)
        {
            return null;
        }
    }

}
