package cz.uhk.dip.mmsparams.clientlib;

import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

/**
 * ClientLib basic constants
 */
public class Constants
{
    private Constants()
    {
    }

    public static final int USE_DEFAULT_TIMEOUT = -1;

    public static final CustomTimeout DEFAULT_TIMEOUT = new CustomTimeout(USE_DEFAULT_TIMEOUT);
}
