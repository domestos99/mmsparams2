package cz.uhk.dip.mmsparams.clientlib;

import cz.uhk.dip.mmsparams.clientlib.communication.email.IEmailMessageService;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.IMmsService;
import cz.uhk.dip.mmsparams.clientlib.communication.mmsc.IMmscService;
import cz.uhk.dip.mmsparams.clientlib.communication.phones.IPhoneService;
import cz.uhk.dip.mmsparams.clientlib.communication.remoteconnect.IRemoteConnectService;
import cz.uhk.dip.mmsparams.clientlib.communication.sms.ISmsService;
import cz.uhk.dip.mmsparams.clientlib.communication.smsc.ISmscService;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfo;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationService;

/**
 * Main interface for creating tests
 */
public interface ITestInstance
{

    /**
     * Provides interface to Device service
     *
     * @return interface with services
     */
    IPhoneService Phones();

    /**
     * Provides interface to SMS service
     *
     * @return interface with services
     */
    ISmsService Sms();

    /**
     * Provides interface to MMS service
     *
     * @return interface with services
     */
    IMmsService Mms();

    /**
     * Provides interface to MMSC service
     *
     * @return interface with services
     */
    IMmscService Mmsc();

    /**
     * Provides interface to Smsc service
     *
     * @return interface with services
     */
    ISmscService Smsc();

    /**
     * Provides interface to Validation service
     *
     * @return interface with services
     */
    IValidationService Validation();

    /**
     * Returns current TestSettings
     *
     * @return interface with services
     */
    TestSettings getTestSettings();

    /**
     * Provider information about current test
     *
     * @return interface with services
     */
    ITestInfo TestInfo();

    /**
     * Return interface to Remote Connect phones
     *
     * @return interface with services
     */
    IRemoteConnectService RemoteConnect();


    /**
     * Closes test case. Disconnect from server.
     *
     * @return true if test was successful
     */
    boolean testFinished();

    /**
     * Closes test case with error. Disconnect from server.
     *
     * @param e test exception
     */
    void testFinishedWithError(Exception e);

    IEmailMessageService Email();
}
