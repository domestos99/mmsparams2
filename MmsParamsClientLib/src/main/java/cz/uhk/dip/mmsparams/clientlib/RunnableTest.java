package cz.uhk.dip.mmsparams.clientlib;

import javax.annotation.Nullable;

import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfo;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;

public abstract class RunnableTest
{
    private final String testName;
    private final String testDesc;

    public RunnableTest(String testName, String testDesc)
    {
        this.testName = testName;
        this.testDesc = testDesc;
    }

    public boolean runTest()
    {
        ClientLogFacade.clearAll();
        final TestSettings ts = init();

        ITestInstance test = null;
        try
        {
            test = TestInstance.initNewTest(ts, this.testName, this.testDesc);
            run(test);
            boolean result = test.testFinished();
            afterTestFinished(test.TestInfo());
            if (!result)
            {
                onTestFailed(test, test.TestInfo(), null);
            }
            return result;
        }
        catch (Exception e)
        {
            if (test != null)
            {
                test.testFinishedWithError(e);
                afterTestFinished(test.TestInfo());
                onTestFailed(test, test.TestInfo(), e);
            }
            else
            {
                onTestFailed(null, null, e);
            }
            return false;
        }
    }

    protected void onTestFailed(@Nullable ITestInstance test, @Nullable ITestInfo testInfo, @Nullable Exception exception)
    {
        // Override me
    }

    protected abstract TestSettings init();

    protected abstract void run(ITestInstance test) throws Exception;

    protected void afterTestFinished(ITestInfo testInfo)
    {

    }
}
