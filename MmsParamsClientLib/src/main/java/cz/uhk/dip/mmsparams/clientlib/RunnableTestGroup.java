package cz.uhk.dip.mmsparams.clientlib;

import cz.uhk.dip.mmsparams.api.utils.CommonUtils;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.TestGroupModel;
import cz.uhk.dip.mmsparams.clientlib.groups.TestGroupSingleton;

public abstract class RunnableTestGroup
{
    private final String testGroupName;
    private final String testGroupDesc;

    public RunnableTestGroup(String testGroupName, String testGroupDesc)
    {
        this.testGroupName = testGroupName;
        this.testGroupDesc = testGroupDesc;
    }

    protected abstract void runTests();

    protected void runTestGroup()
    {
        TestGroupModel testGroupModel = getTestGroupModel();
        TestGroupSingleton.getInstance().setTestGroupModel(testGroupModel);

        try
        {
            runTests();
        }
        finally
        {
            TestGroupSingleton.getInstance().clearTestGroup();
        }
    }

    public String getTestGroupName()
    {
        return testGroupName;
    }

    public String getTestGroupDesc()
    {
        return testGroupDesc;
    }

    protected TestGroupModel getTestGroupModel()
    {
        TestGroupModel testGroupModel = new TestGroupModel();
        testGroupModel.setTestGroupID(CommonUtils.getUUID());
        testGroupModel.setTestGroupName(this.testGroupName);
        testGroupModel.setTestGroupDesc(this.testGroupDesc);
        return testGroupModel;
    }
}
