package cz.uhk.dip.mmsparams.clientlib;

import java.io.Serializable;

public class TestDescription implements Serializable
{
    private String testName;
    private String testDescription;

    public TestDescription()
    {
    }

    public TestDescription(String testName, String testDescription)
    {
        this.testName = testName;
        this.testDescription = testDescription;
    }

    public String getTestName()
    {
        return testName;
    }

    public void setTestName(String testName)
    {
        this.testName = testName;
    }

    public String getTestDescription()
    {
        return testDescription;
    }

    public void setTestDescription(String testDescription)
    {
        this.testDescription = testDescription;
    }

    @Override
    public String toString()
    {
        return "TestDescription{" +
                "testName='" + testName + '\'' +
                ", testDescription='" + testDescription + '\'' +
                '}';
    }
}
