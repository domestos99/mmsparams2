package cz.uhk.dip.mmsparams.clientlib;


import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.interfaces.IClientKeyProvider;
import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.IMessageRegister;
import cz.uhk.dip.mmsparams.api.messages.MessageRegister;
import cz.uhk.dip.mmsparams.api.utils.ConsoleUtil;
import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.utils.TimeFormatter;
import cz.uhk.dip.mmsparams.clientlib.communication.ClientKeyProvider;
import cz.uhk.dip.mmsparams.clientlib.communication.TestIdProvider;
import cz.uhk.dip.mmsparams.clientlib.communication.clientlib.ClientLibService;
import cz.uhk.dip.mmsparams.clientlib.communication.clientlib.IClientLibService;
import cz.uhk.dip.mmsparams.clientlib.communication.email.EmailMessageService;
import cz.uhk.dip.mmsparams.clientlib.communication.email.IEmailMessageService;
import cz.uhk.dip.mmsparams.clientlib.communication.http.HttpUtils;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.IMmsService;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.MmsService;
import cz.uhk.dip.mmsparams.clientlib.communication.mmsc.IMmscService;
import cz.uhk.dip.mmsparams.clientlib.communication.mmsc.MmscService;
import cz.uhk.dip.mmsparams.clientlib.communication.phones.IPhoneService;
import cz.uhk.dip.mmsparams.clientlib.communication.phones.PhoneService;
import cz.uhk.dip.mmsparams.clientlib.communication.remoteconnect.IRemoteConnectService;
import cz.uhk.dip.mmsparams.clientlib.communication.remoteconnect.RemoteConnectService;
import cz.uhk.dip.mmsparams.clientlib.communication.sms.ISmsService;
import cz.uhk.dip.mmsparams.clientlib.communication.sms.SmsService;
import cz.uhk.dip.mmsparams.clientlib.communication.smsc.ISmscService;
import cz.uhk.dip.mmsparams.clientlib.communication.smsc.SmscService;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfo;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfoInternal;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.TestInfo;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketService;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.WebSocketHolder;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.WebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.WebSocketService;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.WebSocketWaiter2;
import cz.uhk.dip.mmsparams.clientlib.errors.ErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientServerLogger;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationService;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationService;
import cz.uhk.dip.mmsparams.clientlib.websocket.WebSocketReceiver;

/**
 * Default {@link ITestInstance} implementation
 */
public class TestInstance implements ITestInstance
{
    private TestInstance(TestSettings testSettings, TestDescription testDescription, ITestIdProvider testIdProvider, IWebSocketService webSocketObject, IPhoneService iPhoneService,
                         ISmsService iSmsService, IMmsService iMmsService,
                         ISmscService iSmscService, IMmscService iMmscService,
                         IClientLibService iClientLibService, IValidationService iValidationService, IRemoteConnectService iRemoteConnectService, IEmailMessageService iEmailMessageService, ITestInfoInternal testInfo, IErrorHandler iErrorHandler)
    {
        this.testSettings = testSettings;
        this.testDescription = testDescription;
        this.webSocketObject = webSocketObject;
        this.iPhoneService = iPhoneService;
        this.iSmsService = iSmsService;
        this.iMmsService = iMmsService;
        this.iMmscService = iMmscService;
        this.iSmscService = iSmscService;
        this.iClientLibService = iClientLibService;
        this.iValidationService = iValidationService;
        this.iRemoteConnectService = iRemoteConnectService;
        this.iEmailMessageService = iEmailMessageService;
        this.testInfo = testInfo;
        this.iErrorHandler = iErrorHandler;
        this.testIdProvider = testIdProvider;
        this.clientServerLogger = new ClientServerLogger(testIdProvider.getTestId());
        ClientLogFacade.addCustomLogger(this.clientServerLogger);

        setupUnhandledException();
    }

    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(TestInstance.class);

    private final TestDescription testDescription;
    private final IWebSocketService webSocketObject;
    private final IPhoneService iPhoneService;
    private final ISmsService iSmsService;
    private final IMmsService iMmsService;
    private final IMmscService iMmscService;
    private final ISmscService iSmscService;
    private final IValidationService iValidationService;
    private final IClientLibService iClientLibService;
    private final TestSettings testSettings;
    private final ITestInfoInternal testInfo;
    private final IErrorHandler iErrorHandler;
    private final IRemoteConnectService iRemoteConnectService;
    private final IEmailMessageService iEmailMessageService;
    private boolean testClosed = false;
    private boolean logSentToServer = false;
    private final ITestIdProvider testIdProvider;
    private final ClientServerLogger clientServerLogger;

    /**
     * Initializes new test and create WebSocket connection
     *
     * @param testSettings test settings for current test
     * @return created new test instance
     */
    @Deprecated
    public static TestInstance initNewTest(final TestSettings testSettings)
    {
        return initNewTest(testSettings, null, null);
    }

    /**
     * Initializes new test and create WebSocket connection
     *
     * @param testSettings test settings for current test
     * @param testName     user name for test
     * @return created new test instance
     */
    @Deprecated
    public static TestInstance initNewTest(final TestSettings testSettings, final String testName)
    {
        return initNewTest(testSettings, testName, null);
    }

    /**
     * Initializes new test and create WebSocket connection
     *
     * @param testSettings test settings for current test
     * @param testName     user name for test
     * @param testDesc     user description for test
     * @return created new test instance
     */
    public static TestInstance initNewTest(final TestSettings testSettings, final String testName, final String testDesc)
    {
        Preconditions.checkNotNull(testSettings, GenericConstants.TEST_SETTINGS);
        Preconditions.checkNotNull(testSettings.getServerAddressProvider(), GenericConstants.SERVER_ADDRESS_PROVIDER);
        Preconditions.isGreater(testSettings.getDefaultWaitSleepLoop(), 0, GenericConstants.DEFAULT_WAIT_SLEEP_LOOP);
        Preconditions.isGreater(testSettings.getDefaultTimeout(), 0, GenericConstants.LONG_TIMEOUT);
        Preconditions.checkStringLenghtMax(testName, 255, GenericConstants.TEXT_LENGHT);
        Preconditions.checkStringLenghtMax(testDesc, 255, GenericConstants.TEXT_LENGHT);

        ITestIdProvider testIdProvider = new TestIdProvider();
        IClientKeyProvider clientKeyProvider = new ClientKeyProvider();

        IErrorHandler iErrorHandler = new ErrorHandler();

        IMessageRegister messageRegister = new MessageRegister();

        WebSocketReceiver webSocketReceiver = new WebSocketReceiver(messageRegister, iErrorHandler);

        IWebSocketService webSocketObject = new WebSocketService(WebSocketHolder.createDefault(), webSocketReceiver, messageRegister,
                testIdProvider, clientKeyProvider, iErrorHandler);

        IWebSocketSender iWebSocketSender = new WebSocketSender(webSocketObject);

        iErrorHandler.setWebSocketSender(iWebSocketSender);

        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(messageRegister, testSettings, iErrorHandler);

        IPhoneService phoneService = new PhoneService(iWebSocketSender, iWebSocketWaiter);
        ISmsService smsService = new SmsService(iWebSocketSender, iWebSocketWaiter);

        IMmsService mmsService = new MmsService(iWebSocketSender, iWebSocketWaiter);

        IMmscService iMmscService = new MmscService(iWebSocketSender, iWebSocketWaiter);

        ISmscService smscService = new SmscService(iWebSocketSender, iWebSocketWaiter);

        IClientLibService gs = new ClientLibService(iWebSocketSender, iWebSocketWaiter);

        IValidationService iValidationService = new ValidationService(iWebSocketSender, iWebSocketWaiter);

        IRemoteConnectService iRemoteConnectService = new RemoteConnectService(iWebSocketSender, iWebSocketWaiter, smsService, smscService, phoneService, testSettings);

        IEmailMessageService iEmailMessageService = new EmailMessageService(iWebSocketSender, iWebSocketWaiter);

        TestDescription testDescription = new TestDescription(testName, testDesc);

        ITestInfoInternal iTestInfo = new TestInfo(messageRegister, getWSConnectSettings(testSettings), testIdProvider.getTestId(), clientKeyProvider.getClientKey(), testDescription);

        TestInstance ti = new TestInstance(testSettings, testDescription, testIdProvider, webSocketObject, phoneService, smsService, mmsService,
                smscService, iMmscService, gs, iValidationService, iRemoteConnectService, iEmailMessageService, iTestInfo, iErrorHandler);

        ti.startTest();

        return ti;
    }

    private void setupUnhandledException()
    {
        Thread.UncaughtExceptionHandler h = new Thread.UncaughtExceptionHandler()
        {
            public void uncaughtException(Thread th, Throwable ex)
            {
                handleUncaughtException(th, ex);
            }
        };
        Thread.currentThread().setUncaughtExceptionHandler(h);
    }

    private synchronized void handleUncaughtException(final Thread th, final Throwable ex)
    {
        this.testInfo.setTestResult(false);
        ClientLogFacade.logException(LOGGER, "Uncaught exception", ex);

        iErrorHandler.notifyNewUncaughtException(ExceptionHelper.castException(ex));

        testFinished(false);
    }

    /**
     * Provides interface with Phone services
     *
     * @return interface with services
     */
    @Override
    public IPhoneService Phones()
    {
        return iPhoneService;
    }

    /**
     * Provides interface with SMS services
     *
     * @return interface with services
     */
    @Override
    public ISmsService Sms()
    {
        return iSmsService;
    }

    /**
     * Provides interface with MMS services
     *
     * @return interface with services
     */
    @Override
    public IMmsService Mms()
    {
        return iMmsService;
    }

    /**
     * Provides interface with Mmsc services
     *
     * @return interface with services
     */
    @Override
    public IMmscService Mmsc()
    {
        return iMmscService;
    }

    /**
     * Provides interface with Smsc services
     *
     * @return interface with services
     */
    @Override
    public ISmscService Smsc()
    {
        return iSmscService;
    }

    /**
     * Provides interface with Validation services
     *
     * @return interface with services
     */
    @Override
    public IValidationService Validation()
    {
        return iValidationService;
    }

    /**
     * Current test settings
     *
     * @return current test settings
     */
    @Override
    public TestSettings getTestSettings()
    {
        return this.testSettings;
    }

    /**
     * Provides interface with information about current test
     *
     * @return interface with services
     */
    @Override
    public ITestInfo TestInfo()
    {
        return this.testInfo;
    }

    /**
     * Provides interface with services for phone Remote Connect
     *
     * @return interface with services
     */
    @Override
    public IRemoteConnectService RemoteConnect()
    {
        return this.iRemoteConnectService;
    }

    @Override
    public IEmailMessageService Email()
    {
        return this.iEmailMessageService;
    }

    private void startTest()
    {
        ClientLogFacade.logAction(LOGGER, "Starting test...");
        ClientLogFacade.logAction(LOGGER, "Name: " + this.testDescription.getTestName());
        ClientLogFacade.logAction(LOGGER, "Desc: " + this.testDescription.getTestDescription());
        ClientLogFacade.logAction(LOGGER, "TestID: " + this.testInfo.getTestId());
        ClientLogFacade.logAction(LOGGER, "ClientKey: " + this.testInfo.getClientKey());

        // Init web socket
        webSocketObject.init(getWSConnectSettings(testSettings));

        iClientLibService.registerClientLib(this.testDescription);

        ClientLogFacade.logAction(LOGGER, "Test started...");
    }

    private static WSConnectSettings getWSConnectSettings(TestSettings testSettings)
    {
        return new WSConnectSettings(testSettings.getServerAddressProvider(), testSettings.getUsername(), testSettings.getPassword());
    }

    /**
     * Closes current test. Disconnect WebSocket.
     */
    @Override
    public boolean testFinished()
    {
        iErrorHandler.checkErrorThrow();
        return testFinished(true);
    }

    private boolean testFinished(boolean success)
    {
        if (testClosed)
        {
            return this.testInfo.getTestResult();
        }

        this.testInfo.setTestResult(success);
        this.testInfo.testFinished();

        // Send test finished
        ClientLogFacade.logAction(LOGGER, "Test finishing...");
        ClientLogFacade.logAction(LOGGER, "Test duration: " + TimeFormatter.format(this.testInfo.getTestDuration()));

        if (success)
        {
            this.iClientLibService.sendTestFinishedOk(this.testInfo);
        }

        if (webSocketObject != null)
            webSocketObject.disconnect();

        if (success)
        {
            ClientLogFacade.logAction(LOGGER, "--------------END---------------");
            ClientLogFacade.logActionWithColor(LOGGER, "-----------Test OK--------------", ConsoleUtil.GREEN_BACKGROUND);
        }

        if (!logSentToServer)
        {
            sendLogToServer();
            this.logSentToServer = true;
        }

        this.testClosed = true;
        return success;
    }

    private void sendLogToServer()
    {
        try
        {
            HttpUtils.sendLogs(getWSConnectSettings(testSettings), this.clientServerLogger);
        }
        catch (Exception e)
        {
            ClientLogFacade.logWarning(LOGGER, "sendLogToServer", e);
        }
    }

    @Override
    public void testFinishedWithError(Exception e)
    {
        handleUncaughtException(Thread.currentThread(), e);
    }
}
