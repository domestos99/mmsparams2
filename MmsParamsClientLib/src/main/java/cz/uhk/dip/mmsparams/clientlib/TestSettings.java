package cz.uhk.dip.mmsparams.clientlib;

import cz.uhk.dip.mmsparams.api.http.ServerAddressProvider;

/**
 * Setting for {@link TestInstance}
 */
public class TestSettings
{
    private int defaultTimeout;
    private long defaultWaitSleepLoop;
    private String username;
    private String password;
    private final ServerAddressProvider serverAddressProvider;

    /**
     * @param serverAddressProvider Provider of URL of test server
     * @param longTimeout           Timeout limit for wait operations in seconds
     * @param username              username for login
     * @param password              password for login
     */
    public TestSettings(ServerAddressProvider serverAddressProvider, int longTimeout, String username, String password)
    {
        this(serverAddressProvider, longTimeout, 100, username, password);
    }

    /**
     * @param serverAddressProvider Provider of URL of test server
     * @param defaultTimeout        Timeout limit for wait operations in seconds
     * @param defaultWaitSleepLoop  Waiter loop time in milisecods
     * @param username              username for login
     * @param password              password for login
     */
    public TestSettings(ServerAddressProvider serverAddressProvider, int defaultTimeout, long defaultWaitSleepLoop, String username, String password)
    {
        this.defaultTimeout = defaultTimeout;
        this.defaultWaitSleepLoop = defaultWaitSleepLoop;
        this.serverAddressProvider = serverAddressProvider;
        this.username = username;
        this.password = password;
    }

    public int getDefaultTimeout()
    {
        return defaultTimeout;
    }

    public void setDefaultTimeout(int longTimeout)
    {
        this.defaultTimeout = defaultTimeout;
    }

    public long getDefaultWaitSleepLoop()
    {
        return defaultWaitSleepLoop;
    }

    public void setDefaultWaitSleepLoop(long defaultWaitSleepLoop)
    {
        this.defaultWaitSleepLoop = defaultWaitSleepLoop;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public ServerAddressProvider getServerAddressProvider()
    {
        return serverAddressProvider;
    }
}
