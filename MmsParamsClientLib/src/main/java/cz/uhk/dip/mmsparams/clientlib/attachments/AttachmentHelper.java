package cz.uhk.dip.mmsparams.clientlib.attachments;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.constants.MediaType;
import cz.uhk.dip.mmsparams.api.constants.MimeTypeMap;
import cz.uhk.dip.mmsparams.api.utils.FileHelper;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsAttachmentSendModel;

/**
 * Helper for loading attachments for MMS messages
 */
public class AttachmentHelper
{
    private AttachmentHelper()
    {
    }

    /**
     * Load image attachment for MMS message from file
     *
     * @param imgPath image path
     * @return image MMS attachment
     * @throws IOException loading file failed
     */
    public static MmsAttachmentSendModel loadImage(final String imgPath) throws IOException
    {
        Preconditions.checkNotNullOrEmpty(imgPath, GenericConstants.PATH);
        final File f = new File(imgPath);
        return loadImage(f);
    }

    /**
     * Load image attachment for MMS message from file
     *
     * @param imageFile image file
     * @return image MMS attachment
     * @throws IOException loading file failed
     */
    public static MmsAttachmentSendModel loadImage(final File imageFile) throws IOException
    {
        Preconditions.checkNotNull(imageFile, GenericConstants.FILE);
        return create(imageFile, MediaType.IMAGE);
    }

    /**
     * Load video attachment for MMS message from file
     *
     * @param videoPath video path
     * @return video MMS attachment
     * @throws IOException loading file failed
     */
    public static MmsAttachmentSendModel loadVideo(final String videoPath) throws IOException
    {
        Preconditions.checkNotNullOrEmpty(videoPath, GenericConstants.PATH);
        final File f = new File(videoPath);
        return loadVideo(f);
    }

    /**
     * Load video attachment for MMS message from file
     *
     * @param videoFile video file
     * @return video MMS attachment
     * @throws IOException loading file failed
     */
    public static MmsAttachmentSendModel loadVideo(final File videoFile) throws IOException
    {
        Preconditions.checkNotNull(videoFile, GenericConstants.FILE);
        return create(videoFile, MediaType.VIDEO);
    }

    /**
     * Load audio attachment for MMS message from file
     *
     * @param audioPath audio path
     * @return audio MMS attachment
     * @throws IOException loading file failed
     */
    public static MmsAttachmentSendModel loadAudio(final String audioPath) throws IOException
    {
        Preconditions.checkNotNullOrEmpty(audioPath, GenericConstants.PATH);
        final File f = new File(audioPath);
        return loadAudio(f);
    }

    /**
     * Load audio attachment for MMS message from file
     *
     * @param audioFile audio file
     * @return audio MMS attachment
     * @throws IOException loading file failed
     */
    public static MmsAttachmentSendModel loadAudio(final File audioFile) throws IOException
    {
        Preconditions.checkNotNull(audioFile, GenericConstants.FILE);
        return create(audioFile, MediaType.AUDIO);
    }

    /**
     * Load document attachment for MMS message from file
     *
     * @param documentPath document path
     * @return document MMS attachment
     * @throws IOException loading file failed
     */
    public static MmsAttachmentSendModel loadDocument(final String documentPath) throws IOException
    {
        Preconditions.checkNotNullOrEmpty(documentPath, GenericConstants.PATH);
        final File f = new File(documentPath);
        return loadDocument(f);
    }

    /**
     * Load document attachment for MMS message from file
     *
     * @param documentFile document file
     * @return document MMS attachment
     * @throws IOException loading file failed
     */
    public static MmsAttachmentSendModel loadDocument(final File documentFile) throws IOException
    {
        Preconditions.checkNotNull(documentFile, GenericConstants.FILE);
        return create(documentFile, MediaType.DOCUMENT);
    }

    private static MmsAttachmentSendModel create(final File file, final MediaType mediaType) throws IOException
    {
        Preconditions.checkNotNull(file, GenericConstants.FILE);
        byte[] bytes = Files.readAllBytes(file.toPath());

        Optional<String> extOpt = FileHelper.getExtension(file);
        String extension = Preconditions.checkOptionalFilled(extOpt, GenericConstants.FILE_EXTENSION);

        return new MmsAttachmentSendModel(file.getName(), extension, bytes, MimeTypeMap.getMimeType(extension), mediaType);
    }

}
