package cz.uhk.dip.mmsparams.clientlib.attachments;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.constants.MimeTypeMap;
import cz.uhk.dip.mmsparams.api.utils.FileHelper;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Attachment;

public class AttachmentHelperMM7
{
    private AttachmentHelperMM7()
    {
    }

    public static MM7Attachment create(final String imgPath, final String contentId) throws IOException
    {
        Preconditions.checkNotNullOrEmpty(imgPath, GenericConstants.PATH);
        final File f = new File(imgPath);
        return create(f, contentId);
    }

    public static MM7Attachment create(final File file, final String contentId) throws IOException
    {
        Preconditions.checkNotNull(file, GenericConstants.FILE);
        byte[] bytes = Files.readAllBytes(file.toPath());

        Optional<String> extOpt = FileHelper.getExtension(file);
        String extension = Preconditions.checkOptionalFilled(extOpt, GenericConstants.FILE_EXTENSION);

        MM7Attachment attach = new MM7Attachment();

        attach.setData(bytes);
        attach.setContentType(MimeTypeMap.getMimeType(extension));
        attach.setContentId(contentId);

        return attach;
    }
}
