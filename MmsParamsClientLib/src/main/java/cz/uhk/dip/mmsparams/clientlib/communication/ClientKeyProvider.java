package cz.uhk.dip.mmsparams.clientlib.communication;

import cz.uhk.dip.mmsparams.api.interfaces.IClientKeyProvider;
import cz.uhk.dip.mmsparams.clientlib.utils.MessageIDProvider;

/**
 * Provides client key
 */
public class ClientKeyProvider implements IClientKeyProvider
{
    private final String clientKey;

    public ClientKeyProvider()
    {
        this.clientKey = MessageIDProvider.generateSenderKey();
    }

    /**
     * Returns client key for current test
     *
     * @return client key
     */
    @Override
    public String getClientKey()
    {
        return clientKey;
    }
}
