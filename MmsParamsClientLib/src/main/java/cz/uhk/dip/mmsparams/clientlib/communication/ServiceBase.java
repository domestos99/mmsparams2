package cz.uhk.dip.mmsparams.clientlib.communication;


import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;

/**
 * Parent of all ClientLib services
 */
public abstract class ServiceBase
{
    protected final IWebSocketSender webSocketSender;
    protected final IWebSocketWaiter webSocketReceiver;

    public ServiceBase(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        this.webSocketSender = webSocketSender;
        this.webSocketReceiver = webSocketReceiver;
    }
}
