package cz.uhk.dip.mmsparams.clientlib.communication;

import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.websocket.MessageIdGenerator;

/**
 * Provides current test id
 */
public class TestIdProvider implements ITestIdProvider
{
    private final String testId;

    public TestIdProvider()
    {
        this.testId = MessageIdGenerator.getNext();
    }

    /**
     * Returns test ID of current test
     *
     * @return test id
     */
    @Override
    public String getTestId()
    {
        return testId;
    }
}
