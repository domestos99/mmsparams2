package cz.uhk.dip.mmsparams.clientlib.communication.clientlib;


import cz.uhk.dip.mmsparams.api.constants.AppVersion;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.registration.TestClientInfoModel;
import cz.uhk.dip.mmsparams.clientlib.TestDescription;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfoInternal;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.groups.TestGroupSingleton;
import cz.uhk.dip.mmsparams.clientlib.utils.ComputerInfoHelper;

public class ClientLibService extends ServiceBase implements IClientLibService
{
    public ClientLibService(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public MessageId registerClientLib(final TestDescription testDescription)
    {
        RegisterClientLibMessage msg = MessageFactory.createServerRecipient(RegisterClientLibMessage.class);
        ClientInfo gi = new ClientInfo();

        gi.setClientKey(webSocketSender.getSenderKey());
        gi.setComputerName(ComputerInfoHelper.getComputerName());
        gi.setCurrentUserName(ComputerInfoHelper.getCurrentUserName());
        msg.setClientInfo(gi);

        TestClientInfoModel testClientInfoModel = new TestClientInfoModel();
        testClientInfoModel.setTestName(testDescription.getTestName());
        testClientInfoModel.setTestDesc(testDescription.getTestDescription());
        msg.setTestClientInfoModel(testClientInfoModel);
        msg.setTestGroupModel(TestGroupSingleton.getInstance().getTestGroupModel());

        msg.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);

        MessageId reqId = this.webSocketSender.sendMessage(msg);
        webSocketReceiver.waitForGenericBooleanResponseMessage(reqId);
        return reqId;
    }

    @Override
    public MessageId sendTestFinishedOk(ITestInfoInternal testInfo)
    {
        TestResultMessage msg = MessageFactory.createServerRecipient(TestResultMessage.class);
        MessageId reqId = this.webSocketSender.sendMessage(msg);
        webSocketReceiver.waitForGenericBooleanResponseMessage(reqId);
        return reqId;
    }
}
