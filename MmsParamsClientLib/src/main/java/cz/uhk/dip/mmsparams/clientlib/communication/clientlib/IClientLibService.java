package cz.uhk.dip.mmsparams.clientlib.communication.clientlib;

import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.clientlib.TestDescription;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfoInternal;

public interface IClientLibService
{
    /**
     * Register client and new test on the server
     *
     * @param testDescription current test name and description
     * @return send message id
     */
    MessageId registerClientLib(final TestDescription testDescription);

    MessageId sendTestFinishedOk(ITestInfoInternal testInfo);
}
