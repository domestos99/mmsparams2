package cz.uhk.dip.mmsparams.clientlib.communication.email;

import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;

public class EmailMessageService extends ServiceBase implements IEmailMessageService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(EmailMessageService.class);

    private final IEmailMessageWait iEmailMessageWait;

    public EmailMessageService(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
        this.iEmailMessageWait = new EmailMessageWait(webSocketSender, webSocketReceiver);
    }

    @Override
    public IEmailMessageWait waitFor()
    {
        return this.iEmailMessageWait;
    }
}
