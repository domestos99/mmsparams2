package cz.uhk.dip.mmsparams.clientlib.communication.email;

import java.io.IOException;

import javax.mail.MessagingException;

import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.messages.email.EmailReceiveMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.email.ReceiveEmailMessageModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;
import cz.uhk.dip.mmsparams.email.receive.ReceiveEmailConfig;
import cz.uhk.dip.mmsparams.email.receive.ReceiveEmailService;

public class EmailMessageWait extends ServiceBase implements IEmailMessageWait
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(EmailMessageWait.class);

    public EmailMessageWait(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public ReceiveEmailMessageModel anyEmailReceive(ReceiveEmailConfig receiveEmailConfig, CustomTimeout customTimeout) throws IOException, MessagingException
    {
        ClientLogFacade.logAction(LOGGER, "Waiting for Email receive");

        ReceiveEmailMessageModel result = ReceiveEmailService.receiveEmail(receiveEmailConfig);

        EmailReceiveMessage request = MessageFactory.createServerRecipient(EmailReceiveMessage.class);

        request.setReceiveEmailMessageModel(result);

        MmsSendMessageId messageID = new MmsSendMessageId(webSocketSender.sendMessage(request));
        webSocketReceiver.waitForGenericBooleanResponseMessage(messageID);


        return result;
    }


}
