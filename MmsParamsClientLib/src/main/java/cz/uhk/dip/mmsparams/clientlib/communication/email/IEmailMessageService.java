package cz.uhk.dip.mmsparams.clientlib.communication.email;

public interface IEmailMessageService
{
    IEmailMessageWait waitFor();
}
