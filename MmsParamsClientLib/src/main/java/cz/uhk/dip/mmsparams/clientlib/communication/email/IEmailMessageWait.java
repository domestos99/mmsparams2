package cz.uhk.dip.mmsparams.clientlib.communication.email;

import java.io.IOException;

import javax.mail.MessagingException;

import cz.uhk.dip.mmsparams.api.websocket.model.email.ReceiveEmailMessageModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.email.receive.ReceiveEmailConfig;

public interface IEmailMessageWait
{
    default ReceiveEmailMessageModel anyEmailReceive(ReceiveEmailConfig receiveEmailConfig) throws IOException, MessagingException
    {
        return anyEmailReceive(receiveEmailConfig, Constants.DEFAULT_TIMEOUT);
    }

    ReceiveEmailMessageModel anyEmailReceive(ReceiveEmailConfig receiveEmailConfig, CustomTimeout customTimeout) throws IOException, MessagingException;
}
