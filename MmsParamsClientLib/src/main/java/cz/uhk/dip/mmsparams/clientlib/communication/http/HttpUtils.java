package cz.uhk.dip.mmsparams.clientlib.communication.http;

import cz.uhk.dip.mmsparams.api.http.HttpPostUtil;
import cz.uhk.dip.mmsparams.api.http.RestApiConstants;
import cz.uhk.dip.mmsparams.api.http.TaskResult;
import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.http.auth.AuthConstants;
import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.http.auth.JwtResponse;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.logging.APILoggerFactory;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.validation.JwtResponseValidator;
import cz.uhk.dip.mmsparams.api.web.model.ClientServerLogModel;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientServerLogger;

public class HttpUtils
{
    private static final ILogger LOGGER = APILoggerFactory.getLogger(HttpUtils.class);

    private HttpUtils()
    {
    }

    public static JwtResponse getAuthToken(final WSConnectSettings wsConnectSettings)
    {
        final String url = wsConnectSettings.getServerAddressProvider().getServerAddress() + AuthConstants.AUTH_ADDRESS;
        String request = JsonUtilsSafe.toJson(new JwtRequest(wsConnectSettings.getUsername(), wsConnectSettings.getPassword()));
        final TaskResult<String> response = HttpPostUtil.postData(url, request);

        if (response.hasError())
        {
            ClientLogFacade.logException(LOGGER, "Invalid response form server while authenticating", response.getError());
            throw ExceptionHelper.castException(response.getError());
        }
        return JsonUtilsSafe.fromJson(response.getResult(), JwtResponse.class);
    }

    public static boolean sendLogs(WSConnectSettings wsConnectSettings, ClientServerLogger clientServerLogger)
    {
        final JwtResponse token = HttpUtils.getAuthToken(wsConnectSettings);

        if (!JwtResponseValidator.validate(token))
        {
            return false;
        }

        ClientServerLogModel model = clientServerLogger.getClientServerLogModel();

        final String url = wsConnectSettings.getServerAddressProvider().getServerAddress() + RestApiConstants.CLIENT_LOGS;

        String request = JsonUtilsSafe.toJson(model);

        final TaskResult<String> response = HttpPostUtil.postData(url, token, request);

        if (response.hasError())
        {
            ClientLogFacade.logException(LOGGER, "Failed to send Client Log", response.getError());
            throw ExceptionHelper.castException(response.getError());
        }
        return true;
    }


}
