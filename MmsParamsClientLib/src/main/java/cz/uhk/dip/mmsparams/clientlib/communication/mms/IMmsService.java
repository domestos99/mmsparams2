package cz.uhk.dip.mmsparams.clientlib.communication.mms;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.communication.profiles.IProfileService;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;

public interface IMmsService
{
    /**
     * Send MMS message request to Phone device
     *
     * @param mms         mms to send
     * @param phoneSender phone to send mms from
     * @return mms send message id
     */
    MmsSendMessageId sendMms(MmsSendModel mms, PhoneInfoModel phoneSender);

    /**
     * Provides interface to Wait operations
     *
     * @return interface with services
     */
    IMmsServiceWait waitFor();

    /**
     * Provides interface to MustTimeout operations
     *
     * @return interface with services
     */
    IMmsServiceMustTimeout mustTimeoutFor();

    /**
     * Provides interface to Phone profile service
     *
     * @return interface with services
     */
    IProfileService Profile();
}
