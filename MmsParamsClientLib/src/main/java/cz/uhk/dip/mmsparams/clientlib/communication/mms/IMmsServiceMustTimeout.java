package cz.uhk.dip.mmsparams.clientlib.communication.mms;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;

public interface IMmsServiceMustTimeout
{
    /**
     * Wait whether SendConf message will not be received by test.
     * Default test timeout is used
     *
     * @param mmsSendReqId sent MMS request id
     * @return true if nothing received
     */
    default boolean mmsConf(MmsSendMessageId mmsSendReqId)
    {
        return mmsConf(mmsSendReqId, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether SendConf message will not be received by test.
     * Specific timeout is used
     *
     * @param mmsSendReqId sent MMS request id
     * @param timeout      specific timeout
     * @return true if nothing received
     */
    boolean mmsConf(MmsSendMessageId mmsSendReqId, CustomTimeout timeout);

    /**
     * Wait whether NotificationInd message will not be received by test.
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return true if nothing received
     */
    default boolean notificationInd(PhoneInfoModel phoneRecipient)
    {
        return notificationInd(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether NotificationInd message will not be received by test.
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return true if nothing received
     */
    boolean notificationInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout);

    /**
     * Wait whether RetrieveConf message will not be received by test.
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return true if nothing received
     */
    default boolean retrieveConf(PhoneInfoModel phoneRecipient)
    {
        return retrieveConf(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether RetrieveConf message will not be received by test.
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return true if nothing received
     */
    boolean retrieveConf(PhoneInfoModel phoneRecipient, CustomTimeout timeout);


    /**
     * Wait whether NotifyRespInd message will not be received by test.
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return true if nothing received
     */
    default boolean notifyRespInd(PhoneInfoModel phoneRecipient)
    {
        return notifyRespInd(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether NotifyRespInd message will not be received by test.
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return true if nothing received
     */
    boolean notifyRespInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout);


    /**
     * Wait whether DeliveryInd message will not be received by test.
     * Default test timeout is used
     *
     * @param phoneSender sender phone instance
     * @return true if nothing received
     */
    default boolean deliveryInd(PhoneInfoModel phoneSender)
    {
        return deliveryInd(phoneSender, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether DeliveryInd message will not be received by test.
     * Specific timeout is used
     *
     * @param phoneSender sender phone instance
     * @param timeout     specific timeout
     * @return true if nothing received
     */
    boolean deliveryInd(PhoneInfoModel phoneSender, CustomTimeout timeout);

    /**
     * Wait whether AcknowledgeInd message will not be received by test.
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return true if nothing received
     */
    default boolean acknowledgeInd(PhoneInfoModel phoneRecipient)
    {
        return acknowledgeInd(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether AcknowledgeInd message will not be received by test.
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return true if nothing received
     */
    boolean acknowledgeInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout);

    /**
     * Wait whether ReadOrigInd message will not be received by test.
     * Default test timeout is used
     *
     * @param phoneSender sender phone instance
     * @return true if nothing received
     */
    default boolean readOrigInd(PhoneInfoModel phoneSender)
    {
        return readOrigInd(phoneSender, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether ReadOrigInd message will not be received by test.
     * Specific timeout is used
     *
     * @param phoneSender sender phone instance
     * @param timeout     specific timeout
     * @return true if nothing received
     */
    boolean readOrigInd(PhoneInfoModel phoneSender, CustomTimeout timeout);

    /**
     * Wait whether ReadRecInd message will not be received by test.
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return true if nothing received
     */
    default boolean readRecInd(PhoneInfoModel phoneRecipient)
    {
        return readRecInd(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether ReadRecInd message will not be received by test.
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return true if nothing received
     */
    boolean readRecInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout);


}
