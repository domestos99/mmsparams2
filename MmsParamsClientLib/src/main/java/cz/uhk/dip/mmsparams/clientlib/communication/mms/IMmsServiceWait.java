package cz.uhk.dip.mmsparams.clientlib.communication.mms;

import cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait.IAcknowledgeIndWait;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait.IDeliveryIndWait;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait.IMmsSendConfWait;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait.IMmsSendReqWait;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait.INotificationIndWait;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait.INotifyRespIndWait;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait.IReadOrigIndWait;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait.IReadRecIndWait;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait.IRetrieveConfWait;

public interface IMmsServiceWait extends IMmsSendReqWait, IMmsSendConfWait, IAcknowledgeIndWait,
        IDeliveryIndWait, INotificationIndWait, INotifyRespIndWait, IRetrieveConfWait,
        IReadOrigIndWait, IReadRecIndWait
{

}
