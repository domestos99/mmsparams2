package cz.uhk.dip.mmsparams.clientlib.communication.mms;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.profiles.IProfileService;
import cz.uhk.dip.mmsparams.clientlib.communication.profiles.ProfileService;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.model.MmsSendModelValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.PhoneInfoModelValidator;

public class MmsService extends ServiceBase implements IMmsService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(MmsService.class);

    private final IMmsServiceWait iMmsServiceWait;
    private final IMmsServiceMustTimeout iMmsServiceMustTimeout;
    private final IProfileService iProfileService;

    public MmsService(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
        iMmsServiceWait = new MmsServiceWait(webSocketSender, webSocketReceiver);
        this.iMmsServiceMustTimeout = new MmsServiceMustTimeout(webSocketSender, webSocketReceiver);
        this.iProfileService = new ProfileService(webSocketSender, webSocketReceiver);
    }

    @Override
    public MmsSendMessageId sendMms(MmsSendModel mms, PhoneInfoModel phoneSender)
    {
        MmsSendModelValidator.validate(mms);
        PhoneInfoModelValidator.validate(phoneSender, GenericConstants.PHONE_SENDER);

        ClientLogFacade.logAction(LOGGER, "Sending MMS");
        MmsSendPhoneRequestMessage request = MessageFactory.create(MmsSendPhoneRequestMessage.class);
        request.setMmsSendModel(mms);
        request.setRecipientKey(phoneSender.getPhoneKey());

        MmsSendMessageId messageID = new MmsSendMessageId(webSocketSender.sendMessage(request));
        webSocketReceiver.waitForGenericBooleanResponseMessage(messageID);
        return messageID;
    }

    @Override
    public IMmsServiceWait waitFor()
    {
        return this.iMmsServiceWait;
    }

    @Override
    public IMmsServiceMustTimeout mustTimeoutFor()
    {
        return this.iMmsServiceMustTimeout;
    }

    @Override
    public IProfileService Profile()
    {
        return this.iProfileService;
    }


}
