package cz.uhk.dip.mmsparams.clientlib.communication.mms;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.AcknowledgeIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.DeliveryIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotifyRespIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadOrigIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadRecIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.model.CustomTimeoutValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.MessageIdValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.PhoneInfoModelValidator;

public class MmsServiceMustTimeout extends ServiceBase implements IMmsServiceMustTimeout
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(MmsServiceMustTimeout.class);

    MmsServiceMustTimeout(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public boolean mmsConf(MmsSendMessageId mmsSendReqId, CustomTimeout timeout)
    {
        MessageIdValidator.validate(mmsSendReqId);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for MMS Conf - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(mmsSendReqId, SendConfResponseMessage.class, timeout.getTimeoutSeconds());
    }

    @Override
    public boolean notificationInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MMS NotificationInd - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(phoneRecipient, NotificationIndResponseMessage.class, timeout.getTimeoutSeconds());
    }

    @Override
    public boolean retrieveConf(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MMS RetrieveConf - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(phoneRecipient, RetrieveConfResponseMessage.class, timeout.getTimeoutSeconds());
    }

    @Override
    public boolean notifyRespInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MMS NotifyRespInd - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(phoneRecipient, NotifyRespIndResponseMessage.class, timeout.getTimeoutSeconds());
    }

    @Override
    public boolean deliveryInd(PhoneInfoModel phoneSender, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneSender, GenericConstants.PHONE_SENDER);
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MMS DeliveryInd - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(phoneSender, DeliveryIndResponseMessage.class, timeout.getTimeoutSeconds());
    }

    @Override
    public boolean acknowledgeInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MMS AcknowledgeInd - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(phoneRecipient, AcknowledgeIndResponseMessage.class, timeout.getTimeoutSeconds());
    }

    @Override
    public boolean readOrigInd(PhoneInfoModel phoneSender, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneSender, GenericConstants.PHONE_SENDER);
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MMS ReadOrigInd - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(phoneSender, ReadOrigIndResponseMessage.class, timeout.getTimeoutSeconds());
    }

    @Override
    public boolean readRecInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MMS ReadRecInd - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(phoneRecipient, ReadRecIndResponseMessage.class, timeout.getTimeoutSeconds());
    }
}
