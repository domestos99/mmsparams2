package cz.uhk.dip.mmsparams.clientlib.communication.mms;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.AcknowledgeIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.DeliveryIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotifyRespIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadOrigIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadRecIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendReqResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.AcknowledgeIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.DeliveryIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotificationIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotifyRespIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.ReadOrigIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.ReadRecIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.model.CustomTimeoutValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.MessageIdValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.PhoneInfoModelValidator;

public class MmsServiceWait extends ServiceBase implements IMmsServiceWait
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(MmsServiceWait.class);

    MmsServiceWait(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public List<SendReqModel> anySendReq(PhoneInfoModel phoneSender, int expectedCount, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneSender, GenericConstants.PHONE_SENDER);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for SendReq");
        List<SendReqResponseMessage> res = webSocketReceiver.waitForResponseMessage(phoneSender, SendReqResponseMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<SendReqModel> result = new ArrayList<>();

        for (SendReqResponseMessage rr : res)
        {
            result.add(rr.getSendReqModel());
        }

        return result;
    }

    @Override
    public SendConfModel anyMmsConf(MmsSendMessageId mmsSendReqId, CustomTimeout timeout)
    {
        MessageIdValidator.validate(mmsSendReqId);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for SendConf");
        SendConfResponseMessage res = webSocketReceiver.waitForResponseMessage(mmsSendReqId, SendConfResponseMessage.class, timeout.getTimeoutSeconds());
        return res.getSendConfModel();
    }

    @Override
    public List<NotificationIndModel> anyNotificationInd(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for NotificationInd");
        List<NotificationIndResponseMessage> res = webSocketReceiver.waitForResponseMessage(phoneRecipient, NotificationIndResponseMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<NotificationIndModel> result = new ArrayList<>();

        for (NotificationIndResponseMessage rr : res)
        {
            result.add(rr.getNotificationIndModel());
        }

        return result;
    }

    @Override
    public List<RetrieveConfModel> anyRetrieveConf(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for RetrieveConf");
        List<RetrieveConfResponseMessage> res = webSocketReceiver.waitForResponseMessage(phoneRecipient, RetrieveConfResponseMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<RetrieveConfModel> result = new ArrayList<>();

        for (RetrieveConfResponseMessage rr : res)
        {
            result.add(rr.getRetrieveConfModel());
        }

        return result;
    }

    @Override
    public List<NotifyRespIndModel> anyNotifyRespInd(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for NotifyRespInd");
        List<NotifyRespIndResponseMessage> res = webSocketReceiver.waitForResponseMessage(phoneRecipient, NotifyRespIndResponseMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<NotifyRespIndModel> result = new ArrayList<>();

        for (NotifyRespIndResponseMessage rr : res)
        {
            result.add(rr.getNotifyRespIndModel());
        }

        return result;
    }

    @Override
    public List<DeliveryIndModel> anyDeliveryInd(PhoneInfoModel phoneSender, int expectedCount, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneSender, GenericConstants.PHONE_SENDER);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for DeliveryInd");
        List<DeliveryIndResponseMessage> res = webSocketReceiver.waitForResponseMessage(phoneSender, DeliveryIndResponseMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<DeliveryIndModel> result = new ArrayList<>();
        for (DeliveryIndResponseMessage rr : res)
        {
            result.add(rr.getDeliveryIndModel());
        }
        return result;
    }

    @Override
    public List<AcknowledgeIndModel> anyAcknowledgeInd(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for AcknowledgeInd");
        List<AcknowledgeIndResponseMessage> res = webSocketReceiver.waitForResponseMessage(phoneRecipient, AcknowledgeIndResponseMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<AcknowledgeIndModel> result = new ArrayList<>();

        for (AcknowledgeIndResponseMessage rr : res)
        {
            result.add(rr.getAcknowledgeIndModel());
        }

        return result;
    }

    @Override
    public List<ReadOrigIndModel> anyReadOrigInd(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for ReadOrigInd");
        List<ReadOrigIndResponseMessage> res = webSocketReceiver.waitForResponseMessage(phoneRecipient, ReadOrigIndResponseMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<ReadOrigIndModel> result = new ArrayList<>();

        for (ReadOrigIndResponseMessage rr : res)
        {
            result.add(rr.getReadOrigIndModel());
        }

        return result;
    }

    @Override
    public ReadRecIndModel anyReadRecInd(MmsSendMessageId mmsSendReqId, CustomTimeout timeout)
    {
        MessageIdValidator.validate(mmsSendReqId);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for ReadRecInd");
        ReadRecIndResponseMessage res = webSocketReceiver.waitForResponseMessage(mmsSendReqId, ReadRecIndResponseMessage.class, timeout.getTimeoutSeconds());
        return res.getReadRecIndModel();
    }
}
