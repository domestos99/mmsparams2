package cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.AcknowledgeIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface IAcknowledgeIndWait
{
    /**
     * Wait for AcknowledgeInd will be received
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return Received AcknowledgeInd
     */
    default AcknowledgeIndModel anyAcknowledgeInd(PhoneInfoModel phoneRecipient)
    {
        return anyAcknowledgeInd(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for AcknowledgeInd will be received
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return Received AcknowledgeInd
     */
    default AcknowledgeIndModel anyAcknowledgeInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        return anyAcknowledgeInd(phoneRecipient, 1, timeout).get(0);
    }

    /**
     * Wait for specific number or AcknowledgeInd PDUs received
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of PDUs expected
     * @return Received AcknowledgeInd
     */
    default List<AcknowledgeIndModel> anyAcknowledgeInd(PhoneInfoModel phoneRecipient, int expectedCount)
    {
        return anyAcknowledgeInd(phoneRecipient, expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number or AcknowledgeInd PDUs received
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of PDUs expected
     * @param timeout        specific timeout
     * @return Received AcknowledgeInd
     */
    List<AcknowledgeIndModel> anyAcknowledgeInd(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout);
}
