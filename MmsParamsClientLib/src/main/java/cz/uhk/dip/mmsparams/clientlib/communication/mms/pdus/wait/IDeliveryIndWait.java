package cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.DeliveryIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface IDeliveryIndWait
{
    /**
     * Wait for NotifyRespInd will be received
     * Default test timeout is used
     *
     * @param phoneSender sender phone instance
     * @return Received DeliveryInd
     */
    default DeliveryIndModel anyDeliveryInd(PhoneInfoModel phoneSender)
    {
        return anyDeliveryInd(phoneSender, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for DeliveryInd will be received
     * Specific timeout is used
     *
     * @param phoneSender sender phone instance
     * @param timeout     specific timeout
     * @return Received DeliveryInd
     */
    default DeliveryIndModel anyDeliveryInd(PhoneInfoModel phoneSender, CustomTimeout timeout)
    {
        return anyDeliveryInd(phoneSender, 1, timeout).get(0);
    }

    /**
     * Wait for specific number or DeliveryInd PDUs received
     * Default test timeout is used
     *
     * @param phoneSender   sender phone instance
     * @param expectedCount number of PDUs expected
     * @return Received DeliveryInd
     */
    default List<DeliveryIndModel> anyDeliveryInd(PhoneInfoModel phoneSender, int expectedCount)
    {
        return anyDeliveryInd(phoneSender, expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number or DeliveryInd PDUs received
     * Specific timeout is used
     *
     * @param phoneSender   sender phone instance
     * @param expectedCount number of PDUs expected
     * @param timeout       specific timeout
     * @return Received DeliveryInd
     */
    List<DeliveryIndModel> anyDeliveryInd(PhoneInfoModel phoneSender, int expectedCount, CustomTimeout timeout);
}
