package cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;

public interface IMmsSendConfWait
{
    /**
     * Wait for SendConf will be received
     * Default test timeout is used
     *
     * @param mmsSendReqId MMS send request message ID
     * @return Received SendConf
     */
    default SendConfModel anyMmsConf(MmsSendMessageId mmsSendReqId)
    {
        return anyMmsConf(mmsSendReqId, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for SendConf will be received
     * Specific timeout is used
     *
     * @param mmsSendReqId MMS send request message ID
     * @param timeout      specific timeout
     * @return Received SendConf
     */
    SendConfModel anyMmsConf(MmsSendMessageId mmsSendReqId, CustomTimeout timeout);
}
