package cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface IMmsSendReqWait
{
    /**
     * Wait for SendReq will be received
     * Default test timeout is used
     *
     * @param phoneSender sender phone instance
     * @return Send SendReq
     */
    default SendReqModel anySendReq(PhoneInfoModel phoneSender)
    {
        return anySendReq(phoneSender, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for SendReq will be received
     * Specific timeout is used
     *
     * @param phoneSender sender phone instance
     * @param timeout     specific timeout
     * @return Send SendReq
     */
    default SendReqModel anySendReq(PhoneInfoModel phoneSender, CustomTimeout timeout)
    {
        return anySendReq(phoneSender, 1, timeout).get(0);
    }

    /**
     * Wait for specific number or SendReq PDUs received
     * Default test timeout is used
     *
     * @param phoneSender   sender phone instance
     * @param expectedCount number of PDUs expected
     * @return Send SendReq
     */
    default List<SendReqModel> anySendReq(PhoneInfoModel phoneSender, int expectedCount)
    {
        return anySendReq(phoneSender, expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number or SendReq PDUs received
     * Specific timeout is used
     *
     * @param phoneSender   sender phone instance
     * @param expectedCount number of PDUs expected
     * @param timeout       specific timeout
     * @return Send SendReq
     */
    List<SendReqModel> anySendReq(PhoneInfoModel phoneSender, int expectedCount, CustomTimeout timeout);
}
