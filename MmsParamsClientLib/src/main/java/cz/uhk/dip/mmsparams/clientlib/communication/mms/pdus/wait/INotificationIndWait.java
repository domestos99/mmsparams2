package cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotificationIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface INotificationIndWait
{
    /**
     * Wait for NotificationInd will be received
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return Received NotificationInd
     */
    default NotificationIndModel anyNotificationInd(PhoneInfoModel phoneRecipient)
    {
        return anyNotificationInd(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for NotificationInd will be received
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return Received NotificationInd
     */
    default NotificationIndModel anyNotificationInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        return anyNotificationInd(phoneRecipient, 1, timeout).get(0);
    }

    /**
     * Wait for specific number or NotificationInd PDUs received
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of PDUs expected
     * @return Received NotificationInd
     */
    default List<NotificationIndModel> anyNotificationInd(PhoneInfoModel phoneRecipient, int expectedCount)
    {
        return anyNotificationInd(phoneRecipient, expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number or NotificationInd PDUs received
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of PDUs expected
     * @param timeout        specific timeout
     * @return Received NotificationInd
     */
    List<NotificationIndModel> anyNotificationInd(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout);
}
