package cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotifyRespIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface INotifyRespIndWait
{
    /**
     * Wait for NotifyRespInd will be received
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return Received NotifyRespInd
     */
    default NotifyRespIndModel anyNotifyRespInd(PhoneInfoModel phoneRecipient)
    {
        return anyNotifyRespInd(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for NotifyRespInd will be received
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return Received NotifyRespInd
     */
    default NotifyRespIndModel anyNotifyRespInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        return anyNotifyRespInd(phoneRecipient, 1, timeout).get(0);
    }

    /**
     * Wait for specific number or NotifyRespInd PDUs received
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of PDUs expected
     * @return Received NotifyRespInd
     */
    default List<NotifyRespIndModel> anyNotifyRespInd(PhoneInfoModel phoneRecipient, int expectedCount)
    {
        return anyNotifyRespInd(phoneRecipient, expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number or NotifyRespInd PDUs received
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of PDUs expected
     * @param timeout        specific timeout
     * @return Received NotifyRespInd
     */
    List<NotifyRespIndModel> anyNotifyRespInd(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout);
}
