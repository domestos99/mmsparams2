package cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.ReadOrigIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface IReadOrigIndWait
{
    /**
     * Wait for ReadOrigInd will be received
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return Received ReadOrigInd
     */
    default ReadOrigIndModel anyReadOrigInd(PhoneInfoModel phoneRecipient)
    {
        return anyReadOrigInd(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for ReadOrigInd will be received
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return Received ReadOrigInd
     */
    default ReadOrigIndModel anyReadOrigInd(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        return anyReadOrigInd(phoneRecipient, 1, timeout).get(0);
    }

    /**
     * Wait for specific number or ReadOrigInd PDUs received
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of PDUs expected
     * @return Received ReadOrigInd
     */
    default List<ReadOrigIndModel> anyReadOrigInd(PhoneInfoModel phoneRecipient, int expectedCount)
    {
        return anyReadOrigInd(phoneRecipient, expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number or ReadOrigInd PDUs received
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of PDUs expected
     * @param timeout        specific timeout
     * @return Received ReadOrigInd
     */
    List<ReadOrigIndModel> anyReadOrigInd(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout);
}
