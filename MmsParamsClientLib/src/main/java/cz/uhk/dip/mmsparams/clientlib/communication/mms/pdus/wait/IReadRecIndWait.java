package cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.ReadRecIndModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;

public interface IReadRecIndWait
{
    /**
     * Wait for ReadRecInd will be received
     * Default test timeout is used
     *
     * @param mmsSendReqId MMS send request message ID
     * @return Received ReadRecInd
     */
    default ReadRecIndModel anyReadRecInd(MmsSendMessageId mmsSendReqId)
    {
        return anyReadRecInd(mmsSendReqId, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for ReadRecInd will be received
     * Specific timeout is used
     *
     * @param mmsSendReqId MMS send request message ID
     * @param timeout      specific timeout
     * @return Received ReadRecInd
     */
    ReadRecIndModel anyReadRecInd(MmsSendMessageId mmsSendReqId, CustomTimeout timeout);
}
