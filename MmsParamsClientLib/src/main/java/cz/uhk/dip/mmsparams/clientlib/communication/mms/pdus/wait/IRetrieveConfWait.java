package cz.uhk.dip.mmsparams.clientlib.communication.mms.pdus.wait;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface IRetrieveConfWait
{
    /**
     * Wait for RetrieveConf will be received
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return Received RetrieveConf
     */
    default RetrieveConfModel anyRetrieveConf(PhoneInfoModel phoneRecipient)
    {
        return anyRetrieveConf(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for RetrieveConf will be received
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return Received RetrieveConf
     */
    default RetrieveConfModel anyRetrieveConf(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        return anyRetrieveConf(phoneRecipient, 1, timeout).get(0);
    }

    /**
     * Wait for specific number or RetrieveConf PDUs received
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of PDUs expected
     * @return Received RetrieveConf
     */
    default List<RetrieveConfModel> anyRetrieveConf(PhoneInfoModel phoneRecipient, int expectedCount)
    {
        return anyRetrieveConf(phoneRecipient, expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number or RetrieveConf PDUs received
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of PDUs expected
     * @param timeout        specific timeout
     * @return Received RetrieveConf
     */
    List<RetrieveConfModel> anyRetrieveConf(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout);
}
