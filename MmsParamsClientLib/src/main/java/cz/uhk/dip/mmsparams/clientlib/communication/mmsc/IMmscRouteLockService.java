package cz.uhk.dip.mmsparams.clientlib.communication.mmsc;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MmscAcquireRouteModel;

public interface IMmscRouteLockService
{
    /**
     * Create subscription to specific MMSC messages which has recipient matching pattern
     *
     * @param acquireRouteModel pattern for recipients number
     * @return acquire was successful
     */
    boolean acquireRouteLock(final MmscAcquireRouteModel acquireRouteModel);
}
