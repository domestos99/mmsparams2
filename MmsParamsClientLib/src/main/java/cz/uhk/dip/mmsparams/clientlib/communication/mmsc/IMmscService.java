package cz.uhk.dip.mmsparams.clientlib.communication.mmsc;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MmscAcquireRouteModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendConnectionModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendMmsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;

public interface IMmscService
{
    /**
     * Create subscription to specific MMSC messages which has recipient matching pattern
     *
     * @param acquireRouteModel pattern for recipients number
     * @return acquire was successful
     * @deprecated use service routeLock() instead
     */
    @Deprecated
    default boolean acquireRouteLock(final MmscAcquireRouteModel acquireRouteModel)
    {
        return this.routeLock().acquireRouteLock(acquireRouteModel);
    }

    /**
     * Deprecated - use acquireRouteLock instead
     *
     * @param acquireRouteModel pattern for recipients number
     * @return acquire was successful
     * @deprecated use service routeLock() instead
     */
    @Deprecated
    default boolean acquirePortRouter(final MmscAcquireRouteModel acquireRouteModel)
    {
        return acquireRouteLock(acquireRouteModel);
    }

    /**
     * Send MMS message request to MMSC
     *
     * @param mms mms to send
     * @return MMS submit response
     */
    MM7SubmitResponseModel sendMms(final MmscSendModel mms);

    /**
     * Send MMS message request to MMSC
     *
     * @param mmscSendConnectionModel MMSC connect info
     * @param mmscSendMmsModel        MMS parameters
     * @return MMS submit response
     */
    default MM7SubmitResponseModel sendMms(final MmscSendConnectionModel mmscSendConnectionModel, final MmscSendMmsModel mmscSendMmsModel)
    {
        return sendMms(new MmscSendModel(mmscSendConnectionModel, mmscSendMmsModel));
    }

    /**
     * Provides interface for Wait operations
     *
     * @return interface with services
     */
    IMmscServiceWait waitFor();

    /**
     * Provides interface for MustTimeout operations
     *
     * @return interface with services
     */
    IMmscServiceMustTimeout mustTimeoutFor();

    /**
     * Provides interfae for MMSC route lock operations
     *
     * @return interface with services
     */
    IMmscRouteLockService routeLock();


}
