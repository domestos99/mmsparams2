package cz.uhk.dip.mmsparams.clientlib.communication.mmsc;

import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface IMmscServiceMustTimeout
{
    /**
     * Wait whether MM7DeliveryReq message will not be received by test.
     * Default test timeout is used
     *
     * @return true if nothing received
     */
    default boolean deliveryReq()
    {
        return deliveryReq(Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether MM7DeliveryReq message will not be received by test.
     * Specific timeout is used
     *
     * @param timeout specific timeout
     * @return true if nothing received
     */
    boolean deliveryReq(CustomTimeout timeout);

    /**
     * Wait whether MM7DeliveryReportReq message will not be received by test.
     * Default test timeout is used
     *
     * @return true if nothing received
     */
    default boolean deliveryReportReq()
    {
        return deliveryReportReq(Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether MM7DeliveryReportReq message will not be received by test.
     * Specific timeout is used
     *
     * @param timeout specific timeout
     * @return true if nothing received
     */
    boolean deliveryReportReq(CustomTimeout timeout);


    /**
     * Wait whether MM7ReadReplyReq message will not be received by test.
     *
     * @return true if nothing received
     */
    default boolean readReplay()
    {
        return readReplay(Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether MM7ReadReplyReq message will not be received by test.
     *
     * @param timeout specific timeout
     * @return true if nothing received
     */
    boolean readReplay(CustomTimeout timeout);

}
