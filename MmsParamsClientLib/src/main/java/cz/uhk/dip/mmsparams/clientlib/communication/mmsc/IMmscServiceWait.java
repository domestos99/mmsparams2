package cz.uhk.dip.mmsparams.clientlib.communication.mmsc;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface IMmscServiceWait
{

    /**
     * Wait for MM7DeliveryReq will be received
     * Default test timeout is used
     *
     * @return Received MM7DeliveryReq
     */
    default MM7DeliveryReqModel anyDeliveryReq()
    {
        return anyDeliveryReq(Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for MM7DeliveryReq will be received
     * Specific timeout is used
     *
     * @param timeout specific timeout
     * @return Received MM7DeliveryReq
     */
    default MM7DeliveryReqModel anyDeliveryReq(CustomTimeout timeout)
    {
        return anyDeliveryReq(1, timeout).get(0);
    }

    /**
     * Wait for specific number or MM7DeliveryReq PDUs received
     * Default test timeout is used
     *
     * @param expectedCount number of PDUs expected
     * @return Received MM7DeliveryReq
     */
    default List<MM7DeliveryReqModel> anyDeliveryReq(int expectedCount)
    {
        return anyDeliveryReq(expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number or MM7DeliveryReq PDUs received
     * Specific timeout is used
     *
     * @param expectedCount number of PDUs expected
     * @param timeout       specific timeout
     * @return Received MM7DeliveryReq
     */
    List<MM7DeliveryReqModel> anyDeliveryReq(int expectedCount, CustomTimeout timeout);

    /**
     * Wait for MM7DeliveryReportReq will be received
     * Default test timeout is used
     *
     * @return Received MM7DeliveryReportReq
     */
    default MM7DeliveryReportReqModel anyDeliveryReportReq()
    {
        return anyDeliveryReportReq(Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for MM7DeliveryReportReq will be received
     * Specific timeout is used
     *
     * @param timeout specific timeout
     * @return Received MM7DeliveryReportReq
     */
    default MM7DeliveryReportReqModel anyDeliveryReportReq(CustomTimeout timeout)
    {
        return anyDeliveryReportReq(1, timeout).get(0);
    }

    /**
     * Wait for specific number or MM7DeliveryReportReq PDUs received
     * Default test timeout is used
     *
     * @param expectedCount number of PDUs expected
     * @return Received MM7DeliveryReportReq
     */
    default List<MM7DeliveryReportReqModel> anyDeliveryReportReq(int expectedCount)
    {
        return anyDeliveryReportReq(expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number or MM7DeliveryReportReq PDUs received
     * Specific timeout is used
     *
     * @param expectedCount number of PDUs expected
     * @param timeout       specific timeout
     * @return Received MM7DeliveryReportReq
     */
    List<MM7DeliveryReportReqModel> anyDeliveryReportReq(int expectedCount, CustomTimeout timeout);

    /**
     * Wait for MM7ReadReplyReqModel will be received
     * Default test timeout is used
     *
     * @return Received MM7ReadReplyReqModel
     */
    default MM7ReadReplyReqModel anyReadReplyReq()
    {
        return anyReadReplyReq(Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for MM7ReadReplyReqModel will be received
     * Specific timeout is used
     *
     * @param timeout specific timeout
     * @return Received MM7ReadReplyReqModel
     */
    default MM7ReadReplyReqModel anyReadReplyReq(CustomTimeout timeout)
    {
        return anyReadReplyReq(1, timeout).get(0);
    }

    /**
     * Wait for specific number or MM7ReadReplyReqModel PDUs received
     * Default test timeout is used
     *
     * @param expectedCount number of PDUs expected
     * @return Received MM7ReadReplyReqModel
     */
    default List<MM7ReadReplyReqModel> anyReadReplyReq(int expectedCount)
    {
        return anyReadReplyReq(expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number or MM7ReadReplyReqModel PDUs received
     * Specific timeout is used
     *
     * @param expectedCount number of PDUs expected
     * @param timeout       specific timeout
     * @return Received MM7ReadReplyReqModel
     */
    List<MM7ReadReplyReqModel> anyReadReplyReq(int expectedCount, CustomTimeout timeout);


}
