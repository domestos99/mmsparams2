package cz.uhk.dip.mmsparams.clientlib.communication.mmsc;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MmscAcquireRouteModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;

public class MmscRouteLockService extends ServiceBase implements IMmscRouteLockService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(MmscRouteLockService.class);

    public MmscRouteLockService(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public boolean acquireRouteLock(final MmscAcquireRouteModel acquireRouteModel)
    {
        Preconditions.checkNotNull(acquireRouteModel, GenericConstants.ACQUIRE_ROUTE_MODEL);
        ClientLogFacade.logAction(LOGGER, "acquireRouteLock");

        MmscAcquireRouteRequestMessage msg = MessageFactory.createServerRecipient(MmscAcquireRouteRequestMessage.class);
        msg.setMmscAcquireRouteModel(acquireRouteModel);
        MessageId msgId = webSocketSender.sendMessage(msg);
        return webSocketReceiver.waitForGenericBooleanResponseMessage(msgId).isValue();
    }
}
