package cz.uhk.dip.mmsparams.clientlib.communication.mmsc;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7SubmitResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;

public class MmscService extends ServiceBase implements IMmscService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(MmscService.class);

    private final IMmscServiceWait iMmscServiceWait;
    private final IMmscServiceMustTimeout iMmscServiceMustTimeout;
    private final IMmscRouteLockService iMmscRouteLockService;

    public MmscService(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
        this.iMmscServiceWait = new MmscServiceWait(webSocketSender, webSocketReceiver);
        this.iMmscServiceMustTimeout = new MmscServiceMustTimeout(webSocketSender, webSocketReceiver);
        this.iMmscRouteLockService = new MmscRouteLockService(webSocketSender, webSocketReceiver);
    }

    @Override
    public MM7SubmitResponseModel sendMms(final MmscSendModel mms)
    {
        Preconditions.checkNotNull(mms, GenericConstants.MMS);

        MmscSendMessage mmscSendMessage = MessageFactory.createServerRecipient(MmscSendMessage.class);
        mmscSendMessage.setMmscSendModel(mms);

        ClientLogFacade.logAction(LOGGER, "Sending MMS with MMSC");
        MessageId msgId = webSocketSender.sendMessage(mmscSendMessage);

        ClientLogFacade.logAction(LOGGER, "Waiting for MMS send confirmation");
        MM7SubmitResponseMessage response = webSocketReceiver.waitForResponseMessage(msgId, MM7SubmitResponseMessage.class);
        return response.getMm7SubmitResponseModel();
    }

    @Override
    public IMmscServiceWait waitFor()
    {
        return this.iMmscServiceWait;
    }

    @Override
    public IMmscServiceMustTimeout mustTimeoutFor()
    {
        return this.iMmscServiceMustTimeout;
    }

    @Override
    public IMmscRouteLockService routeLock()
    {
        return this.iMmscRouteLockService;
    }


}
