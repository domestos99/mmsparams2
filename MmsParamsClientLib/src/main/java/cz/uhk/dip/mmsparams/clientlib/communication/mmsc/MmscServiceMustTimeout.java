package cz.uhk.dip.mmsparams.clientlib.communication.mmsc;

import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReportReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ReadReplyReqMessage;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.validations.model.CustomTimeoutValidator;

public class MmscServiceMustTimeout extends ServiceBase implements IMmscServiceMustTimeout
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(MmscServiceMustTimeout.class);

    MmscServiceMustTimeout(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public boolean deliveryReq(CustomTimeout timeout)
    {
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MMSC DeliveryReq - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(MM7DeliveryReqMessage.class, timeout.getTimeoutSeconds());
    }

    @Override
    public boolean deliveryReportReq(CustomTimeout timeout)
    {
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MMSC DeliveryReportReq - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(MM7DeliveryReportReqMessage.class, timeout.getTimeoutSeconds());
    }

    @Override
    public boolean readReplay(CustomTimeout timeout)
    {
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MMSC MM7ReadReplyReq - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(MM7ReadReplyReqMessage.class, timeout.getTimeoutSeconds());
    }
}
