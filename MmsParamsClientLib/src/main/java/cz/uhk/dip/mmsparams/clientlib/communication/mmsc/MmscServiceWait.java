package cz.uhk.dip.mmsparams.clientlib.communication.mmsc;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReportReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ReadReplyReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.validations.model.CustomTimeoutValidator;

public class MmscServiceWait extends ServiceBase implements IMmscServiceWait
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(MmscServiceWait.class);

    MmscServiceWait(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public List<MM7DeliveryReqModel> anyDeliveryReq(int expectedCount, CustomTimeout timeout)
    {
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for MM7DeliveryReq");

        List<MM7DeliveryReqMessage> res = webSocketReceiver.waitForResponseMessage(MM7DeliveryReqMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<MM7DeliveryReqModel> result = new ArrayList<>();

        for (MM7DeliveryReqMessage mms : res)
        {
            result.add(mms.getMm7DeliveryReqModel());
        }
        return result;
    }

    @Override
    public List<MM7DeliveryReportReqModel> anyDeliveryReportReq(int expectedCount, CustomTimeout timeout)
    {
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MM7DeliveryReportReq");

        List<MM7DeliveryReportReqMessage> res = webSocketReceiver.waitForResponseMessage(MM7DeliveryReportReqMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<MM7DeliveryReportReqModel> result = new ArrayList<>();

        for (MM7DeliveryReportReqMessage dr : res)
        {
            result.add(dr.getMm7DeliveryReportReqModel());
        }
        return result;
    }

    @Override
    public List<MM7ReadReplyReqModel> anyReadReplyReq(int expectedCount, CustomTimeout timeout)
    {
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);
        ClientLogFacade.logAction(LOGGER, "Waiting for MM7ReadReplyReqModel");

        List<MM7ReadReplyReqMessage> res = webSocketReceiver.waitForResponseMessage(MM7ReadReplyReqMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<MM7ReadReplyReqModel> result = new ArrayList<>();

        for (MM7ReadReplyReqMessage rr : res)
        {
            result.add(rr.getMm7ReadReplyReqModel());
        }
        return result;
    }
}
