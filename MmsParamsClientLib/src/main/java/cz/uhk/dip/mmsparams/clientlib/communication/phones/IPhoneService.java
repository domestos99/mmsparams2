package cz.uhk.dip.mmsparams.clientlib.communication.phones;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;

public interface IPhoneService
{
    /**
     * Returns list of all phones connected to server
     *
     * @return list of all phones connected to server
     */
    List<PhoneInfoModel> getAllPhones();

    /**
     * Returns instance of phone with specific IMSI
     *
     * @param imsi IMSI of the phone
     * @return phone instance
     */
    PhoneInfoModel getByIMSI(final String imsi);

    /**
     * Returns instance of phone with specific custom name
     *
     * @param customName custom name set in Android app config
     * @return phone instance
     */
    PhoneInfoModel getByCustomName(final String customName);

    /**
     * Lock phone instance for current test
     *
     * @param phoneInfoModel phone instance
     * @return true if successfully locked
     */
    boolean lockPhoneForTest(final PhoneInfoModel phoneInfoModel);

    /**
     * Unlock phone instance for current test
     *
     * @param phoneInfoModel phone instance
     * @return true if successfully locked
     */
    boolean unLockPhoneForTest(PhoneInfoModel phoneInfoModel);

    /**
     * Returns instance of phone with specific custom name
     * Phone is also locked for current test
     *
     * @param customName custom name set in Android app config
     * @return phone instance
     */
    PhoneInfoModel getByCustomNameAndLock(String customName);
}
