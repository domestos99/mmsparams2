package cz.uhk.dip.mmsparams.clientlib.communication.phones;

import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.exceptions.TestErrorException;
import cz.uhk.dip.mmsparams.api.exceptions.TestInvalidException;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.validations.model.PhoneInfoModelValidator;

public class PhoneService extends ServiceBase implements IPhoneService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(PhoneService.class);

    public PhoneService(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public List<PhoneInfoModel> getAllPhones()
    {
        ClientLogFacade.logAction(LOGGER, "getAllPhones");
        PhoneListRequestMessage phoneListRequestMessage = MessageFactory.createServerRecipient(PhoneListRequestMessage.class);

        ClientLogFacade.logAction(LOGGER, "Requesting All Phones");
        MessageId reqId = webSocketSender.sendMessage(phoneListRequestMessage);

        ClientLogFacade.logAction(LOGGER, "Waiting for All Phones response");
        PhoneListResponseMessage response = webSocketReceiver.waitForResponseMessage(reqId, PhoneListResponseMessage.class);
        return response.getPhoneInfoModels();
    }

    @Override
    public PhoneInfoModel getByIMSI(final String imsi)
    {
        Preconditions.checkNotNullOrEmpty(imsi, GenericConstants.IMSI);

        List<PhoneInfoModel> phones = getAllPhones();

        for (PhoneInfoModel di : phones)
        {
            if (imsi.equals(di.getImsi()))
            {
                return di;
            }
        }
        throw new TestInvalidException("Phone with imsi: " + imsi + " not found!");
    }

    @Override
    public PhoneInfoModel getByCustomName(String customName)
    {
        Preconditions.checkNotNullOrEmpty(customName, GenericConstants.CUSTOM_NAME);

        List<PhoneInfoModel> phones = getAllPhones();

        for (PhoneInfoModel di : phones)
        {
            if (customName.equals(di.getPhoneCustomName()))
            {
                return di;
            }
        }
        throw new TestErrorException("Phone with customName: " + customName + " not found!");
    }

    @Override
    public boolean lockPhoneForTest(PhoneInfoModel phoneInfoModel)
    {
        PhoneInfoModelValidator.validate(phoneInfoModel, GenericConstants.PHONE_INFO_MODEL);

        LockPhoneMessage lockPhoneMessage = MessageFactory.createServerRecipient(LockPhoneMessage.class);
        lockPhoneMessage.setPhoneInfoModel(phoneInfoModel);

        ClientLogFacade.logAction(LOGGER, "Locking phone for test");
        MessageId reqId = webSocketSender.sendMessage(lockPhoneMessage);
        ClientLogFacade.logAction(LOGGER, "Waiting for Lock phone for test confirmation");
        return webSocketReceiver.waitForGenericBooleanResponseMessage(reqId).isValue();
    }

    @Override
    public PhoneInfoModel getByCustomNameAndLock(String customName)
    {
        Preconditions.checkNotNullOrEmpty(customName, GenericConstants.CUSTOM_NAME);
        PhoneInfoModel di = getByCustomName(customName);
        lockPhoneForTest(di);
        return di;
    }

    @Override
    public boolean unLockPhoneForTest(PhoneInfoModel phoneInfoModel)
    {
        PhoneInfoModelValidator.validate(phoneInfoModel, GenericConstants.PHONE_INFO_MODEL);

        UnLockPhoneMessage unLockPhoneMessage = MessageFactory.createServerRecipient(UnLockPhoneMessage.class);

        unLockPhoneMessage.setPhoneInfoModel(phoneInfoModel);

        ClientLogFacade.logAction(LOGGER, "Unlocking phone for test");
        MessageId reqId = webSocketSender.sendMessage(unLockPhoneMessage);
        ClientLogFacade.logAction(LOGGER, "Waiting for Unlock phone for test confirmation");
        return webSocketReceiver.waitForGenericBooleanResponseMessage(reqId).isValue();
    }

}
