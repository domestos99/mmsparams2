package cz.uhk.dip.mmsparams.clientlib.communication.profiles;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;

public interface IProfileService
{
    /**
     * Set MMS recipient profile for specific phone instance
     *
     * @param mmsRecipientPhoneProfile MMS recipient profile
     * @param phoneRecipient           phone instance
     * @return true if successfully set
     */
    boolean setRecipinentPhoneProfile(MmsRecipientPhoneProfile mmsRecipientPhoneProfile, PhoneInfoModel phoneRecipient);
}
