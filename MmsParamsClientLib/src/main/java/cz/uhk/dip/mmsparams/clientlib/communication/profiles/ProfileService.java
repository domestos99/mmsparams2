package cz.uhk.dip.mmsparams.clientlib.communication.profiles;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsRecipientPhoneProfileRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.validations.model.MmsRecipientPhoneProfileValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.PhoneInfoModelValidator;

public class ProfileService extends ServiceBase implements IProfileService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(ProfileService.class);

    public ProfileService(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public boolean setRecipinentPhoneProfile(MmsRecipientPhoneProfile mmsRecipientPhoneProfile, PhoneInfoModel phoneRecipient)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        MmsRecipientPhoneProfileValidator.validate(mmsRecipientPhoneProfile);

        MmsRecipientPhoneProfileRequestMessage request = MessageFactory.create(MmsRecipientPhoneProfileRequestMessage.class);
        request.setMmsRecipientPhoneProfile(mmsRecipientPhoneProfile);
        request.setRecipientKey(phoneRecipient.getPhoneKey());
        ClientLogFacade.logAction(LOGGER, "Setting MmsRecipientPhoneProfile on recipient phone");
        MessageId reqId = webSocketSender.sendMessage(request);
        ClientLogFacade.logAction(LOGGER, "Waiting for set MmsRecipientPhoneProfile confirmation");
        return webSocketReceiver.waitForGenericBooleanResponseMessage(reqId).isValue();
    }
}
