package cz.uhk.dip.mmsparams.clientlib.communication.remoteconnect;

import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface IRemoteConnectService
{
    final CustomTimeout WAIT_FOR_PHONE_TIMEOUT = CustomTimeout.CUSTOM_TIMEOUT_20;

    /**
     * Send SMS from phone A to phone B.
     * This SMS will trigger connecting to server
     *
     * @param phoneSender              sender phone instance
     * @param phoneToConnectNumber     recipient phone number
     * @param phoneToConnectCustomName recipient phone custom name
     * @return phone B instance
     */
    default PhoneInfoModel connectWithSms(PhoneInfoModel phoneSender, String phoneToConnectNumber, String phoneToConnectCustomName)
    {
        return connectWithSms(phoneSender, phoneToConnectNumber, phoneToConnectCustomName, WAIT_FOR_PHONE_TIMEOUT);
    }


    /**
     * Send SMS from phone A to phone B.
     * This SMS will trigger connecting to server
     *
     * @param phoneSender              sender phone instance
     * @param phoneToConnectNumber     recipient phone number
     * @param phoneToConnectCustomName recipient phone custom name
     * @param waitForPhoneTimeout      how long to wait for phone to connect
     * @return phone B instance
     */
    PhoneInfoModel connectWithSms(PhoneInfoModel phoneSender, String phoneToConnectNumber, String phoneToConnectCustomName,
                                  CustomTimeout waitForPhoneTimeout);

    /**
     * Send SMS from Smsc to phone B.
     *
     * @param smscConnectModel         SMSC connect information
     * @param smscNumberFrom           sender phone number
     * @param phoneToConnectNumber     recipient phone number
     * @param phoneToConnectCustomName recipient phone custom name
     * @return phone B instance
     */
    default PhoneInfoModel connectWithSmsc(SmscConnectModel smscConnectModel, String smscNumberFrom, String phoneToConnectNumber,
                                           String phoneToConnectCustomName)
    {
        return connectWithSmsc(smscConnectModel, smscNumberFrom, phoneToConnectNumber, phoneToConnectCustomName, WAIT_FOR_PHONE_TIMEOUT);
    }

    /**
     * Send SMS from SMSC to phone B.
     *
     * @param smscConnectModel         SMSC connect information
     * @param smscNumberFrom           sender phone number
     * @param phoneToConnectNumber     recipient phone number
     * @param phoneToConnectCustomName recipient phone custom name
     * @param waitForPhoneTimeout      how long to wait for phone to connect
     * @return phone B instance
     */
    PhoneInfoModel connectWithSmsc(SmscConnectModel smscConnectModel, String smscNumberFrom, String phoneToConnectNumber, String phoneToConnectCustomName,
                                   CustomTimeout waitForPhoneTimeout);


    /**
     * Send SMS from SMSC to phone B.
     *
     * @param smscConnectModel         Smsc connect information
     * @param smscNumberFrom           sender phone number
     * @param phoneToConnectNumber     recipient phone number
     * @param phoneToConnectCustomName recipient phone custom name
     * @return phone B instance and opened Smsc session ID
     */
    default Tuple<PhoneInfoModel, SmscSessionId> connectWithSmscKeepOpen(SmscConnectModel smscConnectModel, String smscNumberFrom, String phoneToConnectNumber,
                                                                         String phoneToConnectCustomName)
    {
        return connectWithSmscKeepOpen(smscConnectModel, smscNumberFrom, phoneToConnectNumber, phoneToConnectCustomName, WAIT_FOR_PHONE_TIMEOUT);
    }

    /**
     * Send SMS from SMSC to phone B.
     *
     * @param smscConnectModel         SMSC connect information
     * @param smscNumberFrom           sender phone number
     * @param phoneToConnectNumber     recipient phone number
     * @param phoneToConnectCustomName recipient phone custom name
     * @param waitForPhoneTimeout      how long to wait for phone to connect
     * @return phone B instance and opened SMSC session ID
     */
    Tuple<PhoneInfoModel, SmscSessionId> connectWithSmscKeepOpen(SmscConnectModel smscConnectModel, String smscNumberFrom, String phoneToConnectNumber, String phoneToConnectCustomName,
                                                                 CustomTimeout waitForPhoneTimeout);
}
