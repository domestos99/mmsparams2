package cz.uhk.dip.mmsparams.clientlib.communication.remoteconnect;

import cz.uhk.dip.mmsparams.api.connections.AndroidConnectBuilder;
import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;
import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.JavaBase64Converter;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscAddressModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.phones.IPhoneService;
import cz.uhk.dip.mmsparams.clientlib.communication.sms.ISmsService;
import cz.uhk.dip.mmsparams.clientlib.communication.smsc.ISmscService;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.utils.ThreadUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.model.PhoneInfoModelValidator;

public class RemoteConnectService extends ServiceBase implements IRemoteConnectService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(RemoteConnectService.class);

    private final ISmsService iSmsService;
    private final ISmscService iSmscService;
    private final IPhoneService iPhoneService;
    private final TestSettings testSettings;

    public RemoteConnectService(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver, ISmsService iSmsService, ISmscService iSmscService, IPhoneService iPhoneService, TestSettings testSettings)
    {
        super(webSocketSender, webSocketReceiver);
        this.iSmsService = iSmsService;
        this.iSmscService = iSmscService;
        this.iPhoneService = iPhoneService;
        this.testSettings = testSettings;
    }

    @Override
    public PhoneInfoModel connectWithSms(PhoneInfoModel phoneSender, String phoneToConnectNumber, String phoneToConnectCustomName, CustomTimeout waitForPhoneTimeout)
    {
        PhoneInfoModelValidator.validate(phoneSender, GenericConstants.PHONE_SENDER);
        Preconditions.checkNotNullOrEmpty(phoneToConnectNumber, GenericConstants.PHONE_TARGET_NUMBER);
        Preconditions.checkNotNullOrEmpty(phoneToConnectCustomName, GenericConstants.CUSTOM_NAME);

        SmsSendModel sms = new SmsSendModel();
        sms.setTo(phoneToConnectNumber);
        sms.setDeliveryReport(false);
        String smsText = createSmsMessageText(testSettings, phoneToConnectCustomName);

        sms.setText(smsText);

        ClientLogFacade.logAction(LOGGER, "Remote connect - sending SMS with request");
        SmsSendMessageId smsSendID = iSmsService.sendSms(sms, phoneSender);

        ClientLogFacade.logAction(LOGGER, "Remote connect - waiting for SMS with request successfully send");
        SmsSendResponseModel sendOk = iSmsService.waitFor().smsSentSuccessfully(smsSendID);

        ClientLogFacade.logAction(LOGGER, "Remote connect - waiting for phone to connect");
        ThreadUtils.sleep(waitForPhoneTimeout);

        ClientLogFacade.logAction(LOGGER, "Remote connect - trying to get phone");
        return this.iPhoneService.getByCustomName(phoneToConnectCustomName);
    }

    @Override
    public PhoneInfoModel connectWithSmsc(SmscConnectModel smscConnectModel, String smscNumberFrom, String phoneToConnectNumber,
                                          String phoneToConnectCustomName, CustomTimeout waitForPhoneTimeout)
    {
        SmscSessionId sessionId = sendSmscMessage(smscConnectModel, smscNumberFrom, phoneToConnectNumber, phoneToConnectCustomName, waitForPhoneTimeout);

        ClientLogFacade.logAction(LOGGER, "Remote connect - Disconnecting from Smsc");
        this.iSmscService.disconnect(sessionId);
        ClientLogFacade.logAction(LOGGER, "Remote connect - Disconnected from Smsc");

        waitForPhoneToConnect(waitForPhoneTimeout);

        ClientLogFacade.logAction(LOGGER, "Remote connect - trying to get phone");
        return this.iPhoneService.getByCustomName(phoneToConnectCustomName);
    }

    @Override
    public Tuple<PhoneInfoModel, SmscSessionId> connectWithSmscKeepOpen(SmscConnectModel smscConnectModel, String smscNumberFrom, String phoneToConnectNumber,
                                                                        String phoneToConnectCustomName, CustomTimeout waitForPhoneTimeout)
    {
        SmscSessionId sessionId = sendSmscMessage(smscConnectModel, smscNumberFrom, phoneToConnectNumber, phoneToConnectCustomName, waitForPhoneTimeout);

        ClientLogFacade.logAction(LOGGER, "Remote connect - waiting for phone to connect");
        ThreadUtils.sleep(waitForPhoneTimeout);

        ClientLogFacade.logAction(LOGGER, "Remote connect - trying to get phone");
        return new Tuple<>(this.iPhoneService.getByCustomName(phoneToConnectCustomName), sessionId);
    }

    private SmscSessionId sendSmscMessage(SmscConnectModel smscConnectModel, String smscNumberFrom, String phoneToConnectNumber,
                                          String phoneTargetCustomName, CustomTimeout waitForPhoneTimeout)
    {
        ClientLogFacade.logAction(LOGGER, "Remote connect - connecting to Smsc");
        SmscConnectResponseModel status = this.iSmscService.connectToSMSC(smscConnectModel);
        SmscSessionId sessionId = status.getSessionId();
        ClientLogFacade.logAction(LOGGER, "Remote connect - connected to Smsc");

        SmscSendModel smscSendModel = new SmscSendModel();
        smscSendModel.setSenderAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, smscNumberFrom));
        smscSendModel.setRecipientAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, phoneToConnectNumber));
        smscSendModel.setDeliveryReport(false);

        String smsText = createSmsMessageText(testSettings, phoneTargetCustomName);

        smscSendModel.setShortMessage(smsText);

        waitForPhoneToConnect(waitForPhoneTimeout);
        ClientLogFacade.logAction(LOGGER, "Remote connect - SMS with request sent");
        return sessionId;
    }

    private void waitForPhoneToConnect(CustomTimeout waitForPhoneTimeout)
    {
        ClientLogFacade.logAction(LOGGER, "Remote connect - waiting for phone to connect");
        ThreadUtils.sleep(waitForPhoneTimeout);
    }

    private static String createSmsMessageText(TestSettings testSettings, String phoneTargetCustomName)
    {
        return AndroidConnectBuilder.createConnectMessage(new JavaBase64Converter(), testSettings.getServerAddressProvider().getServerAddress(false),
                testSettings.getServerAddressProvider().isHttps(), phoneTargetCustomName, new JwtRequest(testSettings.getUsername(), testSettings.getPassword()));
    }

}
