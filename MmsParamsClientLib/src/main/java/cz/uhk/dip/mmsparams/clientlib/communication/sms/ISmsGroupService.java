package cz.uhk.dip.mmsparams.clientlib.communication.sms;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.clientlib.model.sms.SmsGroup;

public interface ISmsGroupService
{
    SmsGroup sendSmsAndReceive(SmsSendModel sms, PhoneInfoModel phoneSender, PhoneInfoModel phoneRecipient);
}
