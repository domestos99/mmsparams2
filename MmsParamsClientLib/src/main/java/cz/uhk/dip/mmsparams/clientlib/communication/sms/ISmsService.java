package cz.uhk.dip.mmsparams.clientlib.communication.sms;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;

public interface ISmsService
{
    /**
     * Send Sms message request to Phone device
     *
     * @param sms         SMS message to send
     * @param phoneSender phone instance
     * @return return SMS send request message ID
     */
    SmsSendMessageId sendSms(SmsSendModel sms, PhoneInfoModel phoneSender);


    /**
     * Provides interface to SMS group features
     *
     * @return interface with services
     */
    ISmsGroupService SmsGroup();

    /**
     * Provides interface to Wait operations
     *
     * @return interface with services
     */
    ISmsServiceWait waitFor();

    /**
     * Provides interface to MustTimeout operations
     *
     * @return interface with services
     */
    ISmsServiceMustTimeout mustTimeoutFor();
}
