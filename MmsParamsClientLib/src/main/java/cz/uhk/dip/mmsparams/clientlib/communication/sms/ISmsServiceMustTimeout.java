package cz.uhk.dip.mmsparams.clientlib.communication.sms;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;

public interface ISmsServiceMustTimeout
{
    /**
     * Wait whether SMS delivery report message will not be received by test.
     * Default test timeout is used
     *
     * @param smsSendReqId SMS send request message ID
     * @return true if nothing received
     */
    default boolean deliveryReport(SmsSendMessageId smsSendReqId)
    {
        return deliveryReport(smsSendReqId, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether SMS message will not be received by test.
     * Specific timeout is used
     *
     * @param smsSendReqId SMS send request message ID
     * @param timeout      specific timeout
     * @return true if nothing received
     */
    boolean deliveryReport(SmsSendMessageId smsSendReqId, CustomTimeout timeout);

    /**
     * Wait whether received SMS delivery report message will not be received by test.
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return true if nothing received
     */
    default boolean smsReceive(PhoneInfoModel phoneRecipient)
    {
        return smsReceive(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether received SMS message will not be received by test.
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return true if nothing received
     */
    boolean smsReceive(PhoneInfoModel phoneRecipient, CustomTimeout timeout);
}
