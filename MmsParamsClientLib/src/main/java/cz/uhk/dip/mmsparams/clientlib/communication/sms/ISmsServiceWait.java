package cz.uhk.dip.mmsparams.clientlib.communication.sms;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;

public interface ISmsServiceWait
{
    /**
     * Wait for SMS send successfully confirmation
     * Default test timeout is used
     *
     * @param smsSendReqId SMS send request message ID
     * @return Received SMS send successfully confirmation
     */
    default SmsSendResponseModel smsSentSuccessfully(SmsSendMessageId smsSendReqId)
    {
        return smsSentSuccessfully(smsSendReqId, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for SMS send successfully confirmation
     * Specific timeout is used
     *
     * @param smsSendReqId SMS send request message ID
     * @param timeout      specific timeout
     * @return Received SMS send successfully confirmation
     */
    SmsSendResponseModel smsSentSuccessfully(SmsSendMessageId smsSendReqId, CustomTimeout timeout);

    /**
     * Waif for SMS received on recipient phone
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return Received SMS message
     */
    default SmsReceiveModel anySmsReceived(PhoneInfoModel phoneRecipient)
    {
        return anySmsReceived(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Waif for SMS received on recipient phone
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return Received SMS message
     */
    default SmsReceiveModel anySmsReceived(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        return anySmsReceived(phoneRecipient, 1, timeout).get(0);
    }

    /**
     * Wait for specific number of SMS received on recipient phone
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of SMS expected
     * @return Received SMS messages
     */
    default List<SmsReceiveModel> anySmsReceived(PhoneInfoModel phoneRecipient, int expectedCount)
    {
        return anySmsReceived(phoneRecipient, expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number of SMS received on recipient phone
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of SMS expected
     * @param timeout        specific timeout
     * @return Received SMS messages
     */
    List<SmsReceiveModel> anySmsReceived(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout);

    /**
     * Waif for SMS received on recipient phone.
     * One item of the list contains one received SMS part
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @return Received SMS message parts
     */
    default List<SmsReceiveModel> anySmsReceiveAllParts(PhoneInfoModel phoneRecipient)
    {
        return anySmsReceiveAllParts(phoneRecipient, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Waif for SMS received on recipient phone.
     * One item of the list contains one received SMS part
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param timeout        specific timeout
     * @return Received SMS messages parts
     */
    default List<SmsReceiveModel> anySmsReceiveAllParts(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        return anySmsReceiveAllParts(phoneRecipient, 1, timeout).get(0);
    }

    /**
     * Wait for specific number of SMS received on recipient phone
     * List contains list of received SMS parts
     * Default test timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of SMS expected
     * @return Received SMS messages with its parts
     */
    default List<ArrayList<SmsReceiveModel>> anySmsReceiveAllParts(PhoneInfoModel phoneRecipient, int expectedCount)
    {
        return anySmsReceiveAllParts(phoneRecipient, expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait for specific number of SMS received on recipient phone
     * List contains list of received SMS parts
     * Specific timeout is used
     *
     * @param phoneRecipient recipient phone instance
     * @param expectedCount  number of SMS expected
     * @param timeout        specific timeout
     * @return Received SMS messages with its parts
     */
    List<ArrayList<SmsReceiveModel>> anySmsReceiveAllParts(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout);


    /**
     * Waif for SMS delivery report received on sender phone
     * Default test timeout is used
     *
     * @param smsSendReqId SMS send request message ID
     * @return Received SMS delivery report
     */
    default SmsDeliveryReport anyDeliveryReport(SmsSendMessageId smsSendReqId)
    {
        return anyDeliveryReport(smsSendReqId, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Waif for SMS delivery report received on sender phone
     * Specific timeout is used
     *
     * @param smsSendReqId SMS send request message ID
     * @param timeout      specific timeout
     * @return Received SMS delivery report
     */
    SmsDeliveryReport anyDeliveryReport(SmsSendMessageId smsSendReqId, CustomTimeout timeout);
}
