package cz.uhk.dip.mmsparams.clientlib.communication.sms;

import java.util.List;

import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.model.sms.SmsGroup;
import cz.uhk.dip.mmsparams.clientlib.validations.model.PhoneInfoModelValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.SmsSendModelValidator;

public class SmsGroupService implements ISmsGroupService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(SmsGroupService.class);

    private final ISmsService iSmsService;

    public SmsGroupService(ISmsService iSmsService)
    {
        this.iSmsService = iSmsService;
    }

    @Override
    public SmsGroup sendSmsAndReceive(SmsSendModel sms, PhoneInfoModel phoneSender, PhoneInfoModel phoneRecipient)
    {
        SmsSendModelValidator.validate(sms);
        PhoneInfoModelValidator.validate(phoneSender);
        PhoneInfoModelValidator.validate(phoneRecipient);

        ClientLogFacade.logAction(LOGGER, "Begin Send SMS Group");
        SmsSendMessageId smsSendID = this.iSmsService.sendSms(sms, phoneSender);

        SmsSendResponseModel sendOk = this.iSmsService.waitFor().smsSentSuccessfully(smsSendID);

        SmsReceiveModel smsReceiveModel = this.iSmsService.waitFor().anySmsReceived(phoneRecipient);

        List<SmsReceiveModel> smsReceiveModelAllparts = this.iSmsService.waitFor().anySmsReceiveAllParts(phoneRecipient);

        SmsDeliveryReport dr = null;
        if (sms.isDeliveryReport())
            dr = this.iSmsService.waitFor().anyDeliveryReport(smsSendID);

        ClientLogFacade.logAction(LOGGER, "Ending Send SMS Group");
        SmsGroup smsGroup = new SmsGroup();
        smsGroup.setSmsSend(sms);
        smsGroup.setSmsSendConfirmation(sendOk);
        smsGroup.setSmsReceived(smsReceiveModel);
        smsGroup.setSmsReceivedAllParts(smsReceiveModelAllparts);
        smsGroup.setSmsDeliveryReport(dr);
        return smsGroup;
    }
}
