package cz.uhk.dip.mmsparams.clientlib.communication.sms;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.model.PhoneInfoModelValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.SmsSendModelValidator;

public class SmsService extends ServiceBase implements ISmsService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(SmsService.class);

    private final ISmsGroupService iSmsGroupService;
    private final ISmsServiceWait iSmsServiceWait;
    private final ISmsServiceMustTimeout iSmsServiceMustTimeout;

    public SmsService(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
        this.iSmsServiceWait = new SmsServiceWait(webSocketSender, webSocketReceiver);
        this.iSmsServiceMustTimeout = new SmsServiceMustTimeout(webSocketSender, webSocketReceiver);
        iSmsGroupService = new SmsGroupService(this);
    }

    @Override
    public SmsSendMessageId sendSms(SmsSendModel sms, PhoneInfoModel phoneSender)
    {
        SmsSendModelValidator.validate(sms);
        PhoneInfoModelValidator.validate(phoneSender, GenericConstants.PHONE_SENDER);

        ClientLogFacade.logAction(LOGGER, "Sending SMS");

        SmsSendPhoneRequestMessage request = MessageFactory.create(SmsSendPhoneRequestMessage.class);
        request.setSmsSendModel(sms);
        request.setRecipientKey(phoneSender.getPhoneKey());

        SmsSendMessageId messageID = new SmsSendMessageId(webSocketSender.sendMessage(request));
        webSocketReceiver.waitForGenericBooleanResponseMessage(messageID);
        return messageID;
    }

    @Override
    public ISmsGroupService SmsGroup()
    {
        return this.iSmsGroupService;
    }

    @Override
    public ISmsServiceWait waitFor()
    {
        return this.iSmsServiceWait;
    }

    @Override
    public ISmsServiceMustTimeout mustTimeoutFor()
    {
        return this.iSmsServiceMustTimeout;
    }


}
