package cz.uhk.dip.mmsparams.clientlib.communication.sms;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.model.CustomTimeoutValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.MessageIdValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.PhoneInfoModelValidator;

public class SmsServiceMustTimeout extends ServiceBase implements ISmsServiceMustTimeout
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(SmsServiceMustTimeout.class);

    SmsServiceMustTimeout(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public boolean deliveryReport(SmsSendMessageId smsSendReqId, CustomTimeout timeout)
    {
        MessageIdValidator.validate(smsSendReqId);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for SMS Delivery report - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(smsSendReqId, SmsSendDeliveryReportMessage.class, timeout.getTimeoutSeconds());
    }

    @Override
    public boolean smsReceive(PhoneInfoModel phoneRecipient, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for SMS Receive - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(phoneRecipient, SmsReceivePhoneMessage.class, timeout.getTimeoutSeconds());
    }
}
