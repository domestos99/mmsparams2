package cz.uhk.dip.mmsparams.clientlib.communication.sms;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneAllPartsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.model.CustomTimeoutValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.MessageIdValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.PhoneInfoModelValidator;

public class SmsServiceWait extends ServiceBase implements ISmsServiceWait
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(SmsServiceWait.class);

    SmsServiceWait(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public SmsSendResponseModel smsSentSuccessfully(SmsSendMessageId smsSendReqId, CustomTimeout timeout)
    {
        MessageIdValidator.validate(smsSendReqId);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for SMS successfully received");
        SmsSendPhoneResponseMessage res = webSocketReceiver.waitForResponseMessage(smsSendReqId, SmsSendPhoneResponseMessage.class, timeout.getTimeoutSeconds());
        return res.getSmsSendResponseModel();
    }

    @Override
    public List<SmsReceiveModel> anySmsReceived(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for SMS receive");
        // Wait for sms received on receiver device
        List<SmsReceivePhoneMessage> responses = webSocketReceiver.waitForResponseMessage(phoneRecipient, SmsReceivePhoneMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<SmsReceiveModel> lSms = new ArrayList<>();
        for (SmsReceivePhoneMessage a : responses)
        {
            lSms.add(a.getSmsReceiveModel());
        }
        return lSms;
    }

    @Override
    public List<ArrayList<SmsReceiveModel>> anySmsReceiveAllParts(PhoneInfoModel phoneRecipient, int expectedCount, CustomTimeout timeout)
    {
        PhoneInfoModelValidator.validate(phoneRecipient, GenericConstants.PHONE_RECIPIENT);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for SMS receive");
        // Wait for sms received on receiver device
        List<SmsReceivePhoneAllPartsMessage> responses = webSocketReceiver.waitForResponseMessage(phoneRecipient, SmsReceivePhoneAllPartsMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<ArrayList<SmsReceiveModel>> lSms = new ArrayList<>();
        for (SmsReceivePhoneAllPartsMessage a : responses)
        {
            lSms.add(a.getSmsReceiveAllPartsModel());
        }
        return lSms;
    }

    @Override
    public SmsDeliveryReport anyDeliveryReport(SmsSendMessageId smsSendReqId, CustomTimeout timeout)
    {
        MessageIdValidator.validate(smsSendReqId);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "Waiting for SMS Delivery report");
        SmsSendDeliveryReportMessage responseDeliveryReport = webSocketReceiver.waitForResponseMessage(smsSendReqId, SmsSendDeliveryReportMessage.class, timeout.getTimeoutSeconds());
        return responseDeliveryReport.getSmsDeliveryReport();
    }
}
