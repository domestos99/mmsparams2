package cz.uhk.dip.mmsparams.clientlib.communication.smsc;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.clientlib.model.smsc.SmscGroup;

public interface ISmscGroupService
{
    SmscGroup sendSms(final SmscConnectModel smscConnectModel, final SmscSendModel smscSendModel);
}
