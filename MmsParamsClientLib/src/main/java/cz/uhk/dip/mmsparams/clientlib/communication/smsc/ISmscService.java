package cz.uhk.dip.mmsparams.clientlib.communication.smsc;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;

public interface ISmscService
{
    /**
     * Connect server to Smsc
     *
     * @param smscConnectModel connection information
     * @return object with information about connection
     */
    SmscConnectResponseModel connectToSMSC(final SmscConnectModel smscConnectModel);

    /**
     * Send one SMS message from Smsc
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @param smscSendModel SMS to send
     * @return SMS send successfully confirmation
     */
    SmscSendSmsResponseModel sendSms(final SmscSessionId smscSessionId, final SmscSendModel smscSendModel);

    /**
     * Disconnect server from Smsc
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @return true if disconnected successfully
     */
    boolean disconnect(final SmscSessionId smscSessionId);

    /**
     * Provides interface to Wait operations
     *
     * @return interface with services
     */
    ISmscServiceWait waitFor();

    /**
     * Provides interface to MustTimeout operations
     *
     * @return interface with services
     */
    ISmscServiceMustTimeout mustTimeoutFor();

    /**
     * Provides interface to Group features
     *
     * @return interface with services
     */
    ISmscGroupService SmscGroup();


}
