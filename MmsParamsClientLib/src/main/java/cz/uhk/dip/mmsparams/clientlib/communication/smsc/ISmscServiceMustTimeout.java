package cz.uhk.dip.mmsparams.clientlib.communication.smsc;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface ISmscServiceMustTimeout
{
    /**
     * Wait whether SMS will not be received by test
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @return true if nothing received
     */
    default boolean smsReceive(final SmscSessionId smscSessionId)
    {
        return smsReceive(smscSessionId, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether SMS will not be received by test
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @param timeout       specific timeout
     * @return true if nothing received
     */
    boolean smsReceive(final SmscSessionId smscSessionId, CustomTimeout timeout);

    /**
     * Wait whether SMS delivery report will not be received by test
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @return true if nothing received
     */
    default boolean deliveryReport(final SmscSessionId smscSessionId)
    {
        return deliveryReport(smscSessionId, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Wait whether SMS delivery report will not be received by test
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @param timeout       specific timeout
     * @return true if nothing received
     */
    boolean deliveryReport(final SmscSessionId smscSessionId, CustomTimeout timeout);
}
