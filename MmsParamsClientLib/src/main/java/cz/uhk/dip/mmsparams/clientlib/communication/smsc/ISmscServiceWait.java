package cz.uhk.dip.mmsparams.clientlib.communication.smsc;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.Constants;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public interface ISmscServiceWait
{
    /**
     * Waif for SMS received from Smsc
     * Default test timeout is used
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @return received SMS message
     */
    default SmscDeliverSmModel anySmsReceive(final SmscSessionId smscSessionId)
    {
        return anySmsReceive(smscSessionId, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Waif for SMS received from Smsc
     * Default test timeout is used
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @param expectedCount number of SMS expected
     * @return received SMS message
     */
    default List<SmscDeliverSmModel> anySmsReceive(final SmscSessionId smscSessionId, int expectedCount)
    {
        return anySmsReceive(smscSessionId, expectedCount, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Waif for SMS received from Smsc
     * Default test timeout is used
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @param timeout       specific timeout
     * @return received SMS message
     */
    default SmscDeliverSmModel anySmsReceive(final SmscSessionId smscSessionId, CustomTimeout timeout)
    {
        return anySmsReceive(smscSessionId, 1, timeout).get(0);
    }

    /**
     * Waif for SMS received from Smsc
     * Default test timeout is used
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @param expectedCount number of SMS expected
     * @param timeout       specific timeout
     * @return received SMS message
     */
    List<SmscDeliverSmModel> anySmsReceive(final SmscSessionId smscSessionId, int expectedCount, CustomTimeout timeout);

    /**
     * Waif for received SMS delivery report from Smsc
     * Default test timeout is used
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @return received SMS delivery report
     */
    default SmscDeliveryReportModel anyDeliveryReport(final SmscSessionId smscSessionId)
    {
        return anyDeliveryReport(smscSessionId, Constants.DEFAULT_TIMEOUT);
    }

    /**
     * Waif for received SMS delivery report from Smsc
     * Default test timeout is used
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @param expectedCount number or delivery reports expected
     * @return received SMS delivery report
     */
    default List<SmscDeliveryReportModel> anyDeliveryReport(final SmscSessionId smscSessionId, int expectedCount)
    {
        return anyDeliveryReport(smscSessionId, expectedCount, Constants.DEFAULT_TIMEOUT);
    }


    /**
     * Waif for received SMS delivery report from Smsc
     * Default test timeout is used
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @param timeout       specific timeout
     * @return received SMS delivery report
     */
    default SmscDeliveryReportModel anyDeliveryReport(final SmscSessionId smscSessionId, CustomTimeout timeout)
    {
        return anyDeliveryReport(smscSessionId, 1, timeout).get(0);
    }

    /**
     * Waif for received SMS delivery report from Smsc
     * Default test timeout is used
     *
     * @param smscSessionId Smsc connection session id (you can get from SmscConnectResponseModel)
     * @param expectedCount number or delivery reports expected
     * @param timeout       specific timeout
     * @return received SMS delivery report
     */
    List<SmscDeliveryReportModel> anyDeliveryReport(final SmscSessionId smscSessionId, int expectedCount, CustomTimeout timeout);
}
