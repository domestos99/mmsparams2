package cz.uhk.dip.mmsparams.clientlib.communication.smsc;

import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.smsc.SmscGroup;

public class SmscGroupService implements ISmscGroupService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(SmscGroupService.class);

    private final ISmscService iSmscService;

    SmscGroupService(ISmscService iSmscService)
    {
        this.iSmscService = iSmscService;
    }

    @Override
    public SmscGroup sendSms(SmscConnectModel smscConnectModel, SmscSendModel smscSendModel)
    {
        ClientLogFacade.logAction(LOGGER, "Begin Send SMSC Group");
        SmscConnectResponseModel connect = this.iSmscService.connectToSMSC(smscConnectModel);
        SmscSessionId sessionId = connect.getSessionId();
        SmscSendSmsResponseModel sentSms = this.iSmscService.sendSms(sessionId, smscSendModel);

        SmscDeliveryReportModel dr = null;
        if (smscSendModel.isDeliveryReport())
            dr = this.iSmscService.waitFor().anyDeliveryReport(sessionId);

        this.iSmscService.disconnect(sessionId);
        ClientLogFacade.logAction(LOGGER, "Ending Send SMSC Group");

        SmscGroup group = new SmscGroup();
        group.setSmscSendModel(smscSendModel);
        group.setSmscSendSmsResponseModel(sentSms);
        group.setSmscDeliveryReportModel(dr);

        return group;
    }
}
