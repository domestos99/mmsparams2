package cz.uhk.dip.mmsparams.clientlib.communication.smsc;


import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDisconnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.validations.model.MessageIdValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.SmscConnectModelValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.SmscSendSmsModelValidator;

public class SmscService extends ServiceBase implements ISmscService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(SmscService.class);
    private final ISmscGroupService iSmscGroupService;
    private final ISmscServiceWait iSmscServiceWait;
    private final ISmscServiceMustTimeout iSmscServiceMustTimeout;

    public SmscService(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
        this.iSmscServiceWait = new SmscServiceWait(webSocketSender, webSocketReceiver);
        this.iSmscServiceMustTimeout = new SmscServiceMustTimeout(webSocketSender, webSocketReceiver);
        this.iSmscGroupService = new SmscGroupService(this);
    }

    @Override
    public ISmscServiceWait waitFor()
    {
        return this.iSmscServiceWait;
    }

    @Override
    public ISmscServiceMustTimeout mustTimeoutFor()
    {
        return this.iSmscServiceMustTimeout;
    }

    @Override
    public ISmscGroupService SmscGroup()
    {
        return this.iSmscGroupService;
    }

    @Override
    public SmscConnectResponseModel connectToSMSC(final SmscConnectModel smscConnectModel)
    {
        SmscConnectModelValidator.validate(smscConnectModel);

        SmscConnectMessage smscConnectMessage = MessageFactory.createServerRecipient(SmscConnectMessage.class);
        smscConnectMessage.setSmscSendSmsModel(smscConnectModel);

        ClientLogFacade.logAction(LOGGER, "Connecting to SMSC server");
        MessageId msgId = webSocketSender.sendMessage(smscConnectMessage);
        ClientLogFacade.logAction(LOGGER, "Waiting for SMSC connect confirmation");
        SmscConnectResponseMessage response = webSocketReceiver.waitForResponseMessage(msgId, SmscConnectResponseMessage.class);
        ClientLogFacade.logAction(LOGGER, "SMSC connected");
        return response.getSmscConnectResponseModel();
    }

    @Override
    public SmscSendSmsResponseModel sendSms(final SmscSessionId smscSessionId, final SmscSendModel smscSendModel)
    {
        MessageIdValidator.validate(smscSessionId);
        SmscSendSmsModelValidator.validate(smscSendModel);

        ClientLogFacade.logAction(LOGGER, "SMSC - sendSms");

        SmscSendSmsMessage smscSendSmsMessage = MessageFactory.createServerRecipient(SmscSendSmsMessage.class);

        smscSendSmsMessage.setSmscSendModel(smscSendModel);
        smscSendSmsMessage.setSmscSessionId(smscSessionId);

        MessageId msgId = webSocketSender.sendMessage(smscSendSmsMessage);

        SmscSendSmsResponseMessage response = webSocketReceiver.waitForResponseMessage(msgId, SmscSendSmsResponseMessage.class);
        return response.getSmscSendSmsResponseModel();
    }

    @Override
    public boolean disconnect(final SmscSessionId smscSessionId)
    {
        Preconditions.checkNotNull(smscSessionId, GenericConstants.SMSC_SESSION_ID);
        ClientLogFacade.logAction(LOGGER, "Smsc disconnecting");
        SmscDisconnectMessage msg = MessageFactory.createServerRecipient(SmscDisconnectMessage.class);
        msg.setSmscSessionId(smscSessionId);

        MessageId msgId = webSocketSender.sendMessage(msg);
        ClientLogFacade.logAction(LOGGER, "Smsc disconnected");

        GenericBooleanResponseMessage resp = this.webSocketReceiver.waitForGenericBooleanResponseMessage(msgId);
        return resp.isValue();
    }


}
