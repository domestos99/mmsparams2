package cz.uhk.dip.mmsparams.clientlib.communication.smsc;

import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.validations.model.CustomTimeoutValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.MessageIdValidator;

public class SmscServiceMustTimeout extends ServiceBase implements ISmscServiceMustTimeout
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(SmscServiceMustTimeout.class);

    SmscServiceMustTimeout(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public boolean smsReceive(SmscSessionId smscSessionId, CustomTimeout timeout)
    {
        MessageIdValidator.validate(smscSessionId);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "waiting for SmscDeliverSmMessage - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(smscSessionId, SmscDeliverSmMessage.class, timeout.getTimeoutSeconds());
    }

    @Override
    public boolean deliveryReport(final SmscSessionId smscSessionId, CustomTimeout timeout)
    {
        MessageIdValidator.validate(smscSessionId);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "waiting for SmscDeliveryReportMessage - must timeout");
        return webSocketReceiver.waitForResponseMessageMustTimeout(smscSessionId, SmscDeliveryReportMessage.class, timeout.getTimeoutSeconds());
    }
}
