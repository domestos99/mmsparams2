package cz.uhk.dip.mmsparams.clientlib.communication.smsc;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.validations.model.CustomTimeoutValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.MessageIdValidator;

public class SmscServiceWait extends ServiceBase implements ISmscServiceWait
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(SmscServiceWait.class);

    SmscServiceWait(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public List<SmscDeliverSmModel> anySmsReceive(final SmscSessionId smscSessionId, int expectedCount, CustomTimeout timeout)
    {
        MessageIdValidator.validate(smscSessionId);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "waiting for SmscDeliverSmMessage");
        List<SmscDeliverSmMessage> smsReceived = webSocketReceiver.waitForResponseMessage(smscSessionId, SmscDeliverSmMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<SmscDeliverSmModel> models = new ArrayList<>();
        if (smsReceived == null)
        {
            return models;
        }

        for (SmscDeliverSmMessage msg : smsReceived)
        {
            models.add(msg.getSmscDeliverSmModel());
        }
        return models;
    }

    @Override
    public List<SmscDeliveryReportModel> anyDeliveryReport(SmscSessionId smscSessionId, int expectedCount, CustomTimeout timeout)
    {
        MessageIdValidator.validate(smscSessionId);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "waiting for SmscDeliveryReportMessage");
        List<SmscDeliveryReportMessage> deliveryRepors = webSocketReceiver.waitForResponseMessage(smscSessionId, SmscDeliveryReportMessage.class, expectedCount, timeout.getTimeoutSeconds());

        List<SmscDeliveryReportModel> models = new ArrayList<>();
        if (deliveryRepors == null)
            return models;

        for (SmscDeliveryReportMessage msg : deliveryRepors)
        {
            models.add(msg.getSmscDeliveryReportModel());
        }

        return models;
    }
}
