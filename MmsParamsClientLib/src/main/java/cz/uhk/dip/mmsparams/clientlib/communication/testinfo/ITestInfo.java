package cz.uhk.dip.mmsparams.clientlib.communication.testinfo;

import java.util.List;

import cz.uhk.dip.mmsparams.api.messages.MessageRegisterItem;
import cz.uhk.dip.mmsparams.clientlib.TestDescription;

public interface ITestInfo
{

    /**
     * Returns duration between initNewTest and test finished (in milliseconds)
     *
     * @return test duration
     */
    long getTestDuration();

    /**
     * Returns current testID
     *
     * @return testID
     */
    String getTestId();

    /**
     * Returns current clientKey (client key for current test)
     *
     * @return clientKey
     */
    String getClientKey();

    /**
     * Returns all incoming messages receive during test
     *
     * @return all incoming message
     */
    List<MessageRegisterItem> getAllIncomingMessages();

    /**
     * Return incoming message with specific message key see {@link cz.uhk.dip.mmsparams.api.websocket.MessageType}
     *
     * @param messageKey message key
     * @return filtered incoming messages
     */
    List<MessageRegisterItem> getIncomingMessageByType(String messageKey);

    /**
     * Returns all outgoing messages sent during test
     *
     * @return all outgoint messages
     */
    List<MessageRegisterItem> getAllOutgoingMessages();

    /**
     * Return outgoing message with specific message key see {@link cz.uhk.dip.mmsparams.api.websocket.MessageType}
     *
     * @param messageKey message key
     * @return filtered outgoing messages
     */
    List<MessageRegisterItem> getOutgoingMessageByType(String messageKey);

    boolean getTestResult();

    void setTestResult(boolean testResult);

    /**
     * Return current test name and description
     * @return test name and description
     */
    TestDescription getTestDescription();

    void openTestInWebDetail();

}
