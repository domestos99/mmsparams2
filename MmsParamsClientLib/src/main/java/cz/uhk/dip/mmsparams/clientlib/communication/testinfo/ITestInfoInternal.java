package cz.uhk.dip.mmsparams.clientlib.communication.testinfo;

public interface ITestInfoInternal extends ITestInfo
{

    void testFinished();
}
