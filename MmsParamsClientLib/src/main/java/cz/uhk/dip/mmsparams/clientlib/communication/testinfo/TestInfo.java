package cz.uhk.dip.mmsparams.clientlib.communication.testinfo;

import java.awt.Desktop;
import java.net.URI;
import java.util.List;

import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.IMessageRegister;
import cz.uhk.dip.mmsparams.api.messages.MessageRegisterItem;
import cz.uhk.dip.mmsparams.clientlib.TestDescription;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;

public class TestInfo implements ITestInfoInternal
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(TestInfo.class);

    private long dtTestStart;
    private long dtTestEnd;
    private final String testId;
    private final String clientKey;
    private boolean testResult;
    private final TestDescription testDescription;
    private final WSConnectSettings wsConnectSettings;

    private final IMessageRegister iMessageRegister;

    public TestInfo(IMessageRegister messageRegister, WSConnectSettings wsConnectSettings, String testId, String clientKey, TestDescription testDescription)
    {
        iMessageRegister = messageRegister;
        this.wsConnectSettings = wsConnectSettings;
        this.testId = testId;
        this.clientKey = clientKey;
        this.testDescription = testDescription;
        this.dtTestStart = System.currentTimeMillis();
    }

    @Override
    public void testFinished()
    {
        this.dtTestEnd = System.currentTimeMillis();
    }

    @Override
    public long getTestDuration()
    {
        if (dtTestEnd == 0)
        {
            return System.currentTimeMillis() - dtTestStart;
        }
        else
        {
            return dtTestEnd - dtTestStart;
        }
    }

    @Override
    public String getTestId()
    {
        return this.testId;
    }

    @Override
    public String getClientKey()
    {
        return this.clientKey;
    }

    @Override
    public List<MessageRegisterItem> getAllIncomingMessages()
    {
        return this.iMessageRegister.getAllIncoming();
    }

    @Override
    public List<MessageRegisterItem> getIncomingMessageByType(String messageKey)
    {
        return this.iMessageRegister.getIncomByClz(messageKey);
    }

    @Override
    public List<MessageRegisterItem> getAllOutgoingMessages()
    {
        return this.iMessageRegister.getAllOutgoing();
    }

    @Override
    public List<MessageRegisterItem> getOutgoingMessageByType(String messageKey)
    {
        return this.iMessageRegister.getOutgoingByClz(messageKey);
    }

    @Override
    public boolean getTestResult()
    {
        return testResult;
    }

    @Override
    public void setTestResult(boolean testResult)
    {
        this.testResult = testResult;
    }

    @Override
    public TestDescription getTestDescription()
    {
        return this.testDescription;
    }

    @Override
    public void openTestInWebDetail()
    {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE))
        {
            try
            {
                Desktop.getDesktop().browse(new URI(this.wsConnectSettings.getServerAddressProvider().getServerAddress() + "/testinstances/" + this.testId));
            }
            catch (Exception e)
            {
                ClientLogFacade.logException(LOGGER, "openWebDetail", e);
            }
        }

    }
}
