package cz.uhk.dip.mmsparams.clientlib.communication.websocket;

import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.websocket.IWebSocketReceiver;

public interface IWebSocketHolder
{
    void init(IWebSocketReceiver iWebSocketReceiver, final IErrorHandler iErrorHandler, WSConnectSettings wsConnectSettings);

    boolean isWsNull();

    boolean send(String json);

    boolean close(int code, String reason);
}
