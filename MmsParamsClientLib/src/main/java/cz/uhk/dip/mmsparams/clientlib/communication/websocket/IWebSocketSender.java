package cz.uhk.dip.mmsparams.clientlib.communication.websocket;

import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public interface IWebSocketSender
{
    /**
     * Send message via WebSocket
     *
     * @param message message to send
     * @return sent message ID
     */
    MessageId sendMessage(WebSocketMessageBase message);

    /**
     * Send message via WebSocket (ignore test errors) - specific mode - dont use commonly
     *
     * @param message      message to send
     * @param ingoreErrors true if you want to ignore test errors
     * @return sent message ID
     */
    MessageId sendMessage(WebSocketMessageBase message, boolean ingoreErrors);

    /**
     * Returns test sender key
     *
     * @return sender key
     */
    String getSenderKey();
}
