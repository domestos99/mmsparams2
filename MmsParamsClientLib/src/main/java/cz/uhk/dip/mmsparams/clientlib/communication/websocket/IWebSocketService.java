package cz.uhk.dip.mmsparams.clientlib.communication.websocket;

import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public interface IWebSocketService
{
    void init(WSConnectSettings wsConnectSettings);

    MessageId sendMessage(WebSocketMessageBase message);

    String getSenderKey();

    MessageId sendMessage(WebSocketMessageBase message, boolean ignoreErrors);

    void disconnect();
}
