package cz.uhk.dip.mmsparams.clientlib.communication.websocket;

import java.util.List;

import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.Constants;

public interface IWebSocketWaiter
{
    default <T extends WebSocketMessageBase> T waitForResponseMessage(final MessageId messageID, final Class<T> clz)
    {
        return waitForResponseMessage(messageID, clz, Constants.USE_DEFAULT_TIMEOUT);
    }

    <T extends WebSocketMessageBase> T waitForResponseMessage(final MessageId messageID, final Class<T> clz, final int timeout);

    default <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(final MessageId messageID, Class<T> clz)
    {
        return waitForResponseMessageMustTimeout(messageID, clz, Constants.USE_DEFAULT_TIMEOUT);
    }

    <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(final MessageId messageID, Class<T> clz, final int timeout);

    default GenericBooleanResponseMessage waitForGenericBooleanResponseMessage(final MessageId messageID)
    {
        return waitForGenericBooleanResponseMessage(messageID, Constants.USE_DEFAULT_TIMEOUT);
    }

    default GenericBooleanResponseMessage waitForGenericBooleanResponseMessage(final MessageId messageID, final int timeout)
    {
        return waitForResponseMessage(messageID, GenericBooleanResponseMessage.class, timeout);
    }

    default boolean waitForGenericBooleanResponseMessageMustTimeout(final MessageId messageID)
    {
        return waitForGenericBooleanResponseMessageMustTimeout(messageID, Constants.USE_DEFAULT_TIMEOUT);
    }

    default boolean waitForGenericBooleanResponseMessageMustTimeout(final MessageId messageID, final int timeout)
    {
        return waitForResponseMessageMustTimeout(messageID, GenericBooleanResponseMessage.class, timeout);
    }

    default <T extends WebSocketMessageBase> T waitForResponseMessage(final PhoneInfoModel phoneInfoModel, final Class<T> clz)
    {
        return waitForResponseMessage(phoneInfoModel, clz, 1).get(0);
    }

    default <T extends WebSocketMessageBase> List<T> waitForResponseMessage(final PhoneInfoModel phoneInfoModel, final Class<T> clz, final int expectedCount)
    {
        return waitForResponseMessage(phoneInfoModel, clz, expectedCount, Constants.USE_DEFAULT_TIMEOUT);
    }

    <T extends WebSocketMessageBase> List<T> waitForResponseMessage(final PhoneInfoModel phoneInfoModel, final Class<T> clz, final int expectedCount, final int timeout);

    default <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(final PhoneInfoModel phoneInfoModel, final Class<T> clz)
    {
        return waitForResponseMessageMustTimeout(phoneInfoModel, clz, Constants.USE_DEFAULT_TIMEOUT);
    }

    <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(final PhoneInfoModel phoneInfoModel, final Class<T> clz, final int timeout);

    default <T extends WebSocketMessageBase> T waitForResponseMessage(final Class<T> clz)
    {
        return waitForResponseMessage(clz, 1).get(0);
    }

    default <T extends WebSocketMessageBase> List<T> waitForResponseMessage(final Class<T> clz, final int expectedCount)
    {
        return waitForResponseMessage(clz, expectedCount, Constants.USE_DEFAULT_TIMEOUT);
    }

    <T extends WebSocketMessageBase> List<T> waitForResponseMessage(final Class<T> clz, final int expectedCount, final int timeout);


    default <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(final Class<T> clz)
    {
        return waitForResponseMessageMustTimeout(clz, Constants.USE_DEFAULT_TIMEOUT);
    }

    <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(Class<T> clz, final int timeout);


    default <T extends WebSocketMessageBase> T waitForResponseMessage(final SmscSessionId smscSessionId, final Class<T> clz)
    {
        return waitForResponseMessage(smscSessionId, clz, 1).get(0);
    }

    default <T extends WebSocketMessageBase> List<T> waitForResponseMessage(final SmscSessionId smscSessionId, final Class<T> clz, final int expectedCount)
    {
        return waitForResponseMessage(smscSessionId, clz, expectedCount, Constants.USE_DEFAULT_TIMEOUT);
    }

    <T extends WebSocketMessageBase> List<T> waitForResponseMessage(final SmscSessionId smscSessionId, final Class<T> clz, final int expectedCount, final int timeout);

    default <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(final SmscSessionId smscSessionId, final Class<T> clz)
    {
        return waitForResponseMessageMustTimeout(smscSessionId, clz, Constants.USE_DEFAULT_TIMEOUT);
    }

    <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(final SmscSessionId smscSessionId, final Class<T> clz, final int timeout);


}
