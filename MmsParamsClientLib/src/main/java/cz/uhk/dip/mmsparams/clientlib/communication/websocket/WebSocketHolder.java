package cz.uhk.dip.mmsparams.clientlib.communication.websocket;

import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.exceptions.TestInvalidException;
import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.http.auth.JwtResponse;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.validation.JwtResponseValidator;
import cz.uhk.dip.mmsparams.clientlib.communication.http.HttpUtils;
import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.websocket.IWebSocketReceiver;
import cz.uhk.dip.mmsparams.clientlib.websocket.WebSocketHandler;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

public class WebSocketHolder implements IWebSocketHolder
{
    private static final String TAG = WebSocketHolder.class.getSimpleName();
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(WebSocketHolder.class);

    private OkHttpClient client;
    private WebSocket ws;

    public WebSocketHolder()
    {
        client = new OkHttpClient();
    }

    @Override
    public void init(final IWebSocketReceiver iWebSocketReceiver, final IErrorHandler iErrorHandler, final WSConnectSettings wsConnectSettings)
    {
        this.connectToServer(iWebSocketReceiver, iErrorHandler, wsConnectSettings);
    }

    public static WebSocketHolder createDefault()
    {
        return new WebSocketHolder();
    }

    private synchronized void connectToServer(final IWebSocketReceiver iWebSocketReceiver, final IErrorHandler iErrorHandler,
                                              final WSConnectSettings wsConnectSettings)
    {
        ClientLogFacade.logAction(LOGGER, "Starting authentication");

        final JwtResponse token = HttpUtils.getAuthToken(wsConnectSettings);

        if (!JwtResponseValidator.validate(token))
        {
            throw new TestInvalidException("Unable to authenticate");
        }
        ClientLogFacade.logAction(LOGGER, "Authentication successful");

        Request request = new Request.Builder()
                .url(wsConnectSettings.getServerAddressProvider().getWSAddress())
                .addHeader(HttpConstants.AUTHORIZATION, HttpConstants.getAuthHeaderValue(token.getToken()))
                .build();
        WebSocketHandler listener = new WebSocketHandler(iWebSocketReceiver, iErrorHandler);
        ws = client.newWebSocket(request, listener);
        client.dispatcher().executorService().shutdown();
    }

    @Override
    public synchronized boolean isWsNull()
    {
        return ws == null;
    }

    @Override
    public synchronized boolean send(final String json)
    {
        return ws.send(json);
    }

    @Override
    public synchronized boolean close(int code, String reason)
    {
        return ws.close(code, reason);
    }
}
