package cz.uhk.dip.mmsparams.clientlib.communication.websocket;

import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class WebSocketSender implements IWebSocketSender
{
    private final IWebSocketService iWebSocketService;

    public WebSocketSender(IWebSocketService iWebSocketService)
    {
        this.iWebSocketService = iWebSocketService;
    }

    @Override
    public MessageId sendMessage(WebSocketMessageBase message)
    {
        return this.iWebSocketService.sendMessage(message);
    }

    @Override
    public MessageId sendMessage(WebSocketMessageBase message, boolean ingoreErrors)
    {
        return this.iWebSocketService.sendMessage(message, ingoreErrors);
    }

    @Override
    public String getSenderKey()
    {
        return iWebSocketService.getSenderKey();
    }


}

