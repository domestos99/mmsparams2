package cz.uhk.dip.mmsparams.clientlib.communication.websocket;


import javax.annotation.Nullable;

import cz.uhk.dip.mmsparams.api.constants.MemoryConstants;
import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.interfaces.IClientKeyProvider;
import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.IMessageRegister;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketCloseStatus;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.websocket.IWebSocketReceiver;

public class WebSocketService extends WebSocketServiceBase implements IWebSocketService
{
    private final IWebSocketHolder webSocketHolder;
    private final IClientKeyProvider senderKeyProvider;
    private final IWebSocketReceiver iWebSocketReceiver;
    private final IMessageRegister messageRegister;
    private final ITestIdProvider iTestIdProvider;
    private final IErrorHandler iErrorHandler;

    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(WebSocketService.class);

    public WebSocketService(IWebSocketHolder webSocketHolder, IWebSocketReceiver iWebSocketReceiver, IMessageRegister messageRegister,
                            ITestIdProvider iTestIdProvider, IClientKeyProvider iClientKeyProvider, IErrorHandler iErrorHandler)
    {
        this.webSocketHolder = webSocketHolder;
        this.iTestIdProvider = iTestIdProvider;
        this.messageRegister = messageRegister;
        this.iErrorHandler = iErrorHandler;
        this.senderKeyProvider = iClientKeyProvider;
        this.iWebSocketReceiver = iWebSocketReceiver;
    }

    @Override
    public void init(WSConnectSettings wsConnectSettings)
    {
        webSocketHolder.init(iWebSocketReceiver, iErrorHandler, wsConnectSettings);
    }

    @Override
    public String getSenderKey()
    {
        return senderKeyProvider.getClientKey();
    }

    @Override
    public MessageId sendMessage(WebSocketMessageBase message)
    {
        return sendMessage(message, false);
    }

    @Override
    @Nullable
    public MessageId sendMessage(WebSocketMessageBase message, boolean ignoreErrors)
    {
        if (!ignoreErrors)
            iErrorHandler.checkErrorThrow();

        if (webSocketHolder.isWsNull())
        {
            return null;
        }
        if (message == null)
        {
            return null;
        }

        String json = super.getSendableMessage(iTestIdProvider, message);

        if (StringUtil.isEmptyOrNull(json))
        {
            ClientLogFacade.logError(LOGGER, "Cannot send empty json!!! Message: " + message);
            return null;
        }

        if (json.length() >= MemoryConstants.MB16)
        {
            ClientLogFacade.logWarning(LOGGER, "JSON may be too large to send. Size: " + json.length());
        }

        if (!MessageUtils.isMessageValid(message))
        {
            ClientLogFacade.logError(LOGGER, "Message is not valid: " + message);
        }

        messageRegister.insertOutgoingMsg(message);

        sendJson(json);

        return new MessageId(message.getMessageID());
    }

    @Override
    protected void sendJson(String json)
    {
        log(json);
        webSocketHolder.send(json);
    }

    private void log(String json)
    {
        ClientLogFacade.logWsOutgoingMessage(LOGGER, "Sending: " + json);
    }

    @Override
    public void disconnect()
    {
        ClientLogFacade.logAction(LOGGER, "Disconnection web socket...");
        if (!webSocketHolder.isWsNull())
        {
            webSocketHolder.close(WebSocketCloseStatus.NORMAL, "Test finished");
        }
        ClientLogFacade.logAction(LOGGER, "Web socket disconnected.");
    }


}
