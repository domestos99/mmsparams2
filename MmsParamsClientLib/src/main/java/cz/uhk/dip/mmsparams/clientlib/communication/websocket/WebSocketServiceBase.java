package cz.uhk.dip.mmsparams.clientlib.communication.websocket;

import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.clientlib.utils.MessageIDProvider;

public abstract class WebSocketServiceBase implements IWebSocketService
{
    protected String getSendableMessage(final ITestIdProvider testIdProvider, final WebSocketMessageBase message)
    {
        message.setSenderKey(getSenderKey());
        message.setMessageID(MessageIDProvider.generateMessageID());
        message.setTestID(testIdProvider.getTestId());
        return MessageUtils.getSendableMessage(message);
    }

    protected abstract void sendJson(String json);
}
