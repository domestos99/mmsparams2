package cz.uhk.dip.mmsparams.clientlib.communication.websocket;

import java.util.Date;
import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.IMessageRegister;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.messages.MessageRegisterItem;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.utils.TimeoutHelper;
import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.utils.ThreadUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.model.ClassValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.CustomTimeoutValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.MessageIdValidator;
import cz.uhk.dip.mmsparams.clientlib.validations.model.PhoneInfoModelValidator;

public class WebSocketWaiter2 implements IWebSocketWaiter
{
    private static final String TAG = WebSocketWaiter2.class.getSimpleName();
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(WebSocketWaiter2.class);

    private final IMessageRegister messageRegister;
    private final TestSettings testSettings;
    private final IErrorHandler iErrorHandler;

    public WebSocketWaiter2(final IMessageRegister messageRegister, final TestSettings testSettings, IErrorHandler iErrorHandler)
    {
        this.testSettings = testSettings;
        this.messageRegister = messageRegister;
        this.iErrorHandler = iErrorHandler;
    }

    @Override
    public <T extends WebSocketMessageBase> T waitForResponseMessage(MessageId messageID, Class<T> clz, int timeout)
    {
        MessageIdValidator.validate(messageID);
        ClassValidator.validate(clz);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "waitForResponseMessage: " + getInfo(messageID, clz, timeout));

        int correctTimeout = correctTimeout(timeout);
        String messageKey = MessageType.getKeyByClass(clz);

        // Wait for OK confiramation
        Date startDt = new Date();

        WebSocketMessageBase response = null;

        while ((response = this.messageRegister.getIncomByMsgId(messageID, messageKey)) == null)
        {
            iErrorHandler.checkErrorThrow();

            if (TimeoutHelper.checkTimeOut(startDt, correctTimeout))
            {
                iErrorHandler.throwTimeOut(TAG, GenericConstants.WAIT_FOR_RESPONSE_MESSAGE);
            }
            ThreadUtils.sleep(testSettings.getDefaultWaitSleepLoop());

        }
        return (T) response;
    }


    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(MessageId messageID, Class<T> clz, int timeout)
    {
        MessageIdValidator.validate(messageID);
        ClassValidator.validate(clz);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "waitForResponseMessageMustTimeout: " + getInfo(messageID, clz, timeout));
        int correctTimeout = correctTimeout(timeout);
        String messageKey = MessageType.getKeyByClass(clz);

        // Wait for OK confiramation
        Date startDt = new Date();

        while (this.messageRegister.getIncomByMsgId(messageID, messageKey) == null)
        {
            iErrorHandler.checkErrorThrow();
            if (TimeoutHelper.checkTimeOut(startDt, correctTimeout))
            {
                return true;
            }
            ThreadUtils.sleep(testSettings.getDefaultWaitSleepLoop());
        }
        return false;
    }

    @Override
    public <T extends WebSocketMessageBase> List<T> waitForResponseMessage(PhoneInfoModel phoneInfoModel, Class<T> clz, int expectedCount, int timeout)
    {
        PhoneInfoModelValidator.validate(phoneInfoModel, GenericConstants.PHONE_INFO_MODEL);
        ClassValidator.validate(clz);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "waitForResponseMessage: " + getInfo(phoneInfoModel, clz, timeout));
        int correctTimeout = correctTimeout(timeout);
        String messageKey = MessageType.getKeyByClass(clz);
        Date startDt = new Date();

        List<WebSocketMessageBase> response = null;
        while (((response = this.messageRegister.getIncomByDeviceId(phoneInfoModel, messageKey)).size() != expectedCount))
        {
            iErrorHandler.checkErrorThrow();
            if (TimeoutHelper.checkTimeOut(startDt, correctTimeout))
            {
                iErrorHandler.throwTimeOut(TAG, GenericConstants.WAIT_FOR_RESPONSE_MESSAGE);
            }
            ThreadUtils.sleep(testSettings.getDefaultWaitSleepLoop());
        }
        return MessageUtils.copyList(response);
    }

    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(PhoneInfoModel phoneInfoModel, Class<T> clz, int timeout)
    {
        PhoneInfoModelValidator.validate(phoneInfoModel);
        ClassValidator.validate(clz);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "waitForResponseMessageMustTimeout: " + getInfo(phoneInfoModel, clz, timeout));
        int correctTimeout = correctTimeout(timeout);
        String messageKey = MessageType.getKeyByClass(clz);

        // Wait for OK confiramation
        Date startDt = new Date();

        while (this.messageRegister.getIncomByDeviceId(phoneInfoModel, messageKey).isEmpty())
        {
            iErrorHandler.checkErrorThrow();
            if (TimeoutHelper.checkTimeOut(startDt, correctTimeout))
            {
                return true;
            }
            ThreadUtils.sleep(testSettings.getDefaultWaitSleepLoop());
        }
        return false;
    }

    @Override
    public <T extends WebSocketMessageBase> List<T> waitForResponseMessage(Class<T> clz, int expectedCount, int timeout)
    {
        ClassValidator.validate(clz);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);

        ClientLogFacade.logAction(LOGGER, "waitForResponseMessage: " + getInfo(clz, expectedCount, timeout));
        int correctTimeout = correctTimeout(timeout);
        String messageKey = MessageType.getKeyByClass(clz);
        Date startDt = new Date();

        List<MessageRegisterItem> response = null;
        while ((response = this.messageRegister.getIncomByClz(messageKey)).size() != expectedCount)
        {
            iErrorHandler.checkErrorThrow();
            if (TimeoutHelper.checkTimeOut(startDt, correctTimeout))
            {
                iErrorHandler.throwTimeOut(TAG, "waitForResponseMessage");
            }
            ThreadUtils.sleep(testSettings.getDefaultWaitSleepLoop());
        }
        return MessageUtils.copyList(MessageRegisterItem.getOnlyMessages(response));
    }

    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(Class<T> clz, int timeout)
    {
        ClassValidator.validate(clz);
        Preconditions.isGreaterZeroOrDefault(timeout, GenericConstants.TIMEOUT);

        ClientLogFacade.logAction(LOGGER, "waitForResponseMessageMustTimeout: " + getInfo(clz, timeout));
        int correctTimeout = correctTimeout(timeout);
        String messageKey = MessageType.getKeyByClass(clz);

        // Wait for OK confiramation
        Date startDt = new Date();

        while (this.messageRegister.getIncomByClz(messageKey).isEmpty())
        {
            iErrorHandler.checkErrorThrow();
            if (TimeoutHelper.checkTimeOut(startDt, correctTimeout))
            {
                return true;
            }
            ThreadUtils.sleep(testSettings.getDefaultWaitSleepLoop());
        }
        return false;
    }

    @Override
    public <T extends WebSocketMessageBase> List<T> waitForResponseMessage(SmscSessionId smscSessionId, Class<T> clz, int expectedCount, int timeout)
    {
        MessageIdValidator.validate(smscSessionId);
        ClassValidator.validate(clz);
        Preconditions.isGreaterZero(expectedCount, GenericConstants.EXPECTED_COUNT);
        CustomTimeoutValidator.validate(timeout);


        ClientLogFacade.logAction(LOGGER, "waitForResponseMessage: " + getInfo(smscSessionId, clz, expectedCount, timeout));
        int correctTimeout = correctTimeout(timeout);
        String messageKey = MessageType.getKeyByClass(clz);
        Date startDt = new Date();

        List<WebSocketMessageBase> response = null;
        while (((response = this.messageRegister.getIncomBySmscSessionId(smscSessionId, messageKey)).size() != expectedCount))
        {
            iErrorHandler.checkErrorThrow();
            if (TimeoutHelper.checkTimeOut(startDt, correctTimeout))
            {
                iErrorHandler.throwTimeOut(TAG, "waitForResponseMessage");
            }
            ThreadUtils.sleep(testSettings.getDefaultWaitSleepLoop());
        }
        return MessageUtils.copyList(response);
    }

    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(SmscSessionId smscSessionId, Class<T> clz, int timeout)
    {
        MessageIdValidator.validate(smscSessionId);
        ClassValidator.validate(clz);
        CustomTimeoutValidator.validate(timeout);

        int correctTimeout = correctTimeout(timeout);
        ClientLogFacade.logAction(LOGGER, "waitForResponseMessageMustTimeout: " + getInfo(smscSessionId, clz, correctTimeout));
        String messageKey = MessageType.getKeyByClass(clz);

        // Wait for OK confiramation
        Date startDt = new Date();

        while (this.messageRegister.getIncomBySmscSessionId(smscSessionId, messageKey).isEmpty())
        {
            iErrorHandler.checkErrorThrow();
            if (TimeoutHelper.checkTimeOut(startDt, correctTimeout))
            {
                return true;
            }
            ThreadUtils.sleep(testSettings.getDefaultWaitSleepLoop());
        }
        return false;
    }


    private int correctTimeout(final int timeout)
    {
        if (timeout < 0)
            return testSettings.getDefaultTimeout();
        return timeout;
    }


    private <T extends WebSocketMessageBase> String getInfo(MessageId messageID, Class<T> clz, int timeout)
    {
        return clz.getSimpleName() + ": " + messageID.getMessageId() + ": " + timeout;
    }

    private <T extends WebSocketMessageBase> String getInfo(SmscSessionId smscSessionId, Class<T> clz, int timeout)
    {
        return getInfo(smscSessionId, clz, null, timeout);
    }

    private <T extends WebSocketMessageBase> String getInfo(SmscSessionId smscSessionId, Class<T> clz, Integer expectedCount, int timeout)
    {
        return clz.getSimpleName() + ": " + smscSessionId.getSmscSessionId() + ": " + expectedCount + ": " + timeout + " seconds";
    }

    private <T extends WebSocketMessageBase> String getInfo(Class<T> clz, int timeout)
    {
        return getInfo(clz, null, timeout);
    }

    private <T extends WebSocketMessageBase> String getInfo(Class<T> clz, Integer expectedCount, int timeout)
    {
        return clz.getSimpleName() + ": " + expectedCount + ": " + timeout;
    }

    private <T extends WebSocketMessageBase> String getInfo(PhoneInfoModel phoneInfoModel, Class<T> clz, int timeout)
    {
        return clz.getSimpleName() + ": " + phoneInfoModel.getPhoneKey() + ": " + timeout;
    }

}
