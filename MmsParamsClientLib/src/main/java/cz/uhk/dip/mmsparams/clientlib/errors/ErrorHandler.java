package cz.uhk.dip.mmsparams.clientlib.errors;


import javax.annotation.Nonnull;

import cz.uhk.dip.mmsparams.api.errors.TestErrorBuilder;
import cz.uhk.dip.mmsparams.api.exceptions.MmsParamsExceptionBase;
import cz.uhk.dip.mmsparams.api.exceptions.TestErrorException;
import cz.uhk.dip.mmsparams.api.exceptions.TimeOutException;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.utils.LogUtil;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.TestErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;

/**
 * Handles errors occurred in test and when error occurs, stops the test
 */
public class ErrorHandler implements IErrorHandler
{
    // Error message from server
    private GenericErrorResponseMessage error;
    // Exception from Clientlib (local)
    private TestErrorModel clientLibLocalException;
    private IWebSocketSender iWebSocketSender;

    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(ErrorHandler.class);

    @Override
    public synchronized void setWebSocketSender(IWebSocketSender iWebSocketSender)
    {
        this.iWebSocketSender = iWebSocketSender;
    }

    @Override
    public synchronized void checkErrorThrow()
    {
        if (isErrorPresent())
        {
            StringBuilder sb = new StringBuilder();

            ClientLogFacade.logError(LOGGER, "Test Error" + LogUtil.NEW_LINE);

            if (clientLibLocalException != null)
            {
                // Lokalni vyjimka z WebSocketu (puvodem asynchroni)
                sb.append(ExceptionHelper.getStackTrace(clientLibLocalException.getException()));
                trySendErrorToServer(clientLibLocalException);
            }
            else
            {
                sb.append(getError());
                trySendErrorToServer(this.error.getTestErrorModel());
            }
            sb.append("\n---------------------------\n");
            ClientLogFacade.logError(LOGGER, sb.toString());


            if (clientLibLocalException != null)
            {
                throw ExceptionHelper.castException(clientLibLocalException.getException());
            }
            else
            {
                throw new TestErrorException(getError().toString());
            }
        }
    }

    @Override
    public synchronized boolean isErrorPresent()
    {
        return error != null || clientLibLocalException != null;
    }

    @Override
    public synchronized void setError(GenericErrorResponseMessage errorMessage)
    {
        this.error = errorMessage;
    }

    @Override
    public synchronized GenericErrorResponseMessage getError()
    {
        return error;
    }

    @Override
    public synchronized void throwTimeOut(@Nonnull String classTag, @Nonnull String info)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("TIMEOUT").append(LogUtil.NEW_LINE);
        sb.append(classTag).append(": ").append(info);
        ClientLogFacade.logError(LOGGER, sb.toString());
        throw new TimeOutException(sb.toString());
    }

    private synchronized void trySendErrorToServer(final TestErrorModel testErrorModel)
    {
        if (iWebSocketSender == null)
            return;

        try
        {
            TestErrorMessage msg = MessageFactory.createServerRecipient(TestErrorMessage.class);
            msg.setTestErrorModel(testErrorModel);
            iWebSocketSender.sendMessage(msg, true);
        }
        catch (Exception ex)
        {
            ClientLogFacade.logWarning(LOGGER, ExceptionHelper.getStackTrace(ex));
        }
    }


    @Override
    public synchronized void notifyNewUncaughtException(@Nonnull MmsParamsExceptionBase ex)
    {
        // Posledni krok, nez aplikace spadne na neosetrenou vyjimku
        TestErrorModel errorModel = TestErrorBuilder.create().withAll(ex).build();
        trySendErrorToServer(errorModel);
    }

    @Override
    public synchronized void notifyWebSocketError(@Nonnull String classTag, @Nonnull String message, @Nonnull MmsParamsExceptionBase ex)
    {
        // Kdyz spadne WebSocket only
        ClientLogFacade.logException(LOGGER, message, ex);

        final TestErrorModel errorModel = new TestErrorModel();
        errorModel.setException(ex);
        errorModel.setMessage(message);
        errorModel.setTestErrorType(ex.getTestErrorType());
        this.clientLibLocalException = errorModel;
    }
}
