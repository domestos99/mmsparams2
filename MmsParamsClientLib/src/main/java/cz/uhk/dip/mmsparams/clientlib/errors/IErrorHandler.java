package cz.uhk.dip.mmsparams.clientlib.errors;

import javax.annotation.Nonnull;

import cz.uhk.dip.mmsparams.api.exceptions.MmsParamsExceptionBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;

public interface IErrorHandler
{
    void checkErrorThrow();

    boolean isErrorPresent();

    void setError(GenericErrorResponseMessage errorMessage);

    GenericErrorResponseMessage getError();

    void throwTimeOut(@Nonnull String classTag, @Nonnull String info);

    void setWebSocketSender(IWebSocketSender iWebSocketSender);

    void notifyNewUncaughtException(@Nonnull MmsParamsExceptionBase ex);

    void notifyWebSocketError(@Nonnull String classTag, @Nonnull String message, @Nonnull MmsParamsExceptionBase ex);
}
