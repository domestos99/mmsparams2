package cz.uhk.dip.mmsparams.clientlib.groups;

import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.TestGroupModel;

public class TestGroupSingleton
{
    private static final TestGroupSingleton instance = new TestGroupSingleton();

    private TestGroupModel testGroupModel = null;

    //private constructor to avoid client applications to use constructor
    private TestGroupSingleton()
    {
    }

    public static TestGroupSingleton getInstance()
    {
        return instance;
    }

    public TestGroupModel getTestGroupModel()
    {
        return testGroupModel;
    }

    public void setTestGroupModel(TestGroupModel testGroupModel)
    {
        this.testGroupModel = testGroupModel;
    }

    public void clearTestGroup()
    {
        setTestGroupModel(null);
    }
}
