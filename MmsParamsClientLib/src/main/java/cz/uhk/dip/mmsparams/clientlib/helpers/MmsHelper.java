package cz.uhk.dip.mmsparams.clientlib.helpers;

import javax.annotation.Nullable;

import cz.uhk.dip.mmsparams.api.constants.ContentTypeConstants;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.PduPartModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendReqModel;

public class MmsHelper
{
    private MmsHelper()
    {
    }

    @Nullable
    public static String tryGetMessageTest(SendReqModel sendReqModel)
    {
        if (sendReqModel == null)
            return null;

        return tryGetMessageTest(sendReqModel.getParts());
    }

    @Nullable
    public static String tryGetMessageTest(RetrieveConfModel receivedMMS)
    {
        if (receivedMMS == null)
            return null;

        return tryGetMessageTest(receivedMMS.getParts());
    }

    @Nullable
    public static String tryGetMessageTest(PduPartModel[] parts)
    {
        if (parts == null)
            return null;

        for (PduPartModel part : parts)
        {
            if (ContentTypeConstants.PLAIN_TEXT.equals(part.getContentType()))
            {
                return new String(part.getData());
            }
        }
        return null;
    }

    @Nullable
    public static PduPartModel tryGetPduPart(PduPartModel[] parts, String contentType)
    {
        if (parts == null || StringUtil.isEmptyOrNull(contentType))
            return null;

        for (PduPartModel p : parts)
        {
            if (contentType.equals(p.getContentType()))
                return p;
        }
        return null;
    }
}
