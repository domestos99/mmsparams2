package cz.uhk.dip.mmsparams.clientlib.logger;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.ConsoleUtil;
import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;

public class ClientLogFacade
{
    private static ClientLogLevel _clientLogLevel;
    private static List<IClientCustomLogger> customLoggers = new ArrayList<>();

    private ClientLogFacade()
    {
    }

    static
    {
        _clientLogLevel = ClientLogLevel.LOG_ONLY_INFO;
    }

    /**
     * Add custom logger to log to. Default logger will be used as well
     *
     * @param customLogger client custom logger
     */
    public static void addCustomLogger(IClientCustomLogger customLogger)
    {
        Preconditions.checkNotNull(customLogger, GenericConstants.CUSTOM_LOGGER);
        customLoggers.add(customLogger);
    }

    public static void setClientLogLevel(final ClientLogLevel clientLogLevel)
    {
        _clientLogLevel = clientLogLevel;
    }

    public static void logAction(final ILogger logger, String action)
    {
        logActionWithColor(logger, action, ConsoleUtil.GREEN);

    }

    public static void logActionWithColor(final ILogger logger, final String action, final String greenBackground)
    {
        logger.info(ConsoleUtil.getLogWithColor(action, greenBackground));
        for (IClientCustomLogger customLogger : customLoggers)
        {
            customLogger.info(action);
        }
    }

    public static void logWarning(ILogger logger, String warning, Throwable t)
    {
        if (_clientLogLevel == ClientLogLevel.LOG_ALL)
        {
            logWarning(logger, warning + ";" + ExceptionHelper.getStackTrace(t));
        }
    }

    public static void logWarning(final ILogger logger, final String warning)
    {
        logger.warn(ConsoleUtil.getLogWithColor(warning, ConsoleUtil.RED));
        for (IClientCustomLogger customLogger : customLoggers)
        {
            customLogger.warn(warning);
        }
    }

    public static void logException(final ILogger logger, final Throwable t)
    {
        logger.error(t);
        for (IClientCustomLogger customLogger : customLoggers)
        {
            customLogger.error(t);
        }
//        String ex = ExceptionHelper.getStackTrace(t);
//        logWarning(logger, tag, ex);
    }

    public static void logException(final ILogger logger, final String message, final Throwable t)
    {
        logger.error(message, t);
        for (IClientCustomLogger customLogger : customLoggers)
        {
            customLogger.error(message, t);
        }
    }

    public static void logError(final ILogger logger, final String error)
    {
        logger.error(error);
        for (IClientCustomLogger customLogger : customLoggers)
        {
            customLogger.error(error);
        }
    }

    public static void logWsIncomingMessage(final ILogger logger, final String message)
    {
        if (_clientLogLevel == ClientLogLevel.LOG_ALL)
        {
            logger.logIncomingMessage(message);
            for (IClientCustomLogger customLogger : customLoggers)
            {
                customLogger.logIncomingMessage(message);
            }
        }
    }

    public static void logWsUnknownMessage(final ILogger logger, final String message)
    {
        logger.logUnknownMessage(message);
        for (IClientCustomLogger customLogger : customLoggers)
        {
            customLogger.logUnknownMessage(message);
        }
    }

    public static void logWsErrorMessage(final ILogger logger, final GenericErrorResponseMessage errorMessage)
    {
        logger.logErrorMessage(errorMessage.toString());
        for (IClientCustomLogger customLogger : customLoggers)
        {
            customLogger.logErrorMessage(errorMessage.toString());
        }
    }

    public static void logWsOutgoingMessage(final ILogger logger, final String message)
    {
        if (_clientLogLevel == ClientLogLevel.LOG_ALL)
        {
            logger.logOutgoingMessage(message);
            for (IClientCustomLogger customLogger : customLoggers)
            {
                customLogger.logOutgoingMessage(message);
            }
        }
    }

    public static void clearAll()
    {
        _clientLogLevel = ClientLogLevel.LOG_ONLY_INFO;
        customLoggers.clear();
    }


}
