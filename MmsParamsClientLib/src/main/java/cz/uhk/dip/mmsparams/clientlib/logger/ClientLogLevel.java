package cz.uhk.dip.mmsparams.clientlib.logger;

import java.io.Serializable;

/**
 * Represents how many detail log on client
 */
public enum ClientLogLevel implements Serializable
{
    /**
     * Log all actions and message
     */
    LOG_ALL,
    /**
     * Log only actions
     */
    LOG_ONLY_INFO
}
