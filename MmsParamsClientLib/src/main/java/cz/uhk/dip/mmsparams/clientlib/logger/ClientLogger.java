package cz.uhk.dip.mmsparams.clientlib.logger;


import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.ConsoleUtil;
import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;

public class ClientLogger<T> implements ILogger
{
    private final Class<T> clazz;
    private final String clzTag;

    public ClientLogger(final Class<T> clz)
    {
        Preconditions.checkNotNull(clz, "clz");
        this.clazz = clz;
        this.clzTag = clz.getSimpleName();
    }

    @Override
    public void info(String info)
    {
        System.out.println(getLog(clzTag, info));
    }

    @Override
    public void warn(String warning)
    {
        System.out.println(ConsoleUtil.getLogWithColor(warning, ConsoleUtil.RED));
    }

    @Override
    public void error(Throwable t)
    {
        String ex = ExceptionHelper.getStackTrace(t);
        System.out.println(ConsoleUtil.getLogWithColor(getLog(clzTag, ex), ConsoleUtil.RED_BACKGROUND));

    }

    @Override
    public void error(String message, Throwable t)
    {
        String ex = ExceptionHelper.getStackTrace(t);
        System.out.println(ConsoleUtil.getLogWithColor(getLog(clzTag, message + ";" + ex), ConsoleUtil.RED_BACKGROUND));
    }

    @Override
    public void error(String error)
    {
        System.out.println(ConsoleUtil.getLogWithColor(getLog(clzTag, error), ConsoleUtil.RED_BACKGROUND));
    }

    @Override
    public void logIncomingMessage(String messageJson)
    {
        info(messageJson);
    }

    @Override
    public void logOutgoingMessage(String messageJson)
    {
        info(messageJson);
    }

    private String getLog(String tag, String msg)
    {
        return tag + ": " + msg;
    }
}
