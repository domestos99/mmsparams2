package cz.uhk.dip.mmsparams.clientlib.logger;

import cz.uhk.dip.mmsparams.api.logging.ILogger;

public class ClientLoggerFactory
{
    private ClientLoggerFactory()
    {
    }

    public static <T> ILogger getLogger(Class<T> clz)
    {
        return new ClientLogger<T>(clz);
    }
}
