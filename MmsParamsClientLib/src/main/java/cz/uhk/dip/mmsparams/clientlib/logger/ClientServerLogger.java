package cz.uhk.dip.mmsparams.clientlib.logger;

import cz.uhk.dip.mmsparams.api.enums.LogLevel;
import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.web.model.ClientServerLogItem;
import cz.uhk.dip.mmsparams.api.web.model.ClientServerLogModel;

public class ClientServerLogger implements IClientCustomLogger
{
    private final ClientServerLogModel clientServerLogModel;

    public ClientServerLogger(String testID)
    {
        this.clientServerLogModel = new ClientServerLogModel();
        this.clientServerLogModel.setTestID(testID);
    }

    @Override
    public void info(String info)
    {
        addItem(info, LogLevel.INFO);
    }

    @Override
    public void warn(String warning)
    {
        addItem(warning, LogLevel.WARNING);
    }

    @Override
    public void error(Throwable t)
    {
        String ex = ExceptionHelper.getStackTrace(t);
        addItem(ex, LogLevel.ERROR);
    }

    @Override
    public void error(String error)
    {
        addItem(error, LogLevel.ERROR);
    }

    @Override
    public void error(String message, Throwable t)
    {
        String ex = ExceptionHelper.getStackTrace(t);
        addItem(message + ";" + ex, LogLevel.ERROR);
    }

    @Override
    public void logIncomingMessage(String messageJson)
    {
        addItem(messageJson, LogLevel.MESSAGE_IN);
    }

    @Override
    public void logOutgoingMessage(String messageJson)
    {
        addItem(messageJson, LogLevel.MESSAGE_OUT);
    }

    private void addItem(String log, LogLevel logLevel)
    {
        ClientServerLogItem item = new ClientServerLogItem();
        item.setLog(log);
        item.setCreatedDT(System.currentTimeMillis());
        item.setLogLevel(logLevel);
        this.clientServerLogModel.getClientServerLogItems().add(item);
    }

    public ClientServerLogModel getClientServerLogModel()
    {
        return this.clientServerLogModel;
    }
}
