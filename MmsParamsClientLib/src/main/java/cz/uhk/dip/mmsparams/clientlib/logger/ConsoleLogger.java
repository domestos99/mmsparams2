package cz.uhk.dip.mmsparams.clientlib.logger;

public class ConsoleLogger implements IClientCustomLogger
{
    public void log(Object message)
    {
        System.out.println(message);
    }

    @Override
    public void info(String info)
    {
        log(info);
    }

    @Override
    public void warn(String warning)
    {
        log(warning);
    }

    @Override
    public void error(Throwable t)
    {
        log(t);
    }

    @Override
    public void error(String message, Throwable t)
    {
        log(message + ": " + t);
    }

    @Override
    public void error(String error)
    {
        log(error);
    }

    @Override
    public void logIncomingMessage(String messageJson)
    {
        log(messageJson);
    }

    @Override
    public void logOutgoingMessage(String messageJson)
    {
        log(messageJson);
    }
}
