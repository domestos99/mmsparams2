package cz.uhk.dip.mmsparams.clientlib.logger;

import cz.uhk.dip.mmsparams.api.logging.ILogger;

/**
 * Interface representing custom user logger
 */
public interface IClientCustomLogger extends ILogger
{
}
