package cz.uhk.dip.mmsparams.clientlib.model;

import java.util.Objects;

/**
 * Custom operation Timeout
 */
public class CustomTimeout
{
    public static final CustomTimeout CUSTOM_TIMEOUT_10 = new CustomTimeout(10);
    public static final CustomTimeout CUSTOM_TIMEOUT_20 = new CustomTimeout(20);
    public static final CustomTimeout CUSTOM_TIMEOUT_40 = new CustomTimeout(40);
    public static final CustomTimeout CUSTOM_TIMEOUT_60 = new CustomTimeout(60);
    public static final CustomTimeout CUSTOM_TIMEOUT_100 = new CustomTimeout(100);
    public static final CustomTimeout CUSTOM_TIMEOUT_120 = new CustomTimeout(120);
    public static final CustomTimeout CUSTOM_TIMEOUT_180 = new CustomTimeout(180);
    public static final CustomTimeout CUSTOM_TIMEOUT_240 = new CustomTimeout(240);
    public static final CustomTimeout CUSTOM_TIMEOUT_300 = new CustomTimeout(300);

    private final int timeoutSeconds;

    public CustomTimeout(final int timeoutSeconds)
    {
        this.timeoutSeconds = timeoutSeconds;
    }

    public int getTimeoutSeconds()
    {
        return timeoutSeconds;
    }

    @Override
    public String toString()
    {
        return "CustomTimeout{" +
                "timeoutSeconds=" + timeoutSeconds +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomTimeout that = (CustomTimeout) o;
        return timeoutSeconds == that.timeoutSeconds;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(timeoutSeconds);
    }
}
