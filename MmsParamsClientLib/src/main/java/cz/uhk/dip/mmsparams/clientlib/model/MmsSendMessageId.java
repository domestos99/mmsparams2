package cz.uhk.dip.mmsparams.clientlib.model;

import cz.uhk.dip.mmsparams.api.messages.MessageId;

/**
 * Message ID of MMS send message request
 */
public class MmsSendMessageId extends MessageId
{
    public MmsSendMessageId()
    {
        super();
    }

    public MmsSendMessageId(String messageId)
    {
        super(messageId);
    }

    public MmsSendMessageId(MessageId messageId)
    {
        this(messageId.getMessageId());
    }
}
