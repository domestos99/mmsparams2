package cz.uhk.dip.mmsparams.clientlib.model;

import cz.uhk.dip.mmsparams.api.messages.MessageId;

/**
 * Message ID of SMS send message request
 */
public class SmsSendMessageId extends MessageId
{
    public SmsSendMessageId()
    {
        super();
    }

    public SmsSendMessageId(String messageId)
    {
        super(messageId);
    }

    public SmsSendMessageId(MessageId messageId)
    {
        this(messageId.getMessageId());
    }
}
