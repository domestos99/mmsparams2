package cz.uhk.dip.mmsparams.clientlib.model;

import cz.uhk.dip.mmsparams.api.messages.MessageId;

/**
 * Message ID of Smsc send message request
 */
public class SmscMessageId extends MessageId
{
    public SmscMessageId()
    {
        super();
    }

    public SmscMessageId(String messageId)
    {
        super(messageId);
    }

    public SmscMessageId(MessageId messageId)
    {
        this(messageId.getMessageId());
    }
}