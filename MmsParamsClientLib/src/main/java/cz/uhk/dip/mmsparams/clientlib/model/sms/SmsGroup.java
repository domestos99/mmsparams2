package cz.uhk.dip.mmsparams.clientlib.model.sms;

import java.io.Serializable;
import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;

/**
 * Contains all SMS models created while sending and receiving SMS
 */
public class SmsGroup implements Serializable
{
    private SmsSendModel smsSend;
    private SmsSendResponseModel smsSendConfirmation;
    private SmsReceiveModel smsReceived;
    private List<SmsReceiveModel> smsReceivedAllParts;
    private SmsDeliveryReport smsDeliveryReport;

    public SmsSendModel getSmsSend()
    {
        return smsSend;
    }

    public void setSmsSend(SmsSendModel smsSend)
    {
        this.smsSend = smsSend;
    }

    public SmsSendResponseModel getSmsSendConfirmation()
    {
        return smsSendConfirmation;
    }

    public void setSmsSendConfirmation(SmsSendResponseModel smsSendConfirmation)
    {
        this.smsSendConfirmation = smsSendConfirmation;
    }

    public SmsReceiveModel getSmsReceived()
    {
        return smsReceived;
    }

    public void setSmsReceived(SmsReceiveModel smsReceived)
    {
        this.smsReceived = smsReceived;
    }

    public List<SmsReceiveModel> getSmsReceivedAllParts()
    {
        return smsReceivedAllParts;
    }

    public void setSmsReceivedAllParts(List<SmsReceiveModel> smsReceivedAllParts)
    {
        this.smsReceivedAllParts = smsReceivedAllParts;
    }

    public SmsDeliveryReport getSmsDeliveryReport()
    {
        return smsDeliveryReport;
    }

    public void setSmsDeliveryReport(SmsDeliveryReport smsDeliveryReport)
    {
        this.smsDeliveryReport = smsDeliveryReport;
    }
}
