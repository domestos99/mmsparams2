package cz.uhk.dip.mmsparams.clientlib.model.smsc;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;

/**
 * Contains all SMSC models created while sending and receiving SMS
 */
public class SmscGroup
{
    private SmscSendModel smscSendModel;
    private SmscSendSmsResponseModel smscSendSmsResponseModel;
    private SmscDeliveryReportModel smscDeliveryReportModel;

    public SmscSendModel getSmscSendModel()
    {
        return smscSendModel;
    }

    public void setSmscSendModel(SmscSendModel smscSendModel)
    {
        this.smscSendModel = smscSendModel;
    }

    public SmscSendSmsResponseModel getSmscSendSmsResponseModel()
    {
        return smscSendSmsResponseModel;
    }

    public void setSmscSendSmsResponseModel(SmscSendSmsResponseModel smscSendSmsResponseModel)
    {
        this.smscSendSmsResponseModel = smscSendSmsResponseModel;
    }

    public SmscDeliveryReportModel getSmscDeliveryReportModel()
    {
        return smscDeliveryReportModel;
    }

    public void setSmscDeliveryReportModel(SmscDeliveryReportModel smscDeliveryReportModel)
    {
        this.smscDeliveryReportModel = smscDeliveryReportModel;
    }
}
