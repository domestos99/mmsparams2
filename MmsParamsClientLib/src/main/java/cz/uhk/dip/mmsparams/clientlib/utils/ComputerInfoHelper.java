package cz.uhk.dip.mmsparams.clientlib.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;

/**
 * Helper for getting information about client computer
 */
public class ComputerInfoHelper
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(ComputerInfoHelper.class);

    private ComputerInfoHelper()
    {
    }

    /**
     * Return computer name
     *
     * @return computer name
     */
    public static String getComputerName()
    {
        String hostname = "Unknown";

        try
        {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        }
        catch (UnknownHostException ex)
        {
            ClientLogFacade.logException(LOGGER, "getComputerName: Hostname can not be resolved", ex);
        }
        return hostname;
    }

    /**
     * Returns current computer user name
     *
     * @return current computer user name
     */
    public static String getCurrentUserName()
    {
        return System.getProperty("user.name");
    }

}
