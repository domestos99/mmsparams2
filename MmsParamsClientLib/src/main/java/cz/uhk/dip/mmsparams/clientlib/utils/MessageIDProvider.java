package cz.uhk.dip.mmsparams.clientlib.utils;

import cz.uhk.dip.mmsparams.api.websocket.MessageIdGenerator;

/**
 * Provider for messaged IDs
 */
public class MessageIDProvider
{
    private MessageIDProvider()
    {
    }

    /**
     * Returns new message id
     *
     * @return new message id
     */
    public static String generateMessageID()
    {
        return "ClientLib_" + MessageIdGenerator.getNext();
    }

    /**
     * Return new client sender key
     *
     * @return new sender key
     */
    public static String generateSenderKey()
    {
        return "ClientLib_" + MessageIdGenerator.getNext();
    }
}
