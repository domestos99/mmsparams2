package cz.uhk.dip.mmsparams.clientlib.utils;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

public class RandomString
{
    public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String upperSpecial;

    static
    {
        upperSpecial = "ÁÉÍÓŮÚĚ";
    }

    public static final String lower = upper.toLowerCase(Locale.ROOT);

    public static final String lowerSpecial = upperSpecial.toLowerCase(Locale.ROOT);

    public static final String digits = "0123456789";

    public static final String alphanum = upper + lower + digits;

    public static final String alphanumSpecial = upper + upperSpecial + lower + lowerSpecial + digits;

    private final Random random;

    private final char[] symbols;

    private final char[] buf;

    /**
     * Create an alphanumeric strings from a secure generator.
     *
     * @param length length of random string
     */
    public RandomString(int length)
    {
        this(length, false);
    }

    /**
     * Create an alphanumeric strings from a secure generator.
     *
     * @param length     length of random string
     * @param useSpecial use special characters (diacritics)
     */
    public RandomString(int length, boolean useSpecial)
    {
        if (length < 1)
            throw new IllegalArgumentException();

        this.random = Objects.requireNonNull(new SecureRandom());

        if (useSpecial)
            this.symbols = alphanumSpecial.toCharArray();
        else
            this.symbols = alphanum.toCharArray();

        this.buf = new char[length];
    }

    /**
     * Generate a random string.
     *
     * @return random string
     */
    public String nextString()
    {
        for (int idx = 0; idx < buf.length; ++idx)
        {
            if (idx % 5 == 0)
            {
                buf[idx] = ' ';
            }
            else
            {
                buf[idx] = symbols[random.nextInt(symbols.length)];
            }
        }
        return new String(buf);
    }
}
