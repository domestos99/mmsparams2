package cz.uhk.dip.mmsparams.clientlib.utils;

/**
 * Sample test generator. Can be used for generating SMS or MMS texts
 */
public class SampleTextUtils
{
    private SampleTextUtils()
    {
    }

    public static String generateText(int length)
    {
        return generateText(null, length);
    }

    public static String generateText(String prefix, int length)
    {
        return generateTextInternal(prefix, length, false);
    }

    public static String generateText16BIT(int length)
    {
        return generateText16BIT(null, length);
    }

    public static String generateText16BIT(String prefix, int length)
    {
        return generateTextInternal(prefix, length, true);
    }

    private static String generateTextInternal(String prefix, int length, boolean useSpecial)
    {
        StringBuilder sb = new StringBuilder();

        if (prefix == null)
            prefix = "";

        sb.append(prefix);

        String text = new RandomString(length - prefix.length(), useSpecial).nextString();
        sb.append(text.trim());

        while (sb.length() < length)
        {
            sb.append("a");
        }

        return sb.toString();
    }


}
