package cz.uhk.dip.mmsparams.clientlib.utils;

import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public class ThreadUtils
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(ThreadUtils.class);

    private ThreadUtils()
    {
    }

    /**
     * Sleep current thread with exception handling
     *
     * @param millis millisecond to sleep thread for
     */
    public static void sleep(long millis)
    {
        try
        {
            Thread.sleep(millis);
        }
        catch (InterruptedException e)
        {
            ClientLogFacade.logWarning(LOGGER, ExceptionHelper.getStackTrace(e));
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Sleep current thread with exception handling
     *
     * @param customTimeout CustomTimeout with seconds to sleep thread for
     */
    public static void sleep(CustomTimeout customTimeout)
    {
        sleep(customTimeout.getTimeoutSeconds() * 1000L);
    }
}
