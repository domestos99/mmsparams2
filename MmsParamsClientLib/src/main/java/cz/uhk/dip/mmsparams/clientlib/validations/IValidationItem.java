package cz.uhk.dip.mmsparams.clientlib.validations;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;

public interface IValidationItem
{
    /**
     * Validate validation item
     *
     * @return Returns result of validation
     */
    Tuple<Boolean, ValidationResult> validate();
}
