package cz.uhk.dip.mmsparams.clientlib.validations;

import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;

public interface IValidationService
{
    /**
     * Validate items from validation builder and sends result to the server. Throws exception and ends tests when any validation fails
     *
     * @param validationBuilder validation builder to validate
     */
    default void validatePrintThrow(ValidationBuilder validationBuilder)
    {
        Preconditions.checkNotNull(validationBuilder, GenericConstants.VALIDATION_BUILDER);
        this.validatePrintThrow(validationBuilder.build());
    }

    /**
     * Validate more validation items at once and sends result to the server. Throws exception and ends tests when any validation fails
     *
     * @param validationItems validation items to validate
     */
    default void validatePrintThrow(IValidationItem... validationItems)
    {
        validatePrintThrow(ListUtils.toListNull(validationItems));
    }

    /**
     * Validate collection of validation items and sends result to the server. Throws exception and ends tests when any validation fails
     *
     * @param validationItems validation items to validate
     */
    void validatePrintThrow(List<IValidationItem> validationItems);

    /**
     * Validate items from validation builder and sends result to the server.
     *
     * @param validationBuilder validation builder to validate
     * @return validation result
     */
    default List<Tuple<Boolean, ValidationResult>> validateOnly(ValidationBuilder validationBuilder)
    {
        Preconditions.checkNotNull(validationBuilder, GenericConstants.VALIDATION_BUILDER);
        return this.validateOnly(validationBuilder.build());
    }

    /**
     * Validate more validation items at once and sends result to the server
     *
     * @param validationItems validation items to validate
     * @return validation result
     */
    default List<Tuple<Boolean, ValidationResult>> validateOnly(IValidationItem... validationItems)
    {
        return validateOnly(ListUtils.toList(validationItems));
    }

    /**
     * Validate collection of validation items and sends result to the server
     *
     * @param validationItems validation items to validate
     * @return validation result
     */
    List<Tuple<Boolean, ValidationResult>> validateOnly(List<IValidationItem> validationItems);
}
