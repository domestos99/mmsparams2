package cz.uhk.dip.mmsparams.clientlib.validations;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.exceptions.ValidationException;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.LogUtil;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;

public class ValidationHelper
{
    private ValidationHelper()
    {
    }

    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(ValidationHelper.class);

    /**
     * Check whether list has specific size (number of items)
     *
     * @param expected expected list size
     * @param list     list to check
     * @return true if list size is as expected
     * @throws ValidationException at least one of validations failed
     */
    public static boolean checkListCount(int expected, List list) throws ValidationException
    {
        Preconditions.checkNotNull(list, GenericConstants.LIST);

        if (list.size() != expected)
            throw new ValidationException("List count is: " + list.size() + " Expected: " + expected);

        return true;
    }

    public static boolean assertEqual(String expected, String actual) throws ValidationException
    {
        return assertEqual(expected, actual, null);
    }

    public static boolean assertEqual(String expected, String actual, String parameterName) throws ValidationException
    {
        if (!Objects.equals(expected, actual))
        {
            throw new ValidationException("Parameter " + parameterName + " should be " + expected + " but is " + actual);
        }
        return true;
    }


    public static boolean assertEqual(Object expected, Object actual) throws ValidationException
    {
        return assertEqual(expected, actual, null);
    }

    public static boolean assertEqual(Object expected, Object actual, String parameterName) throws ValidationException
    {
        if (!Objects.equals(expected, actual))
        {
            throw new ValidationException("Parameter " + parameterName + " should be " + expected + " but is " + actual);
        }
        return true;
    }

    @Nonnull
    public static List<Tuple<Boolean, ValidationResult>> validateList(final List<IValidationItem> validationItems)
    {
        final List<Tuple<Boolean, ValidationResult>> valResult = new ArrayList<>();

        if (validationItems == null)
            return valResult;

        for (final IValidationItem v : validationItems)
        {
            if (v == null)
                continue;
            valResult.addAll(validate(v));
        }

        return valResult;
    }

    /**
     * Validate items in validation builder
     *
     * @param validationBuilder validation builder instance to validate
     * @return validation result
     */
    public static List<Tuple<Boolean, ValidationResult>> validate(ValidationBuilder validationBuilder)
    {
        Preconditions.checkNotNull(validationBuilder, GenericConstants.VALIDATION_BUILDER);
        return validateList(validationBuilder.build());
    }

    /**
     * Validate more validation items at once
     *
     * @param validationItems validation items to validate
     * @return validation result
     */
    @Nonnull
    public static List<Tuple<Boolean, ValidationResult>> validate(IValidationItem... validationItems)
    {
        Preconditions.checkNotNull(validationItems, GenericConstants.VALIDATION_ITEMS);

        List<Tuple<Boolean, ValidationResult>> valResult = new ArrayList<>();

        if (validationItems == null)
            return valResult;

        for (int i = 0; i < validationItems.length; i++)
        {
            IValidationItem vi = validationItems[i];
            if (vi != null)
            {
                valResult.add(vi.validate());
            }
        }
        return valResult;
    }

    /**
     * Prints validation result to the console
     *
     * @param validationResult validation result to print
     */
    public static void printValidationResult(List<Tuple<Boolean, ValidationResult>> validationResult)
    {
        Preconditions.checkNotNull(validationResult, GenericConstants.VALIDATION_RESULT);

        ClientLogFacade.logAction(LOGGER, "Validations:");

        for (Tuple<Boolean, ValidationResult> vr : validationResult)
        {
            ClientLogFacade.logAction(LOGGER, vr.getX() + ": " + vr.getY() + LogUtil.NEW_LINE);
        }
    }

    /**
     * Check validation results and throw exception if any of them failed
     *
     * @param validationResult validation result to check
     * @throws ValidationException at least one of validations failed
     */
    public static void checkErrorsThrow(List<Tuple<Boolean, ValidationResult>> validationResult) throws ValidationException
    {
        if (validationResult == null)
            return;

        for (Tuple<Boolean, ValidationResult> vr : validationResult)
        {
            if (!vr.getX())
            {
                throw new ValidationException(vr.getY());
            }
        }
    }

    /**
     * Validates more validation items at one, prints result and if any of the validation fails, throws expcetion
     *
     * @param validationItems validation items to validate
     * @throws ValidationException at least one of validations failed
     */
    public static void validatePrintThrow(IValidationItem... validationItems) throws ValidationException
    {
        Preconditions.checkNotNull(validationItems, GenericConstants.VALIDATION_ITEMS);
        List<Tuple<Boolean, ValidationResult>> result = ValidationHelper.validate(validationItems);
        ValidationHelper.printValidationResult(result);
        ValidationHelper.checkErrorsThrow(result);
    }

    /**
     * Validates list of validation items at one, prints result and if any of the validation fails, throws expcetion
     *
     * @param validationItems list of validation items to validate
     * @throws ValidationException at least one of validations failed
     */
    public static void validatePrintThrow(List<IValidationItem> validationItems) throws ValidationException
    {
        Preconditions.checkNotNull(validationItems, GenericConstants.VALIDATION_ITEMS);
        List<Tuple<Boolean, ValidationResult>> result = ValidationHelper.validateList(validationItems);
        ValidationHelper.printValidationResult(result);
        ValidationHelper.checkErrorsThrow(result);
    }


}
