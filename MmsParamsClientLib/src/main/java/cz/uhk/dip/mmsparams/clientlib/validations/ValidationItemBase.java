package cz.uhk.dip.mmsparams.clientlib.validations;

/**
 * Base class for all validation items
 */
public abstract class ValidationItemBase implements IValidationItem
{
    public ValidationItemBase()
    {
    }

    public ValidationItemBase(final String validationName)
    {
        this.validationName = validationName;
    }

    private String validationName;

    public String getValidationName()
    {
        return validationName;
    }

    public void setValidationName(String validationName)
    {
        this.validationName = validationName;
    }
}
