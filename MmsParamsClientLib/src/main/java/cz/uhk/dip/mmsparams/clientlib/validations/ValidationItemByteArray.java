package cz.uhk.dip.mmsparams.clientlib.validations;

import java.util.Arrays;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;

/**
 * Validate whether byte array equals to expected
 * Uses Arrays.equals
 */
public class ValidationItemByteArray extends ValidationItemBase
{
    private static final String TAG = ValidationItemByteArray.class.getSimpleName();

    private final byte[] expected;
    private final byte[] actual;

    public ValidationItemByteArray(byte[] expected, byte[] actual, String validationName)
    {
        super(validationName);
        this.expected = expected;
        this.actual = actual;
    }

    @Override
    public Tuple<Boolean, ValidationResult> validate()
    {
        boolean val = Arrays.equals(expected, actual);
        return new Tuple<>(val, new ValidationResult(TAG, expected, actual, this.getValidationName(), val));
    }
}
