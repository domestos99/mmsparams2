package cz.uhk.dip.mmsparams.clientlib.validations;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;

/**
 * Validate whether boolean value is false
 */
public class ValidationItemFalse extends ValidationItemBase
{
    private static final String TAG = ValidationItemFalse.class.getSimpleName();

    private boolean value;

    public ValidationItemFalse(boolean value, String validationName)
    {
        super(validationName);
        this.value = value;
    }

    @Override
    public Tuple<Boolean, ValidationResult> validate()
    {
        return new Tuple<>(!value, new ValidationResult(TAG, false, value, this.getValidationName(), !value));
    }
}
