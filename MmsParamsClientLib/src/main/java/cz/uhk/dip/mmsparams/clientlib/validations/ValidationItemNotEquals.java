package cz.uhk.dip.mmsparams.clientlib.validations;

import java.io.Serializable;
import java.util.Objects;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;

/**
 * Validate whether specific value is not equal to expected value
 * Uses Objects.equals
 *
 * @param <T> type of object to compare
 */
public class ValidationItemNotEquals<T extends Serializable> extends ValidationItemBase
{
    private static final String TAG = ValidationItemNotEquals.class.getSimpleName();

    private final T expected;
    private final T actual;

    public ValidationItemNotEquals(final T expected, final T actual, final String validationName)
    {
        super(validationName);
        this.expected = expected;
        this.actual = actual;
    }

    @Override
    public Tuple<Boolean, ValidationResult> validate()
    {
        boolean val = !Objects.equals(expected, actual);
        return new Tuple<>(val, new ValidationResult(TAG, expected, actual, this.getValidationName(), val));
    }

}
