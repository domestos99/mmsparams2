package cz.uhk.dip.mmsparams.clientlib.validations;

import java.io.Serializable;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;

/**
 * Validate whether specific value is not null
 */
public class ValidationItemNotNull<T extends Serializable> extends ValidationItemBase
{
    private static final String TAG = ValidationItemNotNull.class.getSimpleName();

    private final T value;

    public ValidationItemNotNull(final T value, String validationName)
    {
        super(validationName);
        this.value = value;
    }

    @Override
    public Tuple<Boolean, ValidationResult> validate()
    {
        return new Tuple<>(value != null, new ValidationResult(TAG, "Nonnull", value, this.getValidationName(), value != null));
    }

}
