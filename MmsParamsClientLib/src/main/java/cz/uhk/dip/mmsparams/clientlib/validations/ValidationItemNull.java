package cz.uhk.dip.mmsparams.clientlib.validations;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;

/**
 * Validate whether specific value is not null
 */
public class ValidationItemNull extends ValidationItemBase
{
    private static final String TAG = ValidationItemNull.class.getSimpleName();

    private Object value;

    public ValidationItemNull(Object value, String validationName)
    {
        super(validationName);
        this.value = value;
    }

    @Override
    public Tuple<Boolean, ValidationResult> validate()
    {
        return new Tuple<>(value == null, new ValidationResult(TAG, "*not null*", value, this.getValidationName(), value != null));
    }

}
