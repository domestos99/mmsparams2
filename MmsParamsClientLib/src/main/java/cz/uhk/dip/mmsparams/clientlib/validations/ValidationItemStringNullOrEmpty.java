package cz.uhk.dip.mmsparams.clientlib.validations;

import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;

public class ValidationItemStringNullOrEmpty extends ValidationItemBase
{
    private static final String TAG = ValidationItemStringNullOrEmpty.class.getSimpleName();

    private String value;

    public ValidationItemStringNullOrEmpty(String value, String validationName)
    {
        super(validationName);
        this.value = value;
    }

    @Override
    public Tuple<Boolean, ValidationResult> validate()
    {
        boolean val = StringUtil.isEmptyOrNull(this.value);
        return new Tuple<>(val, new ValidationResult(TAG, "*null or empty*", value, this.getValidationName(), val));
    }
}
