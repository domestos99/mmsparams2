package cz.uhk.dip.mmsparams.clientlib.validations;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;

/**
 * Validate whether boolean value is true
 */
public class ValidationItemTrue extends ValidationItemBase
{
    private static final String TAG = ValidationItemTrue.class.getSimpleName();

    private boolean value;

    public ValidationItemTrue(boolean value, String validationName)
    {
        super(validationName);
        this.value = value;
    }

    @Override
    public Tuple<Boolean, ValidationResult> validate()
    {
        return new Tuple<>(value, new ValidationResult(TAG, true, value, this.getValidationName(), value));
    }
}
