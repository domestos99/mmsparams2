package cz.uhk.dip.mmsparams.clientlib.validations;

import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;
import cz.uhk.dip.mmsparams.clientlib.communication.ServiceBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;

public class ValidationService extends ServiceBase implements IValidationService
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(ValidationService.class);

    public ValidationService(IWebSocketSender webSocketSender, IWebSocketWaiter webSocketReceiver)
    {
        super(webSocketSender, webSocketReceiver);
    }

    @Override
    public void validatePrintThrow(List<IValidationItem> validationItems)
    {
        Preconditions.checkNotNull(validationItems, GenericConstants.VALIDATION_ITEMS);
        ClientLogFacade.logAction(LOGGER, "validatePrintThrow");

        List<Tuple<Boolean, ValidationResult>> result = ValidationHelper.validateList(validationItems);

        sendValidationsToServer(result);

        ValidationHelper.printValidationResult(result);
        ValidationHelper.checkErrorsThrow(result);
    }

    @Override
    public List<Tuple<Boolean, ValidationResult>> validateOnly(List<IValidationItem> validationItems)
    {
        Preconditions.checkNotNull(validationItems, GenericConstants.VALIDATION_ITEMS);
        ClientLogFacade.logAction(LOGGER, "validateOnly");

        List<Tuple<Boolean, ValidationResult>> result = ValidationHelper.validateList(validationItems);

        sendValidationsToServer(result);
        return result;
    }

    private void sendValidationsToServer(List<Tuple<Boolean, ValidationResult>> result)
    {
        ClientLogFacade.logAction(LOGGER, "sendValidationsToServer");

        TestValidationMessage testValidationMessage = MessageFactory.createServerRecipient(TestValidationMessage.class);
        testValidationMessage.setValidationResult(ListUtils.toArrayList(result));

        // Dont wait for response
        webSocketSender.sendMessage(testValidationMessage);
    }
}
