package cz.uhk.dip.mmsparams.clientlib.validations.builder;

import java.io.Serializable;
import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemByteArray;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemByteArrayNotEquals;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemEquals;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemFalse;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemNotEquals;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemNotNull;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemNull;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemStringNotNullOrEmpty;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemStringNullOrEmpty;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemTrue;

public class ValidationBuilder
{
    private final List<IValidationItem> validations;

    public ValidationBuilder()
    {
        this.validations = ListUtils.newList();
    }

    /**
     * Create new instance of ValidationBuilder
     *
     * @return new instance of ValidationBuilder
     */
    public static ValidationBuilder create()
    {
        return new ValidationBuilder();
    }

    public List<IValidationItem> build()
    {
        return ListUtils.newList(validations);
    }

    /**
     * Validates whether two values are equal
     *
     * @param expected       expected value
     * @param actual         value to be validated
     * @param validationName name of the validation
     * @param <T>            serializable class type
     * @return same instance of the ValidationBuilder
     */
    public <T extends Serializable> ValidationBuilder withEquals(final T expected, final T actual, final String validationName)
    {
        validations.add(new ValidationItemEquals<T>(expected, actual, validationName));
        return this;
    }

    /**
     * Validates whether two values are NOT equal
     *
     * @param expected       expected value
     * @param actual         value to be validated
     * @param validationName name of the validation
     * @param <T>            serializable class type
     * @return same instance of the ValidationBuilder
     */
    public <T extends Serializable> ValidationBuilder withNotEquals(final T expected, final T actual, final String validationName)
    {
        validations.add(new ValidationItemNotEquals<T>(expected, actual, validationName));
        return this;
    }

    /**
     * Validates whether value is null
     *
     * @param value          value to be validated
     * @param validationName name of the validation
     * @param <T>            serializable class type
     * @return same instance of the ValidationBuilder
     */
    public <T extends Serializable> ValidationBuilder withNull(final T value, final String validationName)
    {
        validations.add(new ValidationItemNull(value, validationName));
        return this;
    }

    /**
     * Validates whether value is NOT null
     *
     * @param value          value to be validated
     * @param validationName name of the validation
     * @param <T>            serializable class type
     * @return same instance of the ValidationBuilder
     */
    public <T extends Serializable> ValidationBuilder withNotNull(final T value, final String validationName)
    {
        validations.add(new ValidationItemNotNull<T>(value, validationName));
        return this;
    }

    /**
     * Validates whether value is true
     *
     * @param valueToBeTrue  value to be validated
     * @param validationName name of the validation
     * @return same instance of the ValidationBuilder
     */
    public ValidationBuilder withTrue(final boolean valueToBeTrue, final String validationName)
    {
        validations.add(new ValidationItemTrue(valueToBeTrue, validationName));
        return this;
    }

    /**
     * Validates whether value is false
     *
     * @param valueToBeFalse value to be validated
     * @param validationName name of the validation
     * @return same instance of the ValidationBuilder
     */
    public ValidationBuilder withFalse(final boolean valueToBeFalse, final String validationName)
    {
        validations.add(new ValidationItemFalse(valueToBeFalse, validationName));
        return this;
    }

    /**
     * Validates whether string value is null or empty
     *
     * @param value          value to be validated
     * @param validationName name of the validation
     * @return same instance of the ValidationBuilder
     */
    public ValidationBuilder withNotNullOrEmpty(final String value, String validationName)
    {
        validations.add(new ValidationItemStringNotNullOrEmpty(value, validationName));
        return this;
    }

    /**
     * Validates whether string value is NOT null or NOT empty
     *
     * @param value          value to be validated
     * @param validationName name of the validation
     * @return same instance of the ValidationBuilder
     */
    public ValidationBuilder withNullOrEmpty(final String value, String validationName)
    {
        validations.add(new ValidationItemStringNullOrEmpty(value, validationName));
        return this;
    }

    /**
     * Validates whether two byte arrays are equal
     *
     * @param expected       expected value
     * @param actual         value to be validated
     * @param validationName name of the validation
     * @return same instance of the ValidationBuilder
     */
    public ValidationBuilder withByteArrayEquals(final byte[] expected, final byte[] actual, final String validationName)
    {
        validations.add(new ValidationItemByteArray(expected, actual, validationName));
        return this;
    }

    /**
     * Validates whether two byte arrays are NOT equal
     *
     * @param expected       expected value
     * @param actual         value to be validated
     * @param validationName name of the validation
     * @return same instance of the ValidationBuilder
     */
    public ValidationBuilder withByteArrayNotEquals(final byte[] expected, final byte[] actual, final String validationName)
    {
        validations.add(new ValidationItemByteArrayNotEquals(expected, actual, validationName));
        return this;
    }

    /**
     * Adds custom validator to the validation list
     *
     * @param customValidationItem custom validator instance
     * @return same instance of the ValidationBuilder
     */
    public ValidationBuilder withCustomValidation(final IValidationItem customValidationItem)
    {
        Preconditions.checkNotNull(customValidationItem, GenericConstants.VALIDATION_ITEM);
        validations.add(customValidationItem);
        return this;
    }

    /**
     * Adds list of custom validators to the validation list
     *
     * @param customValidationList list of custom validator instances
     * @return same instance of the ValidationBuilder
     */
    public ValidationBuilder withCustomValidations(final List<IValidationItem> customValidationList)
    {
        Preconditions.checkNotNull(customValidationList, GenericConstants.VALIDATION_ITEMS);
        validations.addAll(customValidationList);
        return this;
    }

    /**
     * Clear list of predefined validations
     *
     * @return same instance of the ValidationBuilder
     */
    public ValidationBuilder clearValidations()
    {
        this.validations.clear();
        return this;
    }


}
