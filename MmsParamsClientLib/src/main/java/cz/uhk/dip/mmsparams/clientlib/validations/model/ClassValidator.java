package cz.uhk.dip.mmsparams.clientlib.validations.model;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class ClassValidator
{
    public static <T extends WebSocketMessageBase> void validate(Class<T> clz)
    {
        Preconditions.checkNotNull(clz, GenericConstants.CLZ);
    }
}
