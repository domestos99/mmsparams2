package cz.uhk.dip.mmsparams.clientlib.validations.model;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;

public class CustomTimeoutValidator
{
    public static void validate(CustomTimeout timeout)
    {
        Preconditions.checkNotNull(timeout, GenericConstants.TIMEOUT);
        Preconditions.isGreaterZeroOrDefault(timeout.getTimeoutSeconds(), GenericConstants.TIMEOUT);
    }

    public static void validate(int timeout)
    {
        Preconditions.isGreaterZeroOrDefault(timeout, GenericConstants.TIMEOUT);
    }
}
