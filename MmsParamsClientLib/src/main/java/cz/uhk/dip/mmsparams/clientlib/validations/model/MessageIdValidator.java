package cz.uhk.dip.mmsparams.clientlib.validations.model;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;

public class MessageIdValidator
{
    public static void validate(MessageId messageId)
    {
        Preconditions.checkNotNull(messageId, GenericConstants.MESSAGE_ID);
        Preconditions.checkNotNullOrEmpty(messageId.getMessageId(), GenericConstants.MESSAGE_ID);
    }

    public static void validate(MmsSendMessageId mmsSendReqId)
    {
        Preconditions.checkNotNull(mmsSendReqId, GenericConstants.MMS_SEND_REQ_ID);
        Preconditions.checkNotNullOrEmpty(mmsSendReqId.getMessageId(), GenericConstants.MMS_SEND_REQ_ID);
    }

    public static void validate(SmscSessionId smscSessionId)
    {
        Preconditions.checkNotNull(smscSessionId, GenericConstants.SMSC_SESSION_ID);
        Preconditions.checkNotNullOrEmpty(smscSessionId.getSmscSessionId(), GenericConstants.SMSC_SESSION_ID);
    }

    public static void validate(SmsSendMessageId smsSendMessageId)
    {
        Preconditions.checkNotNull(smsSendMessageId, GenericConstants.SMS_SEND_MESSAGE_ID);
        Preconditions.checkNotNullOrEmpty(smsSendMessageId.getMessageId(), GenericConstants.SMS_SEND_MESSAGE_ID);
    }
}
