package cz.uhk.dip.mmsparams.clientlib.validations.model;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.enums.ReadStatus;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsDownloadOptionsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsReadReportOptionsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;

public class MmsRecipientPhoneProfileValidator
{
    private MmsRecipientPhoneProfileValidator()
    {
    }

    public static void validate(MmsRecipientPhoneProfile mmsRecipientPhoneProfile)
    {
        Preconditions.checkNotNull(mmsRecipientPhoneProfile, GenericConstants.MMS_RECIPIENT_PHONE_PROFILE);
        Preconditions.checkNotNull(mmsRecipientPhoneProfile.getMmsDownloadOptionsModel(), GenericConstants.MMS_DOWNLOAD_OPTIONS_MODEL);
        Preconditions.checkNotNull(mmsRecipientPhoneProfile.getMmsReadReportOptionsModel(), GenericConstants.MMS_READ_REPORT_OPTIONS_MODEL);

        checkDownloadOpt(mmsRecipientPhoneProfile.getMmsDownloadOptionsModel());
        checkReadReportOpt(mmsRecipientPhoneProfile.getMmsReadReportOptionsModel());
    }

    private static void checkDownloadOpt(final MmsDownloadOptionsModel dwn)
    {
        if (dwn.isAutoDownload())
            Preconditions.checkTrue(dwn.getDownloadAfterSeconds() == 0, "DownloadAfterSeconds must be 0 if autoDownload is true!");

        if (!dwn.isAutoDownload())
        {
            Preconditions.checkTrue(dwn.getDownloadAfterSeconds() == -1 || dwn.getDownloadAfterSeconds() > 0, "DownloadAfterSeconds must be greater then zero or -1");
        }
    }


    private static void checkReadReportOpt(final MmsReadReportOptionsModel rrOpt)
    {
        String err = "Wrong configuration of MmsReadReportOptionsModel - use create methods";
        if (!rrOpt.isSendReadReport())
        {
            Preconditions.checkTrue(rrOpt.getSendAfterSeconds() == 0 && rrOpt.getReadStatus() == ReadStatus.RAED, err);
        }
        else
        {
            Preconditions.checkTrue(rrOpt.getSendAfterSeconds() > 0, err);
            Preconditions.checkNotNull(rrOpt.getReadStatus(), err);
        }
    }
}
