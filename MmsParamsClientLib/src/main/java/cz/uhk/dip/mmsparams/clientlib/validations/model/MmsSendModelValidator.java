package cz.uhk.dip.mmsparams.clientlib.validations.model;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;

public class MmsSendModelValidator
{
    public static void validate(MmsSendModel mmsSendModel)
    {
        Preconditions.checkNotNull(mmsSendModel, GenericConstants.MMS_SEND_MODEL);
        Preconditions.checkNotNullOrEmpty(mmsSendModel.getTo(), GenericConstants.TO);
    }
}
