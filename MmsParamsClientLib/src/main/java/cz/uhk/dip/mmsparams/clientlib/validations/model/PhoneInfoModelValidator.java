package cz.uhk.dip.mmsparams.clientlib.validations.model;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;

public class PhoneInfoModelValidator
{
    public static void validate(PhoneInfoModel phoneInfoModel)
    {
        validate(phoneInfoModel, GenericConstants.PHONE_INFO_MODEL);
    }

    public static void validate(PhoneInfoModel phoneInfoModel, String msg)
    {
        Preconditions.checkNotNull(phoneInfoModel, msg);
        Preconditions.checkNotNullOrEmpty(phoneInfoModel.getPhoneKey(), GenericConstants.PHONE_KEY);
    }
}
