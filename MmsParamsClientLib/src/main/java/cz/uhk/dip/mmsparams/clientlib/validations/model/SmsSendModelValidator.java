package cz.uhk.dip.mmsparams.clientlib.validations.model;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;

public class SmsSendModelValidator
{
    public static void validate(SmsSendModel smsSendModel)
    {
        Preconditions.checkNotNull(smsSendModel, GenericConstants.SMS);
        Preconditions.checkNotNullOrEmpty(smsSendModel.getTo(), GenericConstants.TO);
    }
}
