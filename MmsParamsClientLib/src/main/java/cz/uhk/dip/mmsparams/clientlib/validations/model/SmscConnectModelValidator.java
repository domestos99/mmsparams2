package cz.uhk.dip.mmsparams.clientlib.validations.model;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;

public class SmscConnectModelValidator
{
    public static void validate(SmscConnectModel smscConnectModel)
    {
        Preconditions.checkNotNull(smscConnectModel, GenericConstants.SMSC_CONNECT_MODEL);
        Preconditions.checkNotNullOrEmpty(smscConnectModel.getHost(), GenericConstants.HOST);
        Preconditions.isGreaterZero(smscConnectModel.getPort(), GenericConstants.PORT);
        Preconditions.checkNotNullOrEmpty(smscConnectModel.getSystemId(), GenericConstants.SYSTEM_ID);
        Preconditions.checkNotNullOrEmpty(smscConnectModel.getPassword(), GenericConstants.PASSWORD);
        Preconditions.checkNotNull(smscConnectModel.getConnType(), GenericConstants.SYSTEM_TYPE);
        Preconditions.checkNotNull(smscConnectModel.getConnType(), GenericConstants.CONN_TYPE);
    }
}
