package cz.uhk.dip.mmsparams.clientlib.validations.model;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;

public class SmscSendSmsModelValidator
{
    public static void validate(SmscSendModel smscSendModel)
    {
        Preconditions.checkNotNull(smscSendModel, GenericConstants.SMSC_SEND_SMS_MODEL);
        Preconditions.checkNotNull(smscSendModel.getSenderAddress(), GenericConstants.SENDER_ADDRESS);
        Preconditions.checkNotNull(smscSendModel.getRecipientAddress(), GenericConstants.RECIPIENT_ADDRESS);

        Preconditions.checkNotNullOrEmpty(smscSendModel.getSenderAddress().getAddress(), GenericConstants.SENDER_ADDRESS);
        Preconditions.checkNotNullOrEmpty(smscSendModel.getRecipientAddress().getAddress(), GenericConstants.RECIPIENT_ADDRESS);
    }
}
