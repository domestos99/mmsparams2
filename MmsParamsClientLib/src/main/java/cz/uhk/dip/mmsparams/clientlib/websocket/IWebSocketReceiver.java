package cz.uhk.dip.mmsparams.clientlib.websocket;

import okhttp3.WebSocket;
import okio.ByteString;

/**
 * Received of WebSocket messages
 */
public interface IWebSocketReceiver
{
    /**
     * Receive WebSocket text message
     *
     * @param webSocket connected web socket
     * @param text      incoming text message
     */
    void onMessage(WebSocket webSocket, String text);

    /**
     * Receive WebSocket ByteString message
     *
     * @param webSocket connected web socket
     * @param bytes     incoming ByteString message
     */
    void onMessage(WebSocket webSocket, ByteString bytes);
}
