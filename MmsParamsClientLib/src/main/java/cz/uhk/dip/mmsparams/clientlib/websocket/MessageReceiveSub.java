package cz.uhk.dip.mmsparams.clientlib.websocket;


import javax.annotation.Nullable;

import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.IMessageRegister;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.listeners.IMessageReceiveSub;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.KeepAliveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.email.EmailReceiveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.TestErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsRecipientPhoneProfileRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.AcknowledgeIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.DeliveryIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotifyRespIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadOrigIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.ReadRecIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendReqResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReportReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ReadReplyReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7SubmitResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneAllPartsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDisconnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;
import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;

public class MessageReceiveSub implements IMessageReceiveSub<WebSocketMessageBase>
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(MessageReceiveSub.class);


    private final IMessageRegister messageRegister;
    private final IErrorHandler iErrorHandler;

    public MessageReceiveSub(IMessageRegister messageRegister, IErrorHandler iErrorHandler)
    {
        this.messageRegister = messageRegister;
        this.iErrorHandler = iErrorHandler;
    }

    private void insertIncomingMsg(WebSocketMessageBase msg)
    {
        messageRegister.insertIncomingMsg(msg);
    }

    @Nullable
    @Override
    public WebSocketMessageBase onReceiveUnknown(String msg)
    {
        ClientLogFacade.logWsUnknownMessage(LOGGER, msg);
        messageRegister.insertUnknownMessage(msg);
        return null;
    }

    @Override
    public WebSocketMessageBase onReceive(PhoneListRequestMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(RegisterPhoneMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(EmptyMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(PhoneListResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(SmsSendPhoneRequestMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(SmsReceivePhoneMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(SmsReceivePhoneAllPartsMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(SmsSendPhoneResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(SmsSendDeliveryReportMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(MmsSendPhoneRequestMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(DevMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(MmsRecipientPhoneProfileRequestMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(GenericBooleanResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(GenericErrorResponseMessage msg)
    {
        insertIncomingMsg(msg);
        ClientLogFacade.logWsErrorMessage(LOGGER, msg);
        iErrorHandler.setError(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(SendConfResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(RegisterClientLibMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(NotificationIndResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(DeliveryIndResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(ReadOrigIndResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(AcknowledgeIndResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(RetrieveConfResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(ReadRecIndResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(NotifyRespIndResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(MM7DeliveryReqMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(MM7DeliveryReportReqMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(MM7ReadReplyReqMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(SmscConnectMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(SmscSendSmsMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(SmscDeliverSmMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(SmscDisconnectMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(SmscDeliveryReportMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(SmscConnectResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(SmscSendSmsResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(UnLockPhoneMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(SendReqResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(MmscSendMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(LockPhoneMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(LockedPhonesListResponseMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(LockedPhonesListRequestMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(MM7SubmitResponseMessage msg)
    {

        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(MmscAcquireRouteRequestMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(TestValidationMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(TestErrorMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(MM7ErrorMessage msg)
    {
        insertIncomingMsg(msg);
        return msg;
    }

    @Override
    public WebSocketMessageBase onReceive(KeepAliveMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(TestResultMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    @Override
    public WebSocketMessageBase onReceive(EmailReceiveMessage msg)
    {
        return handleUnexpectedMessage(msg);
    }

    private WebSocketMessageBase handleUnexpectedMessage(WebSocketMessageBase msg)
    {
        ClientLogFacade.logWarning(LOGGER, msg.getClass().getSimpleName() + " should not be received by test: " + msg);
        insertIncomingMsg(msg);
        return msg;
    }
}
