package cz.uhk.dip.mmsparams.clientlib.websocket;


import cz.uhk.dip.mmsparams.api.exceptions.SystemException;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketCloseStatus;
import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public final class WebSocketHandler extends WebSocketListener
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(WebSocketHandler.class);
    private static final String TAG = WebSocketHandler.class.getSimpleName();

    private final IWebSocketReceiver iWebSocketReceiver;
    private final IErrorHandler iErrorHandler;

    public WebSocketHandler(IWebSocketReceiver iWebSocketReceiver, IErrorHandler iErrorHandler)
    {
        this.iWebSocketReceiver = iWebSocketReceiver;
        this.iErrorHandler = iErrorHandler;
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response)
    {
        ClientLogFacade.logAction(LOGGER, "WebSocket open");
    }

    @Override
    public void onMessage(WebSocket webSocket, String text)
    {
        output("Receiving : " + text);
        if (iWebSocketReceiver != null)
            iWebSocketReceiver.onMessage(webSocket, text);
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes)
    {
        ClientLogFacade.logWarning(LOGGER, "Received ByteString message: " + bytes.hex());
        this.onMessage(webSocket, bytes.hex());
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason)
    {
        if (code != WebSocketCloseStatus.NORMAL)
        {
            SystemException e = new SystemException(code + ":" + reason);
            ClientLogFacade.logException(LOGGER, e);
            iErrorHandler.notifyWebSocketError(TAG, "Socket closing: " + code + ":" + reason, e);
        }

        ClientLogFacade.logAction(LOGGER, "Closing : " + code + " / " + reason);
        webSocket.close(WebSocketCloseStatus.NORMAL, null);
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response)
    {
        ClientLogFacade.logException(LOGGER, t);
        SystemException ex = new SystemException("onFailure", t);
        iErrorHandler.notifyWebSocketError(TAG, "Socket failure " + response, ex);

    }

    private void output(String msg)
    {
        ClientLogFacade.logWsIncomingMessage(LOGGER, msg);
    }
}

