package cz.uhk.dip.mmsparams.clientlib.websocket;


import cz.uhk.dip.mmsparams.api.exceptions.SystemException;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.IMessageRegister;
import cz.uhk.dip.mmsparams.api.websocket.router.MessageRouter;
import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import okhttp3.WebSocket;
import okio.ByteString;

/**
 * Default implementation of {@link IWebSocketReceiver}
 */
public class WebSocketReceiver implements IWebSocketReceiver
{
    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(WebSocketReceiver.class);
    private static final String TAG = WebSocketReceiver.class.getSimpleName();

    private final MessageReceiveSub messageReceiveSub;
    private final IErrorHandler iErrorHandler;

    public WebSocketReceiver(IMessageRegister messageRegister, IErrorHandler iErrorHandler)
    {
        this.iErrorHandler = iErrorHandler;
        this.messageReceiveSub = new MessageReceiveSub(messageRegister, iErrorHandler);
    }

    @Override
    public void onMessage(WebSocket webSocket, String message)
    {
        ClientLogFacade.logWsIncomingMessage(LOGGER, message);
        MessageRouter.process(messageReceiveSub, message);
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes)
    {
        final String errMsg = "WebSocketReceiver does not support ByteString message";
        this.iErrorHandler.notifyWebSocketError(TAG, "errMsg", new SystemException(errMsg));
    }
}
