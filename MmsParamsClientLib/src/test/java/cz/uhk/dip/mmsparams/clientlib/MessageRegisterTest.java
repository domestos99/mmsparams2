package cz.uhk.dip.mmsparams.clientlib;

import org.junit.Test;

import java.util.List;

import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.messages.MessageRegister;
import cz.uhk.dip.mmsparams.api.messages.MessageRegisterItem;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.MessageIdGenerator;
import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MessageRegisterTest
{
    @Test
    public void insertIncomingMsgTest()
    {
        MessageRegister mr = new MessageRegister();

        GenericErrorResponseMessage err = MessageFactory.create(GenericErrorResponseMessage.class);

        assertEquals(0, mr.getIncomingCount(err.getClass()));
        mr.insertIncomingMsg(err);
        assertEquals(1, mr.getIncomingCount(err.getClass()));
        mr.insertIncomingMsg(err);
        assertEquals(2, mr.getIncomingCount(err.getClass()));
    }

    @Test
    public void getIncomByMsgIdTest()
    {
        final String msgId = "message-id";
        MessageRegister mr = new MessageRegister();

        GenericErrorResponseMessage err = new MessageBuilder()
                .withMessageId(msgId)
                .build(GenericErrorResponseMessage.class);

        WebSocketMessageBase msg = mr.getIncomByMsgId(new MessageId(msgId), MessageType.getKeyByClass(err.getClass()));
        assertNull(msg);

        mr.insertIncomingMsg(err);
        msg = mr.getIncomByMsgId(new MessageId(msgId), MessageType.getKeyByClass(err.getClass()));
        assertNotNull(msg);
    }

    @Test
    public void getIncomByClzTest()
    {
        MessageRegister mr = new MessageRegister();

        final String msgId = "message-id";
        GenericErrorResponseMessage err = new MessageBuilder()
                .withMessageId(msgId)
                .build(GenericErrorResponseMessage.class);

        List<MessageRegisterItem> msg = mr.getIncomByClz(MessageType.getKeyByClass(err.getClass()));
        assertNotNull(msg);
        assertEquals(0, msg.size());

        mr.insertIncomingMsg(err);
        msg = mr.getIncomByClz(MessageType.getKeyByClass(err.getClass()));
        assertNotNull(msg);
        assertEquals(1, msg.size());

        mr.insertIncomingMsg(err);
        msg = mr.getIncomByClz(MessageType.getKeyByClass(err.getClass()));
        assertNotNull(msg);
        assertEquals(2, msg.size());
    }


    @Test
    public void getIncomByDeviceIdTest()
    {
        MessageRegister mr = new MessageRegister();

        final String msgId = "message-id";
        GenericErrorResponseMessage err = new MessageBuilder()
                .withMessageId(msgId)
                .build(GenericErrorResponseMessage.class);

        PhoneInfoModel di = new PhoneInfoModel();
        di.setPhoneKey(MessageIdGenerator.getNext());
        err.setSenderKey(di.getPhoneKey());

        String msgKey = MessageType.getKeyByClass(err.getClass());


        List<WebSocketMessageBase> msg = mr.getIncomByDeviceId(di, msgKey);
        assertNotNull(msg);
        assertEquals(0, msg.size());


        mr.insertIncomingMsg(err);
        msg = mr.getIncomByDeviceId(di, msgKey);
        assertNotNull(msg);
        assertEquals(1, msg.size());

        mr.insertIncomingMsg(err);
        msg = mr.getIncomByDeviceId(di, msgKey);
        assertNotNull(msg);
        assertEquals(2, msg.size());
    }
}
