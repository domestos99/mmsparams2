package cz.uhk.dip.mmsparams.clientlib;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;
import cz.uhk.dip.mmsparams.api.http.ServerAddressProvider;

import static org.junit.Assert.assertFalse;

public class RunnableTestTest extends UnitTestBase
{

    @Before
    public void setUp() throws Exception
    {

    }

    @Test
    public void initNullTest()
    {
        assertFalse(new TestA("A", "B").runTest());
    }

    @Test
    public void wrongTestSettingsTest()
    {
        assertFalse(new TestB("A", "B").runTest());
    }


    class TestA extends RunnableTest
    {
        public TestA(String testName, String testDesc)
        {
            super(testName, testDesc);
        }

        @Override
        protected TestSettings init()
        {
            return null;
        }

        @Override
        protected void run(ITestInstance test) throws Exception
        {

        }
    }

    class TestB extends RunnableTest
    {

        public TestB(String testName, String testDesc)
        {
            super(testName, testDesc);
        }

        @Override
        protected TestSettings init()
        {
            TestSettings ts = new TestSettings(new ServerAddressProvider(HttpProtocolEnum.HTTP, "xx"), 10, "u", "p");
            return ts;
        }

        @Override
        protected void run(ITestInstance test) throws Exception
        {

        }
    }
}
