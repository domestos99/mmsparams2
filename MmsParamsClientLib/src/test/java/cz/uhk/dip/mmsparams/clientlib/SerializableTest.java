package cz.uhk.dip.mmsparams.clientlib;

import org.junit.Test;
import org.reflections.Reflections;

import java.io.Serializable;
import java.util.Set;

import cz.uhk.dip.mmsparams.api.json.JsonUtils;

import static org.junit.Assert.fail;

public class SerializableTest extends UnitTestBase
{

    private final String clzPackage = "cz.uhk.dip.mmsparams.clientlib";

    @Test
    public void serializableTest() throws IllegalAccessException, InstantiationException
    {
        Class clazz = Serializable.class;
        Reflections reflections = new Reflections(clzPackage);
        Set<Class> subTypes = reflections.getSubTypesOf(clazz);

        for (Class c : subTypes)
        {
            if (c.isInterface())
                continue;

            if (c.isEnum())
            {
                continue;
            }

            if (!c.getPackage().getName().startsWith(clzPackage))
                continue;

            try
            {
                System.out.println(c.getName());

                Object o = c.newInstance();
                String json = JsonUtils.toJson(o);
                Object o2 = JsonUtils.fromJson(json, c);

            }
            catch (Exception e)
            {
                System.out.println(c.getName());
                e.printStackTrace();
                fail();
            }
        }

    }
}
