package cz.uhk.dip.mmsparams.clientlib;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.CommonUtils;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.errors.ErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;

public class UnitTestBase
{
    protected String getUUID()
    {
        return CommonUtils.getUUID();
    }

    protected MessageId getNewMessageId()
    {
        return new MessageId(getUUID());
    }

    protected IErrorHandler getErrorHandler()
    {
        return new ErrorHandler();
    }

    protected PhoneInfoModel getTestPhone()
    {
        PhoneInfoModel phone = new PhoneInfoModel();
        phone.setPhoneKey(GenericConstants.PHONE_KEY);
        return phone;
    }
}
