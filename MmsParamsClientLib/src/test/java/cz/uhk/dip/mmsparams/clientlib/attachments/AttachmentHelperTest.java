package cz.uhk.dip.mmsparams.clientlib.attachments;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.NoSuchFileException;

import cz.uhk.dip.mmsparams.api.constants.MediaType;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsAttachmentSendModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AttachmentHelperTest
{

    @Test
    public void loadImageTest() throws IOException
    {
        MmsAttachmentSendModel attach;
        try
        {
            attach = AttachmentHelper.loadImage("src/test/resources/small.jpg");
        }
        catch (NoSuchFileException e)
        {
            attach = AttachmentHelper.loadImage("MmsParamsClientLib/src/test/resources/small.jpg");
        }
        assertNotNull(attach);
        assertEquals("jpg", attach.getExtension());
        assertEquals(MediaType.IMAGE, attach.getMediaType());
        assertEquals("image/jpeg", attach.getMimeType());
    }

    @Test(expected = IOException.class)
    public void loadImage_error_Test() throws IOException
    {
        MmsAttachmentSendModel attach = AttachmentHelper.loadImage("src/test/resources/xxx.jpg");
    }

    @Test
    public void loadVideoTest() throws IOException
    {
        MmsAttachmentSendModel attach;
        try
        {
            attach = AttachmentHelper.loadVideo("src/test/resources/video.avi");
        }
        catch (NoSuchFileException e)
        {
            attach = AttachmentHelper.loadVideo("MmsParamsClientLib/src/test/resources/video.avi");
        }

        assertNotNull(attach);
        assertEquals("avi", attach.getExtension());
        assertEquals(MediaType.VIDEO, attach.getMediaType());
        assertEquals("video/x-msvideo", attach.getMimeType());
    }

    @Test
    public void loadSoundTest() throws IOException
    {
        MmsAttachmentSendModel attach;
        try
        {
            attach = AttachmentHelper.loadAudio("src/test/resources/sound.wav");
        }
        catch (NoSuchFileException e)
        {
            attach = AttachmentHelper.loadAudio("MmsParamsClientLib/src/test/resources/sound.wav");
        }

        assertNotNull(attach);
        assertEquals("wav", attach.getExtension());
        assertEquals(MediaType.AUDIO, attach.getMediaType());
        assertEquals("audio/wav", attach.getMimeType());
    }

    @Test
    public void loadDocumentTest() throws IOException
    {
        MmsAttachmentSendModel attach;
        try
        {
            attach = AttachmentHelper.loadDocument("src/test/resources/file.txt");
        }
        catch (NoSuchFileException e)
        {
            attach = AttachmentHelper.loadDocument("MmsParamsClientLib/src/test/resources/file.txt");
        }

        assertNotNull(attach);
        assertEquals("txt", attach.getExtension());
        assertEquals(MediaType.DOCUMENT, attach.getMediaType());
        assertEquals("text/plain", attach.getMimeType());
    }
}
