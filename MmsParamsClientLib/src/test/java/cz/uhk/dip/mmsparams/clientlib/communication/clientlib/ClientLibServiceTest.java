package cz.uhk.dip.mmsparams.clientlib.communication.clientlib;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.messages.IMessageRegister;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.messages.MessageRegister;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.clientlib.TestDescription;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfoInternal;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.TestInfo;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ClientLibServiceTest extends UnitTestBase
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private IWebSocketSender sender;

    @Mock
    private IWebSocketWaiter waiter;

    private IClientLibService service;

    @Before
    public void setUp() throws Exception
    {
        sender = mock(IWebSocketSender.class);
        waiter = mock(IWebSocketWaiter.class);
        this.service = new ClientLibService(sender, waiter);
    }

    @Test
    public void registerClientLibTest()
    {
        final IMessageRegister iMessageRegister = new MessageRegister();

        when(sender.sendMessage(argThat(argument -> {
            if (RegisterClientLibMessage.class.equals(argument.getClass()))
            {
                iMessageRegister.insertIncomingMsg(MessageFactory.create(GenericBooleanResponseMessage.class));
                return true;
            }
            return false;
        }))).thenReturn(new MessageId(GenericConstants.MESSAGE_ID));

        TestDescription testDescription = new TestDescription("UT", "UT - test");
        MessageId msgId = service.registerClientLib(testDescription);

        assertNotNull(msgId);
        assertEquals(1, iMessageRegister.getAllIncoming().size());
    }

    @Test
    public void sendTestFinishedOkTest()
    {
        final IMessageRegister iMessageRegister = new MessageRegister();

        when(sender.sendMessage(argThat(argument -> {
            if (TestResultMessage.class.equals(argument.getClass()))
            {
                iMessageRegister.insertIncomingMsg(MessageFactory.create(GenericBooleanResponseMessage.class));
                return true;
            }
            return false;
        }))).thenReturn(new MessageId(GenericConstants.MESSAGE_ID));

        TestDescription testDescription = new TestDescription("UT", "UT - test");
        ITestInfoInternal info = new TestInfo(iMessageRegister, getWSConnectSettings(), GenericConstants.TEST_ID, GenericConstants.CLIENT_KEY, testDescription);
        MessageId msgId = this.service.sendTestFinishedOk(info);

        assertNotNull(msgId);
        assertEquals(1, iMessageRegister.getAllIncoming().size());
    }

    private WSConnectSettings getWSConnectSettings()
    {
        return null;
    }
}
