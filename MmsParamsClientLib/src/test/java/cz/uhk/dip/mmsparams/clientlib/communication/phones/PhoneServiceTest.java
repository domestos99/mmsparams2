package cz.uhk.dip.mmsparams.clientlib.communication.phones;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.exceptions.TestInvalidException;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PhoneServiceTest extends UnitTestBase
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private IWebSocketSender sender;

    @Mock
    private IWebSocketWaiter waiter;

    @Before
    public void setUp()
    {
        sender = mock(IWebSocketSender.class);
        waiter = mock(IWebSocketWaiter.class);
    }

    @Test
    public void getAllPhonesTest()
    {
        PhoneListResponseMessage expected = new MessageBuilder()
                .build(PhoneListResponseMessage.class);
        expected.setPhoneInfoModels(new ArrayList<>());

        registerGetAllMock(expected);

        PhoneService phoneService = new PhoneService(sender, waiter);
        List<PhoneInfoModel> phones = phoneService.getAllPhones();
        assertNotNull(phones);
    }

    @Test
    public void getByIMSITest()
    {
        PhoneListResponseMessage expected = new PhoneListResponseMessage();

        ArrayList<PhoneInfoModel> expectedPhones = new ArrayList<>();
        PhoneInfoModel phone = new PhoneInfoModel();
        phone.setPhoneKey(GenericConstants.PHONE_KEY);
        phone.setImsi(GenericConstants.PHONE_NUMBER);
        expectedPhones.add(phone);
        expected.setPhoneInfoModels(expectedPhones);


        registerGetAllMock(expected);

        PhoneService phoneService = new PhoneService(sender, waiter);
        PhoneInfoModel foundPhone = phoneService.getByIMSI(GenericConstants.PHONE_NUMBER);

        assertNotNull(foundPhone);
        assertEquals(phone, foundPhone);
    }

    @Test(expected = TestInvalidException.class)
    public void getByIMSITest_Ex_Test()
    {
        PhoneService phoneService = new PhoneService(sender, waiter);
        phoneService.getByIMSI(null);
    }


    @Test(expected = TestInvalidException.class)
    public void getByIMSITest_Ex2_Test()
    {
        PhoneListResponseMessage expected = new MessageBuilder()
                .build(PhoneListResponseMessage.class);
        expected.setPhoneInfoModels(new ArrayList<>());

        registerGetAllMock(expected);

        PhoneService phoneService = new PhoneService(sender, waiter);
        phoneService.getByIMSI(GenericConstants.DUMMY);
    }

    @Test
    public void getByCustomNameTest()
    {
        PhoneListResponseMessage expected = new MessageBuilder()
                .build(PhoneListResponseMessage.class);

        ArrayList<PhoneInfoModel> expectedPhones = new ArrayList<>();
        PhoneInfoModel phone = new PhoneInfoModel();
        phone.setPhoneKey(GenericConstants.PHONE_KEY);
        phone.setPhoneCustomName(GenericConstants.PHONE_CUSTOM_NAME);
        expectedPhones.add(phone);
        expected.setPhoneInfoModels(expectedPhones);

        registerGetAllMock(expected);

        PhoneService phoneService = new PhoneService(sender, waiter);
        PhoneInfoModel foundPhone = phoneService.getByCustomName(GenericConstants.PHONE_CUSTOM_NAME);

        assertNotNull(foundPhone);
        assertEquals(phone, foundPhone);
    }

    @Test(expected = TestInvalidException.class)
    public void getByCustomName_Ex_Test()
    {
        PhoneService phoneService = new PhoneService(sender, waiter);
        phoneService.getByCustomName(null);
    }

    private void registerGetAllMock(PhoneListResponseMessage expected)
    {
        when(sender.sendMessage(argThat(argument -> PhoneListRequestMessage.class.equals(argument.getClass())))).thenReturn(new MessageId(GenericConstants.MESSAGE_ID));

        when(waiter.waitForResponseMessage(argThat((ArgumentMatcher<MessageId>) argument -> GenericConstants.MESSAGE_ID.equals(argument.getMessageId())),
                argThat(argument -> PhoneListResponseMessage.class.equals(argument))))
                .thenReturn(expected);
    }
}
