package cz.uhk.dip.mmsparams.clientlib.communication.sms;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.exceptions.TestInvalidException;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.mock.DummyWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.mock.DummyWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class SmsServiceMustTimeoutTest extends UnitTestBase
{
    private IWebSocketSender webSocketSender;
    private IWebSocketWaiter webSocketReceiver;
    ISmsServiceMustTimeout iSmsServiceMustTimeout;

    @Before
    public void setUp()
    {
        webSocketSender = new DummyWebSocketSender();
        webSocketReceiver = new DummyWebSocketWaiter();
        iSmsServiceMustTimeout = new SmsServiceMustTimeout(webSocketSender, webSocketReceiver);
    }

    @Test
    public void deliveryReportTest()
    {
        SmsSendMessageId smsSendMessageId = new SmsSendMessageId(GenericConstants.SMS_SEND_MESSAGE_ID);
        boolean result = iSmsServiceMustTimeout.deliveryReport(smsSendMessageId);
        assertTrue(result);
    }

    @Test
    public void deliveryReport_error_Test()
    {
        try
        {
            boolean result = iSmsServiceMustTimeout.deliveryReport(null);
            fail();
        }
        catch (TestInvalidException e)
        {
        }
        try
        {
            boolean result = iSmsServiceMustTimeout.deliveryReport(new SmsSendMessageId());
            fail();
        }
        catch (TestInvalidException e)
        {
        }
        try
        {
            boolean result = iSmsServiceMustTimeout.deliveryReport(new SmsSendMessageId(GenericConstants.SMS_SEND_MESSAGE_ID), new CustomTimeout(-2));
            fail();
        }
        catch (TestInvalidException e)
        {
        }
    }


    @Test
    public void smsReceiveTest()
    {
        PhoneInfoModel phoneInfoModel = getTestPhone();
        boolean result = iSmsServiceMustTimeout.smsReceive(phoneInfoModel);
        assertTrue(result);
    }

    @Test
    public void smsReceive_error_Test()
    {
        try
        {
            boolean result = iSmsServiceMustTimeout.smsReceive(null);
            fail();
        }
        catch (TestInvalidException e)
        {
        }
        try
        {
            PhoneInfoModel model = new PhoneInfoModel();
            boolean result = iSmsServiceMustTimeout.smsReceive(model);
            fail();
        }
        catch (TestInvalidException e)
        {
        }
        try
        {
            PhoneInfoModel model = getTestPhone();
            boolean result = iSmsServiceMustTimeout.smsReceive(model, new CustomTimeout(-2));
            fail();
        }
        catch (TestInvalidException e)
        {
        }
    }
}
