package cz.uhk.dip.mmsparams.clientlib.communication.sms;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.exceptions.TestInvalidException;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.mock.DummyWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.mock.DummyWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class SmsServiceTest extends UnitTestBase
{
    private IWebSocketSender webSocketSender;
    private IWebSocketWaiter webSocketReceiver;

    @Before
    public void setUp()
    {
        webSocketSender = new DummyWebSocketSender();
        webSocketReceiver = new DummyWebSocketWaiter();
    }

    @Test
    public void sendSmsTest()
    {
        ISmsService iSmsService = new SmsService(webSocketSender, webSocketReceiver);

        SmsSendModel sms = new SmsSendModel();
        sms.setTo("123");
        PhoneInfoModel phone = getTestPhone();
        SmsSendMessageId result = iSmsService.sendSms(sms, phone);
        assertNotNull(result);
        assertNotNull(result.getMessageId());
    }

    @Test
    public void sendSms_invalid_Test()
    {
        ISmsService iSmsService = new SmsService(webSocketSender, webSocketReceiver);
        try
        {
            SmsSendMessageId result = iSmsService.sendSms(null, null);
            fail();
        }
        catch (TestInvalidException e)
        {

        }

        try
        {
            SmsSendModel sms = new SmsSendModel();
            sms.setTo("123");
            SmsSendMessageId result = iSmsService.sendSms(sms, null);
            fail();
        }
        catch (TestInvalidException e)
        {

        }

        try
        {
            PhoneInfoModel phone = getTestPhone();
            SmsSendMessageId result = iSmsService.sendSms(null, phone);
            fail();
        }
        catch (TestInvalidException e)
        {

        }

        try
        {
            SmsSendModel sms = new SmsSendModel();
            sms.setTo("123");
            PhoneInfoModel phone = new PhoneInfoModel();
            SmsSendMessageId result = iSmsService.sendSms(sms, phone);
            fail();
        }
        catch (TestInvalidException e)
        {

        }

        try
        {
            SmsSendModel sms = new SmsSendModel();
            PhoneInfoModel phone = getTestPhone();
            SmsSendMessageId result = iSmsService.sendSms(sms, phone);
            fail();
        }
        catch (TestInvalidException e)
        {

        }
    }

    @Test
    public void interfacesTest()
    {
        ISmsService iSmsService = new SmsService(webSocketSender, webSocketReceiver);

        ISmsServiceWait waitFor = iSmsService.waitFor();
        ISmsServiceMustTimeout timeout = iSmsService.mustTimeoutFor();
        ISmsGroupService group = iSmsService.SmsGroup();

        assertNotNull(waitFor);
        assertNotNull(timeout);
        assertNotNull(group);
    }


}