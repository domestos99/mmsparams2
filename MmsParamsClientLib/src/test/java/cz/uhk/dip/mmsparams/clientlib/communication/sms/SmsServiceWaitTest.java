package cz.uhk.dip.mmsparams.clientlib.communication.sms;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.exceptions.TestInvalidException;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.mock.DummyWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.mock.DummyWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;

import static org.junit.Assert.fail;

public class SmsServiceWaitTest extends UnitTestBase
{
    private IWebSocketSender webSocketSender;
    private IWebSocketWaiter webSocketReceiver;
    private ISmsServiceWait iSmsServiceMustTimeout;

    @Before
    public void setUp()
    {
        webSocketSender = new DummyWebSocketSender();
        webSocketReceiver = new DummyWebSocketWaiter();
        iSmsServiceMustTimeout = new SmsServiceWait(webSocketSender, webSocketReceiver);
    }


    @Test
    public void smsSendSuccessfullyTest()
    {
        SmsSendMessageId smsSendMessageId = new SmsSendMessageId(GenericConstants.SMS_SEND_MESSAGE_ID);
        SmsSendResponseModel result = iSmsServiceMustTimeout.smsSentSuccessfully(smsSendMessageId);
    }

    @Test
    public void smsSendSuccessfully_error_Test()
    {
        try
        {
            SmsSendResponseModel result = iSmsServiceMustTimeout.smsSentSuccessfully(null);
            fail();
        }
        catch (TestInvalidException e)
        {

        }
        try
        {
            SmsSendResponseModel result = iSmsServiceMustTimeout.smsSentSuccessfully(new SmsSendMessageId());
            fail();
        }
        catch (TestInvalidException e)
        {

        }
    }


    @Test
    public void anySmsReceiveTest()
    {
        PhoneInfoModel smsSendMessageId = getTestPhone();
        SmsReceiveModel result = iSmsServiceMustTimeout.anySmsReceived(smsSendMessageId);
    }

    @Test
    public void anySmsReceive_error_Test()
    {
        try
        {
            PhoneInfoModel smsSendMessageId = new PhoneInfoModel();
            SmsReceiveModel result = iSmsServiceMustTimeout.anySmsReceived(smsSendMessageId);
            fail();
        }
        catch (TestInvalidException e)
        {
        }
        try
        {
            SmsReceiveModel result = iSmsServiceMustTimeout.anySmsReceived(null);
            fail();
        }
        catch (TestInvalidException e)
        {
        }
    }

    @Test
    public void anySmsReceiveAllPartsTest()
    {
        PhoneInfoModel smsSendMessageId = getTestPhone();
        SmsReceiveModel result = (SmsReceiveModel) iSmsServiceMustTimeout.anySmsReceiveAllParts(smsSendMessageId);
    }

    @Test
    public void anySmsReceiveAllParts_error_Test()
    {
        try
        {
            PhoneInfoModel smsSendMessageId = new PhoneInfoModel();
            SmsReceiveModel result = (SmsReceiveModel) iSmsServiceMustTimeout.anySmsReceiveAllParts(smsSendMessageId);
            fail();
        }
        catch (TestInvalidException e)
        {
        }
        try
        {
            SmsReceiveModel result = (SmsReceiveModel) iSmsServiceMustTimeout.anySmsReceiveAllParts(null);
            fail();
        }
        catch (TestInvalidException e)
        {
        }
    }

    @Test
    public void anyDeliveryReportTest()
    {
        SmsSendMessageId smsSendMessageId = new SmsSendMessageId(GenericConstants.SMS_SEND_MESSAGE_ID);
        SmsDeliveryReport result = iSmsServiceMustTimeout.anyDeliveryReport(smsSendMessageId);
    }

    @Test
    public void anyDeliveryReport_error_Test()
    {
        try
        {
            SmsSendMessageId smsSendMessageId = new SmsSendMessageId();
            SmsDeliveryReport result = iSmsServiceMustTimeout.anyDeliveryReport(smsSendMessageId);
            fail();
        }
        catch (TestInvalidException e)
        {

        }
        try
        {
            SmsDeliveryReport result = iSmsServiceMustTimeout.anyDeliveryReport(null);
            fail();
        }
        catch (TestInvalidException e)
        {

        }
    }
}
