package cz.uhk.dip.mmsparams.clientlib.communication.websocket;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.exceptions.TimeOutException;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.messages.MessageRegister;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.MessageIdGenerator;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.mock.MockTestSettings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class WebSocketWaiter2Test extends UnitTestBase
{

    @Test
    public void waitForResponseMessageTest() throws Exception
    {
        MessageRegister mr = new MessageRegister();
        TestSettings ts = MockTestSettings.create();
        IWebSocketWaiter ww = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = getTestMsg();

        GenericErrorResponseMessage err = null;
        try
        {
            err = ww.waitForResponseMessage(new MessageId(msg.getMessageID()), msg.getClass());
            fail();
        }
        catch (TimeOutException ex)
        {
        }

        mr.insertIncomingMsg(msg);

        err = ww.waitForResponseMessage(new MessageId(msg.getMessageID()), msg.getClass());
        assertNotNull(err);

        GenericErrorResponseMessage msg2 = getTestMsg();
        mr.insertIncomingMsg(msg2);

        err = ww.waitForResponseMessage(new MessageId(msg.getMessageID()), msg.getClass());
        assertNotNull(err);
        assertEquals(msg.getMessageID(), err.getMessageID());
    }


    @Test
    public void waitForResponseMessageMustTimeoutTest() throws Exception
    {
        MessageRegister mr = new MessageRegister();
        TestSettings ts = MockTestSettings.create();
        WebSocketWaiter2 ww = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = getTestMsg();

        assertTrue(ww.waitForResponseMessageMustTimeout(new MessageId(msg.getMessageID()), msg.getClass()));

        mr.insertIncomingMsg(msg);

        assertFalse(ww.waitForResponseMessageMustTimeout(new MessageId(msg.getMessageID()), msg.getClass()));

        GenericErrorResponseMessage msg2 = getTestMsg();
        mr.insertIncomingMsg(msg2);
        assertFalse(ww.waitForResponseMessageMustTimeout(new MessageId(msg.getMessageID()), msg.getClass()));
    }

    @Test
    public void waitForGenericBooleanResponseMessageMustTimeoutTest() throws Exception
    {
        MessageRegister mr = new MessageRegister();
        TestSettings ts = MockTestSettings.create();
        WebSocketWaiter2 ww = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericBooleanResponseMessage msg = getTestMsgBool();

        assertTrue(ww.waitForGenericBooleanResponseMessageMustTimeout(new MessageId(msg.getMessageID())));

        mr.insertIncomingMsg(msg);

        assertFalse(ww.waitForGenericBooleanResponseMessageMustTimeout(new MessageId(msg.getMessageID())));

        GenericBooleanResponseMessage msg2 = getTestMsgBool();
        mr.insertIncomingMsg(msg2);
        assertFalse(ww.waitForGenericBooleanResponseMessageMustTimeout(new MessageId(msg.getMessageID())));
    }

    @Test
    public void waitForResponseMessage1Test() throws Exception
    {
        // DeviceInfo deviceInfo, Class<T> clz, int expectedCount, int timeout

        MessageRegister mr = new MessageRegister();
        TestSettings ts = MockTestSettings.create();
        IWebSocketWaiter ww = new WebSocketWaiter2(mr, ts, getErrorHandler());

        PhoneInfoModel di = new PhoneInfoModel();
        di.setPhoneKey(MessageIdGenerator.getNext());

        GenericErrorResponseMessage msg = getTestMsg();
        msg.setSenderKey(di.getPhoneKey());


        GenericErrorResponseMessage err = null;
        try
        {
            err = ww.waitForResponseMessage(di, msg.getClass());
            fail();
        }
        catch (TimeOutException ex)
        {
        }

        mr.insertIncomingMsg(msg);

        err = ww.waitForResponseMessage(di, msg.getClass());
        assertNotNull(err);


        GenericErrorResponseMessage msg2 = getTestMsg();
        mr.insertIncomingMsg(msg2);

        err = ww.waitForResponseMessage(di, msg.getClass());
        assertNotNull(err);
        assertEquals(msg.getMessageID(), err.getMessageID());


    }

    @Test
    public void waitForResponseMessageMustTimeout1Test() throws Exception
    {
        // DeviceInfo deviceInfo, Class<T> clz, int timeout
        MessageRegister mr = new MessageRegister();
        TestSettings ts = MockTestSettings.create();
        WebSocketWaiter2 ww = new WebSocketWaiter2(mr, ts, getErrorHandler());

        PhoneInfoModel di = new PhoneInfoModel();
        di.setPhoneKey(MessageIdGenerator.getNext());

        GenericErrorResponseMessage msg = getTestMsg();
        msg.setSenderKey(di.getPhoneKey());

        assertTrue(ww.waitForResponseMessageMustTimeout(di, msg.getClass()));

        mr.insertIncomingMsg(msg);

        assertFalse(ww.waitForResponseMessageMustTimeout(di, msg.getClass()));

        GenericErrorResponseMessage msg2 = getTestMsg();
        mr.insertIncomingMsg(msg2);
        msg2.setSenderKey(di.getPhoneKey());
        assertFalse(ww.waitForResponseMessageMustTimeout(di, msg.getClass()));
    }

    @Test
    public void waitForResponseMessage3Test() throws Exception
    {
        // Class<T> clz, int expectedCount

        MessageRegister mr = new MessageRegister();
        TestSettings ts = MockTestSettings.create();
        IWebSocketWaiter ww = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = getTestMsg();

        GenericErrorResponseMessage err = null;
        try
        {
            err = ww.waitForResponseMessage(new MessageId(msg.getMessageID()), msg.getClass(), 1);
            fail();
        }
        catch (TimeOutException ex)
        {
        }

        mr.insertIncomingMsg(msg);

        err = ww.waitForResponseMessage(new MessageId(msg.getMessageID()), msg.getClass(), 1);
        assertNotNull(err);


        GenericErrorResponseMessage msg2 = getTestMsg();
        mr.insertIncomingMsg(msg2);

        err = ww.waitForResponseMessage(new MessageId(msg.getMessageID()), msg.getClass(), 2);
        assertNotNull(err);
        assertEquals(msg.getMessageID(), err.getMessageID());
    }

    @Test
    public void waitForResponseMessageMustTimeout2Test() throws Exception
    {
        MessageRegister mr = new MessageRegister();
        TestSettings ts = MockTestSettings.create();
        WebSocketWaiter2 ww = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = getTestMsg();

        assertTrue(ww.waitForResponseMessageMustTimeout(msg.getClass()));

        mr.insertIncomingMsg(msg);

        assertFalse(ww.waitForResponseMessageMustTimeout(msg.getClass()));

        GenericErrorResponseMessage msg2 = getTestMsg();
        mr.insertIncomingMsg(msg2);
        assertFalse(ww.waitForResponseMessageMustTimeout(msg.getClass()));
    }


    @Test
    public void waitForResponseMessageSmscTest() throws Exception
    {
        MessageRegister mr = new MessageRegister();
        TestSettings ts = MockTestSettings.create();
        IWebSocketWaiter ww = new WebSocketWaiter2(mr, ts, getErrorHandler());


        SmscSessionId sessionId = new SmscSessionId("sessionId");

        SmscDeliveryReportMessage msg = new MessageBuilder().build(SmscDeliveryReportMessage.class);
        msg.setSmscSessionId(sessionId);


        SmscDeliveryReportMessage err = null;
        try
        {
            err = ww.waitForResponseMessage(sessionId, msg.getClass());
            fail();
        }
        catch (TimeOutException ex)
        {
        }

        mr.insertIncomingMsg(msg);

        err = ww.waitForResponseMessage(sessionId, msg.getClass());
        assertNotNull(err);


        SmscDeliveryReportMessage msg2 = new MessageBuilder().build(SmscDeliveryReportMessage.class);
        // msg2.setSmscSessionId(sessionId); // TODO expected count problem
        mr.insertIncomingMsg(msg2);

        err = ww.waitForResponseMessage(sessionId, msg.getClass());
        assertNotNull(err);
        assertEquals(msg.getMessageID(), err.getMessageID());


    }

    @Test
    public void waitForResponseMessageSmscMustTimeout1Test() throws Exception
    {
        // DeviceInfo deviceInfo, Class<T> clz, int timeout
        MessageRegister mr = new MessageRegister();
        TestSettings ts = MockTestSettings.create();
        WebSocketWaiter2 ww = new WebSocketWaiter2(mr, ts, getErrorHandler());

        SmscSessionId sessionId = new SmscSessionId("sessionId");
        SmscDeliveryReportMessage msg = new MessageBuilder().build(SmscDeliveryReportMessage.class);
        msg.setSmscSessionId(sessionId);


        assertTrue(ww.waitForResponseMessageMustTimeout(sessionId, msg.getClass()));

        mr.insertIncomingMsg(msg);

        assertFalse(ww.waitForResponseMessageMustTimeout(sessionId, msg.getClass()));

        SmscDeliveryReportMessage msg2 = new MessageBuilder().build(SmscDeliveryReportMessage.class);
        mr.insertIncomingMsg(msg2);
        msg2.setSmscSessionId(sessionId);
        assertFalse(ww.waitForResponseMessageMustTimeout(sessionId, msg.getClass()));
    }


    private GenericErrorResponseMessage getTestMsg()
    {
        GenericErrorResponseMessage gm =
                new MessageBuilder()
                        .withMessageIdRandom()
                        .build(GenericErrorResponseMessage.class);
        return gm;
    }

    private GenericBooleanResponseMessage getTestMsgBool()
    {
        return
                new MessageBuilder()
                        .withMessageIdRandom()
                        .build(GenericBooleanResponseMessage.class);
    }
}
