package cz.uhk.dip.mmsparams.clientlib.communication.websocket;

import org.junit.Test;

import java.util.List;

import cz.uhk.dip.mmsparams.api.exceptions.TimeOutException;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.messages.MessageRegister;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.facades.TestUtils;
import cz.uhk.dip.mmsparams.clientlib.mock.MockTestSettings;
import cz.uhk.dip.mmsparams.clientlib.utils.MessageIDProvider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class WebSocketWaiterTest extends UnitTestBase
{
    @Test
    public void countTest1()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        final PhoneInfoModel di = new PhoneInfoModel();

        di.setPhoneKey(MessageIDProvider.generateSenderKey());

        GenericBooleanResponseMessage genMsg1 = getTestMsg(di);
        GenericBooleanResponseMessage genMsg2 = getTestMsg(di);

        mr.insertIncomingMsg(genMsg1);
        mr.insertIncomingMsg(genMsg2);

        new Thread(() -> {
            try
            {
                Thread.sleep(2000);

                GenericBooleanResponseMessage genMsg3 = getTestMsg(di);
                mr.insertIncomingMsg(genMsg3);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        ).start();

        List<GenericBooleanResponseMessage> list = iWebSocketWaiter.waitForResponseMessage(di, GenericBooleanResponseMessage.class, 3);

        assertNotNull(list);
        assertEquals(3, list.size());
    }

    @Test(expected = TimeOutException.class)
    public void countTest2()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        final PhoneInfoModel di = new PhoneInfoModel();

        di.setPhoneKey(MessageIDProvider.generateSenderKey());

        GenericBooleanResponseMessage genMsg1 = getTestMsg(di);
        GenericBooleanResponseMessage genMsg2 = getTestMsg(di);

        mr.insertIncomingMsg(genMsg1);
        mr.insertIncomingMsg(genMsg2);

        new Thread(() -> {
            try
            {
                Thread.sleep(10000);

                GenericBooleanResponseMessage genMsg3 = getTestMsg(di);
                mr.insertIncomingMsg(genMsg3);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        ).start();

        List<GenericBooleanResponseMessage> list = iWebSocketWaiter.waitForResponseMessage(di, GenericBooleanResponseMessage.class, 3);

        assertNotNull(list);
        assertEquals(3, list.size());
    }


    @Test
    public void waitForGenericBooleanResponseMessageTest()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericBooleanResponseMessage msg = MessageFactory.create(GenericBooleanResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        mr.insertIncomingMsg(msg);

        GenericBooleanResponseMessage response = iWebSocketWaiter.waitForGenericBooleanResponseMessage(new MessageId(msg.getMessageID()));

        assertEquals(msg.getClass(), response.getClass());
        assertEquals(msg.isValue(), response.isValue());
    }


    @Test(expected = TimeOutException.class)
    public void waitForGenericBooleanResponseMessage_notFound_Test()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericBooleanResponseMessage msg = MessageFactory.create(GenericBooleanResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        GenericBooleanResponseMessage response = iWebSocketWaiter.waitForGenericBooleanResponseMessage(new MessageId(msg.getMessageID()));

        assertEquals(msg.getClass(), response.getClass());
        assertEquals(msg.isValue(), response.isValue());
    }

    @Test
    public void waitForGenericBooleanResponseMessage_timeoutTest()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericBooleanResponseMessage msg = MessageFactory.create(GenericBooleanResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        mr.insertIncomingMsg(msg);

        GenericBooleanResponseMessage response = iWebSocketWaiter.waitForGenericBooleanResponseMessage(new MessageId(msg.getMessageID()), 5);

        assertEquals(msg.getClass(), response.getClass());
        assertEquals(msg.isValue(), response.isValue());
    }


    @Test(expected = TimeOutException.class)
    public void waitForGenericBooleanResponseMessage_notFound_timeout_Test()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericBooleanResponseMessage msg = MessageFactory.create(GenericBooleanResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        GenericBooleanResponseMessage response = iWebSocketWaiter.waitForGenericBooleanResponseMessage(new MessageId(msg.getMessageID()), 5);

        assertEquals(msg.getClass(), response.getClass());
        assertEquals(msg.isValue(), response.isValue());
    }


    @Test
    public void waitForGenericBooleanResponseMessageMustTimeoutTest()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericBooleanResponseMessage msg = MessageFactory.create(GenericBooleanResponseMessage.class);

        TestUtils.sendTestMessage(msg);
        mr.insertIncomingMsg(msg);

        boolean response = iWebSocketWaiter.waitForGenericBooleanResponseMessageMustTimeout(new MessageId(msg.getMessageID()));
        assertFalse(response);
    }


    @Test
    public void waitForGenericBooleanResponseMessageMustTimeout_notFound_Test()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericBooleanResponseMessage msg = MessageFactory.create(GenericBooleanResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        boolean response = iWebSocketWaiter.waitForGenericBooleanResponseMessageMustTimeout(new MessageId(msg.getMessageID()));

        assertTrue(response);
    }


    @Test
    public void waitForGenericBooleanResponseMessageMustTimeout_timeoutTest()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericBooleanResponseMessage msg = MessageFactory.create(GenericBooleanResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        mr.insertIncomingMsg(msg);

        boolean response = iWebSocketWaiter.waitForGenericBooleanResponseMessageMustTimeout(new MessageId(msg.getMessageID()), 5);

        assertFalse(response);
    }


    @Test
    public void waitForGenericBooleanResponseMessageMustTimeout_notFound_timeout_Test()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericBooleanResponseMessage msg = MessageFactory.create(GenericBooleanResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        // nevlozit zpravu - true =nenalezena
        boolean response = iWebSocketWaiter.waitForGenericBooleanResponseMessageMustTimeout(new MessageId(msg.getMessageID()), 5);

        assertTrue(response);
    }


    @Test
    public void waitForResponseMessageTest()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        TestUtils.sendTestMessage(msg);
        mr.insertIncomingMsg(msg);

        GenericErrorResponseMessage response = iWebSocketWaiter.waitForResponseMessage(new MessageId(msg.getMessageID()), GenericErrorResponseMessage.class);

        assertEquals(msg.getClass(), response.getClass());
    }

    @Test(expected = TimeOutException.class)
    public void waitForResponseMessage_notFound_Test()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        GenericErrorResponseMessage response = iWebSocketWaiter.waitForResponseMessage(new MessageId(msg.getMessageID()), GenericErrorResponseMessage.class);

        assertEquals(msg.getClass(), response.getClass());
    }

    @Test
    public void waitForResponseMessage_timeoutTest() throws Exception
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        TestUtils.sendTestMessage(msg);
        mr.insertIncomingMsg(msg);

        GenericErrorResponseMessage response = iWebSocketWaiter.waitForResponseMessage(new MessageId(msg.getMessageID()), GenericErrorResponseMessage.class, 10);

        assertEquals(msg.getClass(), response.getClass());
    }

    @Test(expected = TimeOutException.class)
    public void waitForResponseMessage_notFound_timeout_Test()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        GenericErrorResponseMessage response = iWebSocketWaiter.waitForResponseMessage(new MessageId(msg.getMessageID()), GenericErrorResponseMessage.class, 5);

        assertEquals(msg.getClass(), response.getClass());
    }

    @Test
    public void waitForResponseMessageMustTimeout_timeout_Test()
    {

        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        boolean wasFound = iWebSocketWaiter.waitForResponseMessageMustTimeout(new MessageId(msg.getMessageID()), GenericErrorResponseMessage.class);

        assertTrue(wasFound);
    }

    @Test
    public void waitForResponseMessageMustTimeout_not_timeout_Test()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        TestUtils.sendTestMessage(msg);
        mr.insertIncomingMsg(msg);

        boolean wasFound = iWebSocketWaiter.waitForResponseMessageMustTimeout(new MessageId(msg.getMessageID()), GenericErrorResponseMessage.class);

        assertFalse(wasFound);
    }

    @Test
    public void waitForResponseMessageMustTimeout_timeout_timeout_Test()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        boolean wasFound = iWebSocketWaiter.waitForResponseMessageMustTimeout(new MessageId(msg.getMessageID()), GenericErrorResponseMessage.class, 5);

        assertTrue(wasFound);
    }

    @Test
    public void waitForResponseMessageMustTimeout_not_timeout_timeout_Test()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        TestUtils.sendTestMessage(msg);
        mr.insertIncomingMsg(msg);

        boolean wasFound = iWebSocketWaiter.waitForResponseMessageMustTimeout(new MessageId(msg.getMessageID()), GenericErrorResponseMessage.class, 5);

        assertFalse(wasFound);
    }


    @Test
    public void waitForResponseMessage_Test()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        TestUtils.sendTestMessage(msg);
        mr.insertIncomingMsg(msg);

        GenericErrorResponseMessage response = iWebSocketWaiter.waitForResponseMessage(GenericErrorResponseMessage.class);

        assertEquals(msg.getClass(), response.getClass());
    }

    @Test(expected = TimeOutException.class)
    public void waitForResponseMessage_notFOund_Test()
    {
        final TestSettings ts = MockTestSettings.create();
        final MessageRegister mr = new MessageRegister();
        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(mr, ts, getErrorHandler());

        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        TestUtils.sendTestMessage(msg);

        GenericErrorResponseMessage response = iWebSocketWaiter.waitForResponseMessage(GenericErrorResponseMessage.class);

        assertEquals(msg.getClass(), response.getClass());
    }


    private GenericBooleanResponseMessage getTestMsg(PhoneInfoModel di)
    {
        GenericBooleanResponseMessage genMsg1 = MessageFactory.create(GenericBooleanResponseMessage.class);
        genMsg1.setSenderKey(di.getPhoneKey());
        genMsg1.setMessageID(MessageIDProvider.generateMessageID());
        return genMsg1;
    }


}
