package cz.uhk.dip.mmsparams.clientlib.errors;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.exceptions.SystemException;
import cz.uhk.dip.mmsparams.api.exceptions.TestErrorException;
import cz.uhk.dip.mmsparams.api.exceptions.TestInvalidException;
import cz.uhk.dip.mmsparams.api.exceptions.TimeOutException;
import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ErrorHandlerTest extends UnitTestBase
{
    private IErrorHandler errorHandler;

    @Before
    public void setUp() throws Exception
    {
        this.errorHandler = getErrorHandler();
    }

    @Test
    public void checkErrorThrowEmptyTest()
    {
        errorHandler.checkErrorThrow();
    }

    @Test(expected = TestErrorException.class)
    public void checkErrorThrowTest()
    {
        GenericErrorResponseMessage msg = getErrorMessage();
        errorHandler.setError(msg);
        errorHandler.checkErrorThrow();
    }

    @Test
    public void isErrorPresent_Test()
    {
        assertFalse(errorHandler.isErrorPresent());
        GenericErrorResponseMessage msg = getErrorMessage();
        errorHandler.setError(msg);
        assertTrue(errorHandler.isErrorPresent());
        assertNotNull(errorHandler.getError());
    }

    @Test(expected = TimeOutException.class)
    public void throwTimeOut_Test()
    {
        errorHandler.throwTimeOut("TAG", "INFO");
    }

    @Test
    public void notifyNewException_Test()
    {
        errorHandler.notifyNewUncaughtException(new TimeOutException());
    }

    @Test
    public void notifyNewException_with_send_Test()
    {
        final boolean[] send = {false};
        errorHandler.setWebSocketSender(new IWebSocketSender()
        {
            @Override
            public MessageId sendMessage(WebSocketMessageBase message)
            {
                send[0] = true;
                return new MessageId(GenericConstants.MESSAGE_ID);
            }

            @Override
            public MessageId sendMessage(WebSocketMessageBase message, boolean ingoreErrors)
            {
                send[0] = true;
                return null;
            }

            @Override
            public String getSenderKey()
            {
                return GenericConstants.SENDER_KEY;
            }
        });

        errorHandler.notifyNewUncaughtException(new SystemException());
        assertTrue(send[0]);
    }

    @Test
    public void notifyWebSocketErrorTest()
    {
        TestInvalidException ex = new TestInvalidException("X");
        errorHandler.notifyWebSocketError("TEST", "test message", ex);
        assertTrue(errorHandler.isErrorPresent());

        try
        {
            errorHandler.checkErrorThrow();
            fail();
        }
        catch (TestInvalidException e)
        {
            assertEquals("X", e.getMessage());
        }
    }


    @Test
    public void notifyNewException_with_send_errro_while_sending_Test()
    {
        final boolean[] send = {false};
        errorHandler.setWebSocketSender(new IWebSocketSender()
        {
            @Override
            public MessageId sendMessage(WebSocketMessageBase message)
            {
                throw new RuntimeException();
            }

            @Override
            public MessageId sendMessage(WebSocketMessageBase message, boolean ingoreErrors)
            {
                throw new RuntimeException();
            }

            @Override
            public String getSenderKey()
            {
                return GenericConstants.SENDER_KEY;
            }
        });

        errorHandler.notifyNewUncaughtException(new SystemException());
        assertFalse(send[0]);
    }

    private GenericErrorResponseMessage getErrorMessage()
    {
        GenericErrorResponseMessage msg = new MessageBuilder()
                .withSenderKey("sender")
                .withMessageIdRandom()
                .withTestId(new ITestIdProvider()
                {
                    @Override
                    public String getTestId()
                    {
                        return "test_id";
                    }
                })
                .build(GenericErrorResponseMessage.class);

        TestErrorModel model = new TestErrorModel();
        model.setMessage("Error message");
        model.setException(new TestErrorException("Test exception"));
        model.setTestErrorType(TestErrorType.TEST_ERROR);
        msg.setTestErrorModel(model);
        return msg;
    }
}
