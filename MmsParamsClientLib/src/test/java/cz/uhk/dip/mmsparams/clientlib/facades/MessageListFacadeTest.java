//package cz.uhk.dip.mmsparams.clientlib.facades;
//
//import org.junit.Test;
//
//import java.util.List;
//
//import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
//import cz.uhk.dip.mmsparams.clientlib.model.MessageId;
//import cz.uhk.dip.mmsparams.websocket.MessageType;
//import cz.uhk.dip.mmsparams.websocket.WebSocketMessageBase;
//import cz.uhk.dip.mmsparams.websocket.messages.DevMessage;
//import cz.uhk.dip.mmsparams.websocket.messages.EmptyMessage;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertNull;
//import static org.junit.Assert.assertTrue;
//
//public class MessageListFacadeTest extends UnitTestBase
//{
//
//    @Test
//    public void insertIncomingMsgTest()
//    {
//        EmptyMessage msg = new EmptyMessage();
//        msg.setMessageID(getUUID());
//        MessageListFacade.insertIncomingMsg(msg);
//    }
//
//    @Test
//    public void isIncommingReceivedTest()
//    {
//        EmptyMessage msg = new EmptyMessage();
//        String msgId = getUUID();
//        msg.setMessageID(msgId);
//
//        assertFalse(MessageListFacade.isIncommingReceived(msgId));
//        MessageListFacade.insertIncomingMsg(msg);
//        assertTrue(MessageListFacade.isIncommingReceived(msgId));
//    }
//
//    @Test
//    public void getIncomMessageTest()
//    {
//        MessageId msgId = new MessageId(getUUID());
//        String msgKey = MessageType.getKeyByClass(DevMessage.class);
//
//        assertNull(MessageListFacade.getIncomMessage(msgId, msgKey));
//
//        DevMessage devMessage = new DevMessage();
//        devMessage.setMessageID(msgId.getMessageId());
//        devMessage.setMessageKey(msgKey);
//
//        MessageListFacade.insertIncomingMsg(devMessage);
//
//        WebSocketMessageBase msg = MessageListFacade.getIncomMessage(msgId, msgKey);
//
//        assertNotNull(msg);
//    }
//
//    @Test
//    public void getIncommingByIdTest()
//    {
//        MessageId msgId = getNewMessageId();
//        String msgKey = MessageType.getKeyByClass(DevMessage.class);
//
//        assertNull(MessageListFacade.getIncommingById(msgId));
//
//        DevMessage devMessage = new DevMessage();
//        devMessage.setMessageID(msgId.getMessageId());
//        devMessage.setMessageKey(msgKey);
//
//        MessageListFacade.insertIncomingMsg(devMessage);
//
//        WebSocketMessageBase msg = MessageListFacade.getIncommingById(msgId);
//
//        assertNotNull(msg);
//    }
//
//
//    @Test
//    public void getIncommingByPhoneKeyTest()
//    {
//        String msgId = getUUID();
//        String senderKey = getUUID();
//        String msgKey = MessageType.getKeyByClass(DevMessage.class);
//
//
//        List<DevMessage> list = MessageListFacade.getIncommingByPhoneKey(senderKey, DevMessage.class);
//        assertNotNull(list);
//        assertEquals(0, list.size());
//
//        assertTrue(MessageListFacade.isIncommingByPhoneKeyCount(senderKey, DevMessage.class, 0));
//        assertFalse(MessageListFacade.isIncommingByPhoneKeyCount(senderKey, DevMessage.class, 1));
//
//
//        DevMessage devMessage = new DevMessage();
//        devMessage.setMessageID(msgId);
//        devMessage.setMessageKey(msgKey);
//        devMessage.setSenderKey(senderKey);
//
//        MessageListFacade.insertIncomingMsg(devMessage);
//
//
//        List<DevMessage> list1 = MessageListFacade.getIncommingByPhoneKey(senderKey, DevMessage.class);
//        assertNotNull(list1);
//        assertEquals(1, list1.size());
//
//        assertFalse(MessageListFacade.isIncommingByPhoneKeyCount(senderKey, DevMessage.class, 0));
//        assertTrue(MessageListFacade.isIncommingByPhoneKeyCount(senderKey, DevMessage.class, 1));
//
//
//        String msgId2 = getUUID();
//        DevMessage devMessage2 = new DevMessage();
//        devMessage2.setMessageID(msgId2);
//        devMessage2.setMessageKey(msgKey);
//        devMessage2.setSenderKey(senderKey);
//        MessageListFacade.insertIncomingMsg(devMessage2);
//
//        List<DevMessage> list2 = MessageListFacade.getIncommingByPhoneKey(senderKey, DevMessage.class);
//        assertNotNull(list2);
//        assertEquals(2, list2.size());
//
//        assertFalse(MessageListFacade.isIncommingByPhoneKeyCount(senderKey, DevMessage.class, 0));
//        assertFalse(MessageListFacade.isIncommingByPhoneKeyCount(senderKey, DevMessage.class, 1));
//        assertTrue(MessageListFacade.isIncommingByPhoneKeyCount(senderKey, DevMessage.class, 2));
//        assertFalse(MessageListFacade.isIncommingByPhoneKeyCount(senderKey, DevMessage.class, 3));
//    }
//
//
//}
