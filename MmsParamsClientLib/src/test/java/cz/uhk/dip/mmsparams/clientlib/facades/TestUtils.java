package cz.uhk.dip.mmsparams.clientlib.facades;

import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.MessageIdGenerator;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class TestUtils
{
    public static WebSocketMessageBase sendTestMessage(final WebSocketMessageBase message)
    {
        if (message == null)
        {
            return null;
        }

        message.setSenderKey(getSenderKey());
        message.setMessageID(generateMessageKey());

        String json = MessageUtils.getSendableMessage(message);

        if (StringUtil.isEmptyOrNull(json))
        {
            return null;
        }

        System.out.println("Sending: " + message);
//        MessageListFacade.insertSendingMsg(message);
        return message;
    }


    public static String getSenderKey()
    {
        return "GroovyClient_" + System.currentTimeMillis();
    }

    private static String generateMessageKey()
    {
        return "Groovy_" + MessageIdGenerator.getNext();
    }

}
