package cz.uhk.dip.mmsparams.clientlib.groups;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.TestGroupModel;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class TestGroupSingletonTest extends UnitTestBase
{
    @Test
    public void test()
    {
        assertNotNull(TestGroupSingleton.getInstance());
        assertNull(TestGroupSingleton.getInstance().getTestGroupModel());
        TestGroupModel model = new TestGroupModel();
        TestGroupSingleton.getInstance().setTestGroupModel(model);
        assertEquals(model, TestGroupSingleton.getInstance().getTestGroupModel());
        TestGroupSingleton.getInstance().clearTestGroup();
        assertNull(TestGroupSingleton.getInstance().getTestGroupModel());
    }

}
