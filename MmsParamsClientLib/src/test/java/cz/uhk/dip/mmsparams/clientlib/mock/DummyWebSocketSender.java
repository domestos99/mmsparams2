package cz.uhk.dip.mmsparams.clientlib.mock;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.WebSocketServiceBase;

public class DummyWebSocketSender extends WebSocketServiceBase implements IWebSocketSender
{
    @Override
    public void init(WSConnectSettings wsConnectSettings)
    {

    }

    @Override
    public MessageId sendMessage(WebSocketMessageBase message)
    {
        String sendMsg = getSendableMessage(new ITestIdProvider()
        {
            @Override
            public String getTestId()
            {
                return GenericConstants.TEST_ID;
            }
        }, message);
        WebSocketMessageBase msg = MessageUtils.getMessageNonTyped(sendMsg);
        return new MessageId(msg.getMessageID());
    }

    @Override
    public MessageId sendMessage(WebSocketMessageBase message, boolean ingoreErrors)
    {
        return sendMessage(message, true);
    }

    @Override
    public void disconnect()
    {

    }

    @Override
    public String getSenderKey()
    {
        return GenericConstants.SENDER_KEY;
    }

    @Override
    protected void sendJson(String json)
    {

    }
}
