package cz.uhk.dip.mmsparams.clientlib.mock;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;

public class DummyWebSocketWaiter implements cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter
{
    @Override
    public <T extends WebSocketMessageBase> T waitForResponseMessage(MessageId messageID, Class<T> clz, int timeout)
    {
        return new MessageBuilder()
                .withMessageId(messageID.getMessageId())
                .build(clz);
    }

    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(MessageId messageID, Class<T> clz, int timeout)
    {
        return true;
    }

    @Override
    public <T extends WebSocketMessageBase> List<T> waitForResponseMessage(PhoneInfoModel phoneInfoModel, Class<T> clz, int expectedCount, int timeout)
    {
        List<T> l = new ArrayList<>(expectedCount);
        for (int i = 0; i < expectedCount; i++)
            l.add(new MessageBuilder()
                    .build(clz));
        return l;
    }

    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(PhoneInfoModel phoneInfoModel, Class<T> clz, int timeout)
    {
        return true;
    }

    @Override
    public <T extends WebSocketMessageBase> List<T> waitForResponseMessage(Class<T> clz, int expectedCount, int timeout)
    {
        List<T> l = new ArrayList<>();
        for (int i = 0; i < expectedCount; i++)
            l.add(new MessageBuilder()
                    .build(clz));
        return l;
    }

    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(Class<T> clz, int timeout)
    {
        return true;
    }

    @Override
    public <T extends WebSocketMessageBase> List<T> waitForResponseMessage(SmscSessionId smscSessionId, Class<T> clz, int expectedCount, int timeout)
    {
        List<T> l = new ArrayList<>();
        for (int i = 0; i < expectedCount; i++)
            l.add(new MessageBuilder()
                    .build(clz));
        return l;
    }

    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(SmscSessionId smscSessionId, Class<T> clz, int timeout)
    {
        return true;
    }
}
