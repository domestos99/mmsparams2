package cz.uhk.dip.mmsparams.clientlib.mock;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.api.interfaces.IClientKeyProvider;
import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.logging.ILogger;
import cz.uhk.dip.mmsparams.api.messages.MessageRegister;
import cz.uhk.dip.mmsparams.api.utils.ConsoleUtil;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.TestDescription;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.communication.ClientKeyProvider;
import cz.uhk.dip.mmsparams.clientlib.communication.TestIdProvider;
import cz.uhk.dip.mmsparams.clientlib.communication.clientlib.ClientLibService;
import cz.uhk.dip.mmsparams.clientlib.communication.clientlib.IClientLibService;
import cz.uhk.dip.mmsparams.clientlib.communication.email.IEmailMessageService;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.IMmsService;
import cz.uhk.dip.mmsparams.clientlib.communication.mms.MmsService;
import cz.uhk.dip.mmsparams.clientlib.communication.mmsc.IMmscService;
import cz.uhk.dip.mmsparams.clientlib.communication.mmsc.MmscService;
import cz.uhk.dip.mmsparams.clientlib.communication.phones.IPhoneService;
import cz.uhk.dip.mmsparams.clientlib.communication.phones.PhoneService;
import cz.uhk.dip.mmsparams.clientlib.communication.remoteconnect.IRemoteConnectService;
import cz.uhk.dip.mmsparams.clientlib.communication.remoteconnect.RemoteConnectService;
import cz.uhk.dip.mmsparams.clientlib.communication.sms.ISmsService;
import cz.uhk.dip.mmsparams.clientlib.communication.sms.SmsService;
import cz.uhk.dip.mmsparams.clientlib.communication.smsc.ISmscService;
import cz.uhk.dip.mmsparams.clientlib.communication.smsc.SmscService;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfo;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfoInternal;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.TestInfo;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketHolder;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketService;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.WebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.WebSocketService;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.WebSocketWaiter2;
import cz.uhk.dip.mmsparams.clientlib.errors.ErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationService;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationService;
import cz.uhk.dip.mmsparams.clientlib.websocket.WebSocketReceiver;

public class MockTestInstance implements ITestInstance
{


    private MockTestInstance(TestSettings testSettings, String testName, String testDesc, IWebSocketService webSocketObject, IPhoneService iPhoneService,
                             ISmsService iSmsService, IMmsService iMmsService,
                             ISmscService iSmscService, IMmscService iMmscService,
                             IClientLibService iClientLibService, IValidationService iValidationService,
                             IRemoteConnectService iRemoteConnectService, ITestInfoInternal testInfo, IErrorHandler iErrorHandler)
    {
        this.testSettings = testSettings;
        this.testName = testName;
        this.testDesc = testDesc;
        this.webSocketObject = webSocketObject;
        this.iPhoneService = iPhoneService;
        this.iSmsService = iSmsService;
        this.iMmsService = iMmsService;
        this.iMmscService = iMmscService;
        this.iSmscService = iSmscService;
        this.iClientLibService = iClientLibService;
        this.iValidationService = iValidationService;
        this.iRemoteConnectService = iRemoteConnectService;
        this.testInfo = testInfo;
        this.iErrorHandler = iErrorHandler;

        setupUnhandledException();
    }

    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(MockTestInstance.class);

    private final String testName;
    private final String testDesc;
    private final IWebSocketService webSocketObject;
    private final IPhoneService iPhoneService;
    private final ISmsService iSmsService;
    private final IMmsService iMmsService;
    private final IMmscService iMmscService;
    private final ISmscService iSmscService;
    private final IValidationService iValidationService;
    private final IClientLibService iClientLibService;
    private final TestSettings testSettings;
    private final ITestInfoInternal testInfo;
    private final IErrorHandler iErrorHandler;
    private final IRemoteConnectService iRemoteConnectService;

    public static ITestInstance initNewTest(TestSettings testSettings, MessageRegister messageRegister)
    {
        ITestIdProvider testIdProvider = new TestIdProvider();
        IErrorHandler iErrorHandler = new ErrorHandler();

        WebSocketReceiver webSocketReceiver = new WebSocketReceiver(messageRegister, iErrorHandler);

        IWebSocketHolder iWebSocketHolder = new MockWebSocketHolder();

        IClientKeyProvider iClientKeyProvider = new ClientKeyProvider();

        IWebSocketService webSocketObject = new WebSocketService(iWebSocketHolder, webSocketReceiver, messageRegister,
                testIdProvider, iClientKeyProvider, iErrorHandler);

        IWebSocketSender iWebSocketSender = new WebSocketSender(webSocketObject);

        IWebSocketWaiter iWebSocketWaiter = new WebSocketWaiter2(messageRegister, testSettings, iErrorHandler);


        IPhoneService ds = new PhoneService(iWebSocketSender, iWebSocketWaiter);
        ISmsService ss = new SmsService(iWebSocketSender, iWebSocketWaiter);

        IMmsService mmsService = new MmsService(iWebSocketSender, iWebSocketWaiter);

        IMmscService iMmscService = new MmscService(iWebSocketSender, iWebSocketWaiter);


        ISmscService smscService = new SmscService(iWebSocketSender, iWebSocketWaiter);


        IClientLibService gs = new ClientLibService(iWebSocketSender, iWebSocketWaiter);

        IValidationService iValidationService = new ValidationService(iWebSocketSender, iWebSocketWaiter);

        TestDescription testDescription = new TestDescription("UT", "UT - test");

        ITestInfoInternal iTestInfo = new TestInfo(messageRegister, getWSConnectSettings(testSettings), testIdProvider.getTestId(), GenericConstants.CLIENT_KEY, testDescription);

        IRemoteConnectService iRemoteConnectService = new RemoteConnectService(iWebSocketSender, iWebSocketWaiter, ss, smscService, ds, testSettings);


        MockTestInstance ti = new MockTestInstance(testSettings, "Test", "test desc", webSocketObject,
                ds, ss, mmsService, smscService, iMmscService, gs, iValidationService, iRemoteConnectService, iTestInfo, iErrorHandler);

        ti.startTest();

        return ti;
    }

    private static WSConnectSettings getWSConnectSettings(TestSettings testSettings)
    {
        return null;
    }


    private void setupUnhandledException()
    {
        Thread.UncaughtExceptionHandler h = new Thread.UncaughtExceptionHandler()
        {
            public void uncaughtException(Thread th, Throwable ex)
            {
                ClientLogFacade.logActionWithColor(LOGGER, "Uncaught exception: " + ex, ConsoleUtil.RED_BACKGROUND);
                if (webSocketObject != null)
                    webSocketObject.disconnect();
            }
        };
        Thread.currentThread().setUncaughtExceptionHandler(h);
    }

    @Override
    public IPhoneService Phones()
    {
        return iPhoneService;
    }

    @Override
    public ISmsService Sms()
    {
        return iSmsService;
    }

    @Override
    public IMmsService Mms()
    {
        return iMmsService;
    }

    @Override
    public IMmscService Mmsc()
    {
        return iMmscService;
    }

    @Override
    public ISmscService Smsc()
    {
        return iSmscService;
    }

//    @Override
//    public ILogger Logger()
//    {
//        return null;
//    }

    @Override
    public IValidationService Validation()
    {
        return iValidationService;
    }

    @Override
    public TestSettings getTestSettings()
    {
        return this.testSettings;
    }

    @Override
    public ITestInfo TestInfo()
    {
        return null;
    }

    @Override
    public IRemoteConnectService RemoteConnect()
    {
        return this.iRemoteConnectService;
    }

    private void startTest()
    {
        WSConnectSettings wsConnectSettings = new WSConnectSettings(getTestSettings().getServerAddressProvider(), "xx", "yy");
        // Init web socket
        webSocketObject.init(wsConnectSettings);

        TestDescription testDescription = new TestDescription("UT", "UT - test");
        iClientLibService.registerClientLib(testDescription);
    }


    @Override
    public boolean testFinished()
    {
        // Send test finished

        if (webSocketObject != null)
            webSocketObject.disconnect();

        ClientLogFacade.logAction(LOGGER, "--------------END---------------");
        ClientLogFacade.logActionWithColor(LOGGER, "-----------Test OK--------------", ConsoleUtil.GREEN_BACKGROUND);
        return true;
    }

    @Override
    public void testFinishedWithError(Exception e)
    {

    }

    @Override
    public IEmailMessageService Email()
    {
        return null;
    }
}
