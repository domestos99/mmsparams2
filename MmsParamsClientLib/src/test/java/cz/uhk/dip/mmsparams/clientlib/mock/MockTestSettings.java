package cz.uhk.dip.mmsparams.clientlib.mock;

import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;
import cz.uhk.dip.mmsparams.api.http.ServerAddressProvider;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;

public class MockTestSettings
{
    public static TestSettings create()
    {
        TestSettings ts = new TestSettings(new ServerAddressProvider(HttpProtocolEnum.HTTP, "test"), 3, 10, "xx", "xx");


        return ts;
    }
}
