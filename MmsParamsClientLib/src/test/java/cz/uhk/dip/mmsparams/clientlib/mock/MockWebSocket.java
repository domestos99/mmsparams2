package cz.uhk.dip.mmsparams.clientlib.mock;

import org.junit.Assert;

import java.util.ArrayList;

import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.DeliveryIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotifyRespIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReportReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7SubmitResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.websocket.WebSocketHandler;

public class MockWebSocket
{
    private final WebSocketHandler webSocketHandler;

    public MockWebSocket(WebSocketHandler listener)
    {
        this.webSocketHandler = listener;
    }

    public void send(String json)
    {
        WebSocketMessageBase msg = MessageUtils.getMessageNonTyped(json);
        String msgKey = msg.getMessageKey();

        if (MessageType.Phone_List_Request_Message.equals(msgKey))
        {
            PhoneListResponseMessage resp = MessageFactory.createServerSenderResponse(PhoneListResponseMessage.class, msg);

            PhoneInfoModel phoneInfoModel = new PhoneInfoModel();
            phoneInfoModel.setPhoneCustomName("phoneA");
            phoneInfoModel.setPhoneKey("phone-key");
            ArrayList<PhoneInfoModel> l = new ArrayList<>();
            l.add(phoneInfoModel);
            resp.setPhoneInfoModels(l);

            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp));
            return;
        }
        else if (MessageType.Register_ClientLib_Message.equals(msgKey))
        {
            GenericBooleanResponseMessage resp = MessageFactory.createServerSenderResponse(GenericBooleanResponseMessage.class, msg);
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp));
            return;
        }
        else if (MessageType.Lock_Phone_Message.equals(msgKey))
        {
            GenericBooleanResponseMessage resp = MessageFactory.createServerSenderResponse(GenericBooleanResponseMessage.class, msg);
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp));
            return;
        }
        else if (MessageType.Sms_SendPhone_Request_Message.equals(msgKey))
        {
            GenericBooleanResponseMessage respGen = MessageFactory.createServerSenderResponse(GenericBooleanResponseMessage.class, msg);
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(respGen));

            SmsSendPhoneResponseMessage resp = MessageFactory.create(SmsSendPhoneResponseMessage.class);
            resp = MessageUtils.prepareResponse(resp, msg);
            resp.setSenderKey("phone-key");

            SmsSendResponseModel model = new SmsSendResponseModel();
            model.setResult(-1);
            resp.setSmsSendResponseModel(model);

            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp));


            SmsReceivePhoneMessage resp2 = MessageFactory.createClientBroadcastMessage(SmsReceivePhoneMessage.class);
            resp2.setSenderKey("phone-key");
            resp2.setMessageID(msg.getMessageID());
            resp2.setTestID(msg.getTestID());


            SmsReceiveModel smsReceiveModel = new SmsReceiveModel();
            ArrayList<String> body = new ArrayList<>();
            body.add("xxx");
            smsReceiveModel.setDisplayMessageBody(body);
            resp2.setSmsReceiveModel(smsReceiveModel);


            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp2));

            SmsSendDeliveryReportMessage resp3 = MessageFactory.create(SmsSendDeliveryReportMessage.class);
            resp3 = MessageUtils.prepareResponse(resp3, msg);
            resp3.setSenderKey("phone-key");

            SmsDeliveryReport dr = new SmsDeliveryReport();
            resp3.setSmsDeliveryReport(dr);

            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp3));


            return;
        }

        else if (MessageType.MmsRecipient_Phone_Profile_Request_Message.equals(msgKey))
        {
            GenericBooleanResponseMessage resp = MessageFactory.create(GenericBooleanResponseMessage.class);
            resp = MessageUtils.prepareResponse(resp, msg);
            resp.setSenderKey("phone-key");
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp));
            return;
        }
        else if (MessageType.Mms_SendPhone_Request_Message.equals(msgKey))
        {
            GenericBooleanResponseMessage respGen = MessageFactory.createServerSenderResponse(GenericBooleanResponseMessage.class, msg);
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(respGen));

            SendConfResponseMessage resp = MessageFactory.create(SendConfResponseMessage.class);
            resp = MessageUtils.prepareResponse(resp, msg);
            resp.setSenderKey("phone-key");
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp));

            NotificationIndResponseMessage resp1 = MessageFactory.create(null, NotificationIndResponseMessage.class);
            resp1 = MessageUtils.prepareResponse(resp1, msg);
            resp1.setSenderKey("phone-key");
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp1));

            RetrieveConfResponseMessage resp2 = MessageFactory.create(RetrieveConfResponseMessage.class);
            resp2 = MessageUtils.prepareResponse(resp2, msg);
            resp2.setSenderKey("phone-key");
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp2));

            NotifyRespIndResponseMessage resp3 = MessageFactory.create(null, NotifyRespIndResponseMessage.class);
            resp3 = MessageUtils.prepareResponse(resp3, msg);
            resp3.setSenderKey("phone-key");
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp3));

            DeliveryIndResponseMessage resp4 = MessageFactory.create(DeliveryIndResponseMessage.class);
            resp4 = MessageUtils.prepareResponse(resp4, msg);
            resp4.setSenderKey("phone-key");
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp4));

            return;
        }


        else if (MessageType.Smsc_Connect_Message.equals(msgKey))
        {
            SmscConnectResponseMessage resp = MessageFactory.createServerSenderResponse(SmscConnectResponseMessage.class, msg);

            SmscConnectResponseModel model = new SmscConnectResponseModel();
            model.setStatus(true);
            model.setSessionId(new SmscSessionId("session1"));
            resp.setSmscConnectResponseModel(model);

            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp));
            return;
        }
        else if (MessageType.Smsc_SendSms_Message.equals(msgKey))
        {
            SmscSendSmsResponseMessage resp = MessageFactory.createServerSenderResponse(SmscSendSmsResponseMessage.class, msg);

            SmscSendSmsResponseModel model = new SmscSendSmsResponseModel();

            resp.setSmscSendSmsResponseModel(model);

            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp));


            SmscDeliveryReportMessage report = MessageFactory.createServerSender(SmscDeliveryReportMessage.class);
            report.setMessageID(msg.getMessageID());
            report.setTestID(msg.getTestID());
            report.setRecipientKey(msg.getSenderKey());
            report.setSmscSessionId(new SmscSessionId("session1"));

            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(report));

            return;


        }

        else if (MessageType.Mmsc_Send_Message.equals(msgKey))
        {
            MM7SubmitResponseMessage resp = MessageFactory.createServerSenderResponse(MM7SubmitResponseMessage.class, msg);
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp));

            MM7DeliveryReqMessage resp2 = MessageFactory.createServerSenderResponse(MM7DeliveryReqMessage.class, msg);
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp2));

            MM7DeliveryReportReqMessage resp3 = MessageFactory.createServerSenderResponse(MM7DeliveryReportReqMessage.class, msg);
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp3));


            return;

        }
        else if (MessageType.Smsc_Disconnect_Message.equals(msgKey))
        {
            GenericBooleanResponseMessage resp = MessageFactory.createServerSenderResponse(GenericBooleanResponseMessage.class, msg);
            webSocketHandler.onMessage(null, JsonUtilsSafe.toJson(resp));
            return;
        }
        else if (MessageType.Test_Validation_Message.equals(msgKey))
        {
            return;
        }


        System.out.println("Fail:");
        System.out.println(json);
        Assert.fail();


    }

    public void close(int code, String reason)
    {
        return;
    }
}
