package cz.uhk.dip.mmsparams.clientlib.mock;

import cz.uhk.dip.mmsparams.api.http.WSConnectSettings;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketHolder;
import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;
import cz.uhk.dip.mmsparams.clientlib.websocket.IWebSocketReceiver;
import cz.uhk.dip.mmsparams.clientlib.websocket.WebSocketHandler;

public class MockWebSocketHolder implements IWebSocketHolder
{
    private MockWebSocket mockWebSocket;

    @Override
    public void init(IWebSocketReceiver iWebSocketReceiver, IErrorHandler iErrorHandler, WSConnectSettings wsConnectSettings)
    {
        WebSocketHandler listener = new WebSocketHandler(iWebSocketReceiver, iErrorHandler);
        this.mockWebSocket = new MockWebSocket(listener);
    }

    @Override
    public boolean isWsNull()
    {
        return mockWebSocket == null;
    }

    @Override
    public boolean send(String json)
    {
        mockWebSocket.send(json);
        return true;
    }

    @Override
    public boolean close(int code, String reason)
    {
        mockWebSocket.close(code, reason);
        return true;
    }
}
