//package cz.uhk.dip.mmsparams.clientlib.mock;
//
//
//import cz.uhk.dip.mmsparams.api.constants.MemoryConstants;
//import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
//import cz.uhk.dip.mmsparams.api.logging.ILogger;
//import cz.uhk.dip.mmsparams.api.messages.MessageId;
//import cz.uhk.dip.mmsparams.api.messages.MessageRegister;
//import cz.uhk.dip.mmsparams.api.utils.StringUtil;
//import cz.uhk.dip.mmsparams.api.websocket.WebSocketCloseStatus;
//import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
//import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketService;
//import cz.uhk.dip.mmsparams.clientlib.communication.websocket.WebSocketServiceBase;
//import cz.uhk.dip.mmsparams.clientlib.errors.IErrorHandler;
//import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
//import cz.uhk.dip.mmsparams.clientlib.logger.ClientLoggerFactory;
//import cz.uhk.dip.mmsparams.clientlib.utils.MessageIDProvider;
//import cz.uhk.dip.mmsparams.clientlib.websocket.IWebSocketReceiver;
//import cz.uhk.dip.mmsparams.clientlib.websocket.WebSocketHandler;
//
//public class MockWebSocketService extends WebSocketServiceBase implements IWebSocketService
//{
//    private MockWebSocket ws;
//    private final String senderKey;
//    private final IWebSocketReceiver iWebSocketReceiver;
//    private final MessageRegister messageRegister;
//    private final ITestIdProvider iTestIdProvider;
//    private final IErrorHandler iErrorHandler;
//
//    private static final ILogger LOGGER = ClientLoggerFactory.getLogger(MockWebSocketService.class);
//
//    public MockWebSocketService(IWebSocketReceiver iWebSocketReceiver, MessageRegister messageRegister, ITestIdProvider iTestIdProvider, IErrorHandler iErrorHandler)
//    {
//        this.iTestIdProvider = iTestIdProvider;
//        this.messageRegister = messageRegister;
//        this.iErrorHandler = iErrorHandler;
//        this.senderKey = MessageIDProvider.generateSenderKey();
//        this.iWebSocketReceiver = iWebSocketReceiver;
//    }
//
//    @Override
//    public void init(String serverAddress)
//    {
//        connectToServer(serverAddress);
//    }
//
//    @Override
//    public String getSenderKey()
//    {
//        return senderKey;
//    }
//
//    @Override
//    public MessageId sendMessage(WebSocketMessageBase message, boolean ignoreErrors)
//    {
//        return null;
//    }
//
//    private void connectToServer(String serverAddress)
//    {
//        WebSocketHandler listener = new WebSocketHandler(iWebSocketReceiver);
//        ws = new MockWebSocket(listener);
//    }
//
//    @Override
//    public MessageId sendMessage(WebSocketMessageBase message)
//    {
//        iErrorHandler.checkErrorThrow();
//
//        if (ws == null)
//        {
//            return null;
//        }
//        if (message == null)
//        {
//            return null;
//        }
//
//        String json = super.getSendableMessage(iTestIdProvider, message);
//
//        if (StringUtil.isEmptyOrNull(json))
//        {
//            return null;
//        }
//
//        if (json.length() >= MemoryConstants.MB16)
//        {
//            ClientLogFacade.logWarning(LOGGER,  "JSON may be too large to send. Size: " + json.length());
//        }
//
//        messageRegister.insertOutgoingMsg(message);
//
//        sendJson(json);
//
//        return new MessageId(message.getMessageID());
//    }
//
//
//    @Override
//    protected void sendJson(String json)
//    {
//        ws.send(json);
//    }
//
//    @Override
//    public void disconnect()
//    {
//        if (ws != null)
//        {
//            ws.close(WebSocketCloseStatus.NORMAL, "Test finished");
//        }
//    }
//
//
//}
