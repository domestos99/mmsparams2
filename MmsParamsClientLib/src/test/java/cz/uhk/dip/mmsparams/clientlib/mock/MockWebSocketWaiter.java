package cz.uhk.dip.mmsparams.clientlib.mock;

import java.util.List;

import cz.uhk.dip.mmsparams.api.messages.MessageId;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;

public class MockWebSocketWaiter implements IWebSocketWaiter
{
    private static final String TAG = MockWebSocketWaiter.class.getSimpleName();

    private final TestSettings testSettings;

    public MockWebSocketWaiter(TestSettings testSettings)
    {
        this.testSettings = testSettings;
    }

//    @Override
//    public <T extends WebSocketMessageBase> T waitForResponseMessage(MessageId messageID, Class<T> clz) throws Exception
//    {
//        WebSocketMessageBase response = waitForResponseMessage(messageID, MessageType.getKeyByClass(clz));
//        return (T) response;
//    }

    @Override
    public <T extends WebSocketMessageBase> T waitForResponseMessage(MessageId messageID, Class<T> clz, int timeout)
    {
        return null;
    }

    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(MessageId messageID, Class<T> clz, int timeout)
    {
        return false;
    }

    @Override
    public <T extends WebSocketMessageBase> List<T> waitForResponseMessage(PhoneInfoModel phoneInfoModel, Class<T> clz, int expectedCount, int timeout)
    {
        return null;
    }

    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(PhoneInfoModel phoneInfoModel, Class<T> clz, int timeout)
    {
        return false;
    }

    @Override
    public <T extends WebSocketMessageBase> List<T> waitForResponseMessage(Class<T> clz, int expectedCount, int timeout)
    {
        return null;
    }

    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(Class<T> clz, int timeout)
    {
        return false;
    }

    @Override
    public <T extends WebSocketMessageBase> List<T> waitForResponseMessage(SmscSessionId smscSessionId, Class<T> clz, int expectedCount, int timeout)
    {
        return null;
    }

    @Override
    public <T extends WebSocketMessageBase> boolean waitForResponseMessageMustTimeout(SmscSessionId smscSessionId, Class<T> clz, int timeout)
    {
        return false;
    }


}
