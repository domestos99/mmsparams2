package cz.uhk.dip.mmsparams.clientlib.pipeline;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.utils.MmsUtils;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.DeliveryIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotificationIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotifyRespIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.mock.MockTestInstance;
import cz.uhk.dip.mmsparams.clientlib.mock.MockTestSettings;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;

public class MmsPipelineTest extends PipelineTestBase
{

    @Test
    public void test1()
    {
        TestSettings ts = MockTestSettings.create();
        ITestInstance test = MockTestInstance.initNewTest(ts, getMessageRegister());

        PhoneInfoModel devices = test.Phones().getByCustomName("phoneA");

        PhoneInfoModel phoneA = devices;
        PhoneInfoModel phoneB = devices;

        test.Phones().lockPhoneForTest(phoneA);
        test.Phones().lockPhoneForTest(phoneB);

        MmsRecipientPhoneProfile recProfiles = MmsRecipientPhoneProfile.createDefault();
        boolean profileSetOk = test.Mms().Profile().setRecipinentPhoneProfile(recProfiles, phoneB);


        MmsSendModel mms = getMmsMessage();

        MmsSendMessageId mmsSendReqId = test.Mms().sendMms(mms, phoneA);

        // Potvrzeni odeslani
        SendConfModel sendConf = test.Mms().waitFor().anyMmsConf(mmsSendReqId);

        // Mms notification was received
        NotificationIndModel notifInd = test.Mms().waitFor().anyNotificationInd(phoneB);

        // Mms was received and downloaded
        RetrieveConfModel retrieveConfs = test.Mms().waitFor().anyRetrieveConf(phoneB);

        // What NotifyResp has recipient sent
        NotifyRespIndModel notifyRespIndModels = test.Mms().waitFor().anyNotifyRespInd(phoneB);

        // Wait for delivery report
        DeliveryIndModel deliveryIndModels = test.Mms().waitFor().anyDeliveryInd(phoneA);

        // Validate MMS send OK
//        List<ValidationItem> vi1 = PduValidationHelper.getSendConfValidationItem(sendConf);
//        // Compare Send MMS and Received MMS
//        List<ValidationItem> vi2 = PduValidationHelper.getMmsPduValidationItem(mms, retrieveConfs);
//        // Validate Delivery Report
//        List<ValidationItem> vi3 = PduValidationHelper.getDeliveryIndValidationItem(deliveryIndModels);
//
//
//        // Validate, that SMS text is the same
//        test.Validate().validatePrintThrow(vi1, vi2, vi3);


        // Should be last command in test
        test.testFinished();
    }

    private static MmsSendModel getMmsMessage()
    {
        MmsSendModel mms = new MmsSendModel();
        String numTo = "+420602301498";

        mms.addTo(numTo);
        mms.setText("Hello world");

        mms = MmsUtils.setDefaultTestProfile(mms);

        mms.setDeliveryReport(true);
        mms.setReadReport(false);

//        MmsAttachmentSendModel img1 = AttachmentHelper.loadImage(AppConfig.SmallImg);
//        mms.addAttachment(img1);

        return mms;
    }
}
