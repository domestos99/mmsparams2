package cz.uhk.dip.mmsparams.clientlib.pipeline;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendConnectionModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendMmsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.mock.MockTestInstance;
import cz.uhk.dip.mmsparams.clientlib.mock.MockTestSettings;

public class MmscPipelineTest extends PipelineTestBase
{
    @Test
    public void test1() throws Exception
    {
        TestSettings ts = MockTestSettings.create();
        ITestInstance test = MockTestInstance.initNewTest(ts, getMessageRegister());


        MmscSendConnectionModel mmscSendConnectionModel = new MmscSendConnectionModel();
        MmscSendMmsModel mmscSendMmsModel = new MmscSendMmsModel();
        MmscSendModel mms = new MmscSendModel(mmscSendConnectionModel, mmscSendMmsModel);

        MM7SubmitResponseModel submitResponse = test.Mmsc().sendMms(mms);


        MM7DeliveryReqModel receive = test.Mmsc().waitFor().anyDeliveryReq();

        MM7DeliveryReportReqModel dr = test.Mmsc().waitFor().anyDeliveryReportReq();


        test.testFinished();
    }
}
