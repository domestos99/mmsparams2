package cz.uhk.dip.mmsparams.clientlib.pipeline;

import org.junit.Before;

import cz.uhk.dip.mmsparams.api.messages.MessageRegister;

public abstract class PipelineTestBase
{
    private MessageRegister messageRegister;

    @Before
    public void testBefore()
    {
        messageRegister = new MessageRegister();
    }

    protected MessageRegister getMessageRegister()
    {
        return this.messageRegister;
    }

//    protected void insertTestPhone(String customName)
//    {
//        RegisterPhoneMessage msg = MessageFactory.create(null, RegisterPhoneMessage.class);
//
//        PhoneInfoModel phoneInfo = new PhoneInfoModel();
//        phoneInfo.setPhoneCustomName(customName);
//
//
//
//
//    }
}
