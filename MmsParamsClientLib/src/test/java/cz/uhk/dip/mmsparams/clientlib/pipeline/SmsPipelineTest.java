package cz.uhk.dip.mmsparams.clientlib.pipeline;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.mock.MockTestInstance;
import cz.uhk.dip.mmsparams.clientlib.mock.MockTestSettings;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemEquals;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemNotNull;

public class SmsPipelineTest extends PipelineTestBase
{
    @Test
    public void test1() throws Exception
    {
        TestSettings ts = MockTestSettings.create();
        ITestInstance test = MockTestInstance.initNewTest(ts, getMessageRegister());

        PhoneInfoModel phoneA = test.Phones().getByCustomName("phoneA");

        PhoneInfoModel phoneB = phoneA;

        test.Phones().lockPhoneForTest(phoneA);

        SmsSendModel sms = new SmsSendModel();
        sms.setTo("+420123456789");
        sms.setDeliveryReport(true);
        String smsText = "xxx";

        sms.setText(smsText);

        SmsSendMessageId smsSendID = test.Sms().sendSms(sms, phoneA);

        SmsSendResponseModel sendOk = test.Sms().waitFor().smsSentSuccessfully(smsSendID);

        SmsReceiveModel smsReceiveModel = test.Sms().waitFor().anySmsReceived(phoneB);

        SmsDeliveryReport dr = test.Sms().waitFor().anyDeliveryReport(smsSendID);

        // Validations
        IValidationItem vt1 = new ValidationItemNotNull(sendOk, "Send SMS OK");
        IValidationItem vt2 = new ValidationItemEquals<String>(smsText, smsReceiveModel.getMessage(), "Sms text");
        IValidationItem vt3 = new ValidationItemNotNull(dr, "Delivery report");

        test.Validation().validatePrintThrow(vt1, vt2, vt3);

        if (!smsText.equals(smsReceiveModel.getMessage()))
            throw new Exception("SMS text not same");

        test.testFinished();
    }


}
