package cz.uhk.dip.mmsparams.clientlib.pipeline;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.SmscConnectType;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscAddressModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.mock.MockTestInstance;
import cz.uhk.dip.mmsparams.clientlib.mock.MockTestSettings;

public class SmscPipelineTest extends PipelineTestBase
{

    @Test
    public void test1() throws Exception
    {
        TestSettings ts = MockTestSettings.create();
        ITestInstance test = MockTestInstance.initNewTest(ts, getMessageRegister());


        SmscConnectModel smscConnectModel = new SmscConnectModel();

        smscConnectModel.setHost("127.0.0.1");
        smscConnectModel.setPort(2775);
        smscConnectModel.setSystemId("smscclient1");
        smscConnectModel.setPassword("password");
        smscConnectModel.setConnType(SmscConnectType.TRANSCEIVER);


        SmscConnectResponseModel status = test.Smsc().connectToSMSC(smscConnectModel);
        SmscSessionId sessionId = status.getSessionId();

        //  SmscDeliverSmModel receivedSms = test.Smsc().waitForAnySmsReceive();

        SmscSendModel smscSendModel = new SmscSendModel();

        smscSendModel.setShortMessage("hello world");
        smscSendModel.setSenderAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, "123"));
        smscSendModel.setRecipientAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, "456"));
        smscSendModel.setDeliveryReport(true);

        SmscSendSmsResponseModel msgId = test.Smsc().sendSms(sessionId, smscSendModel);


        SmscDeliveryReportModel deliveryReportModel = test.Smsc().waitFor().anyDeliveryReport(sessionId);

        test.Smsc().disconnect(sessionId);


        test.testFinished();
    }
}
