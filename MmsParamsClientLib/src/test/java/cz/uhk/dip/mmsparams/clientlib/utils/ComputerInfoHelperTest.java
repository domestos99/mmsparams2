package cz.uhk.dip.mmsparams.clientlib.utils;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;

import static org.junit.Assert.assertFalse;

public class ComputerInfoHelperTest extends UnitTestBase
{

    @Test
    public void getComputerNameTest()
    {
        String name = ComputerInfoHelper.getComputerName();
        assertFalse(StringUtil.isEmptyOrNull(name));
    }

    @Test
    public void getCurrentUserNameTest()
    {
        String name = ComputerInfoHelper.getCurrentUserName();
        assertFalse(StringUtil.isEmptyOrNull(name));
    }
}
