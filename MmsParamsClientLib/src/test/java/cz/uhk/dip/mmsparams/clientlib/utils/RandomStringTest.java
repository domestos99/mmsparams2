package cz.uhk.dip.mmsparams.clientlib.utils;

import org.junit.Test;

public class RandomStringTest
{
    @Test(expected = IllegalArgumentException.class)
    public void constructorTest()
    {
        new RandomString(0, true);
    }

}
