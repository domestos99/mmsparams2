package cz.uhk.dip.mmsparams.clientlib.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SampleTextUtilsTest
{
    @Test
    public void generateText1Test()
    {
        String text = SampleTextUtils.generateText(160);
        assertEquals(160, text.length());
    }

    @Test
    public void generateText2Test()
    {
        String text = SampleTextUtils.generateText(70);
        assertEquals(70, text.length());
    }

    @Test
    public void generateText3Test()
    {
        String text = SampleTextUtils.generateText("UT prefix", 160);
        assertEquals(160, text.length());
        assertTrue(text.startsWith("UT prefix"));
    }

    @Test
    public void generateText4Test()
    {
        String text = SampleTextUtils.generateText("UT prefix", 70);
        assertEquals(70, text.length());
        assertTrue(text.startsWith("UT prefix"));
    }

    @Test
    public void generateText5Test()
    {
        String text = SampleTextUtils.generateText16BIT("UT prefix", 70);
        assertEquals(70, text.length());
        assertTrue(text.startsWith("UT prefix"));
    }

    @Test
    public void generateText6Test()
    {
        String text = SampleTextUtils.generateText16BIT(70);
        assertEquals(70, text.length());
    }

    @Test
    public void generateText7Test()
    {
        String text = SampleTextUtils.generateText("", 70);
        assertEquals(70, text.length());
    }

    @Test
    public void generateText8Test()
    {
        String text = SampleTextUtils.generateText(null, 70);
        assertEquals(70, text.length());
    }

    @Test
    public void generateNoSpaceTest()
    {
        String text = SampleTextUtils.generateText("SmscTest1", 160);
        assertFalse(text.endsWith(" "));
        assertEquals(160, text.length());
    }

    @Test
    public void generateNoSpace16bitTest()
    {
        String text = SampleTextUtils.generateText16BIT("SmscTest1", 160);
        assertFalse(text.endsWith(" "));
        assertEquals(160, text.length());
    }

    @Test
    public void generateNoSpaceAllPrefixTest()
    {
        String a = "";

        for (int i = 1; i <= 160; i++)
        {
            String text = SampleTextUtils.generateText(a, 160);
            assertFalse(text.endsWith(" "));
            assertEquals(160, text.length());
            a += "a";
        }
    }

    @Test
    public void generateNoSpaceAllPrefix16BitTest()
    {
        String a = "";

        for (int i = 1; i <= 160; i++)
        {
            String text = SampleTextUtils.generateText16BIT(a, 160);
            assertFalse(text.endsWith(" "));
            assertEquals(160, text.length());
            a += "a";
        }
    }


}