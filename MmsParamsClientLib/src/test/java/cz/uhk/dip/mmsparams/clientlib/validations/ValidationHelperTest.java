package cz.uhk.dip.mmsparams.clientlib.validations;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.exceptions.TestInvalidException;
import cz.uhk.dip.mmsparams.api.exceptions.ValidationException;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ValidationHelperTest extends UnitTestBase
{

    @Before
    public void setUp() throws Exception
    {
    }

    @Test
    public void checkListCountTest()
    {
        List<String> list = new ArrayList<>();
        list.add(GenericConstants.TEST);
        assertTrue(ValidationHelper.checkListCount(1, list));
    }

    @Test(expected = ValidationException.class)
    public void checkListCount_not_equal_Test()
    {
        List<String> list = new ArrayList<>();
        list.add(GenericConstants.TEST);
        ValidationHelper.checkListCount(2, list);
    }

    @Test(expected = TestInvalidException.class)
    public void checkListCount_null_Test()
    {
        ValidationHelper.checkListCount(0, null);
    }

    @Test
    public void assertEqualTest()
    {
        assertTrue(ValidationHelper.assertEqual(GenericConstants.TEST, GenericConstants.TEST));
    }

    @Test
    public void assertEqual_with_parameter_name_Test()
    {
        assertTrue(ValidationHelper.assertEqual(GenericConstants.TEST, GenericConstants.TEST, "Param name"));
    }

    @Test(expected = ValidationException.class)
    public void assertEqual_not_equals_Test()
    {
        ValidationHelper.assertEqual(GenericConstants.TEST, GenericConstants.TEST + 1);
    }

    @Test
    public void assertEqual_object_Test()
    {
        Object obj = new Object();
        assertTrue(ValidationHelper.assertEqual(obj, obj));
    }

    @Test
    public void assertEqual_object_with_parameter_name_Test()
    {
        Object obj = new Object();
        ValidationHelper.assertEqual(obj, obj, "Param name");
    }

    @Test(expected = ValidationException.class)
    public void assertEqual_object_not_equals_Test()
    {
        Object o1 = new Object();
        Object o2 = new Object();
        assertTrue(ValidationHelper.assertEqual(o1, o2));
    }

    @Test
    public void validateListTest()
    {
        List<IValidationItem> list = new ArrayList<>();
        list.add(new ValidationItemTrue(true, "Shout be true 1"));
        list.add(new ValidationItemTrue(true, "Shout be true 2"));
        list.add(new ValidationItemTrue(true, "Shout be true 3"));
        List<Tuple<Boolean, ValidationResult>> result = ValidationHelper.validateList(list);

        assertNotNull(result);
        assertEquals(list.size(), result.size());
        assertEquals(true, result.get(0).getX());
        assertEquals(true, result.get(1).getX());
        assertEquals(true, result.get(2).getX());
    }

    @Test
    public void validateList_empty_Test()
    {
        List<IValidationItem> list = new ArrayList<>();
        List<Tuple<Boolean, ValidationResult>> result = ValidationHelper.validateList(list);
        assertNotNull(result);
        assertEquals(list.size(), result.size());
    }

    @Test
    public void validateList_null_Test()
    {
        List<Tuple<Boolean, ValidationResult>> result = ValidationHelper.validateList(null);
        assertNotNull(result);
        assertEquals(0, result.size());
    }

    @Test
    public void validateTest()
    {
        IValidationItem vi1 = new ValidationItemTrue(true, "Shout be true 1");
        IValidationItem vi2 = new ValidationItemTrue(true, "Shout be true 2");
        IValidationItem vi3 = new ValidationItemTrue(true, "Shout be true 3");
        List<Tuple<Boolean, ValidationResult>> result = ValidationHelper.validate(vi1, vi2, vi3);
        assertNotNull(result);
        assertEquals(3, result.size());
        assertEquals(true, result.get(0).getX());
        assertEquals(true, result.get(1).getX());
        assertEquals(true, result.get(2).getX());
    }

    @Test
    public void validate_null_Test()
    {
        List<Tuple<Boolean, ValidationResult>> result = ValidationHelper.validate(null, null);
        assertNotNull(result);
        assertEquals(0, result.size());
    }
}
