package cz.uhk.dip.mmsparams.clientlib.validations;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidationItemByteArrayNotEqualsTest extends UnitTestBase
{
    @Test
    public void validateArrayTest()
    {
        byte[] a = new byte[]{1, 2, 3};
        byte[] b = new byte[]{1, 2, 3};

        IValidationItem vi = new ValidationItemByteArrayNotEquals(a, b, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertFalse(result.getX());
    }

    @Test
    public void validateArray2Test()
    {
        byte[] a = new byte[]{1, 2, 3};
        byte[] b = new byte[]{1, 2, 3, 4};

        IValidationItem vi = new ValidationItemByteArrayNotEquals(a, b, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
    }

    @Test
    public void validateArray3Test()
    {
        byte[] a = new byte[]{1, 2, 3};
        byte[] b = new byte[]{5, 6, 7};

        IValidationItem vi = new ValidationItemByteArrayNotEquals(a, b, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
    }
}
