package cz.uhk.dip.mmsparams.clientlib.validations;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ValidationItemFalseTest
{

    @Test
    public void validateTrueTest()
    {
        boolean value = true;
        IValidationItem vi = new ValidationItemFalse(value, "test true");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertFalse(result.getX());
        assertNotNull(result.getY());
    }

    @Test
    public void validateFalseTest()
    {
        boolean value = false;
        IValidationItem vi = new ValidationItemFalse(value, "test false");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
        assertNotNull(result.getY());
    }

}
