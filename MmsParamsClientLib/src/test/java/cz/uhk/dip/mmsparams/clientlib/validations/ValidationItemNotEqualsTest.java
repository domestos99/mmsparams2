package cz.uhk.dip.mmsparams.clientlib.validations;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.mock.SerializableClass;

import static org.junit.Assert.*;

public class ValidationItemNotEqualsTest
{
    @Test
    public void validateBoolTest()
    {
        boolean a = true;
        boolean b = true;
        IValidationItem vi = new ValidationItemNotEquals(a, b, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertFalse(result.getX());
    }

    @Test
    public void validateBool2Test()
    {
        boolean a = true;
        boolean b = false;
        IValidationItem vi = new ValidationItemNotEquals(a, b, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
    }


    @Test
    public void validateIntTest()
    {
        int a = 1;
        int b = 1;
        IValidationItem vi = new ValidationItemNotEquals(a, b, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertFalse(result.getX());
    }

    @Test
    public void validateInt2Test()
    {
        int a = 1;
        int b = 2;
        IValidationItem vi = new ValidationItemNotEquals(a, b, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
    }

    @Test
    public void validateIntegerTest()
    {
        Integer a = 1;
        Integer b = 1;
        IValidationItem vi = new ValidationItemNotEquals(a, b, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertFalse(result.getX());
    }

    @Test
    public void validateInteger2Test()
    {
        Integer a = 1;
        Integer b = 2;
        IValidationItem vi = new ValidationItemNotEquals(a, b, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
    }

    @Test
    public void validateStringTest()
    {
        String a = "A";
        String b = "A";
        IValidationItem vi = new ValidationItemNotEquals(a, b, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertFalse(result.getX());
    }

    @Test
    public void validateString2Test()
    {
        String a = "A";
        String b = "B";
        IValidationItem vi = new ValidationItemNotEquals(a, b, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
    }


    @Test
    public void twoTypesMethodCallTest()
    {
        SerializableClass o = new SerializableClass();
        int a = 1;

        IValidationItem vi = new ValidationItemNotEquals(a, o, "test true");
        Tuple<Boolean, ValidationResult> result = vi.validate();
        assertTrue(result.getX());

    }
    

}
