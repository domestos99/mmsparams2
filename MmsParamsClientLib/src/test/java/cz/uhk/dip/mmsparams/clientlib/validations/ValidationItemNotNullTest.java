package cz.uhk.dip.mmsparams.clientlib.validations;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.mock.SerializableClass;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidationItemNotNullTest
{
    @Test
    public void validateBoolTest()
    {
        boolean value = true;
        IValidationItem vi = new ValidationItemNotNull<Boolean>(value, "test");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
    }

    @Test
    public void validateStringTest()
    {
        String value = "hello";
        IValidationItem vi = new ValidationItemNotNull<String>(value, "test");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
    }

    @Test
    public void validateStringNullTest()
    {
        String value = null;
        IValidationItem vi = new ValidationItemNotNull<String>(value, "test");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertFalse(result.getX());
    }


    @Test
    public void validateObjectTest()
    {
        SerializableClass value = new SerializableClass();
        IValidationItem vi = new ValidationItemNotNull<SerializableClass>(value, "test");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
    }

    @Test
    public void validateObjectNullTest()
    {
        SerializableClass value = null;
        IValidationItem vi = new ValidationItemNotNull<SerializableClass>(value, "test");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertFalse(result.getX());
    }

}
