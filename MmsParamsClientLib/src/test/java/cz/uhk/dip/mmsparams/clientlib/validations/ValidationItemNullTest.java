package cz.uhk.dip.mmsparams.clientlib.validations;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.mock.SerializableClass;

import static org.junit.Assert.*;

public class ValidationItemNullTest extends UnitTestBase
{
    @Test
    public void validateBoolTest()
    {
        boolean value = true;
        IValidationItem vi = new ValidationItemNull(value, "test");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertFalse(result.getX());
    }

    @Test
    public void validateStringTest()
    {
        String value = "hello";
        IValidationItem vi = new ValidationItemNull(value, "test");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertFalse(result.getX());
    }

    @Test
    public void validateStringNullTest()
    {
        String value = null;
        IValidationItem vi = new ValidationItemNull(value, "test");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
    }


    @Test
    public void validateObjectTest()
    {
        SerializableClass value = new SerializableClass();
        IValidationItem vi = new ValidationItemNull(value, "test");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertFalse(result.getX());
    }

    @Test
    public void validateObjectNullTest()
    {
        SerializableClass value = null;
        IValidationItem vi = new ValidationItemNull(value, "test");

        Tuple<Boolean, ValidationResult> result = vi.validate();

        assertTrue(result.getX());
    }

}
