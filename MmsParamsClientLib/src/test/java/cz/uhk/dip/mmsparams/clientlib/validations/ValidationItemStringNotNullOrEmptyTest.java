package cz.uhk.dip.mmsparams.clientlib.validations;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidationItemStringNotNullOrEmptyTest extends UnitTestBase
{
    @Test
    public void validateStringTest()
    {
        String value = "hello";
        IValidationItem vi = new ValidationItemStringNotNullOrEmpty(value, "test");
        Tuple<Boolean, ValidationResult> result = vi.validate();
        assertTrue(result.getX());
    }

    @Test
    public void validateEmptyTest()
    {
        IValidationItem vi = new ValidationItemStringNotNullOrEmpty("", "test");
        Tuple<Boolean, ValidationResult> result = vi.validate();
        assertFalse(result.getX());
    }

    @Test
    public void validateNullTest()
    {
        IValidationItem vi = new ValidationItemStringNotNullOrEmpty(null, "test");
        Tuple<Boolean, ValidationResult> result = vi.validate();
        assertFalse(result.getX());
    }

}
