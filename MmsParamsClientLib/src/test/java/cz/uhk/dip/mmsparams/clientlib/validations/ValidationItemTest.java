package cz.uhk.dip.mmsparams.clientlib.validations;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;

import static org.junit.Assert.assertEquals;

public class ValidationItemTest
{
    private ValidationItemBase validationItem;
    private final String name = "validation";

    @Before
    public void setUp() throws Exception
    {
        validationItem = new ValidationItemBase(name)
        {
            @Override
            public Tuple<Boolean, ValidationResult> validate()
            {
                return new Tuple<>(true, new ValidationResult());
            }
        };
    }

    @Test
    public void getValidationName()
    {
        assertEquals(name, validationItem.getValidationName());
    }

    @Test
    public void setValidationName()
    {
        validationItem.setValidationName(name + 1);
        assertEquals(name + 1, validationItem.getValidationName());
    }

    @Test
    public void validationNameEmptyTest()
    {
        validationItem = new ValidationItemBase()
        {
            @Override
            public Tuple<Boolean, ValidationResult> validate()
            {
                return new Tuple<>(true, new ValidationResult());
            }
        };
        assertEquals(null, validationItem.getValidationName());
    }
}
