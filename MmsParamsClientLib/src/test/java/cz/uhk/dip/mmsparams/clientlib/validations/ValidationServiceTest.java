package cz.uhk.dip.mmsparams.clientlib.validations;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.exceptions.TestInvalidException;
import cz.uhk.dip.mmsparams.api.exceptions.ValidationException;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.communication.websocket.IWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.mock.DummyWebSocketSender;
import cz.uhk.dip.mmsparams.clientlib.mock.DummyWebSocketWaiter;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidationServiceTest extends UnitTestBase
{
    private IValidationService iValidationService;
    IWebSocketSender webSocketSender;
    IWebSocketWaiter webSocketReceiver;

    @Before
    public void setUp() throws Exception
    {
        this.webSocketSender = new DummyWebSocketSender();
        this.webSocketReceiver = new DummyWebSocketWaiter();
        iValidationService = new ValidationService(webSocketSender, webSocketReceiver);
    }

    @Test
    public void validatePrintThrowTest()
    {
        IValidationItem vi = new ValidationItemTrue(true, "Shout be true");
        iValidationService.validatePrintThrow(vi);
    }

    @Test(expected = ValidationException.class)
    public void validatePrintThrow_error_Test()
    {
        IValidationItem vi = new ValidationItemTrue(false, "Shout be true");
        iValidationService.validatePrintThrow(vi);
    }

    @Test
    public void validatePrintThrowListTest()
    {
        List<IValidationItem> list = new ArrayList<>();
        list.add(new ValidationItemTrue(true, "Shout be true 1"));
        list.add(new ValidationItemTrue(true, "Shout be true 2"));
        list.add(new ValidationItemTrue(true, "Shout be true 3"));
        iValidationService.validatePrintThrow(list);
    }

    @Test(expected = ValidationException.class)
    public void validatePrintThrowList_error_Test()
    {
        List<IValidationItem> list = new ArrayList<>();
        list.add(new ValidationItemTrue(true, "Shout be true 1"));
        list.add(new ValidationItemTrue(true, "Shout be true 2"));
        list.add(new ValidationItemTrue(false, "Shout be true 3"));
        iValidationService.validatePrintThrow(list);
    }

    @Test
    public void validatePrintThrowList_null_Test()
    {
        iValidationService.validatePrintThrow();
    }

    @Test(expected = TestInvalidException.class)
    public void validatePrintThrowList_null_list_Test()
    {
        iValidationService.validatePrintThrow((List<IValidationItem>) null);
    }

    @Test(expected = TestInvalidException.class)
    public void validatePrintThrowList_null_validation_builder_Test()
    {
        iValidationService.validatePrintThrow((ValidationBuilder) null);
    }

    @Test
    public void validatePrintThrowList_validation_builder_Test()
    {
        ValidationBuilder vb = new ValidationBuilder();
        vb.withTrue(true, "Should be true");
        iValidationService.validatePrintThrow(vb);
    }

    @Test(expected = ValidationException.class)
    public void validatePrintThrowList_validation_builder_error_Test()
    {
        ValidationBuilder vb = new ValidationBuilder();
        vb.withTrue(false, "Should be true");
        iValidationService.validatePrintThrow(vb);
    }

    @Test
    public void validateOnly_vb_Test()
    {
        ValidationBuilder vb = ValidationBuilder.create()
                .withTrue(true, "Shout be true");

        List<Tuple<Boolean, ValidationResult>> result = iValidationService.validateOnly(vb);
        assertEquals(1, result.size());
        assertTrue(result.get(0).getX());
    }

    @Test
    public void validateOnly_vb_failed_Test()
    {
        ValidationBuilder vb = ValidationBuilder.create()
                .withTrue(false, "Shout be true");

        List<Tuple<Boolean, ValidationResult>> result = iValidationService.validateOnly(vb);
        assertEquals(1, result.size());
        assertFalse(result.get(0).getX());
    }

    @Test
    public void validateOnly_params_Test()
    {
        IValidationItem i1 = new ValidationItemTrue(true, "Shout be true 1");
        IValidationItem i2 = new ValidationItemTrue(false, "Shout be true 2");

        List<Tuple<Boolean, ValidationResult>> result = iValidationService.validateOnly(i1, i2);
        assertEquals(2, result.size());
        assertTrue(result.get(0).getX());
        assertFalse(result.get(1).getX());
    }

    @Test
    public void validateOnly_params_null_Test()
    {
        List<Tuple<Boolean, ValidationResult>> result = iValidationService.validateOnly();
        assertTrue(result.isEmpty());
    }

    @Test(expected = TestInvalidException.class)
    public void validateOnly_vb_null_Test()
    {
        iValidationService.validateOnly((ValidationBuilder) null);
    }

    @Test(expected = TestInvalidException.class)
    public void validateOnly_list_null_Test()
    {
        iValidationService.validateOnly((List<IValidationItem>) null);
    }
}
