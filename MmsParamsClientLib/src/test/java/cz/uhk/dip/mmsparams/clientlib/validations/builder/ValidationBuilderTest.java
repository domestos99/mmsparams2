package cz.uhk.dip.mmsparams.clientlib.validations.builder;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.UnitTestBase;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemByteArray;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemEquals;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemFalse;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemNotEquals;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemNotNull;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemNull;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemTrue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ValidationBuilderTest extends UnitTestBase
{

    @Test
    public void validationBuilderEmptyTest()
    {
        ValidationBuilder vb = new ValidationBuilder();
        List<IValidationItem> validations = vb.build();

        assertNotNull(validations);
        assertEquals(0, validations.size());
    }

    @Test
    public void oneItemEqualsTest()
    {
        ValidationBuilder vb = new ValidationBuilder();

        vb.withEquals("abc", "abc", "Abc equals");
        vb.withEquals("cde", "cde", "cde equals");

        List<IValidationItem> validations = vb.build();

        assertNotNull(validations);
        assertEquals(2, validations.size());
    }

    @Test
    public void createTest()
    {
        ValidationBuilder vb = ValidationBuilder.create();
        assertNotNull(vb);
        assertEquals(0, vb.build().size());
    }

    @Test
    public void withNullTest()
    {
        ValidationBuilder vb = ValidationBuilder.create();
        vb.withNull(null, "Test null");

        List<IValidationItem> validations = vb.build();
        assertEquals(1, validations.size());
        assertEquals(ValidationItemNull.class, validations.get(0).getClass());
    }

    @Test
    public void withNotNullTest()
    {
        ValidationBuilder vb = ValidationBuilder.create();
        vb.withNotNull(123, "Test not null");

        List<IValidationItem> validations = vb.build();
        assertEquals(1, validations.size());
        assertEquals(ValidationItemNotNull.class, validations.get(0).getClass());
    }


    @Test
    public void build()
    {
        ValidationBuilder vb = ValidationBuilder.create();
        List<IValidationItem> vbBuild = vb.build();
        assertNotNull(vbBuild);
        assertTrue(vbBuild.isEmpty());
    }

    @Test
    public void withEqualsTest()
    {
        ValidationBuilder vb = ValidationBuilder.create();
        vb.withEquals(123, 123, "Test");

        List<IValidationItem> validations = vb.build();
        assertEquals(1, validations.size());
        assertEquals(ValidationItemEquals.class, validations.get(0).getClass());
    }

    @Test
    public void withTrueTest()
    {
        ValidationBuilder vb = ValidationBuilder.create();
        vb.withTrue(true, "Test");

        List<IValidationItem> validations = vb.build();
        assertEquals(1, validations.size());
        assertEquals(ValidationItemTrue.class, validations.get(0).getClass());
    }

    @Test
    public void withFalseTest()
    {
        ValidationBuilder vb = ValidationBuilder.create();
        vb.withFalse(false, "Test");

        List<IValidationItem> validations = vb.build();
        assertEquals(1, validations.size());
        assertEquals(ValidationItemFalse.class, validations.get(0).getClass());
    }

    @Test
    public void withByteArrayEqualsTest()
    {
        ValidationBuilder vb = ValidationBuilder.create();
        byte[] bytes = new byte[]{1, 5, 8, 9, 2, 1, 20, 51, 23, 5, 2, 5};
        vb.withByteArrayEquals(bytes, bytes, "Test");

        List<IValidationItem> validations = vb.build();
        assertEquals(1, validations.size());
        assertEquals(ValidationItemByteArray.class, validations.get(0).getClass());
    }

    @Test
    public void withNotEqualsTest()
    {
        ValidationBuilder vb = ValidationBuilder.create();
        String a = "a";
        String b = "b";

        vb.withNotEquals(a, b, "A is not a b");
        List<IValidationItem> validations = vb.build();
        assertEquals(1, validations.size());
        assertEquals(ValidationItemNotEquals.class, validations.get(0).getClass());
    }

    @Test
    public void withCustomValidationTest()
    {
        ValidationBuilder vb = ValidationBuilder.create();
        vb.withCustomValidation(new TestValidationItem());

        List<IValidationItem> validations = vb.build();
        assertEquals(1, validations.size());
        assertEquals(TestValidationItem.class, validations.get(0).getClass());
    }

    @Test
    public void withCustomValidationsTest()
    {
        ValidationBuilder vb = ValidationBuilder.create();

        List<IValidationItem> val = new ArrayList<>();
        val.add(new TestValidationItem());
        val.add(new TestValidationItem());
        vb.withCustomValidations(val);

        List<IValidationItem> validations = vb.build();
        assertEquals(2, validations.size());
        assertEquals(TestValidationItem.class, validations.get(0).getClass());
        assertEquals(TestValidationItem.class, validations.get(1).getClass());
    }

    class TestValidationItem implements IValidationItem
    {

        @Override
        public Tuple<Boolean, ValidationResult> validate()
        {
            return new Tuple<>(true, new ValidationResult());
        }
    }
}
