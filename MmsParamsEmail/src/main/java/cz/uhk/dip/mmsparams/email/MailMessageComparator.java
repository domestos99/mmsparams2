package cz.uhk.dip.mmsparams.email;

import javax.mail.Message;

public class MailMessageComparator implements java.util.Comparator<Message>
{
    @Override
    public int compare(Message o1, Message o2)
    {
        try
        {
            if (o1 == null || o2 == null)
            {
                return -1;
            }
            if (o1.getReceivedDate() == null || o2.getReceivedDate() == null)
            {
                return -1;
            }
            return (o1.getReceivedDate().compareTo(o2.getReceivedDate()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return -1;
        }
    }

}
