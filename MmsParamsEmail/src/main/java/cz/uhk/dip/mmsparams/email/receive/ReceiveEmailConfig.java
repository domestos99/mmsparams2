package cz.uhk.dip.mmsparams.email.receive;

import java.io.Serializable;

public class ReceiveEmailConfig implements Serializable
{
    private String protocol;
    private String host;
    private String port;
    private String userName;
    private String password;
    private String folderName;
    private boolean useSsl;


    public String getProtocol()
    {
        return protocol;
    }

    public void setProtocol(String protocol)
    {
        this.protocol = protocol;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getFolderName()
    {
        return folderName;
    }

    public void setFolderName(String folderName)
    {
        this.folderName = folderName;
    }

    public boolean getUseSsl()
    {
        return useSsl;
    }

    public void setUseSsl(boolean useSsl)
    {
        this.useSsl = useSsl;
    }
}
