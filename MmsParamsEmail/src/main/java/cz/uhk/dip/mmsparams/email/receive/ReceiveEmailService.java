package cz.uhk.dip.mmsparams.email.receive;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;

import cz.uhk.dip.mmsparams.api.websocket.model.email.ReceiveEmailMessageModel;
import cz.uhk.dip.mmsparams.api.websocket.model.email.ReceiveEmailMessagePartModel;
import cz.uhk.dip.mmsparams.email.MailMessageComparator;
import cz.uhk.dip.mmsparams.email.utils.StreamUtils;

public class ReceiveEmailService
{
    private ReceiveEmailService()
    {
    }

    public static ReceiveEmailMessageModel receiveEmail(ReceiveEmailConfig receiveEmailConfig) throws MessagingException, IOException
    {
        try
        {
            return receive(receiveEmailConfig);
        }
        catch (MessagingException e)
        {
            e.printStackTrace();
            throw e;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw e;
        }
    }


    private static ReceiveEmailMessageModel receive(ReceiveEmailConfig config) throws MessagingException, IOException
    {
        Folder folder = null;
        Store store = null;
        try
        {

            Properties properties = getServerProperties(config);
            Session session = Session.getDefaultInstance(properties);

            // connects to the message store
            store = session.getStore(config.getProtocol());
            store.connect(config.getUserName(), config.getPassword());


            // opens the inbox folder
            folder = store.getFolder(config.getFolderName());
            folder.open(Folder.READ_ONLY);

            // fetches new messages from server
            Message[] messages = folder.getMessages();

            List<Message> targetMessages = getTargetMessages(messages);

            List<Message> found = targetMessages.stream().sorted(new MailMessageComparator()).collect(Collectors.toList());

            if (!found.isEmpty())
            {
                Message targetMessage = found.get(found.size() - 1);

                return convertMessage(targetMessage);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if (folder != null)
            {
                folder.close(true);
            }
            if (store != null)
            {
                store.close();
            }
        }
        return null;
    }

    private static Properties getServerProperties(ReceiveEmailConfig config)
    {
        Properties properties = new Properties();

        // server setting
        properties.put(String.format("mail.%s.host", config.getProtocol()), config.getHost());
        properties.put(String.format("mail.%s.port", config.getProtocol()), config.getPort());

        // SSL setting
        if (config.getUseSsl())
        {
            properties.setProperty(String.format("mail.%s.socketFactory.class", config.getProtocol()), "javax.net.ssl.SSLSocketFactory");
            properties.setProperty(String.format("mail.%s.socketFactory.fallback", config.getProtocol()), "false");
            properties.setProperty(String.format("mail.%s.socketFactory.port", config.getProtocol()), config.getPort());
        }

        return properties;
    }


    private static List<Message> getTargetMessages(Message[] messages)
    {
        List<Message> result = new ArrayList<>();

        result.add(messages[messages.length - 1]);
        return result;

//        for (Message msg : messages)
//        {
//            if (msg == null || msg.getFrom() == null)
//                continue;
//
//            for (Address a : msg.getFrom())
//            {
//                if (a.toString().contains("420602334928@mms.o2.cz"))
//                {
//                    targetMessages.add(msg);
//                }
//            }
//        }
    }

    private static ReceiveEmailMessageModel convertMessage(Message targetMessage) throws MessagingException, IOException
    {
        ReceiveEmailMessageModel result = new ReceiveEmailMessageModel();

        Address[] from = targetMessage.getFrom();

        result.setFrom(from[0].toString().trim());

        Address[] allRecipients = targetMessage.getAllRecipients();

        for (Address a : allRecipients)
        {
            result.addTo(a.toString().trim());
        }

        result.setReceivedDate(targetMessage.getReceivedDate());
        result.setSubject(targetMessage.getSubject());
        result.setSentDate(targetMessage.getSentDate());


        // suppose 'message' is an object of type Message
        String contentType = targetMessage.getContentType();
        result.setContentType(contentType);

        if (contentType.contains("multipart"))
        {
            Multipart multiPart = (Multipart) targetMessage.getContent();
            for (int i = 0; i < multiPart.getCount(); i++)
            {
                MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(i);

                byte[] data;
                try (InputStream is = part.getInputStream())
                {
                    data = StreamUtils.reatInputStream(is);
                }

                ReceiveEmailMessagePartModel partModel = new ReceiveEmailMessagePartModel();

                partModel.setFileName(part.getFileName());
                partModel.setEncoding(part.getEncoding());
                partModel.setContentType(part.getContentType());
                partModel.setDescription(part.getDescription());
                partModel.setDisposition(part.getDisposition());
                partModel.setSize(part.getSize());
                partModel.setData(data);

                result.addPart(partModel);
            }
        }
        return result;
    }

}
