package cz.uhk.dip.mmsparams.email.send;

import java.io.Serializable;

public class SendEmailConfig implements Serializable
{
    private String smtpHost;
    private String smtpPort;
    private String smptpUsername;
    private String smptpPassword;
    private String from;
    private String to;

    public String getSmtpHost()
    {
        return smtpHost;
    }

    public void setSmtpHost(String smtpHost)
    {
        this.smtpHost = smtpHost;
    }

    public String getSmtpPort()
    {
        return smtpPort;
    }

    public void setSmtpPort(String smtpPort)
    {
        this.smtpPort = smtpPort;
    }

    public String getSmptpUsername()
    {
        return smptpUsername;
    }

    public void setSmptpUsername(String smptpUsername)
    {
        this.smptpUsername = smptpUsername;
    }

    public String getSmptpPassword()
    {
        return smptpPassword;
    }

    public void setSmptpPassword(String smptpPassword)
    {
        this.smptpPassword = smptpPassword;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public String getTo()
    {
        return to;
    }

    public void setTo(String to)
    {
        this.to = to;
    }
}
