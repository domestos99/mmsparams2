package cz.uhk.dip.mmsparams.email.send;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmailService
{
    private SendEmailService()
    {
    }

    public static void sendEmail(final SendEmailConfig config, final String subject, final String text) throws MessagingException
    {
        try
        {
            Properties properties = new Properties();
            // fill all the information like host name etc.
            properties.setProperty("mail.smtp.host", config.getSmtpHost());
            properties.setProperty("mail.smtp.port", config.getSmtpPort());
            properties.put("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(properties, new Authenticator()
            {
                @Override
                protected PasswordAuthentication getPasswordAuthentication()
                {
                    return new PasswordAuthentication(config.getSmptpUsername(), config.getSmptpPassword());
                }
            });

            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(config.getFrom()));


            message.addRecipient(Message.RecipientType.TO, new InternetAddress(config.getTo()));

            message.setSubject(subject);

            message.setText(text);

            Transport.send(message);

        }
        catch (MessagingException e)
        {
            e.printStackTrace();
            throw e;
        }

    }
}
