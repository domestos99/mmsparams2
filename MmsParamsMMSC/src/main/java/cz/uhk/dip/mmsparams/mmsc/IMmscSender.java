package cz.uhk.dip.mmsparams.mmsc;

import net.instantcom.mm7.MM7Error;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;

public interface IMmscSender
{
    MM7SubmitResponseModel send(final MmscSendModel mmscSendModel) throws MM7Error, IOException;
}
