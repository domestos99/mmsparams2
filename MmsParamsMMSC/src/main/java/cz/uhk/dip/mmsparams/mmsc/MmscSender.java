package cz.uhk.dip.mmsparams.mmsc;

import net.instantcom.mm7.BasicMMSC;
import net.instantcom.mm7.MM7Context;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MM7Message;
import net.instantcom.mm7.SubmitReq;
import net.instantcom.mm7.SubmitRsp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendConnectionModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;
import cz.uhk.dip.mmsparams.mmsc.logging.MMSCLoggerFacade;
import cz.uhk.dip.mmsparams.mmsc.utils.MM7Converter;
import cz.uhk.dip.mmsparams.mmsc.utils.MM7ConverterOut;

public class MmscSender implements IMmscSender
{
    private static final String TAG = MmscSender.class.getSimpleName();

    @Override
    public MM7SubmitResponseModel send(MmscSendModel model) throws MM7Error, IOException
    {
        MMSCLoggerFacade.logInfo(TAG, "Sending: " + model);

        SubmitReq sr = MM7ConverterOut.getSubmitReq(model);

        MmscSendConnectionModel connModel = model.getMmscSendConnectionModel();

        MM7Context mm7Context = new MM7Context();

        mm7Context.setMm7Version(connModel.getMm7Version());
        mm7Context.setMm7Namespace(connModel.getMm7NameSpace());

        mm7Context.setUsername(connModel.getUsername());
        mm7Context.setPassword(connModel.getPassword());

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MM7Message.save(sr, baos, mm7Context);

        MMSCLoggerFacade.logSubmitReq(baos);

        BasicMMSC mmsc = new BasicMMSC(connModel.getAddress());
        mmsc.setContext(mm7Context);
        SubmitRsp submitRsp = mmsc.submit(sr);

        MM7SubmitResponseModel resp = MM7Converter.getModel(submitRsp);

        MMSCLoggerFacade.logInfo(TAG, "MMS Send to MMSC with response: " + resp);

        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
        MM7Message.save(submitRsp, baos2, mm7Context);
        MMSCLoggerFacade.logSubmitRsp(baos2);

        return resp;
    }
}
