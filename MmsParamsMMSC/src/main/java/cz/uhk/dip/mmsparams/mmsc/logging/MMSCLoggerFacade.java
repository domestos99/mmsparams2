package cz.uhk.dip.mmsparams.mmsc.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;

public class MMSCLoggerFacade
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MMSCLoggerFacade.class);

    private MMSCLoggerFacade()
    {
    }

    public static void logSubmitRsp(ByteArrayOutputStream os)
    {
        byte[] imageBytes = os.toByteArray();
        LOGGER.info(new String(imageBytes));
    }

    public static void logSubmitReq(ByteArrayOutputStream os)
    {
        byte[] imageBytes = os.toByteArray();
        LOGGER.info(new String(imageBytes));
    }

    public static void logInfo(String tag, String info)
    {
        LOGGER.info(getMessage(tag, info));
    }

    public static void logWarning(String tag, String warning)
    {
        LOGGER.warn(getMessage(tag, warning));
    }

    public static void logEx(String tag, Exception ex)
    {
        LOGGER.error(tag, ex);
        MMSCLoggerFatalFacade.logEx(tag, ex);
    }

    public static void logEx(String tag, String message, Exception ex)
    {
        LOGGER.error(getMessage(tag, message), ex);
        MMSCLoggerFatalFacade.logEx(tag, message, ex);
    }

    private static String getMessage(String tag, String message)
    {
        return tag + ": " + message;
    }
}
