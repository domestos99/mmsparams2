package cz.uhk.dip.mmsparams.mmsc.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MMSCLoggerFatalFacade
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MMSCLoggerFatalFacade.class);

    private MMSCLoggerFatalFacade()
    {
    }

    public static void logEx(String tag, Exception ex)
    {
        LOGGER.error(tag, ex);
    }

    public static void logEx(String tag, String message, Exception ex)
    {
        LOGGER.error(getMessage(tag, message), ex);
    }

    private static String getMessage(String tag, String message)
    {
        return tag + ": " + message;
    }
}
