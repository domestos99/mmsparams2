package cz.uhk.dip.mmsparams.mmsc.utils;

import net.instantcom.mm7.Address;
import net.instantcom.mm7.BasicContent;
import net.instantcom.mm7.BinaryContent;
import net.instantcom.mm7.ChargedParty;
import net.instantcom.mm7.Content;
import net.instantcom.mm7.ContentTransferEncoding;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MessageClass;
import net.instantcom.mm7.Priority;
import net.instantcom.mm7.RelativeDate;
import net.instantcom.mm7.SubmitRsp;
import net.instantcom.mm7.TextContent;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.enums.AddressCoding;
import cz.uhk.dip.mmsparams.api.enums.AddressType;
import cz.uhk.dip.mmsparams.api.enums.MM7Encoding;
import cz.uhk.dip.mmsparams.api.enums.RecipientType;
import cz.uhk.dip.mmsparams.api.enums.mmsc.MM7ContentType;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ContentModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7RelativeDate;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendMmsModel;

public class MM7Converter
{
    private MM7Converter()
    {
    }

    public static MessageClass getMessageClass(final cz.uhk.dip.mmsparams.api.enums.MessageClass messageClass)
    {
        if (messageClass == null)
            return null;
        switch (messageClass)
        {
            case UNDEFINED:
                return MessageClass.PERSONAL;
            case PERSONAL:
                return MessageClass.PERSONAL;
            case ADVERTISEMENT:
                return MessageClass.ADVERTISMENT;
            case INFORMATIONAL:
                return MessageClass.INFORMATIONAL;
            case AUTO:
                return MessageClass.AUTO;
            default:
                return MessageClass.PERSONAL;
        }
    }

    public static Priority getPriority(final cz.uhk.dip.mmsparams.api.enums.Priority priority)
    {
        if (priority == null)
            return null;
        switch (priority)
        {
            case LOW:
                return Priority.LOW;
            case NORMAL:
                return Priority.NORMAL;
            case HIGH:
                return Priority.HIGH;
            default:
                return Priority.NORMAL;
        }
    }

    public static cz.uhk.dip.mmsparams.api.enums.Priority getPriority(Priority priority)
    {
        if (priority == null)
            return null;

        switch (priority)
        {
            case NORMAL:
                return cz.uhk.dip.mmsparams.api.enums.Priority.NORMAL;
            case HIGH:
                return cz.uhk.dip.mmsparams.api.enums.Priority.HIGH;
            case LOW:
                return cz.uhk.dip.mmsparams.api.enums.Priority.LOW;
        }
        return null;
    }

    public static ChargedParty getChangedParty(final cz.uhk.dip.mmsparams.api.enums.ChargedParty chargedParty)
    {
        if (chargedParty == null)
            return null;
        switch (chargedParty)
        {
            case SENDER:
                return ChargedParty.SENDER;
            case RECIPIENT:
                return ChargedParty.RECIPIENT;
            case BOTH:
                return ChargedParty.BOTH;
            case NEITHER:
                return ChargedParty.NEITHER;
            case THIRD_PARTY:
                return ChargedParty.THIRD_PARTY;
            default:
                return null;
        }
    }

    private static Address.AddressType getAddressType(final AddressType addressType)
    {
        if (addressType == null)
            return null;
        switch (addressType)
        {
            case RFC822_ADDRESS:
                return Address.AddressType.RFC822_ADDRESS;
            case NUMBER:
                return Address.AddressType.NUMBER;
            case SHORT_CODE:
                return Address.AddressType.SHORT_CODE;
            default:
                return Address.AddressType.NUMBER;
        }
    }

    static Address getSenderAddress(MM7Address address)
    {
        if (address == null)
            return null;
        // TODO for sender addres - maybe not using
        Address result = new Address(address.getAddress(), null, getAddressType(address.getAddressType()));
        result.setDisplayOnly(address.isDisplayOnly());
        result.setAddressCoding(getAddressCoding(address.getAddressCoding()));

        return result;
    }


    private static Address getSenderAddress(MM7Address address, Address.RecipientType recipientType)
    {
        return new Address(address.getAddress(), recipientType, getAddressType(address.getAddressType()));
    }

    static MM7Address getMM7Address(final Address address)
    {
        if (address == null)
            return null;

        MM7Address ad = new MM7Address();
        ad.setAddress(address.getAddress());
        ad.setAddressType(getAddressType(address.getAddressType()));
        ad.setRecipientType(getAddressRecipientType(address.getRecipientType()));
        ad.setDisplayOnly(address.isDisplayOnly());
        ad.setAddressCoding(getAddressCoding(address.getAddressCoding()));
        ad.setId(address.getId());

        return ad;
    }

    private static Address.RecipientType getAddressRecipientType(RecipientType recipientType)
    {
        if (recipientType == null)
            return null;

        switch (recipientType)
        {
            case TO:
                return Address.RecipientType.TO;
            case CC:
                return Address.RecipientType.CC;
            case BCC:
                return Address.RecipientType.BCC;
            case FROM:
            default:
                return null;
        }
    }

    private static RecipientType getAddressRecipientType(Address.RecipientType recipientType)
    {
        if (recipientType == null)
            return null;


        switch (recipientType)
        {
            case TO:
                return RecipientType.TO;
            case CC:
                return RecipientType.CC;
            case BCC:
                return RecipientType.BCC;
            default:
                return null;
        }
    }

    private static Address.AddressCoding getAddressCoding(AddressCoding addressCoding)
    {
        if (addressCoding == null)
            return null;

        switch (addressCoding)
        {
            case ENCRYPTED:
                return Address.AddressCoding.ENCRYPTED;
            case OBFUSCATED:
                return Address.AddressCoding.OBFUSCATED;
            default:
                return null;
        }
    }

    private static AddressCoding getAddressCoding(Address.AddressCoding addressCoding)
    {
        if (addressCoding == null)
            return null;

        switch (addressCoding)
        {
            case ENCRYPTED:
                return AddressCoding.ENCRYPTED;
            case OBFUSCATED:
                return AddressCoding.OBFUSCATED;
            default:
                return null;
        }
    }

    private static AddressType getAddressType(Address.AddressType addressType)
    {
        if (addressType == null)
            return null;

        switch (addressType)
        {
            case RFC822_ADDRESS:
                return AddressType.RFC822_ADDRESS;
            case NUMBER:
                return AddressType.NUMBER;
            case SHORT_CODE:
                return AddressType.SHORT_CODE;
        }
        return null;
    }

    static ContentTransferEncoding getMm7Encoding(MM7Encoding mm7Encoding)
    {
        switch (mm7Encoding)
        {
            case BASE64:
                return ContentTransferEncoding.BASE64;
            case UTF8:
                return ContentTransferEncoding.UTF8;
            default:
                return ContentTransferEncoding.BASE64;
        }
    }

    static MM7Encoding getMm7Encoding(ContentTransferEncoding mm7Encoding)
    {
        switch (mm7Encoding)
        {
            case BASE64:
                return MM7Encoding.BASE64;
            case UTF8:
                return MM7Encoding.UTF8;
            default:
                return MM7Encoding.BASE64;
        }
    }

    static ArrayList<MM7Address> getMM7Addresses(final List<Address> addresses)
    {
        ArrayList<MM7Address> result = new ArrayList<>();

        if (addresses == null)
            return result;

        for (int i = 0; i < addresses.size(); i++)
        {
            result.add(getMM7Address(addresses.get(0)));
        }
        return result;
    }

    static List<net.instantcom.mm7.Address> getRecipients(final MmscSendMmsModel mmsModel)
    {
        final List<Address> recipients = new ArrayList<>();

        final ArrayList<MM7Address> to = mmsModel.getRecipientsTo();
        if (to != null && !to.isEmpty())
        {
            for (MM7Address ad : to)
            {
                recipients.add(getSenderAddress(ad, Address.RecipientType.TO));
            }
        }

        final ArrayList<MM7Address> bcc = mmsModel.getRecipientsBcc();
        if (bcc != null && !bcc.isEmpty())
        {
            for (MM7Address ad : bcc)
            {
                recipients.add(getSenderAddress(ad, Address.RecipientType.BCC));
            }
        }

        final ArrayList<MM7Address> cc = mmsModel.getRecipientsCc();
        if (cc != null && !cc.isEmpty())
        {
            for (MM7Address ad : cc)
            {
                recipients.add(getSenderAddress(ad, Address.RecipientType.CC));
            }
        }
        return recipients;
    }


    static RelativeDate getRelativeDate(MM7RelativeDate expiry)
    {
        return new RelativeDate(expiry.getRepresentation());
    }

    public static MM7ErrorModel getError(final MM7Error mm7error)
    {
        if (mm7error == null)
            return null;

        MM7ErrorModel model = new MM7ErrorModel();

        model.setFaultCode(mm7error.getFaultCode());
        model.setFaultMessage(mm7error.getFaultMessage());
        model.setMessage(mm7error.getMessage());
        model.setException((Exception) mm7error);

        return model;
    }

    public static MM7SubmitResponseModel getModel(SubmitRsp submitRsp)
    {
        if (submitRsp == null)
            return null;

        MM7SubmitResponseModel model = new MM7SubmitResponseModel();
        model.setMessageId(submitRsp.getMessageId());
        model.setStatusCode(submitRsp.getStatusCode());
        model.setStatusText(submitRsp.getStatusText());
        model.setMm7Version(submitRsp.getMm7Version());
        model.setNamespace(submitRsp.getNamespace());
        model.setSoapBoundary(submitRsp.getSoapBoundary());
        model.setSoapContentId(submitRsp.getSoapContentId());
        model.setSoapContentType(submitRsp.getSoapContentType());
        model.setTransactionId(submitRsp.getTransactionId());
        model.setMultipart(submitRsp.isMultipart());


        return model;
    }


    public static MM7ContentModel convertContent(Content content)
    {
        if (content == null)
            return null;

        MM7ContentModel parent = new MM7ContentModel();

        fillContentModel(parent, content);

        convertContentRec(parent, content);

        return parent;
    }

    public static void convertContentRec(MM7ContentModel c, Content content)
    {
        if (content.getParts() != null)
        {
            for (Content childContent : content.getParts())
            {
                MM7ContentModel child = new MM7ContentModel();
                fillContentModel(child, childContent);
                c.addPart(child);

                convertContentRec(child, childContent);
            }
        }
    }

    private static void fillContentModel(MM7ContentModel c, Content content)
    {
        if (c == null || content == null)
            return;

        c.setContentId(content.getContentId());
        c.setContentType(content.getContentType());
        c.setContentLocation(content.getContentLocation());

        if (content instanceof BasicContent)
        {
            c.setMm7ContentType(MM7ContentType.BASIC);
        }

        if (content instanceof BinaryContent)
        {
            BinaryContent binary = (BinaryContent)content;

            c.setData(binary.getData());
            c.setMm7ContentType(MM7ContentType.BINARY);
        }
        else if (content instanceof TextContent)
        {
            TextContent text = (TextContent)content;

            c.setText(text.getText());
            c.setMm7Encoding(getMm7Encoding(text.getEncoding()));
            c.setMm7ContentType(MM7ContentType.TEXT);
        }


    }











}
