package cz.uhk.dip.mmsparams.mmsc.utils;

import net.instantcom.mm7.DeliverReq;
import net.instantcom.mm7.DeliveryReportReq;
import net.instantcom.mm7.MM7Request;
import net.instantcom.mm7.ReadReplyReq;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7RequestModelBase;

public class MM7ConverterIn
{
    private MM7ConverterIn()
    {
    }

    private static <T extends MM7RequestModelBase> T fillHeader(T model, MM7Request req)
    {
        model.setSenderAddress(MM7Converter.getMM7Address(req.getSenderAddress()));
        model.setRelayServerId(req.getRelayServerId());
        model.setVaspId(req.getVaspId());
        model.setVasId(req.getVasId());
        model.setMm7Version(req.getMm7Version());
        model.setMm7NamespacePrefix(req.getMm7NamespacePrefix());
        model.setSoapBoundary(req.getSoapBoundary());
        model.setSoapContentId(req.getSoapContentId());
        model.setSoapContentType(req.getSoapContentType());
        model.setTransactionId(req.getTransactionId());
        model.setNamespace(req.getNamespace());

        return model;
    }

    public static MM7DeliveryReqModel getDeliveryReqModel(DeliverReq deliverReq)
    {
        if (deliverReq == null)
            return null;

        MM7DeliveryReqModel model = new MM7DeliveryReqModel();

        fillHeader(model, deliverReq);

        model.setSender(MM7Converter.getMM7Address(deliverReq.getSender()));
        model.setLinkedId(deliverReq.getLinkedId());
        model.setRecipients(MM7Converter.getMM7Addresses(deliverReq.getRecipients()));
        model.setSenderSPI(deliverReq.getSenderSPI());
        model.setRecipientSPI(deliverReq.getRecipientSPI());
        model.setTimeStamp(deliverReq.getTimeStamp());
        model.setReplyChargingId(deliverReq.getReplyChargingId());
        model.setPriority(MM7Converter.getPriority(deliverReq.getPriority()));
        model.setSubject(deliverReq.getSubject());
        model.setApplicId(deliverReq.getApplicId());
        model.setReplyApplicId(deliverReq.getReplyApplicId());
        model.setAuxApplicInfo(deliverReq.getAuxApplicInfo());
        model.setContent(MM7Converter.convertContent(deliverReq.getContent()));


        return model;
    }

    public static MM7DeliveryReportReqModel getDeliveryReportReqModel(DeliveryReportReq deliveryReportReq)
    {
        if (deliveryReportReq == null)
            return null;

        MM7DeliveryReportReqModel model = new MM7DeliveryReportReqModel();

        fillHeader(model, deliveryReportReq);

        model.setMessageID(deliveryReportReq.getMessageID());
        model.setRecipient(MM7Converter.getMM7Address(deliveryReportReq.getRecipient()));
        model.setSender(MM7Converter.getMM7Address(deliveryReportReq.getSender()));
        model.setDate(deliveryReportReq.getDate());
        model.setMmStatus(deliveryReportReq.getMmStatus());
        model.setStatusText(deliveryReportReq.getStatusText());

        return model;
    }

    public static MM7ReadReplyReqModel getReadReplyReq(ReadReplyReq readReplyReq)
    {
        if (readReplyReq == null)
            return null;

        MM7ReadReplyReqModel model = new MM7ReadReplyReqModel();

        fillHeader(model, readReplyReq);

        model.setMessageID(readReplyReq.getMessageID());
        model.setRecipient(MM7Converter.getMM7Address(readReplyReq.getRecipient()));
        model.setSender(MM7Converter.getMM7Address(readReplyReq.getSender()));
        model.setTimeStamp(readReplyReq.getTimeStamp());
        model.setMmStatus(readReplyReq.getMmStatus());
        model.setStatusText(readReplyReq.getStatusText());


        return model;
    }

}
