package cz.uhk.dip.mmsparams.mmsc.utils;

import net.instantcom.mm7.BasicContent;
import net.instantcom.mm7.BinaryContent;
import net.instantcom.mm7.Content;
import net.instantcom.mm7.RelativeDate;
import net.instantcom.mm7.SubmitReq;
import net.instantcom.mm7.TextContent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Attachment;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7RelativeDate;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Text;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendConnectionModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendMmsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;

public class MM7ConverterOut
{
    private MM7ConverterOut()
    {
    }


    public static SubmitReq getSubmitReq(final MmscSendModel model)
    {
        if (model == null)
            return null;

        final SubmitReq req = new SubmitReq();

        MmscSendConnectionModel connModel = model.getMmscSendConnectionModel();
        MmscSendMmsModel mmsModel = model.getMmscSendMmsModel();


        req.setVasId(connModel.getVasId());
        req.setVaspId(connModel.getVaspId());
        req.setSenderAddress(MM7Converter.getSenderAddress(mmsModel.getSenderAddress()));
        req.setRelayServerId(mmsModel.getRelayServerId());


        req.setAllowAdaptations(mmsModel.getAllowAdaptations());
        req.setApplicID(mmsModel.getApplicID());

        req.setAuxApplicId(mmsModel.getAuxApplicId());

        req.setChargedParty(MM7Converter.getChangedParty(mmsModel.getChangedParty()));

        req.setChargedPartyId(mmsModel.getChargedPartyId());

        req.setContent(getSubmitContent(mmsModel));

        req.setTransactionId("mivas" + new Date().getTime());

        MM7RelativeDate expiry = mmsModel.getExpiryDate();
        if (expiry != null)
        {
            RelativeDate rd = MM7Converter.getRelativeDate(expiry);
            req.setExpiryDate(rd);
        }


        // TODO
        //req.setContentClass(ContentClass contentClass);

        // TODO ???
        // req.setDeliveryCondition(List < Integer > deliveryCondition);

        req.setDeliveryReport(mmsModel.getDeliveryReport());

        req.setDistributionIndicator(mmsModel.getDistributionIndicator());

        req.setDrmContent(mmsModel.getDrmContent());

        // TODO
        // req.setEarlistDeliveryTime(RelativeDate earlistDeliveryTime);

        // TODO
        // req.setExpiryDate(RelativeDate expiryDate);

        req.setLinkedId(mmsModel.getLinkedId());

        req.setMessageClass(MM7Converter.getMessageClass(mmsModel.getMessageClass()));

        req.setPriority(MM7Converter.getPriority(mmsModel.getPriority()));

        req.setReadReply(mmsModel.getReadReply());

        req.setRecipients(MM7Converter.getRecipients(mmsModel));

        req.setReplyApplicID(mmsModel.getReplyApplicID());

        req.setReplyChargingSize(mmsModel.getReplyChargingSize());

        // TODO
        //req.setReplyDeadline(RelativeDate replyDeadline());

        req.setServiceCode(mmsModel.getServiceCode());

        req.setSubject(mmsModel.getSubject());

        // TODO ???
        req.setTimeStamp(new Date());


        return req;
    }

    static Content getSubmitContent(final MmscSendMmsModel mmsModel)
    {
        // Add text content
        List<Content> contents = new ArrayList<>();

        ArrayList<MM7Text> text = mmsModel.getMm7Texts();
        if (text != null)
        {
            for (int i = 0; i < text.size(); i++)
            {
                MM7Text t = text.get(i);
                TextContent textContent = new TextContent(t.getText());
                textContent.setEncoding(MM7Converter.getMm7Encoding(t.getMm7Encoding()));
                textContent.setContentId("text" + (i + 1) + ".txt");
                contents.add(textContent);
            }
        }

        ArrayList<MM7Attachment> attachments = mmsModel.getMm7Attachment();

        if (attachments != null)
        {
            for (MM7Attachment att : mmsModel.getMm7Attachment())
            {
                BinaryContent binaryContent = new BinaryContent();
                // someImageHeaders.addHeader("Content-Transfer-Encoding", "base64");
                binaryContent.setData(att.getData());
                binaryContent.setContentType(att.getContentType());
                binaryContent.setContentId(att.getContentId());

                contents.add(binaryContent);
            }
        }

        return new BasicContent(contents);
    }

}
