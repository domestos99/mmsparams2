package net.instantcom.mm7;

/**
 * MmsParams edit
 */
public enum ContentTransferEncoding
{
    BASE64,
    UTF8
}
