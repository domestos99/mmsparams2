/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2007-2014 InstantCom Ltd. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * https://raw.github.com/vnesek/instantcom-mm7/master/LICENSE.txt
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at appropriate location.
 */

package net.instantcom.mm7;

import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.io.OutputStream;

/**
 * MmsParams edit
 */
public class TextContent extends BasicContent
{
    private String text;
    private ContentTransferEncoding encoding;


    public TextContent()
    {
        this("");
    }

    public TextContent(String text)
    {
        setText(text);
        setContentType("text/plain; charset=\"utf-8\"");
        setEncoding(ContentTransferEncoding.BASE64);
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public ContentTransferEncoding getEncoding()
    {
        return encoding;
    }

    public void setEncoding(ContentTransferEncoding encoding)
    {
        this.encoding = encoding;
    }

    @Override
    public void writeTo(OutputStream out, String contentId, MM7Context ctx) throws IOException
    {
        if (contentId == null)
        {
            contentId = getContentId();
        }
        StringBuilder b = new StringBuilder();
        b.append("\r\nContent-Type: ");
        b.append(getContentType());

        if (encoding != null)
        {
            b.append("\r\nContent-Transfer-Encoding: ");
            b.append(getEncoding().toString().toLowerCase());
        }

        if (contentId != null)
        {
            b.append("\r\nContent-ID: <" + contentId + ">");
        }
        b.append("\r\n\r\n");
        out.write(b.toString().getBytes("iso-8859-1"));


        if (encoding == ContentTransferEncoding.BASE64)
        {
            byte byteArray[] = text.getBytes();
            Base64 encoder = new Base64();
            String enc = encoder.encodeToString(byteArray);

            out.write(enc.getBytes("utf-8"));
        }
        else
        {
            out.write(text.getBytes("utf-8"));
        }
    }

    @Override
    public int getContentLength()
    {
        return text.length();
    }


}
