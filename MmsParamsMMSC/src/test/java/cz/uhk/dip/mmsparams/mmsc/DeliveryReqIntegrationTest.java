package cz.uhk.dip.mmsparams.mmsc;

import net.instantcom.mm7.Address;
import net.instantcom.mm7.ContentType;
import net.instantcom.mm7.DeliverReq;
import net.instantcom.mm7.MM7Context;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MM7Response;

import org.junit.Test;
import org.jvnet.mimepull.MIMEConfig;
import org.jvnet.mimepull.MIMEMessage;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class DeliveryReqIntegrationTest
{


    @Test
    public void test1() throws IOException, MM7Error
    {
        String ct = "multipart/related; boundary=\"NextPart_000_0125_01C19839.7237929064\"; start=\"</cmvt256/mm7-deliver>\"; type=text/xml";
        InputStream in = DeliverReq.class.getResourceAsStream("deliver-req2.txt");
        DeliverReq req = (DeliverReq) MM7Response.load(in, ct, new MM7Context());


        assertEquals("wthr8391", req.getLinkedId());
        assertEquals("97254265781@OMMS.com", req.getSender().getAddress());
        assertEquals(Address.AddressType.RFC822_ADDRESS, req.getSender().getAddressType());
    }

    @Test
    public void test2() throws IOException
    {
        String boundary = "Nokia-mm-messageHandler-BoUnDaRy-=_-735647067";

        InputStream in = DeliverReq.class.getResourceAsStream("deliver-req3.txt");

        MIMEMessage msg = new MIMEMessage(in, boundary, new MIMEConfig());

        msg.parseAll();

        in.close();

        assertEquals(2, msg.getAttachments().size());
    }

    @Test
    public void test3()
    {
        String ct = "multipart/related; boundary=\"Nokia-mm-messageHandler-BoUnDaRy-=_-735647067\"; type=text/xml";

        ContentType contentType = new ContentType(ct);
        String par = contentType.getParameter("boundary");

        assertEquals("Nokia-mm-messageHandler-BoUnDaRy-=_-735647067", par);

    }
}
