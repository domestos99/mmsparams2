package cz.uhk.dip.mmsparams.mmsc;

import net.instantcom.mm7.MM7Context;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static org.junit.Assert.assertEquals;

public class MM7EncodingTest
{

    @Test
    public void test1() throws IOException
    {
        MM7Context ctx = new MM7Context();
        ctx.setUsername("user-name");
        ctx.setPassword("my-password");


        String authString = ctx.getUsername() + ':' + ctx.getPassword();

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(128);
        OutputStream buffer64 = ctx.newBase64OutputStream(buffer);
        buffer64.write(authString.getBytes("iso-8859-1"));
        buffer64.close();
        String result = buffer.toString("iso-8859-1");
        result = result.trim();

        String o2result = getEncodeCredentials(ctx.getUsername(), ctx.getPassword());

        assertEquals(o2result, result);

    }

    private String getEncodeCredentials(String username, String password)
    {
        String result = new String();
        String stringToEncode = new String();
        stringToEncode = username + ":" + password;
        Base64 encoder;
        encoder = new Base64();
        byte[] encodedBytes = encoder.encode(stringToEncode.getBytes());
        result = new String(encodedBytes);
        return result;
    }

}
