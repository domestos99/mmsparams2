package cz.uhk.dip.mmsparams.mmsc;

import net.instantcom.mm7.BasicMMSC;
import net.instantcom.mm7.MM7Context;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.SubmitReq;
import net.instantcom.mm7.SubmitRsp;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import cz.uhk.dip.mmsparams.api.enums.AddressType;
import cz.uhk.dip.mmsparams.api.enums.ChargedParty;
import cz.uhk.dip.mmsparams.api.enums.MM7Encoding;
import cz.uhk.dip.mmsparams.api.enums.MessageClass;
import cz.uhk.dip.mmsparams.api.enums.Priority;
import cz.uhk.dip.mmsparams.api.enums.RecipientType;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Attachment;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Text;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendConnectionModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendMmsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;
import cz.uhk.dip.mmsparams.mmsc.mock.MockBasicMMSC;
import cz.uhk.dip.mmsparams.mmsc.utils.MM7Converter;
import cz.uhk.dip.mmsparams.mmsc.utils.MM7ConverterOut;

public class MmscSenderTest
{

    @Test
    public void test1() throws IOException, MM7Error
    {
        MmscSendModel model = getMmscSendModel();
        SubmitReq sr = MM7ConverterOut.getSubmitReq(model);


        MmscSendConnectionModel connModel = model.getMmscSendConnectionModel();

        MM7Context mm7Context = new MM7Context();

        mm7Context.setMm7Version(connModel.getMm7Version());
        mm7Context.setMm7Namespace(connModel.getMm7NameSpace());


        //   MM7Message.save(sr, System.out, mm7Context);


        BasicMMSC mmsc = new MockBasicMMSC(connModel.getAddress());
        mmsc.setContext(mm7Context);
        SubmitRsp submitRsp = mmsc.submit(sr);
        System.out.println(submitRsp);
    }

    private MmscSendModel getMmscSendModel() throws IOException
    {
        MmscSendConnectionModel connModel = new MmscSendConnectionModel();
        connModel.setAddress("http://127.0.0.1:4301/MMSC");
        connModel.setMm7Version("5.6.0");

        connModel.setVasId("test");
        connModel.setVaspId("test");

        connModel.setUsername("test");
        connModel.setPassword("test");


        MmscSendMmsModel mmsModel = new MmscSendMmsModel();

        mmsModel.setSenderAddress(new MM7Address("+420123123123", AddressType.NUMBER, RecipientType.TO));

        mmsModel.addRecipientTo(new MM7Address("789456", AddressType.NUMBER, RecipientType.TO));
        mmsModel.addRecipientBcc(new MM7Address("789456", AddressType.NUMBER, RecipientType.TO));
        mmsModel.addRecipientCc(new MM7Address("789456", AddressType.NUMBER, RecipientType.TO));

        mmsModel.setSubject("This is important message");
        mmsModel.addMm7Text(new MM7Text("text", MM7Encoding.BASE64));


        mmsModel.setDeliveryReport(false);
        mmsModel.setMessageClass(MessageClass.PERSONAL);
        mmsModel.setPriority(Priority.NORMAL);
        mmsModel.setChangedParty(ChargedParty.SENDER);
        mmsModel.setAllowAdaptations(true);


        MM7Attachment attachment = new MM7Attachment();

        attachment.setContentId("picture");
        attachment.setContentType("image/jpeg");


        File file2 = new File("C:\\imgs\\smaller.jpg");
        FileInputStream fin2 = new FileInputStream(file2);
        byte byteArray[] = new byte[fin2.available()];
        int i = -1, k = 0;
        while ((i = fin2.read()) != -1)
        {
            byteArray[k++] = (byte) i;
        }

        attachment.setData(byteArray);

        mmsModel.addMm7Attachment(attachment);


        MmscSendModel model = new MmscSendModel(connModel, mmsModel);
        return model;
    }

}
