package cz.uhk.dip.mmsparams.mmsc;

import net.instantcom.mm7.MM7Context;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MM7Message;
import net.instantcom.mm7.SubmitReq;

import org.junit.Test;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.enums.AddressType;
import cz.uhk.dip.mmsparams.api.enums.ChargedParty;
import cz.uhk.dip.mmsparams.api.enums.MM7Encoding;
import cz.uhk.dip.mmsparams.api.enums.MessageClass;
import cz.uhk.dip.mmsparams.api.enums.Priority;
import cz.uhk.dip.mmsparams.api.enums.RecipientType;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7RelativeDate;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Text;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendConnectionModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendMmsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;
import cz.uhk.dip.mmsparams.mmsc.utils.MM7Converter;
import cz.uhk.dip.mmsparams.mmsc.utils.MM7ConverterOut;

public class TestMmsTest_data
{
    static String numFrom = "999558";
    static String numTo = "+420602701415";

    @Test
    public void test1() throws IOException, MM7Error
    {
        MmscSendConnectionModel conn = new MmscSendConnectionModel();

        conn.setAddress("http://127.0.0.1:2007");
        conn.setUsername("dmccztest");
        conn.setPassword("passdmcczt");
        conn.setVaspId("DMC_CZ_test");
        conn.setVasId("DMC_CZ_test");
        conn.setMm7Version("6.8.0");

        // conn.setMm7NameSpace();

        MmscSendMmsModel mmsModel = new MmscSendMmsModel();


        MM7Address sender = new MM7Address(numFrom, AddressType.NUMBER, RecipientType.FROM, false);
        mmsModel.setSenderAddress(sender);

        MM7Address recipient = new MM7Address(numTo, AddressType.NUMBER, RecipientType.TO, false);
        mmsModel.addRecipientTo(recipient);

        mmsModel.setMessageClass(MessageClass.PERSONAL);
        mmsModel.setExpiryDate(new MM7RelativeDate("PT2M"));

        mmsModel.setDeliveryReport(false);
        mmsModel.setPriority(Priority.NORMAL);
        mmsModel.setSubject("O2 Test");
        mmsModel.setChangedParty(ChargedParty.SENDER);


        String text = "Testovaci MMS";
        mmsModel.addMm7Text(new MM7Text(text, MM7Encoding.BASE64));
        mmsModel.addMm7Text(new MM7Text(text, MM7Encoding.BASE64));

        send(new MmscSendModel(conn, mmsModel));


    }

    private void send(MmscSendModel model) throws MM7Error, IOException
    {
        SubmitReq sr = MM7ConverterOut.getSubmitReq(model);

        MmscSendConnectionModel connModel = model.getMmscSendConnectionModel();

        MM7Context mm7Context = new MM7Context();

        mm7Context.setMm7Version(connModel.getMm7Version());
        mm7Context.setMm7Namespace(connModel.getMm7NameSpace());


        MM7Message.save(sr, System.out, mm7Context);


//        BasicMMSC mmsc = new BasicMMSC(connModel.getAddress());
//        mmsc.setContext(mm7Context);
//        SubmitRsp submitRsp = mmsc.submit(sr);
//        System.out.println(submitRsp);

    }

}
