package cz.uhk.dip.mmsparams.mmsc.mock;

import net.instantcom.mm7.BasicMMSC;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.SubmitReq;
import net.instantcom.mm7.SubmitRsp;

public class MockBasicMMSC extends BasicMMSC
{
    public MockBasicMMSC(String url)
    {
        super(url);
    }

    @Override
    public SubmitRsp submit(SubmitReq submitReq) throws MM7Error
    {
        //        return super.submit(submitReq);
        SubmitRsp resp = new SubmitRsp();
        resp.setMm7Version(submitReq.getMm7Version());
        resp.setStatusCode(1000);
        resp.setStatusText("Success");
        resp.setMessageId("369500617770864640");
        return resp;

    }
}
