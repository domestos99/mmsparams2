package cz.uhk.dip.mmsparams.mmsc.parse;

import net.instantcom.mm7.Address;
import net.instantcom.mm7.BasicContent;
import net.instantcom.mm7.BinaryContent;
import net.instantcom.mm7.Content;
import net.instantcom.mm7.DeliverReq;
import net.instantcom.mm7.MM7Context;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MM7Response;
import net.instantcom.mm7.Priority;
import net.instantcom.mm7.SubmitRsp;
import net.instantcom.mm7.TextContent;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class AtMmsTest
{
    final String fileName = "AT_MMS_part1.txt";

    private DeliverReq getPDU() throws IOException, MM7Error
    {
        String ct = "multipart/related;\ttype=\"text/xml\";\tboundary=\"----=_NextPart_1446592985\";\tstart=\"<start_deliver_67240>\"";

        InputStream in = DeliveryReportExpiredTest.class.getResourceAsStream(fileName);
        return (DeliverReq) MM7Response.load(in, ct, new MM7Context());
    }

    @Test
    public void AO_MMS_Reject_part2_Test() throws IOException, MM7Error
    {
        DeliverReq pdu = getPDU();
        assertNotNull(pdu);


        assertEquals("Jinny", pdu.getVaspId());
        assertEquals("Jinny", pdu.getVasId());
        assertEquals("1729", pdu.getLinkedId());
        // replayServerId
        assertEquals("6.8.0", pdu.getMm7Version());
        assertEquals("mm7-soap", pdu.getSoapContentId());
        assertEquals("1729", pdu.getTransactionId());

        assertNotNull(pdu.getSender());
        assertEquals("420602744279", pdu.getSender().getAddress());
        assertEquals(1, pdu.getRecipients().size());
        assertEquals("99998002", pdu.getRecipients().get(0).getAddress());
        assertEquals(Address.AddressType.SHORT_CODE, pdu.getRecipients().get(0).getAddressType());
        assertEquals(Address.RecipientType.TO, pdu.getRecipients().get(0).getRecipientType());
        assertEquals(false, pdu.getRecipients().get(0).isDisplayOnly());


        assertEquals(Priority.NORMAL, pdu.getPriority());


        assertNotNull(pdu.getContent());
        assertEquals(BasicContent.class, pdu.getContent().getClass());
        // assertEquals("==Multipart==dec3e2b3-8b48-47a9-a538-3f2cbbb5d9ba", pdu.getContent().get);  boundary

        assertEquals(3, pdu.getContent().getParts().size());

        assertEquals(BinaryContent.class, pdu.getContent().getParts().get(0).getClass());
        assertEquals(TextContent.class, pdu.getContent().getParts().get(1).getClass());
        assertEquals(BinaryContent.class, pdu.getContent().getParts().get(2).getClass());


        // Part 1
        BinaryContent part1 = (BinaryContent)pdu.getContent().getParts().get(0);
        assertEquals("0", part1.getContentId());
        assertEquals("24052013.jpg", part1.getContentLocation());
        assertNull(part1.getParts());

        // Part 2
        TextContent part2 = (TextContent)pdu.getContent().getParts().get(1);
        assertEquals("Text\r\n", part2.getText());
        assertEquals("1", part2.getContentId());
        assertNull(part2.getParts());

        // Part 3

        BinaryContent part3 = (BinaryContent)pdu.getContent().getParts().get(2);
        assertEquals("530848044", part3.getContentId());
        assertNull(part3.getParts());




    }
}

