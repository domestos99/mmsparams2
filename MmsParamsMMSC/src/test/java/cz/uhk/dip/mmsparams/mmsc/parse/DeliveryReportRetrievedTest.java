package cz.uhk.dip.mmsparams.mmsc.parse;

import net.instantcom.mm7.DeliveryReportReq;
import net.instantcom.mm7.DeliveryReportRsp;
import net.instantcom.mm7.MM7Context;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MM7Response;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DeliveryReportRetrievedTest
{
    final String fileName = "AO_MMS_Delivery_Report_Retreived.txt";

    private DeliveryReportReq getPDU() throws IOException, MM7Error
    {
        String ct = "text/xml; charset=\"utf-8\"";

        InputStream in = DeliveryReportRetrievedTest.class.getResourceAsStream(fileName);
        DeliveryReportReq req = (DeliveryReportReq) MM7Response.load(in, ct, new MM7Context());
        return req;
    }

    @Test
    public void AO_MMS_Delivery_Report_Retreived_Test() throws IOException, MM7Error
    {
        DeliveryReportReq req = getPDU();

        assertNotNull(req);
        assertEquals("6.8.0", req.getMm7Version());
        assertEquals("3289_mmsc1", req.getMessageID());
        assertEquals("Jinny", req.getVaspId());
        assertEquals("Jinny", req.getVasId());
        assertEquals("", req.getRelayServerId());
        assertEquals("420602634990", req.getRecipient().getAddress());
        assertEquals("999913",req.getSender().getAddress() );
        assertEquals("Retrieved", req.getMmStatus());
        assertEquals("Message Retrieved", req.getStatusText());
        assertEquals("3290", req.getTransactionId());

        Date dt = req.getDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);

        assertEquals(2016, cal.get(Calendar.YEAR));
        assertEquals(1, cal.get(Calendar.MONTH)+1);
        assertEquals(21, cal.get(Calendar.DAY_OF_MONTH));
        assertEquals(13, cal.get(Calendar.HOUR_OF_DAY)-1);
        assertEquals(36, cal.get(Calendar.MINUTE));
        assertEquals(05, cal.get(Calendar.SECOND));
    }

    @Test
    public void AO_MMS_Delivery_Report_Retreived_Response_Test() throws IOException, MM7Error
    {
        DeliveryReportReq req = getPDU();

        DeliveryReportRsp resp = req.reply();

        assertNotNull(resp);

        assertEquals("6.8.0",resp.getMm7Version());
        assertEquals(1000, resp.getStatusCode());
        assertEquals("Success",resp.getStatusText());
        assertEquals("3290", resp.getTransactionId());


    }
}
