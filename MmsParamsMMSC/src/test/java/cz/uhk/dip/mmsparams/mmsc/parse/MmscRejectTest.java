package cz.uhk.dip.mmsparams.mmsc.parse;

import net.instantcom.mm7.DeliveryReportReq;
import net.instantcom.mm7.MM7Context;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MM7Response;
import net.instantcom.mm7.SubmitRsp;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MmscRejectTest
{
    final String fileName = "AO_MMS_Reject_part2.txt";

    private SubmitRsp getPDU() throws IOException, MM7Error
    {
        String ct = "text/xml; charset=\"utf-8\"";

        InputStream in = DeliveryReportExpiredTest.class.getResourceAsStream(fileName);
        return (SubmitRsp) MM7Response.load(in, ct, new MM7Context());
    }

    @Test
    public void AO_MMS_Reject_part2_Test() throws IOException, MM7Error
    {
        SubmitRsp pdu = getPDU();
        assertNotNull(pdu);

        assertEquals(3002, pdu.getStatusCode());
        assertEquals("Message rejected", pdu.getStatusText());
    }
}
