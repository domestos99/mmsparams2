package cz.uhk.dip.mmsparams.mmsc.parse;

import net.instantcom.mm7.DeliveryReportReq;
import net.instantcom.mm7.MM7Context;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MM7Response;
import net.instantcom.mm7.SubmitReq;
import net.instantcom.mm7.SubmitRsp;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import cz.uhk.dip.mmsparams.api.enums.AddressType;
import cz.uhk.dip.mmsparams.api.enums.RecipientType;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7RelativeDate;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendConnectionModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendMmsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;
import cz.uhk.dip.mmsparams.mmsc.utils.MM7ConverterOut;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SendMmsOkTest
{
    final String fileNameReply = "AO_MMS_OK_Reply.txt";

    private SubmitRsp getPDUResp() throws IOException, MM7Error
    {
        String ct = "text/xml";

        InputStream in = SendMmsOkTest.class.getResourceAsStream(fileNameReply);
        SubmitRsp req = (SubmitRsp) MM7Response.load(in, ct, new MM7Context());
        return req;
    }

    @Test
    public void AO_MMS_OK_Test() throws IOException, MM7Error
    {
        MmscSendModel model = new MmscSendModel();

        MmscSendConnectionModel conn = new MmscSendConnectionModel();
        conn.setMm7Version("5.3.0");
        conn.setVasId("MAMP_test");
        conn.setVaspId("MAMP_test");


        MmscSendMmsModel mms = new MmscSendMmsModel();
        mms.setSenderAddress(new MM7Address("99991390408", AddressType.SHORT_CODE, RecipientType.FROM));
        mms.addRecipientBcc(new MM7Address("+420774562164", AddressType.NUMBER, RecipientType.BCC));
        mms.setExpiryDate(new MM7RelativeDate("P2D"));
        mms.setDeliveryReport(true);
        mms.setReadReply(false);
        mms.setSubject("Testovaci MMS");



        model.setMmscSendConnectionModel(conn);
        model.setMmscSendMmsModel(mms);
        SubmitReq submit = MM7ConverterOut.getSubmitReq(model);

        assertNotNull(submit);

//         assertEquals("5.3.0", submit.getMm7Version());
        assertEquals("MAMP_test", submit.getVaspId());
        assertEquals("MAMP_test", submit.getVasId());
        assertEquals("99991390408", submit.getSenderAddress().getAddress());
        assertEquals(1, submit.getRecipients().size());
        assertEquals("+420774562164", submit.getRecipients().get(0).getAddress());


        assertEquals("P2D", submit.getExpiryDate().toString());
        assertEquals(true, submit.getDeliveryReport());
        assertEquals(false, submit.getReadReply());
        assertEquals("Testovaci MMS", submit.getSubject());


    }

    @Test
    public void AO_MMS_OK_Resp_Test() throws IOException, MM7Error
    {
        SubmitRsp resp = getPDUResp();

        assertNotNull(resp);

        // assertEquals("6.8.0", resp.getMm7Version());
        assertEquals("020fa839.2013028102009534.brainstorm.co.uk", resp.getTransactionId());
        assertEquals(1000, resp.getStatusCode());
        assertEquals("Success", resp.getStatusText());
        assertEquals("3295_mmsc1", resp.getMessageId());


    }

}
