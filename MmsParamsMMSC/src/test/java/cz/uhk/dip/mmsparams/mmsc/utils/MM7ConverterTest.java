package cz.uhk.dip.mmsparams.mmsc.utils;

import net.instantcom.mm7.Address;
import net.instantcom.mm7.ChargedParty;
import net.instantcom.mm7.Priority;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import cz.uhk.dip.mmsparams.api.enums.AddressCoding;
import cz.uhk.dip.mmsparams.api.enums.AddressType;
import cz.uhk.dip.mmsparams.api.enums.MessageClass;
import cz.uhk.dip.mmsparams.api.enums.RecipientType;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.mmsc.UnitTestBase;

import static org.junit.Assert.*;

public class MM7ConverterTest extends UnitTestBase
{
    @Test
    public void getMessageClass_null_expectedNull()
    {
        assertNull(MM7Converter.getMessageClass(null));
    }

    @Test
    public void getMessageClass_all_Test()
    {
        assertEquals(net.instantcom.mm7.MessageClass.PERSONAL, MM7Converter.getMessageClass(MessageClass.PERSONAL));
        assertEquals(net.instantcom.mm7.MessageClass.PERSONAL, MM7Converter.getMessageClass(MessageClass.UNDEFINED));
        assertEquals(net.instantcom.mm7.MessageClass.ADVERTISMENT, MM7Converter.getMessageClass(MessageClass.ADVERTISEMENT));
        assertEquals(net.instantcom.mm7.MessageClass.INFORMATIONAL, MM7Converter.getMessageClass(MessageClass.INFORMATIONAL));
        assertEquals(net.instantcom.mm7.MessageClass.AUTO, MM7Converter.getMessageClass(MessageClass.AUTO));
    }

    @Test
    public void getPriority_all_Test()
    {
        assertNull((cz.uhk.dip.mmsparams.api.enums.Priority)null);
        assertEquals(Priority.LOW, MM7Converter.getPriority(cz.uhk.dip.mmsparams.api.enums.Priority.LOW));
        assertEquals(Priority.NORMAL, MM7Converter.getPriority(cz.uhk.dip.mmsparams.api.enums.Priority.NORMAL));
        assertEquals(Priority.HIGH, MM7Converter.getPriority(cz.uhk.dip.mmsparams.api.enums.Priority.HIGH));
    }

    @Test
    public void getPriority2_all_Test()
    {
        assertNull(MM7Converter.getPriority((Priority)null));
        assertEquals(cz.uhk.dip.mmsparams.api.enums.Priority.LOW, MM7Converter.getPriority(Priority.LOW));
        assertEquals(cz.uhk.dip.mmsparams.api.enums.Priority.NORMAL, MM7Converter.getPriority(Priority.NORMAL));
        assertEquals(cz.uhk.dip.mmsparams.api.enums.Priority.HIGH, MM7Converter.getPriority(Priority.HIGH));
    }

    @Test
    public void getChangedParty_all_Test()
    {
        assertNull(MM7Converter.getChangedParty(null));
        assertEquals(ChargedParty.SENDER, MM7Converter.getChangedParty(cz.uhk.dip.mmsparams.api.enums.ChargedParty.SENDER));
        assertEquals(ChargedParty.RECIPIENT, MM7Converter.getChangedParty(cz.uhk.dip.mmsparams.api.enums.ChargedParty.RECIPIENT));
        assertEquals(ChargedParty.BOTH, MM7Converter.getChangedParty(cz.uhk.dip.mmsparams.api.enums.ChargedParty.BOTH));
        assertEquals(ChargedParty.NEITHER, MM7Converter.getChangedParty(cz.uhk.dip.mmsparams.api.enums.ChargedParty.NEITHER));
        assertEquals(ChargedParty.THIRD_PARTY, MM7Converter.getChangedParty(cz.uhk.dip.mmsparams.api.enums.ChargedParty.THIRD_PARTY));
    }

    @Test
    public void getMM7Address_null_Test()
    {
        Address address = new Address("123456", Address.RecipientType.TO, Address.AddressType.NUMBER);
        address.setDisplayOnly(false);
        address.setAddressCoding(Address.AddressCoding.ENCRYPTED);
        address.setId("abc");
        MM7Address result = MM7Converter.getMM7Address(address);

        assertNotNull(result);

        assertEquals(address.getId(), result.getId());
        assertEquals(address.getAddress(), result.getAddress());
        assertEquals(RecipientType.TO, result.getRecipientType());
        assertEquals(AddressType.NUMBER, result.getAddressType());
        assertEquals(AddressCoding.ENCRYPTED, result.getAddressCoding());
        assertEquals(address.isDisplayOnly(), result.isDisplayOnly());
    }










}

