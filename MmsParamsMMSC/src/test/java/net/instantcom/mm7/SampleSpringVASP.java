package net.instantcom.mm7;

/**
 * <p>
 * sample use of VASP
 * </p>
 * Created by bardug on 6/26/2016.
 */
public class SampleSpringVASP implements VASP
{
    @Override
    public DeliverRsp deliver(DeliverReq deliverReq) throws MM7Error
    {
        System.out.println("deliver in VASP was called");

        return null;
    }

    @Override
    public MM7Context getContext()
    {
        return new MM7Context();
    }

    @Override
    public void handleMm7Error(MM7Error mm7error)
    {

    }

    @Override
    public DeliveryReportRsp deliveryReport(DeliveryReportReq deliveryReportReq) throws MM7Error
    {
        System.out.println("deliveryReport in VASP was called");

        return null;
    }

    @Override
    public ReadReplyRsp readReply(ReadReplyReq readReplyReq) throws MM7Error
    {
        System.out.println("readReplyReq in VASP was called");

        return null;
    }
}
