package cz.uhk.dip.mmsparams.smsc;

import com.cloudhopper.smpp.SmppClient;

public interface ISmppClientProvider
{
    SmppClient create();
}
