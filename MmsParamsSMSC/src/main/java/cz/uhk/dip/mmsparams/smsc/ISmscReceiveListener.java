package cz.uhk.dip.mmsparams.smsc;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;

public interface ISmscReceiveListener
{
    void onDeliverySm(final SmscDeliverSmModel deliverSmModel);

    void onDeliveryReport(final SmscDeliveryReportModel deliveryReportModel);
}
