package cz.uhk.dip.mmsparams.smsc;

import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.type.RecoverablePduException;
import com.cloudhopper.smpp.type.SmppChannelException;
import com.cloudhopper.smpp.type.SmppTimeoutException;
import com.cloudhopper.smpp.type.UnrecoverablePduException;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;

public interface ISmscService
{
    SmppSession connect(final SmscConnectModel smppConnectModel, final ISmscReceiveListener iSmppReceiveListener) throws UnrecoverablePduException, SmppChannelException, InterruptedException, SmppTimeoutException;

    SmscSendSmsResponseModel sendSingleSMS(final SmppSession smppSession, final SmscSendModel smppSendSmsModel) throws InterruptedException, SmppChannelException, SmppTimeoutException, UnrecoverablePduException, RecoverablePduException;

    void disconnect(final SmppSession smppSession);
}
