package cz.uhk.dip.mmsparams.smsc;

import com.cloudhopper.smpp.PduAsyncResponse;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.impl.DefaultSmppSessionHandler;
import com.cloudhopper.smpp.pdu.DeliverSm;
import com.cloudhopper.smpp.pdu.Pdu;
import com.cloudhopper.smpp.pdu.PduRequest;
import com.cloudhopper.smpp.pdu.PduResponse;
import com.cloudhopper.smpp.type.RecoverablePduException;
import com.cloudhopper.smpp.type.UnrecoverablePduException;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.smsc.logging.SMSCLoggerFacade;
import cz.uhk.dip.mmsparams.smsc.utils.SmscConverterIn;

public class MySmscSessionHandler extends DefaultSmppSessionHandler
{
    private final ISmscReceiveListener iSmscReceiveListener;
    private static final String TAG = MySmscSessionHandler.class.getSimpleName();

    public MySmscSessionHandler(final ISmscReceiveListener iSmscReceiveListener)
    {
        super();
        this.iSmscReceiveListener = iSmscReceiveListener;
    }

    @Override
    public void firePduRequestExpired(PduRequest pduRequest)
    {
        super.firePduRequestExpired(pduRequest);
    }

    @Override
    public PduResponse firePduRequestReceived(PduRequest pduRequest)
    {
        PduResponse response = pduRequest.createResponse();

        try
        {
            // deliver_sm - prijem nove zpravy
            if (pduRequest.getCommandId() == SmppConstants.CMD_ID_DELIVER_SM || pduRequest.getCommandId() == 5)
            {
                DeliverSm deliverSm = (DeliverSm) pduRequest;

                if (deliverSm.getEsmClass() == SmppConstants.ESM_CLASS_MT_SMSC_DELIVERY_RECEIPT)
                {
                    SmscDeliveryReportModel dr = SmscConverterIn.getSmscDeliveryReportModel(deliverSm);
                    SMSCLoggerFacade.logInfo(TAG, "Received new delivery report: " + dr);
                    iSmscReceiveListener.onDeliveryReport(dr);
                }
                else
                {
                    SmscDeliverSmModel sms = SmscConverterIn.getSmscDeliverySmModel(deliverSm);
                    SMSCLoggerFacade.logInfo(TAG, "Received new sms message: " + sms);
                    iSmscReceiveListener.onDeliverySm(sms);
                }
            }
        }
        catch (Exception e)
        {
            SMSCLoggerFacade.logEx(TAG, e);
        }

        // deliver_sm_resp - moje odpoved, kdyz mi prijde delivery report - asi neni potreba
//        if (response.getCommandId() == SmppConstants.CMD_ID_DELIVER_SM_RESP || response.getCommandId() == -2147483643)
//        {
//            DeliverSmResp deliverSmResp = (DeliverSmResp) response;
//        }


        return response;
    }

    @Override
    public boolean firePduReceived(Pdu pdu)
    {
        // bind_tranceiver_resp
        if (pdu.getCommandId() == SmppConstants.CMD_ID_BIND_TRANSCEIVER_RESP || pdu.getCommandId() == -2147483639)
        {

        }
        // deliver_sm
        if (pdu.getCommandId() == SmppConstants.CMD_ID_DELIVER_SM || pdu.getCommandId() == 5)
        {

        }        // submit_sm_resp

        // submit_sm_resp
        return super.firePduReceived(pdu);
    }


    @Override
    public String lookupResultMessage(int commandStatus)
    {
        // Should be 0
        return super.lookupResultMessage(commandStatus);
    }

    @Override
    public String lookupTlvTagName(short tag)
    {
        return super.lookupTlvTagName(tag);
    }

    @Override
    public void fireChannelUnexpectedlyClosed()
    {
        SMSCLoggerFacade.logWarning(TAG, "fireChannelUnexpectedlyClosed");
        super.fireChannelUnexpectedlyClosed();
    }

    @Override
    public void fireExpectedPduResponseReceived(PduAsyncResponse pduAsyncResponse)
    {
        super.fireExpectedPduResponseReceived(pduAsyncResponse);
    }

    @Override
    public void fireUnexpectedPduResponseReceived(PduResponse pduResponse)
    {
        super.fireUnexpectedPduResponseReceived(pduResponse);
    }

    @Override
    public void fireUnrecoverablePduException(UnrecoverablePduException e)
    {
        super.fireUnrecoverablePduException(e);
    }

    @Override
    public void fireRecoverablePduException(RecoverablePduException e)
    {
        super.fireRecoverablePduException(e);
    }

    @Override
    public void fireUnknownThrowable(Throwable t)
    {
        SMSCLoggerFacade.logEx(TAG, "fireUnknownThrowable", t);
        super.fireUnknownThrowable(t);
    }

    @Override
    public boolean firePduDispatch(Pdu pdu)
    {
        // bind_transmitter
        // submit_sm
        // deliver_sm_resp from web
        // unbind
        return super.firePduDispatch(pdu);
    }


}
