package cz.uhk.dip.mmsparams.smsc;

import com.cloudhopper.smpp.SmppClient;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.pdu.SubmitSmResp;
import com.cloudhopper.smpp.type.RecoverablePduException;
import com.cloudhopper.smpp.type.SmppChannelException;
import com.cloudhopper.smpp.type.SmppTimeoutException;
import com.cloudhopper.smpp.type.UnrecoverablePduException;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.smsc.logging.SMSCLoggerFacade;
import cz.uhk.dip.mmsparams.smsc.utils.SmscConverterIn;
import cz.uhk.dip.mmsparams.smsc.utils.SmscConverterOut;

public class SmscService implements ISmscService
{
    private static final String TAG = SmscService.class.getSimpleName();
    private final ISmppClientProvider iSmppClientProvider;

    public SmscService(final ISmppClientProvider iSmppClientProvider)
    {
        this.iSmppClientProvider = iSmppClientProvider;
    }

    @Override
    public SmppSession connect(final SmscConnectModel smppConnectModel, final ISmscReceiveListener iSmppReceiveListener) throws UnrecoverablePduException, SmppChannelException, InterruptedException, SmppTimeoutException
    {
        SmppClient smppClient = iSmppClientProvider.create();

        SmppSessionConfiguration config0 = SmscConverterOut.getSmppSessionConfiguration(smppConnectModel);

        try
        {
            SMSCLoggerFacade.logInfo(TAG, "Connection to SMSC with: " + smppConnectModel.toString());

            SmppSession session = smppClient.bind(config0, new MySmscSessionHandler(iSmppReceiveListener));

            SMSCLoggerFacade.logInfo(TAG, session, "SMSC bind success");

            return session;
        }
        catch (Exception e)
        {
            SMSCLoggerFacade.logEx(TAG, e);
            throw e;
        }
    }

    @Override
    public SmscSendSmsResponseModel sendSingleSMS(final SmppSession smppSession, final SmscSendModel smppSendSmsModel) throws InterruptedException, SmppChannelException, SmppTimeoutException, UnrecoverablePduException, RecoverablePduException
    {
        try
        {
            SubmitSm submit0 = SmscConverterOut.getSubmitSm(smppSendSmsModel);

            SMSCLoggerFacade.logInfo(TAG, smppSession, "Submiting message: " + smppSendSmsModel);

            SubmitSmResp submitResp = smppSession.submit(submit0, 10000);

            SMSCLoggerFacade.logInfo(TAG, smppSession, "SMS message submited success");

            return SmscConverterIn.getSubmitResponseModel(submitResp);
        }
        catch (Exception e)
        {
            SMSCLoggerFacade.logEx(TAG, e);
            throw e;
        }
    }

    @Override
    public void disconnect(final SmppSession smppSession)
    {
        if (smppSession == null)
        {
            return;
        }
        SMSCLoggerFacade.logInfo(TAG, smppSession, "SMSC session disconnecting");
        smppSession.unbind(1000);
        SMSCLoggerFacade.logInfo(TAG, "SMSC session disconnected");
    }

}
