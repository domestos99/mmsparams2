package cz.uhk.dip.mmsparams.smsc;

import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.type.Address;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.SmscConnectType;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscAddressModel;

public class SmscUtils
{
    public static SmppBindType convertConnType(final SmscConnectType connType)
    {
        if (connType == null)
            return SmppBindType.TRANSCEIVER;

        switch (connType)
        {
            case TRANSCEIVER:
                return SmppBindType.TRANSCEIVER;
            case TRANSMITTER:
                return SmppBindType.TRANSMITTER;
            case RECEIVER:
                return SmppBindType.RECEIVER;
            default:
                return SmppBindType.TRANSCEIVER;
        }
    }

    public static Address getAddress(final SmscAddressModel senderAddress)
    {
        return new Address(senderAddress.getTypeOfNumber().value(), senderAddress.getNumberingPlanIndicator().value(), senderAddress.getAddress());
    }

    public static SmscAddressModel getAddress(final Address address)
    {
        return new SmscAddressModel(TypeOfNumber.valueOf(address.getTon()), NumberingPlanIndicator.valueOf(address.getNpi()), address.getAddress());
    }

    public static byte[] getShortMessage(String shortMessage)
    {
        if (shortMessage == null)
            return null;
        return shortMessage.getBytes();
    }

    public static byte[] getShortMessageUCS2(String shortMessage)
    {
        if (shortMessage == null)
            return null;
        return shortMessage.getBytes(StandardCharsets.UTF_16);
    }


}
