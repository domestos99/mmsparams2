package cz.uhk.dip.mmsparams.smsc.logging;

import com.cloudhopper.smpp.SmppSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.uhk.dip.mmsparams.api.utils.LogUtil;
import cz.uhk.dip.mmsparams.smsc.utils.SmscConverterIn;

public class SMSCLoggerFacade
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SMSCLoggerFacade.class);

    private SMSCLoggerFacade()
    {
    }

    public static void logInfo(String tag, SmppSession smppSession, String info)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(SmscConverterIn.getSmscSessionModel(smppSession));
        sb.append(LogUtil.NEW_LINE);
        sb.append(getMessage(tag, info));
        LOGGER.info(sb.toString());
    }

    public static void logInfo(String tag, String info)
    {
        LOGGER.info(getMessage(tag, info));
    }

    public static void logWarning(String tag, String warning)
    {
        LOGGER.warn(getMessage(tag, warning));
    }

    public static void logEx(String tag, Exception ex)
    {
        LOGGER.error(tag, ex);
        SMSCLoggerFatalFacade.logEx(tag, ex);
    }

    public static void logEx(String tag, String message, Throwable ex)
    {
        LOGGER.error(getMessage(tag, message), ex);
        SMSCLoggerFatalFacade.logEx(tag, message, ex);
    }

    private static String getMessage(String tag, String message)
    {
        return tag + ": " + message;
    }


}
