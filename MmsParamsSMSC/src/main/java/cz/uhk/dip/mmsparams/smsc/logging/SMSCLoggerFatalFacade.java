package cz.uhk.dip.mmsparams.smsc.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SMSCLoggerFatalFacade
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SMSCLoggerFatalFacade.class);

    private SMSCLoggerFatalFacade()
    {
    }

    public static void logEx(String tag, Exception ex)
    {
        LOGGER.error(tag, ex);
    }

    public static void logEx(String tag, String message, Throwable ex)
    {
        LOGGER.error(getMessage(tag, message), ex);
    }

    private static String getMessage(String tag, String message)
    {
        return tag + ": " + message;
    }


}
