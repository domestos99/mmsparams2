package cz.uhk.dip.mmsparams.smsc.utils;

import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.pdu.DeliverSm;
import com.cloudhopper.smpp.pdu.Pdu;
import com.cloudhopper.smpp.pdu.SubmitSmResp;
import com.cloudhopper.smpp.tlv.Tlv;

import java.util.ArrayList;

import javax.annotation.Nonnull;

import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscModelBase;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.TlvModel;
import cz.uhk.dip.mmsparams.smsc.SmscUtils;
import cz.uhk.dip.mmsparams.smsc.logging.SMSCLoggerFacade;

public class SmscConverterIn
{
    private static final String TAG = SmscConverterIn.class.getSimpleName();

    private SmscConverterIn()
    {
    }

    public static SmscSendSmsResponseModel getSubmitResponseModel(final SubmitSmResp submitResp)
    {
        SmscSendSmsResponseModel model = new SmscSendSmsResponseModel();

        // Header
        setHeader(model, submitResp);

        model.setCommandStatus(submitResp.getCommandStatus());
        model.setName(submitResp.getName());
        model.setCommandId(submitResp.getCommandId());
        model.setCommandLength(submitResp.getCommandLength());
        model.setMessageId(submitResp.getMessageId());
        model.setResultMessage(submitResp.getResultMessage());
        model.setSequenceNumber(submitResp.getSequenceNumber());

        return model;
    }

    @Nonnull
    private static <T extends SmscModelBase> T setHeader(@Nonnull final T model, @Nonnull final Pdu pdu)
    {
        model.setName(pdu.getName());
        model.setCommandLength(pdu.getCommandLength());
        model.setCommandId(pdu.getCommandId());
        model.setCommandStatus(pdu.getCommandStatus());
        model.setSequenceNumber(pdu.getSequenceNumber());
        model.setRequest(pdu.isRequest());
        model.setResponse(pdu.isResponse());
        model.setOptionalParameters(convertTlv(pdu.getOptionalParameters()));

        return model;
    }

    private static ArrayList<TlvModel> convertTlv(ArrayList<Tlv> optionalParameters)
    {
        ArrayList<TlvModel> result = new ArrayList<>();

        if (optionalParameters == null || optionalParameters.isEmpty())
            return result;

        for (Tlv tlv : optionalParameters)
        {
            TlvModel tlvModel = new TlvModel();
            tlvModel.setTag(tlv.getTag());
            tlvModel.setTagName(tlv.getTagName());
            tlvModel.setValue(tlv.getValue());
            try
            {
                tlvModel.setValueString(new String(tlv.getValue()));
            }
            catch (Exception e)
            {
                SMSCLoggerFacade.logEx(TAG, "convertTlv", e);
            }
            result.add(tlvModel);
        }
        return result;
    }

    public static SmscDeliverSmModel getSmscDeliverySmModel(DeliverSm pduRequest)
    {
        SmscDeliverSmModel model = new SmscDeliverSmModel();

        // Header
        setHeader(model, pduRequest);

        model.setShortMessageLength(pduRequest.getShortMessageLength());
        model.setShortMessage(pduRequest.getShortMessage());
        model.setReplaceIfPresent(pduRequest.getReplaceIfPresent());
        model.setDataCoding(pduRequest.getDataCoding());
        model.setDefaultMsgId(pduRequest.getDefaultMsgId());
        model.setRegisteredDelivery(pduRequest.getRegisteredDelivery());
        model.setValidityPeriod(pduRequest.getValidityPeriod());
        model.setScheduleDeliveryTime(pduRequest.getScheduleDeliveryTime());
        model.setPriority(pduRequest.getPriority());
        model.setEsmClass(pduRequest.getEsmClass());
        model.setProtocolId(pduRequest.getProtocolId());
        model.setServiceType(pduRequest.getServiceType());
        model.setSourceAddress(SmscUtils.getAddress(pduRequest.getSourceAddress()));
        model.setDestAddress(SmscUtils.getAddress(pduRequest.getDestAddress()));

        return model;
    }

    public static SmscDeliveryReportModel getSmscDeliveryReportModel(DeliverSm pduRequest)
    {
        SmscDeliveryReportModel model = new SmscDeliveryReportModel();

        // Header
        setHeader(model, pduRequest);

        model.setShortMessageLength(pduRequest.getShortMessageLength());
        model.setShortMessage(pduRequest.getShortMessage());
        model.setReplaceIfPresent(pduRequest.getReplaceIfPresent());
        model.setDataCoding(pduRequest.getDataCoding());
        model.setDefaultMsgId(pduRequest.getDefaultMsgId());
        model.setRegisteredDelivery(pduRequest.getRegisteredDelivery());
        model.setValidityPeriod(pduRequest.getValidityPeriod());
        model.setScheduleDeliveryTime(pduRequest.getScheduleDeliveryTime());
        model.setPriority(pduRequest.getPriority());
        model.setEsmClass(pduRequest.getEsmClass());
        model.setProtocolId(pduRequest.getProtocolId());
        model.setServiceType(pduRequest.getServiceType());
        model.setSourceAddress(SmscUtils.getAddress(pduRequest.getSourceAddress()));
        model.setDestAddress(SmscUtils.getAddress(pduRequest.getDestAddress()));


        return model;
    }

    public static SmscSessionModel getSmscSessionModel(SmppSession session)
    {
        SmscSessionModel model = new SmscSessionModel();

        model.setBoundTime(session.getBoundTime());
        model.setInterfaceVersion(session.getInterfaceVersion());
        model.setLocalType(StringUtil.getStringSafe(session.getLocalType()));
        model.setRemoteType(StringUtil.getStringSafe(session.getRemoteType()));

        if (session.getConfiguration() != null)
        {
            SmppSessionConfiguration conf = session.getConfiguration();

            model.setAddressRange(StringUtil.getStringSafe(conf.getAddressRange()));
            model.setBindTimeout(conf.getBindTimeout());
            model.setConnectTimeout(conf.getConnectTimeout());
            model.setHost(conf.getHost());
            model.setInterfaceVersion(conf.getInterfaceVersion());
            model.setName(conf.getName());
            model.setPassword(conf.getPassword());
            model.setPort(conf.getPort());
            model.setRequestExpiryTimeout(conf.getRequestExpiryTimeout());
            model.setSystemId(conf.getSystemId());
            model.setSystemType(conf.getSystemType());
            model.setType(StringUtil.getStringSafe(conf.getType()));
            model.setWindowMonitorInterval(conf.getWindowMonitorInterval());
            model.setWindowSize(conf.getWindowSize());
            model.setWindowWaitTimeout(conf.getWindowWaitTimeout());
            model.setWriteTimeout(conf.getWriteTimeout());
            model.setCountersEnabled(conf.isCountersEnabled());
            model.setUseSsl(conf.isUseSsl());
            // Ignored:
            //dto.setloggingOptions(conf.getLoggingOptions());
            // dto.setsslConfiguration(conf.getSslConfiguration());
        }

        return model;
    }
}
