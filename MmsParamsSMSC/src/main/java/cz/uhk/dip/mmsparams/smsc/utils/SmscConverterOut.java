package cz.uhk.dip.mmsparams.smsc.utils;

import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.tlv.Tlv;
import com.cloudhopper.smpp.type.Address;
import com.cloudhopper.smpp.type.SmppInvalidArgumentException;

import java.util.ArrayList;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.TlvModel;
import cz.uhk.dip.mmsparams.smsc.SmscUtils;

public class SmscConverterOut
{
    private SmscConverterOut()
    {
    }

    public static SmppSessionConfiguration getSmppSessionConfiguration(final SmscConnectModel smppConnectModel)
    {
        SmppSessionConfiguration config0 = new SmppSessionConfiguration();
        config0.setHost(smppConnectModel.getHost());
        config0.setPort(smppConnectModel.getPort());
        config0.setSystemId(smppConnectModel.getSystemId());
        config0.setPassword(smppConnectModel.getPassword());
        config0.setType(SmscUtils.convertConnType(smppConnectModel.getConnType()));

        config0.setSystemType(smppConnectModel.getSystemType());

        config0.setConnectTimeout(smppConnectModel.getConnectTimeout());
        config0.getLoggingOptions().setLogBytes(smppConnectModel.getLogBytes());

        // to enable monitoring (request expiration)
        config0.setRequestExpiryTimeout(smppConnectModel.getRequestExpiryTimeout());
        config0.setWindowMonitorInterval(smppConnectModel.getWindowMonitorInterval());
        config0.setCountersEnabled(smppConnectModel.getCountersEnabled());

        return config0;
    }

    public static SubmitSm getSubmitSm(SmscSendModel smppSendSmsModel) throws SmppInvalidArgumentException
    {
        SubmitSm submit0 = new SubmitSm();

        Address srcAddr = SmscUtils.getAddress(smppSendSmsModel.getSenderAddress());
        Address destAddr = SmscUtils.getAddress(smppSendSmsModel.getRecipientAddress());

        submit0.setSourceAddress(srcAddr);
        submit0.setDestAddress(destAddr);

        if (smppSendSmsModel.isDeliveryReport())
        {
            submit0.setRegisteredDelivery(SmppConstants.REGISTERED_DELIVERY_SMSC_RECEIPT_REQUESTED);
        }
        else
        {
            submit0.setRegisteredDelivery(SmppConstants.REGISTERED_DELIVERY_SMSC_RECEIPT_NOT_REQUESTED);
        }

        for (Tlv tlv : convertTlv(smppSendSmsModel.getOptionalParameters()))
        {
            submit0.setOptionalParameter(tlv);
        }

        if (smppSendSmsModel.isUseUCS2())
        {
            byte[] shortMessageBytes = SmscUtils.getShortMessageUCS2(smppSendSmsModel.getShortMessage());
            submit0.setShortMessage(shortMessageBytes);
            submit0.setDataCoding(SmppConstants.DATA_CODING_UCS2);
        }
        else
        {
            byte[] shortMessageBytes = SmscUtils.getShortMessage(smppSendSmsModel.getShortMessage());
            submit0.setShortMessage(shortMessageBytes);
        }


        return submit0;
    }


    private static ArrayList<Tlv> convertTlv(ArrayList<TlvModel> optionalParameters)
    {
        ArrayList<Tlv> result = new ArrayList<>();

        if (optionalParameters == null || optionalParameters.isEmpty())
            return result;

        for (TlvModel tlv : optionalParameters)
        {
            Tlv tlvModel = new Tlv(tlv.getTag(), tlv.getValue(), tlv.getTagName());
            result.add(tlvModel);
        }
        return result;
    }


}
