//package cz.uhk.dip.mmsparams.smpp;
//
//import com.cloudhopper.smpp.type.RecoverablePduException;
//import com.cloudhopper.smpp.type.SmppChannelException;
//import com.cloudhopper.smpp.type.SmppTimeoutException;
//import com.cloudhopper.smpp.type.UnrecoverablePduException;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import cz.uhk.dip.mmsparams.enums.smpp.NumberingPlanIndicator;
//import cz.uhk.dip.mmsparams.enums.smpp.SmppConnectType;
//import cz.uhk.dip.mmsparams.enums.smpp.TypeOfNumber;
//import cz.uhk.dip.mmsparams.websocket.model.smpp.SmppAddress;
//import cz.uhk.dip.mmsparams.websocket.model.smpp.SmppConnectModel;
//import cz.uhk.dip.mmsparams.websocket.model.smpp.SmppDeliverSmModel;
//import cz.uhk.dip.mmsparams.websocket.model.smpp.SmppDeliveryReportModel;
//import cz.uhk.dip.mmsparams.websocket.model.smpp.SmppDisconnectModel;
//import cz.uhk.dip.mmsparams.websocket.model.smpp.SmppSendSmsModel;
//import cz.uhk.dip.mmsparams.websocket.model.smpp.SmppSendSmsResponseModel;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertTrue;
//
//public class SmppServiceTest
//{
//    private ISmppService service;
//
//    @Before // setup()
//    public void before() throws Exception
//    {
//        service = new SmppService();
//    }
//
//    @After // tearDown()
//    public void after() throws Exception
//    {
//        if (service != null)
//        {
//            SmppDisconnectModel smppDisconnectModel = new SmppDisconnectModel();
//
//            service.disconnect(smppDisconnectModel);
//        }
//    }
//
//
//    @Test
//    public void connectTest() throws InterruptedException, SmppChannelException, UnrecoverablePduException, SmppTimeoutException
//    {
//        service = new SmppService();
//
//        SmppConnectModel smppConnectModel = getConModel();
//
//        service.connect(smppConnectModel);
//
//        Thread.sleep(1000);
//
//        SmppDisconnectModel smppDisconnectModel = new SmppDisconnectModel();
//
//        service.disconnect(smppDisconnectModel);
//    }
//
//    @Test
//    public void disconnect1Test() throws InterruptedException, SmppChannelException, UnrecoverablePduException, SmppTimeoutException
//    {
//        service = new SmppService();
//
//        SmppConnectModel smppConnectModel = getConModel();
//
//        service.connect(smppConnectModel);
//
//        Thread.sleep(1000);
//
//        SmppDisconnectModel smppDisconnectModel = new SmppDisconnectModel();
//        service.disconnect(smppDisconnectModel);
//    }
//
//
//    @Test
//    public void disconnect2Test() throws InterruptedException, SmppChannelException, UnrecoverablePduException, SmppTimeoutException
//    {
//        service = new SmppService();
//
//        SmppConnectModel smppConnectModel = getConModel();
//
//        service.connect(smppConnectModel);
//
//        Thread.sleep(1000);
//
//        SmppDisconnectModel smppDisconnectModel = new SmppDisconnectModel();
//        service.disconnect(smppDisconnectModel);
//        service.disconnect(smppDisconnectModel);
//    }
//
//    @Test
//    public void disconnect3Test() throws InterruptedException, SmppChannelException, UnrecoverablePduException, SmppTimeoutException
//    {
//        service = new SmppService();
//        SmppDisconnectModel smppDisconnectModel = new SmppDisconnectModel();
//        service.disconnect(smppDisconnectModel);
//    }
//
//
//    @Test
//    public void sendSms1Test() throws InterruptedException, SmppChannelException, UnrecoverablePduException, SmppTimeoutException, RecoverablePduException
//    {
//        service = new SmppService();
//
//        SmppConnectModel smppConnectModel = getConModel();
//
//        service.connect(smppConnectModel);
//
//        Thread.sleep(1000);
//
//        SmppSendSmsModel smsModel = getSendSms();
//
//        SmppSendSmsResponseModel resp = service.sendSingleSMS(smsModel);
//
//        assertResponse(resp);
//
//        Thread.sleep(1000);
//
//        SmppDisconnectModel smppDisconnectModel = new SmppDisconnectModel();
//
//        service.disconnect(smppDisconnectModel);
//
//    }
//
//
//    @Test
//    public void sendSms2Test() throws InterruptedException, SmppChannelException, UnrecoverablePduException, SmppTimeoutException, RecoverablePduException
//    {
//        service = new SmppService();
//
//        SmppConnectModel smppConnectModel = getConModel();
//
//        service.connect(smppConnectModel);
//
//        Thread.sleep(1000);
//
//        SmppSendSmsModel smsModel = getSendSms();
//
//        SmppSendSmsResponseModel resp = service.sendSingleSMS(smsModel);
//
//        assertResponse(resp);
//
//        Thread.sleep(1000);
//
//        resp = service.sendSingleSMS(smsModel);
//
//        assertResponse(resp);
//
//        Thread.sleep(1000);
//
//        SmppDisconnectModel smppDisconnectModel = new SmppDisconnectModel();
//
//        service.disconnect(smppDisconnectModel);
//    }
//
//
//    @Test
//    public void sendSms1DRTest() throws InterruptedException, SmppChannelException, UnrecoverablePduException, SmppTimeoutException, RecoverablePduException
//    {
//        service = new SmppService();
//
//        SmppConnectModel smppConnectModel = getConModel();
//
//        final boolean[] deliveryReceived = {false};
//
//        ISmppReceiveListener iSmppReceiveListener = new ISmppReceiveListener()
//        {
//            @Override
//            public void onDeliverySm(SmppDeliverSmModel deliverSmModel)
//            {
//                deliveryReceived[0] = true;
//            }
//
//            @Override
//            public void onDeliveryReport(SmppDeliveryReportModel deliveryReportModel)
//            {
//                deliveryReceived[0] = true;
//            }
//        };
//
//        service.connect(smppConnectModel, iSmppReceiveListener);
//
//        Thread.sleep(1000);
//
//        SmppSendSmsModel smsModel = getSendSms();
//
//        SmppSendSmsResponseModel resp = service.sendSingleSMS(smsModel);
//
////        assertResponse(resp);
//
//        Thread.sleep(2000);
//
//        assertTrue(deliveryReceived[0]);
//
//        SmppDisconnectModel smppDisconnectModel = new SmppDisconnectModel();
//
//        service.disconnect(smppDisconnectModel);
//    }
//
//
//    private void assertResponse(final SmppSendSmsResponseModel resp)
//    {
//        assertNotNull(resp);
//        assertEquals("OK", resp.getResultMessage());
//        assertEquals(-2147483644, resp.getCommandId()); // 80000004‬
//        assertEquals(0, resp.getCommandStatus());
//    }
//
//    private SmppSendSmsModel getSendSms()
//    {
//        SmppSendSmsModel smppSendSmsModel = new SmppSendSmsModel();
//
//        smppSendSmsModel.setShortMessage("hello world");
//        smppSendSmsModel.setSenderAddress(new SmppAddress(TypeOfNumber.UNKNOWN, NumberingPlanIndicator.UNKNOWN, "123"));
//        smppSendSmsModel.setRecipientAddress(new SmppAddress(TypeOfNumber.UNKNOWN, NumberingPlanIndicator.UNKNOWN, "456"));
//
//        return smppSendSmsModel;
//    }
//
//    private SmppConnectModel getConModel()
//    {
//        SmppConnectModel smppConnectModel = new SmppConnectModel();
//
//        smppConnectModel.setHost("127.0.0.1");
//        smppConnectModel.setPort(2775);
//        smppConnectModel.setSystemId("smppclient1");
//        smppConnectModel.setPassword("password");
//        smppConnectModel.setConnType(SmppConnectType.TRANSCEIVER);
//        return smppConnectModel;
//    }
//
//}