package cz.uhk.dip.mmsparams.smsc;

import com.cloudhopper.smpp.SmppClient;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.type.SmppChannelException;
import com.cloudhopper.smpp.type.SmppTimeoutException;
import com.cloudhopper.smpp.type.UnrecoverablePduException;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.smsc.mock.MockSmppClient;
import cz.uhk.dip.mmsparams.smsc.mock.MockSmppSession;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SmscServiceTest extends UnitTestBase
{
    private ISmscService iSmscService;

    @Before
    public void setUp() throws Exception
    {
        iSmscService = new SmscService(new ISmppClientProvider()
        {
            @Override
            public SmppClient create()
            {
                return new MockSmppClient();
            }
        });
    }

    @Test
    public void connectTest() throws UnrecoverablePduException, SmppChannelException, InterruptedException, SmppTimeoutException
    {
        final SmscConnectModel smppConnectModel = new SmscConnectModel();
        final ISmscReceiveListener iSmppReceiveListener = new ISmscReceiveListener()
        {
            @Override
            public void onDeliverySm(SmscDeliverSmModel deliverSmModel)
            {

            }

            @Override
            public void onDeliveryReport(SmscDeliveryReportModel deliveryReportModel)
            {

            }
        };

        SmppSession session = this.iSmscService.connect(smppConnectModel, iSmppReceiveListener);

        MockSmppSession ses = (MockSmppSession) session;

        assertTrue(ses.isBound());
        ses.getBindType();
        ses.getBoundTime();
        ses.getConfiguration();
        ses.getCounters();
        ses.getInterfaceVersion();
        ses.getLocalType();
        ses.getRemoteType();
        ses.getStateName();

        assertFalse(ses.isBinding());
        assertTrue(ses.isBound());
        assertFalse(ses.isClosed());
        assertFalse(ses.isOpen());
        assertFalse(ses.isUnbinding());

    }

    @Test
    public void sendSingleSMSTest()
    {
    }

    @Test
    public void disconnectTest()
    {
//        final SmppSession smppSession = getMockSession();
//        iSmscService.disconnect(smppSession);
    }


}
