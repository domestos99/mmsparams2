package cz.uhk.dip.mmsparams.smsc;

import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.type.Address;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.SmscConnectType;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscAddressModel;

import static org.junit.Assert.assertEquals;

public class SmscUtilsTest extends UnitTestBase
{

    @Test
    public void convertConnTypeTest()
    {
        assertEquals(SmppBindType.RECEIVER, SmscUtils.convertConnType(SmscConnectType.RECEIVER));
        assertEquals(SmppBindType.TRANSMITTER, SmscUtils.convertConnType(SmscConnectType.TRANSMITTER));
        assertEquals(SmppBindType.TRANSCEIVER, SmscUtils.convertConnType(SmscConnectType.TRANSCEIVER));
        assertEquals(SmppBindType.TRANSCEIVER, SmscUtils.convertConnType(null));
    }

    @Test
    public void getAddressTest()
    {
        SmscAddressModel addressModel = new SmscAddressModel(TypeOfNumber.TON_INTERNATIONAL, NumberingPlanIndicator.NPI_NATIONAL, "123456");
        Address adr = SmscUtils.getAddress(addressModel);

        assertEquals(addressModel.getTypeOfNumber().value(), adr.getTon());
        assertEquals(addressModel.getNumberingPlanIndicator().value(), adr.getNpi());
        assertEquals(addressModel.getAddress(), adr.getAddress());
    }

    @Test
    public void getAddress1Test()
    {
        Address address = new Address(SmppConstants.TON_INTERNATIONAL, SmppConstants.NPI_NATIONAL, "123456");

        SmscAddressModel adr = new SmscUtils().getAddress(address);

        assertEquals(address.getTon(), adr.getTypeOfNumber().value());
        assertEquals(address.getNpi(), adr.getNumberingPlanIndicator().value());
        assertEquals(address.getAddress(), adr.getAddress());

    }
}
