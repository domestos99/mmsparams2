package cz.uhk.dip.mmsparams.smsc.mock;

import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.SmppSessionHandler;
import com.cloudhopper.smpp.impl.DefaultSmppServer;
import com.cloudhopper.smpp.impl.DefaultSmppSession;
import com.cloudhopper.smpp.pdu.BaseBindResp;

import org.jboss.netty.channel.Channel;

import java.util.concurrent.ScheduledExecutorService;

public class MockSmppSession extends DefaultSmppSession
{
    private final SmppSessionConfiguration smppConfiguration;

    public MockSmppSession(Type localType, SmppSessionConfiguration configuration, Channel channel, DefaultSmppServer server, Long serverSessionId, BaseBindResp preparedBindResponse, byte interfaceVersion, ScheduledExecutorService monitorExecutor)
    {
        super(localType, configuration, channel, server, serverSessionId, preparedBindResponse, interfaceVersion, monitorExecutor);
        smppConfiguration = configuration;
    }

    public MockSmppSession(Type localType, SmppSessionConfiguration configuration, Channel channel, SmppSessionHandler sessionHandler)
    {
        super(localType, configuration, channel, sessionHandler);
        smppConfiguration = configuration;
    }

    public MockSmppSession(Type localType, SmppSessionConfiguration configuration, Channel channel, SmppSessionHandler sessionHandler, ScheduledExecutorService monitorExecutor)
    {
        super(localType, configuration, channel, sessionHandler, monitorExecutor);
        smppConfiguration = configuration;
    }


    public void doBind(SmppSessionConfiguration config, DefaultSmppSession session)
    {
        setBound();
    }

    public SmppSessionConfiguration getSmppConfiguration()
    {
        return smppConfiguration;
    }
}
