import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {NavigationComponent} from '@app/layouts/secure/navigation/navigation.component';
import {ClientsComponent} from '@app/modules/secure/clients/clients.component';
import {PhonesComponent} from '@app/modules/secure/phones/phones.component';
import {SessionsComponent} from '@app/modules/secure/sessions/sessions.component';
import {CoreModule} from '@app/core';
import {SharedModule} from '@app/shared';
import {RouterModule} from '@angular/router';
import {AuthenticationService} from '@app/core/auth';
import {ErrorInterceptor, LoggerInterceptor, TokenInterceptor} from '@app/core/interceptors';
import {AppRoutingModule} from '@app/app.routing';
import {ClientsDetailComponent} from '@app/modules/secure/clients/detail/clients-detail.component';
import {PhonesDetailComponent} from '@app/modules/secure/phones/detail/phones-detail.component';
import {PhonesListComponent} from '@app/modules/secure/phones/list/phones-list.component';
import {ClientsListComponent} from '@app/modules/secure/clients/list/clients-list.component';
import {SessionsListComponent} from '@app/modules/secure/sessions/list/sessions-list.component';
import {SessionsDetailComponent} from '@app/modules/secure/sessions/detail/sessions-detail.component';
import {TestinstancesComponent} from '@app/modules/secure/testinstances/testinstances.component';
import {TestinstancesListComponent} from '@app/modules/secure/testinstances/list/testinstances-list.component';
import {TestinstancesDetailComponent} from '@app/modules/secure/testinstances/detail/testinstances-detail.component';
import {PhoneinstanceListComponent} from '@app/modules/secure/phoneinstance/list/phoneinstance-list.component';
import {ClientlibinstanceDetailComponent} from '@app/modules/secure/clientlibinstance/detail/clientlibinstance-detail.component';
import {MessageregisterDetailComponent} from '@app/modules/secure/messageregister/detail/messageregister-detail.component';
import {MessageregisterListComponent} from '@app/modules/secure/messageregister/list/messageregister-list.component';
import {SecureComponent} from '@app/layouts/secure/secure.component';
import {DashboardComponent} from '@app/modules/secure/dashboard/dashboard.component';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ValidationresultListComponent} from '@app/modules/secure/validationresult/list/validationresult-list.component';
import {ClientsService} from '@app/core/services/clients.service';
import {PhonesService} from '@app/core/services/phones.service';
import {TestinstancesService} from '@app/core/services/testinstances.service';
import {SessionsService} from '@app/core/services/sessions.service';
import {MessageregisterService} from '@app/core/services/messageregister.service';
import {SmscListComponent} from '@app/modules/secure/smsc/list/smsc-list.component';
import {TesterrorsListComponent} from '@app/modules/secure/testerrors/list/testerrors-list.component';
import {TestingsComponent} from '@app/modules/secure/testing/testings.component';
import {DeviceKeyNameService} from '@app/core/services/deviceKeyName.service';
import {MessageregisterComponent} from '@app/modules/secure/messageregister/messageregister.component';
import {SystemVersionService} from '@app/core/services/systemVersion.service';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';
import {SmscListItemComponent} from '@app/modules/secure/smsc/list-item/smsc-list-item.component';
import {AdminService} from '@app/core/services/admin.service';
import {AdminComponent} from '@app/modules/secure/admin/admin.component';
import {LoginComponent} from '@app/modules/public/login/login.component';
import {PublicComponent} from '@app/layouts/public/public.component';
import {SequenceViewComponent} from '@app/modules/secure/messageregister/sequence-view/sequence-view.component';
import {SequenceViewRowComponent} from '@app/modules/secure/messageregister/sequence-view-row/sequence-view-row.component';
import {TranslateService} from './core/services/translate.service';
import {TesterrorsDetailComponent} from '@app/modules/secure/testerrors/detail/testerrors-detail.component';
import {TestErrorService} from '@app/core/services/test-error.service';
import {ClientlogDetailComponent} from '@app/modules/secure/clientlog/detail/clientlog-detail.component';
import {ClientlogService} from '@app/core/services/clientlog.service';
import {HeaderComponent} from '@app/layouts/secure/header/header.component';
import {TestGroupListComponent} from '@app/modules/secure/test-group/list/test-group-list.component';
import {TestGroupViewService} from '@app/core/services/testGroupView.service';
import {TestInstanceListFilterService} from '@app/core/services/filters/testInstanceListFilter.service';
import {TestGroupListFilterService} from '@app/core/services/filters/testGroupListFilterService';
import {TestErrorTypeComponent} from '@app/modules/secure/help/test-error-type/test-error-type.component';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HeaderComponent,


    DashboardComponent,
    ClientsComponent,
    ClientsDetailComponent,
    ClientsListComponent,
    PhonesListComponent,
    PhonesDetailComponent,
    PhonesComponent,

    SessionsComponent,
    SessionsListComponent,
    SessionsDetailComponent,

    TestinstancesComponent,
    TestinstancesListComponent,
    TestinstancesDetailComponent,

    PhoneinstanceListComponent,
    ClientlibinstanceDetailComponent,
    MessageregisterListComponent,

    MessageregisterDetailComponent,
    MessageregisterComponent,
    ValidationresultListComponent,
    TesterrorsListComponent,
    SmscListComponent,
    SmscListItemComponent,
    TestingsComponent,
    AdminComponent,
    LoginComponent,
    SequenceViewComponent,
    SequenceViewRowComponent,
    TesterrorsDetailComponent,
    ClientlogDetailComponent,
    TestGroupListComponent,
    TestErrorTypeComponent,


    SecureComponent,
    PublicComponent,


  ],
  imports: [

    // angular
    BrowserModule,

    // 3rd party
    // AuthModule,

    // core & shared
    CoreModule,
    SharedModule,


    RouterModule,

    // app
    AppRoutingModule,

    BrowserAnimationsModule


  ],
  entryComponents:
    [

      // modals here

    ],
  providers: [

    AuthenticationService,

    ClientsService,
    PhonesService,
    TestinstancesService,
    TestGroupViewService,
    SessionsService,
    MessageregisterService,
    DeviceKeyNameService,
    SystemVersionService,
    ComponentServiceContainer,
    AdminService,
    TestInstanceListFilterService,
    TestGroupListFilterService,
    TranslateService,
    TestErrorService,
    ClientlogService,

    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: LoggerInterceptor, multi: true},


  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
