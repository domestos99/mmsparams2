import {AppConfig} from '@app/core/app.config';
import {environment} from '@env/environment';

describe('AppConfig.getBaseAddress (isolated test)', () => {

  it('getBaseAddress for dev', () => {
    environment.production = false;
    const value: string = AppConfig.getBaseAddress();
    expect(value).toEqual('http://localhost:4301/api', 'App config address for dev');
  });

  it('getBaseAddress for prod', () => {
    environment.production = true;
    const value: string = AppConfig.getBaseAddress();
    expect(value).not.toEqual('http://localhost:4301/api', 'App config address for prod');
  });
});
