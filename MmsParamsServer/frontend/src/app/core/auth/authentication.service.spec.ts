import {inject, TestBed} from '@angular/core/testing';
import {AuthenticationService} from '@app/core/auth/authentication.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Injector} from '@angular/core';
import {JwtResponse} from '@app/core/rest/model/jwtResponse';
import {AppConfig} from '@app/core/app.config';
import {UrlFacade} from '@app/core/urlFacade';

// https://github.com/angular/angular/issues/19974
describe('AuthenticationService', () => {

  let injector: Injector;
  let httpMock: HttpTestingController;
  let authService: AuthenticationService;

  beforeEach(() => {
    injector = TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthenticationService]
    });

    authService = injector.get(AuthenticationService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', inject([AuthenticationService], (service: AuthenticationService) => {
    expect(service).toBeTruthy();
  }));

  it('AuthenticationService should login', inject([AuthenticationService], (service: AuthenticationService) => {

    const resp: JwtResponse = {expiry: 123, token: 'xxxyyy'};

    authService.login('test', 'test')
      .subscribe(value => {
          expect(value).toBe(resp);
        },
        error => {
          fail();
        });

    const mockReq = httpMock.expectOne(UrlFacade.getAuthenticateUrl());
    expect(mockReq.request.method).toBe('POST');
    expect(mockReq.cancelled).toBeFalsy();
    expect(mockReq.request.responseType).toEqual('json');

    // send mock data to mocked HttpClient
    mockReq.flush(resp);

    httpMock.verify();

    const currentUser: JwtResponse = JSON.parse(localStorage.getItem(AuthenticationService.CURRENT_USER));
    expect(currentUser).toBeTruthy();
    expect(currentUser.expiry).toBe(resp.expiry);
    expect(currentUser.token).toBe(resp.token);
  }));

  it('AuthenticationService should logout', inject([AuthenticationService], (service: AuthenticationService) => {
    service.logout();
    expect(localStorage.getItem(AuthenticationService.CURRENT_USER)).toBeNull();
  }));

  it('AuthenticationService should logout 2', inject([AuthenticationService], (service: AuthenticationService) => {
    localStorage.setItem(AuthenticationService.CURRENT_USER, 'xxx');
    service.logout();
    expect(localStorage.getItem(AuthenticationService.CURRENT_USER)).toBeNull();
  }));

});
