import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {AppConfig} from '@app/core/app.config';
import {JwtRequest} from '@app/core/rest/model/jwtRequest';
import {JwtResponse} from '@app/core/rest/model/jwtResponse';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient) {
  }

  static CURRENT_USER = 'currentUser';

  getApiUrl(): string {
    return AppConfig.getBaseAddress() + '/authenticate';
  }

  login(username: string, password: string) {
    const request: JwtRequest = {
      username: username,
      password: password
    };

    return this.http.post<JwtResponse>(this.getApiUrl(), JSON.stringify(request))
      .pipe(map(response => {
        // login successful if there's a jwt token in the response
        if (response && response.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem(AuthenticationService.CURRENT_USER, JSON.stringify(response));
        }
        return response;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem(AuthenticationService.CURRENT_USER);
  }


}
