import {Mylogger} from '@app/core/logs';

export abstract class ComponentBase {

  protected downloadAsJson(data: any, defaultName: string) {

    try {
      // let result = String.fromCharCode.apply(null, this.data);
      const result = JSON.stringify(data);
      this.initDownload(result, defaultName);
      return;
    } catch (e) {
      Mylogger.logError('downloadAsJson', e);
    }
  }

  private initDownload(_data: any, defaultName: string) {
    const blob = new Blob([_data]);

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, defaultName);
    } else {
      var a = document.createElement('a');
      a.href = URL.createObjectURL(blob);
      a.download = defaultName;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }

  }

}
