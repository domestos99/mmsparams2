import {Injectable} from '@angular/core'
import {HttpClient} from "@angular/common/http";
import {AppConfig} from "@app/core/app.config";
import {UrlHelper} from "@app/core";

@Injectable()
export abstract class ServiceBase {

  abstract getApiCall(): string;

  protected http: HttpClient;
  errorHandler = error => alert('ServiceBase error: ' + error);

  getBaseUrl(): string {
    return AppConfig.getBaseAddress();
  }

  protected getServiceUrl(): string {
    return UrlHelper.combine2(this.getBaseUrl(), this.getApiCall());
  }

  protected getServiceUrl2(appendSlash: boolean): string {
    if (appendSlash)
      return this.getServiceUrl() + "/";
    else
      return this.getServiceUrl();
  }

  constructor(http: HttpClient) {
    this.http = http;
  }


}
