import {NgModule, Optional, SkipSelf} from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import {AuthGuard} from '@app/core/guards';
import {NoAuthGuard} from '@app/core/guards';


import {TokenInterceptor} from '@app/core/interceptors';

import {throwIfAlreadyLoaded} from './guards/module-import.guard';
import {NavigationselperService} from '@app/core/helpers';

import {
  NewlinesPipe,
} from '@app/core/pipes';


@NgModule({
  declarations: [

    NewlinesPipe,


  ],
  imports: [
    HttpClientModule
  ],
  providers: [
    AuthGuard,
    NoAuthGuard,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    NavigationselperService,


  ],
  exports: [

    NewlinesPipe
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
