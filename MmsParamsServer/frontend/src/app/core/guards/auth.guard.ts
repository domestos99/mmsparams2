import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {JwtResponse} from '@app/core/rest/model/jwtResponse';
import {AuthenticationService} from '@app/core/auth';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {

  }

  // https://stackblitz.com/edit/angular-6-registration-login-example?file=app%2F_helpers%2Fjwt.interceptor.ts

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const currentUser: JwtResponse = JSON.parse(localStorage.getItem(AuthenticationService.CURRENT_USER));
    if (currentUser && currentUser.token && currentUser.expiry) {
      const ms = Date.now();
      if (currentUser.expiry > ms) {
        // All is OK
        return true;
      }
    }

    //  not logged in so redirect to login page
    this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }

}
