import {NoAuthGuard} from "@app/core";

describe('NoAuthGuard (isolated test)', () => {
  it('should instantiate', () => {
    const component: NoAuthGuard = new NoAuthGuard();
    expect(component).toBeDefined();
  });

  it('should instantiate', () => {
    const component: NoAuthGuard = new NoAuthGuard();
    const testValue: boolean = component.canActivate();
    expect(testValue).toBe(true, 'NoAuthGuard returns true');
  });


});
