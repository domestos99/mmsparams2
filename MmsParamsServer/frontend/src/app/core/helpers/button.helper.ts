export class ButtonHelper {


  public static inNewTab($event: MouseEvent) {
    return $event && $event.ctrlKey;
  }
}
