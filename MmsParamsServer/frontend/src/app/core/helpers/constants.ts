export class Priority {

  static readonly PRIORITY_LOW: number = 128;
  static readonly PRIORITY_NORMAL: number = 129;
  static readonly PRIORITY_HIGH: number = 130;

  static readonly priorities = [{
    PriorityID: 128,
    PriorityName: 'Low'
  }, {
    PriorityID: 129,
    PriorityName: 'Medium'
  }, {
    PriorityID: 130,
    PriorityName: 'High'
  }];

}

export class YesNoValue {
  static readonly YES = 128;
  static readonly NO = 129;
}

export class MmsVersion {
  static readonly MMS_VERSION_1_0: number = 16;
  static readonly MMS_VERSION_1_1: number = 17;
  static readonly MMS_VERSION_1_2: number = 18;
  static readonly MMS_VERSION_1_3: number = 19;
}

export class Status {
  static readonly STATUS_EXPIRED: number = 128;
  static readonly STATUS_RETRIEVED: number = 129;
  static readonly STATUS_REJECTED: number = 130;
  static readonly STATUS_DEFERRED: number = 131;
  static readonly STATUS_UNRECOGNIZED: number = 132;
  static readonly STATUS_INDETERMINATE: number = 133;
  static readonly STATUS_FORWARDED: number = 134;
  static readonly STATUS_UNREACHABLE: number = 135;
}

export class SenderVisible {
  static readonly SENDER_VISIBILITY_HIDE: number = 128;
  static readonly SENDER_VISIBILITY_SHOW: number = 129;
}

export class ResponseStatus {
  static readonly RESPONSE_STATUS_OK: number = 128;
  static readonly RESPONSE_STATUS_ERROR_UNSPECIFIED: number = 129;
  static readonly RESPONSE_STATUS_ERROR_SERVICE_DENIED: number = 130;
  static readonly RESPONSE_STATUS_ERROR_MESSAGE_FORMAT_CORRUPT: number = 131;
  static readonly RESPONSE_STATUS_ERROR_SENDING_ADDRESS_UNRESOLVED: number = 132;
  static readonly RESPONSE_STATUS_ERROR_MESSAGE_NOT_FOUND: number = 133;
  static readonly RESPONSE_STATUS_ERROR_NETWORK_PROBLEM: number = 134;
  static readonly RESPONSE_STATUS_ERROR_CONTENT_NOT_ACCEPTED: number = 135;
  static readonly RESPONSE_STATUS_ERROR_UNSUPPORTED_MESSAGE: number = 136;
  static readonly RESPONSE_STATUS_ERROR_TRANSIENT_FAILURE: number = 192;
  static readonly RESPONSE_STATUS_ERROR_TRANSIENT_SENDNG_ADDRESS_UNRESOLVED: number = 193;
  static readonly RESPONSE_STATUS_ERROR_TRANSIENT_MESSAGE_NOT_FOUND: number = 194;
  static readonly RESPONSE_STATUS_ERROR_TRANSIENT_NETWORK_PROBLEM: number = 195;
  static readonly RESPONSE_STATUS_ERROR_TRANSIENT_PARTIAL_SUCCESS: number = 196;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_FAILURE: number = 224;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_SERVICE_DENIED: number = 225;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_MESSAGE_FORMAT_CORRUPT: number = 226;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_SENDING_ADDRESS_UNRESOLVED: number = 227;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_MESSAGE_NOT_FOUND: number = 228;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_CONTENT_NOT_ACCEPTED: number = 229;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_LIMITATIONS_NOT_MET: number = 230;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_REQUEST_NOT_ACCEPTED: number = 230;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_FORWARDING_DENIED: number = 232;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_NOT_SUPPORTED: number = 233;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_ADDRESS_HIDING_NOT_SUPPORTED: number = 234;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_LACK_OF_PREPAID: number = 235;
  static readonly RESPONSE_STATUS_ERROR_PERMANENT_END: number = 255;
}

export class RetrieveStatus {
  static readonly RETRIEVE_STATUS_OK: number = 128;
  static readonly RETRIEVE_STATUS_ERROR_TRANSIENT_FAILURE: number = 192;
  static readonly RETRIEVE_STATUS_ERROR_TRANSIENT_MESSAGE_NOT_FOUND: number = 193;
  static readonly RETRIEVE_STATUS_ERROR_TRANSIENT_NETWORK_PROBLEM: number = 194;
  static readonly RETRIEVE_STATUS_ERROR_PERMANENT_FAILURE: number = 224;
  static readonly RETRIEVE_STATUS_ERROR_PERMANENT_SERVICE_DENIED: number = 225;
  static readonly RETRIEVE_STATUS_ERROR_PERMANENT_MESSAGE_NOT_FOUND: number = 226;
  static readonly RETRIEVE_STATUS_ERROR_PERMANENT_CONTENT_UNSUPPORTED: number = 227;
  static readonly RETRIEVE_STATUS_ERROR_END: number = 255;
}

export class ReadStatus {
  static readonly READ_STATUS_READ: number = 128;
  static readonly READ_STATUS__DELETED_WITHOUT_BEING_READ: number = 129;
}

export class MessageClass {
  static readonly MESSAGE_CLASS_PERSONAL: number = 128;
  static readonly MESSAGE_CLASS_ADVERTISEMENT: number = 129;
  static readonly MESSAGE_CLASS_INFORMATIONAL: number = 130;
  static readonly MESSAGE_CLASS_AUTO: number = 131;
  static readonly MESSAGE_CLASS_PERSONAL_STR: string = 'personal';
  static readonly MESSAGE_CLASS_ADVERTISEMENT_STR: string = 'advertisement';
  static readonly MESSAGE_CLASS_INFORMATIONAL_STR: string = 'informational';
  static readonly MESSAGE_CLASS_AUTO_STR: string = 'auto';

  static readonly classes = [{
    ClassID: MessageClass.MESSAGE_CLASS_PERSONAL,
    ClassName: MessageClass.MESSAGE_CLASS_PERSONAL_STR
  }, {
    ClassID: MessageClass.MESSAGE_CLASS_ADVERTISEMENT,
    ClassName: MessageClass.MESSAGE_CLASS_PERSONAL_STR
  }, {
    ClassID: MessageClass.MESSAGE_CLASS_INFORMATIONAL,
    ClassName: MessageClass.MESSAGE_CLASS_PERSONAL_STR
  }, {
    ClassID: MessageClass.MESSAGE_CLASS_AUTO,
    ClassName: MessageClass.MESSAGE_CLASS_PERSONAL_STR
  }];
}

export class MmsPDUType {

  static readonly SEND_REQ: number = 128;
  static readonly SEND_CONF: number = 129;
  static readonly NOTIFICATION_IND: number = 130;
  static readonly NOTIFYRESP_IND: number = 131;
  static readonly RETRIEVE_CONF: number = 132;
  static readonly ACKNOWLEDGE_IND: number = 133;
  static readonly DELIVERY_IND: number = 134;
  static readonly READ_REC_IND: number = 135;
  static readonly READ_ORIG_IND: number = 136;
}
