import {PhoneInstanceDTO} from '@app/core/rest/model/phoneInstanceDTO';
import {PhoneInfoDTO} from '@app/core/rest/model/phoneInfoDTO';

export class DeviceNameKeyHelper {

  public static getDeviceNameByKey2(phones: Array<PhoneInfoDTO>, key: string) {
    if (!key) {
      return key;
    }

    if (!key) {
      return key;
    }

    if (key === 'SERVER') {
      return key;
    }

    if (key.startsWith('ClientLib')) {
      return 'CLIENT';
    }

    const phoneFilter = phones.filter(x => x.phoneKey === key);

    if (phoneFilter && phoneFilter.length > 0) {
      return phoneFilter[0].phoneCustomName;
    }
    return key;
  }

  public static getDeviceNameByKey(phones: Array<PhoneInstanceDTO>, key: string) {
    if (!key) {
      return key;
    }

    if (!key) {
      return key;
    }

    if (key === 'SERVER') {
      return key;
    }

    if (key.startsWith('ClientLib')) {
      return 'CLIENT';
    }

    if (!phones)
    {
      return key;
    }

    const phoneFilter = phones.filter(x => x.phoneInfo.phoneKey === key);

    if (phoneFilter && phoneFilter.length > 0) {
      return phoneFilter[0].phoneInfo.phoneCustomName;
    }
    return key;
  }
}
