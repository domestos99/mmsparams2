export class MessageErrorHelper {

  public static isErrorRow(messageKey: string) {
    return 'GENERICERRORRESPONSEMESSAGE' === messageKey || 'TESTERRORMESSAGE' === messageKey;
  }
}
