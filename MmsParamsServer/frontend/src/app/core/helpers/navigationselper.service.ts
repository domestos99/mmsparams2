import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {UrlHelper} from './urlHelper';


@Injectable()
export class NavigationselperService {

  constructor(private router: Router) {
  }

  public openSessionDetail(inNewTab: boolean, sessionId: string) {
    UrlHelper.navigate(this.router, inNewTab, ['/sessions', sessionId]);
  }

  public openTestInstanceDetail(inNewTab: boolean, testId: string) {
    UrlHelper.navigate(this.router, inNewTab, ['/testinstances', testId]);
  }

  public openClientDetail(inNewTab: boolean, clientKey: string) {
    UrlHelper.navigate(this.router, inNewTab, ['/clients', clientKey]);
  }

  public openPhoneDetail(inNewTab: boolean, phoneKey: string) {
    UrlHelper.navigate(this.router, inNewTab, ['/phones', phoneKey]);
  }

  // row.id, row.testID, row.messageID, row.createdDT
  public openWebSocketMessageDetail(inNewTab: boolean, id: number, testId: string, messageID: string, createdDT: number) {
    let aID: number;
    if (!id || id <= 0) {
      aID = -1;
    }
    UrlHelper.navigate(this.router, inNewTab, ['/messageregister', id, testId, messageID, createdDT]);
  }

  public openMessageSequence(inNewTab: boolean, testID: string) {
    UrlHelper.navigate(this.router, inNewTab, ['/messageregister/sequence', testID]);
  }

  openTestErrorDetail(inNewTab: boolean, testID: string, id: number) {
    UrlHelper.navigate(this.router, inNewTab, ['/testerror', testID, id]);
  }

  openClientLogDetail(inNewTab: boolean, testID: string) {
    UrlHelper.navigate(this.router, inNewTab, ['/clientlogs', testID]);
  }
}
