/**
 * This component has some logic in it, and a very simple template.
 * For now, we only want to test the class logic. For doing so,
 * we will test only the component class without rendering the template.
 * This test is an Isolated test.
 */
import {ObjectHelper} from "@app/core";

describe('ObjectHelper.isNotEmptyOrNull (isolated test)', () => {
  it('isNotEmptyOrNull for null', () => {
    const value: boolean = ObjectHelper.isNotEmptyOrNull(null);
    expect(value).toBe(false, "Null is not null?");
  });

  it('isNotEmptyOrNull for string', () => {
    const testValue: string = 'hello';
    const value: boolean = ObjectHelper.isNotEmptyOrNull(testValue);
    expect(value).toBe(true, "String is not null");
  });

  it('isNotEmptyOrNull for number', () => {
    const testValue: number = 123;
    const value: boolean = ObjectHelper.isNotEmptyOrNull(testValue);
    expect(value).toBe(true, "number is not null");
  });

  it('Object for number', () => {
    const testValue: object = {'test': 1};
    const value: boolean = ObjectHelper.isNotEmptyOrNull(testValue);
    expect(value).toBe(true, "Object is not null");
  });

  it('isNotEmptyOrNull for boolean', () => {
    const testValue: boolean = false;
    const value: boolean = ObjectHelper.isNotEmptyOrNull(testValue);
    expect(value).toBe(true, "Boolean is not null");
  });

});


describe('ObjectHelper.isObject (isolated test)', () => {
  it('isNotEmptyOrNull is not object', () => {
    const value: boolean = ObjectHelper.isObject(null);
    expect(value).toBe(false, "Null is not object");
  });

  it('isNotEmptyOrNull for string', () => {
    const testValue: string = 'hello';
    const value: boolean = ObjectHelper.isObject(testValue);
    expect(value).toBe(false, "String is not object");
  });

  it('isNotEmptyOrNull for number', () => {
    const testValue: number = 123;
    const value: boolean = ObjectHelper.isObject(testValue);
    expect(value).toBe(false, "number is not object");
  });

  it('Object for boolean', () => {
    const testValue = {'test': 1};
    const value: boolean = ObjectHelper.isObject(testValue);
    expect(value).toBe(true, "Object is object");
  });

});


describe('ObjectHelper.isLongArray (isolated test)', () => {

  it('isLongArray is not object', () => {
    const value: boolean = ObjectHelper.isLongArray(null);
    expect(value).toBe(false, "Null is not LongArray");
  });

  it('isLongArray for string', () => {
    const testValue: string = 'hello';
    const value: boolean = ObjectHelper.isLongArray(testValue);
    expect(value).toBe(false, "String is not LongArray");
  });

  it('isLongArray for number', () => {
    const testValue: number = 123;
    const value: boolean = ObjectHelper.isLongArray(testValue);
    expect(value).toBe(false, "number is not LongArray");
  });

  it('isLongArray for number', () => {
    const testValue = {'test': 1};
    const value: boolean = ObjectHelper.isLongArray(testValue);
    expect(value).toBe(false, "Object is not LongArray");
  });

  it('isLongArray for Array 2 strings', () => {
    const testValue: Array<string> = ['a', 'b'];
    const value: boolean = ObjectHelper.isLongArray(testValue);
    expect(value).toBe(false, "Array 2 strings is not LongArray");
  });

  it('isLongArray for Array 200 strings', () => {
    const testValue: Array<string> = [];

    for (let i = 0; i < 200; i++) {
      testValue.push('a');
    }

    const value: boolean = ObjectHelper.isLongArray(testValue);
    expect(value).toBe(true, "Array 200 strings is LongArray");
  });

  it('isLongArray for Array 200 numbers', () => {
    const testValue: Array<number> = [];

    for (let i = 0; i < 200; i++) {
      testValue.push(i);
    }

    const value: boolean = ObjectHelper.isLongArray(testValue);
    expect(value).toBe(true, "Array 200 numbers is LongArray");
  });

});









