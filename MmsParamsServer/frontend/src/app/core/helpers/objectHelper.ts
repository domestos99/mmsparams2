export class ObjectHelper {

  static toJson(obj: any): string {
    return JSON.stringify(obj);
  }

  static isNotEmptyOrNull(o: any): boolean {
    if ((typeof o === 'number') || (typeof o === 'boolean')) {
      return true;
    }
    return !(o == null || Object.keys(o).length === 0);
  }

  static isObject(value: any): boolean {
    return (value instanceof Object);
  }

  static isLongArray(value: any): boolean {
    if (value instanceof Array) {
      if ((value as Array<any>).length > 100)
        return true;
    }
    return false;
  }
}
