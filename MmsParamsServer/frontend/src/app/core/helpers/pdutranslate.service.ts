import {Injectable} from '@angular/core'
import {
  Status,
  Priority,
  MessageClass,
  MmsVersion,
  ResponseStatus,
  RetrieveStatus
} from "@app/core/helpers/constants";

@Injectable()
export class PduTranslateService {
  static getStatus(value): string {
    return null;
  }

  static getClass(value) {
    switch (value) {
      case MessageClass.MESSAGE_CLASS_AUTO:
        return MessageClass.MESSAGE_CLASS_AUTO_STR;
      case MessageClass.MESSAGE_CLASS_INFORMATIONAL:
        return MessageClass.MESSAGE_CLASS_INFORMATIONAL_STR;
      case MessageClass.MESSAGE_CLASS_PERSONAL:
        return MessageClass.MESSAGE_CLASS_PERSONAL_STR;
      case MessageClass.MESSAGE_CLASS_ADVERTISEMENT:
        return MessageClass.MESSAGE_CLASS_ADVERTISEMENT_STR;
      default:
        return value.toString();
    }
  }

  static getPriority(value: number): string {
    switch (value) {
      case Priority.PRIORITY_LOW:
        return 'Low';
      case Priority.PRIORITY_NORMAL:
        return 'Normal';
      case Priority.PRIORITY_HIGH:
        return 'High';
      default:
        return value.toString();
    }
  }

  static getMmsVersion(value): string {
    switch (value) {
      case MmsVersion.MMS_VERSION_1_0:
        return 'MMS_VERSION_1_0';
      case MmsVersion.MMS_VERSION_1_1:
        return 'MMS_VERSION_1_1';
      case MmsVersion.MMS_VERSION_1_2:
        return 'MMS_VERSION_1_2';
      case MmsVersion.MMS_VERSION_1_3:
        return 'MMS_VERSION_1_3';
      default:
        return value;
    }
  }

  static getStatusString(status: number): string {
    switch (status) {
      case Status.STATUS_EXPIRED:
        return 'EXPIRED';
      case Status.STATUS_RETRIEVED:
        return 'RETRIEVED';
      case Status.STATUS_REJECTED:
        return 'REJECTED';
      case Status.STATUS_DEFERRED:
        return 'DEFERRED';
      case Status.STATUS_UNRECOGNIZED:
        return 'UNRECOGNIZED';
      case Status.STATUS_INDETERMINATE:
        return 'INDETERMINATE';
      case Status.STATUS_FORWARDED:
        return 'FORWARDED';
      case Status.STATUS_UNREACHABLE:
        return 'UNREACHABLE';
    }
    return status.toString();
  }


}
