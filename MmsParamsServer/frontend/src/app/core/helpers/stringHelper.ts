export class StringHelper {

  public static contains(value: string, textToContains: string): boolean {
    if (!value || !textToContains) {
      return false;
    }
    if (value.indexOf(textToContains) >= 0) {
      return true;
    }
    return false;
  }
}
