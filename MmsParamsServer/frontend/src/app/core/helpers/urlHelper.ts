import {Router} from '@angular/router';

export class UrlHelper {

  static combine2(a: string, b: string): string {
    return a.trim() + '/' + b.trim();
  }

  static combine3(a: string, b: string, c: string): string {
    return a.trim() + '/' + b.trim() + '/' + c.trim();
  }

  public static openNewTab(router: Router, cmd: any) {
    const url = router.serializeUrl(router.createUrlTree(cmd));
    window.open(url, '_blank');
  }

  public static navigate(router: Router, inNewTab: boolean, cmd: any) {
    if (inNewTab) {
      UrlHelper.openNewTab(router, cmd);
    } else {
      router.navigate(cmd);
    }
  }
}
