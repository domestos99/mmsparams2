import {TestBed} from '@angular/core/testing';
// app
import {ErrorInterceptor} from "@app/core/interceptors/error.interceptor";
import {AuthenticationService} from "@app/core/auth";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {JwtResponse} from '@app/core/rest/model/jwtResponse';
import {ClientsService} from '@app/core/services/clients.service';
import {UrlFacade} from '@app/core/urlFacade';

describe('ErrorInterceptor (isolated test)', () => {

  let service: AuthenticationService;
  let httpMock: HttpTestingController;
  let clientsService: ClientsService;
  let errorInterceptor: ErrorInterceptor;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AuthenticationService,
        ClientsService,
        ErrorInterceptor,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorInterceptor,
          multi: true,
        },
      ],
    });

    service = TestBed.get(AuthenticationService);
    httpMock = TestBed.get(HttpTestingController);
    clientsService = TestBed.get(ClientsService);
    errorInterceptor = TestBed.get(ErrorInterceptor);
    localStorage.removeItem(AuthenticationService.CURRENT_USER);
  });

  it('should instantiate', () => {
    expect(errorInterceptor).toBeDefined();
  });

  it('should not throw', () => {

    const jwtResponse: JwtResponse = {expiry: 123, token: 'xxxyyy'};
    localStorage.setItem(AuthenticationService.CURRENT_USER, JSON.stringify(jwtResponse));

    let response: any;
    let errResponse: any;

    clientsService.getAll().subscribe(value => {
      response = value;
    }, error => {
      errResponse = error;
    });

    const data = 'Some data';
    const mockErrorResponse = {status: 200, statusText: 'OK'};

    const httpRequest = httpMock.expectOne(UrlFacade.getClientsUrl());
    httpRequest.flush(data, mockErrorResponse);

    expect(response).toBeTruthy();
    expect(errResponse).toBeUndefined();

    expect(localStorage.getItem(AuthenticationService.CURRENT_USER)).toBeTruthy();
  });

  it('should throw 404 and not logout', () => {

    const jwtResponse: JwtResponse = {expiry: 123, token: 'xxxyyy'};
    localStorage.setItem(AuthenticationService.CURRENT_USER, JSON.stringify(jwtResponse));

    let response: any;
    let errResponse: any;

    clientsService.getAll().subscribe(value => {
      response = value;
    }, error => {
      errResponse = error;
    });

    const data = 'Invalid request parameters';
    const mockErrorResponse = {status: 404, statusText: 'Not Found'};

    const httpRequest = httpMock.expectOne(UrlFacade.getClientsUrl());
    httpRequest.flush(data, mockErrorResponse);

    expect(response).toBeUndefined();
    expect(errResponse).toBeTruthy();

    expect(localStorage.getItem(AuthenticationService.CURRENT_USER)).toBeTruthy();
  });


  it('should throw 401 and logout', () => {

    const jwtResponse: JwtResponse = {expiry: 123, token: 'xxxyyy'};
    localStorage.setItem(AuthenticationService.CURRENT_USER, JSON.stringify(jwtResponse));

    let response: any;
    let errResponse: any;

    service.login('test', 'test').subscribe(value => {
      response = value;
    }, error => {
      errResponse = error;
    });

    const data = 'Invalid request parameters';
    const mockErrorResponse = {status: 401, statusText: 'Unauthorized'};

    const httpRequest = httpMock.expectOne(UrlFacade.getAuthenticateUrl());
    httpRequest.flush(data, mockErrorResponse);

    expect(response).toBeUndefined();
    expect(errResponse).toBeTruthy();

    expect(localStorage.getItem(AuthenticationService.CURRENT_USER)).toBeNull();
  });

});
