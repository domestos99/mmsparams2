import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthenticationService} from '@app/core/auth';
import {Mylogger} from '@app/core/logs';
import {StringHelper} from "@app/core/helpers/stringHelper";


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401) {
        // auto logout if 401 response returned from api
        this.authenticationService.logout();

        if (StringHelper.contains(request.url, '/api/authenticate')) {

        } else {
          location.reload(true);
        }
      }

      // Vypnuty server: 0 (Unknown Error) - Http failure response for (unknown url): 0 Unknown Error

      Mylogger.logError(err);
      const error = err.status + ' (' + err.statusText + ') - ' + err.message;
      return throwError(error);
    }));
  }
}
