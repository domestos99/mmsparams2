import {TokenInterceptor} from "@app/core/interceptors/token.interceptor";
import {JwtResponse} from '@app/core/rest/model/jwtResponse';
import {AuthenticationService} from '@app/core/auth';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';
import {ClientsService} from '@app/core/services/clients.service';
import {UrlFacade} from '@app/core/urlFacade';

// https://alligator.io/angular/testing-http-interceptors/
describe('TokenInterceptor (isolated test)', () => {

  let service: ClientsService;
  let httpMock: HttpTestingController;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ClientsService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true,
        },
      ],
    });

    service = TestBed.get(ClientsService);
    httpMock = TestBed.get(HttpTestingController);
    localStorage.removeItem(AuthenticationService.CURRENT_USER);
  });


  it('should instantiate', () => {
    const component: TokenInterceptor = new TokenInterceptor();
    expect(component).toBeDefined();
  });

  it('should set header token', () => {
    const component: TokenInterceptor = new TokenInterceptor();

    const jwtResponse: JwtResponse = {expiry: 123, token: 'xxxyyy'};
    localStorage.setItem(AuthenticationService.CURRENT_USER, JSON.stringify(jwtResponse));

    service.getAll().subscribe(value => {
      expect(value).toBeTruthy();
    });

    const httpRequest = httpMock.expectOne(UrlFacade.getClientsUrl());

    expect(httpRequest.request.headers.has('Authorization')).toEqual(true);
    expect(httpRequest.request.headers.get('Authorization')).toBe(
      'Bearer xxxyyy',
    );

    expect(httpRequest.request.headers.has('Content-Type')).toEqual(true);
    expect(httpRequest.request.headers.get('Content-Type')).toBe(
      'application/json',
    );
  });

  it('should not set header token', () => {

    service.getAll().subscribe(value => {
      expect(value).toBeTruthy();
    });

    const httpRequest = httpMock.expectOne(UrlFacade.getClientsUrl());

    expect(httpRequest.request.headers.has('Authorization')).toEqual(false);

    expect(httpRequest.request.headers.has('Content-Type')).toEqual(true);
    expect(httpRequest.request.headers.get('Content-Type')).toBe(
      'application/json',
    );
  });


});
