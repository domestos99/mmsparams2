import {environment} from '@env/environment';


export class Mylogger {

  public static logDebug(message?: any, ...optionalParams: any[]): void {
    Mylogger.log(message, optionalParams);
  }

  public static logError(message?: any, ...optionalParams: any[]): void {
    console.error(message, optionalParams);
  }

  static logData(message?: any, ...optionalParams: any[]) {
    Mylogger.log(message, optionalParams);
  }

  static log(message?: any, ...optionalParams: any[]) {
    if (!environment.production) {
      console.log(message, optionalParams);
    }
  }
}
