export class KeyValuePair<T, V> {


  constructor(public key: T, public val: V) {
  }
}
