export enum YesNoAllEnum {
  YES = 'YES',
  NO = 'NO',
  ALL = 'ALL'
}
