/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { PhoneInfoDTO } from './phoneInfoDTO';
import { WSSessionDTO } from './wSSessionDTO';


export interface PhoneInstanceDTO { 
    id?: number;
    phoneInfo?: PhoneInfoDTO;
    phoneKey?: string;
    session?: WSSessionDTO;
}
