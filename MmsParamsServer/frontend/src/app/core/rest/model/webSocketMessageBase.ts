// /**
//  * Api Documentation
//  * Api Documentation
//  *
//  * OpenAPI spec version: 1.0
//  *
//  *
//  * NOTE: This class is auto generated by the swagger code generator program.
//  * https://github.com/swagger-api/swagger-codegen.git
//  * Do not edit the class manually.
//  */
//
//
// export interface WebSocketMessageBase {
//   messageID?: string;
//   messageKey?: string;
//   recipientKey?: string;
//   senderKey?: string;
//   testID?: string;
//   createdDT?: number;
// }
