import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ServiceBase} from '@app/core/base/servicebase';

@Injectable()
export class AdminService extends ServiceBase {


  getApiCall(): string {
    return 'admin';
  }

  public realodDataFromApi(): Observable<boolean> {
    return this.http.get<boolean>(this.getServiceUrl2(true) + 'refreshcache');
  }

  deleteOldTests(value: string): Observable<boolean> {
    return this.http.get<boolean>(this.getServiceUrl2(true) + 'deleteoldtests/' + value);
  }
}
