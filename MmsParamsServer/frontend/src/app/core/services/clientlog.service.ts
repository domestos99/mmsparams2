import {ServiceBase} from '@app/core/base/servicebase';
import {Observable} from 'rxjs';
import {ClientServerLogDTO} from '@app/core/rest/model/clientServerLogDTO';
import {Injectable} from '@angular/core';

@Injectable()
export class ClientlogService extends ServiceBase {

  getApiCall(): string {
    return 'clientlogs';
  }

  public getByTestID(testID: string): Observable<ClientServerLogDTO> {
    return this.http.get<ClientServerLogDTO>(this.getServiceUrl() + '/' + testID);
  }

}
