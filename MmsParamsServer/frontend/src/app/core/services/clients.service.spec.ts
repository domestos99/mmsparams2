import {inject, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Injector} from '@angular/core';
import {ClientsService} from '@app/core/services/clients.service';
import {ClientInfoDTO} from '@app/core/rest/model/clientInfoDTO';
import {UrlFacade} from '@app/core/urlFacade';

// https://github.com/angular/angular/issues/19974
describe('ClientsService', () => {

  let injector: Injector;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    injector = TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ClientsService]
    });

    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', inject([ClientsService], (service: ClientsService) => {
    expect(service).toBeTruthy();
  }));

  it('ClientsService getAll', inject([ClientsService], (service: ClientsService) => {

    const respExpected: Array<ClientInfoDTO> = [{
      clientKey: 'key1',
    }, {
      clientKey: 'key2'
    }];

    let response: Array<ClientInfoDTO>;
    let errrorResp: any;
    service.getAll()
      .subscribe(data => {
          response = data;
        },
        error => {
          errrorResp = error;
        });

    const mockReq = httpMock.expectOne(UrlFacade.getClientsUrl());
    expect(mockReq.request.method).toBe('GET');
    expect(mockReq.cancelled).toBeFalsy();
    expect(mockReq.request.responseType).toEqual('json');

    mockReq.flush(respExpected);

    httpMock.verify();

    expect(response).toBeTruthy();
    expect(errrorResp).toBeUndefined();
    expect(response).toBe(respExpected);
  }));


});
