import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ClientInfoDTO} from '@app/core/rest/model/clientInfoDTO';
import {ServiceBase} from '@app/core/base/servicebase';
import {ClientLibInstanceDTO} from '@app/core/rest/model/clientLibInstanceDTO';


@Injectable()
export class ClientsService extends ServiceBase {


  getApiCall(): string {
    return 'clients';
  }

  public getAll(): Observable<Array<ClientInfoDTO>> {
    return this.http.get<Array<ClientInfoDTO>>(this.getServiceUrl());
  }

  public getById(clientKey: string): Observable<ClientLibInstanceDTO> {
    return this.http.get<ClientInfoDTO>(this.getServiceUrl() + '/' + clientKey);
  }


  postForceDisconnect(clientKey: string): Observable<boolean> {
    return this.http.post<boolean>(this.getServiceUrl2(true) + 'forcedisconnect/' + clientKey, null);
  }
}
