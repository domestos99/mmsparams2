import {Injectable} from "@angular/core";
import {PhonesService} from "@app/core/services/phones.service";
import {KeyValuePair} from "@app/core/model/keyValuePair";
import {Cacheable} from "@app/core/services/cacheable";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";


@Injectable()
export class DeviceKeyNameService {


  constructor(private phoneService: PhonesService) {
    this.phoneList.getHandler = () => {
      return this.loadPhones();
    };
    this.phoneList.refresh();
  }

  phoneList: Cacheable<Array<KeyValuePair<string, string>>> = new Cacheable<Array<KeyValuePair<string, string>>>();

  loadPhones(): Observable<Array<KeyValuePair<string, string>>> {
    return this.phoneService.getAll()
      .pipe(
        map((value, index) => {
          let r: Array<KeyValuePair<string, string>> = [];
          value.forEach(a => {
            r.push(new KeyValuePair<string, string>(a.phoneKey, a.phoneCustomName));
          });
          return r;
        })
      );
  }

  initMe() {
    this.phoneList.getData2();
  }

  getByKey(key: string): string {


    return key;

    if (!key)
      return key;

    if (key == 'SERVER')
      return key;

    if (key.startsWith("ClientLib")) {
      return 'CLIENT';
    }

    let phoneFilter = this.phoneList.getData2().filter(x => x.key == key);
    if (phoneFilter.length == 1)
      return phoneFilter[0].val;

    this.phoneList.refresh();
    let phoneFilter2 = this.phoneList.getData2().filter(x => x.key == key);
    if (phoneFilter2.length == 1)
      return phoneFilter2[0].val;


    return key;
  }


}
