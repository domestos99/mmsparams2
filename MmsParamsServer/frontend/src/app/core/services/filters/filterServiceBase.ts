export abstract class FilterServiceBase<T> {

  protected abstract getStorageKey(): string;

  protected abstract getDefaultFilter(): T;

  public getFiltersOrDefault(): T {
    const filters: T = JSON.parse(localStorage.getItem(this.getStorageKey()));
    if (filters) {
      return filters;
    } else {
      return this.getDefaultFilter();
    }
  }

  public saveFilters(filters: T) {
    localStorage.setItem(this.getStorageKey(), JSON.stringify(filters));
  }

  public removeFilters() {
    localStorage.removeItem(this.getStorageKey());
  }

}
