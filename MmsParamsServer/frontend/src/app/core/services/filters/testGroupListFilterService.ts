import {Injectable} from '@angular/core';
import {FilterServiceBase} from '@app/core/services/filters/filterServiceBase';

@Injectable()
export class TestGroupListFilterService extends FilterServiceBase<TestGroupListFilterModel> {

  protected getStorageKey(): string {
    return 'testGroupListFilters';
  }

  protected getDefaultFilter(): TestGroupListFilterModel {
    return {
      rememberFilters: false,
      searchString: ''
    };
  }

}

export interface TestGroupListFilterModel {
  searchString: string;
  rememberFilters: boolean;
}
