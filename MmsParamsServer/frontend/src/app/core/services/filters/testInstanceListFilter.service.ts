import {Injectable} from '@angular/core';
import {YesNoAllEnum} from '@app/core/model/yesNoAll.enum';
import {FilterServiceBase} from '@app/core/services/filters/filterServiceBase';

@Injectable()
export class TestInstanceListFilterService extends FilterServiceBase<TestInstanceListFilterModel> {

  protected getStorageKey(): string {
    return 'testInstanceListFilters';
  }

  protected getDefaultFilter(): TestInstanceListFilterModel {
    return {
      searchString: '',
      hasValidationErrorsFilter: YesNoAllEnum.ALL,
      hasErrorsFilter: YesNoAllEnum.ALL,
      rememberFilters: false
    };
  }
}

export interface TestInstanceListFilterModel {
  searchString: string;
  hasErrorsFilter: YesNoAllEnum;
  hasValidationErrorsFilter: YesNoAllEnum;
  rememberFilters: boolean;
}


