import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';


@Injectable()
export class ComponentServiceContainer {

  constructor(private _snackBar: MatSnackBar) {

  }

  public showSnackBar(message: any) {
    return this.showSnackBar2(message, 5000);
  }

  public showSnackBar2(message: any, aDuration: number) {
    this._snackBar.open(message, 'OK', {
      duration: aDuration
    });
  }

}
