import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ServiceBase} from '@app/core/base/servicebase';
import {WebSocketMessageDTO} from "@app/core/rest/model/webSocketMessageDTO";
import {MessageSequenceList} from "@app/core/rest/model/messageSequenceList";


@Injectable()
export class MessageregisterService extends ServiceBase {


  getApiCall(): string {
    return 'messageregister';
  }

  // public getAll(): Observable<Array<ClientInfo>> {
  //   return this.http.get<string>(this.getBaseUrl() + "/" + this.getApiCall());
  // }

  public getById(id: number, testId: string, messageId: string, createdDT: number): Observable<WebSocketMessageDTO> {
    return this.http.get<WebSocketMessageDTO>(this.getServiceUrl() + "/" + id + "/" + testId + "/" + messageId + "/" + createdDT);
  }


  getSequenceMessages(testId: string): Observable<MessageSequenceList> {
    return this.http.get<MessageSequenceList>(this.getServiceUrl() + "/sequence/" + testId);

  }
}
