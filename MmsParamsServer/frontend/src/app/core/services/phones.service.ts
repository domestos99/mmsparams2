import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ServiceBase} from '@app/core/base/servicebase';
import {Mylogger} from '@app/core/logs';
import {PhoneInfoDTO} from '@app/core/rest/model/phoneInfoDTO';


@Injectable()
export class PhonesService extends ServiceBase {


  getApiCall(): string {
    return 'phones';
  }

  public getAll(): Observable<Array<PhoneInfoDTO>> {
    return this.http.get<Array<PhoneInfoDTO>>(this.getServiceUrl());
  }

  public getById(phoneKey: string): Observable<PhoneInfoDTO> {
    return this.http.get<PhoneInfoDTO>(this.getServiceUrl2(true) + phoneKey);
  }


  postForceDisconnect(phoneKey: string): Observable<boolean> {
    return this.http.post<boolean>(this.getServiceUrl2(true) + 'forcedisconnect/' + phoneKey, null);
  }
}
