import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ServiceBase} from '@app/core/base/servicebase';
import {WSSessionDTO} from '@app/core/rest/model/wSSessionDTO';


@Injectable()
export class SessionsService extends ServiceBase {


  getApiCall(): string {
    return 'sessions';
  }

  public getAll(): Observable<Array<WSSessionDTO>> {
    return this.http.get<Array<WSSessionDTO>>(this.getServiceUrl());
  }

  public getById(sessionID: string): Observable<WSSessionDTO> {
    return this.http.get<WSSessionDTO>(this.getServiceUrl() + '/' + sessionID);
  }


}
