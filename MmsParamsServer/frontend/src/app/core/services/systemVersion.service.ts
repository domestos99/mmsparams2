import {Injectable} from "@angular/core";
import {ServiceBase} from "@app/core/base/servicebase";
import {SystemVersionModel} from "@app/core/rest/model/systemVersionModel";

@Injectable()
export class SystemVersionService extends ServiceBase {

  getApiCall(): string {
    return 'systemversion';
  }

  // public getSystemVersion(): Observable<SystemVersionModel> {
  //   return this.http.get<SystemVersionModel>(this.getServiceUrl());
  // }

  get(): SystemVersionModel {

    const vers: SystemVersionModel = {
      systemVersion: '1.0.3.10',
      systemBuild: 'd3b3ce0c-5bd6-4313-bd75-6390cdf281a4'
    };
    return vers;
  }
}
