import {Injectable} from '@angular/core';
import {ServiceBase} from '@app/core/base/servicebase';
import {Observable} from 'rxjs';
import {Mylogger} from '@app/core/logs';
import {TestErrorDTO} from '@app/core/rest/model/testErrorDTO';

@Injectable()
export class TestErrorService extends ServiceBase {

  getApiCall(): string {
    return 'testerror';
  }

  public getById(id: string): Observable<TestErrorDTO> {
    return this.http.get<TestErrorDTO>(this.getServiceUrl() + '/' + id)
      .pipe(resp => {
        Mylogger.logData(resp);
        return resp;
      });
  }

}
