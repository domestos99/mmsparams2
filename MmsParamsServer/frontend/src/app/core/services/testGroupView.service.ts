import {Injectable} from '@angular/core';
import {ServiceBase} from '@app/core/base/servicebase';
import {Observable} from 'rxjs';
import {TestInstanceTestGroupView} from '@app/core/rest/model/testInstanceTestGroupView';

@Injectable()
export class TestGroupViewService extends ServiceBase {

  getApiCall(): string {
    return 'testgroupview';
  }

  public getAll(): Observable<Array<TestInstanceTestGroupView>> {
    return this.http.get<Array<TestInstanceTestGroupView>>(this.getServiceUrl());
  }

}
