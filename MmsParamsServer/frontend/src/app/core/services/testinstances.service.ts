import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ServiceBase} from '@app/core/base/servicebase';
import {ServerTestInstanceDTO} from '@app/core/rest/model/serverTestInstanceDTO';
import {ServerTestInstanceDetailDTO} from '@app/core/rest/model/serverTestInstanceDetailDTO';
import {Mylogger} from '@app/core/logs';


@Injectable()
export class TestinstancesService extends ServiceBase {


  getApiCall(): string {
    return 'testinstances';
  }

  public getAll(isOpen: boolean): Observable<Array<ServerTestInstanceDTO>> {
    return this.http.get<Array<ServerTestInstanceDTO>>(this.getServiceUrl() + '?isOpen=' + isOpen);
  }

  public getById(testId: string): Observable<ServerTestInstanceDetailDTO> {
    return this.http.get<ServerTestInstanceDetailDTO>(this.getServiceUrl() + '/' + testId)
      .pipe(resp => {
        Mylogger.logData(resp);
        return resp;
      });
  }

  public delete2(testId: string): Observable<boolean> {
    return this.http.get<boolean>(this.getServiceUrl() + '/delete/' + testId)
      .pipe(resp => {
        Mylogger.logData(resp);
        return resp;
      });
  }

  public delete(testId: string): Observable<boolean> {
    return this.http.delete<boolean>(this.getServiceUrl() + '/' + testId)
      .pipe(resp => {
        Mylogger.logData(resp);
        return resp;
      });

  }
}
