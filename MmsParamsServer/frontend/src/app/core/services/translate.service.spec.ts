import {inject, TestBed} from '@angular/core/testing';
import {TranslateService} from '@app/core/services/translate.service';

// https://github.com/angular/angular/issues/19974
describe('TranslateService', () => {


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [TranslateService]
    });
  });

  it('should be created', inject([TranslateService], (service: TranslateService) => {
    expect(service).toBeTruthy();
  }));

  it('TranslateService translate null', inject([TranslateService], (service: TranslateService) => {
    expect(service.translate(null)).toBeNull();
  }));

  it('TranslateService translate empty', inject([TranslateService], (service: TranslateService) => {
    expect(service.translate('')).toBe('');
  }));

  it('TranslateService translate unknown', inject([TranslateService], (service: TranslateService) => {
    expect(service.translate('xxxyyy')).toBe('xxxyyy');
  }));

  it('TranslateService translate message', inject([TranslateService], (service: TranslateService) => {
    expect(service.translate('SENDREQRESPONSEMESSAGE')).toBe('MMS SendReq');
  }));

});
