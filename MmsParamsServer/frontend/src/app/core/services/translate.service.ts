import {Injectable} from "@angular/core";
import {MessageTypeTranslator} from "@app/core/helpers/message-type.translator";
import {TranslateItem} from "@app/core/model/translateItem";

@Injectable()
export class TranslateService {


  public translate(key: string): string {

    if (!key)
      return key;

    const tr: TranslateItem = MessageTypeTranslator.translations.find(value => {
      return value.key === key;
    });

    if (tr && tr.value) {
      return tr.value;
    }

    return key;
  }

}
