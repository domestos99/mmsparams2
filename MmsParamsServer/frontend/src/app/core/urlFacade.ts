import {AppConfig} from '@app/core/app.config';

export class UrlFacade {

  public static getAuthenticateUrl(): string {
    return AppConfig.getBaseAddress() + '/authenticate';
  }

  public static getClientsUrl(): string {
    return AppConfig.getBaseAddress() + '/clients';
  }

  static getAdminReloadDataFromApiUrl() {
    return AppConfig.getBaseAddress() + '/admin/refreshcache';
  }
}
