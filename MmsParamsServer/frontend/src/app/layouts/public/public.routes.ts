import {Routes} from '@angular/router';
import {LoginComponent} from '@app/modules/public/login/login.component';


export const PUBLIC_ROUTES: Routes = [
  {path: '', redirectTo: '', pathMatch: 'full'},
  {path: 'login', component: LoginComponent}
];
