import {Component, OnInit} from '@angular/core';
import {SystemVersionService} from '@app/core/services/systemVersion.service';
import {SystemVersionModel} from '@app/core/rest/model/systemVersionModel';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss']
})

export class HeaderComponent implements OnInit {

  constructor(private systemVersionService: SystemVersionService) {
  }

  collapsed = true;

  systemVersion: SystemVersionModel;

  ngOnInit(): void {
    this.systemVersion = this.systemVersionService.get();
  }
}
