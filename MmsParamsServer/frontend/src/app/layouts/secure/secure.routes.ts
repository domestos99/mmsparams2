import {Routes} from '@angular/router';
import {ClientsComponent} from '@app/modules/secure/clients/clients.component';
import {SessionsComponent} from '@app/modules/secure/sessions/sessions.component';
import {TestinstancesComponent} from '@app/modules/secure/testinstances/testinstances.component';
import {DashboardComponent} from '@app/modules/secure/dashboard/dashboard.component';
import {PhonesComponent} from '@app/modules/secure/phones/phones.component';
import {ClientsDetailComponent} from '@app/modules/secure/clients/detail/clients-detail.component';
import {SessionsDetailComponent} from '@app/modules/secure/sessions/detail/sessions-detail.component';
import {TestinstancesDetailComponent} from '@app/modules/secure/testinstances/detail/testinstances-detail.component';
import {MessageregisterDetailComponent} from '@app/modules/secure/messageregister/detail/messageregister-detail.component';
import {PhonesDetailComponent} from '@app/modules/secure/phones/detail/phones-detail.component';
import {TestingsComponent} from '@app/modules/secure/testing/testings.component';
import {AdminComponent} from '@app/modules/secure/admin/admin.component';
import {AuthGuard} from '@app/core';
import {SequenceViewComponent} from '@app/modules/secure/messageregister/sequence-view/sequence-view.component';
import {TesterrorsDetailComponent} from "@app/modules/secure/testerrors/detail/testerrors-detail.component";
import {ClientlogDetailComponent} from "@app/modules/secure/clientlog/detail/clientlog-detail.component";
import {TestGroupListComponent} from '@app/modules/secure/test-group/list/test-group-list.component';

export const SECURE_ROUTES: Routes = [

  {path: '', redirectTo: '', pathMatch: 'full'},
  {path: '', component: DashboardComponent},
  {path: 'dashboard', component: DashboardComponent},

  {
    path: 'clients',
    component: ClientsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'clients/:clientkey',
    component: ClientsDetailComponent, canActivate: [AuthGuard]
  },
  {
    path: 'sessions',
    component: SessionsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'sessions/:id',
    component: SessionsDetailComponent, canActivate: [AuthGuard]
  },
  {
    path: 'phones',
    component: PhonesComponent, canActivate: [AuthGuard]
  },
  {
    path: 'phones/:phonekey',
    component: PhonesDetailComponent, canActivate: [AuthGuard]
  },
  {
    path: 'testinstances',
    component: TestinstancesComponent, canActivate: [AuthGuard]
  },
  {
    path: 'testinstances/:testId',
    component: TestinstancesDetailComponent, canActivate: [AuthGuard]
  },
  {
    path: 'messageregister/sequence/:testId',
    component: SequenceViewComponent, canActivate: [AuthGuard]
  },
  {
    path: 'messageregister/:id/:testId/:messageId/:createdDT',
    component: MessageregisterDetailComponent, canActivate: [AuthGuard]
  },
  {
    path: 'testing',
    component: TestingsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'admin',
    component: AdminComponent, canActivate: [AuthGuard]
  },
  {
    path: 'testerror/:testId/:id',
    component: TesterrorsDetailComponent, canActivate: [AuthGuard]
  },
  {
    path: 'clientlogs/:testId',
    component: ClientlogDetailComponent, canActivate: [AuthGuard]
  },
  {
    path: 'testgroupview',
    component: TestGroupListComponent, canActivate: [AuthGuard]
  },

];
