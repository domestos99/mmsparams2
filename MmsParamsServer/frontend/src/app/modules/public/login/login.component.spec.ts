import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {LoginComponent} from '@app/modules/public/login/login.component';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {AuthenticationService} from '@app/core/auth';
import {Observable} from 'rxjs';
import {MaterialModule} from '@app/shared/material.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {JwtResponse} from '@app/core/rest/model/jwtResponse';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule, MaterialModule, HttpClientTestingModule],
      declarations: [LoginComponent],
      providers: [
        ComponentServiceContainer,
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: Observable.from([{id: 1}]),
            snapshot: {
              queryParams: {}
            },
          },
        },
        {
          provide: Router,
          useClass: class {
            navigate = jasmine.createSpy('navigate');
          }
        },
        AuthenticationService,

      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should logout onInit', () => {
    const jwtResponse: JwtResponse = {expiry: 123, token: 'xxxyyy'};
    localStorage.setItem(AuthenticationService.CURRENT_USER, JSON.stringify(jwtResponse));
    component.ngOnInit();
    expect(localStorage.getItem(AuthenticationService.CURRENT_USER)).toBeNull();
  });


});
