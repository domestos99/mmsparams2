import {Component} from '@angular/core';
import {Mylogger} from '@app/core/logs';
import {AdminService} from '@app/core/services/admin.service';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';


@Component({
  selector: 'admin',
  templateUrl: 'admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {

  constructor(
    private componentServiceContainer: ComponentServiceContainer,
    private adminService: AdminService) {
  }

  loading = false;

  onRefreshServerCaches() {
    this.loading = true;
    this.adminService.realodDataFromApi()
      .subscribe(value => {
          this.loading = false;
          this.componentServiceContainer.showSnackBar('OK');
        },
        error1 => {
          this.handleError(error1);
        });
  }

  deleteOldTests(value: string) {

    if (confirm('Are you sure you want to delete tests older than ' + value + ' days?')) {
      this.loading = true;
      this.adminService.deleteOldTests(value)
        .subscribe(result => {
          this.loading = false;
          this.componentServiceContainer.showSnackBar('OK');
        }, error1 => {
          this.handleError(error1);
        });
    }
  }

  handleError(error1) {
    this.loading = false;
    Mylogger.logError(error1);
    this.componentServiceContainer.showSnackBar(error1);
  }
}
