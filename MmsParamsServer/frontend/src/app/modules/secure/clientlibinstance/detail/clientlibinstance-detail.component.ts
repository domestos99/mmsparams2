import {Component, Input} from '@angular/core';
import {NavigationselperService} from '@app/core';
import {ClientLibInstanceDTO} from '@app/core/rest/model/clientLibInstanceDTO';
import {ButtonHelper} from '@app/core/helpers/button.helper';


@Component({
  selector: 'clientlibinstance-detail',
  templateUrl: 'clientlibinstance-detail.component.html',
  styleUrls: ['./clientlibinstance-detail.component.scss']
})
export class ClientlibinstanceDetailComponent {

  constructor(private navigationService: NavigationselperService) {

  }

  @Input() data: ClientLibInstanceDTO = {};
  @Input() testID: string;
  detailVisible: boolean = true;

  showClientDetail($event) {
    this.navigationService.openClientDetail(ButtonHelper.inNewTab($event), this.data.clientInfo.clientKey);
  }

  openClientLog($event) {
    this.navigationService.openClientLogDetail(ButtonHelper.inNewTab($event), this.testID);
  }

  toggleDetailView() {
    this.detailVisible = !this.detailVisible;
  }
}
