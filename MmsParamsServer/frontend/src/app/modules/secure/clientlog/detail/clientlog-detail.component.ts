import {Component, OnDestroy, OnInit} from '@angular/core';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {ClientlogService} from '@app/core/services/clientlog.service';
import {Mylogger} from '@app/core/logs';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';
import {ActivatedRoute} from '@angular/router';
import {ClientServerLogDTO} from '@app/core/rest/model/clientServerLogDTO';
import {SecureComponentBase} from '@app/modules/secure/secureComponentBase';

@Component({
  selector: 'clientlog-detail',
  templateUrl: 'clientlog-detail.component.html',
  styleUrls: ['./clientlog-detail.component.scss']
})
export class ClientlogDetailComponent extends SecureComponentBase implements OnInit, OnDestroy {

  constructor(protected componentServiceContainer: ComponentServiceContainer,
              private route: ActivatedRoute,
              private navigationService: NavigationselperService,
              private service: ClientlogService) {
    super(componentServiceContainer);
  }

  private sub: any;
  private testId: string;
  loading = false;
  data: ClientServerLogDTO = {};

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.testId = params['testId'];

      this.reload();
    });
  }

  reload() {
    this.loading = true;
    this.service.getByTestID(this.testId)
      .subscribe(value => {
          this.loading = false;
          this.data = value;
          Mylogger.logData('msg:', this.data);
        },
        error1 => {
          this.loading = false;
          Mylogger.logError(error1);
          this.componentServiceContainer.showSnackBar(error1);
        });
  }

  onDownLoadClick($event: MouseEvent) {
    super.downloadAsJson(this.data, this.testId + '_clientlog');
  }

  onShowTest($event) {
    this.navigationService.openTestInstanceDetail(ButtonHelper.inNewTab($event), this.testId);
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  getLogLevelClass(logLevel): string {
    if (logLevel == 'ERROR') {
      return 'error';
    } else if (logLevel == 'WARNING') {
      return 'warning';
    } else {
      return '';
    }
  }


}
