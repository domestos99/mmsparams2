import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ClientsService} from '@app/core/services/clients.service';
import {ComponentServiceContainer} from "@app/core/services/gui/componentServiceContainer";
import {ClientLibInstanceDTO} from "@app/core/rest/model/clientLibInstanceDTO";
import {Mylogger} from "@app/core/logs";
import {SecureComponentBase} from "@app/modules/secure/secureComponentBase";


@Component({
  selector: 'clients-detail',
  templateUrl: 'clients-detail.component.html'
})
export class ClientsDetailComponent extends SecureComponentBase implements OnInit, OnDestroy {

  constructor(protected componentServiceContainer: ComponentServiceContainer, private route: ActivatedRoute, private clientService: ClientsService) {
    super(componentServiceContainer);
  }

  clientKey: string;
  private sub: any;
  data: ClientLibInstanceDTO;
  loading: boolean = false;


  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.clientKey = params['clientkey'];
      this.reload();
    });
  }

  reload(): void {
    this.loading = true;
    this.clientService.getById(this.clientKey)
      .subscribe(value => {
          Mylogger.logData(value);
          this.data = value;
          this.loading = false;
        },
        error1 => {
          this.loading = false;
          Mylogger.logError(error1);
          this.componentServiceContainer.showSnackBar(error1);
        });
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }


  downloadAsJsonEvent($event: MouseEvent) {
    super.downloadAsJson(this.data, this.clientKey);
  }
}
