import {Component, OnInit} from '@angular/core';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {ClientInfoDTO} from '@app/core/rest/model/clientInfoDTO';
import {ClientsService} from "@app/core/services/clients.service";
import {ComponentServiceContainer} from "@app/core/services/gui/componentServiceContainer";
import {Mylogger} from "@app/core/logs";
import {SecureComponentBase} from "@app/modules/secure/secureComponentBase";


@Component({
  selector: 'clients-list',
  templateUrl: 'clients-list.component.html',
  styleUrls: ['clients-list.component.scss']
})
export class ClientsListComponent extends SecureComponentBase implements OnInit {

  constructor(protected componentServiceContainer: ComponentServiceContainer, private navigationselperService: NavigationselperService, private service: ClientsService) {
    super(componentServiceContainer);
  }

  data: Array<ClientInfoDTO> = [];
  loading: boolean = false;

  ngOnInit(): void {
    this.reload();
  }

  reload() {
    this.loading = true;
    return this.service.getAll()
      .subscribe(value => {
          this.data = value;
          this.loading = false;
        },
        error1 => {
          this.loading = false;
          Mylogger.logError(error1);
          this.componentServiceContainer.showSnackBar(error1);
        });
  }

  openDetail($event, row: ClientInfoDTO) {
    this.navigationselperService.openClientDetail(ButtonHelper.inNewTab($event), row.clientKey);
  }

  forceDisconnect(row: ClientInfoDTO) {
    this.service.postForceDisconnect(row.clientKey).subscribe(resp => {
      this.reload();
    });
  }

  onReload() {
    this.reload();
  }

  downloadAsJsonEvent($event: MouseEvent) {
    super.downloadAsJson(this.data, 'ClientList');
  }


}
