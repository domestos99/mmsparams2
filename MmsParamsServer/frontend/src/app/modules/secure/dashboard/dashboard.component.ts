import {Component, OnInit} from '@angular/core';
import {SystemVersionService} from '@app/core/services/systemVersion.service';
import {SystemVersionModel} from '@app/core/rest/model/systemVersionModel';

@Component({
  selector: 'dashboard',
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  systemVersionModel: SystemVersionModel;

  constructor(private versionService: SystemVersionService) {
    this.systemVersionModel = this.versionService.get();
  }

  ngOnInit(): void {

  }

}
