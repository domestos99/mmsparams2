import {Component, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-test-error-type',
  templateUrl: 'test-error-type.component.html',
  styleUrls: ['./test-error-type.component.scss']
})
export class TestErrorTypeComponent {

  @Input() testErrorType: string;

  constructor(public activeModal: NgbActiveModal) {
  }

  getErrorHelpText(): string {
    if (!this.testErrorType) {
      return '';
    }

    switch (this.testErrorType) {
      case 'SYSTEM_ERROR':
        return 'General error; the test terminated unexpectedly - for example, the connection was broken';
      case 'TEST_ERROR':
        return 'Test run errors - eg sending a message within a closed test';
      case 'WRONGLY_WRITTEN_TEST':
        return 'The wrong parameter was used to call the test service';
      case 'VALIDATIONS':
        return 'Test validations are not valid';
      case 'TIMEOUT':
        return 'Timed out waiting for message to be received';
      case 'TEST_FORCE_CLOSE':
        return 'Test was closed from web interface';
      case 'SMSC':
        return 'SMS center errors (connect, disconnect, send, receive)';
      case 'MMSC':
        return 'MMS center errors (connect, disconnect, send, receive)';
      case 'ANDROID':
        return 'Any error in the mobile app';
      case   'GENERIC_ERROR':
        return 'Generic unexpected error';
    }
    return '';

  }
}
