import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MessageregisterService} from '@app/core/services/messageregister.service';
import {WebSocketMessageDTO} from '@app/core/rest/model/webSocketMessageDTO';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';
import {Mylogger} from '@app/core/logs';
import {TranslateService} from '@app/core/services/translate.service';
import {SecureComponentBase} from '@app/modules/secure/secureComponentBase';

@Component({
  selector: 'messageregister-detail',
  templateUrl: 'messageregister-detail.component.html'
})
export class MessageregisterDetailComponent extends SecureComponentBase implements OnInit, OnDestroy {


  private sub: any;
  private id: number;
  private testId: string;
  private messageId: string;
  private createdDT: number;
  jsonObject: object;
  message: WebSocketMessageDTO;
  loading = false;

  constructor(protected componentServiceContainer: ComponentServiceContainer,
              private route: ActivatedRoute,
              private clientService: MessageregisterService,
              private navigationService: NavigationselperService,
              private translateService: TranslateService) {
    super(componentServiceContainer);
  }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.testId = params['testId'];
      this.messageId = params['messageId'];
      this.createdDT = params['createdDT'];
      this.reload();
    });
  }

  reload() {
    this.loading = true;
    this.clientService.getById(this.id, this.testId, this.messageId, this.createdDT)
      .subscribe(value => {
          this.loading = false;
          this.jsonObject = JSON.parse(value.jsonContent);
          this.message = value;
          Mylogger.logData('msg:', this.message);
        },
        error1 => {
          this.loading = false;
          Mylogger.logError(error1);
          this.componentServiceContainer.showSnackBar(error1);
        });
  }

  translateKey(key: string): string {
    return this.translateService.translate(key);
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  getKeys(data: string) {
    return Object.keys(data);
  }

  onShowTest($event) {
    this.navigationService.openTestInstanceDetail(ButtonHelper.inNewTab($event), this.testId);
  }

  onDownloadClick($event: any) {
    super.downloadAsJson(this.jsonObject, this.messageId + '_messageDetail');
  }
}
