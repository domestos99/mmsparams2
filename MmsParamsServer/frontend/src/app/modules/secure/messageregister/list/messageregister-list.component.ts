import {Component, Input} from '@angular/core';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {WebSocketMessageDTO} from '@app/core/rest/model/webSocketMessageDTO';
import {PhoneInstanceDTO} from '@app/core/rest/model/phoneInstanceDTO';
import {DeviceNameKeyHelper} from '@app/core/helpers/device-name-key.helper';
import {TranslateService} from '@app/core/services/translate.service';
import {MessageErrorHelper} from '@app/core/helpers/messageErrorHelper';


@Component({
  selector: 'messageregister-list',
  templateUrl: 'messageregister-list.component.html',
  styleUrls: ['messageregister-list.component.scss']
})
export class MessageregisterListComponent {


  constructor(private navigationselperService: NavigationselperService,
              private translateService: TranslateService) {
  }

  @Input() data: Array<WebSocketMessageDTO> = [];
  @Input() phones: Array<PhoneInstanceDTO> = [];

  openDetail($event: MouseEvent, row: WebSocketMessageDTO) {
    this.navigationselperService.openWebSocketMessageDetail(ButtonHelper.inNewTab($event), row.id, row.testID, row.messageID, row.createdDT);
  }

  getDeviceNameByKey(key: string): string {
    return DeviceNameKeyHelper.getDeviceNameByKey(this.phones, key);
  }

  showAlert(key: string) {
    alert(key);
  }

  isErrorRow(messageKey: string) {
    return MessageErrorHelper.isErrorRow(messageKey);
  }

  translateKey(key: string): string {
    return this.translateService.translate(key);
  }
}
