import {Component, Input} from '@angular/core';
import {MessageRegisterDTO} from '@app/core/rest/model/messageRegisterDTO';
import {PhoneInstanceDTO} from '@app/core/rest/model/phoneInstanceDTO';
import {ButtonHelper, NavigationselperService} from '@app/core';

@Component({
  selector: 'messageregister',
  templateUrl: 'messageregister.component.html'
})
export class MessageregisterComponent {

  constructor(private navigationService: NavigationselperService) {
  }

  @Input() testID: string;
  @Input() data: MessageRegisterDTO = {};
  @Input() phones: Array<PhoneInstanceDTO> = [];
  detailVisible: boolean = true;


  openMessageSequence($event: MouseEvent) {
    this.navigationService.openMessageSequence(ButtonHelper.inNewTab($event), this.testID);
  }

  toggleDetailView() {
    this.detailVisible = !this.detailVisible;
  }
}
