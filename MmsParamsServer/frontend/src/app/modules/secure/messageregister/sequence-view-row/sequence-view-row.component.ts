import {Component, Input} from '@angular/core';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {WebSocketMessageDTO} from '@app/core/rest/model/webSocketMessageDTO';
import {ActivatedRoute} from '@angular/router';
import {MessageregisterService} from '@app/core/services/messageregister.service';
import {DeviceNameKeyHelper} from '@app/core/helpers/device-name-key.helper';
import {TranslateService} from '@app/core/services/translate.service';
import {PhoneInstanceDTO} from '@app/core/rest/model/phoneInstanceDTO';
import {MessageErrorHelper} from '@app/core/helpers/messageErrorHelper';

@Component({
  selector: 'sequence-view-row',
  templateUrl: 'sequence-view-row.component.html',
  styleUrls: ['./sequence-view-row.component.scss']
})
export class SequenceViewRowComponent {


  constructor(private route: ActivatedRoute, private clientService: MessageregisterService, private navigationService: NavigationselperService,
              private translateService: TranslateService) {
  }

  @Input() message: WebSocketMessageDTO = {};
  @Input() phones: Array<PhoneInstanceDTO> = [];

  getDeviceNameByKey(key: string): string {
    return DeviceNameKeyHelper.getDeviceNameByKey(this.phones, key);
  }

  getKeys(data: string) {
    return Object.keys(data);
  }

  getJson(msg: WebSocketMessageDTO): string {
    return JSON.stringify(msg);
  }

  isFromClient(senderKey: string) {
    return senderKey.startsWith('ClientLib');
  }

  isFromServer(senderKey: string) {
    return 'SERVER' === senderKey;
  }

  isFromPhone(senderKey: string) {
    return !this.isFromClient(senderKey) && !this.isFromServer(senderKey);
  }

  openMessageDetail($event: MouseEvent) {
    this.navigationService.openWebSocketMessageDetail(ButtonHelper.inNewTab($event), this.message.id, this.message.testID, this.message.messageID, this.message.createdDT);
  }

  openPhoneDetail($event: MouseEvent, senderKey: string) {
    this.navigationService.openPhoneDetail(ButtonHelper.inNewTab($event), senderKey);
  }

  translateKey(key: string): string {
    return this.translateService.translate(key);
  }

  isErrorRow(messageKey: string) {
    return MessageErrorHelper.isErrorRow(messageKey);
  }
}
