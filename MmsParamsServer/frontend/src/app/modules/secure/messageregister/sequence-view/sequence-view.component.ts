import {Component, OnDestroy, OnInit} from '@angular/core';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {ActivatedRoute} from '@angular/router';
import {MessageregisterService} from '@app/core/services/messageregister.service';
import {MessageSequenceList} from '@app/core/rest/model/messageSequenceList';
import {Mylogger} from '@app/core/logs';
import {PhonesService} from '@app/core/services/phones.service';
import {SecureComponentBase} from '@app/modules/secure/secureComponentBase';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';

@Component({
  selector: 'sequence-view',
  templateUrl: 'sequence-view.component.html'
})
export class SequenceViewComponent extends SecureComponentBase implements OnInit, OnDestroy {

  private sub: any;
  private testId: string;
  messageSequenceList: MessageSequenceList;
  loading: boolean = false;

  constructor(protected componentServiceContainer: ComponentServiceContainer,
              private route: ActivatedRoute,
              private clientService: MessageregisterService,
              private phoneService: PhonesService,
              private navigationService: NavigationselperService
  ) {
    super(componentServiceContainer);
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.testId = params['testId'];
      this.reload();
    });
  }

  reload() {
    this.loading = true;
    this.clientService.getSequenceMessages(this.testId)
      .subscribe(value => {
          Mylogger.logData(value);
          this.messageSequenceList = value;
          this.loading = false;
        },
        error1 => {
          this.loading = false;
          Mylogger.logError(error1);
          this.componentServiceContainer.showSnackBar(error1);
        });

  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  onShowTest($event: MouseEvent) {
    this.navigationService.openTestInstanceDetail(ButtonHelper.inNewTab($event), this.testId);
  }

  onDownLoadClick($event: MouseEvent) {
    super.downloadAsJson(this.messageSequenceList, this.testId + '_messagesequence');
  }
}
