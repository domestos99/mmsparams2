import {Component, Input} from '@angular/core';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {PhoneInstanceDTO} from '@app/core/rest/model/phoneInstanceDTO';

@Component({
  selector: 'phoneinstance-list',
  templateUrl: 'phoneinstance-list.component.html'
})
export class PhoneinstanceListComponent {

  constructor(private navigationselperService: NavigationselperService) {
  }


  @Input() data: Array<PhoneInstanceDTO> = [];
  detailVisible: boolean = true;

  showSessionDetail($event: MouseEvent, sessionId: string) {
    this.navigationselperService.openSessionDetail(ButtonHelper.inNewTab($event), sessionId);
  }


  showPhoneDetail($event: MouseEvent, phoneKey: string) {
    this.navigationselperService.openPhoneDetail(ButtonHelper.inNewTab($event), phoneKey);
  }

  openDetail2($event: MouseEvent, row: PhoneInstanceDTO) {
    this.showPhoneDetail($event, row.phoneInfo.phoneKey);
  }

  toggleDetailView() {
    this.detailVisible = !this.detailVisible;
  }
}
