import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PhoneInfoDTO} from '@app/core/rest/model/phoneInfoDTO';
import {PhonesService} from '@app/core/services/phones.service';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';
import {Mylogger} from '@app/core/logs';
import {SecureComponentBase} from '@app/modules/secure/secureComponentBase';

@Component({
  selector: 'phones-detail',
  templateUrl: 'phones-detail.component.html',
  styleUrls: ['phones-detail.component.scss']
})
export class PhonesDetailComponent extends SecureComponentBase implements OnInit, OnDestroy {


  constructor(protected componentServiceContainer: ComponentServiceContainer,
              private route: ActivatedRoute,
              private phoneService: PhonesService) {
    super(componentServiceContainer);

  }

  @Input() dataInput: PhoneInfoDTO;
  loading: boolean = false;

  phonekey: string;
  private sub: any;
  data: PhoneInfoDTO;


  ngOnInit() {

    if (this.dataInput) {
      this.data = this.dataInput;
    } else {
      this.sub = this.route.params.subscribe(params => {
        this.phonekey = params['phonekey'];
        this.reload();
      });
    }
  }

  reload(): void {
    if (this.dataInput) {
      return;
    } else {
      // In a real app: dispatch action to load the details here.
      this.loading = true;
      this.phoneService.getById(this.phonekey)
        .subscribe(value => {
            Mylogger.logData(value);
            this.data = value;
            this.loading = false;
          },
          error1 => {
            this.loading = false;
            Mylogger.logError(error1);
            this.componentServiceContainer.showSnackBar(error1);
          });
    }
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  downloadAsJsonEvent($event: MouseEvent) {
    super.downloadAsJson(this.data, 'PhoneDetail_' + this.phonekey);
  }
}
