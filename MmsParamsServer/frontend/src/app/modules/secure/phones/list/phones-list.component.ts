import {Component, OnInit} from '@angular/core';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {PhoneInfoDTO} from '@app/core/rest/model/phoneInfoDTO';
import {PhonesService} from "@app/core/services/phones.service";
import {ComponentServiceContainer} from "@app/core/services/gui/componentServiceContainer";
import {Mylogger} from "@app/core/logs";
import {SecureComponentBase} from "@app/modules/secure/secureComponentBase";

@Component({
  selector: 'phones-list',
  templateUrl: 'phones-list.component.html'
})
export class PhonesListComponent extends SecureComponentBase implements OnInit {

  data: Array<PhoneInfoDTO> = [];
  loading: boolean = false;

  constructor(protected componentServiceContainer: ComponentServiceContainer,
              private navigationselperService: NavigationselperService,
              private phonesService: PhonesService) {
    super(componentServiceContainer);

  }

  ngOnInit(): void {
    this.reload();
  }

  reload() {
    this.loading = true;
    return this.phonesService.getAll()
      .subscribe(value => {
          this.data = value;
          this.loading = false;
        },
        error1 => {
          this.loading = false;
          Mylogger.logError(error1);
          this.componentServiceContainer.showSnackBar(error1);
        });
  }


  openDetail($event, row: PhoneInfoDTO) {
    this.navigationselperService.openPhoneDetail(ButtonHelper.inNewTab($event), row.phoneKey);
  }


  forceDisconnect(row: PhoneInfoDTO) {
    this.phonesService.postForceDisconnect(row.phoneKey).subscribe(resp => {
      this.reload();
    });

  }

  onReload() {
    this.reload();
  }

  downloadAsJsonEvent($event: MouseEvent) {
    super.downloadAsJson(this.data, 'PhonesList');
  }


}
