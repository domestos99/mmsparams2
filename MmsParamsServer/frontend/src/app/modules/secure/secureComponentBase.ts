import {ComponentBase} from '@app/core/base/componentBase';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';

export abstract class SecureComponentBase extends ComponentBase {

  constructor(protected componentServiceContainer: ComponentServiceContainer) {
    super();
  }

}
