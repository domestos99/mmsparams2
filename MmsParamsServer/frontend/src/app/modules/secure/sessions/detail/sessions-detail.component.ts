import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SessionsService} from '@app/core/services/sessions.service';
import {WSSessionDTO} from '@app/core/rest/model/wSSessionDTO';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';
import {Mylogger} from '@app/core/logs';
import {SecureComponentBase} from '@app/modules/secure/secureComponentBase';

@Component({
  selector: 'sessions-detail',
  templateUrl: 'sessions-detail.component.html'
})
export class SessionsDetailComponent extends SecureComponentBase implements OnInit, OnDestroy {


  constructor(protected componentServiceContainer: ComponentServiceContainer,
              private route: ActivatedRoute,
              private clientService: SessionsService) {
    super(componentServiceContainer);
  }

  @Input() dateInput: WSSessionDTO;

  sessionID: string;
  private sub: any;
  data: WSSessionDTO;
  loading: boolean = false;


  ngOnInit() {
    if (this.dateInput) {
      this.data = this.dateInput;
    } else {
      this.sub = this.route.params.subscribe(params => {
        this.sessionID = params['id'];
        this.reload();
      });
    }
  }

  reload(): void {
    if (this.dateInput) {
      return;
    } else {
      // In a real app: dispatch action to load the details here.
      this.loading = true;
      this.clientService.getById(this.sessionID)
        .subscribe(value => {
            Mylogger.logData(value);
            this.data = value;
            this.loading = false;
          },
          error1 => {
            this.loading = false;
            Mylogger.logError(error1);
            this.componentServiceContainer.showSnackBar(error1);
          });
    }
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  downloadAsJsonEvent($event: MouseEvent) {
    super.downloadAsJson(this.data, 'Session_' + this.sessionID);
  }
}
