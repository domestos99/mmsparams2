import {Component, OnInit} from '@angular/core';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {SessionsService} from '@app/core/services/sessions.service';
import {WSSessionDTO} from '@app/core/rest/model/wSSessionDTO';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';
import {Mylogger} from '@app/core/logs';
import {SecureComponentBase} from '@app/modules/secure/secureComponentBase';

@Component({
  selector: 'sessions-list',
  templateUrl: 'sessions-list.component.html',
})
export class SessionsListComponent extends SecureComponentBase implements OnInit {

  constructor(protected componentServiceContainer: ComponentServiceContainer,
              private navigationselperService: NavigationselperService,
              private service: SessionsService) {
    super(componentServiceContainer);
  }

  ngOnInit(): void {
    this.reload();
  }

  data: Array<WSSessionDTO> = [];
  loading: boolean = false;

  openDetail($event: MouseEvent, row: WSSessionDTO) {
    this.navigationselperService.openSessionDetail(ButtonHelper.inNewTab($event), row.id);
  }

  reload() {
    this.loading = true;
    return this.service.getAll()
      .subscribe(value => {
          this.data = value;
          this.loading = false;
        },
        error1 => {
          this.loading = false;
          Mylogger.logError(error1);
          this.componentServiceContainer.showSnackBar(error1);
        });
  }

  onReload() {
    this.reload();
  }

  downloadAsJsonEvent($event: MouseEvent) {
    super.downloadAsJson(this.data, 'SessionsList');
  }


}
