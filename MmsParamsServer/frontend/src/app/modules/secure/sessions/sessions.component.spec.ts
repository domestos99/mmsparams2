import {SessionsComponent} from "@app/modules/secure/sessions/sessions.component";

describe('SessionsComponent (isolated test)', () => {
  it('should instantiate', () => {
    const component: SessionsComponent = new SessionsComponent();
    expect(component).toBeDefined();
  });
});
