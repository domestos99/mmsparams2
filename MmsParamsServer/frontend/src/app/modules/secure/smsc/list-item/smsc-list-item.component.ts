import {Component, Input} from '@angular/core';
import {NavigationselperService} from '@app/core';
import {SmscInstanceDTO} from '@app/core/rest/model/smscInstanceDTO';


@Component({
  selector: 'smsc-list-item',
  templateUrl: 'smsc-list-item.component.html',
  styleUrls: ['./smsc-list-item.component.scss']
})
export class SmscListItemComponent {

  constructor(private navigationselperService: NavigationselperService) {
  }

  @Input() row: SmscInstanceDTO = {};


}
