import {Component, Input} from '@angular/core';
import {NavigationselperService} from '@app/core';
import {SmscInstanceDTO} from '@app/core/rest/model/smscInstanceDTO';

@Component({
  selector: 'smsc-list',
  templateUrl: 'smsc-list.component.html',
  styleUrls: ['./smsc-list.component.scss']
})
export class SmscListComponent {

  constructor(private navigationselperService: NavigationselperService) {
  }

  @Input() data: Array<SmscInstanceDTO> = [];
  detailVisible: boolean = true;


  toggleDetailView() {
    this.detailVisible = !this.detailVisible;
  }
}
