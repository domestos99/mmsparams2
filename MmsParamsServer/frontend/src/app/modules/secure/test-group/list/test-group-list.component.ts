import {Component, OnInit} from '@angular/core';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {TestGroupViewService} from '@app/core/services/testGroupView.service';
import {Mylogger} from '@app/core/logs';
import {TestInstanceTestGroupView} from '@app/core/rest/model/testInstanceTestGroupView';
import {SecureComponentBase} from '@app/modules/secure/secureComponentBase';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {MatTableDataSource} from '@angular/material/table';
import {
  TestGroupListFilterModel,
  TestGroupListFilterService
} from '@app/core/services/filters/testGroupListFilterService';


@Component({
  selector: 'test-group-list',
  templateUrl: 'test-group-list.component.html',
  styleUrls: ['./test-group-list.component.scss']
})
export class TestGroupListComponent extends SecureComponentBase implements OnInit {

  constructor(protected componentServiceContainer: ComponentServiceContainer,
              private navigationselperService: NavigationselperService,
              private testGroupViewService: TestGroupViewService,
              private filtersService: TestGroupListFilterService) {
    super(componentServiceContainer);
  }

  hideme = [];
  data: Array<TestInstanceTestGroupView> = [];
  dataSource: MatTableDataSource<TestInstanceTestGroupView> = new MatTableDataSource();

  loading = false;
  allVisible = false;
  filters: TestGroupListFilterModel;

  ngOnInit(): void {
    this.loadFilters();
    this.reload();
  }

  private loadFilters() {

    this.dataSource.filterPredicate = (data1, filter) => {
      return data1.testGroupModelDTO.testGroupName.toLowerCase().includes(filter) ||
        data1.testGroupModelDTO.testGroupID.toLowerCase().includes(filter) ||
        data1.testGroupModelDTO.testGroupDesc.toLowerCase().includes(filter);
    };

    this.filters = this.filtersService.getFiltersOrDefault();
    this.applyFilter(this.filters.searchString);
  }

  reload() {
    this.loading = true;
    this.hideme = [];
    return this.testGroupViewService.getAll()
      .subscribe(value => {
          this.data = value;
          this.setDataSource();
          this.loading = false;
        },
        error1 => {
          this.loading = false;
          Mylogger.logError(error1);
          this.componentServiceContainer.showSnackBar(error1);
        });
  }

  private setDataSource() {
    this.dataSource.data = this.data.filter(value => {
      return true;
    });
  }

  openDetail($event: MouseEvent, testId: string) {
    this.navigationselperService.openTestInstanceDetail(ButtonHelper.inNewTab($event), testId);
  }

  applyFilterInput(searchFilterInput) {
    this.applyFilter(searchFilterInput.value);
  }

  applyFilter(filterValue: string) {
    const filterStr = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterStr;
    this.filters.searchString = filterStr;
    this.resolveFilterSave();
  }

  onRememberFiltersChange($event: MatCheckboxChange) {
    this.filters.rememberFilters = $event.checked;
    this.resolveFilterSave();
  }

  private resolveFilterSave() {
    if (this.filters.rememberFilters) {
      this.filtersService.saveFilters(this.filters);
    } else {
      this.filtersService.removeFilters();
    }
  }
}
