import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TestErrorDTO} from '@app/core/rest/model/testErrorDTO';
import {TestErrorService} from '@app/core/services/test-error.service';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';
import {ActivatedRoute} from '@angular/router';
import {Mylogger} from '@app/core/logs';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {SecureComponentBase} from '@app/modules/secure/secureComponentBase';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TestErrorTypeComponent} from '@app/modules/secure/help/test-error-type/test-error-type.component';


@Component({
  selector: 'testerrors-detail',
  templateUrl: 'testerrors-detail.component.html',
  styleUrls: ['./testerrors-detail.component.scss']
})
export class TesterrorsDetailComponent extends SecureComponentBase implements OnInit, OnDestroy {

  constructor(protected componentServiceContainer: ComponentServiceContainer,
              private route: ActivatedRoute,
              private navigationService: NavigationselperService,
              private service: TestErrorService,
              private modalService: NgbModal) {
    super(componentServiceContainer);
  }

  @Input() data: TestErrorDTO = {};

  testErrorID: string;
  private testId: string;
  private sub: any;
  loading = false;


  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.testErrorID = params['id']; // (+) converts string 'id' to a number
      this.testId = params['testId'];
      this.reload();
    });
  }

  reload() {
    this.loading = true;
    this.service.getById(this.testErrorID)
      .subscribe(value => {
          Mylogger.logData(value);
          this.data = value;
          this.loading = false;
        },
        error1 => {
          this.loading = false;
          Mylogger.logError(error1);
          this.componentServiceContainer.showSnackBar(error1);
        });
  }

  onShowTest($event: MouseEvent) {
    this.navigationService.openTestInstanceDetail(ButtonHelper.inNewTab($event), this.testId);
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  onDownloadClick($event: MouseEvent) {
    super.downloadAsJson(this.data, this.testId + '_' + this.testErrorID + '_errordetail');
  }

  onTestErrorTypeDetail() {
    const modalRef = this.modalService.open(TestErrorTypeComponent, {centered: true});
    modalRef.componentInstance.testErrorType = this.data.testErrorType;
  }
}
