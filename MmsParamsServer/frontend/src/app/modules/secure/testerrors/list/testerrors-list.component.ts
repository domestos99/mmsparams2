import {Component, Input} from '@angular/core';
import {TestErrorDTO} from '@app/core/rest/model/testErrorDTO';
import {ButtonHelper, NavigationselperService} from '@app/core';
import {TestErrorTypeComponent} from '@app/modules/secure/help/test-error-type/test-error-type.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'testerrors-list',
  templateUrl: 'testerrors-list.component.html',
  styleUrls: ['./testerrors-list.component.scss']
})
export class TesterrorsListComponent {

  constructor(private navigationselperService: NavigationselperService,
              private modalService: NgbModal) {
  }

  @Input() data: Array<TestErrorDTO> = [];
  @Input() testId: string;
  detailVisible: boolean = true;


  showStackTrace(row: TestErrorDTO) {
    this.openString(row.exception);
  }

  openString(s: string) {
    this.openNewTabWithData(s);
  }

  private openNewTabWithData(_data: any) {
    const tab = window.open('about:blank', '_blank');
    tab.document.write(_data); // where 'html' is a variable containing your HTML
    tab.document.close(); // to finish loading the page
  }

  openDetail($event: MouseEvent, row: TestErrorDTO) {
    this.navigationselperService.openTestErrorDetail(ButtonHelper.inNewTab($event), this.testId, row.id);
  }

  toggleDetailView() {
    this.detailVisible = !this.detailVisible;
  }

  onTestErrorTypeDetail(_testErrorType) {
    const modalRef = this.modalService.open(TestErrorTypeComponent, {centered: true});
    modalRef.componentInstance.testErrorType = _testErrorType;
  }
}
