import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TestinstancesService} from '@app/core/services/testinstances.service';
import {ServerTestInstanceDetailDTO} from '@app/core/rest/model/serverTestInstanceDetailDTO';
import {Mylogger} from '@app/core/logs';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';
import {NavigationselperService} from '@app/core';
import {SecureComponentBase} from '@app/modules/secure/secureComponentBase';

@Component({
  selector: 'testinstances-detail',
  templateUrl: 'testinstances-detail.component.html',
  styleUrls: ['./testinstances-detail.component.scss']
})
export class TestinstancesDetailComponent extends SecureComponentBase implements OnInit, OnDestroy {

  constructor(protected componentServiceContainer: ComponentServiceContainer,
              private navigationService: NavigationselperService,
              private route: ActivatedRoute,
              private service: TestinstancesService) {
    super(componentServiceContainer);
  }

  testId: string;
  private sub: any;
  data: ServerTestInstanceDetailDTO = {};
  loading: boolean = false;
  detailVisible: boolean = true;


  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.testId = params['testId'];
      this.reload();
    });
  }

  reload() {
    this.loading = true;
    this.service.getById(this.testId)
      .subscribe(value => {
          this.data = value;
          this.loading = false;
        },
        error1 => {
          this.loading = false;
          Mylogger.logError(error1);
          this.componentServiceContainer.showSnackBar(error1);
        });
  }

  onRefresh() {
    this.reload();
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  downloadAsJsonEvent($event: MouseEvent) {
    super.downloadAsJson(this.data, this.testId);
  }

  toggleDetailView() {
    this.detailVisible = !this.detailVisible;
  }
}
