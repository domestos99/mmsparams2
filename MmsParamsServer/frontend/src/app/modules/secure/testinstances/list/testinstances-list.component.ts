import {Component, Input, OnInit} from '@angular/core';
import {NavigationselperService} from '@app/core';
import {ServerTestInstanceDTO} from '@app/core/rest/model/serverTestInstanceDTO';
import {TestinstancesService} from '@app/core/services/testinstances.service';
import {ComponentServiceContainer} from '@app/core/services/gui/componentServiceContainer';
import {Mylogger} from '@app/core/logs';
import {YesNoAllEnum} from '@app/core/model/yesNoAll.enum';
import {ButtonHelper} from '@app/core/helpers/button.helper';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {MatTableDataSource} from '@angular/material/table';
import {
  TestInstanceListFilterModel,
  TestInstanceListFilterService
} from '@app/core/services/filters/testInstanceListFilter.service';


@Component({
  selector: 'testinstances-list',
  templateUrl: 'testinstances-list.component.html',
  styleUrls: ['./testinstances-list.component.scss']
})
export class TestinstancesListComponent implements OnInit {


  constructor(private componentServiceContainer: ComponentServiceContainer,
              private navigationselperService: NavigationselperService,
              private service: TestinstancesService,
              private filtersService: TestInstanceListFilterService) {
  }

  displayedColumns = [
    'detail',
    'testName',
    'hasErrors',
    'hasValidationErrors',
    'createdDT',
    'testId',
    'closed',
    'delete'
  ];

  data: Array<ServerTestInstanceDTO> = [];
  dataSource: MatTableDataSource<ServerTestInstanceDTO> = new MatTableDataSource();


  @Input() isOpen: boolean;
  @Input() showFilters = true;
  loading = false;
  filters: TestInstanceListFilterModel;

  ngOnInit(): void {
    this.loadFilters();
    this.reload();
  }

  private loadFilters() {
    this.filters = this.filtersService.getFiltersOrDefault();
    this.applyFilter(this.filters.searchString);
  }

  reload() {
    this.loading = true;
    return this.service.getAll(this.isOpen)
      .subscribe(value => {
          Mylogger.logData(value);
          this.data = value;
          this.setDataSource();
          this.loading = false;
        },
        error1 => {
          this.loading = false;
          Mylogger.logError(error1);
          this.componentServiceContainer.showSnackBar(error1);
        });
  }


  openDetail($event: MouseEvent, row: ServerTestInstanceDTO) {
    this.navigationselperService.openTestInstanceDetail(ButtonHelper.inNewTab($event), row.testId);
  }

  applyFilterInput(searchFilterInput: any) {
    this.applyFilter(searchFilterInput.value);
  }

  applyFilter(filterValue: string) {
    const filterStr = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterStr;
    this.filters.searchString = filterStr;
    this.resolveFilterSave();
  }

  deleteRow(row: ServerTestInstanceDTO) {

    if (confirm('Are you sure?')) {
      this.service.delete2(row.testId)
        .subscribe(resp => {
          this.reload();
        });
    }
  }

  onErrorsFilterChange(value) {
    this.filters.hasErrorsFilter = value;
    this.resolveFilterSave();
    this.setDataSource();
  }

  onValidationErrorFilterChange(value) {
    this.filters.hasValidationErrorsFilter = value;
    this.resolveFilterSave();
    this.setDataSource();
  }

  private setDataSource() {

    this.dataSource.data = this.data.filter(value => {
      return this.resolveFilter(value);
    });
  }


  resolveFilter(value: ServerTestInstanceDTO) {
    let result = true;

    if (this.filters.hasErrorsFilter === YesNoAllEnum.YES) {
      result = result && !value.hasErrors;
    }

    if (this.filters.hasValidationErrorsFilter === YesNoAllEnum.YES) {
      result = result && (value.validationErrors !== ServerTestInstanceDTO.ValidationErrorsEnum.YES)
        && (value.validationErrors !== ServerTestInstanceDTO.ValidationErrorsEnum.NAN);
    }

    if (this.filters.hasErrorsFilter === YesNoAllEnum.NO) {
      result = result && value.hasErrors;
    }

    if (this.filters.hasValidationErrorsFilter === YesNoAllEnum.NO) {
      result = result && (value.validationErrors !== ServerTestInstanceDTO.ValidationErrorsEnum.NO)
        && (value.validationErrors !== ServerTestInstanceDTO.ValidationErrorsEnum.NAN);
    }
    return result;
  }


  onRememberFiltersChange($event: MatCheckboxChange) {
    this.filters.rememberFilters = $event.checked;
    this.resolveFilterSave();
  }


  private resolveFilterSave() {
    if (this.filters.rememberFilters) {
      this.filtersService.saveFilters(this.filters);
    } else {
      this.filtersService.removeFilters();
    }
  }
}
