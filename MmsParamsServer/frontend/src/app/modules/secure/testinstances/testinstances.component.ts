import {Component, ViewChild} from '@angular/core';

@Component({
  selector: 'testinstances',
  templateUrl: 'testinstances.component.html'
})
export class TestinstancesComponent {

  @ViewChild('listOpen') listOpen;
  @ViewChild('listClose') listClose;


  onRefresh() {
    this.listOpen.reload();
    this.listClose.reload();
  }
}
