import {Component, Input} from '@angular/core';
import {ObjectHelper} from '@app/core/helpers/objectHelper';
import {ValidationResultDTO} from '@app/core/rest/model/validationResultDTO';


@Component({
  selector: 'validationresult-list',
  templateUrl: 'validationresult-list.component.html',
  styleUrls: ['./validationresult-list.component.scss']
})
export class ValidationresultListComponent {

  @Input() data: Array<ValidationResultDTO> = [];

  detailVisible: boolean = true;

  isObject(value: any): boolean {
    return ObjectHelper.isObject(value);
  }

  isLongArray(value: any): boolean {
    return ObjectHelper.isLongArray(value);
  }

  toggleDetailView() {
    this.detailVisible = !this.detailVisible;
  }
}
