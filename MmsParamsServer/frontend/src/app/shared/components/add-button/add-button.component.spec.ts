import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AddButtonComponent} from '@app/shared/components/add-button/add-button.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('AddButtonComponent', () => {
  let component: AddButtonComponent;
  let fixture: ComponentFixture<AddButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddButtonComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('on click', () => {
    let result: any;
    component.onClick.subscribe(value => {
      result = value;
    });
    component.onButtonClick('hello');
    expect(result).toBe('hello');
  });


});
