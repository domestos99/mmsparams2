import {Component, EventEmitter, Output} from '@angular/core';


@Component({
  selector: 'add-button',
  templateUrl: 'add-button.component.html'
})
export class AddButtonComponent {

  @Output() onClick: EventEmitter<any> = new EventEmitter();

  onButtonClick($event) {
    this.onClick.emit($event);
  }

}
