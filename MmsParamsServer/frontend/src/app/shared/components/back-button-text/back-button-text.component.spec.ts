import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BackButtonTextComponent} from '@app/shared/components/back-button-text/back-button-text.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('BackButtonTextComponent', () => {
  let component: BackButtonTextComponent;
  let fixture: ComponentFixture<BackButtonTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BackButtonTextComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackButtonTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
