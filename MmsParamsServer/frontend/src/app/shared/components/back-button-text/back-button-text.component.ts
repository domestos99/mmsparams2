import {Component, EventEmitter, Input, Output} from '@angular/core';


@Component({
  selector: 'back-button-text',
  templateUrl: 'back-button-text.component.html',
  styleUrls: ['./back-button-text.component.scss']
})
export class BackButtonTextComponent {


  @Input() text: string;
  @Output() onClick: EventEmitter<MouseEvent> = new EventEmitter();

  onButtonClick($event: MouseEvent) {
    this.onClick.emit($event);
  }

}
