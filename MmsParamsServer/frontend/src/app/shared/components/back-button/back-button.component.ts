import {Component, Input, Output, EventEmitter, ViewChild} from '@angular/core';


@Component({
  selector: 'back-button',
  templateUrl: 'back-button.component.html',
  styleUrls: ['./back-button.component.scss']
})
export class BackButtonComponent {

  @Output() onClick: EventEmitter<any> = new EventEmitter();

  onButtonClick($event: MouseEvent) {
    this.onClick.emit($event);
  }
}
