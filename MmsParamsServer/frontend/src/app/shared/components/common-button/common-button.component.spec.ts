import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CommonButtonComponent} from '@app/shared/components/common-button/common-button.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('CommonButtonComponent', () => {
  let component: CommonButtonComponent;
  let fixture: ComponentFixture<CommonButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommonButtonComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
