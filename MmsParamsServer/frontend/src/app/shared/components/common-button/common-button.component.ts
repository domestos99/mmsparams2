import {Component, EventEmitter, Input, Output} from '@angular/core';


@Component({
  selector: 'common-button',
  templateUrl: 'common-button.component.html',
  styleUrls: ['./common-button.component.scss']
})
export class CommonButtonComponent {


  // @Input() text: string;
  @Output() onClick: EventEmitter<MouseEvent> = new EventEmitter();

  onButtonClick($event: MouseEvent) {
    this.onClick.emit($event);
  }

}
