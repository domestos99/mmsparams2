import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CommonButtonComponent} from '@app/shared/components/common-button/common-button.component';
import {DetailButtonComponent} from '@app/shared/components/detailbutton/detail-button.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('DetailButtonComponent', () => {
  let component: DetailButtonComponent;
  let fixture: ComponentFixture<DetailButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DetailButtonComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
