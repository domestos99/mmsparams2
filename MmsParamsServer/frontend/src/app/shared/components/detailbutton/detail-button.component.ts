import {Component, EventEmitter, Output} from '@angular/core';


@Component({
  selector: 'detail-button',
  templateUrl: 'detail-button.component.html',
  styleUrls: ['./detail-button.component.scss']
})
export class DetailButtonComponent {


  @Output() onClick: EventEmitter<MouseEvent> = new EventEmitter();

  onButtonClick($event: MouseEvent) {
    this.onClick.emit($event);
  }

}
