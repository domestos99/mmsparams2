import {Component, EventEmitter, Output} from '@angular/core';


@Component({
  selector: 'download-button',
  templateUrl: 'download-button.component.html',
  styleUrls: ['./download-button.component.scss']
})
export class DownloadButtonComponent {

  @Output() onClick: EventEmitter<any> = new EventEmitter();

  onButtonClick($event: MouseEvent) {
    this.onClick.emit($event);
  }
}
