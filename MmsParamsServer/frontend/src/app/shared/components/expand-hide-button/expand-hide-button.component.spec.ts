import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ExpandHideButtonComponent} from "@app/shared/components/expand-hide-button/expand-hide-button.component";

describe('ExpandHideButtonComponent', () => {
  let component: ExpandHideButtonComponent;
  let fixture: ComponentFixture<ExpandHideButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExpandHideButtonComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpandHideButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('on click', () => {
    let result: any;
    component.onClick.subscribe(value => {
      result = value;
    });
    component.onButtonClick('hello');
    expect(result).toBe('hello');
  });


});
