import {Component, EventEmitter, Input, Output} from '@angular/core';


@Component({
  selector: 'expand-hide-button',
  templateUrl: 'expand-hide-button.component.html',
  styleUrls: ['./expand-hide-button.component.scss']
})
export class ExpandHideButtonComponent {

  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Input() isExpanded: boolean = true;


  onButtonClick($event) {
    this.onClick.emit($event);
  }

}
