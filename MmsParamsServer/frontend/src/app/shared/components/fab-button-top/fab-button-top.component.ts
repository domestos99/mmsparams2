import {Component, OnDestroy, OnInit} from '@angular/core';


@Component({
  selector: 'app-fab-button-top',
  templateUrl: 'fab-button-top.component.html',
  styleUrls: ['fab-button-top.component.scss']
})
export class FabButtonTopComponent implements OnInit, OnDestroy {

  isButtonVisible = false;

  constructor() {
  }

  scrollToTop() {
    window.scroll({top: 0, left: 0, behavior: 'smooth'});
  }

  ngOnInit(): void {
    window.addEventListener('scroll', this.handleScroll, true);
  }

  handleScroll = (event): void => {
    this.isButtonVisible = document.body.scrollTop > 20 || document.documentElement.scrollTop > 20;
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.handleScroll, true);
  }
}
