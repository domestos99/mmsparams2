import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {EditButtonComponent} from '@app/shared/components/edit-button/edit-button.component';
import {HelpButtonComponent} from '@app/shared/components/help-button/help-button.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('EditButtonComponent', () => {
  let component: HelpButtonComponent;
  let fixture: ComponentFixture<HelpButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HelpButtonComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
