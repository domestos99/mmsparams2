import {Component, Input, Output, EventEmitter, ViewChild} from '@angular/core';


@Component({
  selector: 'help-button',
  templateUrl: 'help-button.component.html'
})
export class HelpButtonComponent {


  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Input() toolTip: string;

  onButtonClick() {
    this.onClick.emit(null);
  }

}
