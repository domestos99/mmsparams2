import {Component, Input, Output, EventEmitter, ViewChild, OnInit} from '@angular/core';
import {ObjectHelper} from '@app/core/helpers/objectHelper';
import {Mylogger} from "@app/core/logs";


@Component({
  selector: 'jsontable',
  templateUrl: 'jsontable.component.html'
})
export class JsontableComponent implements OnInit {


  @Input() jsondata: object = [];


  getKeys(data: any) {
    return Object.keys(data);
  }

  isObject(jsondatum: any) {
    return ObjectHelper.isObject(jsondatum);
  }

  isLongArray(jsondatum: any) {
    return ObjectHelper.isLongArray(jsondatum);
  }

  ngOnInit(): void {
    Mylogger.logData('jsontable:', this.jsondata);
  }
}
