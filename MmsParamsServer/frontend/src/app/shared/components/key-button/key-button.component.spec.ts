import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {JsontableComponent} from '@app/shared/components/jsontable/jsontable.component';
import {KeyButtonComponent} from '@app/shared/components/key-button/key-button.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('KeyButtonComponent', () => {
  let component: KeyButtonComponent;
  let fixture: ComponentFixture<KeyButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KeyButtonComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
