import {Component, Input, Output, EventEmitter, ViewChild} from '@angular/core';


@Component({
  selector: 'key-button',
  templateUrl: 'key-button.component.html',
  styleUrls: ['./key-button.component.scss']
})
export class KeyButtonComponent {


  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Input() toolTip: string;

  onButtonClick() {
    this.onClick.emit(null);
  }

}
