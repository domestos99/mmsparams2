import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LongdatadisplayComponent} from '@app/shared/components/longdatadisplay/longdatadisplay.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('LongdatadisplayComponent', () => {
  let component: LongdatadisplayComponent;
  let fixture: ComponentFixture<LongdatadisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LongdatadisplayComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LongdatadisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
