import {Component, Input} from '@angular/core';

@Component({
  selector: 'longdatadisplay',
  templateUrl: 'longdatadisplay.component.html',
  styleUrls: ['./longdatadisplay.component.scss']
})

export class LongdatadisplayComponent {

  @Input() data: any;

  getBase64() {
    let binary = '';
    const bytes = new Uint8Array(this.data);
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  openString() {
    try {
      const result = String.fromCharCode.apply(null, this.data);
      this.openNewTabWithData(result);
      return;
    } catch (e) {
    }
    this.openByteArray();
  }

  openDownloadString() {
    try {
      const result = String.fromCharCode.apply(null, this.data);
      this.initDownload(result);
      return;
    } catch (e) {
      console.error(e);
    }
    this.openDownloadByteArray();
  }

  openByteArray() {
    this.openNewTabWithData(this.data);
  }

  openBase64() {
    this.openNewTabWithData(this.getBase64());
  }

  openImage() {
    const image = new Image();
    image.src = 'data:image/jpg;base64,' + this.getBase64();
    this.openNewTabWithData(image.outerHTML);
  }

  openDownloadByteArray() {
    this.initDownload(this.data);
  }

  openDownloadBase64() {
    this.initDownload(this.getBase64());
  }

  private initDownload(_data: any) {

    const blob = new Blob([_data]);

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, 'file');
    } else {
      var a = document.createElement('a');
      a.href = URL.createObjectURL(blob);
      a.download = 'file';
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }

  }


  private openNewTabWithData(_data: any) {
    var tab = window.open('about:blank', '_blank');
    tab.document.write(_data); // where 'html' is a variable containing your HTML
    tab.document.close(); // to finish loading the page
  }


}
