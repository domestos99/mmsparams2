import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {LongdataprotectComponent} from '@app/shared/components/longdataprotect/longdataprotect.component';

describe('LongdataprotectComponent', () => {
  let component: LongdataprotectComponent;
  let fixture: ComponentFixture<LongdataprotectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LongdataprotectComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LongdataprotectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
