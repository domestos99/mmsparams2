import {Component, Input} from '@angular/core';
import {ObjectHelper} from '@app/core';


@Component({
  selector: 'longdataprotect',
  templateUrl: 'longdataprotect.component.html'
})
export class LongdataprotectComponent {

  @Input() jsondata: any = [];
  @Input() limit: number = 300;

  isObject(jsondatum: any) {
    return ObjectHelper.isObject(jsondatum);
  }

  isLongArray(jsondatum: any) {

    if (typeof jsondatum === 'string' && jsondatum.length > this.limit) {
      return true;
    }

    return ObjectHelper.isLongArray(jsondatum);
  }
}
