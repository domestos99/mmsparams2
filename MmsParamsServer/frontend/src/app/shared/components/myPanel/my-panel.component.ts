import {Component, Input} from '@angular/core';


@Component({
  selector: 'my-panel',
  templateUrl: 'my-panel.component.html',
  styleUrls: ['./my-panel.component.scss']
})
export class MyPanelComponent {

  @Input() type: string;

  getClass() {
    if (!this.type) {
      return 'pnl-default';
    }

    if (this.type == 'primary') {
      return 'pnl-primary';
    }
    if (this.type == 'success') {
      return 'pnl-success';
    }
    if (this.type == 'danger') {
      return 'pnl-danger';
    }
    if (this.type == 'warning') {
      return 'pnl-warning';
    }
    if (this.type == 'info') {
      return 'pnl-info';
    }
    return 'pnl-default';
  }
}
