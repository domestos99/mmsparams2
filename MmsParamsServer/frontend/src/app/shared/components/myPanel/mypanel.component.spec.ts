import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MyPanelComponent} from '@app/shared/components/myPanel/my-panel.component';

describe('MyPanelComponent', () => {
  let component: MyPanelComponent;
  let fixture: ComponentFixture<MyPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyPanelComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
