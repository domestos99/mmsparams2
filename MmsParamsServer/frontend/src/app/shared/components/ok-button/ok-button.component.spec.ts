import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MyPanelComponent} from '@app/shared/components/myPanel/my-panel.component';
import {NoDataComponent} from '@app/shared/components/no-data/no-data.component';
import {OkButtonComponent} from '@app/shared/components/ok-button/ok-button.component';

describe('OkButtonComponent', () => {
  let component: OkButtonComponent;
  let fixture: ComponentFixture<OkButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OkButtonComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OkButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
