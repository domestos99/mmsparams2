import {Component, Input, Output, EventEmitter, ViewChild} from '@angular/core';


@Component({
  selector: 'print-button',
  templateUrl: 'print-button.component.html',
  styleUrls: ['./print-button.component.scss']
})
export class PrintButtonComponent {


  @Output() onClick: EventEmitter<any> = new EventEmitter();

  onButtonClick($event) {
    this.onClick.emit($event);
  }

}
