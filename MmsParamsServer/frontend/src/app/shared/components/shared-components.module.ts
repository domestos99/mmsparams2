import {NgModule} from '@angular/core';
import {DeleteButtonComponent} from '@app/shared/components/detelebutton/delete-button.component';
import {DetailButtonComponent} from '@app/shared/components/detailbutton/detail-button.component';
import {AddButtonComponent} from '@app/shared/components/add-button/add-button.component';
import {EditButtonComponent} from '@app/shared/components/edit-button/edit-button.component';
import {YesNoDialogComponent} from '@app/shared/components/yes-no-dialog/yes-no-dialog.component';
import {SpinnerComponent} from '@app/shared/components/spinner/spinner.component';
import {OkButtonComponent} from '@app/shared/components/ok-button/ok-button.component';
import {JsontableComponent} from '@app/shared/components/jsontable/jsontable.component';
import {NoDataComponent} from '@app/shared/components/no-data/no-data.component';
import {WarningPanelComponent} from '@app/shared/components/warning-panel/warning-panel.component';
import {RefreshButtonComponent} from '@app/shared/components/refresh-button/refresh-button.component';
import {LongdataprotectComponent} from '@app/shared/components/longdataprotect/longdataprotect.component';
import {LongdatadisplayComponent} from '@app/shared/components/longdatadisplay/longdatadisplay.component';
import {MymodalComponent} from '@app/shared/components/mymodal/mymodal.component';
import {HelpButtonComponent} from '@app/shared/components/help-button/help-button.component';
import {KeyButtonComponent} from '@app/shared/components/key-button/key-button.component';
import {MyPanelComponent} from '@app/shared/components/myPanel/my-panel.component';
import {BackButtonTextComponent} from '@app/shared/components/back-button-text/back-button-text.component';
import {TestResultComponent} from '@app/shared/components/test-result/test-result.component';
import {YesNoAllComponent} from '@app/shared/components/yes-no-all/yes-no-all.component';
import {YesNoNanComponent} from '@app/shared/components/yesNoNan/yes-no-nan.component';
import {CommonButtonComponent} from '@app/shared/components/common-button/common-button.component';
import {ToolbarPanelComponent} from '@app/shared/components/toobar-panel/toolbar-panel.component';
import {PrintButtonComponent} from '@app/shared/components/print-button/print-button.component';
import {DownloadButtonComponent} from '@app/shared/components/download-button/download-button.component';
import {BackButtonComponent} from '@app/shared/components/back-button/back-button.component';
import {ExpandHideButtonComponent} from '@app/shared/components/expand-hide-button/expand-hide-button.component';
import {FabButtonTopComponent} from '@app/shared/components/fab-button-top/fab-button-top.component';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MaterialModule} from '@app/shared/material.module';

@NgModule({

  imports: [

    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    MaterialModule,

    FontAwesomeModule,
  ],

  declarations: [

    DetailButtonComponent,
    DeleteButtonComponent,
    EditButtonComponent,
    YesNoDialogComponent,
    SpinnerComponent,
    AddButtonComponent,
    OkButtonComponent,
    JsontableComponent,
    NoDataComponent,
    WarningPanelComponent,
    RefreshButtonComponent,
    LongdataprotectComponent,
    LongdatadisplayComponent,
    MymodalComponent,
    HelpButtonComponent,
    KeyButtonComponent,
    MyPanelComponent,
    BackButtonTextComponent,
    TestResultComponent,
    YesNoAllComponent,
    YesNoNanComponent,
    CommonButtonComponent,
    ToolbarPanelComponent,
    PrintButtonComponent,
    DownloadButtonComponent,
    BackButtonComponent,
    ExpandHideButtonComponent,
    FabButtonTopComponent,


  ],
  entryComponents:
    [
      YesNoDialogComponent,
    ],

  exports: [

    DeleteButtonComponent,
    DetailButtonComponent,
    AddButtonComponent,
    EditButtonComponent,
    YesNoDialogComponent,
    SpinnerComponent,
    OkButtonComponent,
    JsontableComponent,
    NoDataComponent,
    WarningPanelComponent,
    RefreshButtonComponent,
    LongdataprotectComponent,
    LongdatadisplayComponent,
    MymodalComponent,
    HelpButtonComponent,
    KeyButtonComponent,
    MyPanelComponent,
    BackButtonTextComponent,
    TestResultComponent,
    YesNoAllComponent,
    YesNoNanComponent,
    CommonButtonComponent,
    ToolbarPanelComponent,
    PrintButtonComponent,
    DownloadButtonComponent,
    BackButtonComponent,
    ExpandHideButtonComponent,
    FabButtonTopComponent,

  ],

  providers: []

})
export class SharedComponentsModule {
}
