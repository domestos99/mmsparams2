import {Component, Input} from '@angular/core';


@Component({
  selector: 'test-result',
  templateUrl: 'test-result.component.html',
  styleUrls: ['./test-result.component.scss']
})
export class TestResultComponent {


  @Input() hasErrors: boolean = false;
  @Input() isOpen: boolean = false;

}
