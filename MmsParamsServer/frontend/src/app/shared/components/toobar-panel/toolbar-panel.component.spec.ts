import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RefreshButtonComponent} from '@app/shared/components/refresh-button/refresh-button.component';
import {TestResultComponent} from '@app/shared/components/test-result/test-result.component';
import {ToolbarPanelComponent} from '@app/shared/components/toobar-panel/toolbar-panel.component';

describe('ToolbarPanelComponent', () => {
  let component: ToolbarPanelComponent;
  let fixture: ComponentFixture<ToolbarPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToolbarPanelComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
