import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

@Component({
  selector: 'toolbar-panel',
  templateUrl: 'toolbar-panel.component.html',
  styleUrls: ['./toolbar-panel.component.scss']
})
export class ToolbarPanelComponent implements OnInit, OnDestroy {

  @Output() onRefreshButtonClick: EventEmitter<any> = new EventEmitter();
  @Output() onDownloadButtonClick: EventEmitter<any> = new EventEmitter();
  @Output() onBackButtonClick: EventEmitter<any> = new EventEmitter();

  @Input() downloadButtonVisible: boolean = false;
  @Input() printButtonVisible: boolean = false;
  @Input() refreshButtonVisible: boolean = true;
  @Input() backButtonVisible: boolean = false;


  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  onRefreshButtonClickEvent($event: any) {
    this.onRefreshButtonClick.emit($event);
  }

  onPrintButtonClickEvent($event: any) {
    window.print();
  }

  onDownloadButtonClickEvent($event: any) {
    this.onDownloadButtonClick.emit($event);
  }

  onBackButtonClickEvent($event) {
    this.onBackButtonClick.emit($event);
  }
}
