import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {WarningPanelComponent} from '@app/shared/components/warning-panel/warning-panel.component';

describe('WarningPanelComponent', () => {
  let component: WarningPanelComponent;
  let fixture: ComponentFixture<WarningPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WarningPanelComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarningPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
