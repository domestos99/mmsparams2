import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {YesNoAllComponent} from '@app/shared/components/yes-no-all/yes-no-all.component';
import {FormsModule} from '@angular/forms';

describe('YesNoAllComponent', () => {
  let component: YesNoAllComponent;
  let fixture: ComponentFixture<YesNoAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [YesNoAllComponent],
      imports: [FormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YesNoAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
