import {Component, EventEmitter, Input, Output} from '@angular/core';
import {YesNoAllEnum} from '@app/core/model/yesNoAll.enum';
import {MatButtonToggleChange} from '@angular/material/button-toggle';


@Component({
  selector: 'yes-no-all',
  templateUrl: 'yes-no-all.component.html',
  styleUrls: ['./yes-no-all.component.scss']
})
export class YesNoAllComponent {

  @Input() defaultValue: YesNoAllEnum = YesNoAllEnum.ALL;
  @Output() onOptionChange: EventEmitter<YesNoAllEnum> = new EventEmitter();

  onOptionChanged($event: MatButtonToggleChange) {
    const val = $event.value;
    if (!val) {
      return;
    }
    if (val == YesNoAllEnum.YES) {
      this.onOptionChange.emit(YesNoAllEnum.YES);
    }
    if (val == YesNoAllEnum.NO) {
      this.onOptionChange.emit(YesNoAllEnum.NO);
    }
    if (val == YesNoAllEnum.ALL) {
      this.onOptionChange.emit(YesNoAllEnum.ALL);
    }
  }
}
