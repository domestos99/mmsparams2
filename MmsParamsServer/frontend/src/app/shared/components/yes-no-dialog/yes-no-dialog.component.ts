import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'yes-no-dialog',
  templateUrl: 'yes-no-dialog.component.html'
})
export class YesNoDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<YesNoDialogComponent>) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
