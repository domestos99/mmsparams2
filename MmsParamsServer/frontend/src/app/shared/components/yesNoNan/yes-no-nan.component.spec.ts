import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {YesNoNanComponent} from '@app/shared/components/yesNoNan/yes-no-nan.component';

describe('YesNoNanComponent', () => {
  let component: YesNoNanComponent;
  let fixture: ComponentFixture<YesNoNanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [YesNoNanComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YesNoNanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
