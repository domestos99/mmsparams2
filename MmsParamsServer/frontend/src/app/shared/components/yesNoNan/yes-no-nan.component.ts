import {Component, Input} from '@angular/core';


@Component({
  selector: 'yes-no-nan',
  templateUrl: 'yes-no-nan.component.html',
  styleUrls: ['./yes-no-nan.component.scss']
})
export class YesNoNanComponent {


  @Input() hasErrors: any;
  @Input() isOpen: boolean = false;

  isYes() {
    return this.hasErrors === 'YES';
  }

  isNo() {
    return  this.hasErrors === 'NO';
  }

  isNaN() {
    return  this.hasErrors === 'NAN';
  }
}
