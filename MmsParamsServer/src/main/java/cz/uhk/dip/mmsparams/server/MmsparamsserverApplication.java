package cz.uhk.dip.mmsparams.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

import cz.uhk.dip.mmsparams.server.configs.CoreSpringConfiguration;
import cz.uhk.dip.mmsparams.server.configs.DbSpringConfiguration;

@SpringBootApplication
@EnableWebSocket
@Import({CoreSpringConfiguration.class, DbSpringConfiguration.class})
public class MmsparamsserverApplication
{
    public static void main(String[] args)
    {
        ApplicationContext applicationContext = SpringApplication.run(MmsparamsserverApplication.class, args);
    }
}
