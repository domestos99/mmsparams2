package cz.uhk.dip.mmsparams.server.auth;

import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.http.auth.JwtResponse;

public interface IJwtTokenService
{
    boolean isTokenValid(String token);

    JwtResponse getNewToken(JwtRequest jwtRequest);
}
