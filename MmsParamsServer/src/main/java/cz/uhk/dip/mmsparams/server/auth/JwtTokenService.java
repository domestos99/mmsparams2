package cz.uhk.dip.mmsparams.server.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Nullable;

import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.http.auth.JwtResponse;
import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.server.database.services.IUserService;

@Service
public class JwtTokenService implements IJwtTokenService
{
    private final IUserService userService;
    private final JwtTokenUtil jwtTokenUtil;

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenService.class);
    private static final String TAG = JwtTokenService.class.getSimpleName();


    @Autowired
    public JwtTokenService(IUserService userService, JwtTokenUtil jwtTokenUtil)
    {
        this.userService = userService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Nullable
    public JwtResponse getNewToken(final JwtRequest jwtRequest)
    {
        if (jwtRequest == null)
            return null;

        List<String> roles = ListUtils.newList();
        final UserModel user = this.userService.getUser(jwtRequest.getUsername(), jwtRequest.getPassword());

        if (user == null)
            return null;

        final Tuple<String, Long> token = this.jwtTokenUtil.generateToken(user.getUsername(), roles);
        return new JwtResponse(token.getX(), token.getY());
    }

    @Override
    public boolean isTokenValid(final String token)
    {
        if (StringUtil.isEmptyOrNull(token))
            return false;

        String rawToken = token;
        if (rawToken.startsWith(HttpConstants.TOKEN_PREFIX))
        {
            rawToken = rawToken.replace(HttpConstants.TOKEN_PREFIX, "");
        }

        final String tokenUserName = this.jwtTokenUtil.getUsernameFromToken(rawToken);

        final UserModel user = this.userService.getUserByUserName(tokenUserName);

        if (user == null)
            return false;

        return this.jwtTokenUtil.validateToken(rawToken, user);
    }
}
