package cz.uhk.dip.mmsparams.server.auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil implements Serializable
{
    private final String secret;

    public JwtTokenUtil(@Value("${jwt.secret}") String secret)
    {
        this.secret = secret;
    }

    //retrieve username from jwt token
    public String getUsernameFromToken(String token)
    {
        return getClaimFromToken(token, Claims::getSubject);
    }

    //retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token)
    {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver)
    {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    //for retrieveing any information from token we will need the secret key
    private Claims getAllClaimsFromToken(String token)
    {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    //check if the token has expired
    private boolean isTokenExpired(String token)
    {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    //validate token
    public boolean validateToken(String token, UserModel userDetails)
    {
        if (StringUtil.isEmptyOrNull(token))
            return false;
        if (userDetails == null)
            return false;
        final String username = getUsernameFromToken(token);
        if (StringUtil.isEmptyOrNull(username))
            return false;
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    public Tuple<String, Long> generateToken(String username, List<String> roles)
    {
        long expiry = System.currentTimeMillis() + SecurityConstants.JWT_TOKEN_VALIDITY;
        String token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, secret)
                .setHeaderParam("typ", HttpConstants.TOKEN_TYPE)
                .setIssuer(SecurityConstants.TOKEN_ISSUER)
                .setAudience(SecurityConstants.TOKEN_AUDIENCE)
                .setSubject(username)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(expiry))
                .claim("rol", roles)
                .compact();
        return new Tuple<>(token, expiry);
    }
}
