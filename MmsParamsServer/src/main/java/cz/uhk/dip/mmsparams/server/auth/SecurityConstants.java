package cz.uhk.dip.mmsparams.server.auth;

public class SecurityConstants
{
    public static final String AUTH_LOGIN_URL = "/api/authenticate";

    // JWT token defaults
    public static final String TOKEN_ISSUER = "mmsparams-server";
    public static final String TOKEN_AUDIENCE = "mmsparams-clients";
    public static final long JWT_TOKEN_VALIDITY = 18000000; // 5 hours
}
