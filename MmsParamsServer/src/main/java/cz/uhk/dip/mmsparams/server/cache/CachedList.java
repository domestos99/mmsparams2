package cz.uhk.dip.mmsparams.server.cache;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nullable;

public class CachedList<TKey, TValue>
{
    private final ConcurrentHashMap<TKey, TValue> detailCache;

    public CachedList()
    {
        this.detailCache = new ConcurrentHashMap<>();
    }

    private synchronized void putToChache(TKey key, TValue value)
    {
        this.detailCache.put(key, value);
    }

    public synchronized boolean cacheContains(TKey key)
    {
        if (key == null)
            return false;
        return this.detailCache.containsKey(key);
    }

    @Nullable
    public synchronized TValue getFromCache(TKey key)
    {
        if (!cacheContains(key))
            return null;
        return this.detailCache.get(key);
    }

    public synchronized Optional<TValue> updateCacheReturn(TKey key, Optional<TValue> valueOpt)
    {
        if (valueOpt.isPresent())
        {
            this.putToChache(key, valueOpt.get());
            return valueOpt;
        }
        else
        {
            return Optional.empty();
        }
    }

    public synchronized void remove(TKey key)
    {
        this.detailCache.remove(key);
    }

    public synchronized void clearCaches()
    {
        this.detailCache.clear();
    }
}
