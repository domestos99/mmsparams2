package cz.uhk.dip.mmsparams.server.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import cz.uhk.dip.mmsparams.api.utils.OptionalUtil;
import cz.uhk.dip.mmsparams.server.database.dto.ClientLibInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.services.IClientInfoDBService;

@Service
public class ClientInfoDBCache
{
    private final IClientInfoDBService service;
    private final CachedList<String, ClientLibInstanceDTO> clientInfoCache;

    @Autowired
    public ClientInfoDBCache(IClientInfoDBService service)
    {
        this.service = service;
        this.clientInfoCache = new CachedList<>();
    }

    public Optional<ClientLibInstanceDTO> getByPhoneKey(String clientKey)
    {
        if (clientInfoCache.cacheContains(clientKey))
            return OptionalUtil.fillOptional(clientInfoCache.getFromCache(clientKey));

        Optional<ClientLibInstanceDTO> row = this.service.getByClientKey(clientKey);
        return clientInfoCache.updateCacheReturn(clientKey, row);
    }

    public void clearCaches()
    {
        this.clientInfoCache.clearCaches();
    }
}
