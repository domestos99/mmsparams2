package cz.uhk.dip.mmsparams.server.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import cz.uhk.dip.mmsparams.api.utils.OptionalUtil;
import cz.uhk.dip.mmsparams.server.database.dto.MessageSequenceList;
import cz.uhk.dip.mmsparams.server.database.dto.WebSocketMessageDTO;
import cz.uhk.dip.mmsparams.server.database.services.IServerMessageDBService;

@Service
public class MessageDBCache
{
    private final IServerMessageDBService service;
    private final CachedList<Integer, WebSocketMessageDTO> cachedList;
    private final CachedList<String, MessageSequenceList> messageSequence;

    @Autowired
    public MessageDBCache(IServerMessageDBService service)
    {
        this.cachedList = new CachedList<>();
        this.service = service;
        this.messageSequence = new CachedList<>();
    }

    public synchronized Optional<WebSocketMessageDTO> getById(Integer id)
    {
        if (cachedList.cacheContains(id))
            return OptionalUtil.fillOptional(cachedList.getFromCache(id));

        Optional<WebSocketMessageDTO> row = this.service.getById(id);
        return cachedList.updateCacheReturn(id, row);
    }

    public Optional<MessageSequenceList> getSequenceByTestID(String testId)
    {
        if (messageSequence.cacheContains(testId))
            return Optional.of(messageSequence.getFromCache(testId));

        Optional<MessageSequenceList> row = this.service.getSequenceByTestID(testId);
        return messageSequence.updateCacheReturn(testId, row);
    }

    public synchronized void clearCaches()
    {
        this.cachedList.clearCaches();
    }
}
