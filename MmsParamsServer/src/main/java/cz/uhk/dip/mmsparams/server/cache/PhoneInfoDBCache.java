package cz.uhk.dip.mmsparams.server.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import cz.uhk.dip.mmsparams.api.utils.OptionalUtil;
import cz.uhk.dip.mmsparams.server.database.dto.PhoneInfoDTO;
import cz.uhk.dip.mmsparams.server.database.services.IPhoneInfoDBService;

@Service
public class PhoneInfoDBCache
{
    private final IPhoneInfoDBService service;
    private final CachedList<String, PhoneInfoDTO> phoneInfoCache;

    @Autowired
    public PhoneInfoDBCache(IPhoneInfoDBService service)
    {
        this.service = service;
        this.phoneInfoCache = new CachedList<>();
    }

    public synchronized Optional<PhoneInfoDTO> getByPhoneKey(String phoneKey)
    {
        if (phoneInfoCache.cacheContains(phoneKey))
            return OptionalUtil.fillOptional(phoneInfoCache.getFromCache(phoneKey));

        Optional<PhoneInfoDTO> row = this.service.getByPhoneKey(phoneKey);
        return phoneInfoCache.updateCacheReturn(phoneKey, row);
    }

    public void clearCaches()
    {
        this.phoneInfoCache.clearCaches();
    }
}
