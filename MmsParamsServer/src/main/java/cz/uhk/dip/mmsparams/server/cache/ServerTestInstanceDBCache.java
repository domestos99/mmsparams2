package cz.uhk.dip.mmsparams.server.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import cz.uhk.dip.mmsparams.api.utils.OptionalUtil;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDetailDTO;
import cz.uhk.dip.mmsparams.server.database.services.IServerTestInstanceDBService;

@Service
public class ServerTestInstanceDBCache
{
    private final IServerTestInstanceDBService service;

    private final CachedList<String, ServerTestInstanceDetailDTO> detailCache;
    private final CopyOnWriteArrayList<ServerTestInstanceDTO> allCache;


    @Autowired
    public ServerTestInstanceDBCache(IServerTestInstanceDBService service)
    {
        this.service = service;
        this.service.setOnChangeListener(o -> invalidateCache(o));
        this.detailCache = new CachedList<>();
        this.allCache = new CopyOnWriteArrayList<>();
    }

    private synchronized void invalidateCache(ServerTestInstanceDetailDTO row)
    {
        if (row != null)
            this.detailCache.remove(row.getTestId());
        this.allCache.clear();
    }

    public synchronized List<ServerTestInstanceDTO> getAllReduced()
    {
        if (!this.allCache.isEmpty())
            return this.allCache;

        List<ServerTestInstanceDTO> list = this.service.getReduced();
//        list.sort(new ServerTestInstanceComparator());
        this.allCache.addAll(list);
        return this.allCache;
    }

    public synchronized Optional<ServerTestInstanceDetailDTO> getByTestId(String testId)
    {
        if (detailCache.cacheContains(testId))
            return OptionalUtil.fillOptional(detailCache.getFromCache(testId));

        Optional<ServerTestInstanceDetailDTO> row = this.service.getByTestId(testId);
        return detailCache.updateCacheReturn(testId, row);
    }


    public synchronized boolean delete(String testId)
    {
        boolean result = this.service.delete(testId);
        if (result)
        {
            this.allCache.clear();
            this.detailCache.remove(testId);
        }
        return result;
    }


    public synchronized void clearCaches()
    {
        detailCache.clearCaches();
        allCache.clear();
    }

    public boolean deleteOldTests(int daysInt)
    {
        boolean result = this.service.deleteOldTests(daysInt);
        if (result)
        {
            this.allCache.clear();
            this.detailCache.clearCaches();
        }
        return result;
    }
}
