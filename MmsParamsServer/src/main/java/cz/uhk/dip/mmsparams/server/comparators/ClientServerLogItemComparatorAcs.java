package cz.uhk.dip.mmsparams.server.comparators;

import java.util.Comparator;

import cz.uhk.dip.mmsparams.server.database.dto.ClientServerLogItemDTO;

public class ClientServerLogItemComparatorAcs implements Comparator<ClientServerLogItemDTO>
{
    @Override
    public int compare(ClientServerLogItemDTO o1, ClientServerLogItemDTO o2)
    {
        long dt1 = o1.getCreatedDT();
        long dt2 = o2.getCreatedDT();
        return (int) (dt1 - dt2);
    }
}
