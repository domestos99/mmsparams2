package cz.uhk.dip.mmsparams.server.comparators;

import java.util.Comparator;

import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO;

public class ServerTestInstanceComparatorDesc implements Comparator<ServerTestInstanceDTO>
{
    @Override
    public int compare(ServerTestInstanceDTO o1, ServerTestInstanceDTO o2)
    {
        long dt1 = o1.getCreatedDT();
        long dt2 = o2.getCreatedDT();
        return (int) (dt2 - dt1);
    }
}
