package cz.uhk.dip.mmsparams.server.comparators;

import java.util.Comparator;

import cz.uhk.dip.mmsparams.server.database.dto.WebSocketMessageDTO;

public class WebSocketMessageBaseDTOComparatorAsc implements Comparator<WebSocketMessageDTO>
{
    @Override
    public int compare(WebSocketMessageDTO o1, WebSocketMessageDTO o2)
    {
        long dt1 = o1.getInsertedDT();
        long dt2 = o2.getInsertedDT();
        return (int) (dt1 - dt2);
    }
}
