package cz.uhk.dip.mmsparams.server.configs;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig
{
    private static long deleteTestAfter;

    @Value("${appconfig.delete_test_after_minutes}")
    public void setDeleteTestAfter(long value)
    {
        if (value == 0)
            value = 10080;
        AppConfig.deleteTestAfter = value;
    }

    public static long getDeleteTestAfter()
    {
        return deleteTestAfter;
    }

    public static String getSocketRoute()
    {
        return "/appsocket";
    }

    private static String getMMSCRoute()
    {
        return "MMSC";
    }

    public static String getMmscPattern()
    {
        return "/MMSC/*";
    }


}
