package cz.uhk.dip.mmsparams.server.configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("cz.uhk.dip.mmsparams.server")
public class CoreSpringConfiguration {

}
