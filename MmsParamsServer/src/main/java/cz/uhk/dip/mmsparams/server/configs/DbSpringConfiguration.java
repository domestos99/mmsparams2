package cz.uhk.dip.mmsparams.server.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "cz.uhk.dip.mmsparams.server")
public class DbSpringConfiguration {
}
