package cz.uhk.dip.mmsparams.server.configs;

import net.instantcom.mm7.VASP;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import cz.uhk.dip.mmsparams.server.interceptors.LoggingFilter;
import cz.uhk.dip.mmsparams.server.interceptors.PerformanceLogFilter;
import cz.uhk.dip.mmsparams.server.web.controllers.MmscServlet;

@Configuration
public class WebConfiguration implements WebMvcConfigurer
{
    @Override
    public void addViewControllers(ViewControllerRegistry registry)
    {
        registry.addViewController("/{spring:\\w+}")
                .setViewName("forward:/");
        registry.addViewController("/**/{spring:\\w+}")
                .setViewName("forward:/");
        registry.addViewController("/{spring:\\w+}/**{spring:?!(\\.js|\\.css)$}")
                .setViewName("forward:/");
    }

    @Bean
    public ServletRegistrationBean exampleServletBean(final VASP vasp)
    {
        ServletRegistrationBean bean = new ServletRegistrationBean(
                new MmscServlet(vasp), AppConfig.getMmscPattern());
        bean.setLoadOnStartup(1);
        return bean;
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBeanMMSC (final LoggingFilter loggingFilter) {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(loggingFilter);
        registrationBean.addUrlPatterns(AppConfig.getMmscPattern());
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBeanPerformance (final PerformanceLogFilter loggingFilter) {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(loggingFilter);
        return registrationBean;
    }

}
