package cz.uhk.dip.mmsparams.server.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean;

import cz.uhk.dip.mmsparams.api.constants.MemoryConstants;
import cz.uhk.dip.mmsparams.server.interceptors.WebSocketHandshakeInterceptor;
import cz.uhk.dip.mmsparams.server.websocket.SimpleServerWebSocketHandler;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer
{

    private final  SimpleServerWebSocketHandler simpleServerWebSocketHandler;
    private final WebSocketHandshakeInterceptor webSocketHandshakeInterceptor;

    @Autowired
    public WebSocketConfig(SimpleServerWebSocketHandler simpleServerWebSocketHandler, WebSocketHandshakeInterceptor webSocketHandshakeInterceptor)
    {
        this.simpleServerWebSocketHandler = simpleServerWebSocketHandler;
        this.webSocketHandshakeInterceptor = webSocketHandshakeInterceptor;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry)
    {
        webSocketHandlerRegistry.addHandler(simpleServerWebSocketHandler, AppConfig.getSocketRoute())
                .addInterceptors(webSocketHandshakeInterceptor);
    }

    @Bean
    public ServletServerContainerFactoryBean createServletServerContainerFactoryBean()
    {
        ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
        container.setMaxTextMessageBufferSize(MemoryConstants.MB16);
        container.setMaxBinaryMessageBufferSize(MemoryConstants.MB16);
        return container;
    }
}