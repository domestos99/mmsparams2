package cz.uhk.dip.mmsparams.server.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import cz.uhk.dip.mmsparams.server.database.dto.ClientLibInstanceDTO;

@Repository
public interface ClientInfoDBDAO extends JpaRepository<ClientLibInstanceDTO, Integer>
{
    List<ClientLibInstanceDTO> findByClientKey(String clientKey);
}
