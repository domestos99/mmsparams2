package cz.uhk.dip.mmsparams.server.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import cz.uhk.dip.mmsparams.server.database.dto.ClientServerLogDTO;

@Repository
public interface ClientServerLogDAO extends JpaRepository<ClientServerLogDTO, Integer>
{
    Optional<ClientServerLogDTO> findByTestID(String testID);
}
