package cz.uhk.dip.mmsparams.server.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import cz.uhk.dip.mmsparams.server.database.dto.PhoneInfoDTO;

@Repository
public interface PhoneInfoDBDAO extends JpaRepository<PhoneInfoDTO, Integer>
{
    List<PhoneInfoDTO> findByPhoneKey(String phoneKey);
}
