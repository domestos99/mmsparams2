package cz.uhk.dip.mmsparams.server.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cz.uhk.dip.mmsparams.server.database.dto.WebSocketMessageDTO;

@Repository
public interface ServerMessageDBDAO extends JpaRepository<WebSocketMessageDTO, Integer>
{
}
