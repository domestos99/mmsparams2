package cz.uhk.dip.mmsparams.server.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDetailDTO;

@Repository
public interface ServerTestInstanceDAO extends JpaRepository<ServerTestInstanceDetailDTO, String>
{
    Optional<ServerTestInstanceDetailDTO> findByTestId(String testId);

    @Query("SELECT s.testId, s.closed, s.createdDT, s.testName, s.hasErrors, s.validationErrors FROM ServerTestInstanceDetailDTO s order by s.createdDT desc")
    List<Object[]> getReduced();
}
