package cz.uhk.dip.mmsparams.server.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cz.uhk.dip.mmsparams.server.database.dto.TestErrorDTO;

@Repository
public interface TestErrorDAO extends JpaRepository<TestErrorDTO, Integer>
{
}
