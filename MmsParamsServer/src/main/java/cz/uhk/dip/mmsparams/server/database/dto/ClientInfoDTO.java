package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;

@Entity
public class ClientInfoDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String clientKey;
    @Column
    private String computerName;
    @Column
    private String currentUserName;

    public static List<ClientInfoDTO> create(List<ClientInfo> clientInfo)
    {
        List<ClientInfoDTO> l = new ArrayList<>();

        if (clientInfo == null)
            return l;

        for (ClientInfo ci : clientInfo)
            l.add(create(ci));
        return l;
    }

    @Nullable
    public static ClientInfoDTO create(ClientInfo clientInfo)
    {
        if (clientInfo == null)
            return null;

        ClientInfoDTO dto = new ClientInfoDTO();
        dto.clientKey = clientInfo.getClientKey();
        dto.computerName = clientInfo.getComputerName();
        dto.currentUserName = clientInfo.getCurrentUserName();
        return dto;
    }

    public String getClientKey()
    {
        return clientKey;
    }

    public void setClientKey(String clientKey)
    {
        this.clientKey = clientKey;
    }

    public void setComputerName(String computerName)
    {
        this.computerName = computerName;
    }

    public String getComputerName()
    {
        return computerName;
    }

    public void setCurrentUserName(String currentUserName)
    {
        this.currentUserName = currentUserName;
    }

    public String getCurrentUserName()
    {
        return currentUserName;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientInfoDTO that = (ClientInfoDTO) o;
        return id == that.id &&
                Objects.equals(clientKey, that.clientKey) &&
                Objects.equals(computerName, that.computerName) &&
                Objects.equals(currentUserName, that.currentUserName);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, clientKey, computerName, currentUserName);
    }
}
