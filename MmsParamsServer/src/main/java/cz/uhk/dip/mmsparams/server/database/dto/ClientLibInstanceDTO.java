package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import cz.uhk.dip.mmsparams.server.tests.ClientLibInstance;

@Entity
public class ClientLibInstanceDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String clientKey;

    @OneToOne(cascade = CascadeType.ALL)
    private WSSessionDTO session;

    @OneToOne(cascade = CascadeType.ALL)
    private ClientInfoDTO clientInfo;


    public static ClientLibInstanceDTO create(ClientLibInstance clientLibInstance)
    {
        if (clientLibInstance == null)
            return null;

        ClientLibInstanceDTO dto = new ClientLibInstanceDTO();
        dto.clientKey = clientLibInstance.getClientInfo().getClientKey();
        dto.session = WSSessionDTO.create(clientLibInstance.getClientSession());
        dto.clientInfo = ClientInfoDTO.create(clientLibInstance.getClientInfo());
        return dto;
    }

    public ClientInfoDTO getClientInfo()
    {
        return clientInfo;
    }

    public void setClientInfo(ClientInfoDTO clientInfo)
    {
        this.clientInfo = clientInfo;
    }

    public WSSessionDTO getSession()
    {
        return session;
    }

    public void setSession(WSSessionDTO session)
    {
        this.session = session;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getClientKey()
    {
        return clientKey;
    }

    public void setClientKey(String clientKey)
    {
        this.clientKey = clientKey;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientLibInstanceDTO that = (ClientLibInstanceDTO) o;
        return id == that.id &&
                Objects.equals(clientKey, that.clientKey) &&
                Objects.equals(session, that.session) &&
                Objects.equals(clientInfo, that.clientInfo);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, clientKey, session, clientInfo);
    }
}
