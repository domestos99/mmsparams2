package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import cz.uhk.dip.mmsparams.api.web.model.ClientServerLogModel;

@Entity
public class ClientServerLogDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String testID;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ClientServerLogItemDTO> items;

    @Nullable
    public static ClientServerLogDTO create(ClientServerLogModel model)
    {
        if (model == null)
            return null;
        ClientServerLogDTO dto = new ClientServerLogDTO();
        dto.testID = model.getTestID();
        dto.items = ClientServerLogItemDTO.create(model.getClientServerLogItems());
        return dto;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTestID()
    {
        return testID;
    }

    public void setTestID(String testID)
    {
        this.testID = testID;
    }

    public List<ClientServerLogItemDTO> getItems()
    {
        return items;
    }

    public void setItems(List<ClientServerLogItemDTO> items)
    {
        this.items = items;
    }
}
