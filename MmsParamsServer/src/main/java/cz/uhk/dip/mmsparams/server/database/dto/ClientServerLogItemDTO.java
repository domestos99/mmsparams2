package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

import cz.uhk.dip.mmsparams.api.enums.LogLevel;
import cz.uhk.dip.mmsparams.api.web.model.ClientServerLogItem;

@Entity
public class ClientServerLogItemDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private long createdDT;

    @Column
    @Lob
    private String log;

    @Enumerated(EnumType.STRING)
    private LogLevel logLevel;

    @Nullable
    public static ClientServerLogItemDTO create(ClientServerLogItem model)
    {
        if (model == null)
            return null;
        ClientServerLogItemDTO dto = new ClientServerLogItemDTO();
        dto.log = model.getLog();
        dto.createdDT = model.getCreatedDT();
        dto.logLevel = model.getLogLevel();
        return dto;
    }

    @NotNull
    public static List<ClientServerLogItemDTO> create(List<ClientServerLogItem> clientServerLogItems)
    {
        List<ClientServerLogItemDTO> l = new ArrayList<>();
        if (clientServerLogItems == null)
            return l;

        for (ClientServerLogItem item : clientServerLogItems)
        {
            l.add(create(item));
        }
        return l;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public long getCreatedDT()
    {
        return createdDT;
    }

    public void setCreatedDT(long createdDT)
    {
        this.createdDT = createdDT;
    }

    public String getLog()
    {
        return log;
    }

    public void setLog(String log)
    {
        this.log = log;
    }

    public LogLevel getLogLevel()
    {
        return logLevel;
    }

    public void setLogLevel(LogLevel logLevel)
    {
        this.logLevel = logLevel;
    }
}
