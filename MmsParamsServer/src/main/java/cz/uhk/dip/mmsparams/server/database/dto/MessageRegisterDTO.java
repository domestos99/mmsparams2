package cz.uhk.dip.mmsparams.server.database.dto;


import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import cz.uhk.dip.mmsparams.api.messages.IMessageRegister;
import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.server.comparators.WebSocketMessageBaseDTOComparatorAsc;

@Entity
public class MessageRegisterDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    // mappedBy = "messageRegisterDTO",
    @OneToMany(cascade = CascadeType.ALL)
    private List<WebSocketMessageDTO> incoming;

    @OneToMany(cascade = CascadeType.ALL)
    private List<WebSocketMessageDTO> outgoing;


    public static MessageRegisterDTO create(IMessageRegister messageRegister)
    {
        MessageRegisterDTO dto = new MessageRegisterDTO();

        if (messageRegister != null)
        {
            dto.incoming = ListUtils.toArrayList(WebSocketMessageDTO.create(messageRegister.getAllIncoming()));

            dto.incoming.sort(new WebSocketMessageBaseDTOComparatorAsc());

            dto.outgoing = ListUtils.toArrayList(WebSocketMessageDTO.create(messageRegister.getAllOutgoing()));
            dto.outgoing.sort(new WebSocketMessageBaseDTOComparatorAsc());
        }
        return dto;
    }


    public List<WebSocketMessageDTO> getIncoming()
    {
        return incoming;
    }

    public List<WebSocketMessageDTO> getOutgoing()
    {
        return outgoing;
    }

    public void setOutgoing(List<WebSocketMessageDTO> outgoing)
    {
        this.outgoing = outgoing;
    }

    public void setIncoming(List<WebSocketMessageDTO> incoming)
    {
        this.incoming = incoming;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageRegisterDTO that = (MessageRegisterDTO) o;
        return id == that.id &&
                Objects.equals(incoming, that.incoming) &&
                Objects.equals(outgoing, that.outgoing);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, incoming, outgoing);
    }
}
