package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.List;

public class MessageSequenceList implements Serializable
{
    private String testID;
    private int msgCount;
    private int endpointCount;
    private List<WebSocketMessageDTO> messages;
    private List<PhoneInstanceDTO> phones;

    public String getTestID()
    {
        return testID;
    }

    public void setTestID(String testID)
    {
        this.testID = testID;
    }

    public List<WebSocketMessageDTO> getMessages()
    {
        return messages;
    }

    public void setMessages(List<WebSocketMessageDTO> messages)
    {
        this.messages = messages;
    }

    public int getMsgCount()
    {
        return msgCount;
    }

    public void setMsgCount(int msgCount)
    {
        this.msgCount = msgCount;
    }

    public int getEndpointCount()
    {
        return endpointCount;
    }

    public void setEndpointCount(int endpointCount)
    {
        this.endpointCount = endpointCount;
    }

    public List<PhoneInstanceDTO> getPhones()
    {
        return phones;
    }

    public void setPhones(List<PhoneInstanceDTO> phones)
    {
        this.phones = phones;
    }
}
