package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import cz.uhk.dip.mmsparams.server.tests.MmscInstance;

@Entity
public class MmscInstanceDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String pattern;

    public static MmscInstanceDTO create(final MmscInstance mmscInstance)
    {
        if (mmscInstance == null)
            return null;

        MmscInstanceDTO dto = new MmscInstanceDTO();

        if (mmscInstance.getAcquireRouteModel() != null)
        {
            dto.pattern = mmscInstance.getAcquireRouteModel().getPattern();
        }

        return dto;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getPattern()
    {
        return pattern;
    }

    public void setPattern(String pattern)
    {
        this.pattern = pattern;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MmscInstanceDTO that = (MmscInstanceDTO) o;
        return id == that.id &&
                Objects.equals(pattern, that.pattern);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, pattern);
    }
}
