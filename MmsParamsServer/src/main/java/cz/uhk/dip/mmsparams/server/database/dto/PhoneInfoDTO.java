package cz.uhk.dip.mmsparams.server.database.dto;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;

@Entity
public class PhoneInfoDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int genid;

    @Column
    private String serial;
    @Column
    private String model;
    @Column
    private String id;
    @Column
    private String manufacturer;
    @Column
    private String brand;
    @Column
    private String usera;
    @Column
    private String type;
    @Column
    private String versionIncremental;
    @Column
    private String versionSdk;
    @Column
    private String board;
    @Column
    private String host;
    @Column
    private String fingerprint;
    @Column
    private String releasea;
    @Column
    private int codesBase, versionSdkInt;
    @Column
    private String phoneKey;

    @OneToMany(cascade = CascadeType.ALL)
    private List<SubscriptionInfoModelDTO> subscriptionInfoModel;
    @Column
    private String phoneNumber;
    @Column
    private String phoneCustomName;
    @Column
    private String imsi;

    public static List<PhoneInfoDTO> create(List<PhoneInfoModel> phoneInfoModel)
    {
        List<PhoneInfoDTO> l = new ArrayList<>();
        if (phoneInfoModel == null)
            return l;
        for (PhoneInfoModel ci : phoneInfoModel)
            l.add(create(ci));
        return l;
    }

    @Nullable
    public static PhoneInfoDTO create(PhoneInfoModel phoneInfoModel)
    {
        if (phoneInfoModel == null)
            return null;

        PhoneInfoDTO dto = new PhoneInfoDTO();
        dto.serial = phoneInfoModel.getSerial();
        dto.model = phoneInfoModel.getModel();
        dto.id = phoneInfoModel.getId();
        dto.manufacturer = phoneInfoModel.getManufacturer();
        dto.brand = phoneInfoModel.getBrand();
        dto.usera = phoneInfoModel.getUser();
        dto.type = phoneInfoModel.getType();
        dto.versionIncremental = phoneInfoModel.getVersionIncremental();
        dto.versionSdk = phoneInfoModel.getVersionSdk();
        dto.board = phoneInfoModel.getBoard();
        dto.host = phoneInfoModel.getHost();
        dto.fingerprint = phoneInfoModel.getFingerprint();
        dto.releasea = phoneInfoModel.getRelease();
        dto.codesBase = phoneInfoModel.getCodesBase();
        dto.versionSdkInt = phoneInfoModel.getVersionSdkInt();
        dto.phoneKey = phoneInfoModel.getPhoneKey();
        dto.subscriptionInfoModel = ListUtils.toList(SubscriptionInfoModelDTO.create(phoneInfoModel.getSubscriptionInfoModel()));
        dto.phoneNumber = phoneInfoModel.getPhoneNumber();
        dto.phoneCustomName = phoneInfoModel.getPhoneCustomName();
        dto.imsi = phoneInfoModel.getImsi();
        return dto;
    }

    public String getSerial()
    {
        return serial;
    }

    public void setSerial(String serial)
    {
        this.serial = serial;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    public String getBrand()
    {
        return brand;
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public String getUsera()
    {
        return usera;
    }

    public void setUsera(String usera)
    {
        this.usera = usera;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getVersionIncremental()
    {
        return versionIncremental;
    }

    public void setVersionIncremental(String versionIncremental)
    {
        this.versionIncremental = versionIncremental;
    }

    public String getVersionSdk()
    {
        return versionSdk;
    }

    public void setVersionSdk(String versionSdk)
    {
        this.versionSdk = versionSdk;
    }

    public String getBoard()
    {
        return board;
    }

    public void setBoard(String board)
    {
        this.board = board;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getFingerprint()
    {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint)
    {
        this.fingerprint = fingerprint;
    }

    public String getReleasea()
    {
        return releasea;
    }

    public void setReleasea(String releasea)
    {
        this.releasea = releasea;
    }

    public int getCodesBase()
    {
        return codesBase;
    }

    public void setCodesBase(int codesBase)
    {
        this.codesBase = codesBase;
    }

    public int getVersionSdkInt()
    {
        return versionSdkInt;
    }

    public void setVersionSdkInt(int versionSdkInt)
    {
        this.versionSdkInt = versionSdkInt;
    }

    public String getPhoneKey()
    {
        return phoneKey;
    }

    public void setPhoneKey(String phoneKey)
    {
        this.phoneKey = phoneKey;
    }

    public List<SubscriptionInfoModelDTO> getSubscriptionInfoModel()
    {
        return subscriptionInfoModel;
    }

    public void setSubscriptionInfoModel(List<SubscriptionInfoModelDTO> subscriptionInfoModel)
    {
        this.subscriptionInfoModel = subscriptionInfoModel;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneCustomName()
    {
        return phoneCustomName;
    }

    public void setPhoneCustomName(String phoneCustomName)
    {
        this.phoneCustomName = phoneCustomName;
    }

    public String getImsi()
    {
        return imsi;
    }

    public void setImsi(String imsi)
    {
        this.imsi = imsi;
    }

    public int getGenid()
    {
        return genid;
    }

    public void setGenid(int genid)
    {
        this.genid = genid;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneInfoDTO that = (PhoneInfoDTO) o;
        return genid == that.genid &&
                codesBase == that.codesBase &&
                versionSdkInt == that.versionSdkInt &&
                Objects.equals(serial, that.serial) &&
                Objects.equals(model, that.model) &&
                Objects.equals(id, that.id) &&
                Objects.equals(manufacturer, that.manufacturer) &&
                Objects.equals(brand, that.brand) &&
                Objects.equals(usera, that.usera) &&
                Objects.equals(type, that.type) &&
                Objects.equals(versionIncremental, that.versionIncremental) &&
                Objects.equals(versionSdk, that.versionSdk) &&
                Objects.equals(board, that.board) &&
                Objects.equals(host, that.host) &&
                Objects.equals(fingerprint, that.fingerprint) &&
                Objects.equals(releasea, that.releasea) &&
                Objects.equals(phoneKey, that.phoneKey) &&
                Objects.equals(subscriptionInfoModel, that.subscriptionInfoModel) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(phoneCustomName, that.phoneCustomName) &&
                Objects.equals(imsi, that.imsi);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(genid, serial, model, id, manufacturer, brand, usera, type, versionIncremental, versionSdk, board, host, fingerprint, releasea, codesBase, versionSdkInt, phoneKey, subscriptionInfoModel, phoneNumber, phoneCustomName, imsi);
    }
}
