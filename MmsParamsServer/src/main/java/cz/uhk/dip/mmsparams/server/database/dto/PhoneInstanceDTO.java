package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import cz.uhk.dip.mmsparams.server.tests.PhoneInstance;

@Entity
public class PhoneInstanceDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String phoneKey;

    @OneToOne(cascade = CascadeType.ALL)
    private WSSessionDTO session;

    @OneToOne(cascade = CascadeType.ALL)
    private PhoneInfoDTO phoneInfo;


    public static List<PhoneInstanceDTO> create(List<PhoneInstance> phoneInstanceList)
    {
        List<PhoneInstanceDTO> dto = new ArrayList<>();
        if (phoneInstanceList != null)
        {
            for (PhoneInstance pi : phoneInstanceList)
            {
                dto.add(create(pi));
            }
        }
        return dto;
    }

    @Nullable
    public static PhoneInstanceDTO create(PhoneInstance pi)
    {
        if (pi == null)
            return null;

        PhoneInstanceDTO dto = new PhoneInstanceDTO();

        dto.phoneKey = pi.getPhoneInfoModel().getPhoneKey();
        dto.session = WSSessionDTO.create(pi.getPhoneSession());
        dto.phoneInfo = PhoneInfoDTO.create(pi.getPhoneInfoModel());
        return dto;
    }

    public String getPhoneKey()
    {
        return phoneKey;
    }

    public void setPhoneKey(String phoneKey)
    {
        this.phoneKey = phoneKey;
    }

    public PhoneInfoDTO getPhoneInfo()
    {
        return phoneInfo;
    }

    public void setPhoneInfo(PhoneInfoDTO phoneInfo)
    {
        this.phoneInfo = phoneInfo;
    }

    public WSSessionDTO getSession()
    {
        return session;
    }

    public void setSession(WSSessionDTO session)
    {
        this.session = session;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneInstanceDTO that = (PhoneInstanceDTO) o;
        return id == that.id &&
                Objects.equals(phoneKey, that.phoneKey) &&
                Objects.equals(session, that.session) &&
                Objects.equals(phoneInfo, that.phoneInfo);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, phoneKey, session, phoneInfo);
    }
}
