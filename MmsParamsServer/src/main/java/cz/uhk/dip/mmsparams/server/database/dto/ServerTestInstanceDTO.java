package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cz.uhk.dip.mmsparams.api.constants.YesNoNaN;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

public class ServerTestInstanceDTO implements Serializable
{
    private TestGroupModelDTO testGroupModelDTO;
    private String testId;
    private boolean closed;
    private long createdDT;
    private String testName;
    private boolean hasErrors;
    private YesNoNaN validationErrors;

    public ServerTestInstanceDTO()
    {
    }

    public ServerTestInstanceDTO(TestGroupModelDTO testGroupModelDTO, String testId, boolean closed, long createdDT, String testName, boolean hasErrors, YesNoNaN validationErrors)
    {
        this.testGroupModelDTO = testGroupModelDTO;
        this.testId = testId;
        this.closed = closed;
        this.createdDT = createdDT;
        this.testName = testName;
        this.hasErrors = hasErrors;
        this.validationErrors = validationErrors;
    }

    public TestGroupModelDTO getTestGroupModelDTO()
    {
        return testGroupModelDTO;
    }

    public void setTestGroupModelDTO(TestGroupModelDTO testGroupModelDTO)
    {
        this.testGroupModelDTO = testGroupModelDTO;
    }

    public void setTestId(String testId)
    {
        this.testId = testId;
    }

    public String getTestId()
    {
        return testId;
    }

    public boolean isClosed()
    {
        return closed;
    }

    public void setClosed(boolean closed)
    {
        this.closed = closed;
    }

    public String getTestName()
    {
        return testName;
    }

    public void setTestName(String testName)
    {
        this.testName = testName;
    }

    public long getCreatedDT()
    {
        return createdDT;
    }

    public void setCreatedDT(long createdDT)
    {
        this.createdDT = createdDT;
    }

    public boolean getClosed()
    {
        return closed;
    }

    public boolean getHasErrors()
    {
        return hasErrors;
    }

    public void setHasErrors(boolean hasErrors)
    {
        this.hasErrors = hasErrors;
    }

    public YesNoNaN getValidationErrors()
    {
        return validationErrors;
    }

    public void setValidationErrors(YesNoNaN validationErrors)
    {
        this.validationErrors = validationErrors;
    }

    public static List<ServerTestInstanceDTO> create(List<ServerTestInstance> tests)
    {
        List<ServerTestInstanceDTO> l = new ArrayList<>();
        if (tests == null)
            return l;

        for (ServerTestInstance st : tests)
        {
            ServerTestInstanceDTO dto = create(st);
            if (dto != null)
            {
                l.add(create(st));
            }
        }

        return l;
    }

    public static ServerTestInstanceDTO create(ServerTestInstance test)
    {
        if (test == null)
            return null;

        ServerTestInstanceDTO dto = new ServerTestInstanceDTO();

        dto.testId = test.getTestID();
        dto.closed = test.isClosed();
        dto.createdDT = test.getCreatedDT();
        dto.testName = test.getTestName();
        dto.hasErrors = test.hasErrors();
        dto.validationErrors = test.hasValidationErrors();
        dto.testGroupModelDTO =  test.getTestGroupModelHist();
        return dto;
    }

    public static List<ServerTestInstanceDTO> createReduced(List<ServerTestInstanceDetailDTO> list)
    {
        List<ServerTestInstanceDTO> l = new ArrayList<>();
        if (list == null)
            return l;

        for (ServerTestInstanceDetailDTO st : list)
        {
            ServerTestInstanceDTO dto = new ServerTestInstanceDTO();

            dto.testId = st.getTestId();
            dto.closed = st.isClosed();
            dto.createdDT = st.getCreatedDT();
            dto.testName = st.getTestName();
            dto.hasErrors = st.getHasErrors();
            dto.validationErrors = st.getValidationErrors();
            dto.testGroupModelDTO =  st.getTestGroupModelDTO();
            l.add(dto);
        }

        return l;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServerTestInstanceDTO that = (ServerTestInstanceDTO) o;
        return closed == that.closed &&
                createdDT == that.createdDT &&
                hasErrors == that.hasErrors &&
                Objects.equals(testGroupModelDTO, that.testGroupModelDTO) &&
                Objects.equals(testId, that.testId) &&
                Objects.equals(testName, that.testName) &&
                validationErrors == that.validationErrors;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(testGroupModelDTO, testId, closed, createdDT, testName, hasErrors, validationErrors);
    }
}
