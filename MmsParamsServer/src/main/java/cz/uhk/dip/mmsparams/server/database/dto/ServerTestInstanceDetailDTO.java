package cz.uhk.dip.mmsparams.server.database.dto;


import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import cz.uhk.dip.mmsparams.api.constants.YesNoNaN;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

@Entity
public class ServerTestInstanceDetailDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String testId;

    @OneToOne(cascade = CascadeType.ALL)
    private TestGroupModelDTO testGroupModelDTO;

    @Column
    private String testName;

    @Column
    private String testDesc;

    @Column
    private boolean closed;

    @OneToOne(cascade = CascadeType.ALL)
    private ClientLibInstanceDTO clientLibInstance;

    @OneToMany(cascade = CascadeType.ALL)
    private List<PhoneInstanceDTO> phoneInstanceList;

    @OneToMany(cascade = CascadeType.ALL)
    private List<SmscInstanceDTO> smscInstances;

    @OneToOne(cascade = CascadeType.ALL)
    private MmscInstanceDTO mmscInstance;

    @OneToOne(cascade = CascadeType.ALL)
    private MessageRegisterDTO messageRegister;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ValidationResultDTO> validationResults;

    @OneToMany(cascade = CascadeType.ALL)
    private List<TestErrorDTO> testErrors;

    @Column
    private long createdDT;

    @Column
    private boolean hasErrors;

    @Enumerated(EnumType.STRING)
    private YesNoNaN validationErrors;

    @Nullable
    public static ServerTestInstanceDetailDTO create(ServerTestInstance test)
    {
        if (test == null)
            return null;

        ServerTestInstanceDetailDTO dto = new ServerTestInstanceDetailDTO();

        dto.testId = test.getTestID();
        dto.testGroupModelDTO = test.getTestGroupModelHist();
        dto.closed = test.isClosed();
        dto.createdDT = test.getCreatedDT();
        dto.testName = test.getTestName();
        dto.testDesc = test.getTestDesc();

        dto.clientLibInstance = test.getClientLibInstanceHist();
        dto.phoneInstanceList = test.getPhoneInstanceListHist();
        dto.smscInstances = test.getSmscInstancesHist();
        dto.mmscInstance = test.getMmscInstanceHist();
        dto.messageRegister = MessageRegisterDTO.create(test.getMessageRegister());
        dto.testErrors = test.getTestErrorsHist();
        dto.validationResults = test.getValidationResultsHist();
        dto.hasErrors = test.hasErrors();
        dto.validationErrors = test.hasValidationErrors();

        return dto;
    }


    public String getTestId()
    {
        return testId;
    }

    public void setTestId(String testId)
    {
        this.testId = testId;
    }

    public boolean isClosed()
    {
        return closed;
    }

    public void setClosed(boolean closed)
    {
        this.closed = closed;
    }

    public ClientLibInstanceDTO getClientLibInstance()
    {
        return clientLibInstance;
    }

    public void setClientLibInstance(ClientLibInstanceDTO clientLibInstance)
    {
        this.clientLibInstance = clientLibInstance;
    }

    public List<PhoneInstanceDTO> getPhoneInstanceList()
    {
        return phoneInstanceList;
    }

    public void setPhoneInstanceList(List<PhoneInstanceDTO> phoneInstanceList)
    {
        this.phoneInstanceList = phoneInstanceList;
    }

    public boolean getHasErrors()
    {
        return hasErrors;
    }

    public void setHasErrors(boolean hasErrors)
    {
        this.hasErrors = hasErrors;
    }

    public MessageRegisterDTO getMessageRegister()
    {
        return messageRegister;
    }

    public void setMessageRegister(MessageRegisterDTO messageRegister)
    {
        this.messageRegister = messageRegister;
    }

    public List<ValidationResultDTO> getValidationResults()
    {
        return validationResults;
    }

    public void setValidationResults(List<ValidationResultDTO> validationResults)
    {
        this.validationResults = validationResults;
    }

    public List<SmscInstanceDTO> getSmscInstances()
    {
        return smscInstances;
    }

    public void setSmscInstances(List<SmscInstanceDTO> smscInstances)
    {
        this.smscInstances = smscInstances;
    }

    public MmscInstanceDTO getMmscInstance()
    {
        return mmscInstance;
    }

    public void setMmscInstance(MmscInstanceDTO mmscInstance)
    {
        this.mmscInstance = mmscInstance;
    }

    public long getCreatedDT()
    {
        return createdDT;
    }

    public void setCreatedDT(long createdDT)
    {
        this.createdDT = createdDT;
    }

    public List<TestErrorDTO> getTestErrors()
    {
        return testErrors;
    }

    public void setTestErrors(List<TestErrorDTO> testErrors)
    {
        this.testErrors = testErrors;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTestName()
    {
        return testName;
    }

    public void setTestName(String testName)
    {
        this.testName = testName;
    }

    public String getTestDesc()
    {
        return testDesc;
    }

    public void setTestDesc(String testDesc)
    {
        this.testDesc = testDesc;
    }

    public boolean getClosed()
    {
        return closed;
    }

    public YesNoNaN getValidationErrors()
    {
        return validationErrors;
    }

    public void setValidationErrors(YesNoNaN validationErrors)
    {
        this.validationErrors = validationErrors;
    }

    public TestGroupModelDTO getTestGroupModelDTO()
    {
        return testGroupModelDTO;
    }

    public void setTestGroupModelDTO(TestGroupModelDTO testGroupModelDTO)
    {
        this.testGroupModelDTO = testGroupModelDTO;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServerTestInstanceDetailDTO that = (ServerTestInstanceDetailDTO) o;
        return id == that.id &&
                closed == that.closed &&
                createdDT == that.createdDT &&
                hasErrors == that.hasErrors &&
                Objects.equals(testId, that.testId) &&
                Objects.equals(testGroupModelDTO, that.testGroupModelDTO) &&
                Objects.equals(testName, that.testName) &&
                Objects.equals(testDesc, that.testDesc) &&
                Objects.equals(clientLibInstance, that.clientLibInstance) &&
                Objects.equals(phoneInstanceList, that.phoneInstanceList) &&
                Objects.equals(smscInstances, that.smscInstances) &&
                Objects.equals(mmscInstance, that.mmscInstance) &&
                Objects.equals(messageRegister, that.messageRegister) &&
                Objects.equals(validationResults, that.validationResults) &&
                Objects.equals(testErrors, that.testErrors) &&
                validationErrors == that.validationErrors;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, testId, testGroupModelDTO, testName, testDesc, closed, clientLibInstance, phoneInstanceList, smscInstances, mmscInstance, messageRegister, validationResults, testErrors, createdDT, hasErrors, validationErrors);
    }
}
