package cz.uhk.dip.mmsparams.server.database.dto;

import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.SmppSessionCounters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.server.tests.SmscInstance;

@Entity
public class SmscInstanceDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String smscSessionId;
    @Column
    private long boundTime;
    @Column
    private byte interfaceVersion;
    @Column
    private String localType;
    @Column
    private String remoteType;
    @Column
    private boolean useSsl;
    @Column
    private boolean countersEnabled;
    @Column
    private long writeTimeout;
    @Column
    private long windowWaitTimeout;
    @Column
    private int windowSize;
    @Column
    private long windowMonitorInterval;
    @Column
    private String type;
    @Column
    private String systemType;
    @Column
    private String systemId;
    @Column
    private long requestExpiryTimeout;
    @Column
    private int port;
    @Column
    private String password;
    @Column
    private String name;
    @Column
    private String host;
    @Column
    private long connectTimeout;
    @Column
    private long bindTimeout;
    @Column
    private String addressRange;


    public static List<SmscInstanceDTO> create(final List<SmscInstance> smscInstances)
    {
        List<SmscInstanceDTO> list = new ArrayList<>();
        if (smscInstances == null)
            return list;
        for (SmscInstance i : smscInstances)
            list.add(create(i));
        return list;
    }

    @Nullable
    public static SmscInstanceDTO create(final SmscInstance smscInstances)
    {
        if (smscInstances == null)
            return null;

        SmscInstanceDTO dto = new SmscInstanceDTO();


        dto.smscSessionId = smscInstances.getSmscSessionId() == null ? null : smscInstances.getSmscSessionId().getSmscSessionId();

        if (smscInstances.getSmscSession() != null)
        {
            SmppSession session = smscInstances.getSmscSession();

            // TODO ingored now
            StringUtil.getStringSafe(session.getBindType());

            dto.boundTime = session.getBoundTime();
            dto.interfaceVersion = session.getInterfaceVersion();
            dto.localType = StringUtil.getStringSafe(session.getLocalType());
            dto.remoteType = StringUtil.getStringSafe(session.getRemoteType());

            if (session.getConfiguration() != null)
            {
                SmppSessionConfiguration conf = session.getConfiguration(); // more
                // TODO ingored now

                dto.addressRange = StringUtil.getStringSafe(conf.getAddressRange());
                dto.bindTimeout = conf.getBindTimeout();
                dto.connectTimeout = conf.getConnectTimeout();
                dto.host = conf.getHost();
                dto.interfaceVersion = conf.getInterfaceVersion();
                //dto.loggingOptions = conf.getLoggingOptions();
                dto.name = conf.getName();
                dto.password = conf.getPassword();
                dto.port = conf.getPort();
                dto.requestExpiryTimeout = conf.getRequestExpiryTimeout();
                // dto.sslConfiguration = conf.getSslConfiguration();
                dto.systemId = conf.getSystemId();
                dto.systemType = conf.getSystemType();
                dto.type = StringUtil.getStringSafe(conf.getType());
                dto.windowMonitorInterval = conf.getWindowMonitorInterval();
                dto.windowSize = conf.getWindowSize();
                dto.windowWaitTimeout = conf.getWindowWaitTimeout();
                dto.writeTimeout = conf.getWriteTimeout();
                dto.countersEnabled = conf.isCountersEnabled();
                dto.useSsl = conf.isUseSsl();


            }
            if (session.getCounters() != null)
            {
                SmppSessionCounters counters = session.getCounters(); // MORE
                // TODO ingored now
            }
        }

        return dto;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getSmscSessionId()
    {
        return smscSessionId;
    }

    public void setSmscSessionId(String smscSessionId)
    {
        this.smscSessionId = smscSessionId;
    }

    public long getBoundTime()
    {
        return boundTime;
    }

    public void setBoundTime(long boundTime)
    {
        this.boundTime = boundTime;
    }

    public byte getInterfaceVersion()
    {
        return interfaceVersion;
    }

    public void setInterfaceVersion(byte interfaceVersion)
    {
        this.interfaceVersion = interfaceVersion;
    }

    public String getLocalType()
    {
        return localType;
    }

    public void setLocalType(String localType)
    {
        this.localType = localType;
    }

    public String getRemoteType()
    {
        return remoteType;
    }

    public void setRemoteType(String remoteType)
    {
        this.remoteType = remoteType;
    }

    public boolean getUseSsl()
    {
        return useSsl;
    }

    public void setUseSsl(boolean useSsl)
    {
        this.useSsl = useSsl;
    }

    public boolean getCountersEnabled()
    {
        return countersEnabled;
    }

    public void setCountersEnabled(boolean countersEnabled)
    {
        this.countersEnabled = countersEnabled;
    }

    public long getWriteTimeout()
    {
        return writeTimeout;
    }

    public void setWriteTimeout(long writeTimeout)
    {
        this.writeTimeout = writeTimeout;
    }

    public long getWindowWaitTimeout()
    {
        return windowWaitTimeout;
    }

    public void setWindowWaitTimeout(long windowWaitTimeout)
    {
        this.windowWaitTimeout = windowWaitTimeout;
    }

    public int getWindowSize()
    {
        return windowSize;
    }

    public void setWindowSize(int windowSize)
    {
        this.windowSize = windowSize;
    }

    public long getWindowMonitorInterval()
    {
        return windowMonitorInterval;
    }

    public void setWindowMonitorInterval(long windowMonitorInterval)
    {
        this.windowMonitorInterval = windowMonitorInterval;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getSystemType()
    {
        return systemType;
    }

    public void setSystemType(String systemType)
    {
        this.systemType = systemType;
    }

    public String getSystemId()
    {
        return systemId;
    }

    public void setSystemId(String systemId)
    {
        this.systemId = systemId;
    }

    public long getRequestExpiryTimeout()
    {
        return requestExpiryTimeout;
    }

    public void setRequestExpiryTimeout(long requestExpiryTimeout)
    {
        this.requestExpiryTimeout = requestExpiryTimeout;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public long getConnectTimeout()
    {
        return connectTimeout;
    }

    public void setConnectTimeout(long connectTimeout)
    {
        this.connectTimeout = connectTimeout;
    }

    public long getBindTimeout()
    {
        return bindTimeout;
    }

    public void setBindTimeout(long bindTimeout)
    {
        this.bindTimeout = bindTimeout;
    }

    public String getAddressRange()
    {
        return addressRange;
    }

    public void setAddressRange(String addressRange)
    {
        this.addressRange = addressRange;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SmscInstanceDTO that = (SmscInstanceDTO) o;
        return id == that.id &&
                boundTime == that.boundTime &&
                interfaceVersion == that.interfaceVersion &&
                useSsl == that.useSsl &&
                countersEnabled == that.countersEnabled &&
                writeTimeout == that.writeTimeout &&
                windowWaitTimeout == that.windowWaitTimeout &&
                windowSize == that.windowSize &&
                windowMonitorInterval == that.windowMonitorInterval &&
                requestExpiryTimeout == that.requestExpiryTimeout &&
                port == that.port &&
                connectTimeout == that.connectTimeout &&
                bindTimeout == that.bindTimeout &&
                Objects.equals(smscSessionId, that.smscSessionId) &&
                Objects.equals(localType, that.localType) &&
                Objects.equals(remoteType, that.remoteType) &&
                Objects.equals(type, that.type) &&
                Objects.equals(systemType, that.systemType) &&
                Objects.equals(systemId, that.systemId) &&
                Objects.equals(password, that.password) &&
                Objects.equals(name, that.name) &&
                Objects.equals(host, that.host) &&
                Objects.equals(addressRange, that.addressRange);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, smscSessionId, boundTime, interfaceVersion, localType, remoteType, useSsl, countersEnabled, writeTimeout, windowWaitTimeout, windowSize, windowMonitorInterval, type, systemType, systemId, requestExpiryTimeout, port, password, name, host, connectTimeout, bindTimeout, addressRange);
    }
}
