package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.SubscriptionInfoModel;

@Entity
public class SubscriptionInfoModelDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private int subscriptionId;
    @Column
    private String iccId;
    @Column
    private String carrierName;
    @Column
    private String displayName;
    @Column
    private int iconTint;
    @Column
    private int mcc;
    @Column
    private int mnc;
    @Column
    private String countryIso;
    @Column
    private int simSlotIndex;
    @Column
    private String number;
    @Column
    private int dataRoaming;


    public static SubscriptionInfoModelDTO[] create(SubscriptionInfoModel[] subscriptionInfoModel)
    {
        if (subscriptionInfoModel == null)
            return null;

        SubscriptionInfoModelDTO[] dtos = new SubscriptionInfoModelDTO[subscriptionInfoModel.length];
        for (int i = 0; i < subscriptionInfoModel.length; i++)
        {
            dtos[i] = create(subscriptionInfoModel[i]);
        }
        return dtos;
    }

    @Nullable
    public static SubscriptionInfoModelDTO create(SubscriptionInfoModel subscriptionInfoModel)
    {
        if (subscriptionInfoModel == null)
            return null;

        SubscriptionInfoModelDTO dto = new SubscriptionInfoModelDTO();
        dto.subscriptionId = subscriptionInfoModel.getSubscriptionId();
        dto.iccId = subscriptionInfoModel.getIccId();
        dto.carrierName = subscriptionInfoModel.getCarrierName();
        dto.displayName = subscriptionInfoModel.getDisplayName();
        dto.iconTint = subscriptionInfoModel.getIconTint();
        dto.mcc = subscriptionInfoModel.getMcc();
        dto.mnc = subscriptionInfoModel.getMnc();
        dto.countryIso = subscriptionInfoModel.getCountryIso();
        dto.simSlotIndex = subscriptionInfoModel.getSimSlotIndex();
        dto.number = subscriptionInfoModel.getNumber();
        dto.dataRoaming = subscriptionInfoModel.getDataRoaming();
        return dto;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getSubscriptionId()
    {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId)
    {
        this.subscriptionId = subscriptionId;
    }

    public String getIccId()
    {
        return iccId;
    }

    public void setIccId(String iccId)
    {
        this.iccId = iccId;
    }

    public String getCarrierName()
    {
        return carrierName;
    }

    public void setCarrierName(String carrierName)
    {
        this.carrierName = carrierName;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public int getIconTint()
    {
        return iconTint;
    }

    public void setIconTint(int iconTint)
    {
        this.iconTint = iconTint;
    }

    public int getMcc()
    {
        return mcc;
    }

    public void setMcc(int mcc)
    {
        this.mcc = mcc;
    }

    public int getMnc()
    {
        return mnc;
    }

    public void setMnc(int mnc)
    {
        this.mnc = mnc;
    }

    public String getCountryIso()
    {
        return countryIso;
    }

    public void setCountryIso(String countryIso)
    {
        this.countryIso = countryIso;
    }

    public int getSimSlotIndex()
    {
        return simSlotIndex;
    }

    public void setSimSlotIndex(int simSlotIndex)
    {
        this.simSlotIndex = simSlotIndex;
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public int getDataRoaming()
    {
        return dataRoaming;
    }

    public void setDataRoaming(int dataRoaming)
    {
        this.dataRoaming = dataRoaming;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriptionInfoModelDTO that = (SubscriptionInfoModelDTO) o;
        return id == that.id &&
                subscriptionId == that.subscriptionId &&
                iconTint == that.iconTint &&
                mcc == that.mcc &&
                mnc == that.mnc &&
                simSlotIndex == that.simSlotIndex &&
                dataRoaming == that.dataRoaming &&
                Objects.equals(iccId, that.iccId) &&
                Objects.equals(carrierName, that.carrierName) &&
                Objects.equals(displayName, that.displayName) &&
                Objects.equals(countryIso, that.countryIso) &&
                Objects.equals(number, that.number);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, subscriptionId, iccId, carrierName, displayName, iconTint, mcc, mnc, countryIso, simSlotIndex, number, dataRoaming);
    }
}

