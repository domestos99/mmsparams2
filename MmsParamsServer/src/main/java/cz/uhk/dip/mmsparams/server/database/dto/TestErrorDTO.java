package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;

@Entity
public class TestErrorDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Enumerated(EnumType.STRING)
    private TestErrorType testErrorType;

    @Column
    @Lob
    private String message;

    @Column
    @Lob
    private String exception;

    public static List<TestErrorDTO> create(List<TestErrorModel> list)
    {
        List<TestErrorDTO> l = new ArrayList<>();

        if (list == null)
            return l;

        for (TestErrorModel ci : list)
            l.add(create(ci));
        return l;
    }

    public static TestErrorDTO create(TestErrorModel model)
    {
        TestErrorDTO dto = new TestErrorDTO();

        dto.testErrorType = model.getTestErrorType();
        dto.message = model.getMessage();
        dto.exception = ExceptionHelper.getStackTrace(model.getException());

        return dto;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public TestErrorType getTestErrorType()
    {
        return testErrorType;
    }

    public void setTestErrorType(TestErrorType testErrorType)
    {
        this.testErrorType = testErrorType;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getException()
    {
        return exception;
    }

    public void setException(String exception)
    {
        this.exception = exception;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestErrorDTO that = (TestErrorDTO) o;
        return id == that.id &&
                testErrorType == that.testErrorType &&
                Objects.equals(message, that.message) &&
                Objects.equals(exception, that.exception);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, testErrorType, message, exception);
    }
}
