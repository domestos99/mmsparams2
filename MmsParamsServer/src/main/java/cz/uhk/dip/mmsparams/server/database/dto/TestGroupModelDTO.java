package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.TestGroupModel;

@Entity
public class TestGroupModelDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String testGroupID;

    @Column
    private String testGroupName;

    @Column
    private String testGroupDesc;


    public static TestGroupModelDTO create(TestGroupModel obj)
    {
        if (obj == null)
            return null;

        TestGroupModelDTO dto = new TestGroupModelDTO();
        dto.testGroupID = obj.getTestGroupID();
        dto.testGroupName = obj.getTestGroupName();
        dto.testGroupDesc = obj.getTestGroupDesc();
        return dto;
    }

    public String getTestGroupID()
    {
        return testGroupID;
    }

    public void setTestGroupID(String testGroupID)
    {
        this.testGroupID = testGroupID;
    }

    public String getTestGroupName()
    {
        return testGroupName;
    }

    public void setTestGroupName(String testGroupName)
    {
        this.testGroupName = testGroupName;
    }

    public String getTestGroupDesc()
    {
        return testGroupDesc;
    }

    public void setTestGroupDesc(String testGroupDesc)
    {
        this.testGroupDesc = testGroupDesc;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestGroupModelDTO that = (TestGroupModelDTO) o;
        return id == that.id &&
                Objects.equals(testGroupID, that.testGroupID) &&
                Objects.equals(testGroupName, that.testGroupName) &&
                Objects.equals(testGroupDesc, that.testGroupDesc);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, testGroupID, testGroupName, testGroupDesc);
    }
}
