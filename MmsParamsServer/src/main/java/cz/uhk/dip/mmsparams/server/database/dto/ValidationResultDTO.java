package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;

@Entity
public class ValidationResultDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String validationItemName;
    @Column
    @Lob
    private String expected;
    @Column
    @Lob
    private String actual;
    @Column
    private String validationName;
    @Column
    private Boolean result;

    @Nullable
    public static List<ValidationResultDTO> create(List<ValidationResult> validationResult)
    {
        if (validationResult == null)
            return null;
        List<ValidationResultDTO> list = new ArrayList<>();
        for (ValidationResult r : validationResult)
            list.add(create(r));
        return list;
    }

    @Nullable
    public static ValidationResultDTO create(ValidationResult validationResult)
    {
        if (validationResult == null)
            return null;

        ValidationResultDTO dto = new ValidationResultDTO();
        dto.validationItemName = validationResult.getValidationItemName();
        dto.expected = StringUtil.getStringSafe(validationResult.getExpected());
        dto.actual = StringUtil.getStringSafe(validationResult.getActual());
        dto.validationName = validationResult.getValidationName();
        dto.result = validationResult.getResult();
        return dto;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getValidationItemName()
    {
        return validationItemName;
    }

    public void setValidationItemName(String validationItemName)
    {
        this.validationItemName = validationItemName;
    }

    public String getExpected()
    {
        return expected;
    }

    public void setExpected(String expected)
    {
        this.expected = expected;
    }

    public String getActual()
    {
        return actual;
    }

    public void setActual(String actual)
    {
        this.actual = actual;
    }

    public String getValidationName()
    {
        return validationName;
    }

    public void setValidationName(String validationName)
    {
        this.validationName = validationName;
    }

    public Boolean getResult()
    {
        return result;
    }

    public void setResult(Boolean result)
    {
        this.result = result;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidationResultDTO that = (ValidationResultDTO) o;
        return id == that.id &&
                Objects.equals(validationItemName, that.validationItemName) &&
                Objects.equals(expected, that.expected) &&
                Objects.equals(actual, that.actual) &&
                Objects.equals(validationName, that.validationName) &&
                Objects.equals(result, that.result);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, validationItemName, expected, actual, validationName, result);
    }
}
