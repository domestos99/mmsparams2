package cz.uhk.dip.mmsparams.server.database.dto;

import org.springframework.web.socket.WebSocketSession;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import cz.uhk.dip.mmsparams.api.utils.ListUtils;

@Entity
public class WSSessionDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int genid;

    @Column
    private String id;
    @Column
    private String acceptedProtocol;
    @Column
    private int textMessageSizeLimit;
    @Column
    private String remoteAddress;

    @ElementCollection
    @Lob
    private List<String> extensions;
    @Column
    private String localAddress;
    @Column
    private String principal;

    public static List<WSSessionDTO> create(List<WebSocketSession> openSessions)
    {
        List<WSSessionDTO> list = new ArrayList<>();

        for (WebSocketSession ws : openSessions)
        {
            list.add(create(ws));
        }

        return list;
    }

    @Nullable
    public static WSSessionDTO create(WebSocketSession ws)
    {
        if (ws == null)
            return null;

        WSSessionDTO dto = new WSSessionDTO();

        dto.setId(ws.getId());
        dto.setAcceptedProtocol(ws.getAcceptedProtocol());
        dto.setTextMessageSizeLimit(ws.getTextMessageSizeLimit());
        dto.setRemoteAddress(ws.getRemoteAddress() == null ? null : ws.getRemoteAddress().toString());
        dto.setExtensions(ListUtils.getString(ws.getExtensions()));
        dto.setLocalAddress(ws.getLocalAddress() == null ? null : ws.getLocalAddress().toString());
        dto.setPrincipal(ws.getPrincipal() == null ? null : ws.getPrincipal().toString());

        return dto;
    }

    public int getGenid()
    {
        return genid;
    }

    public void setGenid(int genid)
    {
        this.genid = genid;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setAcceptedProtocol(String acceptedProtocol)
    {
        this.acceptedProtocol = acceptedProtocol;
    }

    public String getAcceptedProtocol()
    {
        return acceptedProtocol;
    }

    public void setTextMessageSizeLimit(int textMessageSizeLimit)
    {
        this.textMessageSizeLimit = textMessageSizeLimit;
    }

    public int getTextMessageSizeLimit()
    {
        return textMessageSizeLimit;
    }

    public void setRemoteAddress(String remoteAddress)
    {
        this.remoteAddress = remoteAddress;
    }

    public String getRemoteAddress()
    {
        return remoteAddress;
    }

    public void setExtensions(List<String> extensions)
    {
        this.extensions = extensions;
    }

    public List<String> getExtensions()
    {
        return extensions;
    }

    public void setLocalAddress(String localAddress)
    {
        this.localAddress = localAddress;
    }

    public String getLocalAddress()
    {
        return localAddress;
    }

    public void setPrincipal(String principal)
    {
        this.principal = principal;
    }

    public String getPrincipal()
    {
        return principal;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WSSessionDTO that = (WSSessionDTO) o;
        return genid == that.genid &&
                textMessageSizeLimit == that.textMessageSizeLimit &&
                Objects.equals(id, that.id) &&
                Objects.equals(acceptedProtocol, that.acceptedProtocol) &&
                Objects.equals(remoteAddress, that.remoteAddress) &&
                Objects.equals(extensions, that.extensions) &&
                Objects.equals(localAddress, that.localAddress) &&
                Objects.equals(principal, that.principal);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(genid, id, acceptedProtocol, textMessageSizeLimit, remoteAddress, extensions, localAddress, principal);
    }
}
