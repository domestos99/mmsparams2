package cz.uhk.dip.mmsparams.server.database.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import cz.uhk.dip.mmsparams.api.enums.MessageDirection;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.messages.MessageRegisterItem;

@Entity
public class WebSocketMessageDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String senderKey;
    @Column
    private String recipientKey;
    @Column
    private String messageKey;
    @Column
    private String messageID;
    @Column
    private String testID;
    @Column
    private long createdDT;
    @Column
    private long insertedDT;

    @Enumerated(EnumType.STRING)
    private MessageDirection messageDirection;

    @Column
    @Lob
    private String jsonContent;

    @Nullable
    public static List<WebSocketMessageDTO> create(List<MessageRegisterItem> msgs)
    {
        if (msgs == null)
            return null;

        ArrayList<WebSocketMessageDTO> list = new ArrayList<>();
        for (MessageRegisterItem m : msgs)
            list.add(create(m));

        return list;
    }

    @Nullable
    public static WebSocketMessageDTO create(MessageRegisterItem msg)
    {
        if (msg == null)
            return null;
        WebSocketMessageDTO dto = new WebSocketMessageDTO();
        dto.senderKey = msg.getMessage().getSenderKey();
        dto.recipientKey = msg.getMessage().getRecipientKey();
        dto.messageKey = msg.getMessage().getMessageKey();
        dto.messageID = msg.getMessage().getMessageID();
        dto.testID = msg.getMessage().getTestID();
        dto.createdDT = msg.getMessage().getCreatedDT();
        dto.insertedDT = msg.getInsertedDT();
        dto.jsonContent = JsonUtilsSafe.toJson(msg);
        dto.messageDirection = msg.getDirection();

        return dto;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getSenderKey()
    {
        return senderKey;
    }

    public void setSenderKey(String senderKey)
    {
        this.senderKey = senderKey;
    }

    public String getRecipientKey()
    {
        return recipientKey;
    }

    public void setRecipientKey(String recipientKey)
    {
        this.recipientKey = recipientKey;
    }

    public String getMessageKey()
    {
        return messageKey;
    }

    public void setMessageKey(String messageKey)
    {
        this.messageKey = messageKey;
    }

    public String getMessageID()
    {
        return messageID;
    }

    public void setMessageID(String messageID)
    {
        this.messageID = messageID;
    }

    public String getTestID()
    {
        return testID;
    }

    public void setTestID(String testID)
    {
        this.testID = testID;
    }

    public long getCreatedDT()
    {
        return createdDT;
    }

    public void setCreatedDT(long createdDT)
    {
        this.createdDT = createdDT;
    }

    public String getJsonContent()
    {
        return jsonContent;
    }

    public void setJsonContent(String jsonContent)
    {
        this.jsonContent = jsonContent;
    }

    public long getInsertedDT()
    {
        return insertedDT;
    }

    public void setInsertedDT(long insertedDT)
    {
        this.insertedDT = insertedDT;
    }

    public MessageDirection getMessageDirection()
    {
        return messageDirection;
    }

    public void setMessageDirection(MessageDirection messageDirection)
    {
        this.messageDirection = messageDirection;
    }

    //    public MessageRegisterDTO getMessageRegisterDTO()
//    {
//        return messageRegisterDTO;
//    }
//
//    public void setMessageRegisterDTO(MessageRegisterDTO messageRegisterDTO)
//    {
//        this.messageRegisterDTO = messageRegisterDTO;
//    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WebSocketMessageDTO that = (WebSocketMessageDTO) o;
        return id == that.id &&
                createdDT == that.createdDT &&
                insertedDT == that.insertedDT &&
                Objects.equals(senderKey, that.senderKey) &&
                Objects.equals(recipientKey, that.recipientKey) &&
                Objects.equals(messageKey, that.messageKey) &&
                Objects.equals(messageID, that.messageID) &&
                Objects.equals(testID, that.testID) &&
                Objects.equals(jsonContent, that.jsonContent);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, senderKey, recipientKey, messageKey, messageID, testID, createdDT, insertedDT, jsonContent);
    }
}
