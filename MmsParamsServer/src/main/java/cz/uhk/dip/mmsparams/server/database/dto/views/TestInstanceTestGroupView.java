package cz.uhk.dip.mmsparams.server.database.dto.views;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import cz.uhk.dip.mmsparams.api.constants.YesNoNaN;
import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.TestGroupModelDTO;

public class TestInstanceTestGroupView implements Serializable
{
    private TestGroupModelDTO testGroupModelDTO;
    private List<ServerTestInstanceDTO> serverTestInstanceDTOS;
    private boolean hasErrors;
    private YesNoNaN validationErrors;
    private long createdDT;


    public TestInstanceTestGroupView()
    {
        this.serverTestInstanceDTOS = ListUtils.newList();
    }

    public TestGroupModelDTO getTestGroupModelDTO()
    {
        return testGroupModelDTO;
    }

    public void setTestGroupModelDTO(TestGroupModelDTO testGroupModelDTO)
    {
        this.testGroupModelDTO = testGroupModelDTO;
    }

    public List<ServerTestInstanceDTO> getServerTestInstanceDTOS()
    {
        return serverTestInstanceDTOS;
    }

    public void setServerTestInstanceDTOS(List<ServerTestInstanceDTO> serverTestInstanceDTOS)
    {
        this.serverTestInstanceDTOS = serverTestInstanceDTOS;
    }

    public boolean isHasErrors()
    {
        return hasErrors;
    }

    public void setHasErrors(boolean hasErrors)
    {
        this.hasErrors = hasErrors;
    }

    public YesNoNaN getValidationErrors()
    {
        return validationErrors;
    }

    public void setValidationErrors(YesNoNaN validationErrors)
    {
        this.validationErrors = validationErrors;
    }

    public long getCreatedDT()
    {
        return createdDT;
    }

    public void setCreatedDT(long createdDT)
    {
        this.createdDT = createdDT;
    }

    public void fillExtraProperties()
    {
        this.setHasErrors(false);
        this.setValidationErrors(YesNoNaN.NAN);
        this.createdDT = Long.MAX_VALUE;

        boolean hasValidationErrorYes = false;
        boolean hasValidationErrorNan = false;


        for (ServerTestInstanceDTO a : getServerTestInstanceDTOS())
        {
            // If any has error, mark whole as error
            if (a.getHasErrors())
            {
                this.setHasErrors(true);
            }

            // If any has validation error, mark whole as validation error
            if (a.getValidationErrors() == YesNoNaN.YES)
            {
                hasValidationErrorYes = true;
            }
            // If any has validation NAN, mark whole as NAN, unless any has error
            else if (a.getValidationErrors() == YesNoNaN.NAN)
            {
                hasValidationErrorNan = true;
            }

            // Find earliest created time - first test started
            if (a.getCreatedDT() < this.createdDT)
            {
                this.setCreatedDT(a.getCreatedDT());
            }
        }

        if (hasValidationErrorYes)
        {
            this.setValidationErrors(YesNoNaN.YES);
        }
        else if (hasValidationErrorNan)
        {
            this.setValidationErrors(YesNoNaN.NAN);
        }
        else
        {
            this.setValidationErrors(YesNoNaN.NO);
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestInstanceTestGroupView that = (TestInstanceTestGroupView) o;
        return hasErrors == that.hasErrors &&
                createdDT == that.createdDT &&
                Objects.equals(testGroupModelDTO, that.testGroupModelDTO) &&
                Objects.equals(serverTestInstanceDTOS, that.serverTestInstanceDTOS) &&
                validationErrors == that.validationErrors;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(testGroupModelDTO, serverTestInstanceDTOS, hasErrors, validationErrors, createdDT);
    }
}


