package cz.uhk.dip.mmsparams.server.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.server.database.dao.ClientInfoDBDAO;
import cz.uhk.dip.mmsparams.server.database.dto.ClientLibInstanceDTO;

@Service
public class ClientInfoDBService implements IClientInfoDBService
{
    private final ClientInfoDBDAO phoneInfoDBDAO;

    @Autowired
    public ClientInfoDBService(ClientInfoDBDAO phoneInfoDBDAO)
    {
        this.phoneInfoDBDAO = phoneInfoDBDAO;
    }

    @Override
    public Optional<ClientLibInstanceDTO> getByClientKey(String clientKey)
    {
        if (StringUtil.isEmptyOrNull(clientKey))
            return Optional.empty();

        List<ClientLibInstanceDTO> phones = phoneInfoDBDAO.findByClientKey(clientKey);
        if (phones.size() > 0)
            return Optional.of(phones.get(0));
        return Optional.empty();
    }
}
