package cz.uhk.dip.mmsparams.server.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import cz.uhk.dip.mmsparams.api.web.model.ClientServerLogModel;
import cz.uhk.dip.mmsparams.server.comparators.ClientServerLogItemComparatorAcs;
import cz.uhk.dip.mmsparams.server.database.dao.ClientServerLogDAO;
import cz.uhk.dip.mmsparams.server.database.dto.ClientServerLogDTO;

@Service
public class ClientServerLogDBService implements IClientServerLogDBService
{
    private final ClientServerLogDAO clientServerLogDAO;

    @Autowired
    public ClientServerLogDBService(ClientServerLogDAO clientServerLogDAO)
    {
        this.clientServerLogDAO = clientServerLogDAO;
    }


    @Override
    public void save(ClientServerLogModel model)
    {
        ClientServerLogDTO dto = ClientServerLogDTO.create(model);
        if (dto != null)
        {
            this.clientServerLogDAO.save(dto);
        }
    }

    @Override
    public Optional<ClientServerLogDTO> getByTestID(String testId)
    {
        Optional<ClientServerLogDTO> opt =  this.clientServerLogDAO.findByTestID(testId);
        if (!opt.isPresent())
        {
            return opt;
        }

        opt.get().getItems().sort(new ClientServerLogItemComparatorAcs());
        return opt;
    }

    @Override
    public boolean delete(String testId)
    {
        Optional<ClientServerLogDTO> opt = getByTestID(testId);
        if (opt.isPresent())
        {
            this.clientServerLogDAO.delete(opt.get());
            return true;
        }
        return false;
    }
}
