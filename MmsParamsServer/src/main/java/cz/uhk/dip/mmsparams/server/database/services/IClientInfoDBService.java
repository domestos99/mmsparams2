package cz.uhk.dip.mmsparams.server.database.services;

import java.util.Optional;

import cz.uhk.dip.mmsparams.server.database.dto.ClientLibInstanceDTO;

public interface IClientInfoDBService
{
    Optional<ClientLibInstanceDTO> getByClientKey(String clientKey);
}
