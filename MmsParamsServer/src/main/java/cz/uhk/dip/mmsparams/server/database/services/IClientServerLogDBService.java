package cz.uhk.dip.mmsparams.server.database.services;

import java.util.Optional;

import cz.uhk.dip.mmsparams.api.web.model.ClientServerLogModel;
import cz.uhk.dip.mmsparams.server.database.dto.ClientServerLogDTO;

public interface IClientServerLogDBService
{
    public void save(ClientServerLogModel model);

    Optional<ClientServerLogDTO> getByTestID(String testId);

    boolean delete(String testId);
}
