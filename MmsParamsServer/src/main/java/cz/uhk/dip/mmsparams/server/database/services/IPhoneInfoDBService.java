package cz.uhk.dip.mmsparams.server.database.services;

import java.util.Optional;

import cz.uhk.dip.mmsparams.server.database.dto.PhoneInfoDTO;

public interface IPhoneInfoDBService
{
    Optional<PhoneInfoDTO> getByPhoneKey(String phoneKey);
}
