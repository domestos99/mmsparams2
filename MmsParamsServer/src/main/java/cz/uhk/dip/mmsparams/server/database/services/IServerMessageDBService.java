package cz.uhk.dip.mmsparams.server.database.services;

import java.util.Optional;

import cz.uhk.dip.mmsparams.server.database.dto.MessageSequenceList;
import cz.uhk.dip.mmsparams.server.database.dto.WebSocketMessageDTO;

public interface IServerMessageDBService
{
    Optional<WebSocketMessageDTO> getById(Integer id);

    Optional<MessageSequenceList> getSequenceByTestID(String testId);
}
