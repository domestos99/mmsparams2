package cz.uhk.dip.mmsparams.server.database.services;

import java.util.List;
import java.util.Optional;

import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDetailDTO;
import cz.uhk.dip.mmsparams.server.listeners.OnChangeListener;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

public interface IServerTestInstanceDBService
{
    int saveClosedTest(ServerTestInstance test);

    List<ServerTestInstanceDetailDTO> getAll();

    Optional<ServerTestInstanceDetailDTO> getByTestId(String testId);

    boolean delete(String testId);

    void setOnChangeListener(OnChangeListener<ServerTestInstanceDetailDTO> onChangeListener);

    List<ServerTestInstanceDTO> getReduced();

    boolean deleteOldTests(int days);
}
