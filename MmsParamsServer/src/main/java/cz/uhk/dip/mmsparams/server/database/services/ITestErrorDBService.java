package cz.uhk.dip.mmsparams.server.database.services;

import java.util.Optional;

import cz.uhk.dip.mmsparams.server.database.dto.TestErrorDTO;

public interface ITestErrorDBService
{
    Optional<TestErrorDTO> getById(int id);
}
