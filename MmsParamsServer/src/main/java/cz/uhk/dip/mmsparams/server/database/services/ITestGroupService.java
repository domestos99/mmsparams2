package cz.uhk.dip.mmsparams.server.database.services;

import java.util.List;

import cz.uhk.dip.mmsparams.server.database.dto.TestGroupModelDTO;

public interface ITestGroupService
{
    List<TestGroupModelDTO> getAll();
}
