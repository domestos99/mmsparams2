package cz.uhk.dip.mmsparams.server.database.services;

import java.util.List;

import cz.uhk.dip.mmsparams.server.database.dto.views.TestInstanceTestGroupView;

public interface ITestInstanceTestGroupViewService
{
    List<TestInstanceTestGroupView> getAll();
}
