package cz.uhk.dip.mmsparams.server.database.services;

import cz.uhk.dip.mmsparams.server.auth.UserModel;

public interface IUserService
{
    UserModel getUserByUserName(String username);

    UserModel getUser(String username, String password);
}
