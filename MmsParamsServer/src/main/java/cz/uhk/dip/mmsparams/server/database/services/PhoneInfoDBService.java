package cz.uhk.dip.mmsparams.server.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.server.database.dao.PhoneInfoDBDAO;
import cz.uhk.dip.mmsparams.server.database.dto.PhoneInfoDTO;

@Service
public class PhoneInfoDBService implements IPhoneInfoDBService
{
    private final PhoneInfoDBDAO phoneInfoDBDAO;

    @Autowired
    public PhoneInfoDBService(PhoneInfoDBDAO phoneInfoDBDAO)
    {
        this.phoneInfoDBDAO = phoneInfoDBDAO;
    }

    @Override
    public Optional<PhoneInfoDTO> getByPhoneKey(String phoneKey)
    {
        if (StringUtil.isEmptyOrNull(phoneKey))
            return Optional.empty();

        List<PhoneInfoDTO> phones = phoneInfoDBDAO.findByPhoneKey(phoneKey);
        if (phones.size() > 0)
            return Optional.of(phones.get(0));
        return Optional.empty();
    }
}
