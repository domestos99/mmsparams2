package cz.uhk.dip.mmsparams.server.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import cz.uhk.dip.mmsparams.server.cache.ServerTestInstanceDBCache;
import cz.uhk.dip.mmsparams.server.comparators.WebSocketMessageBaseDTOComparatorAsc;
import cz.uhk.dip.mmsparams.server.database.dao.ServerMessageDBDAO;
import cz.uhk.dip.mmsparams.server.database.dto.MessageSequenceList;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDetailDTO;
import cz.uhk.dip.mmsparams.server.database.dto.WebSocketMessageDTO;

@Service
public class ServerMessageDBService implements IServerMessageDBService
{
    private final ServerMessageDBDAO serverMessageDBDAO;

    private final ServerTestInstanceDBCache serverTestInstanceDBCache;

    @Autowired
    public ServerMessageDBService(ServerMessageDBDAO serverMessageDBDAO, ServerTestInstanceDBCache serverTestInstanceDBCache)
    {
        this.serverMessageDBDAO = serverMessageDBDAO;
        this.serverTestInstanceDBCache = serverTestInstanceDBCache;
    }

    @Override
    public Optional<WebSocketMessageDTO> getById(Integer id)
    {
        return serverMessageDBDAO.findById(id);
    }

    @Override
    public Optional<MessageSequenceList> getSequenceByTestID(String testId)
    {
        Optional<ServerTestInstanceDetailDTO> testDetailOpt = serverTestInstanceDBCache.getByTestId(testId);
        if (!testDetailOpt.isPresent())
            return Optional.empty();

        MessageSequenceList result = new MessageSequenceList();

        List<WebSocketMessageDTO> messages = new ArrayList<>();

        messages.addAll(testDetailOpt.get().getMessageRegister().getIncoming());
        messages.addAll(testDetailOpt.get().getMessageRegister().getOutgoing());

        messages.sort(new WebSocketMessageBaseDTOComparatorAsc());

        result.setTestID(testId);
        result.setMsgCount(messages.size());
        result.setMessages(messages);
        result.setPhones(testDetailOpt.get().getPhoneInstanceList());


        List<String> keys = new ArrayList<>();

        for (WebSocketMessageDTO m : messages)
        {
            if (!keys.contains(m.getSenderKey()))
                keys.add(m.getSenderKey());

            if (!keys.contains(m.getRecipientKey()))
                keys.add(m.getRecipientKey());
        }

        result.setEndpointCount(keys.size());


        return Optional.of(result);
    }


}
