package cz.uhk.dip.mmsparams.server.database.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import cz.uhk.dip.mmsparams.api.utils.LogUtil;
import cz.uhk.dip.mmsparams.server.database.dao.ServerTestInstanceDAO;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDetailDTO;
import cz.uhk.dip.mmsparams.server.listeners.OnChangeListener;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

@Service
@Transactional
public class ServerTestInstanceDBService implements IServerTestInstanceDBService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ServerTestInstanceDBService.class);
    private static final String TAG = ServerTestInstanceDBService.class.getSimpleName();

    private final ServerTestInstanceDAO serverTestInstanceDAO;
    private final IClientServerLogDBService clientServerLogDAO;
    private OnChangeListener<ServerTestInstanceDetailDTO> onChangeListener;
    private final EntityManager entityManager;

    @Autowired
    public ServerTestInstanceDBService(ServerTestInstanceDAO serverTestInstanceDAO, IClientServerLogDBService clientServerLogDAO, EntityManager entityManager)
    {
        this.serverTestInstanceDAO = serverTestInstanceDAO;
        this.clientServerLogDAO = clientServerLogDAO;
        this.entityManager = entityManager;
    }

    @Override
    public int saveClosedTest(final ServerTestInstance test)
    {
        ServerTestInstanceDetailDTO dto = ServerTestInstanceDetailDTO.create(test);
        ServerLogFacade.logInfo(LOGGER, TAG, "Saving test with testID: " + test.getTestID() + LogUtil.HASHES + LogUtil.NEW_LINE);
        try
        {
            save(dto);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "saveClosedTest", e);
        }
        if (onChangeListener != null)
            onChangeListener.onChange(dto);
        return dto.getId();
    }

    public void save(ServerTestInstanceDetailDTO dto)
    {
        this.serverTestInstanceDAO.save(dto);
    }

    @Override
    public List<ServerTestInstanceDetailDTO> getAll()
    {
        return this.serverTestInstanceDAO.findAll(new Sort(Sort.Direction.DESC, "createdDT"));
    }

    @Override
    public Optional<ServerTestInstanceDetailDTO> getByTestId(String testId)
    {
        return this.serverTestInstanceDAO.findByTestId(testId);
    }

    @Override
    public boolean delete(String testId)
    {
        Optional<ServerTestInstanceDetailDTO> dto = getByTestId(testId);
        if (dto.isPresent())
        {
            this.serverTestInstanceDAO.delete(dto.get());
            this.clientServerLogDAO.delete(testId);
            return true;
        }
        return false;
    }

    public OnChangeListener<ServerTestInstanceDetailDTO> getOnChangeListener()
    {
        return onChangeListener;
    }

    public void setOnChangeListener(OnChangeListener<ServerTestInstanceDetailDTO> onChangeListener)
    {
        this.onChangeListener = onChangeListener;
    }

    @Override
    public List<ServerTestInstanceDTO> getReduced()
    {
        TypedQuery<ServerTestInstanceDTO> query = this.entityManager.createQuery(
                "select new cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO(" +
                        "g, s.testId, s.closed, s.createdDT, s.testName, s.hasErrors, s.validationErrors)" +
                        "from ServerTestInstanceDetailDTO s left outer join " +
                        "TestGroupModelDTO g on g.id = s.testGroupModelDTO.id order by s.createdDT desc", ServerTestInstanceDTO.class);

        return query.getResultList();
    }

    @Override
    public boolean deleteOldTests(int days)
    {
        if (days < 1)
            return false;

        ServerLogFacade.logInfo(LOGGER, TAG, "Deleting tests older then " + days + " days");
        long limit = System.currentTimeMillis() - (days * 86400000L);
        List<ServerTestInstanceDTO> tests = getReduced();

        List<String> toDelete = new ArrayList<>();

        for (ServerTestInstanceDTO t : tests)
        {
            if (t.getCreatedDT() < limit)
            {
                toDelete.add(t.getTestId());
            }
        }

        for (String del : toDelete)
        {
            this.delete(del);
        }
        ServerLogFacade.logInfo(LOGGER, TAG, "Deleting tests older then " + days + " days: " + toDelete.size() + " tests deleted successfully");
        return true;
    }
}
