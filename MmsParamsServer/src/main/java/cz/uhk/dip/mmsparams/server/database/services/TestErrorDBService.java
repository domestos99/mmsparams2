package cz.uhk.dip.mmsparams.server.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import cz.uhk.dip.mmsparams.server.database.dao.TestErrorDAO;
import cz.uhk.dip.mmsparams.server.database.dto.TestErrorDTO;

@Service
public class TestErrorDBService implements ITestErrorDBService
{
    private final TestErrorDAO testErrorDAO;

    @Autowired
    public TestErrorDBService(TestErrorDAO testErrorDAO)
    {
        this.testErrorDAO = testErrorDAO;
    }

    @Override
    public Optional<TestErrorDTO> getById(int id)
    {
        Optional<TestErrorDTO> result = this.testErrorDAO.findById(id);
        return result;
    }
}
