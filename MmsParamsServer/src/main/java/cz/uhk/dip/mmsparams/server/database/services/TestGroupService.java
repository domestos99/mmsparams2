package cz.uhk.dip.mmsparams.server.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import cz.uhk.dip.mmsparams.server.database.dao.TestGroupDAO;
import cz.uhk.dip.mmsparams.server.database.dto.TestGroupModelDTO;

@Service
public class TestGroupService implements ITestGroupService
{
    private final TestGroupDAO testGroupDAO;

    @Autowired
    public TestGroupService(TestGroupDAO testGroupDAO)
    {
        this.testGroupDAO = testGroupDAO;
    }

    @Override
    public List<TestGroupModelDTO> getAll()
    {
        return testGroupDAO.findAll();
    }
}
