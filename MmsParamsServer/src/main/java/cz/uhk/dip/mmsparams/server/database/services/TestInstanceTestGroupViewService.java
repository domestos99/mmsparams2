package cz.uhk.dip.mmsparams.server.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.server.cache.ServerTestInstanceDBCache;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.views.TestInstanceTestGroupView;

@Service
public class TestInstanceTestGroupViewService implements ITestInstanceTestGroupViewService
{
    private final ServerTestInstanceDBCache serverTestInstanceDBCache;

    @Autowired
    public TestInstanceTestGroupViewService(ServerTestInstanceDBCache serverTestInstanceDBCache)
    {
        this.serverTestInstanceDBCache = serverTestInstanceDBCache;
    }

    public synchronized List<TestInstanceTestGroupView> getAll()
    {
        List<ServerTestInstanceDTO> data = serverTestInstanceDBCache.getAllReduced();

        LinkedHashMap<String, TestInstanceTestGroupView> temp = new LinkedHashMap<>();

        for (ServerTestInstanceDTO d : data)
        {
            if (d.getTestGroupModelDTO() == null)
                continue;

            String groupID = d.getTestGroupModelDTO().getTestGroupID();

            if (temp.containsKey(groupID))
            {
                TestInstanceTestGroupView a = temp.get(groupID);
                a.getServerTestInstanceDTOS().add(d);
            }
            else
            {
                TestInstanceTestGroupView a = new TestInstanceTestGroupView();
                a.setTestGroupModelDTO(d.getTestGroupModelDTO());
                a.getServerTestInstanceDTOS().add(d);
                temp.put(groupID, a);
            }
        }

        List<TestInstanceTestGroupView> result = ListUtils.toList(temp.values());

        for (TestInstanceTestGroupView r : result)
        {
            r.fillExtraProperties();
        }

        return result;
    }


}
