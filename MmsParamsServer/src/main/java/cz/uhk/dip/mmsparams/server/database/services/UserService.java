package cz.uhk.dip.mmsparams.server.database.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cz.uhk.dip.mmsparams.server.auth.UserModel;

@Service
public class UserService implements IUserService
{
    private final String rootUsername;
    private final String rootPassword;

    public UserService(@Value("${appconfig.rootusername}") String rootUsername, @Value("${appconfig.rootpassword}") String rootPassword)
    {
        this.rootUsername = rootUsername;
        this.rootPassword = rootPassword;
    }

    @Override
    public UserModel getUserByUserName(String username)
    {
        if (rootUsername.equals(username))
            return new UserModel(this.rootUsername, this.rootPassword);
        return null;
    }

    @Override
    public UserModel getUser(String username, String password)
    {
        if (rootUsername.equals(username) && this.rootPassword.equals(password))
            return new UserModel(this.rootUsername, this.rootPassword);
        return null;
    }
}
