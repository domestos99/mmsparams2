package cz.uhk.dip.mmsparams.server.interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.server.auth.IJwtTokenService;
import cz.uhk.dip.mmsparams.server.auth.SecurityConstants;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.utils.UriUtils;
import cz.uhk.dip.mmsparams.server.web.constant.WebConstant;
import io.jsonwebtoken.ExpiredJwtException;

@Component
@Order(3)
public class JwtTokenInterceptor extends OncePerRequestFilter
{
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenInterceptor.class);
    private static final String TAG = JwtTokenInterceptor.class.getSimpleName();

    private final IJwtTokenService iJwtTokenService;

    @Autowired
    public JwtTokenInterceptor(IJwtTokenService iJwtTokenService)
    {
        this.iJwtTokenService = iJwtTokenService;
        initNonAuthorizedAddresses();
    }

    private void initNonAuthorizedAddresses()
    {
        this.nonAuthorizedAddresses.add(SecurityConstants.AUTH_LOGIN_URL);
        this.nonAuthorizedAddresses.add(WebConstant.PING_PING);
        this.nonAuthorizedAddresses.add(WebConstant.SYSTEM_VERSION);
        this.nonAuthorizedAddresses.add("/v2/api-docs");
    }

    private List<String> nonAuthorizedAddresses = new ArrayList<>();

    @Override
    public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain) throws ServletException, IOException
    {
        final String uri = UriUtils.getRequestUri(request);

        boolean isAuthenticated = false;

        if (uri.contains("/api") && !nonAuthorizedAddresses.contains(uri))
        {
            // Check token
            final String requestTokenHeader = request.getHeader(HttpConstants.AUTHORIZATION);
            if (!StringUtil.isEmptyOrNull(requestTokenHeader) && requestTokenHeader.startsWith(HttpConstants.TOKEN_PREFIX))
            {
                String rawToken = requestTokenHeader.substring(HttpConstants.TOKEN_PREFIX.length());
                try
                {
                    boolean isTokenValid = this.iJwtTokenService.isTokenValid(rawToken);

                    if (isTokenValid)
                    {
                        isAuthenticated = true;
                    }
                    else
                    {
                        ServerLogFacade.logEx(LOGGER, TAG, "Token is not valid", null);
                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized: Token is not valid");
                    }
                }
                catch (IllegalArgumentException e)
                {
                    ServerLogFacade.logEx(LOGGER, TAG, "Unable to get JWT Token", e);
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized: Unable to get JWT Token");
                }
                catch (ExpiredJwtException e)
                {
                    ServerLogFacade.logEx(LOGGER, TAG, "JWT Token has expired", e);
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized: JWT Token has expired");
                }
                catch (Exception e)
                {
                    ServerLogFacade.logEx(LOGGER, TAG, "checkToken", e);
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
                }
            }
            else
            {
                ServerLogFacade.logWarning(LOGGER, TAG, "JWT Token does not begin with Bearer String");
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized: JWT Token does not begin with Bearer String");
            }
        }
        else
        {
            isAuthenticated = true;
        }

        if (isAuthenticated)
        {
            filterChain.doFilter(request, response);
        }
        else
        {
            if (!response.isCommitted())
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
