package cz.uhk.dip.mmsparams.server.interceptors;

import com.google.common.base.Stopwatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.utils.UriUtils;

@Component
public class PerformanceLogFilter implements Filter
{
    private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceLogFilter.class);
    private static final String TAG = PerformanceLogFilter.class.getSimpleName();

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        ServerLogFacade.logInfo(LOGGER, TAG, "Begin request (" + UriUtils.getRequestMethod(request) + "): " + UriUtils.getRequestUri(request));

        Stopwatch sw = Stopwatch.createStarted();
        try
        {
            chain.doFilter(request, response);
        }
        finally
        {
            sw.stop();

            int status = UriUtils.getResponseStatus(response);

            StringBuilder sb = new StringBuilder();
            sb.append("End request (");
            sb.append(UriUtils.getRequestMethod(request));
            sb.append(") : ");
            sb.append(UriUtils.getRequestUri(request));
            sb.append(" Status: ");
            sb.append(status);
            sb.append("; Duration: ");
            sb.append(sw.elapsed(TimeUnit.MILLISECONDS));
            sb.append(" ms");

            if (status != 200)
            {
                ServerLogFacade.logWarning(LOGGER, TAG, sb.toString());
            }
            else
            {
                ServerLogFacade.logInfo(LOGGER, TAG, sb.toString());
            }
        }
    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    @Override
    public void destroy()
    {

    }

}
