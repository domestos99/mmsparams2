package cz.uhk.dip.mmsparams.server.interceptors;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;

import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.server.auth.IJwtTokenService;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;

@Component
public class WebSocketHandshakeInterceptor implements HandshakeInterceptor
{
    private final IJwtTokenService iJwtTokenService;
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketHandshakeInterceptor.class);
    private static final String TAG = WebSocketHandshakeInterceptor.class.getSimpleName();

    @Autowired
    public WebSocketHandshakeInterceptor(final IJwtTokenService iJwtTokenService)
    {
        this.iJwtTokenService = iJwtTokenService;
    }

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes)
    {
        final String token = ((ServletServerHttpRequest) request).getServletRequest().getHeader(HttpConstants.AUTHORIZATION);

        boolean isValid = false;
        try
        {
            isValid = this.iJwtTokenService.isTokenValid(token);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "beforeHandshake", e);
        }

        if (isValid)
        {
            return true;
        }
        else
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "Invalid WebSocket header token");
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return false;
        }
    }

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception)
    {
        if (exception != null)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "afterHandshake", exception);
        }
    }
}
