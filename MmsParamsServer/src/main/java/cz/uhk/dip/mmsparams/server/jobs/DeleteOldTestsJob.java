package cz.uhk.dip.mmsparams.server.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.uhk.dip.mmsparams.server.database.services.IServerTestInstanceDBService;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;

@Service
public class DeleteOldTestsJob
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteOldTestsJob.class);
    private static final String TAG = DeleteOldTestsJob.class.getSimpleName();

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    private final IServerTestInstanceDBService iServerTestInstanceDBService;
    private final int deleteTestAfterMinutes;

    @Autowired
    public DeleteOldTestsJob(IServerTestInstanceDBService iServerTestInstanceDBService, @Value("${appconfig.delete_test_after_minutes}") int deleteTestAfterMinutes)
    {
        this.iServerTestInstanceDBService = iServerTestInstanceDBService;
        this.deleteTestAfterMinutes = deleteTestAfterMinutes;
    }

    //    @Scheduled(cron = "*/10 * * * * *")
    // @Scheduled(fixedDelay = 10000)
    public void deleteOldTestsJob()
    {
        if (this.deleteTestAfterMinutes <= 0)
            return;

        ServerLogFacade.logInfo(LOGGER, TAG, "deleteOldTestsJob: " + dateFormat.format(new Date()));
//        iServerTestInstanceDBService.deleteAfterMinutes(this.deleteTestAfterMinutes);
    }
}
