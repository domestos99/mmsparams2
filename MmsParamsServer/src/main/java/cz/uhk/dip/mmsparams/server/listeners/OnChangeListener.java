package cz.uhk.dip.mmsparams.server.listeners;

public interface OnChangeListener<T>
{
    void onChange(T o);
}
