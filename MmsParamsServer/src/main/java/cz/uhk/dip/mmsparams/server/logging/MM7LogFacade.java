package cz.uhk.dip.mmsparams.server.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.uhk.dip.mmsparams.api.utils.LogUtil;

public class MM7LogFacade
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MM7LogFacade.class);

    private MM7LogFacade()
    {
    }

    public static void logPostRequest(String contentType, String requestBody)
    {
        ServerLogFacade.logInfo(LOGGER, contentType + "\r\n" + requestBody);
        ServerLogFacade.logInfo(LOGGER, LogUtil.HASHES);
    }

    public static void logPostResponse(String responseBody)
    {
        ServerLogFacade.logInfo(LOGGER, responseBody);
        ServerLogFacade.logInfo(LOGGER, LogUtil.HASHES);
    }
}
