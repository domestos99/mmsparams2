package cz.uhk.dip.mmsparams.server.logging;

import org.slf4j.Logger;

import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class ServerLogFacade
{
    private ServerLogFacade()
    {
    }

    public static void logInfo(final Logger logger, String tag, String info)
    {
        logger.info(getMessage(tag, info));
    }

    public static void logInfo(final Logger logger, String info)
    {
        logInfo(logger, null, info);
    }

    public static void logWarning(final Logger logger, String tag, String warning)
    {
        logger.warn(getMessage(tag, warning));
    }

    public static void logEx(final Logger logger, Exception e)
    {
        logger.error("", e);
        ServerLogFatalFacade.logFatal(logger, e);
    }

    public static void logEx(final Logger logger, String tag, String message, Exception ex)
    {
        logger.error(getMessageWithStack(tag, message), ex);
        ServerLogFatalFacade.logFatal(logger, tag, message, ex);
    }

    public static void logWebSocket(final Logger logger, String info)
    {
        logger.info(info);
    }

    private static String getMessage(String tag, String message)
    {
        return tag + ": " + message + ": ";
    }

    private static String getMessageWithStack(String tag, String message)
    {
        return tag + ": " + message + "\n" + ExceptionHelper.getStackTrace(new Throwable());
    }

    public static void logWarningMsg(Logger logger, String tag, String message, WebSocketMessageBase msg)
    {
        logWarning(logger, tag, message + " " + msg);
    }

    public static void logExMessage(Logger logger, String tag, String message, Exception ex, WebSocketMessageBase request)
    {
        logEx(logger, tag, message + " " + request.toString(), ex);
    }
}
