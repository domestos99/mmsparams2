package cz.uhk.dip.mmsparams.server.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;

public class ServerLogFatalFacade
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ServerLogFatalFacade.class);

    public static void logFatal(final Logger logger, Exception e)
    {
        logger.error("", e);
        LOGGER.error("", e);
    }

    public static void logFatal(final Logger logger, String tag, String message, Exception ex)
    {
        logger.error(getMessageWithStack(tag, message), ex);
        LOGGER.error(getMessageWithStack(tag, message), ex);
    }

    private static String getMessageWithStack(String tag, String message)
    {
        return tag + ": " + message + "\n" + ExceptionHelper.getStackTrace(new Throwable());
    }
}
