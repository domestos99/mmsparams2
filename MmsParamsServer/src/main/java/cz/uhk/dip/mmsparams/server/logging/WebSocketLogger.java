package cz.uhk.dip.mmsparams.server.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

@Component
public class WebSocketLogger
{
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketLogger.class);

    public void logConnectionEstablished(WebSocketSession session)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("ConnectionEstablished");
        getSessionLogInfo(sb, session);
        addEnd(sb);
        ServerLogFacade.logWebSocket(LOGGER, sb.toString());
    }

    public void logTextMessage(WebSocketSession session, TextMessage message)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("TextMessage");
        getSessionLogInfo(sb, session);

        sb.append(message.getPayload());
        addEnd(sb);
        ServerLogFacade.logWebSocket(LOGGER, sb.toString());
    }

    public void logTransportError(WebSocketSession session, Throwable exception)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("TransportError");
        getSessionLogInfo(sb, session);
        sb.append(exception.getMessage());
        sb.append("\n");
        sb.append(exception.toString());
        addEnd(sb);
        ServerLogFacade.logWebSocket(LOGGER, sb.toString());
    }

    public void logConnectionClosed(WebSocketSession session)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("ConnectionClosed");
        getSessionLogInfo(sb, session);
        addEnd(sb);
        ServerLogFacade.logWebSocket(LOGGER, sb.toString());
    }


    public void logUnknownMessage(WebSocketSession session, TextMessage message)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("UnknownMessage");
        getSessionLogInfo(sb, session);
        sb.append(message.getPayload());
        addEnd(sb);
        ServerLogFacade.logWebSocket(LOGGER, sb.toString());
    }

    public void logSendMessage(WebSocketSession session, WebSocketMessageBase message)
    {
        logSendMessage(session, new TextMessage(JsonUtilsSafe.toJson(message)));
    }

    public void logSendMessage(WebSocketSession session, String message)
    {
        logSendMessage(session, new TextMessage(message));
    }

    public void logSendMessage(final WebSocketSession session, final TextMessage message)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("SendMessage");
        getSessionLogInfo(sb, session);

        sb.append(message.getPayload());
        addEnd(sb);
        ServerLogFacade.logWebSocket(LOGGER, sb.toString());
    }


    private StringBuilder getSessionLogInfo(final StringBuilder sb, final WebSocketSession session)
    {
        sb.append("\n");
        sb.append("Session ID: " + session.getId());
        sb.append("\n");
        sb.append("RemoteAddress: " + session.getRemoteAddress());
        sb.append("\n");
        return sb;
    }


    private StringBuilder addEnd(final StringBuilder sb)
    {
        sb.append("\n");
        sb.append("--------------------------------------------------------------------------------");
        return sb.append("\n");
    }


}
