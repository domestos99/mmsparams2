package cz.uhk.dip.mmsparams.server.mmsc;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;

public interface IMmscOnReceiveListener
{
    void onDeliveryReqReceived(final MM7DeliveryReqModel model);

    void onDeliveryReportReqReceived(final MM7DeliveryReportReqModel model);

    void onReadReply(final MM7ReadReplyReqModel mm7ReadReplyReqModel);

    void onError(final MM7ErrorModel model);
}
