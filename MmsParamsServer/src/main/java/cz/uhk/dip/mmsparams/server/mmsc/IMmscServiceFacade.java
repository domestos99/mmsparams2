package cz.uhk.dip.mmsparams.server.mmsc;

import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;

public interface IMmscServiceFacade
{
    MM7SubmitResponseModel send(final MmscSendMessage msg) throws Exception;

    void onDeliveryReqReceived(final MM7DeliveryReqModel model);

    void onDeliveryReportReqReceived(final MM7DeliveryReportReqModel model);

    void onReadReply(final MM7ReadReplyReqModel mm7ReadReplyReqModel);

    void onError(final MM7ErrorModel model);

    void setOnReceiveListener(final IMmscOnReceiveListener mmscOnReceiveListener);
}
