package cz.uhk.dip.mmsparams.server.mmsc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReportReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7DeliveryReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7ReadReplyReqMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IMmscWSController;

@Service
public class MmscOnReceiveListener implements IMmscOnReceiveListener
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MmscOnReceiveListener.class);
    private static final String TAG = MmscOnReceiveListener.class.getSimpleName();
    private final IMmscWSController sessionManager;

    public MmscOnReceiveListener(final IMmscWSController sessionManager)
    {
        this.sessionManager = sessionManager;
    }

    @Override
    public void onDeliveryReqReceived(final MM7DeliveryReqModel model)
    {
        Preconditions.checkNotNull(model, "MM7DeliveryReqModel");
        ServerLogFacade.logInfo(LOGGER, TAG, "onDeliveryReqReceived");

        MM7DeliveryReqMessage msg =
                new MessageBuilder()
                        .withMessageIdRandom()
                        .withSenderKeyServer()
                        .withRecipientKeyClientBroadcast()
                        .build(MM7DeliveryReqMessage.class);

        msg.setMm7DeliveryReqModel(model);
        sessionManager.processMmscBroadcastToClientFromServer(msg, model.getRecipients());
    }

    @Override
    public void onDeliveryReportReqReceived(final MM7DeliveryReportReqModel model)
    {
        Preconditions.checkNotNull(model, "MM7DeliveryReportReqModel");
        ServerLogFacade.logInfo(LOGGER, TAG, "onDeliveryReportReqReceived");

        MM7DeliveryReportReqMessage msg =
                new MessageBuilder()
                        .withSenderKeyServer()
                        .withRecipientKeyClientBroadcast()
                        .withMessageIdRandom()
                        .build(MM7DeliveryReportReqMessage.class);

        msg.setMm7DeliveryReportReqModel(model);
        sessionManager.processMmscBroadcastToClientFromServer(msg, model.getRecipient());
    }

    @Override
    public void onReadReply(final MM7ReadReplyReqModel model)
    {
        Preconditions.checkNotNull(model, "MM7ReadReplyReqModel");
        ServerLogFacade.logInfo(LOGGER, TAG, "onReadReply");
//        mm7ReadReplyReqModel.getRecipient()

        MM7ReadReplyReqMessage msg =
                new MessageBuilder()
                        .withSenderKeyServer()
                        .withRecipientKeyClientBroadcast()
                        .withMessageIdRandom()
                        .build(MM7ReadReplyReqMessage.class);

        msg.setMm7ReadReplyReqModel(model);
        sessionManager.processMmscBroadcastToClientFromServer(msg, model.getRecipient());
    }

    @Override
    public void onError(final MM7ErrorModel model)
    {
        ServerLogFacade.logInfo(LOGGER, TAG, "onError");
        MM7ErrorMessage msg =
                new MessageBuilder()
                        .withSenderKeyServer()
                        .withRecipientKeyClientBroadcast()
                        .withMessageIdRandom()
                        .build(MM7ErrorMessage.class);

        msg.setMm7ErrorModel(model);


        // TODO
        // sessionManager.processMmscBroadcastClientFromServer(msg, model.getRecipient());


    }
}
