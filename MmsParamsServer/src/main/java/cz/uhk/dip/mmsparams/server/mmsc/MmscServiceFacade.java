package cz.uhk.dip.mmsparams.server.mmsc;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;
import cz.uhk.dip.mmsparams.mmsc.IMmscSender;
import cz.uhk.dip.mmsparams.mmsc.MmscSender;

@Service
public class MmscServiceFacade implements IMmscServiceFacade
{
    private final IMmscSender iMmscSender;
    private IMmscOnReceiveListener mmscOnReceiveListener;


    @Autowired
    public MmscServiceFacade()
    {
        this.iMmscSender = new MmscSender();
    }

    @Override
    public MM7SubmitResponseModel send(final MmscSendMessage msg) throws Exception
    {
        return iMmscSender.send(msg.getMmscSendModel());
    }

    @Override
    public void setOnReceiveListener(IMmscOnReceiveListener mmscOnReceiveListener)
    {
        this.mmscOnReceiveListener = mmscOnReceiveListener;
    }

    @Override
    public void onDeliveryReqReceived(final MM7DeliveryReqModel model)
    {
        mmscOnReceiveListener.onDeliveryReqReceived(model);
    }

    @Override
    public void onDeliveryReportReqReceived(final MM7DeliveryReportReqModel model)
    {
        mmscOnReceiveListener.onDeliveryReportReqReceived(model);
    }

    @Override
    public void onReadReply(final MM7ReadReplyReqModel mm7ReadReplyReqModel)
    {
        mmscOnReceiveListener.onReadReply(mm7ReadReplyReqModel);
    }

    @Override
    public void onError(final MM7ErrorModel model)
    {
        mmscOnReceiveListener.onError(model);
    }


}
