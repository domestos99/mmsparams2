package cz.uhk.dip.mmsparams.server.mmsc;

import net.instantcom.mm7.DeliverReq;
import net.instantcom.mm7.DeliverRsp;
import net.instantcom.mm7.DeliveryReportReq;
import net.instantcom.mm7.DeliveryReportRsp;
import net.instantcom.mm7.MM7Context;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MM7Response;
import net.instantcom.mm7.ReadReplyReq;
import net.instantcom.mm7.ReadReplyRsp;
import net.instantcom.mm7.VASP;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;
import cz.uhk.dip.mmsparams.mmsc.utils.MM7Converter;
import cz.uhk.dip.mmsparams.mmsc.utils.MM7ConverterIn;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;

@Service
public class VaspService implements VASP
{
    private static final Logger LOGGER = LoggerFactory.getLogger(VaspService.class);
    private static final String TAG = VaspService.class.getSimpleName();

    private final IMmscServiceFacade iMmscServiceFacade;

    // @Autowired
    private MM7Context context;

    @Autowired
    public VaspService(IMmscServiceFacade iMmscServiceFacade, IMmscOnReceiveListener iMmscOnReceiveListener)
    {
        this.iMmscServiceFacade = iMmscServiceFacade;
        this.iMmscServiceFacade.setOnReceiveListener(iMmscOnReceiveListener);
        context = new MM7Context();
    }

    @Override
    public DeliverRsp deliver(final DeliverReq deliverReq) throws MM7Error
    {
        ServerLogFacade.logInfo(LOGGER, "deliver in VASP was called");

        try
        {
            MM7DeliveryReqModel model = MM7ConverterIn.getDeliveryReqModel(deliverReq);
            iMmscServiceFacade.onDeliveryReqReceived(model);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "deliver", e);
        }
        DeliverRsp deliverRsp;

        if (deliverReq != null)
        {
            deliverRsp = deliverReq.reply();
        }
        else
        {
            deliverRsp = new DeliverRsp();
            deliverRsp.setStatusCode(MM7Response.SC_SUCCESS);
            deliverRsp.setStatusText(deliverRsp.getStatusText());
        }

        return deliverRsp;
    }

    @Override
    public MM7Context getContext()
    {
        return context;
    }

    @Override
    public void handleMm7Error(final MM7Error mm7error)
    {
        MM7ErrorModel model = MM7Converter.getError(mm7error);

        iMmscServiceFacade.onError(model);
    }

    @Override
    public DeliveryReportRsp deliveryReport(final DeliveryReportReq deliveryReportReq) throws MM7Error
    {
        ServerLogFacade.logInfo(LOGGER, "deliveryReport in VASP was called");

        try
        {
            MM7DeliveryReportReqModel model = MM7ConverterIn.getDeliveryReportReqModel(deliveryReportReq);
            iMmscServiceFacade.onDeliveryReportReqReceived(model);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "deliveryReport", e);
        }

        DeliveryReportRsp deliveryReportRsp;
        if (deliveryReportReq != null)
        {
            deliveryReportRsp = deliveryReportReq.reply();
        }
        else
        {
            deliveryReportRsp = new DeliveryReportRsp();
            deliveryReportRsp.setStatusCode(MM7Response.SC_SUCCESS);
            deliveryReportRsp.setStatusText(deliveryReportRsp.getStatusText());
        }

        return deliveryReportRsp;
    }

    @Override
    public ReadReplyRsp readReply(final ReadReplyReq readReplyReq)
    {
        ServerLogFacade.logInfo(LOGGER, "readReplyReq in VASP was called");

        try
        {
            MM7ReadReplyReqModel mm7ReadReplyReqModel = MM7ConverterIn.getReadReplyReq(readReplyReq);
            iMmscServiceFacade.onReadReply(mm7ReadReplyReqModel);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "readReply", e);
        }

        ReadReplyRsp readReplyRsp;
        if (readReplyReq != null)
        {
            readReplyRsp = readReplyReq.reply();
        }
        else
        {
            readReplyRsp = new ReadReplyRsp();
            readReplyRsp.setStatusCode(MM7Response.SC_SUCCESS);
            readReplyRsp.setStatusText(readReplyRsp.getStatusText());
        }

        return readReplyRsp;
    }
}
