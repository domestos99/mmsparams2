package cz.uhk.dip.mmsparams.server.smsc;

public interface ISmscMessageListener
{
    void onMessageReceived();

    void onDeliveryReportReceived();

}
