package cz.uhk.dip.mmsparams.server.smsc;

import com.cloudhopper.smpp.SmppSession;

import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.smsc.ISmscReceiveListener;

public interface ISmscServiceFacade
{
    SmppSession connect(final SmscConnectMessage msg, ISmscReceiveListener iSmscReceiveListener) throws Exception;

    SmscSendSmsResponseModel send(final SmppSession smppSession, final SmscSendSmsMessage msg) throws Exception;

    void disconnect(final SmppSession smppSession);
}
