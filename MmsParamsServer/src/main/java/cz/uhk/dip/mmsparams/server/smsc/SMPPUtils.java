//package cz.uhk.dip.mmsparams.server.SMPP;
//
//import com.cloudhopper.smpp.SmppSessionConfiguration;
//
//import org.jsmpp.InvalidResponseException;
//import org.jsmpp.PDUException;
//import org.jsmpp.bean.BindType;
//import org.jsmpp.bean.NumberingPlanIndicator;
//import org.jsmpp.bean.TypeOfNumber;
//import org.jsmpp.extra.NegativeResponseException;
//import org.jsmpp.extra.ResponseTimeoutException;
//import org.jsmpp.session.BindParameter;
//import org.jsmpp.session.SMPPSession;
//
//import java.io.IOException;
//
//import cz.uhk.dip.mmsparams.SMPP.SmppSubmit;
//
//public class SMPPUtils
//{
//    public static String connect(final SMPPSession session, final SmppSessionConfiguration defaultSmppSessionHandler) throws IOException
//    {
//        // TODO
//        String systemId = session.connectAndBind(
//
//                defaultSmppSessionHandler.getHost(),
//                defaultSmppSessionHandler.getPort(),
//
//                new BindParameter(
//                        // TODO
//                        BindType.BIND_TRX,
//                        defaultSmppSessionHandler.getSystemId(),
//                        defaultSmppSessionHandler.getPassword(),
//                        defaultSmppSessionHandler.getSystemType(),
//
//                        TypeOfNumber.UNKNOWN,
//                        NumberingPlanIndicator.UNKNOWN,
//                        null));
//
//
//        return systemId;
//
//    }
//
//    public static String submitShortMessage(final SMPPSession session, final SmppSubmit smppSubmit) throws PDUException, ResponseTimeoutException, InvalidResponseException, NegativeResponseException, IOException
//    {
//        String messageId = session.submitShortMessage("CMT",
//
//                smppSubmit.getSourceAddress().getTypeOfNumber(),
//                smppSubmit.getSourceAddress().getNumberingPlanIndicator(),
//                smppSubmit.getSourceAddress().getAddress(),
//
//                smppSubmit.getDestAddress().getTypeOfNumber(),
//                smppSubmit.getDestAddress().getNumberingPlanIndicator(),
//                smppSubmit.getDestAddress().getAddress(),
//
//                smppSubmit.getESMClass(),
//
//                smppSubmit.getProtocolId(),
//                smppSubmit.getPriority(),
//                smppSubmit.getScheduleDeliveryTime(),
//
//                smppSubmit.getValidityPeriod(),
//
//                smppSubmit.getRegisteredDelivery(),
//
//                smppSubmit.getReplaceIfPresent(),
//
//                smppSubmit.getDataCoding(),
//                smppSubmit.getDefaultMsgId(),
//
//                smppSubmit.getShortMessageBytes()
//        );
//
//        return messageId;
//    }
//
//
//}
