//package cz.uhk.dip.mmsparams.server.SMPP;
//
//import org.jsmpp.bean.AlertNotification;
//import org.jsmpp.bean.DataSm;
//import org.jsmpp.bean.DeliverSm;
//import org.jsmpp.bean.DeliveryReceipt;
//import org.jsmpp.bean.MessageType;
//import org.jsmpp.extra.ProcessRequestException;
//import org.jsmpp.session.DataSmResult;
//import org.jsmpp.session.MessageReceiverListener;
//import org.jsmpp.session.Session;
//import org.jsmpp.util.InvalidDeliveryReceiptException;
//
//public class SmppMessageReceiverListener implements MessageReceiverListener
//{
//    // TODO - add setting or sth
//
//    private ISmppMessageListener iSmppMessageListener;
//
//    public SmppMessageReceiverListener(ISmppMessageListener iSmppMessageListener)
//    {
//        this.iSmppMessageListener = iSmppMessageListener;
//    }
//
//    public void onAcceptDeliverSm(DeliverSm deliverSm) throws ProcessRequestException
//    {
//        if (MessageType.SMSC_DEL_RECEIPT.containedIn(deliverSm.getEsmClass()))
//        {
//            // delivery receipt
//            try
//            {
//                DeliveryReceipt delReceipt = deliverSm.getShortMessageAsDeliveryReceipt();
//                long id = Long.parseLong(delReceipt.getId()) & 0xffffffff;
//                String messageId = Long.toString(id, 16).toUpperCase();
//                //UTLogger.info("received '{}' : {}", messageId, delReceipt);
//            }
//            catch (InvalidDeliveryReceiptException e)
//            {
//                ServerLogFacade.logEx(LOGGER,e);
//                //UTLogger.error("receive failed, e");
//            }
//        }
//        else
//        {
//            // regular short message
//            // UTLogger.info("Receiving message : {}", new String(deliverSm.getShortMessage()));
//        }
//    }
//
//    public void onAcceptAlertNotification(AlertNotification alertNotification)
//    {
//        // UTLogger.info("onAcceptAlertNotification");
//    }
//
//    @Override
//    public DataSmResult onAcceptDataSm(DataSm dataSm, Session source) throws ProcessRequestException
//    {
//        // UTLogger.info("onAcceptDataSm");
//        return null;
//    }
//}
