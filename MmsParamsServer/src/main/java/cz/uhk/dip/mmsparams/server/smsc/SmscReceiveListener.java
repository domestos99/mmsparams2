package cz.uhk.dip.mmsparams.server.smsc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.MessageIdGenerator;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketConstants;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISmscWSController;
import cz.uhk.dip.mmsparams.smsc.ISmscReceiveListener;

public class SmscReceiveListener implements ISmscReceiveListener
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SmscReceiveListener.class);
    private final ISmscWSController iClientLibBroadcast;
    private final String testID;
    private final SmscSessionId smppSessionId;

    public SmscReceiveListener(ISmscWSController iClientLibBroadcast, String testID, SmscSessionId smppSessionId)
    {
        this.iClientLibBroadcast = iClientLibBroadcast;
        this.testID = testID;
        this.smppSessionId = smppSessionId;
    }

    @Override
    public void onDeliverySm(SmscDeliverSmModel deliverSmModel)
    {
        SmscDeliverSmMessage msg = new MessageBuilder()
                .withSenderKeyServer()
                .withRecipientKeyClientBroadcast()
                .withMessageIdRandom()
                .build(SmscDeliverSmMessage.class);

        msg.setSmscDeliverSmModel(deliverSmModel);
        msg.setSmscSessionId(smppSessionId);
        msg.setTestID(testID);

        try
        {
            iClientLibBroadcast.processSmscBroadcastToClientFromServer(msg, testID);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, e);
        }
    }

    @Override
    public void onDeliveryReport(SmscDeliveryReportModel deliveryReportModel)
    {
        SmscDeliveryReportMessage msg = MessageFactory.createServerSender(null, SmscDeliveryReportMessage.class);

        msg.setSmscDeliveryReportModel(deliveryReportModel);
        msg.setSmscSessionId(smppSessionId);
        msg.setRecipientKey(WebSocketConstants.Server_To_Client_Broadcast);
        msg.setMessageID(MessageIdGenerator.getNext());
        msg.setTestID(testID);

        try
        {
            iClientLibBroadcast.processSmscBroadcastToClientFromServer(msg, testID);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, e);
        }
    }
}
