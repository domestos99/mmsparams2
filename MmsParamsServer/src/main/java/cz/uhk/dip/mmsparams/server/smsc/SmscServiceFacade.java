package cz.uhk.dip.mmsparams.server.smsc;

import com.cloudhopper.smpp.SmppClient;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.impl.DefaultSmppClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.smsc.ISmppClientProvider;
import cz.uhk.dip.mmsparams.smsc.ISmscReceiveListener;
import cz.uhk.dip.mmsparams.smsc.ISmscService;
import cz.uhk.dip.mmsparams.smsc.SmscService;

@Service
public class SmscServiceFacade implements ISmscServiceFacade
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SmscServiceFacade.class);
    private final ISmscService smppService;

    @Autowired
    public SmscServiceFacade()
    {
        this.smppService = new SmscService(new ISmppClientProvider()
        {
            @Override
            public SmppClient create()
            {
                return new DefaultSmppClient();
            }
        });
    }

    @Override
    public SmppSession connect(final SmscConnectMessage msg, final ISmscReceiveListener smppReceiveListener) throws Exception
    {
        try
        {
            return smppService.connect(msg.getSmscSendSmsModel(), smppReceiveListener);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, e);
            throw e;
        }
    }

    @Override
    public SmscSendSmsResponseModel send(final SmppSession smppSession, SmscSendSmsMessage msg) throws Exception
    {
        try
        {
            return smppService.sendSingleSMS(smppSession, msg.getSmscSendModel());
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, e);
            throw e;
        }
    }

    @Override
    public void disconnect(final SmppSession smppSession)
    {
        smppService.disconnect(smppSession);
    }


}
