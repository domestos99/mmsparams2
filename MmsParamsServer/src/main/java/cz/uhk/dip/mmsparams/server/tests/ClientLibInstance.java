package cz.uhk.dip.mmsparams.server.tests;

import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;

public class ClientLibInstance
{
    private final WebSocketSession clientSession;
    private final ClientInfo clientInfo;

    public ClientLibInstance(WebSocketSession clientSession, ClientInfo clientInfo)
    {
        this.clientSession = clientSession;
        this.clientInfo = clientInfo;
    }

    public WebSocketSession getClientSession()
    {
        return clientSession;
    }

    public ClientInfo getClientInfo()
    {
        return clientInfo;
    }
}
