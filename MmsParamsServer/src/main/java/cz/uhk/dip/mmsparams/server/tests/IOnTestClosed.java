package cz.uhk.dip.mmsparams.server.tests;

public interface IOnTestClosed
{
    void onClosed(ServerTestInstance serverTestInstance);
}
