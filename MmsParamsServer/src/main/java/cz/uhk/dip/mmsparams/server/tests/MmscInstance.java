package cz.uhk.dip.mmsparams.server.tests;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MmscAcquireRouteModel;

public class MmscInstance
{
    private final MmscAcquireRouteModel acquireRouteModel;

    public MmscInstance(final MmscAcquireRouteModel acquireRouteModel)
    {
        this.acquireRouteModel = acquireRouteModel;
    }

    public MmscAcquireRouteModel getAcquireRouteModel()
    {
        return acquireRouteModel;
    }
}
