package cz.uhk.dip.mmsparams.server.tests;

import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;

public class PhoneInstance
{
    private final WebSocketSession phoneSession;
    private final PhoneInfoModel phoneInfoModel;


    public PhoneInstance(WebSocketSession phoneSession, PhoneInfoModel phoneInfoModel)
    {
        this.phoneSession = phoneSession;
        this.phoneInfoModel = phoneInfoModel;
    }

    public WebSocketSession getPhoneSession()
    {
        return phoneSession;
    }

    public PhoneInfoModel getPhoneInfoModel()
    {
        return phoneInfoModel;
    }
}
