package cz.uhk.dip.mmsparams.server.tests;

import com.cloudhopper.smpp.SmppSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;

import javax.annotation.Nullable;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.constants.YesNoNaN;
import cz.uhk.dip.mmsparams.api.errors.TestErrorBuilder;
import cz.uhk.dip.mmsparams.api.lists.HistorizedList;
import cz.uhk.dip.mmsparams.api.lists.HistorizedObject;
import cz.uhk.dip.mmsparams.api.messages.IMessageRegister;
import cz.uhk.dip.mmsparams.api.messages.MessageRegister;
import cz.uhk.dip.mmsparams.api.messages.MessageRegisterItem;
import cz.uhk.dip.mmsparams.api.utils.LogUtil;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.TestGroupModel;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MmscAcquireRouteModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.registration.TestClientInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.server.database.dto.ClientLibInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.MmscInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.PhoneInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.SmscInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.TestErrorDTO;
import cz.uhk.dip.mmsparams.server.database.dto.TestGroupModelDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ValidationResultDTO;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.utils.StreamUtils;
import cz.uhk.dip.mmsparams.server.validation.ClientLibInstanceValidator;


public class ServerTestInstance
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ServerTestInstance.class);
    static final String TAG = ServerTestInstance.class.getSimpleName();

    private final String testID;
    private String testName;
    private String testDesc;
    private final long createdDT;
    private boolean closed;

    // Info about client
    private final HistorizedObject<TestGroupModel, TestGroupModelDTO> testGroupModel;
    private final HistorizedObject<ClientLibInstance, ClientLibInstanceDTO> clientLibInstance;
    private final HistorizedList<PhoneInstance, PhoneInstanceDTO> phoneInstanceList;
    private final HistorizedList<SmscInstance, SmscInstanceDTO> smscInstances;
    private final HistorizedList<Tuple<Boolean, ValidationResult>, ValidationResultDTO> validationResults;
    private final HistorizedList<TestErrorModel, TestErrorDTO> testErrors;
    private HistorizedObject<MmscInstance, MmscInstanceDTO> mmscInstance;

    private final IMessageRegister messageRegister;
    private IOnTestClosed iOnTestClosed;

    private ServerTestInstance(final String testId, final TestGroupModel testGroupModel, final TestClientInfoModel testInfo, final ClientLibInstance clientLibInstance, final IOnTestClosed iOnTestClosed)
    {
        Preconditions.checkNotNullOrEmpty(testId, GenericConstants.TEST_ID);
        ClientLibInstanceValidator.validate(clientLibInstance);

        ServerLogFacade.logInfo(LOGGER, TAG, LogUtil.HASHES + LogUtil.NEW_LINE + "Starting new test with testID: " + testId);

        this.testID = testId;
        this.closed = false;
        this.messageRegister = new MessageRegister();
        this.phoneInstanceList = new HistorizedList<>(obj -> {
            return PhoneInstanceDTO.create(obj);
        });
        this.smscInstances = new HistorizedList<>(obj -> {
            return SmscInstanceDTO.create(obj);
        });
        validationResults = new HistorizedList<>(obj -> {
            return ValidationResultDTO.create(obj.getY());
        });
        this.testErrors = new HistorizedList<>(obj -> {
            return TestErrorDTO.create(obj);
        });
        this.createdDT = System.currentTimeMillis();
        this.iOnTestClosed = iOnTestClosed;
        this.clientLibInstance = new HistorizedObject<>(clientLibInstance, obj -> {
            return ClientLibInstanceDTO.create(obj);
        });
        this.testGroupModel = new HistorizedObject<>(testGroupModel, obj -> {
            return TestGroupModelDTO.create(obj);
        });

        if (testInfo != null)
        {
            this.testName = testInfo.getTestName();
            this.testDesc = testInfo.getTestDesc();
        }
    }

    public static ServerTestInstance createNewTest(final String testId, final TestGroupModel testGroupModel, final WebSocketSession clientSession, final TestClientInfoModel testInfo,
                                                   final ClientInfo clientInfo, final IOnTestClosed iOnTestClosed)
    {
        return new ServerTestInstance(testId, testGroupModel, testInfo, new ClientLibInstance(clientSession, clientInfo), iOnTestClosed);
    }

    public boolean insertIncomingMsg(final WebSocketMessageBase messageBase)
    {
        Preconditions.checkNotNull(messageBase, GenericConstants.MESSAGE);
        if (isClosed())
            return false;
        this.messageRegister.insertIncomingMsg(messageBase);
        return true;
    }

    public void insertOutgoingMsg(final WebSocketMessageBase messageBase)
    {
        if (isClosed())
            return;
        this.messageRegister.insertOutgoingMsg(messageBase);
    }

    public String getTestID()
    {
        return testID;
    }

    public TestGroupModel getTestGroupModel()
    {
        return testGroupModel.getObject();
    }

    public TestGroupModelDTO getTestGroupModelHist()
    {
        return testGroupModel.getHistorizedObject();
    }

    public void closeTest()
    {
        if (isClosed())
            return;

        this.closed = true;
        if (this.iOnTestClosed != null)
            this.iOnTestClosed.onClosed(this);
    }

    public boolean isClosed()
    {
        return closed;
    }

    public long getCreatedDT()
    {
        return createdDT;
    }

    public List<Tuple<Boolean, ValidationResult>> getValidationResults()
    {
        return this.validationResults.getAll();
    }

    public void removeClientSessionAndCloseTest(boolean wasClosedOK)
    {
        if (!wasClosedOK)
        {
            TestErrorModel errorModel = TestErrorBuilder.create()
                    .withMessage("Test unexpectedly closed")
                    .withErrorType(TestErrorType.GENERIC_ERROR)
                    .build();
            this.insertTestError(errorModel);
        }

        this.phoneInstanceList.clear();
        closeAllSmscSessions();
        this.smscInstances.clear();
        this.testErrors.clear();
        if (this.mmscInstance != null)
            this.mmscInstance.clear();

        closeTest();
    }

    @Nullable
    public WebSocketSession getClientSession()
    {
        return this.clientLibInstance.getObject().getClientSession();
    }

    public String getClientSessionId()
    {
        WebSocketSession ses = getClientSession();
        if (ses == null)
        {
            return "-1";
        }
        return ses.getId();
    }

    public ClientInfo getClientInfo()
    {
        return clientLibInstance.getObject().getClientInfo();
    }

    public boolean containsSession(final WebSocketSession session)
    {
        String sessionId = session.getId();

        if (sessionId.equals(getClientSessionId()))
            return true;

        for (PhoneInstance pi : phoneInstanceList)
        {
            if (sessionId.equals(pi.getPhoneSession().getId()))
                return true;
        }

        return false;
    }

    public ClientLibInstance getClientLibInstance()
    {
        return this.clientLibInstance.getObject();
    }

    public void lockPhoneForTest(final WebSocketSession session, final PhoneInfoModel phoneInfoModel)
    {
        if (isClosed())
            return;
        if (!containsLockedPhone(phoneInfoModel.getPhoneKey()))
        {
            PhoneInstance pi = new PhoneInstance(session, phoneInfoModel);
            phoneInstanceList.add(pi);
        }
    }

    public void unLockPhoneForTest(final PhoneInfoModel phoneInfoModel)
    {
        if (isClosed())
            return;
        final String phoneKey = phoneInfoModel.getPhoneKey();
        for (int i = phoneInstanceList.size() - 1; i >= 0; i--)
        {
            if (phoneKey.equals(phoneInstanceList.get(i).getPhoneInfoModel().getPhoneKey()))
            {
                phoneInstanceList.remove(i);
                // TODO break ???
            }
        }
    }

    public boolean containsLockedPhone(final String phoneKey)
    {
        if (isClosed())
            return false;

        for (PhoneInstance pi : phoneInstanceList)
        {
            if (pi.getPhoneInfoModel().getPhoneKey().equals(phoneKey))
                return true;
        }
        return false;
    }

    public boolean removeSessionIfExists(String sessionId)
    {
        if (sessionId.equals(getClientSessionId()))
        {
            removeClientSessionAndCloseTest(false);
            return true;
            // TODO return?
        }

        for (int i = phoneInstanceList.size() - 1; i >= 0; i--)
        {
            if (sessionId.equals(phoneInstanceList.get(i).getPhoneSession().getId()))
            {
                phoneInstanceList.remove(i);
            }
        }
        return false;
    }

    public List<PhoneInstance> getPhoneInstanceList()
    {
        return phoneInstanceList.getAll();
    }

    public String getClientLibKey()
    {
        return getClientInfo().getClientKey();
    }

    @Nullable
    public WebSocketSession getSessionByRecipientKey(final String recipientKey)
    {
        if (recipientKey.equals(getClientLibKey()))
        {
            return getClientSession();
        }

        for (PhoneInstance pi : phoneInstanceList)
        {
            if (recipientKey.equals(pi.getPhoneInfoModel().getPhoneKey()))
                return pi.getPhoneSession();
        }

        return null;
    }

    public List<MessageRegisterItem> getIncomingMessages()
    {
        return messageRegister.getAllIncoming();
    }

    public IMessageRegister getMessageRegister()
    {
        return messageRegister;
    }

    private void closeAllSmscSessions()
    {
        for (SmscInstance smscInstance : smscInstances)
        {
            ServerLogFacade.logInfo(LOGGER, TAG, "Closing SMPP session: " + SmscInstanceDTO.create(smscInstance).toString());
            smscInstance.close();
        }
    }

    @Nullable
    public SmscSessionId addNewSmscConnection(SmscSessionId smscSessionId, SmppSession smscSession)
    {
        if (isClosed())
            return null;
        SmscInstance si = new SmscInstance(smscSessionId, smscSession);
        smscInstances.add(si);
        return smscSessionId;
    }

    @Nullable
    public SmppSession getSmscSession(SmscSessionId smscSessionId)
    {
        for (SmscInstance si : smscInstances)
        {
            if (smscSessionId.getSmscSessionId().equals(si.getSmscSessionId().getSmscSessionId()))
                return si.getSmscSession();
        }
        return null;
    }

    public void removeSmscSession(SmscSessionId smscSessionId)
    {
        for (int i = smscInstances.size() - 1; i >= 0; i--)
        {
            if (smscSessionId.equals(smscInstances.get(0).getSmscSessionId()))
            {
                smscInstances.remove(i);
            }
        }
    }

    public int getSmscSessionCount()
    {
        return smscInstances.size();
    }

    public void addNewMmscAcquirePort(MmscAcquireRouteModel acquireRouteModel)
    {
        if (isClosed())
            return;
        this.mmscInstance = new HistorizedObject<>(new MmscInstance(acquireRouteModel), obj -> {
            return MmscInstanceDTO.create(obj);
        });
    }

    @Nullable
    public MmscAcquireRouteModel getMmscAcquireRouteModel()
    {
        if (this.mmscInstance == null)
            return null;
        if (this.mmscInstance.getObject() == null)
            return null;
        return mmscInstance.getObject().getAcquireRouteModel();
    }

    public void addValidationResults(List<Tuple<Boolean, ValidationResult>> validationResult)
    {
        if (isClosed())
            return;
        this.validationResults.addAll(validationResult);
    }

    public void insertTestError(TestErrorModel testError)
    {
        if (isClosed())
            return;
        this.testErrors.add(testError);
    }

    public ClientLibInstanceDTO getClientLibInstanceHist()
    {
        return this.clientLibInstance.getHistorizedObject();
    }

    public List<PhoneInstanceDTO> getPhoneInstanceListHist()
    {
        return phoneInstanceList.getHistorizedAll();
    }

    public List<SmscInstanceDTO> getSmscInstancesHist()
    {
        return smscInstances.getHistorizedAll();
    }

    @Nullable
    public MmscInstanceDTO getMmscInstanceHist()
    {
        if (this.mmscInstance == null)
            return null;
        return this.mmscInstance.getHistorizedObject();
    }

    public List<TestErrorModel> getTestErrors()
    {
        return testErrors.getAll();
    }

    public String getTestName()
    {
        return testName;
    }

    public String getTestDesc()
    {
        return testDesc;
    }

    public List<TestErrorDTO> getTestErrorsHist()
    {
        return testErrors.getHistorizedAll();
    }

    @Nullable
    public MessageRegisterItem findMessage(String messageId, long createdDT)
    {
        List<MessageRegisterItem> inFiltered = StreamUtils.toList(this.getMessageRegister().getAllIncoming().stream()
                .filter(x -> StringUtil.areEqual(x.getMessage().getMessageID(), messageId) && x.getMessage().getCreatedDT() == createdDT));

        if (inFiltered.size() == 1)
            return inFiltered.get(0);


        List<MessageRegisterItem> outFiltered = StreamUtils.toList(this.getMessageRegister().getAllOutgoing().stream()
                .filter(x -> StringUtil.areEqual(x.getMessage().getMessageID(), messageId) && x.getMessage().getCreatedDT() == createdDT));

        if (outFiltered.size() == 1)
            return outFiltered.get(0);

        return null;
    }

    public boolean containsRecipient(String recipientKey)
    {
        if (isClosed())
            return false;

        if (getClientLibKey().equals(recipientKey))
            return true;

        return this.containsLockedPhone(recipientKey);
    }

    public List<ValidationResultDTO> getValidationResultsHist()
    {
        return this.validationResults.getHistorizedAll();
    }

    public boolean hasErrors()
    {
        return !this.getTestErrorsHist().isEmpty();
    }

    public YesNoNaN hasValidationErrors()
    {
        if (getValidationResultsHist().isEmpty())
            return YesNoNaN.NAN;

        for (ValidationResultDTO val : getValidationResultsHist())
        {
            if (val != null && !val.getResult())
                return YesNoNaN.YES;
        }
        return YesNoNaN.NO;
    }


    public boolean processTestResult(TestResultMessage msg)
    {
        removeClientSessionAndCloseTest(true);
        return true;
    }


}
