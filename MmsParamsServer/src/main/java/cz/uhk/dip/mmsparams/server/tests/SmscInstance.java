package cz.uhk.dip.mmsparams.server.tests;

import com.cloudhopper.smpp.SmppSession;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;

public class SmscInstance
{
    private final SmscSessionId smscSessionId;
    private final SmppSession smppSession;

    public SmscInstance(final SmscSessionId smscSessionId, final SmppSession smscSession)
    {
        this.smppSession = smscSession;
        this.smscSessionId = smscSessionId;
    }


    public SmppSession getSmscSession()
    {
        return smppSession;
    }

    public SmscSessionId getSmscSessionId()
    {
        return smscSessionId;
    }

    public void close()
    {
        smppSession.unbind(0);
    }
}
