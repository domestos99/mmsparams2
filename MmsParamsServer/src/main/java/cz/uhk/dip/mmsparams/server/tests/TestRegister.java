package cz.uhk.dip.mmsparams.server.tests;

import com.cloudhopper.smpp.SmppSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.exceptions.TestErrorException;
import cz.uhk.dip.mmsparams.api.http.TaskResult;
import cz.uhk.dip.mmsparams.api.messages.MessageRegisterItem;
import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.api.utils.MmscAcquireRouteUtil;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.registration.TestClientInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.server.configs.AppConfig;
import cz.uhk.dip.mmsparams.server.database.services.IServerTestInstanceDBService;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.utils.PreconditionsSafe;

@Service
public class TestRegister
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TestRegister.class);
    private static final String TAG = TestRegister.class.getSimpleName();

    private final ConcurrentHashMap<String, ServerTestInstance> tests = new ConcurrentHashMap<>();

    private final IServerTestInstanceDBService serverTestInstanceDBService;

    @Autowired
    public TestRegister(IServerTestInstanceDBService serverTestInstanceDBService)
    {
        this.serverTestInstanceDBService = serverTestInstanceDBService;
    }

    public synchronized boolean addTestMessage(WebSocketSession session, WebSocketMessageBase message)
    {
        if (PreconditionsSafe.checkIsNull(message, WebSocketMessageBase.class.getSimpleName()))
            return false;

        String testId = message.getTestID();

        if (!containsTest(testId))
        {
            return false;
        }

        ServerTestInstance test = getTestInstance(testId);

        if (PreconditionsSafe.checkIsNull(test, ServerTestInstance.TAG, false))
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "No ServerTestInstance found for testID: " + testId);
            return false;
        }

        return test.insertIncomingMsg(message);
    }

    public synchronized boolean registerNewTest(final WebSocketSession session, final RegisterClientLibMessage registerClientLibMessage)
    {
        if (PreconditionsSafe.checkIsNull(session, WebSocketSession.class.getSimpleName()))
            return false;

        if (PreconditionsSafe.checkIsNull(registerClientLibMessage, RegisterClientLibMessage.class.getSimpleName()))
            return false;

        final String testId = registerClientLibMessage.getTestID();
        final TestClientInfoModel testInfo = registerClientLibMessage.getTestClientInfoModel();

        if (PreconditionsSafe.checkIsNullOrEmpty(testId, GenericConstants.TEST_ID))
        {
            return false;
        }
        if (tests.containsKey(testId))
        {
            // Send ERROR - registruje test po druhe
            ServerLogFacade.logWarning(LOGGER, TAG, "Register test more then once: " + testId);
            return false;
        }

        final ServerTestInstance test = ServerTestInstance.createNewTest(testId, registerClientLibMessage.getTestGroupModel(), session, testInfo, registerClientLibMessage.getClientInfo(), new IOnTestClosed()
        {
            @Override
            public void onClosed(ServerTestInstance serverTestInstance)
            {
                serverTestInstanceDBService.saveClosedTest(serverTestInstance);
            }
        });

        tests.put(testId, test);
        return true;
    }

    // Remove session from test, if exists - ClientLib/Phone for example
    public synchronized void removeSessionIfExists(final String sessionId)
    {
        if (PreconditionsSafe.checkIsNullOrEmpty(sessionId, GenericConstants.SESSION_ID))
            return;

        List<String> testsToRemove = new ArrayList<>();
        // Remove Groovy or Phone session
        // Unlock phones
        for (Map.Entry<String, ServerTestInstance> i : tests.entrySet())
        {
            ServerTestInstance sti = i.getValue();
            boolean wasRemoved = sti.removeSessionIfExists(sessionId);
            if (wasRemoved)
                testsToRemove.add(sti.getTestID());
        }
        for (String testIDToRemove : testsToRemove)
        {
            deleteTestRecord(testIDToRemove);
        }
    }

    public synchronized TaskResult<Boolean> canForward(final WebSocketSession session, final WebSocketMessageBase message)
    {
        if (PreconditionsSafe.checkIsNull(message, WebSocketMessageBase.class.getSimpleName()))
        {
            return new TaskResult<>(new TestErrorException("Message is null"));
        }

        String testId = message.getTestID();

        if (!containsTest(testId))
        {
            return new TaskResult<>(new TestErrorException("TestID not found"));
        }

        ServerTestInstance test = getTestInstance(testId);
        if (test == null)
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "No ServerTestInstance found for testID: " + testId);
            return new TaskResult<>(new TestErrorException("No ServerTestInstance found for testID: " + testId));
        }

        if (test.isClosed())
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "Test is already closed. TestID: " + testId);
            return new TaskResult<>(new TestErrorException("Test is already closed. TestID: " + testId));
        }

        boolean containsSession = test.containsSession(session);

        if (!containsSession)
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "Test does not contains session. SessionID: " + session.getId() + " : TestID: " + testId);
            // Send error
            return new TaskResult<>(new TestErrorException("Test does not contains session. SessionID: " + session.getId() + " : TestID: " + testId));
        }

        boolean result = true;

        result &= test.containsRecipient(message.getRecipientKey());
        if (!result)
        {
            return new TaskResult<>(new TestErrorException("Test doest not contains recipient. TestID:" + testId + "; Recipient: " + message.getRecipientKey()));
        }

        return new TaskResult<>(result);
    }

    @Nullable
    public synchronized ClientLibInstance getClientSession(final String testID)
    {
        if (!containsTest(testID))
        {
            return null;
        }

        ServerTestInstance test = getTestInstance(testID);

        if (test == null || test.isClosed())
            return null;

        return test.getClientLibInstance();
    }

    @Nullable
    public synchronized WebSocketSession getSessionByRecipientKey(final String testId, final String recipientKey)
    {
        if (!containsTest(testId))
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "getSessionByRecipientKey: testID not found; recipientKey: " + recipientKey);
            return null;
        }
        if (PreconditionsSafe.checkIsNullOrEmpty(recipientKey, GenericConstants.RECIPIENT_KEY))
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "getSessionByRecipientKey: recipientKey is null or empty; testID: " + testId);
            return null;
        }

        ServerTestInstance test = getTestInstance(testId);

        if (PreconditionsSafe.checkIsNull(test, ServerTestInstance.TAG, false))
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "TestRegister does not contains test for TestID: " + testId);
            return null;
        }

        return test.getSessionByRecipientKey(recipientKey);
    }

    public synchronized boolean lockPhone(final WebSocketSession session, LockPhoneMessage msg)
    {
        if (PreconditionsSafe.checkIsNull(msg, LockPhoneMessage.class.getSimpleName()))
            return false;

        String testId = msg.getTestID();

        if (!containsTest(testId))
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "lockPhone; testID not found: " + testId + "; msg: " + msg);
            return false;
        }

        boolean canLock = checkPhoneLockForAnotherTest(msg.getPhoneInfoModel(), testId);

        if (canLock)
        {
            ServerTestInstance test = getTestInstance(testId);
            if (test == null)
                return false;
            test.lockPhoneForTest(session, msg.getPhoneInfoModel());
            return true;
        }
        else
        {
            // Send ERROR - telefon zamcen pro jiny test
            return false;
        }
    }

    public synchronized boolean unLockPhone(@NotNull final UnLockPhoneMessage msg)
    {
        if (PreconditionsSafe.checkIsNull(msg, UnLockPhoneMessage.class.getSimpleName()))
            return false;

        String testId = msg.getTestID();

        if (!containsTest(testId))
        {
            return false;
        }

        ServerTestInstance test = getTestInstance(testId);
        if (test == null)
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "Test not found for TestID: " + testId);
            return false;
        }

        test.unLockPhoneForTest(msg.getPhoneInfoModel());
        test.insertIncomingMsg(msg);
        return true;
    }

    private synchronized boolean checkPhoneLockForAnotherTest(@NotNull final PhoneInfoModel phoneInfoModel, @NotNull final String testId)
    {
        if (PreconditionsSafe.checkIsNull(phoneInfoModel, PhoneInfoModel.class.getSimpleName()))
            return false;

        if (!containsTest(testId))
            return false;

        for (Map.Entry<String, ServerTestInstance> i : tests.entrySet())
        {
            if (i.getKey().equals(testId))
                continue;

            if (i.getValue().containsLockedPhone(phoneInfoModel.getPhoneKey()))
                return false;
        }
        return true;
    }

    @Nullable
    public synchronized ServerTestInstance getTestByPhoneSenderKey(@NotNull final String senderKey)
    {
        if (PreconditionsSafe.checkIsNullOrEmpty(senderKey, GenericConstants.SENDER_KEY))
            return null;

        for (Map.Entry<String, ServerTestInstance> i : tests.entrySet())
        {
            if (i.getValue().containsLockedPhone(senderKey))
                return i.getValue();
        }
        return null;
    }

    public synchronized List<MessageRegisterItem> getIncomingMessages(@NotNull final String testId)
    {
        final ServerTestInstance test = getTestInstance(testId);
        if (PreconditionsSafe.checkIsNull(test, ServerTestInstance.TAG, false))
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "TestRegister does not contains test for TestID: " + testId);
            return new ArrayList<>();
        }
        return test.getIncomingMessages();
    }

    public synchronized List<ClientInfo> getConnectedClients()
    {
        List<ClientInfo> list = new ArrayList<>();
        for (Map.Entry<String, ServerTestInstance> i : tests.entrySet())
        {
            ClientLibInstance inst = i.getValue().getClientLibInstance();
            if (inst != null)
                list.add(inst.getClientInfo());
        }
        return list;
    }

    @Nullable
    public synchronized ServerTestInstance getTestInstance(@NotNull final String testId)
    {
        if (!containsTest(testId))
        {
            return null;
        }
        return tests.get(testId);
    }

    public List<ServerTestInstance> getAllTests()
    {
        return ListUtils.toList(tests.values());
    }

    public List<ServerTestInstance> getAllOpenTests()
    {
        return ListUtils.toList(tests.values())
                .stream()
                .filter(serverTestInstance -> !serverTestInstance.isClosed())
                .collect(Collectors.toList());
    }

    @Nullable
    public SmscSessionId addNewSmscConnection(@NotNull final String testID, @NotNull final SmscSessionId smscSessionId, @NotNull final SmppSession smscSession)
    {
        if (!containsTest(testID))
        {
            return null;
        }

        ServerTestInstance ti = getTestInstance(testID);
        if (PreconditionsSafe.checkIsNull(ti, ServerTestInstance.TAG))
        {
            // Error
            return null;
        }

        return ti.addNewSmscConnection(smscSessionId, smscSession);
    }

    @Nullable
    public SmppSession getSmscSession(String testID, SmscSessionId smscSessionId)
    {
        if (!containsTest(testID))
        {
            return null;
        }
        ServerTestInstance ti = getTestInstance(testID);
        if (PreconditionsSafe.checkIsNull(ti, ServerTestInstance.TAG))
        {
            // Error
            return null;
        }

        return ti.getSmscSession(smscSessionId);
    }

    public void removeSmscSession(String testID, SmscSessionId smscSessionId)
    {
        if (!containsTest(testID))
        {
            return;
        }

        ServerTestInstance ti = getTestInstance(testID);
        if (PreconditionsSafe.checkIsNull(ti, ServerTestInstance.TAG))
        {
            // Error
            return;
        }

        ti.removeSmscSession(smscSessionId);
    }

    @Nullable
    public ClientLibInstance getClientInfoByClientKey(String clientKey)
    {
        if (PreconditionsSafe.checkIsNullOrEmpty(clientKey, GenericConstants.CLIENT_KEY))
            return null;

        for (ServerTestInstance i : tests.values())
        {
            if (i.getClientInfo() == null)
                continue;

            if (i.getClientInfo().getClientKey().equals(clientKey))
            {
                return i.getClientLibInstance();
            }
        }
        return null;
    }

    public synchronized List<ServerTestInstance> getTestsByMmscRecipients(List<MM7Address> recipients)
    {
        List<ServerTestInstance> result = new ArrayList<>();
        for (ServerTestInstance test : tests.values())
        {
            if (test.isClosed())
                continue;

            if (MmscAcquireRouteUtil.isMatch(test.getMmscAcquireRouteModel(), recipients))
                result.add(test);
        }
        return result;
    }

    @Nullable
    public WebSocketMessageBase getMessageByTestIdMessageId(String testId, String messageId)
    {
        if (!containsTest(testId))
        {
            return null;
        }

        for (MessageRegisterItem msg : getIncomingMessages(testId))
        {
            if (msg.getMessage().getMessageID().equals(messageId))
                return msg.getMessage();
        }
        return null;
    }

    public boolean deleteTestRecord(ServerTestInstance serverTestInstance)
    {
        if (PreconditionsSafe.checkIsNull(serverTestInstance, ServerTestInstance.TAG))
            return false;

        return this.deleteTestRecord(serverTestInstance.getTestID());
    }

    public synchronized boolean deleteTestRecord(String testId)
    {
        if (tests.containsKey(testId))
        {
            tests.remove(testId);
            return true;
        }
        else
        {
            return false;
        }
    }

    public synchronized boolean processValidationResult(TestValidationMessage msg)
    {
        if (PreconditionsSafe.checkIsNull(msg, TestValidationMessage.class.getSimpleName()))
        {
            return false;
        }

        String testId = msg.getTestID();
        if (!containsTest(testId))
        {
            // TODO
            return false;
        }

        ServerTestInstance test = getTestInstance(testId);
        if (PreconditionsSafe.checkIsNull(test, ServerTestInstance.TAG))
        {
            return false;
        }

        test.addValidationResults(msg.getValidationResult());
        return true;
    }

    public synchronized void deleteOldTests()
    {
        long limit = System.currentTimeMillis() - AppConfig.getDeleteTestAfter();

        for (Map.Entry<String, ServerTestInstance> o : tests.entrySet())
        {
            String key = o.getKey();
            ServerTestInstance test = tests.get(key);
            long created = test.getCreatedDT();
            if (created < limit && test.isClosed())
            {
                deleteTestRecord(test);
            }
        }
    }

    private synchronized boolean containsTest(final String testId)
    {
        if (PreconditionsSafe.checkIsNullOrEmpty(testId, GenericConstants.TEST_ID))
        {
            return false;
        }

        if (!tests.containsKey(testId))
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "TestRegister does not contains test for TestID: " + testId);
            return false;
        }
        return true;
    }

    public void addOutgointMessage(final WebSocketSession session, final WebSocketMessageBase message)
    {
        if (PreconditionsSafe.checkIsNull(message, WebSocketMessageBase.class.getSimpleName()))
            return;

        ServerTestInstance test = getTestInstance(message.getTestID());
        if (test == null)
            return;

        test.insertOutgoingMsg(message);
    }

    public void addOutgointMessage(final WebSocketSession session, final String payload)
    {
        if (PreconditionsSafe.checkIsNullOrEmpty(payload, GenericConstants.PAYLOAD))
            return;

        WebSocketMessageBase msg = MessageUtils.getMessageNonTyped(payload);
        addOutgointMessage(session, msg);
    }

    public Boolean forceCloseTest(String testId)
    {
        if (PreconditionsSafe.checkIsNullOrEmpty(testId, GenericConstants.TEST_ID))
            return false;

        ServerTestInstance test = getTestInstance(testId);
        if (test == null)
            return false;

        ServerLogFacade.logInfo(LOGGER, "Force close test. TestID: " + testId);
        test.closeTest();
        return true;
    }

    public boolean processTestResult(TestResultMessage msg)
    {
        if (PreconditionsSafe.checkIsNull(msg, TestResultMessage.class.getSimpleName()))
        {
            return false;
        }

        String testId = msg.getTestID();
        if (!containsTest(testId))
        {
            // TODO
            return false;
        }

        ServerTestInstance test = getTestInstance(testId);
        if (PreconditionsSafe.checkIsNull(test, ServerTestInstance.TAG))
        {
            return false;
        }

        return test.processTestResult(msg);
    }
}
