package cz.uhk.dip.mmsparams.server.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public class ControllerHelper
{
    private ControllerHelper()
    {
    }

    public static <T> ResponseEntity<T> getResult(final Optional<T> optEntity)
    {
        if (!optEntity.isPresent())
            return new ResponseEntity<T>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<T>(optEntity.get(), HttpStatus.OK);
    }
}
