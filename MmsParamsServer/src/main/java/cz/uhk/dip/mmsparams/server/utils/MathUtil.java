package cz.uhk.dip.mmsparams.server.utils;

public class MathUtil
{
    private MathUtil()
    {
    }

    public static int getTotalPages(int total, int pageSize)
    {
        return (int) Math.ceil(total / (double) pageSize);
    }
}
