package cz.uhk.dip.mmsparams.server.utils;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

public class MessageUtilsServer
{
    private MessageUtilsServer()
    {
    }

    public static WebSocketMessageBase changeRecipientKey(final WebSocketMessageBase message, final ServerTestInstance stInstance)
    {
        message.setRecipientKey(stInstance.getClientLibKey());
        message.setTestID(stInstance.getTestID());
        return message;
    }
}
