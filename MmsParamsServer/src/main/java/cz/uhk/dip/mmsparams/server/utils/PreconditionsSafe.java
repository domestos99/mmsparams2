package cz.uhk.dip.mmsparams.server.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;

public class PreconditionsSafe
{
    private static final Logger LOGGER = LoggerFactory.getLogger(PreconditionsSafe.class);
    private static final String TAG = PreconditionsSafe.class.getSimpleName();

    private PreconditionsSafe()
    {
    }

    public static boolean checkIsNull(Object obj, String simpleName)
    {
        return checkIsNull(obj, simpleName, true);
    }

    public static boolean checkIsNull(Object obj, String simpleName, boolean log)
    {
        if (obj == null)
        {
            if (log)
                ServerLogFacade.logWarning(LOGGER, TAG, simpleName + " is NULL!");
            return true;
        }
        return false;
    }

    public static boolean checkIsNullOrEmpty(String s, String name)
    {
        return checkIsNullOrEmpty(s, name, true);
    }

    public static boolean checkIsNullOrEmpty(String s, String name, boolean log)
    {
        if (StringUtil.isEmptyOrNull(s))
        {
            if (log)
                ServerLogFacade.logWarning(LOGGER, TAG, name + " is NULL or EMPTY!");
            return true;
        }
        return false;
    }
}
