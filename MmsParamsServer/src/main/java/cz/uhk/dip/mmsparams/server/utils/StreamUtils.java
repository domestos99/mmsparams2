package cz.uhk.dip.mmsparams.server.utils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamUtils
{
    private StreamUtils()
    {
    }

    public static <T> List<T> toList(Stream<T> webSocketMessageBaseStream)
    {
        return webSocketMessageBaseStream.collect(Collectors.toList());
    }
}
