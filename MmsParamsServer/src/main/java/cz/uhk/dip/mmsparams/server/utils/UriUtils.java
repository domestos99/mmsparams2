package cz.uhk.dip.mmsparams.server.utils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UriUtils
{
    private UriUtils()
    {
    }

    public static String getRequestUri(ServletRequest request)
    {
        HttpServletRequest req = (HttpServletRequest) request;
        return req == null ? null : req.getRequestURI();
    }

    public static String getRequestMethod(ServletRequest request)
    {
        HttpServletRequest req = (HttpServletRequest) request;
        return req == null ? null : req.getMethod();
    }

    public static int getResponseStatus(ServletResponse response)
    {
        HttpServletResponse res = (HttpServletResponse) response;
        return res == null ? null : res.getStatus();
    }
}
