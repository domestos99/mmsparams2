package cz.uhk.dip.mmsparams.server.validation;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.server.tests.ClientLibInstance;

public class ClientLibInstanceValidator
{
    private ClientLibInstanceValidator()
    {
    }

    public static void validate(ClientLibInstance clientLibInstance)
    {
        Preconditions.checkNotNull(clientLibInstance, GenericConstants.CLIENT_LIB_INSTANCE);
        Preconditions.checkNotNull(clientLibInstance.getClientSession(), GenericConstants.SESSION);
        Preconditions.checkNotNull(clientLibInstance.getClientInfo(), GenericConstants.CLIENT_INFO);
        Preconditions.checkNotNullOrEmpty(clientLibInstance.getClientInfo().getClientKey(), GenericConstants.CLIENT_KEY);
    }
}
