package cz.uhk.dip.mmsparams.server.validation;

import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;

public class JwtRequestValidator
{
    public static boolean validate(JwtRequest authenticationRequest)
    {
        if (authenticationRequest == null || StringUtil.isEmptyOrNull(authenticationRequest.getUsername()) || StringUtil.isEmptyOrNull(authenticationRequest.getPassword()))
            return false;
        return true;
    }
}
