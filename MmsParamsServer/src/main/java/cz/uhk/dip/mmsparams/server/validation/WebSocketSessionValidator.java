package cz.uhk.dip.mmsparams.server.validation;

import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;

public class WebSocketSessionValidator
{
    private WebSocketSessionValidator()
    {
    }

    public static void validate(WebSocketSession session)
    {
        Preconditions.checkNotNull(session, GenericConstants.SESSION);
        Preconditions.checkNotNullOrEmpty(session.getId(), GenericConstants.SESSION_ID);
        Preconditions.checkTrue(session.isOpen(), GenericConstants.OPEN);
    }
}
