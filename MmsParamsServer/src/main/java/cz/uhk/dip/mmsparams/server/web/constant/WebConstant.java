package cz.uhk.dip.mmsparams.server.web.constant;

public final class WebConstant
{
    public static final String CLIENT_CLIENTS = "/api/clients";
    public static final String CLIENT_CLIENTS_CLIENTKEY = "/api/clients/{clientKey}";
    public static final String CLIENT_FORCE_DISCONNECT = "/api/clients/forcedisconnect/{clientKey}";

    public static final String MESSAGE_REGISTER_TESTID_MESSAGEID = "/api/messageregister/{id}/{testId}/{messageId}/{createdDT}";
    public static final String MESSAGE_REGISTER_TESTID_MESSAGEID_SEQUENCE = "/api/messageregister/sequence/{testId}";

    public static final String PHONES_ALL = "/api/phones";
    public static final String PHONES_PHONEKEY = "/api/phones/{phoneKey}";
    public static final String PHONES_FORCE_DISCONNECT = "/api/phones/forcedisconnect/{phoneKey}";

    public static final String SESSION_ALL = "/api/sessions";
    public static final String SESSION_BY_ID = "/api/sessions/{id}";


    public static final String SYSTEM_VERSION = "/api/systemversion";


    public static final String TEST_INSTANCE_ALL = "/api/testinstances";
    public static final String TEST_INSTANCE_BY_TEST_ID = "/api/testinstances/{testId}";
    public static final String TEST_INSTANCE_DELETE_BY_TEST_ID = "/api/testinstances/{testId}";
    public static final String TEST_INSTANCE_DELETE_BY_TEST_ID_2 = "/api/testinstances/delete/{testId}";
    public static final String TEST_FORCE_CLOSE = "/api/testinstances/forceclose/{testId}";


    public static final String PING_PING = "/api/Ping";
    public static final String ADMIN_REFRESH_CACHES = "/api/admin/refreshcache";
    public static final String ADMIN_DELETE_OLD_TESTS = "/api/admin/deleteoldtests/{days}";

    public static final String TEST_ERROR_GET_BY_ID = "/api/testerror/{id}";

    public static final String CLIENT_LOGS_BY_TEST_ID = "/api/clientlogs/{testId}";

    public static final String TEST_INSTANCE_VIEW = "/api/testgroupview";


}
