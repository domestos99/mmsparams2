package cz.uhk.dip.mmsparams.server.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import cz.uhk.dip.mmsparams.server.cache.ClientInfoDBCache;
import cz.uhk.dip.mmsparams.server.cache.MessageDBCache;
import cz.uhk.dip.mmsparams.server.cache.PhoneInfoDBCache;
import cz.uhk.dip.mmsparams.server.cache.ServerTestInstanceDBCache;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;

import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.ADMIN_DELETE_OLD_TESTS;
import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.ADMIN_REFRESH_CACHES;

@RestController
public class AdminController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
    private static final String TAG = AdminController.class.getSimpleName();

    private final ClientInfoDBCache clientInfoDBCache;
    private final MessageDBCache messageDBCache;
    private final PhoneInfoDBCache phoneInfoDBCache;
    private final ServerTestInstanceDBCache serverTestInstanceDBCache;

    @Autowired
    public AdminController(ClientInfoDBCache clientInfoDBCache, MessageDBCache messageDBCache, PhoneInfoDBCache phoneInfoDBCache, ServerTestInstanceDBCache serverTestInstanceDBCache)
    {
        this.clientInfoDBCache = clientInfoDBCache;
        this.messageDBCache = messageDBCache;
        this.phoneInfoDBCache = phoneInfoDBCache;
        this.serverTestInstanceDBCache = serverTestInstanceDBCache;
    }


    @GetMapping(path = ADMIN_REFRESH_CACHES)
    public ResponseEntity<Boolean> refreshServerCaches()
    {
        this.clientInfoDBCache.clearCaches();
        this.messageDBCache.clearCaches();
        this.phoneInfoDBCache.clearCaches();
        this.serverTestInstanceDBCache.clearCaches();

        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @GetMapping(path = ADMIN_DELETE_OLD_TESTS)
    public ResponseEntity<Boolean> deleteOldTests(@PathVariable("days") String days)
    {
        boolean result = false;
        try
        {
            int daysInt = Integer.parseInt(days);
            result = this.serverTestInstanceDBCache.deleteOldTests(daysInt);
            refreshServerCaches();
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "deleteOldTests", e);
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
