package cz.uhk.dip.mmsparams.server.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import cz.uhk.dip.mmsparams.api.utils.OptionalUtil;
import cz.uhk.dip.mmsparams.server.cache.ClientInfoDBCache;
import cz.uhk.dip.mmsparams.server.database.dto.ClientInfoDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ClientLibInstanceDTO;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IClientLibWSController;

import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.CLIENT_CLIENTS;
import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.CLIENT_CLIENTS_CLIENTKEY;
import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.CLIENT_FORCE_DISCONNECT;

@RestController
public class ClientController
{
    private final IClientLibWSController iClientLibWSController;
    private final ClientInfoDBCache clientInfoDBCache;

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientController.class);
    private static final String TAG = ClientController.class.getSimpleName();

    @Autowired
    public ClientController(IClientLibWSController iClientLibWSController, ClientInfoDBCache clientInfoDBCache)
    {
        this.iClientLibWSController = iClientLibWSController;
        this.clientInfoDBCache = clientInfoDBCache;
    }

    @GetMapping(path = CLIENT_CLIENTS, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<ClientInfoDTO>> getClients()
    {
        return new ResponseEntity<>(ClientInfoDTO.create(iClientLibWSController.getConnectedClients()), HttpStatus.OK);
    }

    @GetMapping(path = CLIENT_CLIENTS_CLIENTKEY, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClientLibInstanceDTO> getClientByKey(@PathVariable("clientKey") String clientKey)
    {
        Optional<ClientLibInstanceDTO> dtoOpt = this.clientInfoDBCache.getByPhoneKey(clientKey);

        if (!dtoOpt.isPresent())
        {
            dtoOpt = OptionalUtil.fillOptional(ClientLibInstanceDTO.create(iClientLibWSController.getConnectedClient(clientKey)));
        }

        if (!dtoOpt.isPresent())
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(dtoOpt.get(), HttpStatus.OK);
    }

    @PostMapping(path = CLIENT_FORCE_DISCONNECT)
    public ResponseEntity<Boolean> postForceDisconnect(@PathVariable("clientKey") String clientKey)
    {
        return new ResponseEntity<>(iClientLibWSController.forceDisconnectClient(clientKey), HttpStatus.OK);
    }

}
