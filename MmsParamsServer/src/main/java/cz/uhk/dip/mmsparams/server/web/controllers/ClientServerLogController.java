package cz.uhk.dip.mmsparams.server.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import cz.uhk.dip.mmsparams.api.http.RestApiConstants;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.web.model.ClientServerLogModel;
import cz.uhk.dip.mmsparams.server.database.dto.ClientServerLogDTO;
import cz.uhk.dip.mmsparams.server.database.services.IClientServerLogDBService;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;

import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.CLIENT_LOGS_BY_TEST_ID;

@RestController
public class ClientServerLogController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientServerLogController.class);
    private static final String TAG = ClientServerLogController.class.getSimpleName();

    private final IClientServerLogDBService iClientServerLogDBService;

    @Autowired
    public ClientServerLogController(IClientServerLogDBService iClientServerLogDBService)
    {
        this.iClientServerLogDBService = iClientServerLogDBService;
    }

    @GetMapping(path = CLIENT_LOGS_BY_TEST_ID, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClientServerLogDTO> getClientLogsByTestID(@PathVariable("testId") String testId)
    {
        Optional<ClientServerLogDTO> opt = this.iClientServerLogDBService.getByTestID(testId);

        if (!opt.isPresent())
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(opt.get(), HttpStatus.OK);
    }

    @PostMapping(path = RestApiConstants.CLIENT_LOGS, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Boolean> postClientServerLog(@RequestBody byte[] data)
    {
        try
        {
            ClientServerLogModel a = JsonUtilsSafe.fromJson(data, ClientServerLogModel.class);

            iClientServerLogDBService.save(a);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "postClientServerLog", e);
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
    }

}
