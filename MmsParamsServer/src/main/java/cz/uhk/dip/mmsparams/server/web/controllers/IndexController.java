package cz.uhk.dip.mmsparams.server.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController
{
    @GetMapping("/")
    public String get()
    {
        return "index.html";
    }
}
