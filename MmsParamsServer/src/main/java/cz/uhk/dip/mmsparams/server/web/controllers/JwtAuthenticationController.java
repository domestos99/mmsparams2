package cz.uhk.dip.mmsparams.server.web.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cz.uhk.dip.mmsparams.api.http.auth.AuthConstants;
import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.http.auth.JwtResponse;
import cz.uhk.dip.mmsparams.server.auth.IJwtTokenService;
import cz.uhk.dip.mmsparams.server.validation.JwtRequestValidator;

@RestController
public class JwtAuthenticationController
{
    private final IJwtTokenService iJwtTokenService;

    @Autowired
    public JwtAuthenticationController(IJwtTokenService iJwtTokenService)
    {
        this.iJwtTokenService = iJwtTokenService;
    }

    @PostMapping(value = AuthConstants.AUTH_ADDRESS, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<JwtResponse> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest)
    {
        if (!JwtRequestValidator.validate(authenticationRequest))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        final JwtResponse newToken = this.iJwtTokenService.getNewToken(authenticationRequest);

        if (newToken == null)
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        return new ResponseEntity<>(newToken, HttpStatus.OK);
    }


}
