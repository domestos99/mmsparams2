package cz.uhk.dip.mmsparams.server.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import cz.uhk.dip.mmsparams.api.utils.OptionalUtil;
import cz.uhk.dip.mmsparams.server.cache.MessageDBCache;
import cz.uhk.dip.mmsparams.server.database.dto.MessageSequenceList;
import cz.uhk.dip.mmsparams.server.database.dto.WebSocketMessageDTO;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;
import cz.uhk.dip.mmsparams.server.tests.TestRegister;

import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.MESSAGE_REGISTER_TESTID_MESSAGEID;
import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.MESSAGE_REGISTER_TESTID_MESSAGEID_SEQUENCE;


@RestController
public class MessageRegisterController
{
    private final TestRegister testRegister;
    private final MessageDBCache messageDBCache;

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageRegisterController.class);
    private static final String TAG = MessageRegisterController.class.getSimpleName();

    @Autowired
    public MessageRegisterController(TestRegister testRegister, MessageDBCache messageDBCache)
    {
        this.testRegister = testRegister;
        this.messageDBCache = messageDBCache;
    }

    @GetMapping(path = MESSAGE_REGISTER_TESTID_MESSAGEID, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<WebSocketMessageDTO> getMessageById(@PathVariable("id") Integer id, @PathVariable("testId") String testId,
                                                              @PathVariable("messageId") String messageId,
                                                              @PathVariable("createdDT") long createdDT)
    {
        Optional<WebSocketMessageDTO> dtoOpt = this.messageDBCache.getById(id);

        if (!dtoOpt.isPresent())
        {
            ServerTestInstance xxx = testRegister.getTestInstance(testId);
            if (xxx == null)
            {
                dtoOpt = Optional.empty();
            }
            else
            {
                WebSocketMessageDTO aaa = WebSocketMessageDTO.create(xxx.findMessage(messageId, createdDT));
                dtoOpt = OptionalUtil.fillOptional(aaa);
            }
        }

        if (!dtoOpt.isPresent())
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(dtoOpt.get(), HttpStatus.OK);
    }


    @GetMapping(path = MESSAGE_REGISTER_TESTID_MESSAGEID_SEQUENCE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MessageSequenceList> sequence(@PathVariable("testId") String testId)
    {
        Optional<MessageSequenceList> dtoOpt = this.messageDBCache.getSequenceByTestID(testId);

        if (!dtoOpt.isPresent())
        {
//            ServerTestInstance xxx = testRegister.getTestInstance(testId);
//            if (xxx == null)
//            {
//                dtoOpt = Optional.empty();
//            }
//            else
//            {
//                WebSocketMessageDTO aaa = WebSocketMessageDTO.create(xxx.findMessage(messageId, createdDT));
//                dtoOpt = OptionalUtil.fillOptional(aaa);
//            }
        }

        if (!dtoOpt.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(dtoOpt.get(), HttpStatus.OK);
    }


}
