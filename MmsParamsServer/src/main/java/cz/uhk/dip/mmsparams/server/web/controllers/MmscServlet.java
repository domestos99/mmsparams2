package cz.uhk.dip.mmsparams.server.web.controllers;

import net.instantcom.mm7.DeliverReq;
import net.instantcom.mm7.DeliverRsp;
import net.instantcom.mm7.DeliveryReportReq;
import net.instantcom.mm7.DeliveryReportRsp;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MM7Message;
import net.instantcom.mm7.MM7Request;
import net.instantcom.mm7.MM7Response;
import net.instantcom.mm7.ReadReplyReq;
import net.instantcom.mm7.ReadReplyRsp;
import net.instantcom.mm7.VASP;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;

@Component
public class MmscServlet extends HttpServlet
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MmscServlet.class);
    private static final String TAG = MmscServlet.class.getSimpleName();

    private final VASP vasp;

    @Autowired
    public MmscServlet(VASP vasp)
    {
        this.vasp = vasp;
    }

    private VASP getVasp()
    {
        return this.vasp;
    }

    // Uses LoggingFilter!
    @Override
    public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException
    {
        // Copy request and response
        final ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper(req);
        final ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(resp);

        boolean wasSuccess;
        try
        {
            // Try process request
            wasSuccess = doPostInternal(requestWrapper, responseWrapper);
        }
        catch (Exception ex)
        {
            wasSuccess = false;
            ServerLogFacade.logEx(LOGGER, TAG, "doPost-whole", ex);
        }

        if (wasSuccess)
        {
            // OK, finish - success, close all
            // Do not forget this line after reading response content or actual response will be empty!
            responseWrapper.copyBodyToResponse();
        }
        else
        {
            // Get Request content
            final String requestBody = new String(requestWrapper.getContentAsByteArray());

            // Try to return Success response if error
            resolveResponseIfError(responseWrapper, requestBody);
            // Do not forget this line after reading response content or actual response will be empty!
            responseWrapper.copyBodyToResponse();
        }
    }

    private boolean doPostInternal(HttpServletRequest req, HttpServletResponse resp)
    {
        try
        {
            // Decode incoming SOAP message
            InputStream in = req.getInputStream();

            MM7Request mm7request;
            try
            {
                String ct = req.getContentType();

                ServerLogFacade.logInfo(LOGGER, TAG, "Request content type: " + ct);
                mm7request = (MM7Request) MM7Message.load(in, ct, getVasp().getContext());
            }
            finally
            {
                if (in != null)
                {
                    try
                    {
                        in.close();
                    }
                    catch (Exception e)
                    {
                        ServerLogFacade.logEx(LOGGER, TAG, "doPost - close in stream", e);
                    }
                }
            }

            // Call a callback on a client
            MM7Response mm7response = dispatchMM7Request(mm7request);
            // Everything OK, write proper response
            writeRespToStream(resp, mm7response);
        }
        catch (MM7Error mm7error)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "MM7 request failed", mm7error);

            try
            {
                getVasp().handleMm7Error(mm7error);
            }
            catch (Exception e)
            {
                ServerLogFacade.logEx(LOGGER, TAG, "MM7 request failed - handleMm7Error", e);
            }

            return false;
        }
        catch (Exception ex)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "MM7 request failed", ex);
            return false;
        }
        return true;
    }

    private void writeRespToStream(final HttpServletResponse resp, final MM7Response mm7response) throws IOException
    {
        // Write out SOAP message
        resp.setContentType(mm7response.getSoapContentType());

        final OutputStream out = resp.getOutputStream();
        try
        {
            MM7Message.save(mm7response, out, getVasp().getContext());
        }
        catch (Exception ex)
        {
            ServerLogFacade.logEx(LOGGER, ex);
            throw ex;
        }
        finally
        {
            if (out != null)
            {
                try
                {
                    out.close();
                }
                catch (Exception ex)
                {
                    ServerLogFacade.logEx(LOGGER, ex);
                }
            }
        }
    }

    protected MM7Response dispatchMM7Request(final MM7Request req) throws MM7Error
    {
        MM7Response resp;
        if (req instanceof DeliverReq)
        {
            resp = getVasp().deliver((DeliverReq) req);
        }
        else if (req instanceof DeliveryReportReq)
        {
            resp = getVasp().deliveryReport((DeliveryReportReq) req);
        }
        else if (req instanceof ReadReplyReq)
        {
            resp = getVasp().readReply((ReadReplyReq) req);
        }
        else
        {
            throw new MM7Error("method not supported: " + req.getClass());
        }
        return resp;
    }

    private void resolveResponseIfError(final ContentCachingResponseWrapper responseWrapper, final String requestBody) throws IOException
    {
        MM7Response response = null;
        if (requestBody.contains("DeliverReq"))
        {
            response = new DeliverRsp();
            response.setStatusCode(MM7Response.SC_SUCCESS);
            response.setStatusText(response.getStatusText());

        }
        else if (requestBody.contains("DeliveryReportReq"))
        {
            response = new DeliveryReportRsp();
            response.setStatusCode(MM7Response.SC_SUCCESS);
            response.setStatusText(response.getStatusText());

        }
        else if (requestBody.contains("ReadReplyReq"))
        {
            response = new ReadReplyRsp();
            response.setStatusCode(MM7Response.SC_SUCCESS);
            response.setStatusText(response.getStatusText());
        }

        if (response != null)
        {
            writeRespToStream(responseWrapper, response);
        }
        else
        {
            // TODO neco napsat do response
        }
    }
}
