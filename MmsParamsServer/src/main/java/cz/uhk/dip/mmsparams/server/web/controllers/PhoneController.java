package cz.uhk.dip.mmsparams.server.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import cz.uhk.dip.mmsparams.api.utils.OptionalUtil;
import cz.uhk.dip.mmsparams.server.cache.PhoneInfoDBCache;
import cz.uhk.dip.mmsparams.server.database.dto.PhoneInfoDTO;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IPhoneWSController;

import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.PHONES_ALL;
import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.PHONES_FORCE_DISCONNECT;
import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.PHONES_PHONEKEY;


@RestController
public class PhoneController
{
    private final IPhoneWSController iPhoneWSController;
    private final PhoneInfoDBCache phoneInfoDBCache;

    private static final Logger LOGGER = LoggerFactory.getLogger(PhoneController.class);
    private static final String TAG = PhoneController.class.getSimpleName();

    @Autowired
    public PhoneController(IPhoneWSController iPhoneWSController, PhoneInfoDBCache phoneInfoDBCache)
    {
        this.iPhoneWSController = iPhoneWSController;
        this.phoneInfoDBCache = phoneInfoDBCache;
    }

    @GetMapping(path = PHONES_ALL, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<PhoneInfoDTO>> getPhones()
    {
        return new ResponseEntity<>(PhoneInfoDTO.create(iPhoneWSController.getConnectedPhones()), HttpStatus.OK);
    }

    @GetMapping(path = PHONES_PHONEKEY, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PhoneInfoDTO> getPhonesByPhoneKey(@PathVariable("phoneKey") String phoneKey)
    {
        Optional<PhoneInfoDTO> dtoOpt = this.phoneInfoDBCache.getByPhoneKey(phoneKey);

        if (!dtoOpt.isPresent())
        {
            dtoOpt = OptionalUtil.fillOptional(PhoneInfoDTO.create(iPhoneWSController.getConnectedPhone(phoneKey)));
        }

        if (!dtoOpt.isPresent())
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(dtoOpt.get(), HttpStatus.OK);

    }

    @PostMapping(path = PHONES_FORCE_DISCONNECT)
    public ResponseEntity<Boolean> postForceDisconnect(@PathVariable("phoneKey") String phoneKey) throws IOException
    {
        return new ResponseEntity<>(iPhoneWSController.forceDisconnectPhone(phoneKey), HttpStatus.OK);
    }

}
