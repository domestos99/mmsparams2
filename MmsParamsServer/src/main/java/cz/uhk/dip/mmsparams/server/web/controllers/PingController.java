package cz.uhk.dip.mmsparams.server.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.uhk.dip.mmsparams.api.websocket.model.PingGetModel;

import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.PING_PING;

@RestController
public class PingController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(PingController.class);
    private static final String TAG = PingController.class.getSimpleName();

    @GetMapping(path = PING_PING, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PingGetModel> get()
    {
        PingGetModel gm = new PingGetModel();
        gm.setStatus(1);
        return new ResponseEntity<>(gm, HttpStatus.OK);
    }

}
