package cz.uhk.dip.mmsparams.server.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import cz.uhk.dip.mmsparams.server.database.dto.WSSessionDTO;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISessionWSController;

import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.SESSION_ALL;
import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.SESSION_BY_ID;

@RestController
public class SessionListController
{
    private final ISessionWSController iSessionWSController;

    private static final Logger LOGGER = LoggerFactory.getLogger(SessionListController.class);
    private static final String TAG = SessionListController.class.getSimpleName();

    @Autowired
    public SessionListController(ISessionWSController iSessionWSController)
    {
        this.iSessionWSController = iSessionWSController;
    }

    @GetMapping(path = SESSION_ALL, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<WSSessionDTO>> getSessions()
    {
        return new ResponseEntity<>(WSSessionDTO.create(iSessionWSController.getOpenSessions()), HttpStatus.OK);
    }

    @GetMapping(path = SESSION_BY_ID, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<WSSessionDTO> getSessionById(@PathVariable("id") String id)
    {
        WSSessionDTO result = WSSessionDTO.create(iSessionWSController.getSessionById(id));

        if (result == null)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
