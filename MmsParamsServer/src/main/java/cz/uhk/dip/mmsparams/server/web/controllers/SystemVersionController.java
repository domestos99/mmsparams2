package cz.uhk.dip.mmsparams.server.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.uhk.dip.mmsparams.api.websocket.model.SystemVersionModel;

import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.SYSTEM_VERSION;

@RestController
public class SystemVersionController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SystemVersionController.class);
    private static final String TAG = SystemVersionController.class.getSimpleName();

    @GetMapping(path = SYSTEM_VERSION, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<SystemVersionModel> getSystemVersion()
    {
        return new ResponseEntity<>(SystemVersionModel.create(), HttpStatus.OK);
    }
}
