package cz.uhk.dip.mmsparams.server.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import cz.uhk.dip.mmsparams.server.database.dto.TestErrorDTO;
import cz.uhk.dip.mmsparams.server.database.services.ITestErrorDBService;

import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.TEST_ERROR_GET_BY_ID;

@RestController
public class TestErrorController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TestErrorController.class);
    private static final String TAG = TestErrorController.class.getSimpleName();

    private final ITestErrorDBService iTestErrorDBService;

    @Autowired
    public TestErrorController(ITestErrorDBService iTestErrorDBService)
    {
        this.iTestErrorDBService = iTestErrorDBService;
    }

    @GetMapping(path = TEST_ERROR_GET_BY_ID, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<TestErrorDTO> getTestErrorById(@PathVariable("id") String id)
    {
        int aID = Integer.parseInt(id);

        Optional<TestErrorDTO> dtoOpt = this.iTestErrorDBService.getById(aID);

//        if (!dtoOpt.isPresent())
//        {
//            dtoOpt = OptionalUtil.fillOptional(ClientLibInstanceDTO.create(iClientLibWSController.getConnectedClient(clientKey)));
//        }

        if (!dtoOpt.isPresent())
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(dtoOpt.get(), HttpStatus.OK);
    }

}
