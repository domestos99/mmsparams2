package cz.uhk.dip.mmsparams.server.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import cz.uhk.dip.mmsparams.server.database.dto.views.TestInstanceTestGroupView;
import cz.uhk.dip.mmsparams.server.database.services.ITestInstanceTestGroupViewService;

import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.TEST_INSTANCE_VIEW;

@RestController
public class TestGroupViewController
{
    private final ITestInstanceTestGroupViewService iTestInstanceTestGroupViewService;

    @Autowired
    public TestGroupViewController(ITestInstanceTestGroupViewService iTestInstanceTestGroupViewService)
    {
        this.iTestInstanceTestGroupViewService = iTestInstanceTestGroupViewService;
    }

    @GetMapping(path = TEST_INSTANCE_VIEW, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<TestInstanceTestGroupView>> getAll()
    {
//        List<ServerTestInstanceDTO> dtos = null;
//        if (isOpen)
//        {
//            dtos = ServerTestInstanceDTO.create(testRegister.getAllOpenTests());
//            dtos.sort(new ServerTestInstanceComparatorDesc());
//        }
//        else
//        {
//            dtos = serverTestInstanceDBCache.getAllReduced();
//        }

        List<TestInstanceTestGroupView> data = iTestInstanceTestGroupViewService.getAll();

        return new ResponseEntity<>(data, HttpStatus.OK);
    }


}
