package cz.uhk.dip.mmsparams.server.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import cz.uhk.dip.mmsparams.api.utils.OptionalUtil;
import cz.uhk.dip.mmsparams.server.cache.ServerTestInstanceDBCache;
import cz.uhk.dip.mmsparams.server.comparators.ServerTestInstanceComparatorDesc;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDetailDTO;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;
import cz.uhk.dip.mmsparams.server.tests.TestRegister;

import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.TEST_FORCE_CLOSE;
import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.TEST_INSTANCE_ALL;
import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.TEST_INSTANCE_BY_TEST_ID;
import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.TEST_INSTANCE_DELETE_BY_TEST_ID;
import static cz.uhk.dip.mmsparams.server.web.constant.WebConstant.TEST_INSTANCE_DELETE_BY_TEST_ID_2;


@RestController
public class TestInstanceController
{
    private final TestRegister testRegister;
    private final ServerTestInstanceDBCache serverTestInstanceDBCache;

    private static final Logger LOGGER = LoggerFactory.getLogger(TestInstanceController.class);
    private static final String TAG = TestInstanceController.class.getSimpleName();

    @Autowired
    public TestInstanceController(TestRegister testRegister, ServerTestInstanceDBCache serverTestInstanceDBCache)
    {
        this.testRegister = testRegister;
        this.serverTestInstanceDBCache = serverTestInstanceDBCache;
    }

    @GetMapping(path = TEST_INSTANCE_ALL, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<ServerTestInstanceDTO>> getTestInstances(@RequestParam("isOpen") boolean isOpen)
    {
        List<ServerTestInstanceDTO> dtos = null;
        if (isOpen)
        {
            dtos = ServerTestInstanceDTO.create(testRegister.getAllOpenTests());
            dtos.sort(new ServerTestInstanceComparatorDesc());
        }
        else
        {
            dtos = serverTestInstanceDBCache.getAllReduced();
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @GetMapping(path = TEST_INSTANCE_BY_TEST_ID, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ServerTestInstanceDetailDTO> getTestInstanceDetail(@PathVariable("testId") String testId)
    {
        Optional<ServerTestInstanceDetailDTO> test = this.serverTestInstanceDBCache.getByTestId(testId);

        if (!test.isPresent())
        {
            ServerTestInstance xxx = testRegister.getTestInstance(testId);
            if (xxx == null)
            {
                test = Optional.empty();
            }
            else
            {
                ServerTestInstanceDetailDTO aaa = ServerTestInstanceDetailDTO.create(xxx);
                test = OptionalUtil.fillOptional(aaa);
            }
        }

        if (!test.isPresent())
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(test.get(), HttpStatus.OK);
    }


    @PostMapping(path = TEST_FORCE_CLOSE)
    public ResponseEntity<Boolean> postForceClose(@PathVariable("testId") String testId)
    {
        return new ResponseEntity<>(this.testRegister.forceCloseTest(testId), HttpStatus.OK);
    }

    @GetMapping(path = TEST_INSTANCE_DELETE_BY_TEST_ID_2)
    public ResponseEntity<Boolean> deleteTestInstance2(@PathVariable("testId") String testId)
    {
        boolean removeTestRegister = this.testRegister.deleteTestRecord(testId);

        if (!removeTestRegister)
        {
            removeTestRegister = this.serverTestInstanceDBCache.delete(testId);
        }

        return new ResponseEntity<>(removeTestRegister, HttpStatus.OK);
    }

    @DeleteMapping(path = TEST_INSTANCE_DELETE_BY_TEST_ID)
    public ResponseEntity<Boolean> deleteTestInstance(@PathVariable("testId") String testId)
    {
        boolean removeTestRegister = this.testRegister.deleteTestRecord(testId);

        if (!removeTestRegister)
        {
            removeTestRegister = this.serverTestInstanceDBCache.delete(testId);
        }

        return new ResponseEntity<>(removeTestRegister, HttpStatus.OK);
    }


}
