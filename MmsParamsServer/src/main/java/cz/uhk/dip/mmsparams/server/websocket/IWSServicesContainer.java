package cz.uhk.dip.mmsparams.server.websocket;

import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IBroadcastWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IClientLibWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IEmailWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IForwardWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IMmscWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IPhoneWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISessionWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISmscWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ITestErrorWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IValidationResultWSController;

public interface IWSServicesContainer
{
    IBroadcastWSController getBroadcastWSController();

    IClientLibWSController getClientLibWSController();

    IForwardWSController getForwardWSController();

    IMmscWSController getMmscWSController();

    IPhoneWSController getPhoneWSController();

    ISessionWSController getSessionWSController();

    ISmscWSController getSmscWSController();

    ITestErrorWSController getTestErrorWSController();

    IValidationResultWSController getValidationResultWSController();

    IEmailWSController getEmailWSController();
}
