package cz.uhk.dip.mmsparams.server.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;

public class SendMessageUtils
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SendMessageUtils.class);
    private static final String TAG = SendMessageUtils.class.getSimpleName();

    private SendMessageUtils()
    {
    }

    public static boolean sendMessage(final WebSocketSession senderSession, final WebSocketMessageBase message)
    {
        return sendMessage(senderSession, JsonUtilsSafe.toJson(message));
    }

    public static boolean sendMessage(final WebSocketSession senderSession, final String json)
    {
        return sendMessage(senderSession, new TextMessage(json));
    }

    private static boolean sendMessage(final WebSocketSession senderSession, final TextMessage textMessage)
    {
        try
        {
            senderSession.sendMessage(textMessage);
            return true;
        }
        catch (IOException e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "sendMessage", e);
            return false;
        }
    }
}
