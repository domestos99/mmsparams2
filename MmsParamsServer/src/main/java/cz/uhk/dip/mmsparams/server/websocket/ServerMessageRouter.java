package cz.uhk.dip.mmsparams.server.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.email.EmailReceiveMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.TestErrorMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDisconnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;

public class ServerMessageRouter
{
    private ServerMessageRouter()
    {
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerMessageRouter.class);
    private static final String TAG = ServerMessageRouter.class.getSimpleName();

    // Process message where RecipientKey is Server
    // Register client/Phone
    // Connect to SMSC; send SMSC message; disconnect
    // Connect to MMSC; send MMSC message
    public static boolean processServerRecipientKey(WebSocketSession session, IWSServicesContainer wsServicesContainer, String payload)
    {
        String messageKey = MessageUtils.getMessageKey(payload);

        if (MessageType.Register_Phone_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            RegisterPhoneMessage registerPhoneMessage = JsonUtilsSafe.fromJson(payload, RegisterPhoneMessage.class);
            boolean result = wsServicesContainer.getPhoneWSController().registerPhone(session, registerPhoneMessage);
            logOperationAfter(messageKey, result);
            return result;
        }
        else if (MessageType.Phone_List_Request_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            PhoneListRequestMessage phoneListRequestMessage = JsonUtilsSafe.fromJson(payload, PhoneListRequestMessage.class);
            boolean result = wsServicesContainer.getPhoneWSController().sendDeviceList(session, phoneListRequestMessage);
            logOperationAfter(messageKey, result);
            return result;
        }
        else if (MessageType.Register_ClientLib_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            RegisterClientLibMessage msg = JsonUtilsSafe.fromJson(payload, RegisterClientLibMessage.class);
            boolean result = wsServicesContainer.getClientLibWSController().registerLibClient(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }

        else if (MessageType.Test_Validation_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            TestValidationMessage msg = JsonUtilsSafe.fromJson(payload, TestValidationMessage.class);
            boolean result = wsServicesContainer.getValidationResultWSController().processValidationResult(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }

        else if (MessageType.Test_Error_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            TestErrorMessage msg = JsonUtilsSafe.fromJson(payload, TestErrorMessage.class);
            boolean result = wsServicesContainer.getTestErrorWSController().insertTestError(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }
        else if (MessageType.Email_Receive_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            EmailReceiveMessage msg = JsonUtilsSafe.fromJson(payload, EmailReceiveMessage.class);
            boolean result = wsServicesContainer.getEmailWSController().processEmailReceive(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }

        else if (MessageType.Locked_Phones_List_Request.equals(messageKey))
        {
            logOperationBefore(messageKey);
            LockedPhonesListRequestMessage msg = JsonUtilsSafe.fromJson(payload, LockedPhonesListRequestMessage.class);
            boolean result = wsServicesContainer.getPhoneWSController().sendLockedPhonesList(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }
        else if (MessageType.Lock_Phone_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            LockPhoneMessage msg = JsonUtilsSafe.fromJson(payload, LockPhoneMessage.class);
            boolean result = wsServicesContainer.getPhoneWSController().lockPhone(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }
        else if (MessageType.UnLock_Phone_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            UnLockPhoneMessage msg = JsonUtilsSafe.fromJson(payload, UnLockPhoneMessage.class);
            boolean result = wsServicesContainer.getPhoneWSController().unLockPhone(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }


        else if (MessageType.Smsc_Connect_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            SmscConnectMessage msg = JsonUtilsSafe.fromJson(payload, SmscConnectMessage.class);
            boolean result = wsServicesContainer.getSmscWSController().connectSmsc(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }
        else if (MessageType.Smsc_SendSms_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            SmscSendSmsMessage msg = JsonUtilsSafe.fromJson(payload, SmscSendSmsMessage.class);
            boolean result = wsServicesContainer.getSmscWSController().sendSmscMessage(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }
        else if (MessageType.Smsc_Disconnect_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            SmscDisconnectMessage msg = JsonUtilsSafe.fromJson(payload, SmscDisconnectMessage.class);
            boolean result = wsServicesContainer.getSmscWSController().disconnectSmsc(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }


        else if (MessageType.Mmsc_Send_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            MmscSendMessage msg = JsonUtilsSafe.fromJson(payload, MmscSendMessage.class);
            boolean result = wsServicesContainer.getMmscWSController().sendMmsc(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }
        else if (MessageType.Mmsc_AcquireRoute_Request_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            MmscAcquireRouteRequestMessage msg = JsonUtilsSafe.fromJson(payload, MmscAcquireRouteRequestMessage.class);
            boolean result = wsServicesContainer.getMmscWSController().mmscAcquireRouterPort(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }
        else if (MessageType.Test_Result_Message.equals(messageKey))
        {
            logOperationBefore(messageKey);
            TestResultMessage msg = JsonUtilsSafe.fromJson(payload, TestResultMessage.class);
            boolean result = wsServicesContainer.getClientLibWSController().testResultProcess(session, msg);
            logOperationAfter(messageKey, result);
            return result;
        }
        else if (MessageType.Keep_Alive_Message.equals(messageKey))
        {
            return true;
        }

        ServerLogFacade.logWarning(LOGGER, TAG, "Message not processed!! " + payload);
        // Send MMSC message
        return false;
    }

    private static void logOperationBefore(String messageKey)
    {
        ServerLogFacade.logInfo(LOGGER, TAG, "Processing message with key: " + messageKey);
    }

    private static void logOperationAfter(String messageKey, boolean result)
    {
        if (result)
        {
            ServerLogFacade.logInfo(LOGGER, TAG, "Processed message with key: " + messageKey + " result: " + result);
        }
        else
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "Finished processing message with key: " + messageKey + " result: " + result);
        }
    }

    public static boolean processBroadcastToClients(final WebSocketSession session, final IWSServicesContainer wsServicesContainer, final String payload)
    {
        WebSocketMessageBase msg = MessageUtils.getMessageNonTyped(payload);
        ServerLogFacade.logInfo(LOGGER, TAG, "Broadcasting message with key: " + msg.getMessageKey());
        boolean result = wsServicesContainer.getBroadcastWSController().processBroadcastToClients(session, msg);

        if (result)
        {
            ServerLogFacade.logInfo(LOGGER, TAG, "Message broadcasted with key: " + msg.getMessageKey() + " result: " + result);
        }
        else
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "Finished broadcasting message with key: " + msg.getMessageKey() + " result: " + result);
        }

        return result;
    }

    public static boolean processForwardMessage(WebSocketSession session, IWSServicesContainer wsServicesContainer, String payload)
    {
        WebSocketMessageBase msg = MessageUtils.getMessageNonTyped(payload);
        ServerLogFacade.logInfo(LOGGER, TAG, "Forwarding message with key: " + msg.getMessageKey());
        boolean result = wsServicesContainer.getForwardWSController().processForwardMessage(session, msg);

        if (result)
        {
            ServerLogFacade.logInfo(LOGGER, TAG, "Message forwarded with key: " + msg.getMessageKey() + " result: " + result);
        }
        else
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "Finished forwarding message with key: " + msg.getMessageKey() + " result: " + result);
        }

        return result;
    }


}
