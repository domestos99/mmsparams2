package cz.uhk.dip.mmsparams.server.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketConstants;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.logging.WebSocketLogger;

@Service
public class SimpleServerWebSocketHandler extends TextWebSocketHandler
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleServerWebSocketHandler.class);
    private static final String TAG = SimpleServerWebSocketHandler.class.getSimpleName();

    private final IWSServicesContainer wsServicesContainer;
    private final WebSocketLogger webSocketLogger;

    @Autowired
    public SimpleServerWebSocketHandler(IWSServicesContainer wsServicesContainer, WebSocketLogger webSocketLogger)
    {
        this.wsServicesContainer = wsServicesContainer;
        this.webSocketLogger = webSocketLogger;
    }

    @Override
    public synchronized void afterConnectionEstablished(WebSocketSession session)
    {
        webSocketLogger.logConnectionEstablished(session);
        wsServicesContainer.getSessionWSController().addSession(session);
    }

    @Override
    protected synchronized void handleTextMessage(WebSocketSession session, TextMessage message)
    {
        try
        {
            final String payload = message.getPayload();

            if (StringUtil.isEmptyOrNull(payload))
            {
                ServerLogFacade.logWarning(LOGGER, TAG, "Payload is null or empty!!!");
                return;
            }

            final EmptyMessage emptyMessage = JsonUtilsSafe.fromJson(payload, EmptyMessage.class);

            if (!MessageType.Keep_Alive_Message.equals(emptyMessage.getMessageKey()))
            {
                // Unknown message type
                if (!MessageUtils.isMessageValid(emptyMessage))
                {
                    webSocketLogger.logUnknownMessage(session, message);
                    return;
                }
                webSocketLogger.logTextMessage(session, message);
            }

            // Dont know recipient - send based on test
            if (WebSocketConstants.Server_To_Client_Broadcast.equals(emptyMessage.getRecipientKey()))
            {
                // Send to ClientLib in test context
                boolean wasOk = ServerMessageRouter.processBroadcastToClients(session, wsServicesContainer, payload);

                if (!wasOk)
                {
                    ServerLogFacade.logWarning(LOGGER, TAG, "Probably wrong configuration for: " + payload);
                }
                return;
            }
            else if (WebSocketConstants.Server_Recipient_Key.equals(emptyMessage.getRecipientKey()))
            {
                // Message for server to process
                boolean wasOk = ServerMessageRouter.processServerRecipientKey(session, wsServicesContainer, payload);

                if (!wasOk)
                {
                    ServerLogFacade.logWarning(LOGGER, TAG, "Probably wrong configuration for: " + payload);
                }
                return;
            }
            else
            {
                // Forward message to specific endpoint - must be in test context
                boolean wasOk = ServerMessageRouter.processForwardMessage(session, wsServicesContainer, payload);

                if (!wasOk)
                {
                    ServerLogFacade.logWarning(LOGGER, TAG, "Probably wrong configuration for: " + payload);
                }
                return;
            }
        }
        catch (Exception ex)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "all block", ex);
        }
    }


    @Override
    public synchronized void handleTransportError(WebSocketSession session, Throwable exception) throws Exception
    {
        webSocketLogger.logTransportError(session, exception);
        super.handleTransportError(session, exception);
    }

    @Override
    public synchronized void afterConnectionClosed(WebSocketSession session, CloseStatus status)
    {
        webSocketLogger.logConnectionClosed(session);
        wsServicesContainer.getSessionWSController().removeSession(session.getId());
    }
}
