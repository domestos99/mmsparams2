package cz.uhk.dip.mmsparams.server.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IBroadcastWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IClientLibWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IEmailWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IForwardWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IMmscWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IPhoneWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISessionWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISmscWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ITestErrorWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IValidationResultWSController;

@Service
public class WSServicesContainer implements IWSServicesContainer
{
    private final IBroadcastWSController broadcastWSController;
    private final IClientLibWSController clientLibWSController;
    private final IForwardWSController forwardWSController;
    private final IMmscWSController mmscWSController;
    private final IPhoneWSController phoneWSController;
    private final ISessionWSController sessionWSController;
    private final ISmscWSController smscWSController;
    private final ITestErrorWSController testErrorWSController;
    private final IValidationResultWSController validationResultWSController;
    private final IEmailWSController emailWSController;


    @Autowired
    public WSServicesContainer(IBroadcastWSController broadcastWSController, IClientLibWSController clientLibWSController, IForwardWSController forwardWSController, IMmscWSController mmscWSController, IPhoneWSController phoneWSController, ISessionWSController sessionWSController, ISmscWSController smscWSController, ITestErrorWSController testErrorWSController, IValidationResultWSController validationResultWSController, IEmailWSController emailWSController)
    {
        this.broadcastWSController = broadcastWSController;
        this.clientLibWSController = clientLibWSController;
        this.forwardWSController = forwardWSController;
        this.mmscWSController = mmscWSController;
        this.phoneWSController = phoneWSController;
        this.sessionWSController = sessionWSController;
        this.smscWSController = smscWSController;
        this.testErrorWSController = testErrorWSController;
        this.validationResultWSController = validationResultWSController;
        this.emailWSController = emailWSController;
    }

    public IBroadcastWSController getBroadcastWSController()
    {
        return broadcastWSController;
    }

    public IClientLibWSController getClientLibWSController()
    {
        return clientLibWSController;
    }

    public IForwardWSController getForwardWSController()
    {
        return forwardWSController;
    }

    public IMmscWSController getMmscWSController()
    {
        return mmscWSController;
    }

    public IPhoneWSController getPhoneWSController()
    {
        return phoneWSController;
    }

    public ISessionWSController getSessionWSController()
    {
        return sessionWSController;
    }

    public ISmscWSController getSmscWSController()
    {
        return smscWSController;
    }

    public ITestErrorWSController getTestErrorWSController()
    {
        return testErrorWSController;
    }

    public IValidationResultWSController getValidationResultWSController()
    {
        return validationResultWSController;
    }

    @Override
    public IEmailWSController getEmailWSController()
    {
        return emailWSController;
    }
}
