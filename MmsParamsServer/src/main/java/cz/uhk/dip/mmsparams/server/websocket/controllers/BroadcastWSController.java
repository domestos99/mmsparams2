package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.exceptions.TestErrorException;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;
import cz.uhk.dip.mmsparams.server.utils.MessageUtilsServer;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IBroadcastWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IForwardWSController;

@Service
public class BroadcastWSController extends WSControllerBase implements IBroadcastWSController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(BroadcastWSController.class);
    private static final String TAG = BroadcastWSController.class.getSimpleName();

    private final IForwardWSController iForwardWSController;

    @Autowired
    public BroadcastWSController(WSContainer wsContainer, IForwardWSController iForwardWSController)
    {
        super(wsContainer);
        this.iForwardWSController = iForwardWSController;
    }

    @Override
    public synchronized boolean processBroadcastToClients(final WebSocketSession senderSession, final WebSocketMessageBase messageToBroadcast)
    {
        // Can get test ID
        String testID = messageToBroadcast.getTestID();

        if (!StringUtil.isEmptyOrNull(testID))
        {
            // Pokud ma zprava k preposlani vyplnen TestID
            ServerTestInstance stInstance = getTestRegister().getTestInstance(testID);

            if (stInstance == null || stInstance.isClosed())
            {
                // Test nebyl zalozen nebo jiz byl uzavren
                // Test not found or test is closed
                ServerLogFacade.logWarning(LOGGER, TAG, "Attempted to broadcast message to closed test: " + messageToBroadcast);
                final String errMsg = "Attempted to broadcast message to closed test";
                sendError(senderSession, TAG, errMsg, new TestErrorException(errMsg), messageToBroadcast);
                return false;
            }
            else
            {
                // Nalezen test, kam zpravu preposlat
                MessageUtilsServer.changeRecipientKey(messageToBroadcast, stInstance);
                this.iForwardWSController.processForwardMessage(senderSession, messageToBroadcast);
                return true;
            }
        }

        // Neni vyplneno TestID - bud chyba nebo neznama zprava z telefonu
        // Nalezt test podle zamceneho telefonu
        ServerTestInstance stInstance = getTestRegister().getTestByPhoneSenderKey(messageToBroadcast.getSenderKey());

        if (stInstance != null)
        {
            MessageUtilsServer.changeRecipientKey(messageToBroadcast, stInstance);
            this.iForwardWSController.processForwardMessage(senderSession, messageToBroadcast);

            return true;
        }

        ServerLogFacade.logWarning(LOGGER, TAG, "Unable to broadcast message to client: " + messageToBroadcast);
        final String errMsg = "Unable to broadcast message to client";
        sendError(senderSession, TAG, errMsg, new TestErrorException(errMsg), messageToBroadcast);
        return false;
    }

    @Override
    public synchronized boolean processBroadcastClientFromServer(final WebSocketMessageBase message, final String testId)
    {
        ServerTestInstance testInstance = getTestRegister().getTestInstance(testId);

        if (testInstance == null)
        {
            // Test not found or test is closed
            ServerLogFacade.logWarning(LOGGER, TAG, "Test not found: " + testId);
            return false;
        }
        else
        {
            MessageUtilsServer.changeRecipientKey(message, testInstance);
            this.iForwardWSController.processForwardMessage(testInstance.getClientSession(), message);
            return true;
        }
    }

}
