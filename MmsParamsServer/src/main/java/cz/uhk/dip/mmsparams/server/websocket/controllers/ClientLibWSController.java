package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;

import javax.annotation.Nullable;

import cz.uhk.dip.mmsparams.api.constants.AppVersion;
import cz.uhk.dip.mmsparams.api.exceptions.SystemException;
import cz.uhk.dip.mmsparams.api.exceptions.TestInvalidException;
import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.tests.ClientLibInstance;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;
import cz.uhk.dip.mmsparams.server.validation.WebSocketSessionValidator;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IClientLibWSController;

@Service
public class ClientLibWSController extends WSControllerBase implements IClientLibWSController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientLibWSController.class);
    private static final String TAG = ClientLibWSController.class.getSimpleName();

    @Autowired
    public ClientLibWSController(WSContainer wsContainer)
    {
        super(wsContainer);
    }

    @Override
    public synchronized boolean registerLibClient(final WebSocketSession session, final RegisterClientLibMessage registerClientLibMessage)
    {
        WebSocketSessionValidator.validate(session);

        // register new test
        boolean registerResult = getTestRegister().registerNewTest(session, registerClientLibMessage);

        if (!registerResult)
        {
            final String errorMsg = "Unable register new test";
            ServerLogFacade.logWarningMsg(LOGGER, TAG, errorMsg, registerClientLibMessage);
            TestInvalidException testInvalidException = new TestInvalidException(errorMsg);
            sendError(session, TAG, errorMsg, testInvalidException, registerClientLibMessage);
            return false;
        }

        getTestRegister().addTestMessage(session, registerClientLibMessage);

        if (AppVersion.SYSTEM_BUILD.equals(registerClientLibMessage.getSystemBuildVersion()))
        {
            sendGenericOK(session, registerClientLibMessage);
            return true;
        }
        else
        {
            final String errorMsg = "AppVersion.SYSTEM_BUILD are different!!! Server: " + AppVersion.SYSTEM_BUILD +
                    "; Client: " + registerClientLibMessage.getSystemBuildVersion();
            SystemException ex = new SystemException(errorMsg);
            sendError(session, TAG, errorMsg, ex, registerClientLibMessage);
            return false;
        }
    }

    @Override
    public boolean testResultProcess(WebSocketSession session, TestResultMessage msg)
    {
        try
        {
            beforeProcessMessage(session, msg);

            boolean result = getTestRegister().processTestResult(msg);
            if (result)
            {
                return sendGenericOK(session, msg);
            }
            else
            {
                final String errorMsg = "Unable to process test result";
                SystemException ex = new SystemException(errorMsg);
                sendError(session, TAG, errorMsg, ex, msg);
                return false;
            }
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "testResultProcess", e);
            sendError(session, TAG, "testResultProcess", ExceptionHelper.castException(e), msg);
            return false;
        }
    }

    @Override
    public synchronized List<ClientInfo> getConnectedClients()
    {
        return getTestRegister().getConnectedClients();
    }

    @Override
    @Nullable
    public synchronized ClientLibInstance getConnectedClient(String clientKey)
    {
        return getTestRegister().getClientInfoByClientKey(clientKey);
    }

    @Override
    public synchronized boolean forceDisconnectClient(String clientKey)
    {
        boolean result = false;

        if (StringUtil.isEmptyOrNull(clientKey))
            return result;

        for (ServerTestInstance test : getTestRegister().getAllOpenTests())
        {
            if (clientKey.equals(test.getClientLibKey()))
            {
                if (!test.isClosed())
                {
                    TestErrorModel err = new TestErrorModel();
                    err.setTestErrorType(TestErrorType.TEST_FORCE_CLOSE);
                    err.setMessage("Client force disconnect from web");
                    test.insertTestError(err);
                }
                try
                {
                    test.getClientSession().close(CloseStatus.GOING_AWAY);
                    result = true;
                }
                catch (Exception e)
                {
                    ServerLogFacade.logEx(LOGGER, TAG, "forceDisconnectClient", e);
                    result = false;
                }
//                iSessionWSController.removeSession(test.getClientSessionId());
            }
        }

        return result;
    }


}
