package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.websocket.messages.email.EmailReceiveMessage;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IEmailWSController;

@Service
public class EmailWSController extends WSControllerBase implements IEmailWSController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailWSController.class);
    private static final String TAG = EmailWSController.class.getSimpleName();

    @Autowired
    public EmailWSController(WSContainer wsContainer)
    {
        super(wsContainer);
    }

    @Override
    public boolean processEmailReceive(WebSocketSession session, EmailReceiveMessage msg)
    {
        try
        {
            beforeProcessMessage(session, msg);

            // Do nothing

            sendGenericOK(session, msg);
            return true;
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "processEmailReceive", e);
            sendError(session, TAG, "processEmailReceive", ExceptionHelper.castException(e), msg);
            return false;
        }
    }
}
