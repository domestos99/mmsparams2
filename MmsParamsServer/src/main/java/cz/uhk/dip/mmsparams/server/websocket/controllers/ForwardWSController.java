package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.util.Map;

import cz.uhk.dip.mmsparams.api.exceptions.TestErrorException;
import cz.uhk.dip.mmsparams.api.http.TaskResult;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IForwardWSController;

@Service
public class ForwardWSController extends WSControllerBase implements IForwardWSController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ForwardWSController.class);
    private static final String TAG = ForwardWSController.class.getSimpleName();


    @Autowired
    public ForwardWSController(WSContainer wsContainer)
    {
        super(wsContainer);
    }

    @Override
    // TODO refactor and check?
    public synchronized boolean processForwardMessage(final WebSocketSession session, final WebSocketMessageBase message)
    {
        // Check can forward
        TaskResult<Boolean> canForward = getTestRegister().canForward(session, message);

        if (canForward.hasError())
        {
            // Error
            final String errMsg = "Unable to forward message: " + canForward.getError().getMessage();
            TestErrorException ex = new TestErrorException(errMsg);
            sendError(session, TAG, errMsg, ex, message);
            return false;
        }
        // Add to TestRegister
        getTestRegister().addTestMessage(session, message);

        // Forward
        return forwardMessage(session, message);
    }

    private synchronized boolean forwardMessage(final WebSocketSession senderSession, final WebSocketMessageBase message)
    {
        for (Map.Entry<String, PhoneInfoModel> a : getPhones().entrySet())
        {
            if (a.getValue().getPhoneKey().equals(message.getRecipientKey()))
            {
                WebSocketSession ses = getSessions().get(a.getKey());
                sendOutgoingMessage(ses, message);
                return true;
            }
        }

        WebSocketSession recipientSession = getTestRegister().getSessionByRecipientKey(message.getTestID(), message.getRecipientKey());

        if (recipientSession != null)
        {
            sendOutgoingMessage(recipientSession, message);
            return true;
        }

        // Error - unable to forward message
        final String errMsg = "Unable to forward message: " + message;
        TestErrorException exception = new TestErrorException(errMsg);
        sendError(senderSession, TAG, errMsg, exception, message);
        return false;
    }

}
