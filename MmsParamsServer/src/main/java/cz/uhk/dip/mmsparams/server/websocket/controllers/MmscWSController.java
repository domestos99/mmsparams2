package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;

import cz.uhk.dip.mmsparams.api.exceptions.MmscException;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MM7SubmitResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.mmsc.IMmscServiceFacade;
import cz.uhk.dip.mmsparams.server.mmsc.MmscOnReceiveListener;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IForwardWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IMmscWSController;

@Service
public class MmscWSController extends WSControllerBase implements IMmscWSController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MmscWSController.class);
    private static final String TAG = MmscWSController.class.getSimpleName();

    private final IMmscServiceFacade mmscServiceFacade;
    private final IForwardWSController iForwardWSController;

    @Autowired
    public MmscWSController(WSContainer wsContainer, IMmscServiceFacade mmscServiceFacade, IForwardWSController iForwardWSController)
    {
        super(wsContainer);
        this.mmscServiceFacade = mmscServiceFacade;
        this.iForwardWSController = iForwardWSController;
        this.mmscServiceFacade.setOnReceiveListener(new MmscOnReceiveListener(this));
    }

    @Override
    public boolean sendMmsc(WebSocketSession session, MmscSendMessage msg)
    {
        try
        {
            beforeProcessMessage(session, msg);

            MM7SubmitResponseModel resp = mmscServiceFacade.send(msg);

            MM7SubmitResponseMessage response = MessageFactory.createServerSenderResponse(null, MM7SubmitResponseMessage.class, msg);

            response.setMm7SubmitResponseModel(resp);

            sendOutgoingMessage(session, response);
            return true;
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "sendMmsc", e);
            sendError(session, TAG, "sendMmsc", new MmscException("sendMmsc", e), msg);
            return false;
        }
    }

    @Override
    public boolean mmscAcquireRouterPort(WebSocketSession session, MmscAcquireRouteRequestMessage msg)
    {
        try
        {
            beforeProcessMessage(session, msg);

            ServerTestInstance testInst = getTestRegister().getTestInstance(msg.getTestID());

            if (testInst == null)
            {
                final String errMsg = "mmscAcquireRouterPort: ServerTestInstance not found fror testId: " + msg.getTestID();
                sendError(session, TAG, errMsg, new MmscException(errMsg), msg);
                return false;
            }

            testInst.addNewMmscAcquirePort(msg.getMmscAcquireRouteModel());

            sendGenericOK(session, msg);
            return true;
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "mmscAcquireRouterPort", e);
            sendError(session, TAG, "mmscAcquireRouterPort", new MmscException("mmscAcquireRouterPort", e), msg);
            return false;
        }
    }

    @Override
    public boolean processMmscBroadcastToClientFromServer(WebSocketMessageBase message, List<MM7Address> recipients)
    {
        // Find test by recipient acquire address
        List<ServerTestInstance> tests = getTestRegister().getTestsByMmscRecipients(recipients);

        if (tests == null)
            return false;

        // TODO - odesílat všem nebo udělat nějaký zámek na MM7 route?
        for (ServerTestInstance st : tests)
        {
            String recKey = st.getClientInfo().getClientKey();
            message.setRecipientKey(recKey);
            message.setTestID(st.getTestID());
            this.iForwardWSController.processForwardMessage(st.getClientSession(), message);
        }

        return true;
    }


}
