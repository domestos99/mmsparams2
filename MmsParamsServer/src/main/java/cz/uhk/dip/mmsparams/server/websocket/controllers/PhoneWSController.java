package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.enums.PhoneListRequestFilter;
import cz.uhk.dip.mmsparams.api.exceptions.TestErrorException;
import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.utils.Preconditions;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.server.tests.PhoneInstance;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;
import cz.uhk.dip.mmsparams.server.utils.StreamUtils;
import cz.uhk.dip.mmsparams.server.validation.WebSocketSessionValidator;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IPhoneWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISessionWSController;
import io.micrometer.core.lang.Nullable;

@Service
public class PhoneWSController extends WSControllerBase implements IPhoneWSController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(PhoneWSController.class);
    private static final String TAG = PhoneWSController.class.getSimpleName();

    private final ISessionWSController iSessionWSController;

    @Autowired
    public PhoneWSController(WSContainer wsContainer, ISessionWSController iSessionWSController)
    {
        super(wsContainer);
        this.iSessionWSController = iSessionWSController;
    }

    @Override
    public synchronized boolean registerPhone(final WebSocketSession session, final RegisterPhoneMessage registerPhoneMessage)
    {
        WebSocketSessionValidator.validate(session);
        Preconditions.checkNotNull(registerPhoneMessage, GenericConstants.MESSAGE);
        Preconditions.checkNotNull(registerPhoneMessage.getPhoneInfoModel(), GenericConstants.PHONE_INFO_MODEL);
        Preconditions.checkNotNullOrEmpty(registerPhoneMessage.getPhoneInfoModel().getPhoneKey(), GenericConstants.PHONE_KEY);

        String sessionID = session.getId();
        if (getPhones().containsKey(sessionID))
        {
            getPhones().remove(sessionID);
        }
        getPhones().put(sessionID, registerPhoneMessage.getPhoneInfoModel());
        return sendGenericOK(session, registerPhoneMessage);
    }

    @Override
    public synchronized boolean sendDeviceList(final WebSocketSession session, final PhoneListRequestMessage phoneListRequestMessage)
    {
        beforeProcessMessage(session, phoneListRequestMessage);

        // TODO check filter ???
        PhoneListResponseMessage phoneListResponseMessage = MessageFactory.createServerSenderResponse(null, PhoneListResponseMessage.class, phoneListRequestMessage);
        phoneListResponseMessage.setPhoneInfoModels(getPhonesWithFilter(phoneListRequestMessage.getTestID(), phoneListRequestMessage.getFilter()));

        return sendOutgoingMessage(session, phoneListResponseMessage);
    }

    @Override
    public ArrayList<PhoneInfoModel> getPhonesWithFilter(String testId, PhoneListRequestFilter filter)
    {
        // TODO
        return ListUtils.toArrayList(getConnectedPhones());
    }

    @Override
    public synchronized List<PhoneInfoModel> getConnectedPhones()
    {
        return ListUtils.toList(getPhones().values());
    }

    @Override
    public synchronized WebSocketSession getPhoneSession(String phoneKey)
    {
        if (StringUtil.isEmptyOrNull(phoneKey))
            return null;

        for (Map.Entry<String, PhoneInfoModel> i : getPhones().entrySet())
        {
            if (phoneKey.equals(i.getValue().getPhoneKey()))
            {
                String sessionId = i.getKey();
                return getSessions().get(sessionId);
            }
        }
        return null;
    }

    @Override
    public synchronized boolean lockPhone(final WebSocketSession session, final LockPhoneMessage msg)
    {
        beforeProcessMessage(session, msg);
        WebSocketSession phoneSession = getPhoneSession(msg.getPhoneInfoModel().getPhoneKey());

        if (phoneSession == null)
        {
            final String errMsg = "Unable to lock phone (phone session not found)";
            sendError(session, TAG, errMsg, new TestErrorException(errMsg), msg);
            return false;
        }

        boolean locked = getTestRegister().lockPhone(phoneSession, msg);
        if (locked)
        {
            sendGenericOK(session, msg);
            return true;
        }
        else
        {
            // Error
            final String errMsg = "Unable to lock phone";
            sendError(session, TAG, errMsg, new TestErrorException(errMsg), msg);
            return false;
        }
    }

    @Override
    public synchronized boolean unLockPhone(final WebSocketSession session, final UnLockPhoneMessage msg)
    {
        beforeProcessMessage(session, msg);
        boolean unLocked = getTestRegister().unLockPhone(msg);
        if (unLocked)
        {
            sendGenericOK(session, msg);
            return true;
        }
        else
        {
            // Error
            final String errMsg = "Unable to unlock phone";
            sendError(session, TAG, errMsg, new TestErrorException(errMsg), msg);
            return false;
        }
    }

    @Override
    public boolean forceDisconnectPhone(String phoneKey) throws IOException
    {
        if (StringUtil.isEmptyOrNull(phoneKey))
            return false;

        boolean result = false;
        for (Map.Entry<String, PhoneInfoModel> pi : getPhones().entrySet())
        {
            if (pi.getValue().getPhoneKey().equals(phoneKey))
            {
                WebSocketSession session = getSessions().get(pi.getKey());
                session.close(CloseStatus.NORMAL);
                iSessionWSController.removeSession(pi.getKey());
                result = true;
            }
        }
        return result;
    }

    @Nullable
    @Override
    public PhoneInfoModel getConnectedPhone(String phoneKey)
    {
        if (StringUtil.isEmptyOrNull(phoneKey))
            return null;

        for (PhoneInfoModel i : getPhones().values())
        {
            if (i.getPhoneKey().equals(phoneKey))
                return i;
        }
        return null;
    }

    @Override
    public boolean sendLockedPhonesList(WebSocketSession session, LockedPhonesListRequestMessage msg)
    {
        beforeProcessMessage(session, msg);

        ServerTestInstance test = getTestRegister().getTestInstance(msg.getTestID());

        if (test == null)
        {
            return false;
        }

        List<PhoneInstance> a = test.getPhoneInstanceList();

        ArrayList<PhoneInfoModel> b = ListUtils.toArrayList(StreamUtils.toList(a.stream().map(x -> x.getPhoneInfoModel())));

        LockedPhonesListResponseMessage responseMessage = MessageFactory.createServerSenderResponse(LockedPhonesListResponseMessage.class, msg);
        responseMessage.setPhoneInfoModels(ListUtils.toArrayList(b));

        return sendOutgoingMessage(session, responseMessage);
    }


}
