package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;

import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.validation.WebSocketSessionValidator;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISessionWSController;

@Service
public class SessionWSController extends WSControllerBase implements ISessionWSController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SessionWSController.class);
    private static final String TAG = SessionWSController.class.getSimpleName();

    public SessionWSController(WSContainer wsContainer)
    {
        super(wsContainer);
    }

    @Override
    public synchronized void addSession(final WebSocketSession session)
    {
        WebSocketSessionValidator.validate(session);
        ServerLogFacade.logInfo(LOGGER, TAG, "Adding session: " + session.getId());
        getSessions().put(session.getId(), session);
    }

    @Override
    public WebSocketSession getSessionById(final String sessionId)
    {
        return getSessions().get(sessionId);
    }

    @Override
    public synchronized void removeSession(final String id)
    {
        ServerLogFacade.logInfo(LOGGER, TAG, "Removing session: " + id);

        getSessions().remove(id);

        if (getPhones().containsKey(id))
            getPhones().remove(id);

        getTestRegister().removeSessionIfExists(id);
    }

    @Override
    public synchronized List<WebSocketSession> getOpenSessions()
    {
        return ListUtils.toList(getSessions().values());
    }
}
