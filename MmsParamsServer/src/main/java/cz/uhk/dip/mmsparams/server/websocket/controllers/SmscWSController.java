package cz.uhk.dip.mmsparams.server.websocket.controllers;

import com.cloudhopper.smpp.SmppSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.exceptions.SmscException;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.MessageIdGenerator;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDisconnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.smsc.ISmscServiceFacade;
import cz.uhk.dip.mmsparams.server.smsc.SmscReceiveListener;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IBroadcastWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISmscWSController;
import cz.uhk.dip.mmsparams.smsc.utils.SmscConverterIn;

@Service
public class SmscWSController extends WSControllerBase implements ISmscWSController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SmscWSController.class);
    private static final String TAG = SmscWSController.class.getSimpleName();

    private final ISmscServiceFacade smscServiceFacade;
    private final IBroadcastWSController iBroadcastWSController;

    @Autowired
    public SmscWSController(WSContainer wsContainer, ISmscServiceFacade smscServiceFacade, IBroadcastWSController iBroadcastWSController)
    {
        super(wsContainer);
        this.smscServiceFacade = smscServiceFacade;
        this.iBroadcastWSController = iBroadcastWSController;
    }

    @Override
    public boolean connectSmsc(WebSocketSession session, SmscConnectMessage msg)
    {
        beforeProcessMessage(session, msg);

        SmscSessionId smscSessionId = new SmscSessionId(MessageIdGenerator.getNext());
        SmscReceiveListener receiveListener = new SmscReceiveListener(this, msg.getTestID(), smscSessionId);

        try
        {
            final SmppSession smppSession = smscServiceFacade.connect(msg, receiveListener);

            SmscSessionId sessionId = getTestRegister().addNewSmscConnection(msg.getTestID(), smscSessionId, smppSession);

            SmscConnectResponseMessage response = MessageFactory.createServerSenderResponse(null, SmscConnectResponseMessage.class, msg);

            SmscConnectResponseModel m = new SmscConnectResponseModel();
            m.setStatus(true);
            m.setSessionId(sessionId);
            m.setSmscSessionModel(SmscConverterIn.getSmscSessionModel(smppSession));
            response.setSmscConnectResponseModel(m);

            return sendOutgoingMessage(session, response);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "connectSmsc", e);
            SmscException ex = new SmscException("connectSmsc", e);
            sendError(session, TAG, "connectSmsc", ex, msg);
            return false;
        }
    }

    @Override
    public boolean disconnectSmsc(final WebSocketSession session, final SmscDisconnectMessage msg)
    {
        try
        {
            beforeProcessMessage(session, msg);

            SmppSession smscSession = getTestRegister().getSmscSession(msg.getTestID(), msg.getSmscSessionId());
            smscServiceFacade.disconnect(smscSession);
            getTestRegister().removeSmscSession(msg.getTestID(), msg.getSmscSessionId());

            // Send OK
            return sendGenericOK(session, msg);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "disconnectSmsc", e);
            SmscException ex = new SmscException("disconnectSmsc", e);
            sendError(session, TAG, "disconnectSmsc", ex, msg);
            return false;
        }
    }

    @Override
    public boolean sendSmscMessage(WebSocketSession session, SmscSendSmsMessage msg)
    {
        try
        {
            beforeProcessMessage(session, msg);

            SmppSession smscSession = getTestRegister().getSmscSession(msg.getTestID(), msg.getSmscSessionId());

            SmscSendSmsResponseModel responseModel = smscServiceFacade.send(smscSession, msg);

            SmscSendSmsResponseMessage response = MessageFactory.createServerSenderResponse(null, SmscSendSmsResponseMessage.class, msg);
            response.setSmscSendSmsResponseModel(responseModel);

            return sendOutgoingMessage(session, response);
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "sendSmscMessage", e);
            SmscException ex = new SmscException("sendSmscMessage", e);
            sendError(session, TAG, "sendSmscMessage", ex, msg);
            return false;
        }
    }

    @Override
    public void processSmscBroadcastToClientFromServer(SmscMessageBase msg, String testID)
    {
        iBroadcastWSController.processBroadcastClientFromServer(msg, testID);
    }


}
