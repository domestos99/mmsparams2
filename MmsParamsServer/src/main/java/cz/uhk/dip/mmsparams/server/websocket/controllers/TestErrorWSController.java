package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.TestErrorMessage;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ITestErrorWSController;

@Service
public class TestErrorWSController extends WSControllerBase implements ITestErrorWSController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TestErrorWSController.class);
    private static final String TAG = TestErrorWSController.class.getSimpleName();

    @Autowired
    public TestErrorWSController(WSContainer wsContainer)
    {
        super(wsContainer);
    }

    @Override
    public boolean insertTestError(WebSocketSession session, TestErrorMessage msg)
    {
        try
        {
            beforeProcessMessage(session, msg);

            ServerTestInstance testInst = getTestRegister().getTestInstance(msg.getTestID());

            if (testInst == null)
                return false;

            testInst.insertTestError(msg.getTestErrorModel());

            sendGenericOK(session, msg);
            return true;
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "insertTestError", e);
            sendError(session, TAG, "insertTestError", ExceptionHelper.castException(e), msg);
            return false;
        }

    }
}
