package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.exceptions.TestErrorException;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IValidationResultWSController;

@Service
public class ValidationResultWSController extends WSControllerBase implements IValidationResultWSController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationResultWSController.class);
    private static final String TAG = ValidationResultWSController.class.getSimpleName();

    @Autowired
    public ValidationResultWSController(WSContainer wsContainer)
    {
        super(wsContainer);
    }

    @Override
    public boolean processValidationResult(WebSocketSession session, TestValidationMessage msg)
    {
        try
        {
            beforeProcessMessage(session, msg);

            boolean result = getTestRegister().processValidationResult(msg);
            if (result)
            {
                return sendGenericOK(session, msg);
            }
            else
            {
                ServerLogFacade.logWarning(LOGGER, TAG, "Unable to process validation result: " + msg);
                final String errMsg = "Unable to process validation result";
                sendError(session, TAG, errMsg, new TestErrorException(errMsg), msg);
                return false;
            }
        }
        catch (Exception e)
        {
            ServerLogFacade.logEx(LOGGER, TAG, "processValidationResult", e);
            sendError(session, TAG, "processValidationResult", new TestErrorException("processValidationResult"), msg);
            return false;
        }
    }

}
