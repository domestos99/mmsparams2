package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.server.logging.WebSocketLogger;
import cz.uhk.dip.mmsparams.server.tests.TestRegister;

@Service
public class WSContainer
{
    private final WebSocketLogger webSocketLogger;

    private final ConcurrentMap<String, WebSocketSession> sessions = new ConcurrentHashMap<>();

    private final ConcurrentMap<String, PhoneInfoModel> phones = new ConcurrentHashMap<>();

    private final TestRegister testRegister;


    @Autowired
    public WSContainer(final WebSocketLogger webSocketLogger, final TestRegister testRegister)
    {
        this.webSocketLogger = webSocketLogger;
        this.testRegister = testRegister;
    }

    public WebSocketLogger getWebSocketLogger()
    {
        return webSocketLogger;
    }

    public TestRegister getTestRegister()
    {
        return testRegister;
    }

    public ConcurrentMap<String, WebSocketSession> getSessions()
    {
        return sessions;
    }

    public ConcurrentMap<String, PhoneInfoModel> getPhones()
    {
        return phones;
    }
}
