package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.WebSocketSession;

import java.util.concurrent.ConcurrentMap;

import javax.annotation.Nonnull;

import cz.uhk.dip.mmsparams.api.exceptions.MmsParamsExceptionBase;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketConstants;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.server.logging.ServerLogFacade;
import cz.uhk.dip.mmsparams.server.logging.WebSocketLogger;
import cz.uhk.dip.mmsparams.server.tests.TestRegister;
import cz.uhk.dip.mmsparams.server.validation.WebSocketSessionValidator;
import cz.uhk.dip.mmsparams.server.websocket.SendMessageUtils;

public abstract class WSControllerBase
{
    private static final Logger LOGGER = LoggerFactory.getLogger(WSControllerBase.class);
    private static final String TAG = WSControllerBase.class.getSimpleName();

    private final WSContainer wsContainer;

    @Autowired
    public WSControllerBase(WSContainer wsContainer)
    {
        this.wsContainer = wsContainer;
    }

    protected WebSocketLogger getWebSocketLogger()
    {
        return wsContainer.getWebSocketLogger();
    }

    protected TestRegister getTestRegister()
    {
        return wsContainer.getTestRegister();
    }

    public ConcurrentMap<String, WebSocketSession> getSessions()
    {
        return wsContainer.getSessions();
    }

    public ConcurrentMap<String, PhoneInfoModel> getPhones()
    {
        return wsContainer.getPhones();
    }

    protected synchronized boolean sendGenericOK(WebSocketSession session, WebSocketMessageBase msg)
    {
        GenericBooleanResponseMessage resp = MessageFactory.createServerSenderResponse(GenericBooleanResponseMessage.class, msg);
        resp.setValue(true);
        return sendOutgoingMessage(session, resp);
    }

    protected void sendError(WebSocketSession session, String classTag, String message, @Nonnull MmsParamsExceptionBase ex, WebSocketMessageBase request)
    {
        ServerLogFacade.logExMessage(LOGGER, TAG, message, ex, request);
        GenericErrorResponseMessage resp = MessageFactory.createGenericErrorResponseMessage(classTag, ex, request, message);
        resp.setSenderKey(WebSocketConstants.Server_Recipient_Key);
        sendOutgoingMessage(session, resp);
    }

    protected boolean sendOutgoingMessage(final WebSocketSession senderSession, final WebSocketMessageBase message)
    {
        if (!MessageUtils.isMessageValid(message))
        {
            ServerLogFacade.logWarning(LOGGER, TAG, "Message is not valid: " + message);
        }

        getWebSocketLogger().logSendMessage(senderSession, message);
        getTestRegister().addOutgointMessage(senderSession, message);
        return SendMessageUtils.sendMessage(senderSession, message);
    }

    protected void beforeProcessMessage(final WebSocketSession session, final WebSocketMessageBase msg)
    {
        WebSocketSessionValidator.validate(session);
        getTestRegister().addTestMessage(session, msg);
    }

}
