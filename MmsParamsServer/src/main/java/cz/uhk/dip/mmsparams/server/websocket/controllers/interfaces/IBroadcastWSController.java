package cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces;

import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public interface IBroadcastWSController
{
    boolean processBroadcastToClients(WebSocketSession session, final WebSocketMessageBase messageToBroadcast);

    boolean processBroadcastClientFromServer(WebSocketMessageBase message, String testId);
}
