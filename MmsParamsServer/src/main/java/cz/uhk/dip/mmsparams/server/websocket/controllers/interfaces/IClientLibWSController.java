package cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces;

import org.springframework.web.socket.WebSocketSession;

import java.util.List;

import javax.annotation.Nullable;

import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.TestResultMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.server.tests.ClientLibInstance;

public interface IClientLibWSController
{
    boolean registerLibClient(WebSocketSession session, RegisterClientLibMessage registerClientLibMessage);

    List<ClientInfo> getConnectedClients();

    @Nullable
    ClientLibInstance getConnectedClient(String clientKey);

    boolean forceDisconnectClient(String clientKey);

    boolean testResultProcess(WebSocketSession session, TestResultMessage msg);
}
