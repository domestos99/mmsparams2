package cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces;

import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.websocket.messages.email.EmailReceiveMessage;

public interface IEmailWSController
{
    boolean processEmailReceive(WebSocketSession session, EmailReceiveMessage msg);
}
