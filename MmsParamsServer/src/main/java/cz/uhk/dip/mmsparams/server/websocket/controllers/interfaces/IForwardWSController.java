package cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces;

import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public interface IForwardWSController
{
    boolean processForwardMessage(WebSocketSession session, WebSocketMessageBase message);
}
