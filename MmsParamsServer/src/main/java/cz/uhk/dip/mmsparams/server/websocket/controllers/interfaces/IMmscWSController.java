package cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces;

import org.springframework.web.socket.WebSocketSession;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;

public interface IMmscWSController
{
    boolean sendMmsc(WebSocketSession session, MmscSendMessage msg);

    boolean mmscAcquireRouterPort(WebSocketSession session, MmscAcquireRouteRequestMessage msg);

    default boolean processMmscBroadcastToClientFromServer(WebSocketMessageBase message, MM7Address recipients)
    {
        List<MM7Address> l = new ArrayList<>();
        l.add(recipients);
        return processMmscBroadcastToClientFromServer(message, l);
    }

    boolean processMmscBroadcastToClientFromServer(WebSocketMessageBase message, List<MM7Address> recipients);
}
