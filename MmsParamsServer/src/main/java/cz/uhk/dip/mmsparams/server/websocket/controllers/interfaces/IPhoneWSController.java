package cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces;

import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.enums.PhoneListRequestFilter;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockedPhonesListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import io.micrometer.core.lang.Nullable;

public interface IPhoneWSController
{
    boolean registerPhone(WebSocketSession session, RegisterPhoneMessage registerPhoneMessage);

    boolean sendDeviceList(WebSocketSession session, PhoneListRequestMessage phoneListRequestMessage);

    ArrayList<PhoneInfoModel> getPhonesWithFilter(String testId, PhoneListRequestFilter filter);

    List<PhoneInfoModel> getConnectedPhones();

    WebSocketSession getPhoneSession(String phoneKey);

    boolean lockPhone(WebSocketSession session, LockPhoneMessage msg);

    boolean unLockPhone(WebSocketSession session, UnLockPhoneMessage msg);

    boolean forceDisconnectPhone(String phoneKey) throws IOException;

    @Nullable
    PhoneInfoModel getConnectedPhone(String phoneKey);

    boolean sendLockedPhonesList(WebSocketSession session, LockedPhonesListRequestMessage msg);
}
