package cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces;

import org.springframework.web.socket.WebSocketSession;

import java.util.List;

public interface ISessionWSController
{
    void addSession(WebSocketSession session);

    WebSocketSession getSessionById(String sessionId);

    void removeSession(String id);

    List<WebSocketSession> getOpenSessions();
}
