package cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces;

import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDisconnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;

public interface ISmscWSController
{
    boolean connectSmsc(WebSocketSession session, SmscConnectMessage msg);

    boolean disconnectSmsc(WebSocketSession session, SmscDisconnectMessage msg);

    boolean sendSmscMessage(WebSocketSession session, SmscSendSmsMessage msg);

    void processSmscBroadcastToClientFromServer(SmscMessageBase msg, String testID);
}
