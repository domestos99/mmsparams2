package cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces;

import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.websocket.messages.errors.TestErrorMessage;

public interface ITestErrorWSController
{
    boolean insertTestError(WebSocketSession session, TestErrorMessage msg);
}
