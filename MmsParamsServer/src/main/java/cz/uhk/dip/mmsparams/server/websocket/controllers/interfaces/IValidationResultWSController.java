package cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces;

import org.springframework.web.socket.WebSocketSession;

import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;

public interface IValidationResultWSController
{
    boolean processValidationResult(WebSocketSession session, TestValidationMessage msg);
}
