package cz.uhk.dip.mmsparams.server;

import cz.uhk.dip.mmsparams.server.database.dao.ServerTestInstanceDAO;
import cz.uhk.dip.mmsparams.server.database.dao.TestGroupDAO;
import cz.uhk.dip.mmsparams.server.database.services.IClientServerLogDBService;
import cz.uhk.dip.mmsparams.server.database.services.ServerTestInstanceDBService;
import cz.uhk.dip.mmsparams.server.logging.WebSocketLogger;
import cz.uhk.dip.mmsparams.server.mocks.MockMmscServiceFacade;
import cz.uhk.dip.mmsparams.server.mocks.MockSmscServiceFacade;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.tests.TestRegister;
import cz.uhk.dip.mmsparams.server.websocket.IWSServicesContainer;
import cz.uhk.dip.mmsparams.server.websocket.WSServicesContainer;
import cz.uhk.dip.mmsparams.server.websocket.controllers.BroadcastWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.ClientLibWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.EmailWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.ForwardWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.MmscWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.PhoneWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.SessionWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.SmscWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.TestErrorWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.ValidationResultWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.WSContainer;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IBroadcastWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IClientLibWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IEmailWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IForwardWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IMmscWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IPhoneWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISessionWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISmscWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ITestErrorWSController;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IValidationResultWSController;
import io.micrometer.core.lang.Nullable;

public class UTUtils
{
    public static IWSServicesContainer createSessionManager()
    {
        return createSessionManager(getWebSocketLogger(), getTestRegister());
    }

    public static IWSServicesContainer createSessionManager(WebSocketLogger webSocketLogger, TestRegister testRegister)
    {
        WSContainer wsContainer = new WSContainer(webSocketLogger, testRegister);

        IClientLibWSController clientLibWSController = new ClientLibWSController(wsContainer);
        IForwardWSController forwardWSController = new ForwardWSController(wsContainer);
        IBroadcastWSController broadcastWSController = new BroadcastWSController(wsContainer, forwardWSController);
        IMmscWSController mmscWSController = new MmscWSController(wsContainer, new MockMmscServiceFacade(), forwardWSController);
        ISessionWSController sessionWSController = new SessionWSController(wsContainer);
        IPhoneWSController phoneWSController = new PhoneWSController(wsContainer, sessionWSController);
        ISmscWSController smscWSController = new SmscWSController(wsContainer, new MockSmscServiceFacade(), broadcastWSController);
        ITestErrorWSController testErrorWSController = new TestErrorWSController(wsContainer);
        IValidationResultWSController validationResultWSController = new ValidationResultWSController(wsContainer);
        IEmailWSController emailWSController = new EmailWSController(wsContainer);

        return new WSServicesContainer(broadcastWSController, clientLibWSController, forwardWSController,
                mmscWSController, phoneWSController, sessionWSController,
                smscWSController, testErrorWSController, validationResultWSController, emailWSController);
    }

    public static WebSocketLogger getWebSocketLogger()
    {
        return new WebSocketLogger();
    }

    public static TestRegister getTestRegister()
    {
        return new TestRegister(getTestServerTestInstanceDBService());
    }

    private static ServerTestInstanceDBService getTestServerTestInstanceDBService()
    {
        return new ServerTestInstanceDBService(getTestServerTestInstanceDAO(), getClientServerLogDAO(), null);
    }

    private static IClientServerLogDBService getClientServerLogDAO()
    {
        return null;
    }

    @Nullable
    public static ServerTestInstanceDAO getTestServerTestInstanceDAO()
    {
        return null;
    }

    public static TestGroupDAO getTestGroupDAOForUT()
    {
        return null;
    }


    public static MockWebSocketSession getNewSession()
    {
        return new MockWebSocketSession();
    }

//    public static ISmscServiceFacade getSmscService()
//    {
//        return new MockSmscServiceFacade();
//    }
}
