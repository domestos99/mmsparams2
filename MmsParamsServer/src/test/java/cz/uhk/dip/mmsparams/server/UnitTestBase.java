package cz.uhk.dip.mmsparams.server;

import org.junit.Before;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.utils.CommonUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketConstants;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.TestGroupModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.registration.TestClientInfoModel;
import cz.uhk.dip.mmsparams.server.database.services.IServerTestInstanceDBService;
import cz.uhk.dip.mmsparams.server.logging.WebSocketLogger;
import cz.uhk.dip.mmsparams.server.mock.MockServerTestInstanceDBService;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;
import cz.uhk.dip.mmsparams.server.tests.TestRegister;
import cz.uhk.dip.mmsparams.server.websocket.IWSServicesContainer;

public class UnitTestBase
{

    protected ITestIdProvider iTestIdProvider;

    @Before
    public void testBeforeBase()
    {
        iTestIdProvider = new ITestIdProvider()
        {
            @Override
            public String getTestId()
            {
                return "pipeline-test-id";
            }
        };
    }

    protected IWSServicesContainer createSessionManager()
    {
        return UTUtils.createSessionManager();
    }

    protected IWSServicesContainer createSessionManager(WebSocketLogger webSocketLogger, TestRegister testRegister)
    {
        return UTUtils.createSessionManager(webSocketLogger, testRegister);
    }

    protected MockWebSocketSession getNewSession()
    {
        return UTUtils.getNewSession();
    }

    protected TestGroupModel getTestGroupForUT()
    {
        TestGroupModel group = new TestGroupModel();
        group.setTestGroupID(CommonUtils.getUUID());
        group.setTestGroupName("UT Test group");
        group.setTestGroupDesc("UT Test group desc");
        return group;
    }

    protected String getUUID()
    {
        return CommonUtils.getUUID();
    }

    protected IServerTestInstanceDBService getTestServerTestInstanceDBService()
    {
        return new MockServerTestInstanceDBService();
    }

    protected ClientInfo getClientInfoForTest()
    {
        return getClientInfoForTest(1);
    }

    protected ClientInfo getClientInfoForTest(int index)
    {
        ClientInfo clientInfo = new ClientInfo();
        clientInfo.setClientKey(GenericConstants.CLIENT_KEY + index);
        clientInfo.setComputerName("computername");
        clientInfo.setCurrentUserName("username");
        return clientInfo;
    }

    protected TestClientInfoModel getTestClientInfoForTest()
    {
        TestClientInfoModel t = new TestClientInfoModel();
        t.setTestName("test");
        return t;
    }

    protected ITestIdProvider getUTTestProvider(int testIndex)
    {
        return new ITestIdProvider()
        {
            @Override
            public String getTestId()
            {
                return GenericConstants.TEST_ID + testIndex;
            }
        };
    }

    protected PhoneInfoModel getPhoneForUT()
    {
        return getPhoneForUT(1);
    }

    protected PhoneInfoModel getPhoneForUT(int index)
    {
        PhoneInfoModel phone = new PhoneInfoModel();
        phone.setPhoneKey(WebSocketConstants.Android_Key_Prefix +  GenericConstants.PHONE_KEY + index);
        return phone;
    }

    protected ServerTestInstance getTestServerTestInstance()
    {
        return ServerTestInstance.createNewTest(getUUID(), getTestGroupForUT(), getNewSession(), getTestClientInfoModelForTest(), getClientInfoForTest(), null);
    }

    private TestClientInfoModel getTestClientInfoModelForTest()
    {
        TestClientInfoModel ci = new TestClientInfoModel();
        ci.setTestName("test");
        ci.setTestDesc("test desc");
        return ci;
    }


}
