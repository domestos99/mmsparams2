package cz.uhk.dip.mmsparams.server.comparators;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO;

import static org.junit.Assert.assertEquals;

public class ServerTestInstanceComparatorTestDesc
{

    private ServerTestInstanceComparatorDesc serverTestInstanceComparatorDesc;

    @Before
    public void setUp()
    {
        this.serverTestInstanceComparatorDesc = new ServerTestInstanceComparatorDesc();
    }

    @Test
    public void compareTest()
    {
        ServerTestInstanceDTO first = new ServerTestInstanceDTO();
        first.setCreatedDT(10);
        ServerTestInstanceDTO second = new ServerTestInstanceDTO();
        second.setCreatedDT(20);


        List<ServerTestInstanceDTO> list = new ArrayList<>();
        list.add(first);
        list.add(second);

        list.sort(serverTestInstanceComparatorDesc);
        assertEquals(second.getCreatedDT(), list.get(0).getCreatedDT());
        assertEquals(first.getCreatedDT(), list.get(1).getCreatedDT());


        list = new ArrayList<>();
        list.add(second);
        list.add(first);

        list.sort(serverTestInstanceComparatorDesc);
        assertEquals(second.getCreatedDT(), list.get(0).getCreatedDT());
        assertEquals(first.getCreatedDT(), list.get(1).getCreatedDT());
    }
}
