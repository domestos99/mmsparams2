package cz.uhk.dip.mmsparams.server.comparators;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.server.database.dto.WebSocketMessageDTO;

import static org.junit.Assert.assertEquals;

public class WebSocketMessageBaseDTOComparatorAscTest
{

    private WebSocketMessageBaseDTOComparatorAsc webSocketMessageBaseDTOComparatorAsc;

    @Before
    public void setUp()
    {
        this.webSocketMessageBaseDTOComparatorAsc = new WebSocketMessageBaseDTOComparatorAsc();
    }

    @Test
    public void compareTest()
    {
        WebSocketMessageDTO first = new WebSocketMessageDTO();
        first.setInsertedDT(10);
        WebSocketMessageDTO second = new WebSocketMessageDTO();
        second.setInsertedDT(20);


        List<WebSocketMessageDTO> list = new ArrayList<>();
        list.add(first);
        list.add(second);

        list.sort(webSocketMessageBaseDTOComparatorAsc);
        assertEquals(first.getInsertedDT(), list.get(0).getInsertedDT());
        assertEquals(second.getInsertedDT(), list.get(1).getInsertedDT());

        list = new ArrayList<>();
        list.add(second);
        list.add(first);

        list.sort(webSocketMessageBaseDTOComparatorAsc);
        assertEquals(first.getInsertedDT(), list.get(0).getInsertedDT());
        assertEquals(second.getInsertedDT(), list.get(1).getInsertedDT());
    }
}
