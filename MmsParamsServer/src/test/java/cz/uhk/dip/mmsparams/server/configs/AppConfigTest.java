package cz.uhk.dip.mmsparams.server.configs;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AppConfigTest
{
    @Test
    public void socketRouteTest()
    {
        assertEquals("/appsocket", AppConfig.getSocketRoute());
    }

    @Test
    public void getMmscPatternTest()
    {
        assertEquals("/MMSC/*", AppConfig.getMmscPattern());
    }
}