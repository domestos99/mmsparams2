package cz.uhk.dip.mmsparams.server.controllers;

import net.instantcom.mm7.DeliverReq;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;
import java.io.InputStream;

import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.server.mock.MockVaspService;
import cz.uhk.dip.mmsparams.server.web.controllers.MmscServlet;

import static org.junit.Assert.assertEquals;

public class MmscServletTest
{
    @Test
    public void test1() throws IOException
    {
        MmscServlet ctr = new MmscServlet(new MockVaspService());

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setMethod("POST");
        request.setContentType("multipart/related; boundary=\"NextPart_000_0125_01C19839.7237929064\"; type=\"text/xml\"");

        InputStream in = DeliverReq.class.getResourceAsStream("deliver-req1.txt");

        request.setContent(StringUtil.convertStreamToString(in).getBytes());

        MockHttpServletResponse response = new MockHttpServletResponse();

        ctr.doPost(request, response);

        assertEquals(200, response.getStatus());
    }

    @Test
    public void test2() throws IOException
    {
        MmscServlet ctr = new MmscServlet(new MockVaspService());

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setMethod("POST");
        request.setContentType("multipart/related; boundary=\"NextPart_000_0125_01C19839.7237929064\"; type=\"text/xml\"");

        InputStream in = DeliverReq.class.getResourceAsStream("deliver-req2.txt");

        request.setContent(StringUtil.convertStreamToString(in).getBytes());

        MockHttpServletResponse response = new MockHttpServletResponse();

        ctr.doPost(request, response);

        assertEquals(200, response.getStatus());
    }

    @Test
    public void test3() throws IOException
    {
        MmscServlet ctr = new MmscServlet(new MockVaspService());

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setMethod("POST");
        request.setContentType("multipart/related; boundary=\"Nokia-mm-messageHandler-BoUnDaRy-=_-735647067\"; type=\"text/xml\"");

        InputStream in = DeliverReq.class.getResourceAsStream("deliver-req3.txt");

        request.setContent(StringUtil.convertStreamToString(in).getBytes());

        MockHttpServletResponse response = new MockHttpServletResponse();

        ctr.doPost(request, response);

        assertEquals(200, response.getStatus());
    }

    @Test
    public void test5() throws IOException
    {
        MmscServlet ctr = new MmscServlet(new MockVaspService());

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setMethod("POST");
        request.setContentType("multipart/related; boundary=\"----=_Part_15_16023213.1346680532641\"; type=\"text/xml\"");

        InputStream in = DeliverReq.class.getResourceAsStream("deliver-req5.txt");

        request.setContent(StringUtil.convertStreamToString(in).getBytes());

        MockHttpServletResponse response = new MockHttpServletResponse();

        ctr.doPost(request, response);

        assertEquals(200, response.getStatus());
    }


}
