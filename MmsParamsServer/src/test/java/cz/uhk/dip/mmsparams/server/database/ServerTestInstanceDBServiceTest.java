package cz.uhk.dip.mmsparams.server.database;

import com.cloudhopper.smpp.SmppSession;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import cz.uhk.dip.mmsparams.api.constants.YesNoNaN;
import cz.uhk.dip.mmsparams.api.enums.MessageDirection;
import cz.uhk.dip.mmsparams.api.interfaces.ITestIdProvider;
import cz.uhk.dip.mmsparams.api.messages.MessageRegisterItem;
import cz.uhk.dip.mmsparams.api.utils.CommonUtils;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.TestGroupModel;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.error.TestErrorType;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MmscAcquireRouteModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.registration.TestClientInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.database.dto.ClientInfoDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ClientLibInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.MessageRegisterDTO;
import cz.uhk.dip.mmsparams.server.database.dto.MmscInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.PhoneInfoDTO;
import cz.uhk.dip.mmsparams.server.database.dto.PhoneInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDetailDTO;
import cz.uhk.dip.mmsparams.server.database.dto.SmscInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.TestErrorDTO;
import cz.uhk.dip.mmsparams.server.database.dto.TestGroupModelDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ValidationResultDTO;
import cz.uhk.dip.mmsparams.server.database.dto.WSSessionDTO;
import cz.uhk.dip.mmsparams.server.database.dto.WebSocketMessageDTO;
import cz.uhk.dip.mmsparams.server.database.services.ITestGroupService;
import cz.uhk.dip.mmsparams.server.database.services.ServerTestInstanceDBService;
import cz.uhk.dip.mmsparams.server.mocks.MockSmscSession;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@Transactional
@Rollback
public class ServerTestInstanceDBServiceTest extends UnitTestBase
{

    @Autowired
    private ServerTestInstanceDBService service;

    @Autowired
    private ITestGroupService testGroupService;

    @Test
    @Rollback
    public void saveAndLoadTest()
    {
        assertNotNull(this.service);

        ServerTestInstanceDetailDTO dto = new ServerTestInstanceDetailDTO();


        dto.setTestId(CommonUtils.getUUID());
        dto.setClosed(true);
        dto.setCreatedDT(System.currentTimeMillis());
        dto.setHasErrors(true);
        dto.setValidationErrors(YesNoNaN.YES);

        ClientLibInstanceDTO clientLibInstanceDTO = getClientLibInstanceDTO();
        dto.setClientLibInstance(clientLibInstanceDTO);

        List<PhoneInstanceDTO> phoneInstanceDTOS = getPhoneInstaneceList();
        dto.setPhoneInstanceList(phoneInstanceDTOS);

        List<SmscInstanceDTO> smscInstanceDTOS = getSmscInstanceList();
        dto.setSmscInstances(smscInstanceDTOS);

        MmscInstanceDTO mmscInstanceDTO = getMmscInstanceDTO();
        dto.setMmscInstance(mmscInstanceDTO);

        MessageRegisterDTO messageRegisterDTO = getMessageRegister();
        dto.setMessageRegister(messageRegisterDTO);

        List<ValidationResultDTO> validationResultDTOS = getValidationResultListDTO();
        dto.setValidationResults(validationResultDTOS);

        List<TestErrorDTO> testErrors = TestErrorDTO.create(getTestErrors());
        dto.setTestErrors(testErrors);

        service.save(dto);


        int id = dto.getId();

        // Load
        Optional<ServerTestInstanceDetailDTO> dbOpt = service.getByTestId(dto.getTestId());
        assertTrue(dbOpt.isPresent());
        ServerTestInstanceDetailDTO db = dbOpt.get();


        db.getPhoneInstanceList().size();
        db.getValidationResults().size();
        db.getTestErrors().size();
        db.getMessageRegister().getIncoming().size();
        // validations
        assertTrue(dto.equals(db));


        // GetReduced
        List<ServerTestInstanceDTO> reduced = service.getReduced();


        System.out.println("Deleting data");

        service.delete(db.getTestId());


        System.out.println("OK");

    }


    @Test
    @Rollback
    public void serverTestInstanceSaveTest()
    {
        String testId = CommonUtils.getUUID();
        TestGroupModel testGroupModel = getTestGroupForUT();
        TestClientInfoModel testClientInfoModel = new TestClientInfoModel();
        testClientInfoModel.setTestName("UnitTest test");
        testClientInfoModel.setTestDesc("UnitTest test desc");

        ClientInfo ci = getClientInfo();

        ServerTestInstance test = ServerTestInstance.createNewTest(testId, testGroupModel, getNewSession(), testClientInfoModel, ci, serverTestInstance -> {
            saveAndValidateTest(serverTestInstance);
        });


        PhoneInfoModel phoneInfoModel = getPhoneInfoModel(1);
        test.lockPhoneForTest(getNewSession(), phoneInfoModel);
        PhoneInfoModel phoneInfoModel2 = getPhoneInfoModel(2);
        test.lockPhoneForTest(getNewSession(), phoneInfoModel2);

        SmscSessionId smscSessionId = new SmscSessionId(CommonUtils.getUUID());
        SmppSession smppSession = new MockSmscSession(null);
        test.addNewSmscConnection(smscSessionId, smppSession);


        MmscAcquireRouteModel acq = getMmscInstance();
        test.addNewMmscAcquirePort(acq);

        test.insertIncomingMsg(getTestMessage(1));
        test.insertIncomingMsg(getTestMessage(2));
        test.insertIncomingMsg(getTestMessage(3));
        test.insertIncomingMsg(getTestMessage(4));
        test.insertIncomingMsg(getTestMessage(5));

        test.insertOutgoingMsg(getTestMessage(10));
        test.insertOutgoingMsg(getTestMessage(20));
        test.insertOutgoingMsg(getTestMessage(30));
        test.insertOutgoingMsg(getTestMessage(40));
        test.insertOutgoingMsg(getTestMessage(50));


        test.addValidationResults(getValidationResultList());


        List<TestErrorModel> testErrors = getTestErrors();
        for (TestErrorModel e : testErrors)
            test.insertTestError(e);

        test.removeClientSessionAndCloseTest(true);
    }


    @Test
    @Rollback
    public void deleteOldTestsTest()
    {
        ServerTestInstanceDetailDTO dto1 = new ServerTestInstanceDetailDTO();
        dto1.setTestId(CommonUtils.getUUID());
        dto1.setClosed(true);
        dto1.setCreatedDT(System.currentTimeMillis());
        dto1.setHasErrors(true);
        dto1.setValidationErrors(YesNoNaN.NO);

        service.save(dto1);

        ServerTestInstanceDetailDTO dto2 = new ServerTestInstanceDetailDTO();
        dto2.setTestId(CommonUtils.getUUID());
        dto2.setClosed(true);
        dto2.setCreatedDT(123);
        dto2.setHasErrors(true);
        dto2.setValidationErrors(YesNoNaN.YES);
        service.save(dto2);

        List<ServerTestInstanceDTO> reduced = service.getReduced();
        assertEquals(2, reduced.size());


        boolean deleteReult = service.deleteOldTests(2);
        assertTrue(deleteReult);

        reduced = service.getReduced();
        assertEquals(1, reduced.size());
    }

    @Test
    @Rollback
    public void testNameColumnLenght()
    {
        ServerTestInstanceDetailDTO dto1 = new ServerTestInstanceDetailDTO();
        dto1.setTestId(CommonUtils.getUUID());
        dto1.setClosed(true);
        dto1.setCreatedDT(System.currentTimeMillis());
        dto1.setHasErrors(true);
        dto1.setValidationErrors(YesNoNaN.NAN);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 250; i++)
        {
            sb.append("a");
        }
        dto1.setTestName(sb.toString());

        service.save(dto1);
    }

    @Test
    @Rollback
    public void groupTest()
    {
        ServerTestInstanceDetailDTO dto1 = new ServerTestInstanceDetailDTO();
        dto1.setTestId(CommonUtils.getUUID());
        dto1.setClosed(true);
        dto1.setCreatedDT(System.currentTimeMillis());
        dto1.setHasErrors(true);
        dto1.setValidationErrors(YesNoNaN.YES);
        dto1.setTestGroupModelDTO(TestGroupModelDTO.create(getTestGroupForUT()));

        service.save(dto1);

        Optional<ServerTestInstanceDetailDTO> dbDTO = service.getByTestId(dto1.getTestId());
        assertTrue(dbDTO.isPresent());
        assertEquals(dto1.getTestGroupModelDTO(), dbDTO.get().getTestGroupModelDTO());

        ServerTestInstanceDetailDTO dto2 = new ServerTestInstanceDetailDTO();
        dto2.setTestId(CommonUtils.getUUID());
        dto2.setClosed(true);
        dto2.setCreatedDT(System.currentTimeMillis());
        dto2.setHasErrors(true);
        dto2.setValidationErrors(YesNoNaN.YES);
        TestGroupModelDTO testGroupModelDTO = new TestGroupModelDTO();
        testGroupModelDTO.setTestGroupID(dto1.getTestGroupModelDTO().getTestGroupID());
        testGroupModelDTO.setTestGroupName(dto1.getTestGroupModelDTO().getTestGroupName());
        testGroupModelDTO.setTestGroupDesc(dto1.getTestGroupModelDTO().getTestGroupDesc());
        dto2.setTestGroupModelDTO(testGroupModelDTO);

        service.save(dto2);

        List<TestGroupModelDTO> dbAllGroups = testGroupService.getAll();
        assertEquals(2, dbAllGroups.size());

        List<ServerTestInstanceDTO> reduced = service.getReduced();
        assertNotNull(reduced);
        assertEquals(2, reduced.size());
    }

    @Test
    @Rollback
    public void getReduced_groups_Test()
    {
        ServerTestInstanceDetailDTO dto1 = new ServerTestInstanceDetailDTO();
        dto1.setTestId(CommonUtils.getUUID());
        dto1.setClosed(true);
        dto1.setCreatedDT(System.currentTimeMillis());
        dto1.setHasErrors(true);
        dto1.setValidationErrors(YesNoNaN.YES);
        dto1.setTestGroupModelDTO(TestGroupModelDTO.create(getTestGroupForUT()));

        service.save(dto1);

        Optional<ServerTestInstanceDetailDTO> dbDTO = service.getByTestId(dto1.getTestId());
        assertTrue(dbDTO.isPresent());
        assertEquals(dto1.getTestGroupModelDTO(), dbDTO.get().getTestGroupModelDTO());

        ServerTestInstanceDetailDTO dto2 = new ServerTestInstanceDetailDTO();
        dto2.setTestId(CommonUtils.getUUID());
        dto2.setClosed(true);
        dto2.setCreatedDT(System.currentTimeMillis());
        dto2.setHasErrors(true);
        dto2.setValidationErrors(YesNoNaN.YES);

        service.save(dto2);

        List<TestGroupModelDTO> dbAllGroups = testGroupService.getAll();
        assertEquals(1, dbAllGroups.size());

        List<ServerTestInstanceDetailDTO> allDetails = service.getAll();
        assertNotNull(allDetails);
        assertEquals(2, allDetails.size());

        List<ServerTestInstanceDTO> reduced = service.getReduced();
        assertNotNull(reduced);
        assertEquals(2, reduced.size());

        assertNull(reduced.get(0).getTestGroupModelDTO());
        assertNotNull(reduced.get(1).getTestGroupModelDTO());
    }

    @Test
    @Rollback
    public void getReduced_groups2_Test()
    {
        ServerTestInstanceDetailDTO dto1 = new ServerTestInstanceDetailDTO();
        dto1.setTestGroupModelDTO(TestGroupModelDTO.create(getTestGroupForUT()));

        service.save(dto1);

        List<TestGroupModelDTO> dbAllGroups = testGroupService.getAll();
        assertEquals(1, dbAllGroups.size());

        List<ServerTestInstanceDetailDTO> allDetails = service.getAll();
        assertNotNull(allDetails);
        assertEquals(1, allDetails.size());

        List<ServerTestInstanceDTO> reduced = service.getReduced();
        assertNotNull(reduced);
        assertEquals(1, reduced.size());

        assertNotNull(reduced.get(0).getTestGroupModelDTO());
    }

    @Test
    @Rollback
    public void getReduced_groups3_Test()
    {
        ServerTestInstanceDetailDTO dto1 = new ServerTestInstanceDetailDTO();

        service.save(dto1);

        List<TestGroupModelDTO> dbAllGroups = testGroupService.getAll();
        assertEquals(0, dbAllGroups.size());

        List<ServerTestInstanceDetailDTO> allDetails = service.getAll();
        assertNotNull(allDetails);
        assertEquals(1, allDetails.size());

        List<ServerTestInstanceDTO> reduced = service.getReduced();
        assertNotNull(reduced);
        assertEquals(1, reduced.size());

        assertNull(reduced.get(0).getTestGroupModelDTO());
    }

    private void saveAndValidateTest(ServerTestInstance serverTestInstance)
    {
        int id = service.saveClosedTest(serverTestInstance);
        // load record from DB
        Optional<ServerTestInstanceDetailDTO> dbOpt = service.getByTestId(serverTestInstance.getTestID());

        assertTrue(dbOpt.isPresent());
        ServerTestInstanceDetailDTO db = dbOpt.get();

        // Evaluate to force lazy load
        db.getPhoneInstanceList().size();
        db.getValidationResults().size();
        db.getTestErrors().size();
        db.getMessageRegister().getIncoming().size();


        assertEquals(serverTestInstance.getTestID(), db.getTestId());
        assertEquals(serverTestInstance.getTestName(), db.getTestName());
        assertEquals(serverTestInstance.getTestDesc(), db.getTestDesc());
        assertEquals(serverTestInstance.isClosed(), db.isClosed());
        assertEquals(serverTestInstance.getCreatedDT(), db.getCreatedDT());


        assertEquals(serverTestInstance.getClientLibInstance().getClientSession().getId(), db.getClientLibInstance().getSession().getId());
        assertEquals(serverTestInstance.getClientLibInstance().getClientInfo().getClientKey(), db.getClientLibInstance().getClientInfo().getClientKey());
        assertEquals(serverTestInstance.getClientLibInstance().getClientInfo().getComputerName(), db.getClientLibInstance().getClientInfo().getComputerName());
        assertEquals(serverTestInstance.getClientLibInstance().getClientInfo().getCurrentUserName(), db.getClientLibInstance().getClientInfo().getCurrentUserName());


        assertEquals(serverTestInstance.getPhoneInstanceListHist().size(), db.getPhoneInstanceList().size());
        assertEquals(serverTestInstance.getSmscInstancesHist().size(), db.getSmscInstances().size());

        assertEquals(serverTestInstance.getValidationResults().size(), db.getValidationResults().size());
        assertEquals(serverTestInstance.getTestErrorsHist().size(), db.getTestErrors().size());
        assertEquals(serverTestInstance.getMessageRegister().getAllIncoming().size(), db.getMessageRegister().getIncoming().size());
        assertEquals(serverTestInstance.getMessageRegister().getAllOutgoing().size(), db.getMessageRegister().getOutgoing().size());


        System.out.println("Deleting data");
        this.service.delete(serverTestInstance.getTestID());

        System.out.println("OK");

    }


    private ClientInfo getClientInfo()
    {
        ClientInfo ci = new ClientInfo();
        ci.setClientKey(CommonUtils.getUUID());
        ci.setComputerName("computername");
        ci.setCurrentUserName("currentusername");
        return ci;
    }


    private List<TestErrorModel> getTestErrors()
    {
        List<TestErrorModel> list = new ArrayList<>();

        TestErrorModel err1 = new TestErrorModel();
        err1.setTestErrorType(TestErrorType.TEST_FORCE_CLOSE);
        err1.setMessage(CommonUtils.getUUID());
        list.add(err1);


        TestErrorModel err2 = new TestErrorModel();
        err2.setTestErrorType(TestErrorType.WRONGLY_WRITTEN_TEST);
        err2.setMessage(CommonUtils.getUUID());
        err2.setException(new Exception("Test Exception"));
        list.add(err2);

        TestErrorModel err3 = new TestErrorModel();
        err3.setTestErrorType(TestErrorType.TIMEOUT);
        err3.setMessage(CommonUtils.getUUID() + CommonUtils.getUUID() + CommonUtils.getUUID()
                + CommonUtils.getUUID() + CommonUtils.getUUID() + CommonUtils.getUUID()
                + CommonUtils.getUUID() + CommonUtils.getUUID() + CommonUtils.getUUID()
                + CommonUtils.getUUID() + CommonUtils.getUUID() + CommonUtils.getUUID()
                + CommonUtils.getUUID() + CommonUtils.getUUID() + CommonUtils.getUUID());
        list.add(err3);

        return list;
    }


    private List<Tuple<Boolean, ValidationResult>> getValidationResultList()
    {
        List<Tuple<Boolean, ValidationResult>> list = new ArrayList<>();

        ValidationResult dto = new ValidationResult();
        ValidationResult dto2 = new ValidationResult();

        dto.setActual("A");
        dto.setExpected("A");
        dto.setResult(true);
        dto.setValidationItemName("validation item 1");
        dto.setValidationName("validation name 1");

        dto2.setActual("B");
        dto2.setExpected("A");
        dto2.setResult(false);
        dto2.setValidationItemName("validation item 2");
        dto2.setValidationName("validation name 2");

        list.add(new Tuple<>(true, dto));
        list.add(new Tuple<>(false, dto2));

        return list;
    }

    private List<ValidationResultDTO> getValidationResultListDTO()
    {
        List<ValidationResultDTO> list = new ArrayList<>();

        ValidationResultDTO dto = new ValidationResultDTO();
        ValidationResultDTO dto2 = new ValidationResultDTO();

        dto.setActual("A");
        dto.setExpected("A");
        dto.setResult(true);
        dto.setValidationItemName("validation item 1");
        dto.setValidationName("validation name 1");

        dto2.setActual("B");
        dto2.setExpected("A");
        dto2.setResult(false);
        dto2.setValidationItemName("validation item 2");
        dto2.setValidationName("validation name 2");

        list.add(dto);
        list.add(dto2);

        return list;
    }

    private MessageRegisterDTO getMessageRegister()
    {
        MessageRegisterDTO dto = new MessageRegisterDTO();

        List<WebSocketMessageDTO> incoming = new ArrayList<>();

        incoming.add(WebSocketMessageDTO.create(getTestMessage2(1)));
        incoming.add(WebSocketMessageDTO.create(getTestMessage2(2)));
        incoming.add(WebSocketMessageDTO.create(getTestMessage2(3)));
        incoming.add(WebSocketMessageDTO.create(getTestMessage2(4)));

        dto.setIncoming(incoming);

        List<WebSocketMessageDTO> outgoing = new ArrayList<>();

        outgoing.add(WebSocketMessageDTO.create(getTestMessage2(10)));
        outgoing.add(WebSocketMessageDTO.create(getTestMessage2(20)));
        outgoing.add(WebSocketMessageDTO.create(getTestMessage2(30)));
        outgoing.add(WebSocketMessageDTO.create(getTestMessage2(40)));

        dto.setOutgoing(outgoing);


        return dto;
    }

    private MmscInstanceDTO getMmscInstanceDTO()
    {
        return null;
    }

    private MmscAcquireRouteModel getMmscInstance()
    {
        MmscAcquireRouteModel acq = new MmscAcquireRouteModel();
        acq.setPattern("99988775*");
        return acq;
    }

    private List<SmscInstanceDTO> getSmscInstanceList()
    {
        return null;
    }

    private List<PhoneInstanceDTO> getPhoneInstaneceList()
    {
        List<PhoneInstanceDTO> list = new ArrayList<>();


        PhoneInstanceDTO dto = new PhoneInstanceDTO();

        dto.setSession(createTestSessionDTO());
        dto.setPhoneInfo(getPhoneInfo(1));
        PhoneInstanceDTO dto2 = new PhoneInstanceDTO();
        dto2.setSession(createTestSessionDTO());
        dto2.setPhoneInfo(getPhoneInfo(2));

        list.add(dto);
        list.add(dto2);


        return list;
    }

    private PhoneInfoModel getPhoneInfoModel(int i)
    {
        PhoneInfoModel phoneInfoModel = new PhoneInfoModel();
        phoneInfoModel.setPhoneKey(CommonUtils.getUUID() + i);
        return phoneInfoModel;
    }

    private PhoneInfoDTO getPhoneInfo(int i)
    {
        PhoneInfoDTO dto = new PhoneInfoDTO();

        dto.setPhoneKey(CommonUtils.getUUID());

        return dto;
    }

    private ClientLibInstanceDTO getClientLibInstanceDTO()
    {
        ClientLibInstanceDTO dto = new ClientLibInstanceDTO();
        dto.setSession(createTestSessionDTO());
        dto.setClientInfo(getClientInfoDto());
        dto.setClientKey(dto.getClientInfo().getClientKey());
        return dto;
    }

    private WSSessionDTO createTestSessionDTO()
    {
        WSSessionDTO dto = new WSSessionDTO();
        dto.setId(CommonUtils.getUUID());
        dto.setLocalAddress(CommonUtils.getUUID());
        dto.setRemoteAddress(CommonUtils.getUUID());
        dto.setAcceptedProtocol(CommonUtils.getUUID());
        dto.setExtensions(CommonUtils.getUUIDList(5));
        dto.setPrincipal(CommonUtils.getUUID());
        return dto;
    }

    private ClientInfoDTO getClientInfoDto()
    {
        ClientInfoDTO dto = new ClientInfoDTO();

        dto.setClientKey(CommonUtils.getUUID());
        dto.setComputerName("computername");
        dto.setCurrentUserName("currentusername");
        return dto;
    }

    private MessageRegisterItem getTestMessage2(int i)
    {
        return new MessageRegisterItem(getTestMessage(i), MessageDirection.IN);
    }

    private DevMessage getTestMessage(int i)
    {
        return new MessageBuilder()
                .withSenderKey("sender" + i)
                .withMessageIdRandom()
                .withTestId(new ITestIdProvider()
                {
                    @Override
                    public String getTestId()
                    {
                        return "test-id" + i;
                    }
                })
                .build(DevMessage.class);
    }


}
