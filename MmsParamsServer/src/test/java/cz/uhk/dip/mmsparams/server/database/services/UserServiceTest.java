package cz.uhk.dip.mmsparams.server.database.services;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.auth.UserModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class UserServiceTest extends UnitTestBase
{
    private UserService userService;

    @Before
    public void setUp() throws Exception
    {

    }

    @Test
    public void getUserByUserNameTest()
    {
        userService = new UserService("login", "password");
        UserModel user = userService.getUserByUserName("login");
        assertNotNull(user);
        assertEquals("login", user.getUsername());
        assertEquals("password", user.getPassword());
    }

    @Test
    public void getUserByUser_wrong_login_NameTest()
    {
        userService = new UserService("login", "password");
        UserModel user = userService.getUserByUserName("login0");
        assertNull(user);
    }

    @Test
    public void getUserTest()
    {
        String un = "login";
        String pwd = "password";
        userService = new UserService(un, pwd);
        UserModel user = userService.getUser(un, pwd);
        assertNotNull(user);
        assertEquals(un, user.getUsername());
        assertEquals(pwd, user.getPassword());
    }

    @Test
    public void getUser_wrong_username_Test()
    {
        String un = "login";
        String pwd = "password";
        userService = new UserService(un, pwd);
        UserModel user = userService.getUser("xxx", pwd);
        assertNull(user);
    }

    @Test
    public void getUser_wrong_password_Test()
    {
        String un = "login";
        String pwd = "password";
        userService = new UserService(un, pwd);
        UserModel user = userService.getUser(un, "xxx");
        assertNull(user);
    }

    @Test
    public void getUser_wrong_username_and_password_Test()
    {
        String un = "login";
        String pwd = "password";
        userService = new UserService(un, pwd);
        UserModel user = userService.getUser("xxx", "xxx");
        assertNull(user);
    }
}
