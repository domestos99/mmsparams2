package cz.uhk.dip.mmsparams.server.interceptors;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.server.auth.IJwtTokenService;
import cz.uhk.dip.mmsparams.server.auth.JwtTokenService;
import cz.uhk.dip.mmsparams.server.auth.JwtTokenUtil;
import cz.uhk.dip.mmsparams.server.database.services.UserService;
import cz.uhk.dip.mmsparams.server.web.constant.WebConstant;

import static org.junit.Assert.assertEquals;

public class JwtTokenInterceptorTest
{
    private JwtTokenInterceptor jwtTokenInterceptor;
    private JwtTokenUtil jwtTokenUtil;
    private UserService userService;
    private IJwtTokenService authService;

    @Before
    public void setUp()
    {
        this.jwtTokenUtil = new JwtTokenUtil("xxx-abc");
        this.userService = new UserService("test", "test");
        this.authService = new JwtTokenService(userService, jwtTokenUtil);
        this.jwtTokenInterceptor = new JwtTokenInterceptor(authService);
    }

    @Test
    public void doFilterInternal_no_token_test() throws ServletException, IOException
    {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/api/clients");
        MockHttpServletResponse resp = new MockHttpServletResponse();
        MockFilterChain mockFilterChain = new MockFilterChain();
        jwtTokenInterceptor.doFilterInternal(request, resp, mockFilterChain);
        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, resp.getStatus());
    }

    @Test
    public void doFilterInternal_invalid_token_test() throws ServletException, IOException
    {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/api/clients");
        MockHttpServletResponse resp = new MockHttpServletResponse();
        MockFilterChain mockFilterChain = new MockFilterChain();


        List<String> roles = ListUtils.newList();
        roles.add("admin");
        Tuple<String, Long> token = this.jwtTokenUtil.generateToken("test", roles);
        request.addHeader(HttpConstants.AUTHORIZATION, HttpConstants.TOKEN_PREFIX + token.getX() + "abc");


        jwtTokenInterceptor.doFilterInternal(request, resp, mockFilterChain);

        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, resp.getStatus());
    }

    @Test
    public void doFilterInternal_expiry_token_test() throws ServletException, IOException
    {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/api/clients");
        MockHttpServletResponse resp = new MockHttpServletResponse();
        MockFilterChain mockFilterChain = new MockFilterChain();

        String expiryToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJtbXNwYXJhbXMtc2VydmVyIiwiYXVkIjoibW1zcGFyYW1zLWNsaWVudHMiLCJzdWIiOiJ0ZXN0IiwiaWF0IjoxNTY1MjIxNTQ3LCJleHAiOjAsInJvbCI6WyJhZG1pbiJdfQ.JjKcswXWDkKkG8BkY9lEK9tISu5DnK1u5-CTau63iRPAOOngqaXRNzgbYVlpWMvAA4UzKe5L5QzM3_M0Ax6SrQ";
        request.addHeader(HttpConstants.AUTHORIZATION, HttpConstants.TOKEN_PREFIX + expiryToken);

        jwtTokenInterceptor.doFilterInternal(request, resp, mockFilterChain);

        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, resp.getStatus());
    }


    @Test
    public void doFilterInternal_wrong_user_token_test() throws ServletException, IOException
    {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/api/clients");
        MockHttpServletResponse resp = new MockHttpServletResponse();
        MockFilterChain mockFilterChain = new MockFilterChain();


        List<String> roles = ListUtils.newList();
        roles.add("admin");
        Tuple<String, Long> token = this.jwtTokenUtil.generateToken("test1", roles);
        request.addHeader(HttpConstants.AUTHORIZATION, HttpConstants.TOKEN_PREFIX + token.getX());


        jwtTokenInterceptor.doFilterInternal(request, resp, mockFilterChain);

        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, resp.getStatus());
    }

    @Test
    public void doFilterInternal_empty_token_test() throws ServletException, IOException
    {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/api/clients");
        MockHttpServletResponse resp = new MockHttpServletResponse();
        MockFilterChain mockFilterChain = new MockFilterChain();

        request.addHeader(HttpConstants.AUTHORIZATION, "");

        jwtTokenInterceptor.doFilterInternal(request, resp, mockFilterChain);

        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, resp.getStatus());
    }

    @Test
    public void doFilterInternal_200_test() throws ServletException, IOException
    {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/api/clients");
        MockHttpServletResponse resp = new MockHttpServletResponse();
        MockFilterChain mockFilterChain = new MockFilterChain();

        List<String> roles = ListUtils.newList();
        roles.add("admin");
        Tuple<String, Long> token = this.jwtTokenUtil.generateToken("test", roles);
        request.addHeader(HttpConstants.AUTHORIZATION, HttpConstants.TOKEN_PREFIX + token.getX());


        jwtTokenInterceptor.doFilterInternal(request, resp, mockFilterChain);

        assertEquals(HttpServletResponse.SC_OK, resp.getStatus());
    }

    @Test
    public void checkRoutesTest() throws ServletException, IOException
    {
        checkRoute(WebConstant.CLIENT_CLIENTS);
        checkRoute(WebConstant.CLIENT_CLIENTS_CLIENTKEY);
        checkRoute(WebConstant.CLIENT_FORCE_DISCONNECT);
        checkRoute(WebConstant.MESSAGE_REGISTER_TESTID_MESSAGEID);
        checkRoute(WebConstant.PHONES_ALL);
        checkRoute(WebConstant.PHONES_PHONEKEY);
        checkRoute(WebConstant.PHONES_FORCE_DISCONNECT);
        checkRoute(WebConstant.SESSION_ALL);
        checkRoute(WebConstant.SESSION_BY_ID);

        checkRoute(WebConstant.TEST_INSTANCE_ALL);
        checkRoute(WebConstant.TEST_INSTANCE_BY_TEST_ID);
        checkRoute(WebConstant.TEST_INSTANCE_DELETE_BY_TEST_ID);
        checkRoute(WebConstant.TEST_FORCE_CLOSE);
        checkRoute(WebConstant.ADMIN_REFRESH_CACHES);
        // checkRoute(WebConstant.PING_PING); // not secured
        // checkRoute(WebConstant.SYSTEM_VERSION); // not secured


    }

    private void checkRoute(String route) throws ServletException, IOException
    {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", route);
        MockHttpServletResponse resp = new MockHttpServletResponse();
        MockFilterChain mockFilterChain = new MockFilterChain();
        jwtTokenInterceptor.doFilterInternal(request, resp, mockFilterChain);
        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, resp.getStatus());
    }


}
