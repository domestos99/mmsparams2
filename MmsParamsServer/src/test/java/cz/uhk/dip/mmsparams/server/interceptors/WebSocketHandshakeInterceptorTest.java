package cz.uhk.dip.mmsparams.server.interceptors;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;

import javax.servlet.http.HttpServletRequest;

import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.auth.IJwtTokenService;
import cz.uhk.dip.mmsparams.server.auth.JwtTokenService;
import cz.uhk.dip.mmsparams.server.auth.JwtTokenUtil;
import cz.uhk.dip.mmsparams.server.database.services.IUserService;
import cz.uhk.dip.mmsparams.server.database.services.UserService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WebSocketHandshakeInterceptorTest extends UnitTestBase
{
    private WebSocketHandshakeInterceptor interceptor;
    private IUserService userService;
    private IJwtTokenService tokenService;
    private JwtTokenUtil jwtTokenUtil;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ServletServerHttpRequest request;
    @Mock
    private ServerHttpResponse response;
    @Mock
    private HttpServletRequest httpServletRequest;


    @Before
    public void setUp() throws Exception
    {
        request = mock(ServletServerHttpRequest.class);
        response = mock(ServerHttpResponse.class);
        this.httpServletRequest = mock(HttpServletRequest.class);

        this.jwtTokenUtil = new JwtTokenUtil("secret");
        this.userService = new UserService("test", "test");
        this.tokenService = new JwtTokenService(this.userService, this.jwtTokenUtil);
        this.interceptor = new WebSocketHandshakeInterceptor(this.tokenService);
    }

    @Test
    public void beforeHandshakeTest()
    {
        when(request.getServletRequest()).thenReturn(this.httpServletRequest);
        JwtRequest jwtRequest = new JwtRequest("test", "test");
        String token = HttpConstants.TOKEN_PREFIX + this.tokenService.getNewToken(jwtRequest).getToken();
        when(this.httpServletRequest.getHeader(eq(HttpConstants.AUTHORIZATION))).thenReturn(token);
        boolean result = interceptor.beforeHandshake(request, response, null, null);
        assertTrue(result);
    }

    @Test
    public void beforeHandshake_invalid_token_Test()
    {
        when(request.getServletRequest()).thenReturn(this.httpServletRequest);
        JwtRequest jwtRequest = new JwtRequest("test", "test");
        String token = HttpConstants.TOKEN_PREFIX + this.tokenService.getNewToken(jwtRequest).getToken() + "abc";
        when(this.httpServletRequest.getHeader(eq(HttpConstants.AUTHORIZATION))).thenReturn(token);
        boolean result = interceptor.beforeHandshake(request, response, null, null);
        assertFalse(result);
    }

    @Test
    public void beforeHandshake_no_token_Test()
    {
        when(request.getServletRequest()).thenReturn(this.httpServletRequest);
        when(this.httpServletRequest.getHeader(eq(HttpConstants.AUTHORIZATION))).thenReturn(null);
        boolean result = interceptor.beforeHandshake(request, response, null, null);
        assertFalse(result);
    }
}
