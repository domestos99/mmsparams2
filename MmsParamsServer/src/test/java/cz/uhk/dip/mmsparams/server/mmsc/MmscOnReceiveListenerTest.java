package cz.uhk.dip.mmsparams.server.mmsc;

import org.junit.Before;
import org.junit.Test;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;

import cz.uhk.dip.mmsparams.api.exceptions.TestInvalidException;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.IMmscWSController;

import static org.junit.Assert.assertEquals;

public class MmscOnReceiveListenerTest
{
    private IMmscOnReceiveListener listener;

    private IMmscWSController iClientLibBroadcast;

    private int callCount = 0;

    @Before
    public void setup()
    {
        callCount = 0;
        iClientLibBroadcast = new IMmscWSController()
        {
            @Override
            public boolean sendMmsc(WebSocketSession session, MmscSendMessage msg)
            {
                return false;
            }

            @Override
            public boolean mmscAcquireRouterPort(WebSocketSession session, MmscAcquireRouteRequestMessage msg)
            {
                return false;
            }

            @Override
            public boolean processMmscBroadcastToClientFromServer(WebSocketMessageBase message, List<MM7Address> recipients)
            {
                if (!MessageUtils.isMessageValid(message))
                    throw new RuntimeException("Message invalid!");

                callCount++;

                return true;
            }
        };
        listener = new MmscOnReceiveListener(iClientLibBroadcast);
    }

    @Test
    public void onDeliveryReqReceivedTest()
    {
        MM7DeliveryReqModel model = new MM7DeliveryReqModel();
        listener.onDeliveryReqReceived(model);
        assertEquals(1, callCount);
    }

    @Test(expected = TestInvalidException.class)
    public void onDeliveryReqReceived_null_Test()
    {
        listener.onDeliveryReqReceived(null);
        assertEquals(1, callCount);
    }


    @Test
    public void onDeliveryReportReqReceivedTest()
    {
        MM7DeliveryReportReqModel model = new MM7DeliveryReportReqModel();
        listener.onDeliveryReportReqReceived(model);
        assertEquals(1, callCount);
    }

    @Test(expected = TestInvalidException.class)
    public void onDeliveryReportReqReceived_null_Test()
    {
        listener.onDeliveryReportReqReceived(null);
        assertEquals(1, callCount);
    }

    @Test
    public void onReadReplyTest()
    {
        MM7ReadReplyReqModel model = new MM7ReadReplyReqModel();
        listener.onReadReply(model);
        assertEquals(1, callCount);
    }

    @Test(expected = TestInvalidException.class)
    public void onReadReply_null_Test()
    {
        listener.onReadReply(null);
        assertEquals(1, callCount);
    }

    @Test
    public void onErrorTest()
    {
        listener.onError(null);
    }
}
