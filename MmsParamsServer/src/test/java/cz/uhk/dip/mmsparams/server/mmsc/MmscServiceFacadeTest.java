package cz.uhk.dip.mmsparams.server.mmsc;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;

import static org.junit.Assert.assertEquals;

public class MmscServiceFacadeTest
{
    private IMmscServiceFacade facade;

    private int callCount1 = 0;
    private int callCount2 = 0;
    private int callCount3 = 0;
    private int callCount4 = 0;

    @Before
    public void setUp() throws Exception
    {
        callCount1 = 0;
        callCount2 = 0;
        callCount3 = 0;
        callCount4 = 0;

        facade = new MmscServiceFacade();
        facade.setOnReceiveListener(new IMmscOnReceiveListener()
        {
            @Override
            public void onDeliveryReqReceived(MM7DeliveryReqModel model)
            {
                callCount1++;
            }

            @Override
            public void onDeliveryReportReqReceived(MM7DeliveryReportReqModel model)
            {
                callCount2++;
            }

            @Override
            public void onReadReply(MM7ReadReplyReqModel mm7ReadReplyReqModel)
            {
                callCount3++;
            }

            @Override
            public void onError(MM7ErrorModel model)
            {
                callCount4++;
            }
        });
    }

    @Test
    public void sendTest()
    {

    }

    @Test
    public void onDeliveryReqReceivedTest()
    {
        MM7DeliveryReqModel model = new MM7DeliveryReqModel();
        facade.onDeliveryReqReceived(model);
        assertEquals(1, callCount1);
        assertEquals(0, callCount2);
        assertEquals(0, callCount3);
        assertEquals(0, callCount4);

    }

    @Test
    public void onDeliveryReportReqReceivedTest()
    {
        MM7DeliveryReportReqModel model = new MM7DeliveryReportReqModel();
        facade.onDeliveryReportReqReceived(model);
        assertEquals(0, callCount1);
        assertEquals(1, callCount2);
        assertEquals(0, callCount3);
        assertEquals(0, callCount4);
    }

    @Test
    public void onReadReplyTest()
    {
        MM7ReadReplyReqModel model = new MM7ReadReplyReqModel();
        facade.onReadReply(model);
        assertEquals(0, callCount1);
        assertEquals(0, callCount2);
        assertEquals(1, callCount3);
        assertEquals(0, callCount4);
    }

    @Test
    public void onErrorTest()
    {
        MM7ErrorModel model = new MM7ErrorModel();
        facade.onError(model);
        assertEquals(0, callCount1);
        assertEquals(0, callCount2);
        assertEquals(0, callCount3);
        assertEquals(1, callCount4);
    }
}
