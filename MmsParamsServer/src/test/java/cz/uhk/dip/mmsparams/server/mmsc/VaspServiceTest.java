package cz.uhk.dip.mmsparams.server.mmsc;

import net.instantcom.mm7.DeliverReq;
import net.instantcom.mm7.DeliverRsp;
import net.instantcom.mm7.DeliveryReportReq;
import net.instantcom.mm7.DeliveryReportRsp;
import net.instantcom.mm7.MM7Context;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MM7Response;
import net.instantcom.mm7.ReadReplyReq;
import net.instantcom.mm7.ReadReplyRsp;
import net.instantcom.mm7.VASP;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class VaspServiceTest
{
    private VASP vasp;
    private int callCount1 = 0;
    private int callCount2 = 0;
    private int callCount3 = 0;
    private int callCount4 = 0;

    @Before
    public void setUp() throws Exception
    {
        callCount1 = 0;
        callCount2 = 0;
        callCount3 = 0;
        callCount4 = 0;

        vasp = new VaspService(new IMmscServiceFacade()
        {
            @Override
            public MM7SubmitResponseModel send(MmscSendMessage msg) throws Exception
            {
                fail();
                return null;
            }

            @Override
            public void onDeliveryReqReceived(MM7DeliveryReqModel model)
            {
                if (model == null)
                    fail();
                callCount1++;
            }

            @Override
            public void onDeliveryReportReqReceived(MM7DeliveryReportReqModel model)
            {
                if (model == null)
                    fail();
                callCount2++;
            }

            @Override
            public void onReadReply(MM7ReadReplyReqModel mm7ReadReplyReqModel)
            {
                if (mm7ReadReplyReqModel == null)
                    fail();
                callCount3++;
            }

            @Override
            public void onError(MM7ErrorModel model)
            {
                if (model == null)
                    fail();
                callCount4++;
            }

            @Override
            public void setOnReceiveListener(IMmscOnReceiveListener mmscOnReceiveListener)
            {

            }
        }, new IMmscOnReceiveListener()
        {
            @Override
            public void onDeliveryReqReceived(MM7DeliveryReqModel model)
            {

            }

            @Override
            public void onDeliveryReportReqReceived(MM7DeliveryReportReqModel model)
            {

            }

            @Override
            public void onReadReply(MM7ReadReplyReqModel mm7ReadReplyReqModel)
            {

            }

            @Override
            public void onError(MM7ErrorModel model)
            {

            }
        });
    }

    @Test
    public void deliverTest() throws MM7Error
    {
        DeliverReq deliverReq = new DeliverReq();
        DeliverRsp resp = vasp.deliver(deliverReq);

        assertNotNull(resp);
        validateResp(resp);
        assertEquals(1, callCount1);
        assertEquals(0, callCount2);
        assertEquals(0, callCount3);
        assertEquals(0, callCount4);
    }

    @Test(expected = AssertionError.class)
    public void deliver_null_Test() throws MM7Error
    {
        DeliverRsp resp = vasp.deliver(null);
    }

    @Test
    public void getContext()
    {
        MM7Context context = vasp.getContext();
        assertNotNull(context);
    }

    @Test
    public void handleMm7Error()
    {
        MM7Error error = new MM7Error();
        vasp.handleMm7Error(error);

        assertEquals(0, callCount1);
        assertEquals(0, callCount2);
        assertEquals(0, callCount3);
        assertEquals(1, callCount4);
    }

    @Test
    public void deliveryReportTest() throws MM7Error
    {
        DeliveryReportReq deliveryReportReq = new DeliveryReportReq();
        DeliveryReportRsp resp = vasp.deliveryReport(deliveryReportReq);

        assertNotNull(resp);
        validateResp(resp);
        assertEquals(0, callCount1);
        assertEquals(1, callCount2);
        assertEquals(0, callCount3);
        assertEquals(0, callCount4);
    }

    @Test(expected = AssertionError.class)
    public void deliveryReport_null_Test() throws MM7Error
    {
        DeliveryReportRsp resp = vasp.deliveryReport(null);
    }

    @Test
    public void readReplyTest() throws MM7Error
    {
        ReadReplyReq readReplyReq = new ReadReplyReq();
        ReadReplyRsp resp = vasp.readReply(readReplyReq);

        assertNotNull(resp);
        validateResp(resp);
        assertEquals(0, callCount1);
        assertEquals(0, callCount2);
        assertEquals(1, callCount3);
        assertEquals(0, callCount4);
    }

    @Test(expected = AssertionError.class)
    public void readReply_null_Test() throws MM7Error
    {
        ReadReplyRsp resp = vasp.readReply(null);
    }

    private void validateResp(MM7Response resp)
    {
        assertEquals(MM7Response.SC_SUCCESS, resp.getStatusCode());
    }
}
