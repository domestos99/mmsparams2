package cz.uhk.dip.mmsparams.server.mock;

import java.util.List;
import java.util.Optional;

import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDetailDTO;
import cz.uhk.dip.mmsparams.server.database.services.IServerTestInstanceDBService;
import cz.uhk.dip.mmsparams.server.listeners.OnChangeListener;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

public class MockServerTestInstanceDBService implements IServerTestInstanceDBService
{
    @Override
    public int saveClosedTest(ServerTestInstance test)
    {
        return 0;
    }

    @Override
    public List<ServerTestInstanceDetailDTO> getAll()
    {
        return null;
    }

    @Override
    public Optional<ServerTestInstanceDetailDTO> getByTestId(String testId)
    {
        return Optional.empty();
    }

    @Override
    public boolean delete(String testId)
    {
        return false;
    }

    @Override
    public void setOnChangeListener(OnChangeListener<ServerTestInstanceDetailDTO> onChangeListener)
    {

    }

    @Override
    public List<ServerTestInstanceDTO> getReduced()
    {
        return null;
    }

    @Override
    public boolean deleteOldTests(int days)
    {
        return false;
    }


}
