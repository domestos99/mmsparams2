package cz.uhk.dip.mmsparams.server.mock;

import net.instantcom.mm7.DeliverReq;
import net.instantcom.mm7.DeliverRsp;
import net.instantcom.mm7.DeliveryReportReq;
import net.instantcom.mm7.DeliveryReportRsp;
import net.instantcom.mm7.MM7Context;
import net.instantcom.mm7.MM7Error;
import net.instantcom.mm7.MM7Response;
import net.instantcom.mm7.ReadReplyReq;
import net.instantcom.mm7.ReadReplyRsp;
import net.instantcom.mm7.VASP;

public class MockVaspService implements VASP
{
    @Override
    public DeliverRsp deliver(DeliverReq deliverReq) throws MM7Error
    {
        DeliverRsp deliverRsp = new DeliverRsp();
        deliverRsp.setStatusCode(MM7Response.SC_SUCCESS);
        deliverRsp.setStatusText(deliverRsp.getStatusText());

        return deliverRsp;
    }

    @Override
    public DeliveryReportRsp deliveryReport(DeliveryReportReq deliveryReportReq) throws MM7Error
    {
        DeliveryReportRsp deliveryReportRsp = new DeliveryReportRsp();
        deliveryReportRsp.setStatusCode(MM7Response.SC_SUCCESS);
        deliveryReportRsp.setStatusText(deliveryReportRsp.getStatusText());

        return deliveryReportRsp;
    }

    @Override
    public ReadReplyRsp readReply(ReadReplyReq readReplyReq) throws MM7Error
    {
        return null;
    }

    @Override
    public MM7Context getContext()
    {
        return new MM7Context();
    }

    @Override
    public void handleMm7Error(MM7Error mm7error)
    {

    }
}
