package cz.uhk.dip.mmsparams.server.mocks;

import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReportReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ErrorModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ReadReplyReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;
import cz.uhk.dip.mmsparams.server.mmsc.IMmscOnReceiveListener;
import cz.uhk.dip.mmsparams.server.mmsc.IMmscServiceFacade;

public class MockMmscServiceFacade implements IMmscServiceFacade
{
    public MockMmscServiceFacade()
    {


    }

    @Override
    public MM7SubmitResponseModel send(MmscSendMessage msg) throws Exception
    {
        return new MM7SubmitResponseModel();
    }

    @Override
    public void onDeliveryReqReceived(MM7DeliveryReqModel model)
    {

    }

    @Override
    public void onDeliveryReportReqReceived(MM7DeliveryReportReqModel model)
    {

    }

    @Override
    public void onReadReply(MM7ReadReplyReqModel mm7ReadReplyReqModel)
    {

    }

    @Override
    public void onError(MM7ErrorModel model)
    {

    }

    @Override
    public void setOnReceiveListener(IMmscOnReceiveListener mmscOnReceiveListener)
    {

    }
}
