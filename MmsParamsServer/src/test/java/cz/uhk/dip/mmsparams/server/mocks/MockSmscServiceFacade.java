package cz.uhk.dip.mmsparams.server.mocks;

import com.cloudhopper.smpp.SmppSession;

import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.server.smsc.ISmscServiceFacade;
import cz.uhk.dip.mmsparams.smsc.ISmscReceiveListener;

public class MockSmscServiceFacade implements ISmscServiceFacade
{
    public MockSmscServiceFacade()
    {

    }

    @Override
    public SmppSession connect(SmscConnectMessage msg, ISmscReceiveListener iSmscReceiveListener) throws Exception
    {
        return new MockSmscSession(iSmscReceiveListener);
    }

    @Override
    public SmscSendSmsResponseModel send(SmppSession smscSession, SmscSendSmsMessage msg) throws Exception
    {
        return new SmscSendSmsResponseModel();
    }

    @Override
    public void disconnect(SmppSession smscSession)
    {

    }
}
