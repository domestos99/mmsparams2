package cz.uhk.dip.mmsparams.server.mocks;

import com.cloudhopper.commons.util.windowing.Window;
import com.cloudhopper.commons.util.windowing.WindowFuture;
import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.SmppSessionCounters;
import com.cloudhopper.smpp.pdu.EnquireLink;
import com.cloudhopper.smpp.pdu.EnquireLinkResp;
import com.cloudhopper.smpp.pdu.PduRequest;
import com.cloudhopper.smpp.pdu.PduResponse;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.pdu.SubmitSmResp;
import com.cloudhopper.smpp.type.RecoverablePduException;
import com.cloudhopper.smpp.type.SmppChannelException;
import com.cloudhopper.smpp.type.SmppTimeoutException;
import com.cloudhopper.smpp.type.UnrecoverablePduException;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.smsc.ISmscReceiveListener;

public class MockSmscSession implements SmppSession
{
    private final ISmscReceiveListener smscReceiveListener;

    public MockSmscSession(ISmscReceiveListener smscReceiveListener)
    {
        this.smscReceiveListener = smscReceiveListener;
    }

    public void mockOnDeliverySm(final SmscDeliverSmModel deliverSmModel)
    {
        smscReceiveListener.onDeliverySm(deliverSmModel);
    }
    public void mockOnDeliveryReport(final SmscDeliveryReportModel deliveryReportModel)
    {
        smscReceiveListener.onDeliveryReport(deliveryReportModel);
    }

    @Override
    public SmppBindType getBindType()
    {
        return null;
    }

    @Override
    public Type getLocalType()
    {
        return null;
    }

    @Override
    public Type getRemoteType()
    {
        return null;
    }

    @Override
    public SmppSessionConfiguration getConfiguration()
    {
        return null;
    }

    @Override
    public String getStateName()
    {
        return null;
    }

    @Override
    public byte getInterfaceVersion()
    {
        return 0;
    }

    @Override
    public boolean areOptionalParametersSupported()
    {
        return false;
    }

    @Override
    public boolean isOpen()
    {
        return false;
    }

    @Override
    public boolean isBinding()
    {
        return false;
    }

    @Override
    public boolean isBound()
    {
        return false;
    }

    @Override
    public boolean isUnbinding()
    {
        return false;
    }

    @Override
    public boolean isClosed()
    {
        return false;
    }

    @Override
    public long getBoundTime()
    {
        return 0;
    }

    @Override
    public Window<Integer, PduRequest, PduResponse> getRequestWindow()
    {
        return null;
    }

    @Override
    public Window<Integer, PduRequest, PduResponse> getSendWindow()
    {
        return null;
    }

    @Override
    public boolean hasCounters()
    {
        return false;
    }

    @Override
    public SmppSessionCounters getCounters()
    {
        return null;
    }

    @Override
    public void close()
    {

    }

    @Override
    public void unbind(long timeoutMillis)
    {

    }

    @Override
    public void destroy()
    {

    }

    @Override
    public EnquireLinkResp enquireLink(EnquireLink request, long timeoutMillis) throws RecoverablePduException, UnrecoverablePduException, SmppTimeoutException, SmppChannelException, InterruptedException
    {
        return null;
    }

    @Override
    public SubmitSmResp submit(SubmitSm request, long timeoutMillis) throws RecoverablePduException, UnrecoverablePduException, SmppTimeoutException, SmppChannelException, InterruptedException
    {
        return null;
    }

    @Override
    public WindowFuture<Integer, PduRequest, PduResponse> sendRequestPdu(PduRequest request, long timeoutMillis, boolean synchronous) throws RecoverablePduException, UnrecoverablePduException, SmppTimeoutException, SmppChannelException, InterruptedException
    {
        return null;
    }


    @Override
    public void sendResponsePdu(PduResponse response) throws RecoverablePduException, UnrecoverablePduException, SmppChannelException, InterruptedException
    {

    }
}
