package cz.uhk.dip.mmsparams.server.mocks;

import org.springframework.http.HttpHeaders;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketExtension;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cz.uhk.dip.mmsparams.api.websocket.MessageType;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

import static org.junit.Assert.assertEquals;

public class MockWebSocketSession implements WebSocketSession
{
    private final String id;

    public MockWebSocketSession()
    {
        id = UUID.randomUUID().toString();
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public URI getUri()
    {
        return null;
    }

    @Override
    public HttpHeaders getHandshakeHeaders()
    {
        return null;
    }

    @Override
    public Map<String, Object> getAttributes()
    {
        return null;
    }

    @Override
    public Principal getPrincipal()
    {
        return null;
    }

    @Override
    public InetSocketAddress getLocalAddress()
    {
        return null;
    }

    @Override
    public InetSocketAddress getRemoteAddress()
    {
        return null;
    }

    @Override
    public String getAcceptedProtocol()
    {
        return null;
    }

    @Override
    public void setTextMessageSizeLimit(int messageSizeLimit)
    {

    }

    @Override
    public int getTextMessageSizeLimit()
    {
        return 0;
    }

    @Override
    public void setBinaryMessageSizeLimit(int messageSizeLimit)
    {

    }

    @Override
    public int getBinaryMessageSizeLimit()
    {
        return 0;
    }

    @Override
    public List<WebSocketExtension> getExtensions()
    {
        return null;
    }


    public List<WebSocketMessageBase> messages = new ArrayList<>();

    public <T extends WebSocketMessageBase> T getMessageFromMessagesListLast(Class<T> clz)
    {
        WebSocketMessageBase msg = this.messages.get(this.messages.size() - 1);
        assertEquals(clz, msg.getClass());
        assertEquals(MessageType.getKeyByClass(clz), msg.getMessageKey());
        return (T) msg;
    }

    public <T extends WebSocketMessageBase> T getMessageFromMessagesList(int index, Class<T> clz)
    {
        WebSocketMessageBase msg = this.messages.get(index);
        assertEquals(clz, msg.getClass());
        assertEquals(MessageType.getKeyByClass(clz), msg.getMessageKey());
        return (T) msg;
    }

    @Override
    public void sendMessage(WebSocketMessage<?> message) throws IOException
    {
        WebSocketMessageBase msg = MessageUtils.getMessageNonTyped(message.getPayload().toString());
        messages.add(msg);
    }

    @Override
    public boolean isOpen()
    {
        return true;
    }

    @Override
    public void close() throws IOException
    {

    }

    @Override
    public void close(CloseStatus status) throws IOException
    {

    }

    public void checkMessageCount(int expectedCount)
    {
        assertEquals(expectedCount, messages.size());
    }
}
