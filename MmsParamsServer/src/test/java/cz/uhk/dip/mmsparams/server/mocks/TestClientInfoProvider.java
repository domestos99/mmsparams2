package cz.uhk.dip.mmsparams.server.mocks;

import cz.uhk.dip.mmsparams.api.interfaces.IClientInfoProvider;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;

public class TestClientInfoProvider implements IClientInfoProvider
{
    @Override
    public ClientInfo getClientInfo()
    {
        ClientInfo gi = new ClientInfo();

        gi.setClientKey("groovy-key");
        gi.setComputerName("test-pc-name");
        gi.setCurrentUserName("best-user");

        return gi;
    }
}
