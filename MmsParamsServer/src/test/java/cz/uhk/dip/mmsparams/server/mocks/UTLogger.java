package cz.uhk.dip.mmsparams.server.mocks;


public class UTLogger
{
    public static void error(String s, Exception e)
    {
        System.out.println(s);
        e.printStackTrace();
    }

    public static void info(String s, String systemId)
    {
        System.out.println(s);
    }

    public static void info(String s, Exception ex)
    {
        System.out.println(s);
        ex.printStackTrace();
    }

//    public static void info(String s, String systemId, DeliveryReceipt dr)
//    {
//        System.out.println(s + " --- " + systemId + "---" + dr);
//    }

    public static void info(String s, String host, int port)
    {
        System.out.println(s);
    }

    public static void error(String s)
    {
        System.out.println(s);
    }

    public static void info(String s)
    {
        System.out.println(s);
    }
}
