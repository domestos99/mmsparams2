package cz.uhk.dip.mmsparams.server.pipeline;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.testSuites.PipelineTestCategory;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@Category(PipelineTestCategory.class)
public class ClientLibRegisterPipelineTest extends PipelineTestBase
{
    @Before
    public void before()
    {
        super.testBeforeBase();
    }

    @Test
    public void test1() throws Exception
    {
        MockWebSocketSession clientSession = getNewSession();

        handler.afterConnectionEstablished(clientSession);

        checkOneSession(iSessionManager, clientSession);

        RegisterClientLibMessage msg =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .withRecipientKeyServer()
                        .withSenderKey(getClientKey())
                        .withMessageIdRandom()
                        .build(RegisterClientLibMessage.class);


        ClientInfo gi = new ClientInfo();

        gi.setClientKey("groovy-key");
        gi.setComputerName("test-pc-name");
        gi.setCurrentUserName("best-user");

        msg.setClientInfo(gi);


        sendMessage(clientSession, msg);

        checkOneSession(iSessionManager, clientSession);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), 1);


        ServerTestInstance test = testRegister.getTestInstance(getTestId());
        assertNotNull(test);
        assertFalse(test.isClosed());
        assertNotNull(test.getClientInfo());
        assertEquals(gi.getClientKey(), test.getClientInfo().getClientKey());
        assertEquals(gi.getClientKey(), test.getClientLibInstance().getClientInfo().getClientKey());
        assertEquals(clientSession.getId(), test.getClientLibInstance().getClientSession().getId());

    }


}
