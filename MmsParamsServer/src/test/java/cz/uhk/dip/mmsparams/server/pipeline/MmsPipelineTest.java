package cz.uhk.dip.mmsparams.server.pipeline;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.MmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.NotificationIndResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.RetrieveConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mms.pdus.SendConfResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotificationIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.mocks.TestClientInfoProvider;
import cz.uhk.dip.mmsparams.server.testSuites.PipelineTestCategory;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@Category(PipelineTestCategory.class)
public class MmsPipelineTest extends PipelineTestBase
{
    static String numFrom = "+420602301498";
    static String numTo = "+420602334928";

    @Before
    public void before()
    {
        super.testBefore();
    }

    @Test
    public void test1() throws Exception
    {
        int expectedMessageCount = 0;
        MockWebSocketSession phoneSession1 = getNewSession();

        handler.afterConnectionEstablished(phoneSession1);

        checkOneSession(iSessionManager, phoneSession1);

        RegisterPhoneMessage registerPhoneMessage1 =
                new MessageBuilder()
                        .withNoTestId()
                        .withRecipientKeyServer()
                        .withSenderKey(getPhoneKey(1))
                        .withMessageIdRandom()
                        .build(RegisterPhoneMessage.class);


        PhoneInfoModel di1 = new PhoneInfoModel();
        di1.setPhoneKey("phone-key");
        registerPhoneMessage1.setPhoneInfoModel(di1);

        sendMessage(phoneSession1, registerPhoneMessage1);

        checkSessionsCount(1);
        checkOneDevice(iSessionManager, di1);
        phoneSession1.checkMessageCount(1);
        phoneSession1.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);


        MockWebSocketSession phoneSession2 = getNewSession();

        handler.afterConnectionEstablished(phoneSession2);

        checkSessionsCount(2);

        RegisterPhoneMessage registerPhoneMessage2 =
                new MessageBuilder()
                        .withNoTestId()
                        .withRecipientKeyServer()
                        .withSenderKey(getPhoneKey(2))
                        .withMessageIdRandom()
                        .build(RegisterPhoneMessage.class);


        PhoneInfoModel di2 = new PhoneInfoModel();
        di2.setPhoneKey("phone-key-recipient");
        registerPhoneMessage2.setPhoneInfoModel(di2);

        sendMessage(phoneSession2, registerPhoneMessage2);

        checkSessionsCount(2);
        checkDeviceCount(2);
        phoneSession2.checkMessageCount(1);
        phoneSession2.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);


        MockWebSocketSession clientSession = getNewSession();

        handler.afterConnectionEstablished(clientSession);

        ClientInfo gi = new TestClientInfoProvider().getClientInfo();

        RegisterClientLibMessage msg = getRegisterClientLibMessage();

        sendMessage(clientSession, msg);
        expectedMessageCount++;

        checkSessionsCount(3);
        clientSession.checkMessageCount(1);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);
        clientSession.checkMessageCount(1);
        clientSession.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);


        ServerTestInstance test = testRegister.getTestInstance(getTestId());
        assertNotNull(test);
        assertFalse(test.isClosed());
        assertNotNull(test.getClientInfo());
        assertEquals(gi.getClientKey(), test.getClientInfo().getClientKey());
        assertEquals(gi.getClientKey(), test.getClientLibInstance().getClientInfo().getClientKey());
        assertEquals(clientSession.getId(), test.getClientLibInstance().getClientSession().getId());

        // Lock devices
        LockPhoneMessage lockPhoneMessage =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .withRecipientKeyServer()
                        .withSenderKey(getClientKey())
                        .withMessageIdRandom()
                        .build(LockPhoneMessage.class);

        lockPhoneMessage.setPhoneInfoModel(di1);

        sendMessage(clientSession, lockPhoneMessage);
        expectedMessageCount++;

        checkSessionsCount(3);
        checkOneClient(iSessionManager, gi);
        clientSession.checkMessageCount(2);
        clientSession.checkMessageCount(2);
        clientSession.getMessageFromMessagesList(1, GenericBooleanResponseMessage.class);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);

        LockPhoneMessage lockPhoneMessage2 =
                new MessageBuilder().withTestId(iTestIdProvider)
                        .withRecipientKeyServer()
                        .withSenderKey(getClientKey())
                        .withMessageIdRandom()
                        .build(LockPhoneMessage.class);

        lockPhoneMessage2.setPhoneInfoModel(di2);
        sendMessage(clientSession, lockPhoneMessage2);
        expectedMessageCount++;

        checkSessionsCount(3);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);
        clientSession.checkMessageCount(3);
        clientSession.getMessageFromMessagesList(2, GenericBooleanResponseMessage.class);

        // Send MMS
        MmsSendModel mmsSendModel = createMms();
        MmsSendPhoneRequestMessage smsSendPhoneRequestMessage =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .withSenderKey(gi.getClientKey())
                        .withRecipientKey(di1.getPhoneKey())
                        .withMessageIdRandom()
                        .build(MmsSendPhoneRequestMessage.class);


        smsSendPhoneRequestMessage.setMmsSendModel(mmsSendModel);


        sendMessage(clientSession, smsSendPhoneRequestMessage);
        expectedMessageCount++;

        checkSessionsCount(3);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);

        phoneSession1.checkMessageCount(2);


        // Emulate android device - sender
        SendConfResponseMessage sendConfMessage =
                new MessageBuilder()
                        .withNoTestId()
                        .withSenderKey(di1.getPhoneKey())
                        .withPrepareResponse(smsSendPhoneRequestMessage)
                        .build(SendConfResponseMessage.class);

        SendConfModel sendConfModel = new SendConfModel();
        sendConfMessage.setSendConfModel(sendConfModel);

        sendMessage(phoneSession1, sendConfMessage);
        expectedMessageCount++;

        clientSession.checkMessageCount(4);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);


        NotificationIndResponseMessage notificationIndResponseMessage =
                new MessageBuilder()
                        .withRecipientKeyClientBroadcast()
                        .withMessageIdRandom()
                        .build(NotificationIndResponseMessage.class);


        NotificationIndModel notificationIndModel = new NotificationIndModel();
        notificationIndResponseMessage.setNotificationIndModel(notificationIndModel);


        sendMessage(phoneSession1, sendConfMessage);
        expectedMessageCount++;
        clientSession.checkMessageCount(5);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);


        RetrieveConfResponseMessage retrieveMessage =
                new MessageBuilder()
                        .withRecipientKeyClientBroadcast()
                        .withMessageIdRandom()
                        .build(RetrieveConfResponseMessage.class);


        RetrieveConfModel retrieveConfModel = new RetrieveConfModel();
        retrieveMessage.setRetrieveConfModel(retrieveConfModel);

        sendMessage(phoneSession1, sendConfMessage);
        expectedMessageCount++;
        clientSession.checkMessageCount(6);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);


    }

    private static MmsSendModel createMms()
    {
        MmsSendModel sms = new MmsSendModel();
        sms.addTo(numTo);
        sms.setText("Hello world");
        sms.setDeliveryReport(true);

        return sms;
    }
}
