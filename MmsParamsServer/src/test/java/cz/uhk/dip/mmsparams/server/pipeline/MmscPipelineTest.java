package cz.uhk.dip.mmsparams.server.pipeline;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscSendMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.mocks.TestClientInfoProvider;
import cz.uhk.dip.mmsparams.server.testSuites.PipelineTestCategory;

import static org.junit.Assert.assertEquals;

@Category(PipelineTestCategory.class)
public class MmscPipelineTest extends PipelineTestBase
{
    @Before
    public void before()
    {
        super.testBefore();
    }

    @Test
    public void test1() throws Exception
    {
        int expectedMessageCount = 0;

        MockWebSocketSession clientSession = getNewSession();
        handler.afterConnectionEstablished(clientSession);
        checkSessionsCount(1);

        RegisterClientLibMessage msg =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .withSenderKey(getClientKey())
                        .withRecipientKeyServer()
                        .withMessageIdRandom()
                        .build(RegisterClientLibMessage.class);

        ClientInfo gi = new TestClientInfoProvider().getClientInfo();
        msg.setClientInfo(gi);

        sendMessage(clientSession, msg);
        expectedMessageCount++;
        checkSessionsCount(1);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);
        assertEquals(1, clientSession.messages.size());


        MmscSendModel mmscSendModel = new MmscSendModel();

        MmscSendMessage mmscSendMessage =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .withSenderKey(getClientKey())
                        .withRecipientKeyServer()
                        .withMessageIdRandom()
                        .build(MmscSendMessage.class);

        mmscSendMessage.setMmscSendModel(mmscSendModel);

        sendMessage(clientSession, mmscSendMessage);
        expectedMessageCount++;

        checkSessionsCount(1);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);

        assertEquals(2, clientSession.messages.size());


    }

}
