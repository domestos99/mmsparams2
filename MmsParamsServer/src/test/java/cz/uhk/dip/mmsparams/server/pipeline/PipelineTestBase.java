package cz.uhk.dip.mmsparams.server.pipeline;

import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.AppVersion;
import cz.uhk.dip.mmsparams.api.json.JsonUtils;
import cz.uhk.dip.mmsparams.api.messages.MessageRegisterItem;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.logging.WebSocketLogger;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.mocks.TestClientInfoProvider;
import cz.uhk.dip.mmsparams.server.testSuites.PipelineTestCategory;
import cz.uhk.dip.mmsparams.server.tests.TestRegister;
import cz.uhk.dip.mmsparams.server.websocket.IWSServicesContainer;
import cz.uhk.dip.mmsparams.server.websocket.SimpleServerWebSocketHandler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Category(PipelineTestCategory.class)
public abstract class PipelineTestBase extends UnitTestBase
{
    TestRegister testRegister;
    WebSocketLogger webSocketLogger;
    IWSServicesContainer iSessionManager;
    //    ISmscServiceFacade iSmscServiceFacade;
//    IMmscServiceFacade iMmscServiceFacade;
    SimpleServerWebSocketHandler handler;


    @Before
    public void testBefore()
    {
        super.testBeforeBase();

        testRegister = new TestRegister(getTestServerTestInstanceDBService());
        webSocketLogger = new WebSocketLogger();
//
//        iSmscServiceFacade = new MockSmscServiceFacade();
//        iMmscServiceFacade = new MockMmscServiceFacade();

        iSessionManager = createSessionManager(webSocketLogger, testRegister);
        // new SessionManager(webSocketLogger, testRegister, iSmscServiceFacade, iMmscServiceFacade);


        handler = new SimpleServerWebSocketHandler(iSessionManager, webSocketLogger);
    }

    protected void registerPhone(String phoneKey) throws Exception
    {
        MockWebSocketSession phoneSession = getNewSession();

        handler.afterConnectionEstablished(phoneSession);

        checkOneSession(iSessionManager, phoneSession);

        RegisterPhoneMessage registerPhoneMessage =
                new MessageBuilder()
                        .withNoTestId()
                        .withSenderKey(getPhoneKey(1))
                        .withRecipientKeyServer()
                        .withMessageIdRandom()
                        .build(RegisterPhoneMessage.class);


        PhoneInfoModel di = new PhoneInfoModel();
        di.setPhoneKey(phoneKey);
        registerPhoneMessage.setPhoneInfoModel(di);

        sendMessage(phoneSession, registerPhoneMessage);

        checkOneSession(iSessionManager, phoneSession);

        checkOneDevice(iSessionManager, di);
    }

    protected void checkSessionsCount(int expectedCount)
    {
        List<WebSocketSession> sessionList = iSessionManager.getSessionWSController().getOpenSessions();
        assertNotNull(sessionList);
        assertEquals(expectedCount, sessionList.size());
    }

    protected void checkOneSession(IWSServicesContainer iSessionManager, MockWebSocketSession phoneSession)
    {
        List<WebSocketSession> sessionList = iSessionManager.getSessionWSController().getOpenSessions();
        assertNotNull(sessionList);
        assertEquals(1, sessionList.size());
        assertEquals(phoneSession.getId(), sessionList.get(0).getId());
    }

    protected void checkOneDevice(IWSServicesContainer iSessionManager, PhoneInfoModel phoneInfoModel)
    {
        List<PhoneInfoModel> deviceList = iSessionManager.getPhoneWSController().getConnectedPhones();
        assertNotNull(deviceList);
        assertEquals(1, deviceList.size());
        assertEquals(phoneInfoModel.getPhoneKey(), deviceList.get(0).getPhoneKey());
    }

    protected void checkDeviceCount(int expectedCount)
    {
        List<PhoneInfoModel> deviceList = iSessionManager.getPhoneWSController().getConnectedPhones();
        assertNotNull(deviceList);
        assertEquals(expectedCount, deviceList.size());
    }

    protected void checkOneClient(IWSServicesContainer iSessionManager, ClientInfo gi)
    {
        List<ClientInfo> clients = iSessionManager.getClientLibWSController().getConnectedClients();
        assertNotNull(clients);
        assertEquals(1, clients.size());
        assertEquals(gi.getClientKey(), clients.get(0).getClientKey());
    }

    protected void checkTestRegisterMessagesCount(String testId, int expectedCount)
    {
        List<MessageRegisterItem> messages = testRegister.getIncomingMessages(testId);
        assertNotNull(messages);
        assertEquals(expectedCount, messages.size());
    }

    protected void sendMessage(WebSocketSession session, WebSocketMessageBase msg) throws Exception
    {
        String json = JsonUtils.toJson(msg);
        handler.handleMessage(session, new TextMessage(json));
    }

    protected String getTestId()
    {
        return iTestIdProvider.getTestId();
    }

    protected String getClientKey()
    {
        return "client-key";
    }

    protected String getPhoneKey(int id)
    {
        return "phone-key_" + id;
    }

    protected LockPhoneMessage getLockPhoneMessage(PhoneInfoModel di1)
    {
        LockPhoneMessage msg = new MessageBuilder()
                .withTestId(iTestIdProvider)
                .withSenderKey(getClientKey())
                .withRecipientKeyServer()
                .withMessageIdRandom()
                .build(LockPhoneMessage.class);

        msg.setPhoneInfoModel(di1);

        return msg;
    }

    protected UnLockPhoneMessage getUnLockPhoneMessage(PhoneInfoModel di1)
    {
        UnLockPhoneMessage msg = new MessageBuilder()
                .withTestId(iTestIdProvider)
                .withSenderKey(getClientKey())
                .withRecipientKeyServer()
                .withMessageIdRandom()
                .build(UnLockPhoneMessage.class);

        msg.setPhoneInfoModel(di1);

        return msg;
    }

    protected RegisterClientLibMessage getRegisterClientLibMessage()
    {
        RegisterClientLibMessage msg = new MessageBuilder()
                .withTestId(iTestIdProvider)
                .withSenderKey(getClientKey())
                .withRecipientKeyServer()
                .withMessageIdRandom()
                .build(RegisterClientLibMessage.class);
        msg.setClientInfo(new TestClientInfoProvider().getClientInfo());
        msg.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);
        return msg;
    }

    protected RegisterPhoneMessage getRegisterPhoneMessage(int id)
    {
        return new MessageBuilder()
                .withNoTestId()
                .withSenderKey(getPhoneKey(id))
                .withRecipientKeyServer()
                .withMessageIdRandom()
                .build(RegisterPhoneMessage.class);
    }

    protected ClientInfo getClientInfo()
    {
        ClientInfo ci = new ClientInfo();
        ci.setClientKey(getClientKey());
        return ci;
    }

    protected SmscConnectMessage getSmscConnectMessage()
    {
        return new MessageBuilder()
                .withTestId(iTestIdProvider)
                .withRecipientKeyServer()
                .withSenderKey(getClientKey())
                .withMessageIdRandom()
                .build(SmscConnectMessage.class);
    }

}
