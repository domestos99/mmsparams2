package cz.uhk.dip.mmsparams.server.pipeline;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.testSuites.PipelineTestCategory;

import static org.junit.Assert.assertEquals;

@Category(PipelineTestCategory.class)
public class RequestDeviceListPipelineTest extends PipelineTestBase
{
    @Before
    public void before()
    {
        super.testBefore();
    }

    @Test
    public void test1() throws Exception
    {
        registerPhone("phone-key");

        MockWebSocketSession clientSession = getNewSession();
        handler.afterConnectionEstablished(clientSession);
        checkSessionsCount(2);

        RegisterClientLibMessage msg = getRegisterClientLibMessage();

        sendMessage(clientSession, msg);
        assertEquals(1, clientSession.messages.size());

        PhoneListRequestMessage phoneListRequestMessage =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .withSenderKey(getClientKey())
                        .withRecipientKeyServer()
                        .withMessageIdRandom()
                        .build(PhoneListRequestMessage.class);

        sendMessage(clientSession, phoneListRequestMessage);

        checkSessionsCount(2);
        checkOneClient(iSessionManager, msg.getClientInfo());
        checkTestRegisterMessagesCount(getTestId(), 2);

        assertEquals(2, clientSession.messages.size());


    }


}
