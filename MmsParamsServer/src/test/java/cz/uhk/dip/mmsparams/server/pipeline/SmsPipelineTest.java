package cz.uhk.dip.mmsparams.server.pipeline;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsReceivePhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.mocks.TestClientInfoProvider;
import cz.uhk.dip.mmsparams.server.testSuites.PipelineTestCategory;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@Category(PipelineTestCategory.class)
public class SmsPipelineTest extends PipelineTestBase
{
    static String numFrom = "+420602301498";
    static String numTo = "+420602334928";

    @Before
    public void before()
    {
        super.testBefore();
    }

    @Test
    public void test1() throws Exception
    {
        int expectedMessageCount = 0;
        MockWebSocketSession phoneSession1 = getNewSession();

        handler.afterConnectionEstablished(phoneSession1);

        checkOneSession(iSessionManager, phoneSession1);

        RegisterPhoneMessage registerPhoneMessage1 = getRegisterPhoneMessage(1);

        PhoneInfoModel di1 = new PhoneInfoModel();
        di1.setPhoneKey("phone-key");
        registerPhoneMessage1.setPhoneInfoModel(di1);

        sendMessage(phoneSession1, registerPhoneMessage1);

        checkSessionsCount(1);
        checkOneDevice(iSessionManager, di1);

        MockWebSocketSession phoneSession2 = getNewSession();

        handler.afterConnectionEstablished(phoneSession2);

        checkSessionsCount(2);

        RegisterPhoneMessage registerPhoneMessage2 = getRegisterPhoneMessage(2);

        PhoneInfoModel di2 = new PhoneInfoModel();
        di2.setPhoneKey("phone-key-recipient");
        registerPhoneMessage2.setPhoneInfoModel(di2);

        sendMessage(phoneSession2, registerPhoneMessage2);

        checkSessionsCount(2);
        checkDeviceCount(2);


        MockWebSocketSession clientSession = getNewSession();

        handler.afterConnectionEstablished(clientSession);

        ClientInfo gi = new TestClientInfoProvider().getClientInfo();
        RegisterClientLibMessage msg = getRegisterClientLibMessage();

        sendMessage(clientSession, msg);
        expectedMessageCount++;

        checkSessionsCount(3);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);
        clientSession.checkMessageCount(1);
        clientSession.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);


        ServerTestInstance test = testRegister.getTestInstance(getTestId());
        assertNotNull(test);
        assertFalse(test.isClosed());
        assertNotNull(test.getClientInfo());
        assertEquals(gi.getClientKey(), test.getClientInfo().getClientKey());
        assertEquals(gi.getClientKey(), test.getClientLibInstance().getClientInfo().getClientKey());
        assertEquals(clientSession.getId(), test.getClientLibInstance().getClientSession().getId());

        // Lock devices
        LockPhoneMessage lockPhoneMessage = getLockPhoneMessage(di1);

        sendMessage(clientSession, lockPhoneMessage);
        expectedMessageCount++;

        checkSessionsCount(3);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);

        LockPhoneMessage lockPhoneMessage2 = getLockPhoneMessage(di2);
        sendMessage(clientSession, lockPhoneMessage2);
        expectedMessageCount++;

        checkSessionsCount(3);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);



        SmsSendModel sms = createSms1();
        SmsSendPhoneRequestMessage smsSendPhoneRequestMessage =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .withSenderKey(gi.getClientKey())
                        .withRecipientKey(di1.getPhoneKey())
                        .withMessageIdRandom()
                        .build(SmsSendPhoneRequestMessage.class);


        smsSendPhoneRequestMessage.setSmsSendModel(sms);


        sendMessage(clientSession, smsSendPhoneRequestMessage);
        expectedMessageCount++;

        checkSessionsCount(3);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);

        assertEquals(2, phoneSession1.messages.size());


        // Emulate android device - sender
        SmsSendPhoneResponseMessage smsSendPhoneresponseMessage =
                new MessageBuilder()
                        .withNoTestId()
                        .withSenderKey(di1.getPhoneKey())
                        .withPrepareResponse(smsSendPhoneRequestMessage)
                        .build(SmsSendPhoneResponseMessage.class);


        SmsSendResponseModel responseModel = new SmsSendResponseModel();
        responseModel.setResult(-1);
        smsSendPhoneresponseMessage.setSmsSendResponseModel(responseModel);

        sendMessage(phoneSession1, smsSendPhoneresponseMessage);
        expectedMessageCount++;

        assertEquals(4, clientSession.messages.size());
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);


        // Emulate android device - recipient
        SmsReceivePhoneMessage smsReceivePhoneMessage =
                new MessageBuilder()
                        .withNoTestId()
                        .withRecipientKeyClientBroadcast()
                        .withSenderKey(di2.getPhoneKey())
                        .withMessageIdRandom()
                        .build(SmsReceivePhoneMessage.class);

        SmsReceiveModel smsReceiveModel = new SmsReceiveModel();
        smsReceivePhoneMessage.setSmsReceiveModel(smsReceiveModel);

        sendMessage(phoneSession2, smsReceivePhoneMessage);
        expectedMessageCount++;


        assertEquals(5, clientSession.messages.size());
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);


    }


    private static SmsSendModel createSms1()
    {
        SmsSendModel sms = new SmsSendModel();
        sms.setTo(numTo);
        sms.setText("Hello world");
        sms.setDeliveryReport(true);
        return sms;
    }
}
