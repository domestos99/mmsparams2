package cz.uhk.dip.mmsparams.server.pipeline;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscConnectResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscSendSmsResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.server.mocks.MockSmscSession;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.mocks.TestClientInfoProvider;
import cz.uhk.dip.mmsparams.server.testSuites.PipelineTestCategory;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Category(PipelineTestCategory.class)
public class SmscPipelineTest extends PipelineTestBase
{
    @Before
    public void before()
    {
        super.testBefore();
    }

    @Test
    public void test1() throws Exception
    {
        int expectedMessageCount = 0;

        MockWebSocketSession clientSession = getNewSession();
        handler.afterConnectionEstablished(clientSession);
        checkSessionsCount(1);

        RegisterClientLibMessage msg = getRegisterClientLibMessage();
        ClientInfo gi = new TestClientInfoProvider().getClientInfo();

        msg.setClientInfo(gi);

        sendMessage(clientSession, msg);
        expectedMessageCount++;
        checkSessionsCount(1);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);
        assertEquals(1, clientSession.messages.size());


        SmscConnectMessage smscConnectMessage =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .withSenderKey(getClientKey())
                        .withRecipientKeyServer()
                        .withMessageIdRandom()
                        .build(SmscConnectMessage.class);


        SmscConnectModel smscConnectModel = new SmscConnectModel();
        smscConnectMessage.setSmscSendSmsModel(smscConnectModel);


        sendMessage(clientSession, smscConnectMessage);
        expectedMessageCount++;

        checkSessionsCount(1);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);

        assertEquals(2, clientSession.messages.size());

        SmscConnectResponseMessage connectResponseMessage = clientSession.getMessageFromMessagesList(1, SmscConnectResponseMessage.class);

        SmscConnectResponseModel smscConnectResponseModel = connectResponseMessage.getSmscConnectResponseModel();
        SmscSessionId smscSessionId = smscConnectResponseModel.getSessionId();
        assertNotNull(smscSessionId);


        SmscSendModel smscSendModel = new SmscSendModel();


        SmscSendSmsMessage smscSendSmsMessage =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .withSenderKey(getClientKey())
                        .withRecipientKeyServer()
                        .withMessageIdRandom()
                        .build(SmscSendSmsMessage.class);

        smscSendSmsMessage.setSmscSessionId(smscSessionId);

        smscSendSmsMessage.setSmscSendModel(smscSendModel);


        sendMessage(clientSession, smscSendSmsMessage);
        expectedMessageCount++;

        checkSessionsCount(1);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);


        // Get send smsc response
        assertEquals(3, clientSession.messages.size());
        SmscSendSmsResponseMessage smscSendSmsResponseMessage = clientSession.getMessageFromMessagesList(2, SmscSendSmsResponseMessage.class);
        SmscSendSmsResponseModel smscSendSmsResponseModel = smscSendSmsResponseMessage.getSmscSendSmsResponseModel();
        assertNotNull(smscSendSmsResponseModel);


        // Emulate SMSC
        // Send receive message
        MockSmscSession mockSmscSession = (MockSmscSession) testRegister.getSmscSession(iTestIdProvider.getTestId(), smscSessionId);

        SmscDeliverSmModel smscDeliverSmModel = new SmscDeliverSmModel();
        smscDeliverSmModel.setPriority((byte) 1);


        mockSmscSession.mockOnDeliverySm(smscDeliverSmModel);
        expectedMessageCount++;

        checkSessionsCount(1);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);


        // Get send smsc response
        assertEquals(4, clientSession.messages.size());

        SmscDeliverSmMessage smscDeliverSmMessage = clientSession.getMessageFromMessagesList(3, SmscDeliverSmMessage.class);
        SmscDeliverSmModel smscDeliverSmModel1 = smscDeliverSmMessage.getSmscDeliverSmModel();
        assertNotNull(smscDeliverSmModel1);

        assertEquals((byte) 1, smscDeliverSmModel1.getPriority());


        // Send DR message

        SmscDeliveryReportModel smscDeliveryReportModel = new SmscDeliveryReportModel();
        smscDeliveryReportModel.setServiceType("service123");
        mockSmscSession.mockOnDeliveryReport(smscDeliveryReportModel);
        expectedMessageCount++;

        checkSessionsCount(1);
        checkOneClient(iSessionManager, gi);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);

        assertEquals(5, clientSession.messages.size());

        SmscDeliveryReportMessage smscDeliveryReportMessage = clientSession.getMessageFromMessagesList(4, SmscDeliveryReportMessage.class);
        SmscDeliveryReportModel smscDeliveryReportModel1 = smscDeliveryReportMessage.getSmscDeliveryReportModel();
        assertNotNull(smscDeliveryReportModel1);

        assertEquals("service123", smscDeliveryReportModel1.getServiceType());


    }


    @Test
    public void testTwoConnections() throws Exception
    {
        int expectedMessageCount = 0;

        MockWebSocketSession clientSession = getNewSession();
        handler.afterConnectionEstablished(clientSession);
        checkSessionsCount(1);

        RegisterClientLibMessage msg = getRegisterClientLibMessage();

        ClientInfo clientInfo = msg.getClientInfo();

        sendMessage(clientSession, msg);
        expectedMessageCount++;
        checkSessionsCount(1);
        checkOneClient(iSessionManager, clientInfo);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);
        assertEquals(1, clientSession.messages.size());


        SmscConnectMessage smscConnectMessage = getSmscConnectMessage();


        SmscConnectModel smscConnectModel = new SmscConnectModel();
        smscConnectMessage.setSmscSendSmsModel(smscConnectModel);


        sendMessage(clientSession, smscConnectMessage);
        expectedMessageCount++;

        checkSessionsCount(1);
        checkOneClient(iSessionManager, clientInfo);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);

        assertEquals(2, clientSession.messages.size());

        SmscConnectResponseMessage connectResponseMessage = clientSession.getMessageFromMessagesList(1, SmscConnectResponseMessage.class);

        SmscConnectResponseModel smscConnectResponseModel = connectResponseMessage.getSmscConnectResponseModel();
        SmscSessionId smscSessionId = smscConnectResponseModel.getSessionId();
        assertNotNull(smscSessionId);


        SmscConnectMessage smscConnectMessage2 = getSmscConnectMessage();


        SmscConnectModel smscConnectModel2 = new SmscConnectModel();
        smscConnectMessage2.setSmscSendSmsModel(smscConnectModel2);


        sendMessage(clientSession, smscConnectMessage2);
        expectedMessageCount++;

        checkSessionsCount(1);
        checkOneClient(iSessionManager, clientInfo);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);

        assertEquals(3, clientSession.messages.size());

        SmscConnectResponseMessage connectResponseMessage2 = clientSession.getMessageFromMessagesList(1, SmscConnectResponseMessage.class);

        SmscConnectResponseModel smscConnectResponseModel2 = connectResponseMessage2.getSmscConnectResponseModel();
        SmscSessionId smscSessionId2 = smscConnectResponseModel2.getSessionId();
        assertNotNull(smscSessionId2);


        ServerTestInstance testInstance = testRegister.getTestInstance(iTestIdProvider.getTestId());
        int sessionCount = testInstance.getSmscSessionCount();
        assertEquals(2, sessionCount);


        SmscSendModel smscSendModel = new SmscSendModel();


        SmscSendSmsMessage smscSendSmsMessage =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .withRecipientKeyServer()
                        .withSenderKey(getClientKey())
                        .withMessageIdRandom()
                        .build(SmscSendSmsMessage.class);


        smscSendSmsMessage.setSmscSessionId(smscSessionId);

        smscSendSmsMessage.setSmscSendModel(smscSendModel);

        sendMessage(clientSession, smscSendSmsMessage);
        expectedMessageCount++;

        checkSessionsCount(1);
        checkOneClient(iSessionManager, clientInfo);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);


        // Get send smsc response
        assertEquals(4, clientSession.messages.size());
        SmscSendSmsResponseMessage smscSendSmsResponseMessage = clientSession.getMessageFromMessagesList(3, SmscSendSmsResponseMessage.class);
        SmscSendSmsResponseModel smscSendSmsResponseModel = smscSendSmsResponseMessage.getSmscSendSmsResponseModel();
        assertNotNull(smscSendSmsResponseModel);


        SmscSendModel smscSendModel2 = new SmscSendModel();

        SmscSendSmsMessage smscSendSmsMessage2 =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .withRecipientKeyServer()
                        .withSenderKey(getClientKey())
                        .withMessageIdRandom()
                        .build(SmscSendSmsMessage.class);

        smscSendSmsMessage2.setSmscSessionId(smscSessionId);

        smscSendSmsMessage2.setSmscSendModel(smscSendModel2);


        sendMessage(clientSession, smscSendSmsMessage2);
        expectedMessageCount++;

        checkSessionsCount(1);
        checkOneClient(iSessionManager, clientInfo);
        checkTestRegisterMessagesCount(getTestId(), expectedMessageCount);


        // Get send smsc response
        assertEquals(5, clientSession.messages.size());
        SmscSendSmsResponseMessage smscSendSmsResponseMessage2 = clientSession.getMessageFromMessagesList(4, SmscSendSmsResponseMessage.class);
        SmscSendSmsResponseModel smscSendSmsResponseModel2 = smscSendSmsResponseMessage2.getSmscSendSmsResponseModel();
        assertNotNull(smscSendSmsResponseModel2);


    }


}
