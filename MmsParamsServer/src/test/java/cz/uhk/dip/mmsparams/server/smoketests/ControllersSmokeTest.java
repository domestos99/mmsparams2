package cz.uhk.dip.mmsparams.server.smoketests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import cz.uhk.dip.mmsparams.server.web.controllers.AdminController;
import cz.uhk.dip.mmsparams.server.web.controllers.ClientController;
import cz.uhk.dip.mmsparams.server.web.controllers.IndexController;
import cz.uhk.dip.mmsparams.server.web.controllers.JwtAuthenticationController;
import cz.uhk.dip.mmsparams.server.web.controllers.MessageRegisterController;
import cz.uhk.dip.mmsparams.server.web.controllers.MmscServlet;
import cz.uhk.dip.mmsparams.server.web.controllers.PhoneController;
import cz.uhk.dip.mmsparams.server.web.controllers.PingController;
import cz.uhk.dip.mmsparams.server.web.controllers.SessionListController;
import cz.uhk.dip.mmsparams.server.web.controllers.SystemVersionController;
import cz.uhk.dip.mmsparams.server.web.controllers.TestInstanceController;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class ControllersSmokeTest
{
    @Autowired
    private AdminController adminController;
    @Autowired
    private ClientController clientController;
    @Autowired
    private IndexController indexController;
    @Autowired
    private JwtAuthenticationController jwtAuthenticationController;
    @Autowired
    private MessageRegisterController messageRegisterController;
    @Autowired
    private MmscServlet mmscServlet;
    @Autowired
    private PhoneController phoneController;
    @Autowired
    private PingController pingController;
    @Autowired
    private SessionListController sessionListController;
    @Autowired
    private SystemVersionController systemVersionController;
    @Autowired
    private TestInstanceController testInstanceController;

    @Test
    public void initAdminControllerTest()
    {
        assertNotNull(this.adminController);
    }

    @Test
    public void initClientControllerTest()
    {
        assertNotNull(this.clientController);
    }

    @Test
    public void initIndexControllerTest()
    {
        assertNotNull(this.indexController);
    }

    @Test
    public void initJwtAuthenticationControllerTest()
    {
        assertNotNull(this.jwtAuthenticationController);
    }

    @Test
    public void initMessageRegisterControllerTest()
    {
        assertNotNull(this.messageRegisterController);
    }

    @Test
    public void initMmscServletTest()
    {
        assertNotNull(this.mmscServlet);
    }

    @Test
    public void initPhoneControllerTest()
    {
        assertNotNull(this.phoneController);
    }

    @Test
    public void initPingControllerTest()
    {
        assertNotNull(this.pingController);
    }

    @Test
    public void initSessionListControllerTest()
    {
        assertNotNull(this.sessionListController);
    }

    @Test
    public void initSystemVersionControllerTest()
    {
        assertNotNull(this.systemVersionController);
    }

    @Test
    public void initTestInstanceControllerTest()
    {
        assertNotNull(this.testInstanceController);
    }


}
