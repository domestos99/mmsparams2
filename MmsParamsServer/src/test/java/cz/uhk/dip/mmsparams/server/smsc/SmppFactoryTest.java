//package cz.uhk.dip.mmsparams.server.SMSC;
//
//import com.cloudhopper.smpp.SmppSessionConfiguration;
//
//import org.jsmsc.InvalidResponseException;
//import org.jsmsc.PDUException;
//import org.jsmsc.bean.AlertNotification;
//import org.jsmsc.bean.BindType;
//import org.jsmsc.bean.DataSm;
//import org.jsmsc.bean.DeliverSm;
//import org.jsmsc.bean.DeliveryReceipt;
//import org.jsmsc.bean.MessageType;
//import org.jsmsc.bean.NumberingPlanIndicator;
//import org.jsmsc.bean.TypeOfNumber;
//import org.jsmsc.extra.NegativeResponseException;
//import org.jsmsc.extra.ProcessRequestException;
//import org.jsmsc.extra.ResponseTimeoutException;
//import org.jsmsc.session.BindParameter;
//import org.jsmsc.session.DataSmResult;
//import org.jsmsc.session.MessageReceiverListener;
//import org.jsmsc.session.SMSCSession;
//import org.jsmsc.session.Session;
//import org.jsmsc.util.InvalidDeliveryReceiptException;
//import org.junit.Test;
//
//import java.io.IOException;
//
//import cz.uhk.dip.mmsparams.SMSC.SmscSubmit;
//import cz.uhk.dip.mmsparams.server.mocks.UTLogger;
//
//public class SmscFactoryTest
//{
//
//
//
//    @Test
//    public void test1() throws InterruptedException, IOException
//    {
//        String host = "127.0.0.1";
//        int port = 2775;
//        SMSCSession session = new SMSCSession();
//
//        UTLogger.info("Connect and bind to {} port {}", host, port);
//
//        SmscSessionConfiguration defaultSmscSessionHandler = SmscSessionConfigurationFactory.create();
//        String systemId = SMSCUtils.connect(session, defaultSmscSessionHandler);
//
//        UTLogger.info("Connected with SMSC with system id {}", systemId);
//
//        Thread.sleep(10000);
//
//
//        session.unbindAndClose();
//    }
//
//    @Test
//    public void test2() throws Exception
//    {
//        SMSCSession session = new SMSCSession();
//        try
//        {
//            UTLogger.info("Connecting");
//
//            SmscSessionConfiguration defaultSmscSessionHandler = SmscSessionConfigurationFactory.create();
//            String systemId = SMSCUtils.connect(session, defaultSmscSessionHandler);
//
//
//            UTLogger.info("Connected with SMSC with system id {}", systemId);
//
//            try
//            {
//                SmscSubmit smscSubmit = SmscSubmitFactory.create();
//                String messageId = SMSCUtils.submitShortMessage(session, smscSubmit);
//
//
//                UTLogger.info("Message submitted, message_id is {}", messageId);
//            }
//            catch (PDUException e)
//            {
//                // Invalid PDU parameter
//                UTLogger.error("Invalid PDU parameter", e);
//                throw e;
//            }
//            catch (ResponseTimeoutException e)
//            {
//                // Response timeout
//                UTLogger.error("Response timeout", e);
//                throw e;
//            }
//            catch (InvalidResponseException e)
//            {
//                // Invalid response
//                UTLogger.error("Receive invalid response", e);
//                throw e;
//            }
//            catch (NegativeResponseException e)
//            {
//                // Receiving negative response (non-zero command_status)
//                UTLogger.error("Receive negative response, e");
//
//                throw e;
//            }
//            catch (IOException e)
//            {
//                UTLogger.error("IO error occured", e);
//                throw e;
//            }
//
//            session.unbindAndClose();
//
//        }
//        catch (IOException e)
//        {
//            UTLogger.error("Failed connect and bind to host", e);
//            throw e;
//        }
//
//
//    }
//
//    @Test
//    public void test3() throws Exception
//    {
//        SMSCSession session = new SMSCSession();
//        try
//        {
//            String host = "127.0.0.1";
//            int port = 2775;
//
//            UTLogger.info("Connecting");
//            SmscSessionConfiguration defaultSmscSessionHandler = SmscSessionConfigurationFactory.create();
//            String systemId = SMSCUtils.connect(session, defaultSmscSessionHandler);
//
//
//            UTLogger.info("Connected with SMSC with system id {}", systemId);
//
//            try
//            {
//
//
//                SmscSubmit smscSubmit = SmscSubmitFactory.create();
//                String messageId = SMSCUtils.submitShortMessage(session, smscSubmit);
//
//                UTLogger.info("Message submitted, message_id is {}", messageId);
//            }
//            catch (PDUException e)
//            {
//                // Invalid PDU parameter
//                UTLogger.error("Invalid PDU parameter", e);
//                throw e;
//            }
//            catch (ResponseTimeoutException e)
//            {
//                // Response timeout
//                UTLogger.error("Response timeout", e);
//                throw e;
//            }
//            catch (InvalidResponseException e)
//            {
//                // Invalid response
//                UTLogger.error("Receive invalid response", e);
//                throw e;
//            }
//            catch (NegativeResponseException e)
//            {
//                // Receiving negative response (non-zero command_status)
//                UTLogger.error("Receive negative response, e");
//
//                throw e;
//            }
//            catch (IOException e)
//            {
//                UTLogger.error("IO error occured", e);
//                throw e;
//            }
//
//
//            // Set listener to receive deliver_sm
//            session.setMessageReceiverListener(new MessageReceiverListener()
//            {
//                public void onAcceptDeliverSm(DeliverSm deliverSm) throws ProcessRequestException
//                {
//                    // deliverSm.getCommandId() // USE
//                    // deliverSm.isSmeDeliveryAckRequested() // USE
//
//                    if (MessageType.SMSC_DEL_RECEIPT.containedIn(deliverSm.getEsmClass()))
//                    {
//                        // delivery receipt
//                        try
//                        {
//                            DeliveryReceipt delReceipt = deliverSm.getShortMessageAsDeliveryReceipt();
//                            long id = Long.parseLong(delReceipt.getId()) & 0xffffffff;
//                            String messageId = Long.toString(id, 16).toUpperCase();
//                            UTLogger.info("received '{}' : {}", messageId, delReceipt);
//                        }
//                        catch (InvalidDeliveryReceiptException e)
//                        {
//                            UTLogger.error("receive failed, e");
//                        }
//                    }
//                    else
//                    {
//                        // regular short message
//                        UTLogger.info("Receiving message : {}", new String(deliverSm.getShortMessage()));
//                    }
//                }
//
//                public void onAcceptAlertNotification(AlertNotification alertNotification)
//                {
//                    UTLogger.info("onAcceptAlertNotification");
//                }
//
//                @Override
//                public DataSmResult onAcceptDataSm(DataSm dataSm, Session source) throws ProcessRequestException
//                {
//                    UTLogger.info("onAcceptDataSm");
//                    return null;
//                }
//
//            });
//
//            // wait 3 second
//            try
//            {
//                Thread.sleep(500000);
//            }
//            catch (InterruptedException e)
//            {
//                UTLogger.info("Interrupted exception", e);
//                throw e;
//            }
//
//
//            session.unbindAndClose();
//
//        }
//        catch (IOException e)
//        {
//            UTLogger.error("Failed connect and bind to host", e);
//            throw e;
//        }
//
//
//    }
//
//
//
//    @Test
//    public void deliveryTest() throws Exception
//    {
//        SMSCSession session = new SMSCSession();
//        try
//        {
//            UTLogger.info("Connecting");
//
//            SmscSessionConfiguration defaultSmscSessionHandler = SmscSessionConfigurationFactory.create();
//            String systemId = SMSCUtils.connect(session, defaultSmscSessionHandler);
//
//            UTLogger.info("Connected with SMSC with system id {}", systemId);
//
//            // Set listener to receive deliver_sm
//            session.setMessageReceiverListener(new MessageReceiverListener()
//            {
//                public void onAcceptDeliverSm(DeliverSm deliverSm) throws ProcessRequestException
//                {
//                    if (MessageType.SMSC_DEL_RECEIPT.containedIn(deliverSm.getEsmClass()))
//                    {
//                        // delivery receipt
//                        try
//                        {
//                            DeliveryReceipt delReceipt = deliverSm.getShortMessageAsDeliveryReceipt();
//                            long id = Long.parseLong(delReceipt.getId()) & 0xffffffff;
//                            String messageId = Long.toString(id, 16).toUpperCase();
//                            UTLogger.info("received '{}' : {}", messageId, delReceipt);
//                        }
//                        catch (InvalidDeliveryReceiptException e)
//                        {
//                            UTLogger.error("receive failed, e");
//                        }
//                    }
//                    else
//                    {
//                        // regular short message
//                        UTLogger.info("Receiving message : {}", new String(deliverSm.getShortMessage()));
//                    }
//                }
//
//                public void onAcceptAlertNotification(AlertNotification alertNotification)
//                {
//                    UTLogger.info("onAcceptAlertNotification");
//                }
//
//                @Override
//                public DataSmResult onAcceptDataSm(DataSm dataSm, Session source) throws ProcessRequestException
//                {
//                    UTLogger.info("onAcceptDataSm");
//                    return null;
//                }
//
//            });
//
//            // wait 3 second
//            try
//            {
//                Thread.sleep(500000);
//            }
//            catch (InterruptedException e)
//            {
//                UTLogger.info("Interrupted exception", e);
//                throw e;
//            }
//
//
//            session.unbindAndClose();
//
//        }
//        catch (IOException e)
//        {
//            UTLogger.error("Failed connect and bind to host", e);
//            throw e;
//        }
//
//
//    }
//}