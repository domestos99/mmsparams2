package cz.uhk.dip.mmsparams.server.smsc;

import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppSessionConfiguration;

public class SmppSessionConfigurationFactory
{
    public static SmppSessionConfiguration create()
    {
        SmppSessionConfiguration config0 = new SmppSessionConfiguration();
        config0.setWindowSize(1);
        config0.setName("Tester.Session.0");
        config0.setType(SmppBindType.TRANSCEIVER);
        config0.setHost("127.0.0.1");
        config0.setPort(2775);
        config0.setConnectTimeout(10000);
        config0.setSystemId("smscclient1");
        config0.setPassword("password");
        config0.getLoggingOptions().setLogBytes(true);
        // to enable monitoring (request expiration)
        config0.setRequestExpiryTimeout(30000);
        config0.setWindowMonitorInterval(15000);
        config0.setCountersEnabled(true);


        config0.setSystemType("cp");


        return config0;
    }
}
