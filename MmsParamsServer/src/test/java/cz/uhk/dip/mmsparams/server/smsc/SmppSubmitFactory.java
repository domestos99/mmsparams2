//package cz.uhk.dip.mmsparams.server.SMSC;
//
//import com.cloudhopper.smpp.type.Address;
//
//import org.jsmsc.bean.Alphabet;
//import org.jsmsc.bean.ESMClass;
//import org.jsmsc.bean.GeneralDataCoding;
//import org.jsmsc.bean.IntermediateNotification;
//import org.jsmsc.bean.MessageClass;
//import org.jsmsc.bean.NumberingPlanIndicator;
//import org.jsmsc.bean.RegisteredDelivery;
//import org.jsmsc.bean.SMEOriginatedAcknowledgement;
//import org.jsmsc.bean.SMSCDeliveryReceipt;
//import org.jsmsc.bean.TypeOfNumber;
//
//import java.util.Date;
//
//import cz.uhk.dip.mmsparams.SMSC.SmscSubmit;
//
//
//public class SmscSubmitFactory
//{
//    public static SmscSubmit create()
//    {
//        SmscSubmit submit = new SmscSubmit();
//
//        String msgTest = "Hello world";
//
//        submit.setSourceAddress(new Address(TypeOfNumber.INTERNATIONAL, NumberingPlanIndicator.ISDN, "1616"));
//        submit.setDestAddress(new Address(TypeOfNumber.INTERNATIONAL, NumberingPlanIndicator.ISDN, "628176504657"));
//        submit.setShortMessage(msgTest);
//
//        submit.setESMClass(new ESMClass());
//
//        // set RegisteredDelivery
//        final RegisteredDelivery registeredDelivery = new RegisteredDelivery();
//        registeredDelivery.setSMSCDeliveryReceipt(SMSCDeliveryReceipt.SUCCESS_FAILURE);
//        registeredDelivery.setSMEOriginatedAcknowledgement(SMEOriginatedAcknowledgement.DELIVERY);
//        registeredDelivery.setIntermediateNotification(IntermediateNotification.REQUESTED);
//
//        submit.setProtocolId((byte) 0);
//        submit.setPriority((byte) 1);
//
//        submit.setScheduleDeliveryTime(new Date());
//
//        submit.setValidityPeriod(null);
//
//        submit.setRegisteredDelivery(registeredDelivery);
//
//        submit.setReplaceIfPresent((byte) 0);
//
//        submit.setDataCoding(new GeneralDataCoding(Alphabet.ALPHA_DEFAULT, MessageClass.CLASS1, false));
//
//        submit.setDefaultMsgId((byte) 0);
//
//
//        return submit;
//    }
//}
