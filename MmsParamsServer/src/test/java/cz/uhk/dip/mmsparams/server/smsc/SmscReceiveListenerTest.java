package cz.uhk.dip.mmsparams.server.smsc;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketConstants;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliverSmMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscDeliveryReportMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.smsc.SmscMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.server.websocket.controllers.interfaces.ISmscWSController;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SmscReceiveListenerTest
{

    @Mock
    private ISmscWSController iClientLibBroadcast;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void setup()
    {
        iClientLibBroadcast = mock(ISmscWSController.class);
    }

    private final String testId = "test-id";
    private final String sessionId = "session-id";

    @Test
    public void onDeliverySmTest() throws IOException
    {
        SmscReceiveListener listener = new SmscReceiveListener(iClientLibBroadcast, testId, new SmscSessionId(sessionId));

        SmscDeliverSmModel model = new SmscDeliverSmModel();
        listener.onDeliverySm(model);

        veryfyCall(SmscDeliverSmMessage.class);
    }


    @Test
    public void onDeliveryReportTest() throws IOException
    {
        SmscReceiveListener listener = new SmscReceiveListener(iClientLibBroadcast, testId, new SmscSessionId(sessionId));

        SmscDeliveryReportModel model = new SmscDeliveryReportModel();
        listener.onDeliveryReport(model);

        listener.onDeliveryReport(model);

        veryfyCall(SmscDeliveryReportMessage.class);
    }

    private void veryfyCall(Class clz) throws IOException
    {
        verify(iClientLibBroadcast, atLeastOnce()).processSmscBroadcastToClientFromServer(argThat(new ArgumentMatcher<SmscMessageBase>()
        {
            @Override
            public boolean matches(SmscMessageBase argument)
            {
                if (argument == null)
                    return false;

                if (!clz.equals(argument.getClass()))
                    return false;

                if (!testId.equals(argument.getTestID()))
                    return false;

                if (!MessageUtils.isMessageValid(argument))
                    return false;

                if (!WebSocketConstants.Server_Recipient_Key.equals(argument.getSenderKey()))
                    return false;

                if (!WebSocketConstants.Server_To_Client_Broadcast.equals(argument.getRecipientKey()))
                    return false;

                if (argument.getSmscSessionId() == null || !sessionId.equals(argument.getSmscSessionId().getSmscSessionId()))
                    return false;

                return true;
            }
        }), argThat(new ArgumentMatcher<String>()
        {
            @Override
            public boolean matches(String argument)
            {
                return testId.equals(argument);
            }
        }));
    }
}