package cz.uhk.dip.mmsparams.server.tests;

import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ServerTestInstanceTest extends UnitTestBase
{

    @Test
    public void addGroovyClientSession()
    {
        MockWebSocketSession session = getNewSession();
        ClientInfo clientInfo = getClientInfoForTest();
        ServerTestInstance test = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), session, getTestClientInfoForTest(), clientInfo, null);

        assertEquals(session.getId(), test.getClientSessionId());
    }

    @Test
    public void insertIncomingMsg()
    {
    }

    @Test
    public void getTestID()
    {
        ServerTestInstance test = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), getNewSession(), getTestClientInfoForTest(), getClientInfoForTest(), null);
        assertEquals(GenericConstants.TEST_ID, test.getTestID());
    }

    @Test
    public void closeTest()
    {
        ServerTestInstance serverTestInstance = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), getNewSession(), getTestClientInfoForTest(), getClientInfoForTest(), null);
        assertFalse(serverTestInstance.isClosed());
        serverTestInstance.removeClientSessionAndCloseTest(true);
        assertTrue(serverTestInstance.isClosed());
    }

    @Test
    public void removeGroovySession()
    {
        MockWebSocketSession session = getNewSession();
        ClientInfo clientInfo = getClientInfoForTest();

        ServerTestInstance test = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), session, getTestClientInfoForTest(), clientInfo, null);


        assertNotNull(test.getClientInfo());

        test.removeClientSessionAndCloseTest(true);

        assertNotNull(test.getClientInfo());
    }

    @Test
    public void getGroovySessionId()
    {
        MockWebSocketSession session = new MockWebSocketSession();
        ClientInfo clientInfo = getClientInfoForTest();

        ServerTestInstance test = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), session, getTestClientInfoForTest(), clientInfo, null);


        assertEquals(session.getId(), test.getClientSessionId());

        test.removeClientSessionAndCloseTest(true);


        assertNotEquals("-1", test.getClientSessionId());
    }

    @Test
    public void containsLockedPhone1Test()
    {
        ServerTestInstance st = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), getNewSession(), getTestClientInfoForTest(), getClientInfoForTest(), null);
        assertFalse(st.containsLockedPhone("phone"));
    }

    @Test
    public void containsLockedPhone2Test()
    {
        MockWebSocketSession session = getNewSession();
        PhoneInfoModel di = new PhoneInfoModel();
        di.setPhoneKey(GenericConstants.PHONE_KEY);

        ServerTestInstance st = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), session, getTestClientInfoForTest(), getClientInfoForTest(), null);

        st.lockPhoneForTest(session, di);

        assertFalse(st.containsLockedPhone("phone"));
        assertTrue(st.containsLockedPhone(GenericConstants.PHONE_KEY));
    }


    @Test
    public void lockPhoneForTest1Test()
    {
        MockWebSocketSession session = getNewSession();
        ServerTestInstance st = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), session, getTestClientInfoForTest(), getClientInfoForTest(), null);

        PhoneInfoModel di = new PhoneInfoModel();
        di.setPhoneKey(GenericConstants.PHONE_KEY);
        assertEquals(0, st.getPhoneInstanceList().size());

        st.lockPhoneForTest(session, di);
        assertEquals(1, st.getPhoneInstanceList().size());

        st.lockPhoneForTest(session, di);
        assertEquals(1, st.getPhoneInstanceList().size());
    }


    @Test
    public void unLockPhoneForTest1Test()
    {
        MockWebSocketSession session = getNewSession();
        ServerTestInstance st = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), session, getTestClientInfoForTest(), getClientInfoForTest(), null);

        PhoneInfoModel di = new PhoneInfoModel();
        di.setPhoneKey(GenericConstants.PHONE_KEY);

        assertEquals(0, st.getPhoneInstanceList().size());
        st.unLockPhoneForTest(di);
        assertEquals(0, st.getPhoneInstanceList().size());

        st.lockPhoneForTest(session, di);
        assertEquals(1, st.getPhoneInstanceList().size());

        st.unLockPhoneForTest(di);
        assertEquals(0, st.getPhoneInstanceList().size());
    }


    @Test
    public void removeSessionIfExists1Test()
    {
        MockWebSocketSession session = getNewSession();

        ClientInfo clientInfo = new ClientInfo();
        clientInfo.setClientKey("groovy-key");

        ServerTestInstance st = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), session, getTestClientInfoForTest(), clientInfo, null);

        assertNotEquals("-1", st.getClientSessionId());

        assertEquals(session.getId(), st.getClientSessionId());

        st.removeSessionIfExists(session.getId());

        assertTrue(st.isClosed());
        assertNotNull(st.getClientLibInstance());
        assertNotNull(st.getClientInfo());
        assertNotEquals("-1", st.getClientSessionId());
    }


    @Test
    public void removeSessionIfExists2Test()
    {
        MockWebSocketSession session = getNewSession();

        ClientInfo clientInfo = new ClientInfo();
        clientInfo.setClientKey("groovy-key");

        ServerTestInstance st = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), session, getTestClientInfoForTest(), clientInfo, null);

        assertEquals(session.getId(), st.getClientSessionId());

        MockWebSocketSession session2 = getNewSession();

        PhoneInfoModel di = new PhoneInfoModel();
        di.setPhoneKey("phone-key");

        assertEquals(0, st.getPhoneInstanceList().size());
        st.lockPhoneForTest(session2, di);

        assertEquals(1, st.getPhoneInstanceList().size());

        st.removeSessionIfExists(session2.getId());
        assertEquals(0, st.getPhoneInstanceList().size());

        assertFalse(st.isClosed());
        assertNotNull(st.getClientLibInstance());
        assertNotNull(st.getClientInfo());
        assertEquals(session.getId(), st.getClientSessionId());
    }


    @Test
    public void removeSessionIfExists3Test()
    {
        ServerTestInstance st = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), getNewSession(), getTestClientInfoForTest(), getClientInfoForTest(), null);


        assertNotEquals("-1", st.getClientSessionId());

        assertEquals(0, st.getPhoneInstanceList().size());

        st.removeSessionIfExists("xxx");

        assertEquals(0, st.getPhoneInstanceList().size());
        assertFalse(st.isClosed());
        assertNotNull(st.getClientLibInstance());
        assertNotNull(st.getClientInfo());
        assertNotEquals("-1", st.getClientSessionId());
    }


    @Test
    public void removeSessionIfExists4Test()
    {
        MockWebSocketSession session = getNewSession();

        ClientInfo clientInfo = getClientInfoForTest();

        ServerTestInstance st = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), session, getTestClientInfoForTest(), clientInfo, null);

        assertEquals(session.getId(), st.getClientSessionId());


        MockWebSocketSession session2 = getNewSession();

        PhoneInfoModel di = getPhoneForUT();

        assertEquals(0, st.getPhoneInstanceList().size());
        st.lockPhoneForTest(session2, di);

        assertEquals(1, st.getPhoneInstanceList().size());

        st.removeSessionIfExists("xxx");
        assertEquals(1, st.getPhoneInstanceList().size());

        assertFalse(st.isClosed());
        assertNotNull(st.getClientLibInstance());
        assertNotNull(st.getClientInfo());
        assertEquals(session.getId(), st.getClientSessionId());
    }


}
