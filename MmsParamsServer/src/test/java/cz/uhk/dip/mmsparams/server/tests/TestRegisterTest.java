package cz.uhk.dip.mmsparams.server.tests;

import org.junit.Before;
import org.junit.Test;
import org.springframework.web.socket.WebSocketSession;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.http.TaskResult;
import cz.uhk.dip.mmsparams.api.messages.MessageRegisterItem;
import cz.uhk.dip.mmsparams.api.utils.CommonUtils;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.validation.TestValidationMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TestRegisterTest extends UnitTestBase
{
    private String testId = "test-id";
    private String clientKey = "client-key";

    @Before
    public void before()
    {
        super.testBeforeBase();
    }

    @Test
    public void canForward1Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        MockWebSocketSession session = getNewSession();
        TaskResult<Boolean> result = tr.canForward(session, null);
        assertTrue(result.hasError());
    }

    @Test
    public void canForward2Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        MockWebSocketSession session = getNewSession();
        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        TaskResult<Boolean> result = tr.canForward(session, msg);
        assertTrue(result.hasError());
    }

    @Test
    public void canForward3Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        MockWebSocketSession session = getNewSession();
        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);
        msg.setTestID(testId);

        // Test not found
        TaskResult<Boolean> result = tr.canForward(session, msg);
        assertTrue(result.hasError());
    }

    @Test
    public void canForward4Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        MockWebSocketSession session = getNewSession();
        GenericErrorResponseMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(GenericErrorResponseMessage.class);


        RegisterClientLibMessage reg =
                new MessageBuilder()
                        .withTestId(getUTTestProvider(1))
                        .build(RegisterClientLibMessage.class);
        reg.setClientInfo(getClientInfoForTest());

        tr.registerNewTest(session, reg);

//        tr.closeTest(msg);
        tr.removeSessionIfExists(session.getId());

        // Test is closed
        TaskResult<Boolean> result = tr.canForward(session, msg);
        assertTrue(result.hasError());
    }

    @Test
    public void canForward5Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        MockWebSocketSession session = getNewSession();
        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);
        msg.setTestID("xxx");

        RegisterClientLibMessage reg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(RegisterClientLibMessage.class);
        reg.setClientInfo(getClientInfoForTest());

        tr.registerNewTest(session, reg);

        // Test is closed
        TaskResult<Boolean> result = tr.canForward(session, msg);
        assertTrue(result.hasError());
    }

    @Test
    public void canForward6Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        MockWebSocketSession session = getNewSession();
        GenericErrorResponseMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .withRecipientKey(CommonUtils.getUUID())
                .build(GenericErrorResponseMessage.class);

        RegisterClientLibMessage reg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(RegisterClientLibMessage.class);
        reg.setClientInfo(getClientInfoForTest());

        tr.registerNewTest(session, reg);

        // Test is closed
        TaskResult<Boolean> result = tr.canForward(session, msg);
        assertTrue(result.hasError());
    }

    @Test
    public void addTestMessage_null_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        tr.addTestMessage(getNewSession(), null);

    }

    @Test
    public void addTestMessage_empty_testID_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        DevMessage devMessage = MessageFactory.create(DevMessage.class);
        tr.addTestMessage(getNewSession(), devMessage);
    }

    @Test
    public void addTestMessageTest()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());

        RegisterClientLibMessage registerClient = new MessageBuilder()
                .withTestId(() -> testId)
                .build(RegisterClientLibMessage.class);

        registerClient.setClientInfo(getClientInfoForTest());

        tr.registerNewTest(getNewSession(), registerClient);


        DevMessage devMessage = new MessageBuilder()
                .withTestId(() -> testId)
                .build(DevMessage.class);
        tr.addTestMessage(getNewSession(), devMessage);

        List<MessageRegisterItem> messages = tr.getIncomingMessages(testId);
        assertEquals(1, messages.size());
    }

    @Test
    public void getIncomingMessages_null_testID_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        List<MessageRegisterItem> messages = tr.getIncomingMessages(null);
        assertNotNull(messages);
        assertEquals(0, messages.size());
    }

    @Test
    public void registerNewTest_null_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        tr.registerNewTest(getNewSession(), null);
        tr.registerNewTest(null, null);
    }

    @Test
    public void registerNewTest_register_twice_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());

        RegisterClientLibMessage registerClient = new MessageBuilder()
                .withTestId(() -> testId)
                .build(RegisterClientLibMessage.class);
        registerClient.setClientInfo(getClientInfoForTest());

        tr.registerNewTest(getNewSession(), registerClient);
        tr.registerNewTest(getNewSession(), registerClient);

        ServerTestInstance testInstance = tr.getTestInstance(testId);
        assertNotNull(testInstance);
    }

    @Test
    public void getClientSessionTest()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());

        RegisterClientLibMessage registerClient = new MessageBuilder()
                .withTestId(() -> testId)
                .build(RegisterClientLibMessage.class);
        registerClient.setClientInfo(getClientInfoForTest());

        MockWebSocketSession clientSession = getNewSession();
        tr.registerNewTest(clientSession, registerClient);

        ClientLibInstance getSession = tr.getClientSession(testId);

        assertEquals(clientSession.getId(), getSession.getClientSession().getId());
    }

    @Test
    public void getClientSession_test_closed_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());

        RegisterClientLibMessage registerClient = new MessageBuilder()
                .withTestId(() -> testId)
                .build(RegisterClientLibMessage.class);
        registerClient.setClientInfo(getClientInfoForTest());

        MockWebSocketSession clientSession = getNewSession();
        tr.registerNewTest(clientSession, registerClient);

        ServerTestInstance test = tr.getTestInstance(testId);
        test.closeTest();

        ClientLibInstance getSession = tr.getClientSession(testId);

        assertNull(getSession);
    }

    @Test
    public void getClientSession_testID_null_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        ClientLibInstance getSession = tr.getClientSession(null);
        assertNull(getSession);
    }

    @Test
    public void getSessionByRecipientKeyTest()
    {
        String clientKey = "client-key";
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());

        RegisterClientLibMessage registerClient = new MessageBuilder()
                .withTestId(() -> testId)
                .build(RegisterClientLibMessage.class);

        ClientInfo ci = new ClientInfo();
        ci.setClientKey(clientKey);
        registerClient.setClientInfo(ci);

        MockWebSocketSession clientSession = getNewSession();
        tr.registerNewTest(clientSession, registerClient);

        WebSocketSession session = tr.getSessionByRecipientKey(testId, clientKey);
        assertNotNull(session);

    }


    @Test
    public void getSessionByRecipientKey_testID_null_Test()
    {
        String recipientKey = "recipient-key";
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        WebSocketSession session = tr.getSessionByRecipientKey(null, recipientKey);
        assertNull(session);
    }

    @Test
    public void getTestByPhoneSenderKeyTest()
    {
        String phoneKey = "sender-key";
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());

        RegisterClientLibMessage registerClient = new MessageBuilder()
                .withTestId(() -> testId)
                .build(RegisterClientLibMessage.class);

        ClientInfo ci = new ClientInfo();
        ci.setClientKey(clientKey);
        registerClient.setClientInfo(ci);

        MockWebSocketSession clientSession = getNewSession();
        tr.registerNewTest(clientSession, registerClient);

        RegisterPhoneMessage registerPhoneMessage = new MessageBuilder()
                .build(RegisterPhoneMessage.class);

        PhoneInfoModel model = new PhoneInfoModel();
        model.setPhoneKey(phoneKey);
        registerPhoneMessage.setPhoneInfoModel(model);

        LockPhoneMessage lockPhoneMessage = new MessageBuilder()
                .withTestId(() -> testId)
                .build(LockPhoneMessage.class);

        lockPhoneMessage.setPhoneInfoModel(model);

        tr.lockPhone(getNewSession(), lockPhoneMessage);

        ServerTestInstance test = tr.getTestByPhoneSenderKey(phoneKey);
        assertNotNull(test);
    }

    @Test
    public void getTestByPhoneSenderKey_null_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        ServerTestInstance test = tr.getTestByPhoneSenderKey(null);
        assertNull(test);
    }

    @Test
    public void getConnectedClientsTest()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());

        registerClient(tr);

        List<ClientInfo> clients = tr.getConnectedClients();
        assertNotNull(clients);
        assertEquals(1, clients.size());
    }

    private void registerClient(TestRegister tr)
    {
        RegisterClientLibMessage registerClient = new MessageBuilder()
                .withTestId(() -> testId)
                .build(RegisterClientLibMessage.class);

        ClientInfo ci = new ClientInfo();
        ci.setClientKey(clientKey);
        registerClient.setClientInfo(ci);

        MockWebSocketSession clientSession = getNewSession();
        tr.registerNewTest(clientSession, registerClient);
    }

    @Test
    public void getConnectedClients_empty_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        List<ClientInfo> clients = tr.getConnectedClients();
        assertNotNull(clients);
        assertEquals(0, clients.size());
    }

    @Test
    public void getAllTestsTest()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        registerClient(tr);
        List<ServerTestInstance> tests = tr.getAllTests();
        assertNotNull(tests);
        assertEquals(1, tests.size());
    }

    @Test
    public void getAllTests2Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        registerClient(tr);
        registerClient(tr);
        List<ServerTestInstance> tests = tr.getAllTests();
        assertNotNull(tests);
        assertEquals(1, tests.size());
    }

    @Test
    public void getAllTests_empty_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        List<ServerTestInstance> tests = tr.getAllTests();
        assertNotNull(tests);
        assertEquals(0, tests.size());
    }

    @Test
    public void getAllOpenTestsTest()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        registerClient(tr);
        List<ServerTestInstance> tests = tr.getAllOpenTests();
        assertNotNull(tests);
        assertEquals(1, tests.size());
    }

    @Test
    public void getAllOpenTests2Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        registerClient(tr);
        registerClient(tr);
        List<ServerTestInstance> tests = tr.getAllOpenTests();
        assertNotNull(tests);
        assertEquals(1, tests.size());
    }

    @Test
    public void getAllOpenTests_empty_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        List<ServerTestInstance> tests = tr.getAllOpenTests();
        assertNotNull(tests);
        assertEquals(0, tests.size());
    }

    @Test
    public void getAllOpenTests_closed_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        registerClient(tr);

        ServerTestInstance test = tr.getTestInstance(testId);
        test.closeTest();

        List<ServerTestInstance> tests = tr.getAllOpenTests();
        assertNotNull(tests);
        assertEquals(0, tests.size());
    }

    @Test
    public void processValidationResultTest()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        registerClient(tr);

        TestValidationMessage testValidationMessage = new MessageBuilder()
                .withTestId(() -> testId)
                .build(TestValidationMessage.class);

        ArrayList<Tuple<Boolean, ValidationResult>> vr = new ArrayList<>();
        vr.add(new Tuple<>(true, new ValidationResult()));

        testValidationMessage.setValidationResult(vr);

        tr.processValidationResult(testValidationMessage);

        ServerTestInstance test = tr.getTestInstance(testId);

        List<Tuple<Boolean, ValidationResult>> validations = test.getValidationResults();
        assertNotNull(validations);
        assertEquals(1, validations.size());
    }

    @Test
    public void processValidationResult_null_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        tr.processValidationResult(null);
    }

    @Test
    public void processValidationResult_null_testID_Test()
    {
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        TestValidationMessage testValidationMessage = new MessageBuilder()
                .build(TestValidationMessage.class);
        tr.processValidationResult(testValidationMessage);

    }


    @Test
    public void getMessageByTestIdMessageIdTest()
    {
        String messageId = "message-id";
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        registerClient(tr);

        DevMessage devMessage = new MessageBuilder()
                .withTestId(() -> testId)
                .withMessageId(messageId)
                .build(DevMessage.class);

        tr.addTestMessage(getNewSession(), devMessage);

        WebSocketMessageBase msg = tr.getMessageByTestIdMessageId(testId, messageId);
        assertNotNull(msg);
    }

    @Test
    public void getMessageByTestIdMessageId_not_found_Test()
    {
        String messageId = "message-id";
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        registerClient(tr);
        WebSocketMessageBase msg = tr.getMessageByTestIdMessageId(testId, messageId);
        assertNull(msg);
    }

    @Test
    public void getMessageByTestIdMessageId_null_testID_Test()
    {
        String messageId = "message-id";
        TestRegister tr = new TestRegister(getTestServerTestInstanceDBService());
        WebSocketMessageBase msg = tr.getMessageByTestIdMessageId(null, messageId);
        assertNull(msg);
    }

}
