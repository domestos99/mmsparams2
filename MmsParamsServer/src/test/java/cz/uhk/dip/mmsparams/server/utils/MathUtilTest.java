package cz.uhk.dip.mmsparams.server.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MathUtilTest
{
    @Test
    public void getPageButtonsCountTest()
    {
        int itemsCount = 10;
        int ps = 5;
        int totalPages = MathUtil.getTotalPages(itemsCount, ps);
        assertEquals(2, totalPages);

        itemsCount = 1;
        totalPages = MathUtil.getTotalPages(itemsCount, ps);
        assertEquals(1, totalPages);

        itemsCount = 9;
        totalPages = MathUtil.getTotalPages(itemsCount, ps);
        assertEquals(2, totalPages);
    }

}
