package cz.uhk.dip.mmsparams.server.utils;

import org.junit.Test;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;

import static org.junit.Assert.assertEquals;

public class MessageUtilsServerTest extends UnitTestBase
{

    @Test
    public void changeRecipientKey1Test() throws IOException
    {
        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);

        MockWebSocketSession session = getNewSession();
        ClientInfo gi = new ClientInfo();
        gi.setClientKey(GenericConstants.CLIENT_KEY);

        ServerTestInstance test = ServerTestInstance.createNewTest(GenericConstants.TEST_ID, getTestGroupForUT(), session, getTestClientInfoForTest(), gi, null);

        WebSocketMessageBase newMsg = MessageUtilsServer.changeRecipientKey(msg, test);

        assertEquals(GenericConstants.TEST_ID, newMsg.getTestID());
        assertEquals(GenericConstants.CLIENT_KEY, newMsg.getRecipientKey());
        assertEquals(msg.getClass(), newMsg.getClass());

    }
}
