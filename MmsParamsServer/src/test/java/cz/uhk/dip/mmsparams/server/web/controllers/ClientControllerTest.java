package cz.uhk.dip.mmsparams.server.web.controllers;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

import cz.uhk.dip.mmsparams.api.constants.AppVersion;
import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.cache.ClientInfoDBCache;
import cz.uhk.dip.mmsparams.server.database.dto.ClientInfoDTO;
import cz.uhk.dip.mmsparams.server.database.dto.ClientLibInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.services.IClientInfoDBService;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.websocket.IWSServicesContainer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ClientControllerTest extends UnitTestBase
{
    private ClientController clientController;
    private IWSServicesContainer iwsServicesContainer;
    private ClientInfoDBCache clientInfoDBCache;

    @Before
    public void setUp()
    {
        iwsServicesContainer = createSessionManager();
        clientInfoDBCache = new ClientInfoDBCache(new IClientInfoDBService()
        {
            @Override
            public Optional<ClientLibInstanceDTO> getByClientKey(String clientKey)
            {
                return Optional.empty();
            }
        });
        clientController = new ClientController(iwsServicesContainer.getClientLibWSController(), clientInfoDBCache);
    }

    @Test
    public void getClientsTest()
    {
        MockWebSocketSession session = getNewSession();
        iwsServicesContainer.getSessionWSController().addSession(session);

        RegisterClientLibMessage regMsg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .withMessageIdRandom()
                .withSenderKey(GenericConstants.SENDER_KEY)
                .withRecipientKeyServer()
                .build(RegisterClientLibMessage.class);
        regMsg.setClientInfo(getClientInfoForTest());
        regMsg.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);

        assertTrue(iwsServicesContainer.getClientLibWSController().registerLibClient(session, regMsg));


        ResponseEntity<List<ClientInfoDTO>> response = clientController.getClients();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        List<ClientInfoDTO> body = response.getBody();
        assertNotNull(body);
        assertEquals(1, body.size());
    }

    @Test
    public void getClients_empty_Test()
    {
        ResponseEntity<List<ClientInfoDTO>> response = clientController.getClients();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        List<ClientInfoDTO> body = response.getBody();
        assertNotNull(body);
        assertEquals(0, body.size());
    }

    @Test
    public void getClientByKeyTest()
    {
        MockWebSocketSession session = getNewSession();
        iwsServicesContainer.getSessionWSController().addSession(session);

        String clientKey = getClientInfoForTest().getClientKey();
        RegisterClientLibMessage regMsg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .withMessageIdRandom()
                .withSenderKey(clientKey)
                .withRecipientKeyServer()
                .build(RegisterClientLibMessage.class);
        regMsg.setClientInfo(getClientInfoForTest());
        regMsg.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);

        assertTrue(iwsServicesContainer.getClientLibWSController().registerLibClient(session, regMsg));

        ResponseEntity<ClientLibInstanceDTO> response = clientController.getClientByKey(clientKey);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        ClientLibInstanceDTO body = response.getBody();
        assertNotNull(body);
    }

    @Test
    public void getClientByKey_not_found_null_Test()
    {
        ResponseEntity<ClientLibInstanceDTO> response = clientController.getClientByKey(null);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    public void getClientByKey_not_found_value_Test()
    {
        ResponseEntity<ClientLibInstanceDTO> response = clientController.getClientByKey(GenericConstants.CLIENT_KEY);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }
}
