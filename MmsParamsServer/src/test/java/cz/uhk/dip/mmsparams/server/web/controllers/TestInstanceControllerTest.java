package cz.uhk.dip.mmsparams.server.web.controllers;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import cz.uhk.dip.mmsparams.api.utils.ListUtils;
import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.cache.ServerTestInstanceDBCache;
import cz.uhk.dip.mmsparams.server.database.dto.ServerTestInstanceDTO;
import cz.uhk.dip.mmsparams.server.database.services.IServerTestInstanceDBService;
import cz.uhk.dip.mmsparams.server.database.services.ServerTestInstanceDBService;
import cz.uhk.dip.mmsparams.server.tests.ServerTestInstance;
import cz.uhk.dip.mmsparams.server.tests.TestRegister;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestInstanceControllerTest extends UnitTestBase
{
    private TestRegister testRegister;
    private ServerTestInstanceDBCache serverTestInstanceDBCache;
    private IServerTestInstanceDBService iServerTestInstanceDBService;

    @Before
    public void setUp()
    {
        this.iServerTestInstanceDBService = new ServerTestInstanceDBService(null, null, null)
        {
            @Override
            public List<ServerTestInstanceDTO> getReduced()
            {
                List<ServerTestInstanceDTO> l = ListUtils.newList();
                l.add(new ServerTestInstanceDTO());
                l.add(new ServerTestInstanceDTO());
                l.add(new ServerTestInstanceDTO());
                return l;
            }
        };
        this.testRegister = new TestRegister(iServerTestInstanceDBService){
            @Override
            public List<ServerTestInstance> getAllOpenTests()
            {
                List<ServerTestInstance> l = ListUtils.newList();
                l.add(getTestServerTestInstance());
                return l;
            }
        };
        this.serverTestInstanceDBCache = new ServerTestInstanceDBCache(iServerTestInstanceDBService);
    }

    @Test
    public void getTestInstances_true_Test()
    {
        TestInstanceController ctr = new TestInstanceController(testRegister, serverTestInstanceDBCache);

        ResponseEntity<List<ServerTestInstanceDTO>> response = ctr.getTestInstances(true);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        List<ServerTestInstanceDTO> body = response.getBody();
        assertNotNull(body);
        assertEquals(1, body.size());
    }


    @Test
    public void getTestInstances_false_Test()
    {
        TestInstanceController ctr = new TestInstanceController(testRegister, serverTestInstanceDBCache);

        ResponseEntity<List<ServerTestInstanceDTO>> response = ctr.getTestInstances(false);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        List<ServerTestInstanceDTO> body = response.getBody();
        assertNotNull(body);
        assertEquals(3, body.size());
    }


}
