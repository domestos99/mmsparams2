package cz.uhk.dip.mmsparams.server.websocket;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import cz.uhk.dip.mmsparams.api.constants.AppVersion;
import cz.uhk.dip.mmsparams.api.constants.GenericConstants;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.mmsc.MmscAcquireRouteRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListRequestMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.PhoneListResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.UnLockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.SubscriptionInfoModel;
import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class SessionManagerTest extends UnitTestBase
{
    @Before
    public void before()
    {
        super.testBeforeBase();
    }

    private IWSServicesContainer registerPhone(IWSServicesContainer sm)
    {
        return registerPhone2(sm).getX();
    }

    private Tuple<IWSServicesContainer, MockWebSocketSession> registerPhone2(IWSServicesContainer sm)
    {
        if (sm == null)
            sm = createSessionManager();

        MockWebSocketSession session = getNewSession();
        RegisterPhoneMessage reg = MessageFactory.create(RegisterPhoneMessage.class);
        PhoneInfoModel di = new PhoneInfoModel();
        di.setPhoneKey(getUUID());

        di.setSubscriptionInfoModel(new SubscriptionInfoModel[]{new SubscriptionInfoModel()});

        reg.setPhoneInfoModel(di);
        assertTrue(sm.getPhoneWSController().registerPhone(session, reg));
        return new Tuple<IWSServicesContainer, MockWebSocketSession>(sm, session);
    }

    @Test
    public void registerPhoneTest()
    {
        registerPhone(null);
    }

    @Test
    public void registerPhone2Test() throws Exception
    {
        IWSServicesContainer sm = registerPhone(null);

        for (int i = 0; i < 10; i++)
        {
            sm = registerPhone(sm);
        }
    }

    @Test
    public void sendDeviceListTest() throws Exception
    {
        IWSServicesContainer sm = registerPhone(null);


        MockWebSocketSession session2 = getNewSession();
        PhoneListRequestMessage dlReq = MessageFactory.create(PhoneListRequestMessage.class);
        dlReq.setSenderKey(getUUID());
        dlReq.setMessageID(getUUID());

        sm.getPhoneWSController().sendDeviceList(session2, dlReq);
        session2.checkMessageCount(1);

        PhoneListResponseMessage resp = session2.getMessageFromMessagesList(0, PhoneListResponseMessage.class);
        assertNotEquals(null, resp);
    }


    @Test
    public void sendDeviceListTest2() throws Exception
    {
        IWSServicesContainer sm = registerPhone(null);
        for (int i = 0; i < 10; i++)
        {
            sm = registerPhone(sm);
        }

        MockWebSocketSession session2 = getNewSession();
        PhoneListRequestMessage dlReq = MessageFactory.create(PhoneListRequestMessage.class);
        dlReq.setSenderKey(getUUID());
        dlReq.setMessageID(getUUID());

        sm.getPhoneWSController().sendDeviceList(session2, dlReq);
        session2.checkMessageCount(1);

        PhoneListResponseMessage resp = session2.getMessageFromMessagesList(0, PhoneListResponseMessage.class);

        assertNotEquals(null, resp);
        ArrayList<PhoneInfoModel> devices = resp.getPhoneInfoModels();
        assertEquals(11, devices.size());
    }

    @Test
    public void lockPhoneTest()
    {
        IWSServicesContainer sm = createSessionManager();
        MockWebSocketSession clientSession = getNewSession();
        registerClientForTestBegin(sm, clientSession, 1);
        LockPhoneMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(LockPhoneMessage.class);

        PhoneInfoModel di = getPhoneForUT();
        msg.setPhoneInfoModel(di);

        MockWebSocketSession phoneSession = getNewSession();
        sm.getSessionWSController().addSession(phoneSession);
        registerPhoneAnonym(sm, phoneSession, di);

        assertTrue(sm.getPhoneWSController().lockPhone(clientSession, msg));

        clientSession.checkMessageCount(2);

        GenericBooleanResponseMessage resp = clientSession.getMessageFromMessagesList(1, GenericBooleanResponseMessage.class);
        assertNotNull(resp);
        assertTrue(resp.isValue());
    }


    @Test
    public void lockPhone_lock_unknown_phone_Test()
    {
        IWSServicesContainer sm = createSessionManager();
        MockWebSocketSession clientSession = getNewSession();
        registerClientForTestBegin(sm, clientSession, 1);
        LockPhoneMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(LockPhoneMessage.class);

        PhoneInfoModel di = getPhoneForUT();
        msg.setPhoneInfoModel(di);

        MockWebSocketSession phoneSession = getNewSession();
        sm.getSessionWSController().addSession(phoneSession);
        registerPhoneAnonym(sm, phoneSession, di);

        msg.setPhoneInfoModel(getPhoneForUT(2));
        assertFalse(sm.getPhoneWSController().lockPhone(clientSession, msg));

        clientSession.checkMessageCount(2);

        GenericErrorResponseMessage resp = clientSession.getMessageFromMessagesList(1, GenericErrorResponseMessage.class);
        assertNotNull(resp);
    }


    @Test
    public void lockPhone_lock_twice_Test()
    {
        IWSServicesContainer sm = createSessionManager();
        MockWebSocketSession clientSession = getNewSession();
        registerClientForTestBegin(sm, clientSession, 1);
        LockPhoneMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(LockPhoneMessage.class);

        PhoneInfoModel di = getPhoneForUT();
        msg.setPhoneInfoModel(di);

        MockWebSocketSession phoneSession = getNewSession();
        sm.getSessionWSController().addSession(phoneSession);
        registerPhoneAnonym(sm, phoneSession, di);

        assertTrue(sm.getPhoneWSController().lockPhone(clientSession, msg));

        phoneSession.checkMessageCount(1);
        phoneSession.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);

        GenericBooleanResponseMessage resp = clientSession.getMessageFromMessagesList(1, GenericBooleanResponseMessage.class);
        assertNotNull(resp);
        assertTrue(resp.isValue());

        assertTrue(sm.getPhoneWSController().lockPhone(clientSession, msg));
        clientSession.checkMessageCount(3);
        GenericBooleanResponseMessage resp2 = clientSession.getMessageFromMessagesList(2, GenericBooleanResponseMessage.class);
        assertNotNull(resp2);
    }

    @Test
    public void lockPhone_lock_twice_two_test_Test()
    {
        IWSServicesContainer sm = createSessionManager();
        MockWebSocketSession clientSession = getNewSession();
        registerClientForTestBegin(sm, clientSession, 1);
        LockPhoneMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(LockPhoneMessage.class);

        PhoneInfoModel di = getPhoneForUT();
        msg.setPhoneInfoModel(di);

        MockWebSocketSession phoneSession = getNewSession();
        sm.getSessionWSController().addSession(phoneSession);
        registerPhoneAnonym(sm, phoneSession, di);

        assertTrue(sm.getPhoneWSController().lockPhone(clientSession, msg));

        clientSession.checkMessageCount(2);

        GenericBooleanResponseMessage resp = clientSession.getMessageFromMessagesList(1, GenericBooleanResponseMessage.class);
        assertNotNull(resp);
        assertTrue(resp.isValue());


        MockWebSocketSession clientSession2 = getNewSession();
        registerClientForTestBegin(sm, clientSession2, 2);
        LockPhoneMessage msg2 = new MessageBuilder()
                .withTestId(getUTTestProvider(2))
                .build(LockPhoneMessage.class);
        msg2.setPhoneInfoModel(di);
        assertFalse(sm.getPhoneWSController().lockPhone(clientSession2, msg2));
        clientSession2.checkMessageCount(2);
        GenericErrorResponseMessage resp2 = clientSession2.getMessageFromMessagesList(1, GenericErrorResponseMessage.class);
        assertNotNull(resp2);
    }

    @Test
    public void lockPhone_phone_not_registered_Test()
    {
        IWSServicesContainer sm = createSessionManager();
        registerClientForTestBegin(sm, getNewSession(), 1);
        LockPhoneMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(LockPhoneMessage.class);

        PhoneInfoModel di = getPhoneForUT();
        msg.setPhoneInfoModel(di);

        MockWebSocketSession phoneSession = getNewSession();

        assertFalse(sm.getPhoneWSController().lockPhone(phoneSession, msg));

        phoneSession.checkMessageCount(1);

        GenericErrorResponseMessage resp = phoneSession.getMessageFromMessagesList(0, GenericErrorResponseMessage.class);
        assertNotNull(resp);
    }


    @Test
    public void unLockPhoneTest()
    {
        IWSServicesContainer sm = createSessionManager();
        MockWebSocketSession clientSession = getNewSession();
        registerClientForTestBegin(sm, clientSession, 1);
        LockPhoneMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(LockPhoneMessage.class);

        PhoneInfoModel di = getPhoneForUT();
        msg.setPhoneInfoModel(di);

        MockWebSocketSession phoneSession = getNewSession();
        sm.getSessionWSController().addSession(phoneSession);
        registerPhoneAnonym(sm, phoneSession, di);

        assertTrue(sm.getPhoneWSController().lockPhone(clientSession, msg));

        clientSession.checkMessageCount(2);

        GenericBooleanResponseMessage resp = clientSession.getMessageFromMessagesList(1, GenericBooleanResponseMessage.class);
        assertNotNull(resp);
        assertTrue(resp.isValue());

        UnLockPhoneMessage unLockPhoneMessage = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(UnLockPhoneMessage.class);
        unLockPhoneMessage.setPhoneInfoModel(di);
        assertTrue(sm.getPhoneWSController().unLockPhone(clientSession, unLockPhoneMessage));

        clientSession.checkMessageCount(3);
        GenericBooleanResponseMessage respUnlock = clientSession.getMessageFromMessagesList(2, GenericBooleanResponseMessage.class);
        assertNotNull(respUnlock);
        assertTrue(respUnlock.isValue());
    }


    private void registerPhoneAnonym(IWSServicesContainer sm, MockWebSocketSession phoneSession, PhoneInfoModel di)
    {
        RegisterPhoneMessage msg = MessageFactory.create(RegisterPhoneMessage.class);
        msg.setPhoneInfoModel(di);
        sm.getPhoneWSController().registerPhone(phoneSession, msg);
        GenericBooleanResponseMessage resp = phoneSession.getMessageFromMessagesListLast(GenericBooleanResponseMessage.class);
    }

    @Test
    public void mmscAcquireRouterPortTest()
    {
        IWSServicesContainer iSessionManager = createSessionManager();

        MockWebSocketSession session = getNewSession();

        RegisterClientLibMessage registerClientLibMessage =
                new MessageBuilder()
                        .withTestId(getUTTestProvider(1))
                        .build(RegisterClientLibMessage.class);
        registerClientLibMessage.setClientInfo(getClientInfoForTest());
        registerClientLibMessage.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);

        assertTrue(iSessionManager.getClientLibWSController().registerLibClient(session, registerClientLibMessage));

        MmscAcquireRouteRequestMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))

                .build(MmscAcquireRouteRequestMessage.class);

        assertTrue(iSessionManager.getMmscWSController().mmscAcquireRouterPort(session, msg));


        assertEquals(2, session.messages.size());
        session.getMessageFromMessagesList(1, GenericBooleanResponseMessage.class);
    }

    @Test
    public void mmscAcquireRouterPort_test_not_found_Test()
    {
        IWSServicesContainer iSessionManager = createSessionManager();

        MockWebSocketSession session = getNewSession();

        MmscAcquireRouteRequestMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))

                .build(MmscAcquireRouteRequestMessage.class);

        assertFalse(iSessionManager.getMmscWSController().mmscAcquireRouterPort(session, msg));


        assertEquals(1, session.messages.size());
        session.getMessageFromMessagesList(0, GenericErrorResponseMessage.class);
    }


    @Test
    public void processBroadcastToClients_from_phone_to_client_Test()
    {
        IWSServicesContainer iSessionManager = createSessionManager();

        MockWebSocketSession phoneSession = getNewSession();
        MockWebSocketSession clientSession = getNewSession();

        // Connect and register phone
        iSessionManager.getSessionWSController().addSession(phoneSession);
        registerPhoneForTestBegin(iSessionManager, phoneSession, 1);

        // Connect and register client
        iSessionManager.getSessionWSController().addSession(clientSession);
        registerClientForTestBegin(iSessionManager, clientSession, 1);

        // Lock phone
        LockPhoneMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(LockPhoneMessage.class);

        PhoneInfoModel di = getPhoneForUT();
        msg.setPhoneInfoModel(di);
        iSessionManager.getPhoneWSController().lockPhone(clientSession, msg);


        DevMessage devMessage = new MessageBuilder()
                .withMessageIdRandom()
                .withNoTestId()
                .withRecipientKeyClientBroadcast()
                .withSenderKey(di.getPhoneKey())
                .build(DevMessage.class);

        assertEquals(1, phoneSession.messages.size());
        assertEquals(2, clientSession.messages.size());

        iSessionManager.getBroadcastWSController().processBroadcastToClients(phoneSession, devMessage);

        assertEquals(3, clientSession.messages.size());

        DevMessage receivedMessage = clientSession.getMessageFromMessagesList(2, DevMessage.class);
        assertEquals(devMessage.getMessageID(), receivedMessage.getMessageID());
        assertEquals(getUTTestProvider(1).getTestId(), receivedMessage.getTestID());
        assertEquals(di.getPhoneKey(), receivedMessage.getSenderKey());
        assertEquals(getClientInfoForTest().getClientKey(), receivedMessage.getRecipientKey());
    }

    @Test
    public void processBroadcastToClients_fill_test_id_exists_Test()
    {
        IWSServicesContainer iSessionManager = createSessionManager();

        MockWebSocketSession phoneSession = getNewSession();
        MockWebSocketSession clientSession = getNewSession();

        // Connect and register phone
        iSessionManager.getSessionWSController().addSession(phoneSession);
        registerPhoneForTestBegin(iSessionManager, phoneSession, 1);

        // Connect and register client
        iSessionManager.getSessionWSController().addSession(clientSession);
        registerClientForTestBegin(iSessionManager, clientSession, 1);

        // Lock phone
        LockPhoneMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(LockPhoneMessage.class);

        PhoneInfoModel di = getPhoneForUT();
        msg.setPhoneInfoModel(di);
        iSessionManager.getPhoneWSController().lockPhone(clientSession, msg);


        DevMessage devMessage = new MessageBuilder()
                .withMessageIdRandom()
                .withTestId(getUTTestProvider(1))
                .withRecipientKeyClientBroadcast()
                .withSenderKey(di.getPhoneKey())
                .build(DevMessage.class);

        assertEquals(1, phoneSession.messages.size());
        assertEquals(2, clientSession.messages.size());

        iSessionManager.getBroadcastWSController().processBroadcastToClients(phoneSession, devMessage);

        assertEquals(3, clientSession.messages.size());

        DevMessage receivedMessage = clientSession.getMessageFromMessagesList(2, DevMessage.class);
        assertEquals(devMessage.getMessageID(), receivedMessage.getMessageID());
        assertEquals(devMessage.getTestID(), receivedMessage.getTestID());
        assertEquals(di.getPhoneKey(), receivedMessage.getSenderKey());
        assertEquals(getClientInfoForTest().getClientKey(), receivedMessage.getRecipientKey());
    }

    @Test
    public void processBroadcastToClients_fill_test_id_test_not_exist_Test()
    {
        IWSServicesContainer iSessionManager = createSessionManager();

        MockWebSocketSession phoneSession = getNewSession();

        // Connect and register phone
        iSessionManager.getSessionWSController().addSession(phoneSession);
        registerPhoneForTestBegin(iSessionManager, phoneSession, 1);

        DevMessage devMessage = new MessageBuilder()
                .withMessageIdRandom()
                .withNoTestId()
                .withRecipientKeyClientBroadcast()
                .withSenderKey(GenericConstants.PHONE_KEY)
                .build(DevMessage.class);

        assertEquals(1, phoneSession.messages.size());

        boolean broadcastResult = iSessionManager.getBroadcastWSController().processBroadcastToClients(phoneSession, devMessage);
        assertFalse(broadcastResult);

        assertEquals(2, phoneSession.messages.size());
        GenericErrorResponseMessage errorMsg = phoneSession.getMessageFromMessagesListLast(GenericErrorResponseMessage.class);
        assertNotNull(errorMsg);
    }

    @Test
    public void processBroadcastToClients_from_phone_to_client_test_not_exists_Test()
    {
        IWSServicesContainer iSessionManager = createSessionManager();

        MockWebSocketSession phoneSession = getNewSession();
        MockWebSocketSession clientSession = getNewSession();

        // Connect and register phone
        iSessionManager.getSessionWSController().addSession(phoneSession);
        registerPhoneForTestBegin(iSessionManager, phoneSession, 1);

        // Connect and register client
        iSessionManager.getSessionWSController().addSession(clientSession);
        registerClientForTestBegin(iSessionManager, clientSession, 1);

        // Lock phone
        LockPhoneMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(LockPhoneMessage.class);

        PhoneInfoModel di = getPhoneForUT();
        msg.setPhoneInfoModel(di);
        iSessionManager.getPhoneWSController().lockPhone(clientSession, msg);


        DevMessage devMessage = new MessageBuilder()
                .withMessageIdRandom()
                .withTestId(getUTTestProvider(2))
                .withRecipientKeyClientBroadcast()
                .withSenderKey(di.getPhoneKey())
                .build(DevMessage.class);

        assertEquals(1, phoneSession.messages.size());
        assertEquals(2, clientSession.messages.size());

        iSessionManager.getBroadcastWSController().processBroadcastToClients(phoneSession, devMessage);

        assertEquals(2, clientSession.messages.size());
        assertEquals(2, phoneSession.messages.size());
        GenericErrorResponseMessage errorMsg = phoneSession.getMessageFromMessagesListLast(GenericErrorResponseMessage.class);
        assertNotNull(errorMsg);
    }

    @Test
    public void removeSessionTest()
    {
        IWSServicesContainer iSessionManager = createSessionManager();
        MockWebSocketSession clientSession = getNewSession();

        iSessionManager.getSessionWSController().addSession(clientSession);

        assertEquals(1, iSessionManager.getSessionWSController().getOpenSessions().size());
        assertEquals(0, iSessionManager.getPhoneWSController().getConnectedPhones().size());

        iSessionManager.getSessionWSController().removeSession(clientSession.getId());

        assertEquals(0, iSessionManager.getSessionWSController().getOpenSessions().size());
        assertEquals(0, iSessionManager.getPhoneWSController().getConnectedPhones().size());
    }

    @Test
    public void removeSession_phone_Test()
    {
        IWSServicesContainer iSessionManager = createSessionManager();
        MockWebSocketSession phoneSession = getNewSession();

        iSessionManager.getSessionWSController().addSession(phoneSession);

        registerPhoneForTestBegin(iSessionManager, phoneSession, 1);

        assertEquals(1, iSessionManager.getSessionWSController().getOpenSessions().size());
        assertEquals(1, iSessionManager.getPhoneWSController().getConnectedPhones().size());

        iSessionManager.getSessionWSController().removeSession(phoneSession.getId());

        assertEquals(0, iSessionManager.getSessionWSController().getOpenSessions().size());
        assertEquals(0, iSessionManager.getPhoneWSController().getConnectedPhones().size());
    }

    @Test
    public void getConnectedPhoneTest()
    {
        IWSServicesContainer iSessionManager = createSessionManager();
        MockWebSocketSession phoneSession = getNewSession();

        iSessionManager.getSessionWSController().addSession(phoneSession);

        assertEquals(0, iSessionManager.getPhoneWSController().getConnectedPhones().size());

        registerPhoneForTestBegin(iSessionManager, phoneSession, 1);

        assertEquals(1, iSessionManager.getPhoneWSController().getConnectedPhones().size());

        phoneSession = getNewSession();
        registerPhoneForTestBegin(iSessionManager, phoneSession, 2);

        assertEquals(2, iSessionManager.getPhoneWSController().getConnectedPhones().size());
    }

    @Test
    public void registerLibClientTest()
    {
        IWSServicesContainer iSessionManager = createSessionManager();
        MockWebSocketSession clientSession = getNewSession();

        iSessionManager.getSessionWSController().addSession(clientSession);

        RegisterClientLibMessage registerClientLibMessage =
                new MessageBuilder()
                        .withMessageIdRandom()
                        .withTestId(getUTTestProvider(1))
                        .build(RegisterClientLibMessage.class);
        registerClientLibMessage.setClientInfo(getClientInfoForTest());
        registerClientLibMessage.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);
        boolean regResult = iSessionManager.getClientLibWSController().registerLibClient(clientSession, registerClientLibMessage);
        assertTrue(regResult);

        GenericBooleanResponseMessage respMsg = clientSession.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);
        assertNotNull(respMsg);
        assertEquals(registerClientLibMessage.getTestID(), respMsg.getTestID());
        assertEquals(registerClientLibMessage.getMessageID(), respMsg.getMessageID());
        assertEquals(registerClientLibMessage.getSenderKey(), respMsg.getRecipientKey());
    }

    @Test
    public void registerLibClient_invalid_version_Test()
    {
        IWSServicesContainer iSessionManager = createSessionManager();
        MockWebSocketSession clientSession = getNewSession();

        iSessionManager.getSessionWSController().addSession(clientSession);

        RegisterClientLibMessage registerClientLibMessage =
                new MessageBuilder()
                        .withMessageIdRandom()
                        .withTestId(getUTTestProvider(1))
                        .build(RegisterClientLibMessage.class);
        registerClientLibMessage.setClientInfo(getClientInfoForTest());
        registerClientLibMessage.setSystemBuildVersion("xx");
        boolean regResult = iSessionManager.getClientLibWSController().registerLibClient(clientSession, registerClientLibMessage);
        assertFalse(regResult);

        GenericErrorResponseMessage respMsg = clientSession.getMessageFromMessagesList(0, GenericErrorResponseMessage.class);
        assertNotNull(respMsg);
        assertEquals(registerClientLibMessage.getTestID(), respMsg.getTestID());
        assertEquals(registerClientLibMessage.getMessageID(), respMsg.getMessageID());
        assertEquals(registerClientLibMessage.getSenderKey(), respMsg.getRecipientKey());
    }

    private void registerClientForTestBegin(IWSServicesContainer sm, MockWebSocketSession senderSession, int testIndex)
    {
        RegisterClientLibMessage registerClientLibMessage =
                new MessageBuilder()
                        .withMessageIdRandom()
                        .withTestId(getUTTestProvider(testIndex))
                        .build(RegisterClientLibMessage.class);
        registerClientLibMessage.setClientInfo(getClientInfoForTest());
        registerClientLibMessage.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);
        sm.getClientLibWSController().registerLibClient(senderSession, registerClientLibMessage);
        GenericBooleanResponseMessage resp = senderSession.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);
        assertEquals(registerClientLibMessage.getMessageID(), resp.getMessageID());
    }

    private void registerPhoneForTestBegin(IWSServicesContainer sm, MockWebSocketSession senderSession, int testIndex)
    {
        RegisterPhoneMessage registerClientLibMessage =
                new MessageBuilder()
                        .withMessageIdRandom()
                        .withTestId(getUTTestProvider(testIndex))
                        .build(RegisterPhoneMessage.class);

        registerClientLibMessage.setPhoneInfoModel(getPhoneForUT());

        sm.getPhoneWSController().registerPhone(senderSession, registerClientLibMessage);
        GenericBooleanResponseMessage resp = senderSession.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);
        assertEquals(registerClientLibMessage.getMessageID(), resp.getMessageID());
    }

}
