//package cz.uhk.dip.mmsparams.server.websocket;
//
//import org.junit.Test;
//import org.springframework.web.socket.TextMessage;
//
//import java.util.ArrayList;
//
//import cz.uhk.dip.mmsparams.api.UnitTestBase;
//import cz.uhk.dip.mmsparams.json.JsonUtils;
//import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
//import cz.uhk.dip.mmsparams.websocket.MessageUtils;
//import cz.uhk.dip.mmsparams.websocket.WebSocketConstants;
//import cz.uhk.dip.mmsparams.websocket.messages.device.DeviceListRequestMessage;
//import cz.uhk.dip.mmsparams.websocket.messages.device.DeviceListResponseMessage;
//import cz.uhk.dip.mmsparams.websocket.messages.EmptyMessage;
//import cz.uhk.dip.mmsparams.websocket.messages.RegisterClientLibMessage;
//import cz.uhk.dip.mmsparams.websocket.messages.RegisterPhoneMessage;
//import cz.uhk.dip.mmsparams.websocket.messages.errors.GenericErrorResponseMessage;
//import cz.uhk.dip.mmsparams.websocket.model.DeviceInfo;
//import cz.uhk.dip.mmsparams.websocket.model.GroovyInfo;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotEquals;
//
//public class SimpleServerWebSocketHandlerTest extends UnitTestBase
//{
//    private SimpleServerWebSocketHandler getHandler()
//    {
//        return new SimpleServerWebSocketHandler();
//    }
//
//    @Test
//    public void Msg1() throws Exception
//    {
//        SimpleServerWebSocketHandler handler = getHandler();
//
//        MockWebSocketSession session = getNewSession();
//
//        TextMessage tm = new TextMessage("kdfjkldad");
//        handler.mockHandleTextMessage(session, tm);
//    }
//
//    @Test
//    public void registerPhoneTest() throws Exception
//    {
//        MockSimpleServerWebSocketHandler handler = getHandler();
//
//        // Phone connect
//        MockWebSocketSession phoneSession = getNewSession();
//        handler.mockAfterConnectionEstablished(phoneSession);
//
//        RegisterPhoneMessage reg = new RegisterPhoneMessage();
//        DeviceInfo di = new DeviceInfo();
//        di.setPhoneKey(getUUID());
//        reg.setDeviceInfo(di);
//        reg.setRecipientKey(WebSocketConstants.Server_Recipient_Key);
//
//        String json = MessageUtils.getSendableMessage(reg);
//
//        handler.mockHandleTextMessage(phoneSession, new TextMessage(json));
//    }
//
//
//    @Test
//    public void requestPhoneListTest() throws Exception
//    {
//        MockSimpleServerWebSocketHandler handler = getHandler();
//
//        // Phone connect
//        MockWebSocketSession phoneSession = handler.startNewSession();
//
//        RegisterPhoneMessage reg = new RegisterPhoneMessage();
//        DeviceInfo di = new DeviceInfo();
//        di.setPhoneKey(getUUID());
//        reg.setDeviceInfo(di);
//        reg.setRecipientKey(WebSocketConstants.Server_Recipient_Key);
//
//        String json = MessageUtils.getSendableMessage(reg);
//
//        handler.mockHandleTextMessage(phoneSession, new TextMessage(json));
//
//
//        MockWebSocketSession clientSession = handler.startNewSession();
//        DeviceListRequestMessage req = new DeviceListRequestMessage();
//        req.setRecipientKey(WebSocketConstants.Server_Recipient_Key);
//
//        json = MessageUtils.getSendableMessage(req);
//        handler.mockHandleTextMessage(clientSession, new TextMessage(json));
//
//
//        assertEquals(0, phoneSession.messages.size());
//        assertEquals(1, clientSession.messages.size());
//
//
//        DeviceListResponseMessage resp = JsonUtils.fromJson(clientSession.messages.get(0).getPayload().toString(), DeviceListResponseMessage.class);
//
//        assertNotEquals(null, resp);
//        assertEquals(1, resp.getDeviceInfos().size());
//
//        ArrayList<DeviceInfo> list = resp.getDeviceInfos();
//
//        assertEquals(di.getPhoneKey(), list.get(0).getPhoneKey());
//    }
//
//
//    @Test
//    public void requestGroovyTest() throws Exception
//    {
//        MockSimpleServerWebSocketHandler handler = getHandler();
//
//
//        MockWebSocketSession clientSession = handler.startNewSession();
//        RegisterClientLibMessage reg = new RegisterClientLibMessage();
//        GroovyInfo gi = new GroovyInfo();
//        gi.setGroovyKey(getUUID());
//        reg.setGroovyInfo(gi);
//
//
//        reg.setRecipientKey(WebSocketConstants.Server_Recipient_Key);
//
//        String json = MessageUtils.getSendableMessage(reg);
//
//        handler.mockHandleTextMessage(clientSession, new TextMessage(json));
//
//        // No Error
//        assertEquals(0, clientSession.messages.size());
//
//        MockWebSocketSession clientSession2 = handler.startNewSession();
//        RegisterClientLibMessage reg2 = new RegisterClientLibMessage();
//        GroovyInfo gi2 = new GroovyInfo();
//        gi2.setGroovyKey(getUUID());
//        reg2.setGroovyInfo(gi2);
//
//
//        reg2.setRecipientKey(WebSocketConstants.Server_Recipient_Key);
//
//        String json2 = MessageUtils.getSendableMessage(reg2);
//
//        handler.mockHandleTextMessage(clientSession2, new TextMessage(json2));
//
//        assertEquals(0, clientSession.messages.size());
//        assertEquals(1, clientSession2.messages.size());
//
//
//        GenericErrorResponseMessage resp = JsonUtils.fromJson(clientSession2.messages.get(0).getPayload().toString(), GenericErrorResponseMessage.class);
//
//    }
//
//
//    @Test
//    public void broadcastGroovyTest() throws Exception
//    {
//
//        MockSimpleServerWebSocketHandler handler = getHandler();
//
//
//        final MockWebSocketSession clientSession = handler.startNewSession();
//        EmptyMessage emptyMessage = new EmptyMessage();
//        emptyMessage.setRecipientKey(WebSocketConstants.Server_To_Client_Broadcast);
//        final String json = MessageUtils.getSendableMessage(emptyMessage);
//        // Send message - no recipient is registered
//        handler.mockHandleTextMessage(clientSession, new TextMessage(json));
//        assertEquals(0, clientSession.messages.size());
//
//
//        // Register groovy
//        final MockWebSocketSession clientSession2 = handler.startNewSession();
//        RegisterClientLibMessage reg = new RegisterClientLibMessage();
//        GroovyInfo gi = new GroovyInfo();
//        gi.setGroovyKey(getUUID());
//        reg.setGroovyInfo(gi);
//
//        reg.setRecipientKey(WebSocketConstants.Server_Recipient_Key);
//        final String json2 = MessageUtils.getSendableMessage(reg);
//        handler.mockHandleTextMessage(clientSession2, new TextMessage(json2));
//        assertEquals(0, clientSession2.messages.size());
//
//
//        handler.mockHandleTextMessage(clientSession, new TextMessage(json));
//
//
//        assertEquals(0, clientSession.messages.size());
//        assertEquals(1, clientSession2.messages.size());
//
//
//    }
//
//
//    @Test
//    public void forwardTest() throws Exception
//    {
//        MockSimpleServerWebSocketHandler handler = getHandler();
//
//        // Phone connect
//        MockWebSocketSession phoneSession = handler.startNewSession();
//
//
//        RegisterPhoneMessage reg = new RegisterPhoneMessage();
//        DeviceInfo di = new DeviceInfo();
//        di.setPhoneKey(getUUID());
//        reg.setDeviceInfo(di);
//        reg.setRecipientKey(WebSocketConstants.Server_Recipient_Key);
//
//        String json = MessageUtils.getSendableMessage(reg);
//
//        handler.mockHandleTextMessage(phoneSession, new TextMessage(json));
//
//
//        MockWebSocketSession clientSession = handler.startNewSession();
//        EmptyMessage emptyMessage = new EmptyMessage();
//        emptyMessage.setRecipientKey(di.getPhoneKey());
//
//        json = MessageUtils.getSendableMessage(emptyMessage);
//
//
//        handler.mockHandleTextMessage(clientSession, new TextMessage(json));
//
//
//        assertEquals(1, phoneSession.messages.size());
//        assertEquals(0, clientSession.messages.size());
//    }
//
//
//}