package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.junit.Before;
import org.junit.Test;

import cz.uhk.dip.mmsparams.api.constants.AppVersion;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.websocket.IWSServicesContainer;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class ClientLibWSControllerTest extends UnitTestBase
{
    @Before
    public void before()
    {
        super.testBeforeBase();
    }


    @Test
    public void registerClientLibTest() throws Exception
    {
        IWSServicesContainer sm = createSessionManager();

        MockWebSocketSession session = getNewSession();
        RegisterClientLibMessage msg =
                new MessageBuilder()
                        .withTestId(getUTTestProvider(1))
                        .build(RegisterClientLibMessage.class);
        ClientInfo gi = getClientInfoForTest();
        msg.setClientInfo(gi);
        msg.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);

        boolean result = sm.getClientLibWSController().registerLibClient(session, msg);
        assertTrue(result);

        session.getMessageFromMessagesListLast(GenericBooleanResponseMessage.class);
        session.checkMessageCount(1);
    }

    @Test
    public void registerClientLib_twice_with_same_message_Test() throws Exception
    {
        IWSServicesContainer sm = createSessionManager();

        MockWebSocketSession session = getNewSession();
        RegisterClientLibMessage msg =
                new MessageBuilder()
                        .withTestId(getUTTestProvider(1))
                        .build(RegisterClientLibMessage.class);
        msg.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);

        ClientInfo ci = getClientInfoForTest();
        msg.setClientInfo(ci);

        assertTrue(sm.getClientLibWSController().registerLibClient(session, msg));
        assertFalse(sm.getClientLibWSController().registerLibClient(session, msg));

        session.checkMessageCount(2);
        session.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);
        session.getMessageFromMessagesList(1, GenericErrorResponseMessage.class);
    }

    @Test
    public void registerClientLib_twice_with_two_messages_Test() throws Exception
    {
        IWSServicesContainer sm = createSessionManager();

        MockWebSocketSession session = getNewSession();
        RegisterClientLibMessage msg =
                new MessageBuilder()
                        .withTestId(getUTTestProvider(1))
                        .build(RegisterClientLibMessage.class);
        ClientInfo gi =getClientInfoForTest();
        msg.setClientInfo(gi);
        msg.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);
        assertTrue(sm.getClientLibWSController().registerLibClient(session, msg));

        RegisterClientLibMessage msg2 =
                new MessageBuilder()
                        .withTestId(iTestIdProvider)
                        .build(RegisterClientLibMessage.class);
        ClientInfo gi2 = new ClientInfo();
        gi2.setClientKey(getUUID());
        msg2.setClientInfo(gi2);
        assertFalse(sm.getClientLibWSController().registerLibClient(session, msg2));

        session.checkMessageCount(2);
        session.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);
        session.getMessageFromMessagesList(1, GenericErrorResponseMessage.class);
    }


    @Test
    public void registerClientLib_two_session_same_testID_Test() throws Exception
    {
        IWSServicesContainer sm = createSessionManager();

        MockWebSocketSession session = getNewSession();
        RegisterClientLibMessage msg =
                new MessageBuilder()
                        .withTestId(getUTTestProvider(1))
                        .build(RegisterClientLibMessage.class);
        ClientInfo gi = getClientInfoForTest(1);
        msg.setClientInfo(gi);
        sm.getClientLibWSController().registerLibClient(session, msg);

        MockWebSocketSession session2 = getNewSession();
        RegisterClientLibMessage msg2 =
                new MessageBuilder()
                        .withTestId(getUTTestProvider(1))
                        .build(RegisterClientLibMessage.class);
        ClientInfo gi2 = getClientInfoForTest(2);
        msg2.setClientInfo(gi2);
        sm.getClientLibWSController().registerLibClient(session2, msg2);

        // Error
        session2.checkMessageCount(1);
        GenericErrorResponseMessage errorMsg = session2.getMessageFromMessagesList(0, GenericErrorResponseMessage.class);
        assertNotEquals(null, errorMsg);
    }

}
