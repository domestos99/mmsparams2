package cz.uhk.dip.mmsparams.server.websocket.controllers;

import org.junit.Test;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.constants.AppVersion;
import cz.uhk.dip.mmsparams.api.utils.MessageBuilder;
import cz.uhk.dip.mmsparams.api.utils.MessageFactory;
import cz.uhk.dip.mmsparams.api.websocket.messages.DevMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.EmptyMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.clientlib.RegisterClientLibMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.errors.GenericErrorResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.generic.GenericBooleanResponseMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.phone.LockPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.messages.registration.RegisterPhoneMessage;
import cz.uhk.dip.mmsparams.api.websocket.model.clientlib.ClientInfo;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.server.UnitTestBase;
import cz.uhk.dip.mmsparams.server.mocks.MockWebSocketSession;
import cz.uhk.dip.mmsparams.server.websocket.IWSServicesContainer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class ForwardWSControllerTest extends UnitTestBase
{

    @Test
    public void forwardMessageTest() throws IOException
    {
        IWSServicesContainer sm = createSessionManager();

        // Register one phone
        MockWebSocketSession phoneSession = getNewSession();
        sm.getSessionWSController().addSession(phoneSession);

        RegisterPhoneMessage reg = MessageFactory.create(RegisterPhoneMessage.class);
        PhoneInfoModel di = getPhoneForUT();

        reg.setPhoneInfoModel(di);
        sm.getPhoneWSController().registerPhone(phoneSession, reg);


        MockWebSocketSession senderSession = getNewSession();

        EmptyMessage emptyMessage = MessageFactory.create(EmptyMessage.class);
        emptyMessage.setRecipientKey(di.getPhoneKey());


        assertFalse(sm.getForwardWSController().processForwardMessage(senderSession, emptyMessage));

        phoneSession.checkMessageCount(1);
        senderSession.getMessageFromMessagesList(0, GenericErrorResponseMessage.class);
    }

    @Test
    public void registerTwoPhone_forwardMessage_toOnlyOne_Test() throws IOException
    {
        IWSServicesContainer sm = createSessionManager();

        // Register one phone
        MockWebSocketSession phoneSession = getNewSession();
        sm.getSessionWSController().addSession(phoneSession);

        RegisterPhoneMessage reg = MessageFactory.create(RegisterPhoneMessage.class);
        PhoneInfoModel di = getPhoneForUT(1);
        reg.setPhoneInfoModel(di);
        sm.getPhoneWSController().registerPhone(phoneSession, reg);


        // Register two phone
        MockWebSocketSession phoneSession2 = getNewSession();
        sm.getSessionWSController().addSession(phoneSession2);

        RegisterPhoneMessage reg2 = MessageFactory.create(RegisterPhoneMessage.class);
        PhoneInfoModel di2 = getPhoneForUT(2);
        di2.setPhoneKey(getUUID());
        reg2.setPhoneInfoModel(di2);
        sm.getPhoneWSController().registerPhone(phoneSession2, reg2);


        MockWebSocketSession senderSession = getNewSession();

        EmptyMessage emptyMessage = new MessageBuilder()

                .build(EmptyMessage.class);
        emptyMessage.setRecipientKey(di.getPhoneKey());

        sm.getForwardWSController().processForwardMessage(senderSession, emptyMessage);

        phoneSession.checkMessageCount(1);
        phoneSession2.checkMessageCount(1);
        senderSession.checkMessageCount(1);

        phoneSession.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);
        phoneSession2.getMessageFromMessagesList(0, GenericBooleanResponseMessage.class);
        senderSession.getMessageFromMessagesList(0, GenericErrorResponseMessage.class);
    }

    @Test
    public void forwardMessage_to_unknown_recipient_Test() throws IOException
    {
        IWSServicesContainer sm = createSessionManager();

        MockWebSocketSession senderSession = getNewSession();

        EmptyMessage emptyMessage = MessageFactory.create(EmptyMessage.class);
        emptyMessage.setRecipientKey(getUUID());

        boolean forwarded = sm.getForwardWSController().processForwardMessage(senderSession, emptyMessage);

        assertEquals(1, senderSession.messages.size());
        assertFalse(forwarded);

        GenericErrorResponseMessage resp = senderSession.getMessageFromMessagesList(0, GenericErrorResponseMessage.class);
        assertNotEquals(null, resp);
    }


    @Test
    public void forwardMessage_not_testID_Test() throws IOException
    {
        IWSServicesContainer sm = createSessionManager();

        MockWebSocketSession senderSession = getNewSession();

        GenericErrorResponseMessage msg = MessageFactory.create(GenericErrorResponseMessage.class);
        // Forward unknown test
        assertFalse(sm.getForwardWSController().processForwardMessage(senderSession, msg));
        senderSession.checkMessageCount(1);
        senderSession.getMessageFromMessagesList(0, GenericErrorResponseMessage.class);
    }

    @Test
    public void processForwardMessage2Test() throws IOException
    {
        IWSServicesContainer sm = createSessionManager();

        MockWebSocketSession senderSession = getNewSession();

        GenericErrorResponseMessage msg = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .build(GenericErrorResponseMessage.class);

        // Forward test id not found test
        boolean result = sm.getForwardWSController().processForwardMessage(senderSession, msg);
        assertFalse(result);
    }

    @Test
    public void forwardMessage_to_locked_phone_Test()
    {
        IWSServicesContainer sm = createSessionManager();

        MockWebSocketSession clientSession = getNewSession();
        sm.getSessionWSController().addSession(clientSession);
        RegisterClientLibMessage msg =
                new MessageBuilder()
                        .withSenderKey(getClientInfoForTest().getClientKey())
                        .withTestId(getUTTestProvider(1))
                        .build(RegisterClientLibMessage.class);
        ClientInfo gi = getClientInfoForTest();
        msg.setClientInfo(gi);
        msg.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);

        assertTrue(sm.getClientLibWSController().registerLibClient(clientSession, msg));

        clientSession.getMessageFromMessagesListLast(GenericBooleanResponseMessage.class);
        clientSession.checkMessageCount(1);

        MockWebSocketSession phoneSession = getNewSession();
        sm.getSessionWSController().addSession(phoneSession);
        RegisterPhoneMessage regPhone = new MessageBuilder()
                .withMessageIdRandom()
                .withRecipientKeyServer()
                .withSenderKey(getPhoneForUT().getPhoneKey())
                .build(RegisterPhoneMessage.class);
        regPhone.setPhoneInfoModel(getPhoneForUT());
        assertTrue(sm.getPhoneWSController().registerPhone(phoneSession, regPhone));

        phoneSession.getMessageFromMessagesListLast(GenericBooleanResponseMessage.class);
        phoneSession.checkMessageCount(1);

        LockPhoneMessage lockPhoneMessage = new MessageBuilder()
                .withTestId(getUTTestProvider(1))
                .withMessageIdRandom()
                .withRecipientKeyServer()
                .withSenderKey(getClientInfoForTest().getClientKey())
                .build(LockPhoneMessage.class);
        lockPhoneMessage.setPhoneInfoModel(getPhoneForUT());
        assertTrue(sm.getPhoneWSController().lockPhone(clientSession, lockPhoneMessage));

        clientSession.checkMessageCount(2);
        clientSession.getMessageFromMessagesList(1, GenericBooleanResponseMessage.class);

        DevMessage devMessage = new MessageBuilder()
                .withMessageIdRandom()
                .withTestId(getUTTestProvider(1))
                .withRecipientKey(getPhoneForUT().getPhoneKey())
                .withSenderKey(getClientInfoForTest().getClientKey())
                .build(DevMessage.class);

        assertTrue(sm.getForwardWSController().processForwardMessage(clientSession, devMessage));
    }

    @Test
    public void forwardMessage_to_not_locked_phone_Test()
    {
        IWSServicesContainer sm = createSessionManager();

        MockWebSocketSession clientSession = getNewSession();
        sm.getSessionWSController().addSession(clientSession);
        RegisterClientLibMessage msg =
                new MessageBuilder()
                        .withSenderKey(getClientInfoForTest().getClientKey())
                        .withTestId(getUTTestProvider(1))
                        .build(RegisterClientLibMessage.class);
        ClientInfo gi = getClientInfoForTest();
        msg.setClientInfo(gi);
        msg.setSystemBuildVersion(AppVersion.SYSTEM_BUILD);

        assertTrue(sm.getClientLibWSController().registerLibClient(clientSession, msg));

        clientSession.getMessageFromMessagesListLast(GenericBooleanResponseMessage.class);
        clientSession.checkMessageCount(1);

        MockWebSocketSession phoneSession = getNewSession();
        sm.getSessionWSController().addSession(phoneSession);
        RegisterPhoneMessage regPhone = new MessageBuilder()
                .withMessageIdRandom()
                .withRecipientKeyServer()
                .withSenderKey(getPhoneForUT().getPhoneKey())
                .build(RegisterPhoneMessage.class);
        regPhone.setPhoneInfoModel(getPhoneForUT());
        assertTrue(sm.getPhoneWSController().registerPhone(phoneSession, regPhone));

        phoneSession.getMessageFromMessagesListLast(GenericBooleanResponseMessage.class);
        phoneSession.checkMessageCount(1);


        DevMessage devMessage = new MessageBuilder()
                .withMessageIdRandom()
                .withTestId(getUTTestProvider(1))
                .withRecipientKey(getPhoneForUT().getPhoneKey())
                .withSenderKey(getClientInfoForTest().getClientKey())
                .build(DevMessage.class);

        assertFalse(sm.getForwardWSController().processForwardMessage(clientSession, devMessage));
        clientSession.checkMessageCount(2);
        clientSession.getMessageFromMessagesList(1, GenericErrorResponseMessage.class);
    }















}
