package cz.uhk.dip.mmsparams.testsamples;

import cz.uhk.dip.mmsparams.api.enums.HttpProtocolEnum;
import cz.uhk.dip.mmsparams.api.enums.smsc.SmscConnectType;
import cz.uhk.dip.mmsparams.api.http.ServerAddressProvider;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendConnectionModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;

public class AppConfig
{
    public static final String SmallImg = "C:\\imgs\\small.jpg"; // "src/main/resources/small.jpg"; //
    public static final String smallAudio = "src/main/resources/smallaudio.mp3"; //"C:\\imgs\\smallaudio.mp3";
    public static final String SmallVideo = "C:\\imgs\\smallvideo.mp4"; //"src/main/resources/smallvideo.mp4"; // "C:\\imgs\\smallvideo.mp4";

    private static final int DefaultTimeout = 40;
    private static final int DefaultWaitSleepLoop = 100;

    private static final String NumberHonor = "+420602301498"; //"+420602301498";
    private static final String NumberSony = "+420602334928";
    private static final String NumberSamsung = "+420602334928";

    private static final String NumberHonorShort = "602301498";
    private static final String NumberHonorLong = "00420602301498";
    private static final String Honor_Custom_Name = "honor";
    private static final String Samsung_Custom_Name = "samsung";
    private static final String Sony_Custom_Name = "sony";

    public static final String username = "test";
    public static final String password = "test";


    // Edit here
    public static final String CustomNameA = Samsung_Custom_Name;
    public static final String CustomNameB = Honor_Custom_Name;

    public static final String NumberInvalid = "123";
    public static final String NumberToA = NumberSony;
    public static final String NumberToB = NumberHonor;
    public static final String NumberToBShort = NumberHonorShort;
    public static final String NumberToBLong = NumberHonorLong;

    public static final String SMSCNumber = "999994";
    public static final String SMS_CENTER_ADDRESS = "+420602909909";
    // End Edit here

    public static TestSettings getDefaultTestSettings()
    {
        // 192.168.0.104
        return new TestSettings(new ServerAddressProvider(HttpProtocolEnum.HTTP,"192.168.0.101", "4301"), DefaultTimeout, DefaultWaitSleepLoop, username, password);
    }

    public static SmscConnectModel getConnectModel()
    {
        SmscConnectModel smscConnectModel = new SmscConnectModel();
        smscConnectModel.setHost("10.21.83.155");
        smscConnectModel.setPort(7669);
        smscConnectModel.setSystemId("testMPStool");
        smscConnectModel.setPassword("SMPPtest");
        smscConnectModel.setSystemType("smpp");
        smscConnectModel.setConnType(SmscConnectType.TRANSCEIVER);


        smscConnectModel.setConnectTimeout(10000);
        smscConnectModel.setLogBytes(true);
        smscConnectModel.setRequestExpiryTimeout(10000);
        smscConnectModel.setWindowMonitorInterval(15000);
        smscConnectModel.setCountersEnabled(true);


        return smscConnectModel;
    }

    public static MmscSendConnectionModel getMmscConnectModel()
    {
        MmscSendConnectionModel conn = new MmscSendConnectionModel();

        conn.setAddress("http://127.0.0.1:2007");
        conn.setUsername("vastester");
        conn.setPassword("testervas");
        conn.setVasId("VasTester_test");
        conn.setVaspId("VasTester_test");
        conn.setMm7Version("6.8.0");

        return conn;
    }


}
