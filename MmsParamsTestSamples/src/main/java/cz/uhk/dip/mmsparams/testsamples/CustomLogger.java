package cz.uhk.dip.mmsparams.testsamples;

import cz.uhk.dip.mmsparams.clientlib.logger.IClientCustomLogger;

public class CustomLogger implements IClientCustomLogger
{
    @Override
    public void info(String info)
    {
        // Log somewhere
    }

    @Override
    public void warn(String warning)
    {
        // Log somewhere
    }

    @Override
    public void error(Throwable t)
    {
        // Log somewhere
    }

    @Override
    public void error(String message, Throwable t)
    {
        // Log somewhere
    }

    @Override
    public void error(String error)
    {
        // Log somewhere
    }

    @Override
    public void logIncomingMessage(String messageJson)
    {
        // Log somewhere
    }

    @Override
    public void logOutgoingMessage(String messageJson)
    {
        // Log somewhere
    }
}
