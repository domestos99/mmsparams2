package cz.uhk.dip.mmsparams.testsamples;

import cz.uhk.dip.mmsparams.email.receive.ReceiveEmailConfig;
import cz.uhk.dip.mmsparams.email.send.SendEmailConfig;

public class EmailConfig
{
    public static SendEmailConfig getSendEmailConfig()
    {
        SendEmailConfig config = new SendEmailConfig();

        config.setSmtpHost("localhost");
        config.setSmtpPort("465");
        config.setSmptpUsername("sender@mmsparams.cz");
        config.setSmptpPassword("password");
        config.setFrom("sender@mmsparams.cz");
        config.setTo("notify@mmsparams.cz");

        return config;
    }

    public static ReceiveEmailConfig getReceiveEmailConfig()
    {
        ReceiveEmailConfig config = new ReceiveEmailConfig();

        config.setProtocol("imap");
        config.setHost("imap.seznam.cz");
        config.setPort("143");
        config.setUserName("sender@mmsparams.cz");
        config.setPassword("password");
        config.setFolderName("Inbox");

        return config;
    }

}
