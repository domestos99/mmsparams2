package cz.uhk.dip.mmsparams.testsamples.customvalidations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemBase;

public class CustomRegexValidator extends ValidationItemBase implements IValidationItem
{
    private static final String TAG = CustomRegexValidator.class.getSimpleName();

    private Pattern pattern = Pattern.compile("my-regex");
    private String valueToValidate;

    public CustomRegexValidator(String valueToValidate, String validationName)
    {
        super(validationName);
        this.valueToValidate = valueToValidate;
    }

    @Override
    public Tuple<Boolean, ValidationResult> validate()
    {
        Matcher val = pattern.matcher(this.valueToValidate);
        boolean result = val.matches();
        return new Tuple<>(result, new ValidationResult(TAG, pattern.pattern(), this.valueToValidate, this.getValidationName(), result));
    }
}
