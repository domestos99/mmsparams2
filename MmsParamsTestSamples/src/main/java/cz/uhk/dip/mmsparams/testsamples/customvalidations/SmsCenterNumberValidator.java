package cz.uhk.dip.mmsparams.testsamples.customvalidations;

import java.util.Objects;

import cz.uhk.dip.mmsparams.api.utils.Tuple;
import cz.uhk.dip.mmsparams.api.validation.ValidationResult;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemBase;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmsCenterNumberValidator extends ValidationItemBase implements IValidationItem
{
    private static final String TAG = SmsCenterNumberValidator.class.getSimpleName();

    private String expected = AppConfig.SMS_CENTER_ADDRESS;
    private String smsCenterNumber;

    public SmsCenterNumberValidator(String smsCenterNumber, String validationName)
    {
        super(validationName);
        this.smsCenterNumber = smsCenterNumber;
    }

    @Override
    public Tuple<Boolean, ValidationResult> validate()
    {
        boolean val = Objects.equals(expected, smsCenterNumber);
        return new Tuple<>(val, new ValidationResult(TAG, expected, smsCenterNumber, this.getValidationName(), val));
    }
}
