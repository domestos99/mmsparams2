package cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus;

import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.PduHeaders2;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.AcknowledgeIndModel;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;

public class AcknowledgeIndValidationHelper
{
    public static List<IValidationItem> getMmsVersionValidation(AcknowledgeIndModel acknowledgeIndModel)
    {
        return ValidationBuilder.create()
                .withEquals(PduHeaders2.MMS_VERSION_1_2, acknowledgeIndModel.getMmsVersion(), "AcknowledgeIndModel: Mms Version OK")
                .build();
    }
}
