package cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus;

import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.PduHeaders2;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.DeliveryIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;

public class DeliveryIndValidationHelper
{
    public static List<IValidationItem> getMmsVersionValidation(DeliveryIndModel deliveryIndModel)
    {
        return ValidationBuilder.create()
                .withEquals(PduHeaders2.MMS_VERSION_1_2, deliveryIndModel.getMmsVersion(), "DeliveryIndModel: Mms Version OK")
                .build();
    }

    public static List<IValidationItem> getTransactionIdValidation(SendConfModel sendConf, DeliveryIndModel deliveryIndModel)
    {
        return ValidationBuilder.create()
                .withEquals(sendConf.getMessageId(), deliveryIndModel.getMessageId(), "SendConf-DeliveryIndModel: MessageID OK")
                .build();
    }

    public static List<IValidationItem> getStatusValidation(DeliveryIndModel deliveryIndModel)
    {
        return ValidationBuilder.create()
                .withEquals(PduHeaders2.STATUS_RETRIEVED, deliveryIndModel.getStatus(), "DeliveryIndModel: STATUS_RETRIEVED OK")
                .build();
    }


}
