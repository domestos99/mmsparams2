package cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus;

import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.PduHeaders2;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotificationIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendReqModel;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;

public class NotificationIndValidationHelper
{
    public static List<IValidationItem> getTransactionIdValidation(SendConfModel sendConf, NotificationIndModel notificationIndModel)
    {
        // WARNING! #############################
        return ValidationBuilder.create()
                .withEquals(sendConf.getMessageId(), notificationIndModel.getTransactionId(), "SendConf-NotificationInd: TransactionId OK")
                .build();
    }

    public static List<IValidationItem> getMmsVersionValidation(NotificationIndModel notificationIndModel)
    {
        return ValidationBuilder.create()
                .withEquals(PduHeaders2.MMS_VERSION_1_2, notificationIndModel.getMmsVersion(), "NotificationIndModel: Mms Version OK")
                .build();
    }

    public static List<IValidationItem> getPriority_DR_Validation(SendReqModel sendReqModel, NotificationIndModel notificationIndModel)
    {
        return ValidationBuilder.create()
                .withEquals(sendReqModel.getPriority(), notificationIndModel.getPriority(), "SendReqModel-NotificationIndModel: Priority OK")
                .withEquals(sendReqModel.getDeliveryReport(), notificationIndModel.getDeliveryReport(), "SendReqModel-NotificationIndModel: DR OK")
                .build();
    }

}
