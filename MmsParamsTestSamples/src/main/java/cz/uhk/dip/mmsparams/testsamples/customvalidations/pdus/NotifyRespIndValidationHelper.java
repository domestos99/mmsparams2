package cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus;

import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.PduHeaders2;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotifyRespIndModel;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;

public class NotifyRespIndValidationHelper
{
    public static List<IValidationItem> getMmsVersionValidation(NotifyRespIndModel notifyRespIndModel)
    {
        return ValidationBuilder.create()
                .withEquals(PduHeaders2.MMS_VERSION_1_2, notifyRespIndModel.getMmsVersion(), "NotifyRespIndModel: Mms Version OK")
                .build();
    }

    public static List<IValidationItem> getStatusRetrievendValidation(NotifyRespIndModel notifyRespIndModel)
    {
        return ValidationBuilder.create()
                .withEquals(PduHeaders2.STATUS_RETRIEVED, notifyRespIndModel.getStatus(), "NotifyRespIndModel: STATUS_RETRIEVED OK")
                .build();
    }

    public static List<IValidationItem> getStatuDeferedValidation(NotifyRespIndModel notifyRespIndModel)
    {
        return ValidationBuilder.create()
                .withEquals(PduHeaders2.STATUS_DEFERRED, notifyRespIndModel.getStatus(), "NotifyRespIndModel: STATUS_DEFERRED OK")
                .build();
    }
}
