package cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus;

import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.ContentTypeConstants;
import cz.uhk.dip.mmsparams.api.constants.PduHeaders2;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.PduPartModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendReqModel;
import cz.uhk.dip.mmsparams.clientlib.helpers.MmsHelper;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;

public class RetrieveConfValidationHelper
{
    public static List<IValidationItem> getPriority_DR_Validation(SendReqModel sendReqModel, RetrieveConfModel retrieveConfModel)
    {
        return ValidationBuilder.create()
                .withEquals(sendReqModel.getPriority(), retrieveConfModel.getPriority(), "SendReqModel-RetrieveConfModel: Priority OK")
                .withEquals(sendReqModel.getMessageClass().toLowerCase(), retrieveConfModel.getMessageClass().toLowerCase(), "SendReqModel-RetrieveConfModel: Message Class OK")
                .withEquals(sendReqModel.getDeliveryReport(), retrieveConfModel.getDeliveryReport(), "SendReqModel-RetrieveConfModel: DR OK")
                .withEquals(sendReqModel.getReadReport(), retrieveConfModel.getReadReport(), "SendReqModel-RetrieveConfModel: RR OK")
                .build();
    }

    public static List<IValidationItem> getMessageIdValidation(SendConfModel sendConf, RetrieveConfModel retrieveConfModel)
    {
        return ValidationBuilder.create()
                .withEquals(sendConf.getMessageId(), retrieveConfModel.getMessageId(), "SendConf-RetrieveConfModel: MessageID OK")
                .build();
    }

    public static List<IValidationItem> getTransactionIdValidation(SendConfModel sendConf, RetrieveConfModel retrieveConfModel)
    {
        // WARNING! #############################
        ValidationBuilder vb = ValidationBuilder.create()
                .withEquals(sendConf.getMessageId(), retrieveConfModel.getMessageId(), "SendConf-RetrieveConf: TransactionId");
        return vb.build();
    }

    public static List<IValidationItem> getMmsVersionValidation(RetrieveConfModel retrieveConfModel)
    {
        return ValidationBuilder.create()
                .withEquals(PduHeaders2.MMS_VERSION_1_3, retrieveConfModel.getMmsVersion(), "RetrieveConfModel: Mms Version OK")
                .build();
    }

    public static List<IValidationItem> getResponseStatusValidation(RetrieveConfModel retrieveConfModel)
    {
        return ValidationBuilder.create()
                .withEquals(PduHeaders2.RESPONSE_STATUS_OK, retrieveConfModel.getRetrieveStatus(), "RetrieveConfModel: Response Status OK")
                .withEquals("Mretrieveconf Ok text0", retrieveConfModel.getRetrieveText(), "RetrieveConfModel: Response text OK")
                .build();
    }

    public static List<IValidationItem> getNumberOfPartsValidation(RetrieveConfModel retrieveConfModel, int numberOfParts)
    {
        int numParts = 0;
        if (retrieveConfModel.getParts() != null)
        {
            numParts = retrieveConfModel.getParts().length;
        }
        return ValidationBuilder.create()
                .withEquals(numberOfParts, numParts, "RetrieveConfModel: Number of parts OK")
                .build();
    }

    public static List<IValidationItem> getPartsContainsTextValidation(RetrieveConfModel retrieveConfModel)
    {
        return getPartsContainsContentTypeValidation(retrieveConfModel, ContentTypeConstants.PLAIN_TEXT);
    }

    public static List<IValidationItem> getPartsContainsSmilValidation(RetrieveConfModel retrieveConfModel)
    {
        return getPartsContainsContentTypeValidation(retrieveConfModel, ContentTypeConstants.APPLICATION_SMIL);
    }

    public static List<IValidationItem> getPartsContainsContentTypeValidation(RetrieveConfModel retrieveConfModel, String contentType)
    {
        boolean result = false;
        if (retrieveConfModel.getParts() != null)
        {
            for (PduPartModel part : retrieveConfModel.getParts())
            {
                if (contentType.equals(part.getContentType()))
                {
                    result = true;
                    break;
                }
            }
        }
        return ValidationBuilder.create()
                .withTrue(result, "RetrieveConfModel: Parts constain contentType: " + contentType)
                .build();
    }

    public static List<IValidationItem> getTextEqualsValidation(SendReqModel sendReqModel, RetrieveConfModel retrieveConfModel)
    {
        return ValidationBuilder.create()
                .withEquals(MmsHelper.tryGetMessageTest(sendReqModel), MmsHelper.tryGetMessageTest(retrieveConfModel), "SendReqModel-RetrieveConfModel: Text equals")
                .build();
    }

    /**
     * Validate all PDU parts equal
     *
     * @param sendReq       SendReq PDU
     * @param retrieveConfs RetrieveConf PDU
     * @return validation items
     */
    public static List<IValidationItem> getPduPartsValidation(SendReqModel sendReq, RetrieveConfModel retrieveConfs)
    {
        final String prefix = "SendReqModel-RetrieveConfModel: PDU Parts - ";
        ValidationBuilder vb = ValidationBuilder.create();

        int sendCount = sendReq.getParts().length;
        int receivedCount = retrieveConfs.getParts().length;

        vb.withEquals(sendCount, receivedCount, prefix + "count equals");


        // vb.withCustomValidations(get)
        for (PduPartModel p : sendReq.getParts())
        {
            PduPartModel receivedPart = MmsHelper.tryGetPduPart(retrieveConfs.getParts(), p.getContentType());
            if (receivedPart == null)
            {
                // validacni chyba
                vb.withEquals(p.getContentType(), "*not found*", prefix + "PDU part not found in received PDU");
            }
            else
            {
                vb.withEquals(p.getContentType(), receivedPart.getContentType(), prefix + "Content Type");
                if (ContentTypeConstants.APPLICATION_SMIL.equals(p.getContentType()))
                {
                    // WARNING! #############################
                    // Remove \r\n character at the end of the SMIL
                    vb.withEquals(new String(p.getData()), new String(receivedPart.getData()).trim(), prefix + p.getContentType() + " PDU data");
                    vb.withEquals(p.getDataLength(), receivedPart.getDataLength() - 2, prefix + p.getContentType() + " PDU data lenght");
                }
                else
                {
                    vb.withByteArrayEquals(p.getData(), receivedPart.getData(), prefix + p.getContentType() + " PDU data");
                    vb.withEquals(p.getDataLength(), receivedPart.getDataLength(), prefix + p.getContentType() + " PDU data lenght");
                }
            }
        }


        return vb.build();
    }
}
