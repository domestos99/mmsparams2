package cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus;

import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.PduHeaders2;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;

public class SendConfValidationHelper
{
    public static List<IValidationItem> getMessageIDTransactionIDNotEmptyValidation(SendConfModel sendConf)
    {
        return ValidationBuilder.create()
                .withNotNullOrEmpty(sendConf.getMessageId(), "SendConf: MessageID not null or empty")
                .withNotNullOrEmpty(sendConf.getTransactionId(), "SendConf: TransactionId not null or empty")
                .build();
    }

    public static List<IValidationItem> getMmsVersionValidation(SendConfModel sendConf)
    {
        return ValidationBuilder.create()
                .withEquals(PduHeaders2.MMS_VERSION_1_0, sendConf.getMmsVersion(), "SendConf: Mms Version OK")
                .build();
    }

    public static List<IValidationItem> getResponseStatusValidation(SendConfModel sendConf)
    {
        return ValidationBuilder.create()
                .withEquals(PduHeaders2.RESPONSE_STATUS_OK, sendConf.getResponseStatus(), "SendConf: Response Status OK")
                .withEquals("Msendconf Ok text0", sendConf.getResponseText(), "SendConf: Response text OK")
                .build();
    }


}
