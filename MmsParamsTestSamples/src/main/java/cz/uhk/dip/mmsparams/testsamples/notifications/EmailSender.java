package cz.uhk.dip.mmsparams.testsamples.notifications;

import javax.annotation.Nullable;

import cz.uhk.dip.mmsparams.api.utils.ExceptionHelper;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfo;
import cz.uhk.dip.mmsparams.testsamples.EmailConfig;
import cz.uhk.dip.mmsparams.email.send.SendEmailService;

public class EmailSender
{
    public static void sendEmail(@Nullable ITestInstance test, @Nullable ITestInfo testInfo, @Nullable Exception exception)
    {
        try
        {
            System.out.println("Sending email with error");

            StringBuilder sb = new StringBuilder();

            if (testInfo != null)
            {
                sb.append("MmsParams - test failed with following error");
                sb.append(System.lineSeparator());

                sb.append(testInfo.getTestDescription().getTestName());
                sb.append(System.lineSeparator());
                sb.append(testInfo.getTestDescription().getTestDescription());
                sb.append(System.lineSeparator());
            }
            if (test != null && testInfo != null)
            {
                sb.append(System.lineSeparator());
                sb.append(test.getTestSettings().getServerAddressProvider().getServerAddress()).append("/testinstances/").append(testInfo.getTestId());
                sb.append(System.lineSeparator());
                sb.append(System.lineSeparator());
            }
            if (exception != null)
            {
                sb.append(exception.getMessage());
                sb.append(System.lineSeparator());
                sb.append(System.lineSeparator());
                sb.append(ExceptionHelper.getStackTrace(exception));
            }
            String subject = "MmsParams - test failed";

            SendEmailService.sendEmail(EmailConfig.getSendEmailConfig(), subject, sb.toString());

            System.out.println("Email with test error sent successfully...");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
