package cz.uhk.dip.mmsparams.testsamples.testcases;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.dip.mmsparams.api.constants.SmsConstants;
import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscAddressModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogLevel;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.IValidationItem;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemEquals;
import cz.uhk.dip.mmsparams.clientlib.validations.ValidationItemTrue;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmsAndSmscTest extends RunnableTest
{
    public SmsAndSmscTest()
    {
        super("SmsAndSmscTest", "Komplexni test se SMSC a vice mobily");
    }

    static String numA = AppConfig.NumberToB;
    static String numB = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        /**
         * 1) Odeslat sms ze SMSC na honor
         * 2) Prijmout SMS na honoru
         * 3) Odeslat prijatou sms na honoru na sony
         * 4) Prijmout SMS na sony
         * 5) Odeslat prijatou sms na sony na SMSC
         * 6) na SMSC prijmout sms ze sony
         * ---
         * 7) Kontrola, ze je text zpravy stejny + dalsi validace
         *
         */

        ClientLogFacade.setClientLogLevel(ClientLogLevel.LOG_ALL);
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        List<IValidationItem> validations = new ArrayList<>();

        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);
        PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

        SmscConnectResponseModel smscConnect = test.Smsc().connectToSMSC(AppConfig.getConnectModel());
        SmscSessionId smppSession = smscConnect.getSessionId();

        String smsOriginalText = "hello world";

        // From SMSC to HONOR
        SmscSendModel smsFromSmscToHonor = new SmscSendModel();
        smsFromSmscToHonor.setShortMessage(smsOriginalText);
        smsFromSmscToHonor.setSenderAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, AppConfig.SMSCNumber));
        smsFromSmscToHonor.setRecipientAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, numA));
        smsFromSmscToHonor.setDeliveryReport(true);

        // Send SMS from SMS
        SmscSendSmsResponseModel smsFromSmscToHonorSendStatus = test.Smsc().sendSms(smppSession, smsFromSmscToHonor);

        // TODO

        SmscDeliveryReportModel smsFromSmscToHonorDR = test.Smsc().waitFor().anyDeliveryReport(smppSession);

        // TODO


        // Wait for HONOR to receive SMS
        SmsReceiveModel smsRelivery = test.Sms().waitFor().anySmsReceived(phoneA);
        validations.add(new ValidationItemEquals(smsOriginalText, smsRelivery.getMessage(), "SMS from SMSC to HONOR - text equals"));


        // Resend received SMS from honor to sony
        SmsSendModel smsFromHonorToSony = new SmsSendModel();
        smsFromHonorToSony.setTo(numB);
        smsFromHonorToSony.setText(smsRelivery.getMessage());
        smsFromHonorToSony.setDeliveryReport(true);

        // Send SMS from HONOR to SONY
        SmsSendMessageId smsFromHonorToSonyMessageID = test.Sms().sendSms(smsFromHonorToSony, phoneA);

        SmsSendResponseModel smsFromHonorToSonySendStatus = test.Sms().waitFor().smsSentSuccessfully(smsFromHonorToSonyMessageID);
        validations.add(new ValidationItemEquals(SmsConstants.RESULT_OK, smsFromHonorToSonySendStatus.getResult(), "smsFromHonorToSonySendStatus"));


        SmsDeliveryReport smsFromHonorToSonyDR = test.Sms().waitFor().anyDeliveryReport(smsFromHonorToSonyMessageID);
//        validations.add(new ValidationItemEquals(0, smsFromHonorToSonyDR.getResult(), "smsFromHonorToSonyDR"));


        // Wait for SONY received message
        SmsReceiveModel smsFromHonorToSonyReceived = test.Sms().waitFor().anySmsReceived(phoneB);
        validations.add(new ValidationItemEquals(smsOriginalText, smsFromHonorToSonyReceived.getMessage(), "SMS from HONOR to SONY - text equals"));


        // Send SMS from SONY to SMSC
        SmsSendModel smsFromSonyToSMSC = new SmsSendModel();
        smsFromSonyToSMSC.setText(smsFromHonorToSonyReceived.getMessage());
        smsFromSonyToSMSC.setTo(AppConfig.SMSCNumber);
        smsFromSonyToSMSC.setDeliveryReport(true);


        SmsSendMessageId smsFromSonyToSmscMessageID = test.Sms().sendSms(smsFromSonyToSMSC, phoneB);

        SmsSendResponseModel smsFromSonyToSmscSendSuccessfully = test.Sms().waitFor().smsSentSuccessfully(smsFromSonyToSmscMessageID);
        validations.add(new ValidationItemEquals(SmsConstants.RESULT_OK, smsFromSonyToSmscSendSuccessfully.getResult(), "smsFromSonyToSmscSendSuccessfully"));


        SmsDeliveryReport smsFromSonyToSMSCdr = test.Sms().waitFor().anyDeliveryReport(smsFromSonyToSmscMessageID);
//        validations.add(new ValidationItemEquals(0, smsFromSonyToSMSCdr.getResult(), "smsFromSonyToSMSCdr"));

        // Wait for SMSC received SMS from SONY
        SmscDeliverSmModel smscSMSCReceivedFromSony = test.Smsc().waitFor().anySmsReceive(smppSession);

        validations.add(new ValidationItemEquals(smsOriginalText, new String(smscSMSCReceivedFromSony.getShortMessage()), "smscSMSCReceivedFromSony - SMS text equals"));

        // Disconnect from SMSC
        boolean smscDisconnectStatus = test.Smsc().disconnect(smppSession);
        validations.add(new ValidationItemTrue(smscDisconnectStatus, "smscSMSCReceivedFromSony"));


        test.Validation().validatePrintThrow(validations);


        test.testFinished();
    }


}
