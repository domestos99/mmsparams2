package cz.uhk.dip.mmsparams.testsamples.testcases.dev;

import cz.uhk.dip.mmsparams.clientlib.RunnableTestGroup;

public class DevGroupTest extends RunnableTestGroup
{
    public static void main(String[] args) throws Exception
    {
        new DevGroupTest().runTestGroup();
    }

    public DevGroupTest()
    {
        super("DevGroupTest", null);
    }

    @Override
    protected void runTests()
    {
        new DeviceGetSampleTest1().runTest();
    }
}
