package cz.uhk.dip.mmsparams.testsamples.testcases.dev;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogLevel;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class DeviceGetSampleTest1 extends RunnableTest
{
    public static void main(String[] args) throws Exception
    {
        new DeviceGetSampleTest1().runTest();
    }

    public DeviceGetSampleTest1()
    {
        super("DeviceGetSampleTest1", "Test for development - do not use while real testing!");
    }

    @Override
    protected TestSettings init()
    {
        ClientLogFacade.setClientLogLevel(ClientLogLevel.LOG_ALL);
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        MmsRecipientPhoneProfile mmsRecipientPhoneProfile = MmsRecipientPhoneProfile.createDefault();

//        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock("samsung");
//
//        boolean result = test.Mms().Profile().setRecipinentPhoneProfile(mmsRecipientPhoneProfile, phoneA);
//
//        ValidationBuilder vb = ValidationBuilder.create()
//                .withTrue(result, "Set profile OK");
//
//        test.Validation().validatePrintThrow(vb);

        // test.Sms().waitFor().anyDeliveryReport(new SmsSendMessageId("xx"), CustomTimeout.CUSTOM_TIMEOUT_10);
        //  test.Smsc().connectToSMSC(AppConfig.getConnectModel());

        //   test.Sms().sendSms(null, null);

        //test.Phones().getByCustomNameAndLock(AppConfig.Honor_Custom_Name);

//        ValidationBuilder vb = ValidationBuilder.create()
//                .withTrue(false, "xxx");
//
//        test.Validation().validatePrintThrow(vb);


        //  Thread.sleep(5000);

        // test.Phones().getByCustomNameAndLock("honor");

//        test.testFinished();
//        IValidationItem vi = new ValidationItemNotNull(1, "not null val");
//        test.Validation().validatePrintThrow(vi);
//
//        throw new RuntimeException("test");

        test.testFinished();


    }


}
