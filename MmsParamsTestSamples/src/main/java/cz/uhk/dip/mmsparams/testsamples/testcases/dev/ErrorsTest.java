package cz.uhk.dip.mmsparams.testsamples.testcases.dev;

import java.io.IOException;

import javax.annotation.Nullable;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsAttachmentSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.attachments.AttachmentHelper;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfo;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogLevel;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;
import cz.uhk.dip.mmsparams.testsamples.CustomLogger;
import cz.uhk.dip.mmsparams.testsamples.notifications.EmailSender;

public class ErrorsTest extends RunnableTest
{
    private int i;

    public ErrorsTest(int i)
    {
        super("Error Test: " + i, "Error producing errors");
        this.i = i;
    }

    @Override
    protected TestSettings init()
    {
        ClientLogFacade.setClientLogLevel(ClientLogLevel.LOG_ALL);
        ClientLogFacade.addCustomLogger(new CustomLogger());
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws IOException
    {
        switch (i)
        {
            case 0:
                break;
            case 1:
                ValidationBuilder vb = ValidationBuilder.create()
                        .withTrue(true, "xxx")
                        .withTrue(false, "yyy");
                test.Validation().validatePrintThrow(vb);
                break;
            case 2:
                test.Sms().sendSms(null, null);
                break;
            case 3:
                test.Phones().getByCustomNameAndLock("my super custom name");
                break;
            case 4:
                test.Smsc().connectToSMSC(AppConfig.getConnectModel());
                break;
            case 5:
                test.Sms().waitFor().anyDeliveryReport(new SmsSendMessageId("xx"), CustomTimeout.CUSTOM_TIMEOUT_10);
                break;
            case 7:
                SmsSendModel smsSendModel = new SmsSendModel(AppConfig.NumberToB, "xxx", true);
                PhoneInfoModel phone7 = new PhoneInfoModel();
                phone7.setPhoneKey("yy");
                SmsSendMessageId smsSend = test.Sms().sendSms(smsSendModel, phone7);
                break;
            case 8:
                MmsSendModel mms = getMmsMessage();
                PhoneInfoModel phone8 = new PhoneInfoModel();
                phone8.setPhoneKey("xx");
                test.Mms().sendMms(mms, phone8);
                break;
            case 9:
                ValidationBuilder vb2 = ValidationBuilder.create()
                        .withTrue(true, "xxx");
                test.Validation().validatePrintThrow(vb2);
                break;
        }
    }

    private MmsSendModel getMmsMessage() throws IOException
    {
        String numTo = AppConfig.NumberToB;
        MmsSendModel mms = MmsSendModel.createDefault(numTo, "MmsTest1", true);

        MmsAttachmentSendModel img1 = AttachmentHelper.loadImage(AppConfig.SmallImg);
        mms.addAttachment(img1);

        return mms;
    }

    @Override
    protected void onTestFailed(@Nullable ITestInstance test, @Nullable ITestInfo testInfo, @Nullable Exception exception)
    {
        try
        {
            EmailSender.sendEmail(test, testInfo, exception);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
