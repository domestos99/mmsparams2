package cz.uhk.dip.mmsparams.testsamples.testcases.dev;

import cz.uhk.dip.mmsparams.clientlib.RunnableTestGroup;

public class ErrorsTestGroupAll extends RunnableTestGroup
{
    public ErrorsTestGroupAll()
    {
        super("ErrorsTestGroupAll", "Test group with diffent types of errors");
    }

    public static void main(String[] args) throws Exception
    {
        new ErrorsTestGroupAll().runTestGroup();
    }

    @Override
    protected void runTests()
    {
        new ErrorsTest(0).runTest();
        new ErrorsTest(1).runTest();
        new ErrorsTest(2).runTest();
        new ErrorsTest(3).runTest();
        new ErrorsTest(4).runTest();
        new ErrorsTest(5).runTest();

        new ErrorsTest(7).runTest();
        new ErrorsTest(8).runTest();
        new ErrorsTest(9).runTest();
    }
}
