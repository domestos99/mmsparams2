package cz.uhk.dip.mmsparams.testsamples.testcases.dev;

import cz.uhk.dip.mmsparams.api.constants.AppVersion;
import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.http.HttpGetUtil;
import cz.uhk.dip.mmsparams.api.http.TaskResult;
import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.websocket.model.PingGetModel;
import cz.uhk.dip.mmsparams.api.websocket.model.SystemVersionModel;

public class PingTest
{
    public static void main(String[] args) throws Exception
    {
        final String baseUrl = "localhost:4301";
        final String urlPing = HttpConstants.URL_PREFIX_HTTP + baseUrl + "/api/Ping";
        TaskResult<String> pingResult = HttpGetUtil.getDataFromUrl(urlPing);

        if (pingResult.hasError())
            throw pingResult.getError();


        PingGetModel pingGetModel = JsonUtilsSafe.fromJson(pingResult.getResult(), PingGetModel.class);
        if (pingGetModel == null || pingGetModel.getStatus() != 1)
        {
            throw new Exception("Invalid ping value");
        }
        System.out.println("Sever ping OK");

        final String versionUrl = HttpConstants.URL_PREFIX_HTTP + baseUrl + "/api/systemversion";

        TaskResult<String> versionResult = HttpGetUtil.getDataFromUrl(versionUrl);

        if (versionResult.hasError())
            throw versionResult.getError();

        SystemVersionModel systemVersionModel = JsonUtilsSafe.fromJson(versionResult.getResult(), SystemVersionModel.class);

        if (systemVersionModel == null ||
                !AppVersion.SYSTEM_VERSION.equals(systemVersionModel.getSystemVersion()) ||
                !AppVersion.SYSTEM_BUILD.equals(systemVersionModel.getSystemBuild()))
        {
            throw new Exception("Invalid ping value");
        }
        System.out.println("Server and local version OK");


    }
}
