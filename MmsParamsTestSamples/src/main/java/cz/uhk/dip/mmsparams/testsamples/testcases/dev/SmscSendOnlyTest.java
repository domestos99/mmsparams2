package cz.uhk.dip.mmsparams.testsamples.testcases.dev;

import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;
import cz.uhk.dip.mmsparams.api.utils.TlvHelper;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscAddressModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.TlvModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogLevel;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.clientlib.utils.ThreadUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmscSendOnlyTest extends RunnableTest
{
    public SmscSendOnlyTest()
    {
        super("SmscSendOnlyTest", "Zkouska pripojeni a odpojeni k SMSC");
    }

    static String numTo = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        ClientLogFacade.setClientLogLevel(ClientLogLevel.LOG_ALL);
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        SmscConnectResponseModel smscConnectStatus = test.Smsc().connectToSMSC(AppConfig.getConnectModel());
        SmscSessionId sessionID = smscConnectStatus.getSessionId();

        SmscSendModel sms = getSmsMessage();
        // Send SMS from SMSC
        SmscSendSmsResponseModel sendResult = test.Smsc().sendSms(sessionID, sms);

        SmscDeliveryReportModel smscDeliveryReport = test.Smsc().waitFor().anyDeliveryReport(sessionID);

        // Sleep 2 seconds
        ThreadUtils.sleep(5000);

        boolean smscDisconnectStatus = test.Smsc().disconnect(sessionID);

        for (TlvModel tlv : smscDeliveryReport.getOptionalParameters())
        {
            String tag = TlvHelper.getTlvName(tlv.getTag());
            String value = new String(tlv.getValue());
        }

        ValidationBuilder vb = new ValidationBuilder();
        // Pripojeni OK
        vb.withTrue(smscConnectStatus.getStatus(), "SMSC Connect status true");
        // Odpojeni OK
        vb.withTrue(smscDisconnectStatus, "SMSC Disconnect status true");

        test.Validation().validatePrintThrow(vb);


        test.testFinished();
    }


    private static SmscSendModel getSmsMessage()
    {
        SmscSendModel sms = new SmscSendModel();

        sms.setShortMessage(SampleTextUtils.generateText("SmscTest1", 160));
        sms.setSenderAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, AppConfig.SMSCNumber));
        sms.setRecipientAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, numTo));
        sms.setDeliveryReport(true);

        return sms;
    }


}
