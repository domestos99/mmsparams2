package cz.uhk.dip.mmsparams.testsamples.testcases.groups;

import cz.uhk.dip.mmsparams.clientlib.RunnableTestGroup;
import cz.uhk.dip.mmsparams.testsamples.testcases.mms.email.MmsEmailTest1;

public class MmsEmailAllGroup extends RunnableTestGroup
{
    public static void main(String[] args)
    {
        new MmsEmailAllGroup().runTestGroup();
    }

    public MmsEmailAllGroup()
    {
        super("MmsEmailAllGroup", "Mms Email all tests");
    }

    @Override
    protected void runTests()
    {
        new MmsEmailTest1().runTest();
    }
}
