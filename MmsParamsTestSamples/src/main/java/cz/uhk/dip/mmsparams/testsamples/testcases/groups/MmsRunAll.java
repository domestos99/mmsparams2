package cz.uhk.dip.mmsparams.testsamples.testcases.groups;


import cz.uhk.dip.mmsparams.clientlib.RunnableTestGroup;
import cz.uhk.dip.mmsparams.testsamples.testcases.mms.MmsTest1;
import cz.uhk.dip.mmsparams.testsamples.testcases.mms.MmsTest10;
import cz.uhk.dip.mmsparams.testsamples.testcases.mms.MmsTest2;
import cz.uhk.dip.mmsparams.testsamples.testcases.mms.MmsTest3;
import cz.uhk.dip.mmsparams.testsamples.testcases.mms.MmsTest4;
import cz.uhk.dip.mmsparams.testsamples.testcases.mms.MmsTest5;
import cz.uhk.dip.mmsparams.testsamples.testcases.mms.MmsTest6SmsWebLink;
import cz.uhk.dip.mmsparams.testsamples.testcases.mms.MmsTest7;
import cz.uhk.dip.mmsparams.testsamples.testcases.mms.MmsTest8;
import cz.uhk.dip.mmsparams.testsamples.testcases.mms.MmsTest9;

public class MmsRunAll extends RunnableTestGroup
{
    public static void main(String[] args) throws Exception
    {
        new MmsRunAll().runTestGroup();
    }

    public MmsRunAll()
    {
        super("MmsRunAll", "Mms all tests");
    }

    @Override
    protected void runTests()
    {
        new MmsTest1().runTest();
        new MmsTest2().runTest();
        new MmsTest3().runTest();
        new MmsTest4().runTest();
        new MmsTest5().runTest();
       // new MmsTest6SmsWebLink().runTest();
        new MmsTest7().runTest();
        new MmsTest8().runTest();
        new MmsTest9().runTest();
        new MmsTest10().runTest();
    }
}
