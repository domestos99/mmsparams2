package cz.uhk.dip.mmsparams.testsamples.testcases.groups;

import cz.uhk.dip.mmsparams.clientlib.RunnableTestGroup;

public class QuickRunGroup extends RunnableTestGroup
{
    public static void main(String[] args)
    {
        new QuickRunGroup().runTestGroup();
    }

    public QuickRunGroup()
    {
        super("QuickRunGroup", "Test Quick - for testing one test purposes");
    }


    @Override
    protected void runTests()
    {
        // Add test here

    }
}
