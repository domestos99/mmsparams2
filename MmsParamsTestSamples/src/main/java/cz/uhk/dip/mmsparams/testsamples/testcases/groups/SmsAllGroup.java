package cz.uhk.dip.mmsparams.testsamples.testcases.groups;

import cz.uhk.dip.mmsparams.clientlib.RunnableTestGroup;
import cz.uhk.dip.mmsparams.testsamples.testcases.sms.SmsTest1;
import cz.uhk.dip.mmsparams.testsamples.testcases.sms.SmsTest10;
import cz.uhk.dip.mmsparams.testsamples.testcases.sms.SmsTest2;
import cz.uhk.dip.mmsparams.testsamples.testcases.sms.SmsTest3;
import cz.uhk.dip.mmsparams.testsamples.testcases.sms.SmsTest4;
import cz.uhk.dip.mmsparams.testsamples.testcases.sms.SmsTest5;
import cz.uhk.dip.mmsparams.testsamples.testcases.sms.SmsTest6;
import cz.uhk.dip.mmsparams.testsamples.testcases.sms.SmsTest7;
import cz.uhk.dip.mmsparams.testsamples.testcases.sms.SmsTest8;
import cz.uhk.dip.mmsparams.testsamples.testcases.sms.SmsTest9;

public class SmsAllGroup extends RunnableTestGroup
{
    public static void main(String[] args)
    {
        new SmsAllGroup().runTestGroup();
    }

    public SmsAllGroup()
    {
        super("SmsAllGroup", "All SMS tests");
    }

    @Override
    protected void runTests()
    {
//        boolean connectResult = new RemoteConnectSmsTest().runTest();
//        if (!connectResult)
//        {
//            // Connect failed - dont continue
//            return;
//        }
        new SmsTest1().runTest();
        new SmsTest2().runTest();
        new SmsTest3().runTest();
        new SmsTest4().runTest();
        new SmsTest5().runTest();
        new SmsTest6().runTest();
        new SmsTest7().runTest();
        new SmsTest8().runTest();
        new SmsTest9().runTest();
        new SmsTest10().runTest();
    }
}
