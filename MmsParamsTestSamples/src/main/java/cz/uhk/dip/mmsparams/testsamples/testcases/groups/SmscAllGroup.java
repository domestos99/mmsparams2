package cz.uhk.dip.mmsparams.testsamples.testcases.groups;

import cz.uhk.dip.mmsparams.clientlib.RunnableTestGroup;
import cz.uhk.dip.mmsparams.testsamples.testcases.smsc.SmscTest1;
import cz.uhk.dip.mmsparams.testsamples.testcases.smsc.SmscTest2;
import cz.uhk.dip.mmsparams.testsamples.testcases.smsc.SmscTest3;
import cz.uhk.dip.mmsparams.testsamples.testcases.smsc.SmscTest4;
import cz.uhk.dip.mmsparams.testsamples.testcases.smsc.SmscTest5;
import cz.uhk.dip.mmsparams.testsamples.testcases.smsc.SmscTest6;
import cz.uhk.dip.mmsparams.testsamples.testcases.smsc.SmscTest7;
import cz.uhk.dip.mmsparams.testsamples.testcases.smsc.SmscTest8;

public class SmscAllGroup extends RunnableTestGroup
{
    public static void main(String[] args)
    {
        new SmscAllGroup().runTestGroup();
    }

    public SmscAllGroup()
    {
        super("SmscAllGroup", "All SMSC tests");
    }

    @Override
    protected void runTests()
    {
        new SmscTest1().runTest();
        new SmscTest2().runTest();

        // zprava delky 320 znaku - pouziti tlv
        new SmscTest3().runTest();

        new SmscTest4().runTest();
        new SmscTest5().runTest();

        // 6 Problem s kodovanim
        new SmscTest6().runTest();
        // 7 odeslani SMSC -> SMSC - zprava neprijde, ale DR ano
        new SmscTest7().runTest();

        // 8 asi nefunguje spravne - jsou tam dve session a nevi se, na kterou to prijde
        // new SmscTest8().runTest();
    }
}
