package cz.uhk.dip.mmsparams.testsamples.testcases.mms;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.DeliveryIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotificationIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotifyRespIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;
import cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus.DeliveryIndValidationHelper;
import cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus.NotificationIndValidationHelper;
import cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus.NotifyRespIndValidationHelper;
import cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus.RetrieveConfValidationHelper;
import cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus.SendConfValidationHelper;

public class MmsTemplateTest
{
    protected void runTemplate(ITestInstance test, MmsSendModel mms)
    {
        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);
        PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

        MmsRecipientPhoneProfile recProfiles = MmsRecipientPhoneProfile.createDefault();
        boolean setProfileResult = test.Mms().Profile().setRecipinentPhoneProfile(recProfiles, phoneB);

        MmsSendMessageId mmsSendReqId = test.Mms().sendMms(mms, phoneA);

        SendReqModel sendReq = test.Mms().waitFor().anySendReq(phoneA);

        // Potvrzeni odeslani
        SendConfModel sendConf = test.Mms().waitFor().anyMmsConf(mmsSendReqId);

        // Mms notification was received
        NotificationIndModel notifInd = test.Mms().waitFor().anyNotificationInd(phoneB);

        // Mms was received and downloaded
        RetrieveConfModel retrieveConfs = test.Mms().waitFor().anyRetrieveConf(phoneB);

        // What NotifyResp has recipient sent
        NotifyRespIndModel notifyRespIndModel = test.Mms().waitFor().anyNotifyRespInd(phoneB);

        // Wait for delivery report
        DeliveryIndModel deliveryIndModel = test.Mms().waitFor().anyDeliveryInd(phoneA);


        ValidationBuilder vb = new ValidationBuilder();


        vb.withTrue(setProfileResult, "Set Recipient profile");

        // SendConf validations
        vb.withCustomValidations(SendConfValidationHelper.getMessageIDTransactionIDNotEmptyValidation(sendConf));
        vb.withCustomValidations(SendConfValidationHelper.getMmsVersionValidation(sendConf));
        vb.withCustomValidations(SendConfValidationHelper.getResponseStatusValidation(sendConf));

        // DeliveryInd validations
        vb.withCustomValidations(DeliveryIndValidationHelper.getMmsVersionValidation(deliveryIndModel));
        vb.withCustomValidations(DeliveryIndValidationHelper.getTransactionIdValidation(sendConf, deliveryIndModel));
        vb.withCustomValidations(DeliveryIndValidationHelper.getStatusValidation(deliveryIndModel));


        // NotificationInd validations
        vb.withCustomValidations(NotificationIndValidationHelper.getTransactionIdValidation(sendConf, notifInd));
        vb.withCustomValidations(NotificationIndValidationHelper.getMmsVersionValidation(notifInd));
        // vb.withCustomValidations(NotificationIndValidationHelper.getPriority_DR_Validation(sendReq, notifInd));

        // NotifyRespInd validations
        vb.withCustomValidations(NotifyRespIndValidationHelper.getMmsVersionValidation(notifyRespIndModel));
        vb.withCustomValidations(NotifyRespIndValidationHelper.getStatusRetrievendValidation(notifyRespIndModel));


        // RetrieveConf validations

        vb.withCustomValidations(RetrieveConfValidationHelper.getPriority_DR_Validation(sendReq, retrieveConfs));
        vb.withCustomValidations(RetrieveConfValidationHelper.getMessageIdValidation(sendConf, retrieveConfs));
        vb.withCustomValidations(RetrieveConfValidationHelper.getTransactionIdValidation(sendConf, retrieveConfs));
        vb.withCustomValidations(RetrieveConfValidationHelper.getMmsVersionValidation(retrieveConfs));
        vb.withCustomValidations(RetrieveConfValidationHelper.getMmsVersionValidation(retrieveConfs));
        vb.withCustomValidations(RetrieveConfValidationHelper.getResponseStatusValidation(retrieveConfs));

        vb.withCustomValidations(RetrieveConfValidationHelper.getPartsContainsSmilValidation(retrieveConfs));
        vb.withCustomValidations(RetrieveConfValidationHelper.getTextEqualsValidation(sendReq, retrieveConfs));

        vb.withCustomValidations(RetrieveConfValidationHelper.getPduPartsValidation(sendReq, retrieveConfs));


        // Validate, that SMS text is the same
        test.Validation().validatePrintThrow(vb);


        // Should be last command in test
        test.testFinished();
    }


}
