package cz.uhk.dip.mmsparams.testsamples.testcases.mms;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsAttachmentSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.DeliveryIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.attachments.AttachmentHelper;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;
import cz.uhk.dip.mmsparams.testsamples.customvalidations.CustomRegexValidator;

public class MmsTest6SmsWebLink extends RunnableTest
{
    public MmsTest6SmsWebLink()
    {
        super("MmsReceivedAsSmsWebLink", "Send MMS from one phone and receive SMS with link to MMS web album");
    }

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        ValidationBuilder vb = new ValidationBuilder();

        // Sender and recipient are the same so only copy reference
        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);
        PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

        MmsSendModel mms = getMmsMessage();
        MmsSendMessageId mmsSendReqId = test.Mms().sendMms(mms, phoneA);

        // Potvrzeni odeslani
        SendConfModel sendConf = test.Mms().waitFor().anyMmsConf(mmsSendReqId);

        // Wait for delivery report
        DeliveryIndModel deliveryIndModels = test.Mms().waitFor().anyDeliveryInd(phoneA);

        boolean deliveryInd = test.Mms().mustTimeoutFor().notificationInd(phoneB);

        vb.withTrue(deliveryInd, "NotificationInd not received");

        // Phone B receives SMS with link to MMS web album
        SmsReceiveModel receiveSms = test.Sms().waitFor().anySmsReceived(phoneB);

        // Check SMS test with Reqex validator
        vb.withCustomValidation(new CustomRegexValidator(receiveSms.getMessage(), "Sms Regex validation"));


        // Validate, that SMS text is the same
        test.Validation().validatePrintThrow(vb);


        // Should be last command in test
        test.testFinished();
    }

    private static MmsSendModel getMmsMessage() throws IOException
    {
        String numTo = AppConfig.NumberToB;
        MmsSendModel mms = MmsSendModel.createDefault(numTo, "MmsTest1", true);

        MmsAttachmentSendModel img1 = AttachmentHelper.loadImage(AppConfig.SmallImg);
        mms.addAttachment(img1);

        return mms;
    }


}
