package cz.uhk.dip.mmsparams.testsamples.testcases.mms;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class MmsTest7 extends RunnableTest
{
    public MmsTest7()
    {
        super("MmsTest7", "Obsah MMS jen text");
    }

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    private static String numTo = AppConfig.NumberToB;

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        new MmsTemplateTest().runTemplate(test, getMmsMessage());
    }

    private static MmsSendModel getMmsMessage()
    {
        MmsSendModel mms = MmsSendModel.createDefault(numTo, "MmsTest7", true);
        // No Attachment - text only
        return mms;
    }
}
