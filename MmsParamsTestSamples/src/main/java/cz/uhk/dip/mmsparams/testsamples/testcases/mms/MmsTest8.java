package cz.uhk.dip.mmsparams.testsamples.testcases.mms;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsAttachmentSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.attachments.AttachmentHelper;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class MmsTest8 extends RunnableTest
{
    public MmsTest8()
    {
        super("MmsTest8", "obsah MMS jen obrazek");
    }

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    private static String numTo = AppConfig.NumberToB;

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        new MmsTemplateTest().runTemplate(test, getMmsMessage());
    }

    private static MmsSendModel getMmsMessage() throws IOException
    {
        // Only attachment; no text
        MmsSendModel mms = MmsSendModel.createDefault(numTo, null, true);

        MmsAttachmentSendModel img1 = AttachmentHelper.loadImage(AppConfig.SmallImg);
        mms.addAttachment(img1);

        return mms;
    }
}
