package cz.uhk.dip.mmsparams.testsamples.testcases.mms;

import java.io.IOException;
import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsAttachmentSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.DeliveryIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotificationIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotifyRespIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.attachments.AttachmentHelper;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class MmsTestMultiple extends RunnableTest
{
    public MmsTestMultiple()
    {
        super("MmsTestMultiple", "Send 2 MMSs");
    }

    static String numTo = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);
        PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

        MmsRecipientPhoneProfile recProfiles = MmsRecipientPhoneProfile.createDefault();
        boolean profileSetOk = test.Mms().Profile().setRecipinentPhoneProfile(recProfiles, phoneB);


        MmsSendModel mms = getMmsMessage();
        MmsSendModel mms2 = getMmsMessage();

        MmsSendMessageId mmsSendReqId = test.Mms().sendMms(mms, phoneA);
        MmsSendMessageId mmsSendReqId2 = test.Mms().sendMms(mms2, phoneA);

        // Potvrzeni odeslani
        SendConfModel sendConf = test.Mms().waitFor().anyMmsConf(mmsSendReqId);
        SendConfModel sendConf2 = test.Mms().waitFor().anyMmsConf(mmsSendReqId2);

        // Mms notification was received
        List<NotificationIndModel> notifInd = test.Mms().waitFor().anyNotificationInd(phoneB, 2);

        // Mms was received and downloaded
        List<RetrieveConfModel> retrieveConfs = test.Mms().waitFor().anyRetrieveConf(phoneB, 2);

        // What NotifyResp has recipient sent
        List<NotifyRespIndModel> notifyRespIndModels = test.Mms().waitFor().anyNotifyRespInd(phoneB, 2);

        // Wait for delivery report
        List<DeliveryIndModel> deliveryIndModels = test.Mms().waitFor().anyDeliveryInd(phoneA, 2);

        // TODO validate


        // Should be last command in test
        test.testFinished();
    }

    private static MmsSendModel getMmsMessage() throws IOException
    {
        MmsSendModel mms = MmsSendModel.createDefault(numTo, "MmsTestMultiple", true);

        MmsAttachmentSendModel img1 = AttachmentHelper.loadImage(AppConfig.SmallImg);
        mms.addAttachment(img1);

        return mms;
    }


}
