package cz.uhk.dip.mmsparams.testsamples.testcases.mms;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.enums.ReadStatus;
import cz.uhk.dip.mmsparams.api.utils.MmsUtils;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsAttachmentSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsDownloadOptionsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsReadReportOptionsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.DeliveryIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotificationIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotifyRespIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.attachments.AttachmentHelper;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class MmsTestReadReport extends RunnableTest
{
    public MmsTestReadReport()
    {
        super("MmsTestReadReport", "MMS with read report");
    }

    static String numTo = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);
        PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

        MmsRecipientPhoneProfile recProfiles = MmsRecipientPhoneProfile.create(
                MmsDownloadOptionsModel.createManualDownload(10),
                MmsReadReportOptionsModel.createNoReadReport(10, ReadStatus.RAED),
                true);

        boolean profileSetOk = test.Mms().Profile().setRecipinentPhoneProfile(recProfiles, phoneB);


        MmsSendModel mms = getMmsMessage();

        MmsSendMessageId mmsSendReqId = test.Mms().sendMms(mms, phoneA);

        // Potvrzeni odeslani
        SendConfModel sendConf = test.Mms().waitFor().anyMmsConf(mmsSendReqId);

        // Mms notification was received
        NotificationIndModel notifInd = test.Mms().waitFor().anyNotificationInd(phoneB);

        // Mms was received and downloaded
        RetrieveConfModel retrieveConfs = test.Mms().waitFor().anyRetrieveConf(phoneB);

        // What NotifyResp has recipient sent
        NotifyRespIndModel notifyRespIndModels = test.Mms().waitFor().anyNotifyRespInd(phoneB);

        // Wait for delivery report
        DeliveryIndModel deliveryIndModels = test.Mms().waitFor().anyDeliveryInd(phoneA);


        ValidationBuilder vb = new ValidationBuilder();

        vb.withTrue(profileSetOk, "Recipient profile set OK");

        // TODO add validations


        test.Validation().validatePrintThrow(vb);

        test.testFinished();
    }

    private static MmsSendModel getMmsMessage() throws IOException
    {
        MmsSendModel mms = new MmsSendModel();

        mms.addTo(numTo);
        mms.setText("Hello world");

        mms = MmsUtils.setDefaultTestProfile(mms);

        mms.setDeliveryReport(true);
        mms.setReadReport(false);

        MmsAttachmentSendModel img1 = AttachmentHelper.loadImage(AppConfig.SmallImg);
        mms.addAttachment(img1);

        return mms;
    }


}
