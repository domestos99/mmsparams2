package cz.uhk.dip.mmsparams.testsamples.testcases.mms.email;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.websocket.model.email.ReceiveEmailMessageModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsAttachmentSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.attachments.AttachmentHelper;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.clientlib.utils.ThreadUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;
import cz.uhk.dip.mmsparams.testsamples.EmailConfig;
import cz.uhk.dip.mmsparams.testsamples.customvalidations.pdus.SendConfValidationHelper;

public class MmsEmailTest1 extends RunnableTest
{
    public MmsEmailTest1()
    {
        super("MmsEmailTest1", "Send MMS to email");
    }

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    private String email = "domestos99@seznam.cz";
    private String emailFrom = "<420602334928@mms.o2.cz>";

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);

        MmsSendModel mms = getMmsMessage();
        MmsSendMessageId mmsSendReqId = test.Mms().sendMms(mms, phoneA);

        SendReqModel sendReq = test.Mms().waitFor().anySendReq(phoneA);

        // Potvrzeni odeslani
        SendConfModel sendConf = test.Mms().waitFor().anyMmsConf(mmsSendReqId);

        ValidationBuilder vb = new ValidationBuilder();

        // SendConf validations
        vb.withCustomValidations(SendConfValidationHelper.getMessageIDTransactionIDNotEmptyValidation(sendConf));
        vb.withCustomValidations(SendConfValidationHelper.getMmsVersionValidation(sendConf));
        vb.withCustomValidations(SendConfValidationHelper.getResponseStatusValidation(sendConf));

        // Wait for a while to receive a email
        ThreadUtils.sleep(CustomTimeout.CUSTOM_TIMEOUT_20);
        ReceiveEmailMessageModel emailMessage = test.Email().waitFor().anyEmailReceive(EmailConfig.getReceiveEmailConfig());

        vb.withEquals(emailFrom, emailMessage.getFrom(), "Email from");
        vb.withEquals(3, emailMessage.getParts().size(), "Email parts count");

        vb.withByteArrayEquals(mms.getText().getBytes(), emailMessage.getParts().get(1).getData(), "Email text attachment equals");
        vb.withByteArrayEquals(mms.getMmsAttachmentSendModel().get(0).getData(), emailMessage.getParts().get(2).getData(), "Email image equals");


        // Validate, that SMS text is the same
        test.Validation().validatePrintThrow(vb);


        // Should be last command in test
        test.testFinished();
    }

    private MmsSendModel getMmsMessage() throws IOException
    {
        MmsSendModel mms = MmsSendModel.createDefault(email, SampleTextUtils.generateText16BIT("MmsEmailTest1", 70), true);

        MmsAttachmentSendModel img1 = AttachmentHelper.loadImage(AppConfig.SmallImg);
        mms.addAttachment(img1);

        return mms;
    }


}
