package cz.uhk.dip.mmsparams.testsamples.testcases.mmsc;

import java.util.List;

import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7ContentModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7DeliveryReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MmscAcquireRouteModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class MmscTestReceive extends RunnableTest
{
    public MmscTestReceive()
    {
        super("MmscTestSendAndDR", "MmscTestSendAndDR - prvni test MMSC + DR");
    }

    static String numFrom = "+420602701415";
    static String numTo = "9999081";

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        // Zamek prijeti MMS
        // MMS dorucene na toto cislou budou presmerovany do tohoto testu
        MmscAcquireRouteModel acquire = new MmscAcquireRouteModel();
        acquire.setPattern(numTo);
        test.Mmsc().acquirePortRouter(acquire);


        MM7DeliveryReqModel receivedMMS = test.Mmsc().waitFor().anyDeliveryReq();

        // TODO add validations
        MM7ContentModel content = receivedMMS.getContent();

        // mmsParts contains smil, text, media
        List<MM7ContentModel> mmsParts = content.getParts();



        test.testFinished();
    }
}
