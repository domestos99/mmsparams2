package cz.uhk.dip.mmsparams.testsamples.testcases.mmsc;

import java.io.IOException;

import cz.uhk.dip.mmsparams.api.enums.AddressType;
import cz.uhk.dip.mmsparams.api.enums.ChargedParty;
import cz.uhk.dip.mmsparams.api.enums.MM7Encoding;
import cz.uhk.dip.mmsparams.api.enums.MessageClass;
import cz.uhk.dip.mmsparams.api.enums.Priority;
import cz.uhk.dip.mmsparams.api.enums.RecipientType;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Address;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7RelativeDate;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7SubmitResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.MM7Text;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendConnectionModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendMmsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mmsc.send.MmscSendModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.attachments.AttachmentHelperMM7;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class MmscTestSendOnlyImage extends RunnableTest
{
    public MmscTestSendOnlyImage()
    {
        super("MmscTestSendAndDR", "MmscTestSendAndDR - odeslani MMS z MMSC na telefon");
    }

    static String numFrom = "9999081";
    static String numTo = "+420602701415";

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        MmscSendConnectionModel conn = AppConfig.getMmscConnectModel();

        MmscSendMmsModel mmsModel = createMMS();

        MmscSendModel mmscSendModel = new MmscSendModel(conn, mmsModel);
        MM7SubmitResponseModel submitResp = test.Mmsc().sendMms(mmscSendModel);

        ValidationBuilder vb = ValidationBuilder.create();

        vb.withEquals(1000, submitResp.getStatusCode(), "MMSC Submit Resp status code");


        test.testFinished();
    }

    private MmscSendMmsModel createMMS() throws IOException
    {
        MmscSendMmsModel mmsModel = new MmscSendMmsModel();

        MM7Address sender = new MM7Address(numFrom, AddressType.NUMBER, RecipientType.FROM);
        mmsModel.setSenderAddress(sender);

        MM7Address recipient = new MM7Address(numTo, AddressType.NUMBER, RecipientType.TO);
        mmsModel.addRecipientTo(recipient);

        mmsModel.setMessageClass(MessageClass.PERSONAL);
        mmsModel.setExpiryDate(new MM7RelativeDate("PT2M"));

        // NO DR
        mmsModel.setDeliveryReport(false);
        mmsModel.setPriority(Priority.NORMAL);
        mmsModel.setSubject("MmscTestSendOnly_Subject");
        mmsModel.setChangedParty(ChargedParty.SENDER);

        // Add Text
        String text = "TestovaciMMS";
        mmsModel.addMm7Text(new MM7Text(text, MM7Encoding.BASE64));

        // Add img
        mmsModel.addMm7Attachment(AttachmentHelperMM7.create(AppConfig.SmallImg, "my-img-1"));

        return mmsModel;
    }


}
