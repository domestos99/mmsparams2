package cz.uhk.dip.mmsparams.testsamples.testcases.remoteconnect;

import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class RemoteConnectSmsTest extends RunnableTest
{
    public RemoteConnectSmsTest()
    {
        super("RemoteConnectSmsTest", "Phone remote connect test");
    }

    static String phoneToConnectNumber = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        // Sender
        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);

        // Phone to connect
        PhoneInfoModel phoneB = test.RemoteConnect().connectWithSms(phoneA, phoneToConnectNumber,
                AppConfig.CustomNameB);


        ValidationBuilder vb = new ValidationBuilder();
        vb.withNotNull(phoneB, "phoneB");

        test.Validation().validatePrintThrow(vb);


        test.testFinished();
    }


}
