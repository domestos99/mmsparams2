package cz.uhk.dip.mmsparams.testsamples.testcases.remoteconnect;

import cz.uhk.dip.mmsparams.api.http.auth.JwtRequest;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class RemoteConnectSmscTest extends RunnableTest
{
    public RemoteConnectSmscTest()
    {
        super("RemoteConnectSmscTest", "Phone remote connect test");
    }

    static String smscNumberFrom = AppConfig.SMSCNumber;
    static String phoneToConnectNumber = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        SmscConnectModel smscConnectModel = AppConfig.getConnectModel();

        PhoneInfoModel phoneB = test.RemoteConnect().connectWithSmsc(smscConnectModel, smscNumberFrom, phoneToConnectNumber,
                AppConfig.CustomNameB);


        ValidationBuilder vb = new ValidationBuilder();
        vb.withNotNull(phoneB, "Phone B connected");

        test.Validation().validatePrintThrow(vb);


        test.testFinished();
    }


}
