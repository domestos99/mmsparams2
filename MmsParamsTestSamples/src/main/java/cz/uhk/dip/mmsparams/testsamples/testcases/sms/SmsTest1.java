package cz.uhk.dip.mmsparams.testsamples.testcases.sms;

import javax.annotation.Nullable;

import cz.uhk.dip.mmsparams.api.constants.SmsConstants;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.communication.testinfo.ITestInfo;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogLevel;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;
import cz.uhk.dip.mmsparams.testsamples.CustomLogger;
import cz.uhk.dip.mmsparams.testsamples.customvalidations.SmsCenterNumberValidator;
import cz.uhk.dip.mmsparams.testsamples.notifications.EmailSender;

public class SmsTest1 extends RunnableTest
{
    public SmsTest1()
    {
        super("SmsTest1", "Zprava delky 160 znaku; 7bit; delivery report");
    }

    private static String numTo = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        ClientLogFacade.setClientLogLevel(ClientLogLevel.LOG_ALL);
        ClientLogFacade.addCustomLogger(new CustomLogger());

        // Test settings contains Server address; Log Path; Default timeout for all operations
        TestSettings ts = AppConfig.getDefaultTestSettings();
        return ts;
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);

        // Sender and recipient are the same so only copy reference
        PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

        SmsSendModel sms = new SmsSendModel();
        sms.setTo(numTo);
        sms.setServiceCenterSender(AppConfig.SMS_CENTER_ADDRESS); // If null = default
        sms.setDeliveryReport(true);

        // Generate random text with prefix and length 160 characters total
        String smsText = SampleTextUtils.generateText("SmsTest1", 160);
        sms.setText(smsText);

        SmsSendMessageId smsSendID = test.Sms().sendSms(sms, phoneA);

        SmsSendResponseModel sendOk = test.Sms().waitFor().smsSentSuccessfully(smsSendID);

        SmsReceiveModel smsReceiveModel = test.Sms().waitFor().anySmsReceived(phoneB);

        SmsDeliveryReport dr = test.Sms().waitFor().anyDeliveryReport(smsSendID);

        // Validations
        ValidationBuilder vb = new ValidationBuilder();

        vb.withNotNull(sendOk, "Send SMS OK");
        vb.withEquals(SmsConstants.RESULT_OK, sendOk.getResult(), "SMS send ok");

        vb.withEquals(smsText, smsReceiveModel.getMessage(), "Sms text");
        vb.withTrue(dr.getStatusReportMessage(), "Is delivery report");

        // Custom validator - validate SMS center address in SMS Delivery Report
        SmsCenterNumberValidator smsCenterNumberValidator = new SmsCenterNumberValidator(dr.getServiceCenterAddress(), "Service center address");
        vb.withCustomValidation(smsCenterNumberValidator);

        // Date time when DR was received?
        // new Date(dr.getTimestampMillis());

        test.Validation().validatePrintThrow(vb);

        test.testFinished();
    }

    @Override
    protected void onTestFailed(@Nullable ITestInstance test, @Nullable ITestInfo testInfo, @Nullable Exception exception)
    {
        try
        {
            // TODO setup EmailSender and uncomment
            // EmailSender.sendEmail(test, testInfo, exception);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


}
