package cz.uhk.dip.mmsparams.testsamples.testcases.sms;

import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmsTest2 extends RunnableTest
{
    public SmsTest2()
    {
        super("SmsTest2", "Zprava delky 70 znaku; 16bit; delivery report");
    }

    static String numTo = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    private SmsSendModel getSms()
    {
        SmsSendModel sms = new SmsSendModel();
        sms.setTo(numTo);
        sms.setDeliveryReport(true);
        String smsText = SampleTextUtils.generateText16BIT("SmsTest2", 70);
        sms.setText(smsText);
        return sms;
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        new SmsTestTemplate().runTemplate(test, getSms());
    }


}
