package cz.uhk.dip.mmsparams.testsamples.testcases.sms;

import cz.uhk.dip.mmsparams.api.constants.SmsConstants;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmsTest3 extends RunnableTest
{
    public SmsTest3()
    {
        super("SmsTest3", "Zprava delky 320 znaku; 7bit; delivery report");
    }

    static String numTo = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);

        PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

        SmsSendModel sms = new SmsSendModel();
        sms.setTo(numTo);
        sms.setDeliveryReport(true);

        String smsText = SampleTextUtils.generateText("SmsTest3", 320);

        sms.setText(smsText);

        SmsSendMessageId smsSendID = test.Sms().sendSms(sms, phoneA);

        SmsSendResponseModel sendOk = test.Sms().waitFor().smsSentSuccessfully(smsSendID);

        SmsReceiveModel smsReceiveModel = test.Sms().waitFor().anySmsReceived(phoneB);

        SmsDeliveryReport dr = test.Sms().waitFor().anyDeliveryReport(smsSendID);


        // Validations
        ValidationBuilder vb = new ValidationBuilder();

        vb.withEquals(SmsConstants.RESULT_OK, sendOk.getResult(), "Send SMS OK");
        vb.withEquals(smsText, smsReceiveModel.getMessage(), "Sms text");
        // TODO validate DR

        test.Validation().validatePrintThrow(vb);


        test.testFinished();
    }


}
