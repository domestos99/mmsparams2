package cz.uhk.dip.mmsparams.testsamples.testcases.sms;

import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmsTest5 extends RunnableTest
{
    public SmsTest5()
    {
        super("SmsTest5", "Zprava delky 160 znaku; 7bit; bez delivery report");
    }

    static String numTo = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        SmsSendModel sms = new SmsSendModel();
        sms.setTo(numTo);
        sms.setDeliveryReport(false);
        String smsText = SampleTextUtils.generateText("SmsTest5", 160);
        sms.setText(smsText);

        new SmsTestTemplate().runTemplate(test, sms);
    }


}
