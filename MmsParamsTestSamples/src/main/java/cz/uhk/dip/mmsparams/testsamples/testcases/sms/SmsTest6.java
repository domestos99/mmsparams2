package cz.uhk.dip.mmsparams.testsamples.testcases.sms;

import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmsTest6 extends RunnableTest
{
    public SmsTest6()
    {
        super("SmsTest6", "Zprava delky 160 znaku; 7bit; delivery report; obsahuje specialni znaky");
    }

    static String numTo = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        SmsSendModel sms = new SmsSendModel();
        sms.setTo(numTo);
        sms.setDeliveryReport(true);
        String smsText = "SmsTest6 [ x x ] { y y } ^ xxx The quick, brow when MTV ax quiz prog.  [xx]{yy}^xxx";
        sms.setText(smsText);

        new SmsTestTemplate().runTemplate(test, sms);
    }


}
