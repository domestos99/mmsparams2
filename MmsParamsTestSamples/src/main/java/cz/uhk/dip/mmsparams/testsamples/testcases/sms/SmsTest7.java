package cz.uhk.dip.mmsparams.testsamples.testcases.sms;

import cz.uhk.dip.mmsparams.api.constants.SmsConstants;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogLevel;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmsTest7 extends RunnableTest
{
    public SmsTest7()
    {
        super("SmsTest7", "zprava na neplatneho prijemce");
    }

    static String numTo = AppConfig.NumberInvalid;

    @Override
    protected TestSettings init()
    {
        ClientLogFacade.setClientLogLevel(ClientLogLevel.LOG_ALL);
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        PhoneInfoModel deviceOne = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);

        SmsSendModel sms = new SmsSendModel();
        sms.setTo(numTo);
        sms.setDeliveryReport(true);
        String smsText = SampleTextUtils.generateText("SmsTest7", 160);
        sms.setText(smsText);

        SmsSendMessageId smsSendID = test.Sms().sendSms(sms, deviceOne);

        SmsSendResponseModel sendOk = test.Sms().waitFor().smsSentSuccessfully(smsSendID);

        // Validations
        ValidationBuilder vb = new ValidationBuilder();
        vb.withEquals(SmsConstants.RESULT_FIRST_USER, sendOk.getResult(), "Send SMS NOT OK");

        test.Validation().validatePrintThrow(vb);

        test.testFinished();
    }


}
