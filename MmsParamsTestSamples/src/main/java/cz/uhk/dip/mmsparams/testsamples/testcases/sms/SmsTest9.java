package cz.uhk.dip.mmsparams.testsamples.testcases.sms;

import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmsTest9 extends RunnableTest
{
    public SmsTest9()
    {
        super("SmsTest9", "zprava na prijemce ve tvaru " + numTo);
    }

    static String numTo = AppConfig.NumberToBShort;

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        SmsSendModel sms = new SmsSendModel();
        sms.setTo(numTo);
        sms.setDeliveryReport(true);
        String smsText = SampleTextUtils.generateText("SmsTest9", 160);
        sms.setText(smsText);

        new SmsTestTemplate().runTemplate(test, sms);
    }


}
