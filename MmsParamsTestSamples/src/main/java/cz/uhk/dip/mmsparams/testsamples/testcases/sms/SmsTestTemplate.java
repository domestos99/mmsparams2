package cz.uhk.dip.mmsparams.testsamples.testcases.sms;

import cz.uhk.dip.mmsparams.api.constants.SmsConstants;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;
import cz.uhk.dip.mmsparams.testsamples.customvalidations.SmsCenterNumberValidator;

public class SmsTestTemplate
{
    protected void runTemplate(ITestInstance test, SmsSendModel sms)
    {
        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);
        PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

        SmsSendMessageId smsSendID = test.Sms().sendSms(sms, phoneA);

        SmsSendResponseModel sendOk = test.Sms().waitFor().smsSentSuccessfully(smsSendID);

        SmsReceiveModel smsReceiveModel = test.Sms().waitFor().anySmsReceived(phoneB);

        SmsDeliveryReport dr = null;
        boolean deliveryReportTimeouted = false;
        if (sms.isDeliveryReport())
        {
            dr = test.Sms().waitFor().anyDeliveryReport(smsSendID);
        }
        else
        {
            deliveryReportTimeouted = test.Sms().mustTimeoutFor().deliveryReport(smsSendID);
        }

        // Validations
        ValidationBuilder vb = new ValidationBuilder();
        vb.withEquals(SmsConstants.RESULT_OK, sendOk.getResult(), "Send SMS OK");
        vb.withEquals(sms.getText(), smsReceiveModel.getMessage(), "Sms text");

        if (sms.isDeliveryReport())
        {
            vb.withNotNull(dr, "Delivery report");

            // Custom validator - validate SMS center address in SMS Delivery Report
            SmsCenterNumberValidator smsCenterNumberValidator = new SmsCenterNumberValidator(dr.getServiceCenterAddress(), "Service center address");
            vb.withCustomValidation(smsCenterNumberValidator);
        }
        else
        {
            vb.withTrue(deliveryReportTimeouted, "Delivery report not received");
        }

        test.Validation().validatePrintThrow(vb);

        test.testFinished();
    }


}

