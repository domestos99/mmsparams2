package cz.uhk.dip.mmsparams.testsamples.testcases.sms;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendResponseModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmsTestTwoRecipients extends RunnableTest
{
    public SmsTestTwoRecipients()
    {
        super("SmsTestTwoRecipients", "Odeslani zpravy na dva prijemce a kontrola");
    }

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        // Test settings contains Server address; Log Path; Default timeout for all operations
        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);
        PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

        SmsSendModel sms = new SmsSendModel();
        sms.setTo(AppConfig.NumberToB);
        sms.setDeliveryReport(true);
        String smsText = SampleTextUtils.generateText("SmsTest1", 160);
        sms.setText(smsText);

        SmsSendMessageId smsSendIDA = test.Sms().sendSms(sms, phoneA);

        sms.setTo(AppConfig.NumberToB);
        SmsSendMessageId smsSendIDB = test.Sms().sendSms(sms, phoneA);

        SmsSendResponseModel sendOkA = test.Sms().waitFor().smsSentSuccessfully(smsSendIDA);
        SmsSendResponseModel sendOkB = test.Sms().waitFor().smsSentSuccessfully(smsSendIDB);

        SmsReceiveModel smsReceiveModelA = test.Sms().waitFor().anySmsReceived(phoneA);
        SmsReceiveModel smsReceiveModelB = test.Sms().waitFor().anySmsReceived(phoneB);


        SmsDeliveryReport drA = test.Sms().waitFor().anyDeliveryReport(smsSendIDA);
        SmsDeliveryReport drB = test.Sms().waitFor().anyDeliveryReport(smsSendIDA);

        // Validations
        ValidationBuilder vb = ValidationBuilder.create();

        vb.withNotNull(sendOkA, "Send SMS OK A");
        vb.withNotNull(sendOkB, "Send SMS OK B");
        vb.withEquals(smsText, smsReceiveModelA.getMessage(), "Sms text A");
        vb.withEquals(smsText, smsReceiveModelB.getMessage(), "Sms text B");
        vb.withNotNull(drA, "Delivery report A");
        vb.withNotNull(drB, "Delivery report B");

        test.Validation().validatePrintThrow(vb);

        test.testFinished();
    }


}
