package cz.uhk.dip.mmsparams.testsamples.testcases.smsc;

import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscAddressModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogLevel;
import cz.uhk.dip.mmsparams.clientlib.model.smsc.SmscGroup;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmscGroupSample1 extends RunnableTest
{
    public SmscGroupSample1()
    {
        super("SmscGroupSample1", "SMSC test, kde je vyuzita skupina pro SMSC modely");
    }

    static String numFrom = AppConfig.SMSCNumber;
    static String numTo = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        ClientLogFacade.setClientLogLevel(ClientLogLevel.LOG_ALL);
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        SmscConnectModel smscConnectModel = AppConfig.getConnectModel();

        SmscSendModel smscSendModel = new SmscSendModel();
        smscSendModel.setShortMessage("hello world");
        smscSendModel.setSenderAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, numFrom));
        smscSendModel.setRecipientAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, numTo));
        smscSendModel.setDeliveryReport(true);

        SmscGroup group = test.Smsc().SmscGroup().sendSms(smscConnectModel, smscSendModel);


        // Validations
        ValidationBuilder vb = new ValidationBuilder();
        vb.withNotNull(group.getSmscDeliveryReportModel(), "Delivery report not null");

        test.Validation().validatePrintThrow(vb);

        test.testFinished();
    }
}
