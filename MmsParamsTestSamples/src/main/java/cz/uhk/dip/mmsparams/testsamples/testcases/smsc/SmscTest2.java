package cz.uhk.dip.mmsparams.testsamples.testcases.smsc;

import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscAddressModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmscTest2 extends RunnableTest
{
    public SmscTest2()
    {
        super("SmscTest2", "prava delky 70 znaku, 16bit kodovani ze SMSC na mobil");
    }

    static String numTo = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        // Test settings contains Server address; Log Path; Default timeout for all operations
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);


        SmscConnectModel connectModel = AppConfig.getConnectModel();

        // Connect to SMSC
        SmscConnectResponseModel connectResult = test.Smsc().connectToSMSC(connectModel);
        SmscSessionId sessionID = connectResult.getSessionId();

        SmscSendModel sms = getSmsMessage();
        // Send SMS from SMSC
        SmscSendSmsResponseModel sendResult = test.Smsc().sendSms(sessionID, sms);

        SmscDeliveryReportModel smscDeliveryReport = test.Smsc().waitFor().anyDeliveryReport(sessionID);

        // Disconnect from SMSC
        boolean disconnectResult = test.Smsc().disconnect(sessionID);

        // Wait for phone to receive message
        SmsReceiveModel smsReceived = test.Sms().waitFor().anySmsReceived(phoneB);


        ValidationBuilder vb = new ValidationBuilder();

        vb.withEquals(sms.getShortMessage(), smsReceived.getMessage(), "SMS message equals");
        vb.withTrue(disconnectResult, "SMSC disconnect OK");
        // TODO validate smscDeliveryReport

        test.Validation().validatePrintThrow(vb);
        test.testFinished();

    }

    private static SmscSendModel getSmsMessage()
    {
        SmscSendModel sms = new SmscSendModel();

        sms.setShortMessage(SampleTextUtils.generateText16BIT("SmscTest2", 70));
        sms.setUseUCS2(true);

        sms.setSenderAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, AppConfig.SMSCNumber));
        sms.setRecipientAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, numTo));
        sms.setDeliveryReport(true);

        return sms;
    }
}
