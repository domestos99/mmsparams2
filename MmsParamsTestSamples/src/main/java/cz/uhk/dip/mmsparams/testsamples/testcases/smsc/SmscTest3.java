package cz.uhk.dip.mmsparams.testsamples.testcases.smsc;

import cz.uhk.dip.mmsparams.api.constants.smsc.SMSCConstant;
import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscAddressModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.TlvModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;
import cz.uhk.dip.mmsparams.testsamples.testcases.dev.SmscSendOnlyTest;

public class SmscTest3 extends RunnableTest
{
    public SmscTest3()
    {
        super("SmscTest3", "zprava delky 320 znaku, 7bit, deliver report; ze SMSC na mobil");
    }

    static String numTo = AppConfig.NumberToB;

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        // Test settings contains Server address; Log Path; Default timeout for all operations
        PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

        SmscConnectModel connectModel = AppConfig.getConnectModel();

        // Connect to SMSC
        SmscConnectResponseModel connectResult = test.Smsc().connectToSMSC(connectModel);
        SmscSessionId sessionID = connectResult.getSessionId();

        SmscSendModel sms = getSmsMessage();
        // Send SMS from SMSC
        SmscSendSmsResponseModel sendResult = test.Smsc().sendSms(sessionID, sms);

        SmscDeliveryReportModel smscDeliveryReport = test.Smsc().waitFor().anyDeliveryReport(sessionID);

        // Disconnect from SMSC
        boolean disconnectResult = test.Smsc().disconnect(sessionID);

        // Wait for phone to receive message
        SmsReceiveModel smsReceived = test.Sms().waitFor().anySmsReceived(phoneB);


        ValidationBuilder vb = new ValidationBuilder();

        vb.withEquals(smsSendText, smsReceived.getMessage(), "SMS message equals");
        vb.withTrue(disconnectResult, "SMSC disconnect OK");
        // TODO validate smscDeliveryReport

        test.Validation().validatePrintThrow(vb);
        test.testFinished();

    }

    final static String smsSendText = SampleTextUtils.generateText("SmscTest3", 320);

    private static SmscSendModel getSmsMessage()
    {
        SmscSendModel sms = new SmscSendModel();

        sms.setSenderAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, AppConfig.SMSCNumber));
        sms.setRecipientAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, numTo));
        sms.setDeliveryReport(true);

        // Use TLV for long message
        sms.setShortMessage(null);
        TlvModel tlv = new TlvModel();
        tlv.setTag(SMSCConstant.TAG_MESSAGE_PAYLOAD);
        tlv.setValue(smsSendText.getBytes());

        sms.addOptionalParameters(tlv);

        return sms;
    }
}
