package cz.uhk.dip.mmsparams.testsamples.testcases.smsc;

import java.nio.charset.StandardCharsets;

import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.model.SmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmscTest6 extends RunnableTest
{
    public SmscTest6()
    {
        super("SmscTest6", "zprava delky 70 znaku, 16bit kodovani z mobilu na SMSC");
    }

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

        SmscConnectModel connectModel = AppConfig.getConnectModel();

        // Connect to SMSC
        SmscConnectResponseModel connectResult = test.Smsc().connectToSMSC(connectModel);
        SmscSessionId sessionID = connectResult.getSessionId();

        SmsSendModel sms = getSmsMessage();
        // Send SMS from PHONE
        SmsSendMessageId smsSendResult = test.Sms().sendSms(sms, phoneA);

        SmsDeliveryReport smsDR = test.Sms().waitFor().anyDeliveryReport(smsSendResult);


        SmscDeliverSmModel smsReceived = test.Smsc().waitFor().anySmsReceive(sessionID);
        boolean disconnectResult = test.Smsc().disconnect(sessionID);


        ValidationBuilder vb = new ValidationBuilder();

        vb.withEquals(sms.getText(), new String(smsReceived.getShortMessage(), StandardCharsets.UTF_16), "SMS message equals");
        vb.withTrue(disconnectResult, "SMSC disconnect OK");
        // TODO validate smsDR


        test.Validation().validatePrintThrow(vb);
        test.testFinished();

    }

    private static SmsSendModel getSmsMessage()
    {
        SmsSendModel sms = new SmsSendModel();
        sms.setTo(AppConfig.SMSCNumber);
        sms.setText(SampleTextUtils.generateText16BIT("SmscTest6", 70));
        sms.setDeliveryReport(true);

        return sms;
    }
}
