package cz.uhk.dip.mmsparams.testsamples.testcases.smsc;

import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscAddressModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmscTest7 extends RunnableTest
{
    public SmscTest7()
    {
        super("SmscTest7", "Odeslani SMS ze SMSC na SMSC - prijde DR ale zprava ne");
    }

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        SmscConnectModel connectModel = AppConfig.getConnectModel();

        // Connect to SMSC
        SmscConnectResponseModel connectResult = test.Smsc().connectToSMSC(connectModel);
        SmscSessionId sessionID = connectResult.getSessionId();

        SmscSendModel sms = getSmsMessage();


        SmscSendSmsResponseModel smsSendResult = test.Smsc().sendSms(sessionID, sms);
        SmscDeliveryReportModel smsDR = test.Smsc().waitFor().anyDeliveryReport(sessionID);

        boolean smsNotReceived = test.Smsc().mustTimeoutFor().smsReceive(sessionID);

        boolean disconnectResult = test.Smsc().disconnect(sessionID);


        ValidationBuilder vb = new ValidationBuilder();

        vb.withTrue(smsNotReceived, "SMS not received");
        vb.withTrue(disconnectResult, "SMSC disconnect OK");
        // TODO validate smsSendResult
        // TODO validate smsDR


        test.Validation().validatePrintThrow(vb);
        test.testFinished();

    }

    private static SmscSendModel getSmsMessage()
    {
        SmscSendModel sms = new SmscSendModel();

        sms.setShortMessage(SampleTextUtils.generateText("SmscTest7", 160));
        sms.setSenderAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, AppConfig.SMSCNumber));
        sms.setRecipientAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, AppConfig.SMSCNumber));
        sms.setDeliveryReport(true);

        return sms;
    }
}
