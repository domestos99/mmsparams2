package cz.uhk.dip.mmsparams.testsamples.testcases.smsc;

import cz.uhk.dip.mmsparams.api.enums.smsc.NumberingPlanIndicator;
import cz.uhk.dip.mmsparams.api.enums.smsc.TypeOfNumber;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscAddressModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliverSmModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscDeliveryReportModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSendSmsResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.utils.SampleTextUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmscTest8 extends RunnableTest
{
    public SmscTest8()
    {
        super("SmscTest8", "Odeslani SMS ze SMSC na SMSC - 2 bind session (ruzna SMSC)");
    }

    @Override
    protected TestSettings init()
    {
        // !!!!!!
        // ASI NEBUDE FUNGOVAT, KDYZ JE SMSC SERVER STEJNY
        // POUZE UKAZKA, JAK PRIPOJIT VICE SMSC
        // !!!!!!

        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        // Create BIND 1
        SmscConnectModel connectModel = AppConfig.getConnectModel();
        SmscConnectResponseModel connectResult = test.Smsc().connectToSMSC(connectModel);
        SmscSessionId sessionID = connectResult.getSessionId();


        // Create BIND 2
        SmscConnectModel connectModel2 = AppConfig.getConnectModel();
        SmscConnectResponseModel connectResult2 = test.Smsc().connectToSMSC(connectModel2);
        SmscSessionId sessionID2 = connectResult.getSessionId();


        SmscSendModel sms = getSmsMessage();

        SmscSendSmsResponseModel smsSendResult = test.Smsc().sendSms(sessionID, sms);
        SmscDeliveryReportModel smsDR = test.Smsc().waitFor().anyDeliveryReport(sessionID);

        SmscDeliverSmModel smsReceived = test.Smsc().waitFor().anySmsReceive(sessionID2);

        boolean disconnectResult = test.Smsc().disconnect(sessionID);
        boolean disconnectResult2 = test.Smsc().disconnect(sessionID2);


        ValidationBuilder vb = new ValidationBuilder();

        vb.withEquals(sms.getShortMessage(), new String(smsReceived.getShortMessage()), "SMS message equals");

        vb.withTrue(disconnectResult, "SMSC 1 disconnect OK");
        vb.withTrue(disconnectResult2, "SMSC 2 disconnect OK");
        // TODO validate smsSendResult
        // TODO validate smsDR


        test.Validation().validatePrintThrow(vb);
        test.testFinished();

    }

    private static SmscSendModel getSmsMessage()
    {
        SmscSendModel sms = new SmscSendModel();

        sms.setShortMessage(SampleTextUtils.generateText("SmscTest8", 160));
        sms.setSenderAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, AppConfig.SMSCNumber));
        sms.setRecipientAddress(new SmscAddressModel(TypeOfNumber.TON_UNKNOWN, NumberingPlanIndicator.NPI_UNKNOWN, AppConfig.SMSCNumber));
        sms.setDeliveryReport(true);

        return sms;
    }
}
