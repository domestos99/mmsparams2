package cz.uhk.dip.mmsparams.testsamples.testcases.smsc;

import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscConnectResponseModel;
import cz.uhk.dip.mmsparams.api.websocket.model.smsc.SmscSessionId;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogFacade;
import cz.uhk.dip.mmsparams.clientlib.logger.ClientLogLevel;
import cz.uhk.dip.mmsparams.clientlib.utils.ThreadUtils;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class SmscTestConnect extends RunnableTest
{
    public SmscTestConnect()
    {
        super("SmscTestConnect", "Zkouska pripojeni a odpojeni k SMSC");
    }

    @Override
    protected TestSettings init()
    {
        ClientLogFacade.setClientLogLevel(ClientLogLevel.LOG_ALL);
        TestSettings ts = AppConfig.getDefaultTestSettings();
        return ts;
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        SmscConnectResponseModel smscConnectStatus = test.Smsc().connectToSMSC(AppConfig.getConnectModel());
        SmscSessionId smscSession = smscConnectStatus.getSessionId();

        // Sleep 2 seconds
        ThreadUtils.sleep(5000);

        boolean smscDisconnectStatus = test.Smsc().disconnect(smscSession);


        ValidationBuilder vb = new ValidationBuilder();
        // Pripojeni OK
        vb.withTrue(smscConnectStatus.getStatus(), "SMSC Connect status true");
        // Odpojeni OK
        vb.withTrue(smscDisconnectStatus, "SMSC Disconnect status true");

        test.Validation().validatePrintThrow(vb);


        test.testFinished();
    }

}
