package cz.uhk.dip.mmsparams.testsamples.testcases.testTools;

import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class OpenWebDetailTest extends RunnableTest
{
    public static void main(String[] args)
    {
        new OpenWebDetailTest().runTest();
    }

    public OpenWebDetailTest()
    {
        super("OpenWebDetailTest", "Open test detail in browser");
    }

    @Override
    protected TestSettings init()
    {
        return AppConfig.getDefaultTestSettings();
    }

    @Override
    protected void run(ITestInstance test) throws Exception
    {
        test.TestInfo().openTestInWebDetail();
    }
}
