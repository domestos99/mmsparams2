package cz.uhk.dip.mmsparams.testsamples.testcases.wrong;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.clientlib.ITestInstance;
import cz.uhk.dip.mmsparams.clientlib.RunnableTest;
import cz.uhk.dip.mmsparams.clientlib.RunnableTestGroup;
import cz.uhk.dip.mmsparams.clientlib.TestSettings;
import cz.uhk.dip.mmsparams.clientlib.helpers.MmsHelper;
import cz.uhk.dip.mmsparams.clientlib.model.CustomTimeout;
import cz.uhk.dip.mmsparams.clientlib.model.MmsSendMessageId;
import cz.uhk.dip.mmsparams.clientlib.validations.builder.ValidationBuilder;
import cz.uhk.dip.mmsparams.testsamples.AppConfig;

public class MmsWrongTest1 extends RunnableTestGroup
{
    public MmsWrongTest1()
    {
        super("MmsWrongTest1", "Chybne napsany MMS test, ktery zpusobi kolizi pri validacich");
    }

    @Override
    protected void runTests()
    {
        // Odeslat MMS s dorucenim za 60 sekund a ukoncit test
        new MmsWrongTest1Part1().runTest();
        // Odeslat MMS s dorucenim za 320 sekund
        // Cekani na doruceni
        // Prijde MMS z prvniho testu
        // => chyba
        new MmsWrongTest1Part2().runTest();
    }

    static class MmsWrongTest1Part1 extends RunnableTest
    {

        public MmsWrongTest1Part1()
        {
            super("MmsWrongTest - part 1", "Spatna napsany test - part 1");
        }

        @Override
        protected TestSettings init()
        {
            return AppConfig.getDefaultTestSettings();
        }

        @Override
        protected void run(ITestInstance test) throws Exception
        {
            // Sender and recipient are the same so only copy reference
            PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);
            PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

            // Set recipient profile
            MmsRecipientPhoneProfile recProfiles = MmsRecipientPhoneProfile.createDefault();
            boolean profileSetOk = test.Mms().Profile().setRecipinentPhoneProfile(recProfiles, phoneB);

            MmsSendModel mms = getMmsMessage();
            mms.setDeliveryTime(20L);
            MmsSendMessageId mmsSendReqId = test.Mms().sendMms(mms, phoneA);

            test.testFinished();
        }
    }

    static class MmsWrongTest1Part2 extends RunnableTest
    {
        public MmsWrongTest1Part2()
        {
            super("MmsWrongTest - part 2", "Spatna napsany test - part 2");
        }

        @Override
        protected TestSettings init()
        {
            return AppConfig.getDefaultTestSettings();
        }

        @Override
        protected void run(ITestInstance test) throws Exception
        {
            // Sender and recipient are the same so only copy reference
            PhoneInfoModel phoneA = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameA);
            PhoneInfoModel phoneB = test.Phones().getByCustomNameAndLock(AppConfig.CustomNameB);

            MmsSendModel mms2 = getMmsMessage();
            mms2.setText("MmsTest2");
            mms2.setDeliveryTime(320L);
            MmsSendMessageId mmsSendReqId = test.Mms().sendMms(mms2, phoneA);

            RetrieveConfModel receivedMMS = test.Mms().waitFor().anyRetrieveConf(phoneB, CustomTimeout.CUSTOM_TIMEOUT_120);

            ValidationBuilder vb = new ValidationBuilder();

            String mmsText = MmsHelper.tryGetMessageTest(receivedMMS);
            vb.withEquals(mms2.getText(), mmsText, "MMS Test equals");

            test.Validation().validatePrintThrow(vb);

            test.testFinished();
        }
    }

    private static MmsSendModel getMmsMessage()
    {
        String numTo = AppConfig.NumberToB;
        MmsSendModel mms = MmsSendModel.createDefault(numTo, "MmsTest1", true);
        return mms;
    }

}
