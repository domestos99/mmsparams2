@echo off

rmdir ..\MmsParamsServer\src\main\resources\static /S /Q
mkdir ..\MmsParamsServer\src\main\resources\static
xcopy /s "..\MmsParamsServer\frontend\dist" "..\MmsParamsServer\src\main\resources\static"

pause
