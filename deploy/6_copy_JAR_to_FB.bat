@echo off

copy ..\MmsParamsAPI\target\MmsParamsAPI-1.0.3.jar ..\Firebase\angular-web\src\assets\data\MmsParamsAPI-1.0.3.jar
copy ..\MmsParamsClientLib\target\MmsParamsClientLib-1.0.3-jar-with-dependencies.jar ..\Firebase\angular-web\src\assets\data\MmsParamsClientLib-1.0.3.jar
copy ..\MmsParamsServer\target\MmsParamsServer-1.0.3.jar ..\Firebase\angular-web\src\assets\data\MmsParamsServer-1.0.3.jar

pause
